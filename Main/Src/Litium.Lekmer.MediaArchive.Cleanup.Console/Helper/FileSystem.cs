﻿using System.IO;

namespace Litium.Lekmer.MediaArchive.Cleanup.Console
{
	public class FileSystem
	{
		public bool ExistsDir(string path)
		{
			return Directory.Exists(path);
		}

		public void DeleteDir(string path)
		{
			if (ExistsDir(path))
			{
				Directory.Delete(path);
			}
		}

		public void DeleteDirContent(DirectoryInfo directoryInfo)
		{
			foreach (FileInfo file in directoryInfo.GetFiles())
			{
				file.Delete();
			}

			foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
			{
				dir.Delete(true);
			}
		}

		public void CreateDir(string path)
		{
			if (!ExistsDir(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		public void MoveDir(string sourcePath, string destPath)
		{
			var sourceDirInfo = new DirectoryInfo(sourcePath);
			if (sourceDirInfo.Parent == null) return;

			var destination = Path.Combine(destPath, sourceDirInfo.Parent.Name);
			CreateDir(destination);

			destination = Path.Combine(destination, sourceDirInfo.Name);
			DeleteDir(destination);

			Directory.Move(sourcePath, destination);
		}
	}
}