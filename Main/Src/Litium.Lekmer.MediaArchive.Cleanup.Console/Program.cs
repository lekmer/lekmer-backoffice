﻿using System;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.MediaArchive.Cleanup.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main()
		{
			try
			{
				_log.Info("MediaArchive.Cleanup.Console started.");

				Cleanup();

				_log.Info("MediaArchive.Cleanup.Console finished.");
			}
			catch (Exception ex)
			{
				_log.Error("MediaArchive.Cleanup.Console failed.", ex);
			}
		}

		private static void Cleanup()
		{
			var cleanupHelper = new CleanupHelper(new MediaArchiveCleanupSetting());
			cleanupHelper.Cleanup();
		}
	}
}