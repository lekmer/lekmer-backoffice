﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.MediaArchive.Cleanup.Console
{
	public class MediaArchiveCleanupRepository
	{
		public virtual MediaArchiveCleanupType IsMediaFileUsed(int mediaId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("MediaId", mediaId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("MediaArchiveCleanupRepository.IsMediaFileUsed");

			var isMediaFileUsed = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCheckMediaFileUsage]", parameters, dbSettings);

			return (MediaArchiveCleanupType) isMediaFileUsed;
		}
	}
}