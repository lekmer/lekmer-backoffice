﻿using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerImageSize : IImageSize
	{
		/// <summary>
		/// Gets or sets the maximum width for an image to be reformatted to when using this <see cref="T:Litium.Scensum.Media.IImageSize"/> on mobiles.
		/// </summary>
		/// 
		/// <remarks>
		/// Validation should ensure that this property is not lower than or equal to 0.
		/// </remarks>
		int? MobileWidth { get; set; }

		/// <summary>
		/// Gets or sets the maximum height for an image to be reformatted to when using this <see cref="T:Litium.Scensum.Media.IImageSize"/> on mobiles.
		/// </summary>
		/// 
		/// <remarks>
		/// Validation should ensure that this property is not lower than or equal to 0.
		/// </remarks>
		int? MobileHeight { get; set; }

		/// <summary>
		/// Gets or sets a quality indicator between 1 and 100 of the <see cref="T:Litium.Scensum.Media.IImageSize"/> on mobiles.
		/// </summary>
		/// 
		/// <remarks>
		/// Validation should ensure that this property is between 1 and 100.
		/// </remarks>
		int? MobileQuality { get; set; }
	}
}
