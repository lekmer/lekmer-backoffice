﻿using System.Collections.ObjectModel;
using System.IO;
using Litium.Scensum.Core;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerImageService : IImageService
	{
		/// <summary>
		/// Retrieves the resized image as a <see cref="T:System.IO.Stream"/>.
		/// </summary>
		/// <param name="id">The unique identifier of the <see cref="T:Litium.Scensum.Media.IMediaItem"/> from which to retrieve data.</param><param name="extension">The extension of the media file.</param><param name="imageSizeCommonName">Common name of the image size specifiying width/height/quality.</param>
		/// <param name="mobileVersion">When need mobile version of image, send true.</param>
		/// <param name="mime">Returns mime for media.</param>
		/// <returns>
		/// A <see cref="T:System.IO.Stream"/> containing the raw data of the resized image.
		/// </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The supplied <paramref name="id"/> was less than or equal to 0.</exception><exception cref="T:System.ArgumentNullException">The supplied <paramref name="extension"/> was null or empty.</exception>
		Stream LoadImage(int id, string extension, string imageSizeCommonName, bool mobileVersion, out string mime);

		Collection<IImage> GetAllBySizeTable(IUserContext context, int sizeTableId);
	}
}
