﻿using System.Collections.ObjectModel;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerImageSecureService : IImageSecureService
	{
		void SaveMovie(ILekmerImage movie);
		Collection<ILekmerMovie> GetMoviesByProductId(int productId);

	}
}
