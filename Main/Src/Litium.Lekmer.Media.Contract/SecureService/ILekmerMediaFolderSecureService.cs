﻿using System.Collections.ObjectModel;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerMediaFolderSecureService : IMediaFolderSecureService
	{
		Collection<IMediaFolder> GetAllByParentId(int? folderId);

		Collection<IMediaFolder> GetAllByTitle(string title);
	}
}