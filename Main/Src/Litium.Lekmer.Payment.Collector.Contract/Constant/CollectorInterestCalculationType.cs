﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorInterestCalculationType
	{
		Flat = 1,
		Annuity = 2,
	}
}
