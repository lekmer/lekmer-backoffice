﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorResponseCode
	{
		Ok = 0,
		NoRisk = 2,
		TimeoutResponse = 3,
		SpecifiedError = 4,
		UnspecifiedError = 5,
		Denied = 6,
		Pending = 7,
		RepeatableCalls = 10
	}
}
