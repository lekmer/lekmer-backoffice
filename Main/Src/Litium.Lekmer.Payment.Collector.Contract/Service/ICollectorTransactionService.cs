﻿namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorTransactionService
	{
		int CreateGetAddress(int storeId, string civicNumber);
		int CreateAddInvoice(int storeId, string civicNumber, int orderId, string productCode);
		int CreateAddInvoiceTimeout(int storeId, string civicNumber, int orderId, string productCode);
		int CreateInvoiceNotification(int storeId, string queryString);

		void SaveResult(ICollectorResult collectorResult);
		void SaveAddInvoiceResponse(ICollectorAddInvoiceResult collectorAddInvoiceResult);
		void SaveInvoiceNotificationResult(ICollectorNotificationResult collectorNotificationResult);
	}
}
