﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorGetAddressResult : ICollectorResult
	{
		Collection<ICollectorAddress> CollectorAddresses { get; set; }
	}
}