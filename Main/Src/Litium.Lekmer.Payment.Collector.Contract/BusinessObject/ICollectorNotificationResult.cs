﻿namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorNotificationResult : ICollectorResult
	{
		string InvoiceNo { get; set; }
		int OrderNo { get; set; }
		int InvoiceStatus { get; set; }
	}
}
