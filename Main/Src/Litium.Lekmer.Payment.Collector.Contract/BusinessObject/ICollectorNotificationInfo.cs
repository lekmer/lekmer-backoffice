﻿namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorNotificationInfo
	{
		string InvoiceNo { get; set; }
		string OrderNoRaw { get; set; }
		string InvoiceStatusRaw { get; set; }
		int OrderNo { get; set; }
		int InvoiceStatus { get; set; }
	}
}
