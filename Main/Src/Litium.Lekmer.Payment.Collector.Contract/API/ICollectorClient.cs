﻿using Litium.Lekmer.Payment.Collector.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorClient
	{
		ICollectorSetting CollectorSetting { get; set; }

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Sweden.
		/// </summary>
		ICollectorGetAddressResult GetAddress(IChannel channel, string civicNumber, string clientIpAddress);

		/// <summary>
		/// Reserves an amount at Collector.
		/// </summary>
		ICollectorAddInvoiceResult AddInvoice(IChannel channel, ICustomer customer, IOrderFull order, string paymentTypeId, decimal totalAmount);

		void Initialize(string channelCommonName);
	}
}