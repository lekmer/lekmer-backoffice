﻿using System;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Lekmer.Payment.Collector.Setting;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorApi
	{
		ICollectorSetting CollectorSetting { get; set; }

		/// <summary>
		/// The operation returns address information of a customer based on civic registration number, only available in Sweden.
		/// </summary>
		ICollectorGetAddressResponse GetAddress(string civicNumber, string clientIpAddress, string countryCode);

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		ICollectorAddInvoiceResult AddInvoice(
			string clientIpAddress,
			string countryCode,
			string currencyCode,
			int customerId,
			Address deliveryAddress,
			CollectorGender gender,
			Address invoiceAddress,
			InvoiceRow[] invoiceRows,
			CollectorInvoiceType invoiceType,
			DateTime orderDate,
			int orderId,
			string productCode,
			string companyReference,
			string civicNumber
			);

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		ICollectorAddInvoiceResult AddInvoiceTimeout(
			string clientIpAddress,
			string countryCode,
			string currencyCode,
			int customerId,
			Address deliveryAddress,
			CollectorGender gender,
			Address invoiceAddress,
			InvoiceRow[] invoiceRows,
			CollectorInvoiceType invoiceType,
			DateTime orderDate,
			int orderId,
			string productCode,
			string companyReference,
			string civicNumber
			);

		void Initialize(string channelCommonName);
	}
}