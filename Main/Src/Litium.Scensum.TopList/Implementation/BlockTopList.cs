﻿using System;
using Litium.Lekmer.SiteStructure;

namespace Litium.Scensum.TopList
{
	[Serializable]
	public class BlockTopList : LekmerBlockBase, IBlockTopList
	{
		private bool _includeAllCategories;
		private int _orderStatisticsDayCount;
		private int? _linkContentNodeId;
		private string _customUrl;
		private IBlockSetting _setting;

		public bool IncludeAllCategories
		{
			get { return _includeAllCategories; }
			set
			{
				CheckChanged(_includeAllCategories, value);
				_includeAllCategories = value;
			}
		}
		public int OrderStatisticsDayCount
		{
			get { return _orderStatisticsDayCount; }
			set
			{
				CheckChanged(_orderStatisticsDayCount, value);
				_orderStatisticsDayCount = value;
			}
		}
		public int? LinkContentNodeId
		{
			get
			{
				return _linkContentNodeId;
			}
			set
			{
				CheckChanged(_linkContentNodeId, value);
				_linkContentNodeId = value;
			}
		}
		public string CustomUrl
		{
			get
			{
				return _customUrl;
			}
			set
			{
				CheckChanged(_customUrl, value);
				_customUrl = value;
			}
		}
		public string AliasDefaultCommonName { get { return "Sitestructure.Block.ButtonText.{0}"; }}
		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}