﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList.Mapper
{
	public class BlockTopListCategoryDataMapper : DataMapperBase<IBlockTopListCategory>
	{
		private DataMapperBase<ICategory> _categoryDataMapper;

		public BlockTopListCategoryDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_categoryDataMapper = DataMapperResolver.Resolve<ICategory>(DataReader);
		}


		protected override IBlockTopListCategory Create()
		{
			var blockCategory = IoC.Resolve<IBlockTopListCategory>();
			blockCategory.BlockId = MapValue<int>("BlockTopListCategory.BlockId");
			blockCategory.IncludeSubcategories = MapValue<bool>("BlockTopListCategory.IncludeSubcategories");
			blockCategory.Category = _categoryDataMapper.MapRow();
			blockCategory.SetUntouched();
			return blockCategory;
		}
	}
}