﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class BlockTopListProductService : IBlockTopListProductService
	{
		protected BlockTopListProductRepository Repository { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected IBlockTopListCategoryService BlockTopListCategoryService { get; private set; }

		public BlockTopListProductService(
			BlockTopListProductRepository repository,
			IProductService productService,
			IBlockTopListCategoryService blockTopListCategoryService)
		{
			Repository = repository;
			ProductService = productService;
			BlockTopListCategoryService = blockTopListCategoryService;
		}

		/// <summary>
		/// Returns only products with positive stock number.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="block"></param>
		/// <returns></returns>
		public virtual Collection<IBlockTopListProduct> GetAllByBlock(IUserContext context, IBlockTopList block)
		{
			return Repository.GetAllByBlock(context.Channel.Id, block.Id);
		}
	}
}