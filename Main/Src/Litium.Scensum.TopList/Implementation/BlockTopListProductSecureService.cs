using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class BlockTopListProductSecureService : IBlockTopListProductSecureService
	{
		protected BlockTopListProductRepository Repository { get; private set; }

		public BlockTopListProductSecureService(BlockTopListProductRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IBlockTopListProduct> GetAllByBlock(int channelId, int blockId)
		{
			return Repository.GetAllByBlockSecure(channelId, blockId);
		}

		public virtual void Save(int blockId, Collection<IBlockTopListProduct> blockProducts)
		{
			if (blockProducts == null) throw new ArgumentNullException("blockProducts");
			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAll(blockId);
				foreach (var blockProduct in blockProducts)
				{
					blockProduct.BlockId = blockId;
					Repository.Save(blockProduct);
				}
				transaction.Complete();
			}
		}
	}
}