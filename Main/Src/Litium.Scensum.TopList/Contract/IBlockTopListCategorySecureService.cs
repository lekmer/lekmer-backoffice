﻿using System.Collections.ObjectModel;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListCategorySecureService
	{
		Collection<IBlockTopListCategory> GetAllByBlock(int blockId);
		void Save(int blockId, Collection<IBlockTopListCategory> blockCategories);
	}
}