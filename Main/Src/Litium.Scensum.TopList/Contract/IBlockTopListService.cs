using Litium.Scensum.Core;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListService
	{
		IBlockTopList GetById(IUserContext context, int id);
	}
}