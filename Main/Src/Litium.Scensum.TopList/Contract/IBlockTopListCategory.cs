using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListCategory : IBusinessObjectBase
	{
		int BlockId { get; set; }
		ICategory Category { get; set; }
		bool IncludeSubcategories { get; set; }
	}
}