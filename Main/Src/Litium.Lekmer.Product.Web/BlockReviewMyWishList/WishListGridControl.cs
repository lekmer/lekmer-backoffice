﻿using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockReviewMyWishList
{
	public class WishListGridControl : GridControl<WishListProduct>
	{
		public bool EditMode { get; set; }
		public WishListProductUpdateSizeForm WishListProductUpdateSizeForm { get; set; }
		public WishListPackagetUpdateSizeForm WishListPackagetUpdateSizeForm { get; set; }

		protected override string RenderItem(int columnIndex, WishListProduct item)
		{
			ILekmerProductView lekmerProductView = item.Product;
			IWishListItem wishListItem = item.WishListItem;

			bool isValidPackage;
			string packageItemList = RenderPackageItemList(wishListItem, out isValidPackage);

			if (!isValidPackage) return string.Empty;

			Fragment fragmentItem = Template.GetFragment(ItemFragmentName);
			AddPositionCondition(fragmentItem, columnIndex, ColumnCount);

			fragmentItem.AddEntity((IProductView) lekmerProductView);
			fragmentItem.AddVariable("Product.Description", lekmerProductView.Description, VariableEncoding.None);

			bool hasSize = lekmerProductView.DefaultProductSize != null;
			fragmentItem.AddCondition("Product.HasSize", hasSize);
			int? sizeId = null;
			if (hasSize)
			{
				sizeId = lekmerProductView.DefaultProductSize.SizeInfo.Id;
				fragmentItem.AddEntity(lekmerProductView.DefaultProductSize);
			}
			fragmentItem.AddVariable("Iterate:ProductSizeOption", RenderProductSizeOptions(lekmerProductView, sizeId), VariableEncoding.None);

			fragmentItem.AddVariable("WishListItem.Id", wishListItem.Id.ToString);
			fragmentItem.AddVariable("WishListItem.Ordinal", wishListItem.Ordinal.ToString);

			int? hashCode = wishListItem.GetPackageItemsHashCode();
			bool isHashCode = hashCode.HasValue;
			fragmentItem.AddCondition("WishListItem.IsPackageItemsHashCode", isHashCode);
			fragmentItem.AddVariable("WishListItem.PackageItemsHashCode", isHashCode ? hashCode.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
			fragmentItem.AddVariable("Iterate:PackageItem", packageItemList, VariableEncoding.None);
			fragmentItem.AddCondition("EditMode", EditMode);
			WishListProductUpdateSizeForm.MapFieldsToFragment(fragmentItem);
			WishListPackagetUpdateSizeForm.MapFieldsToFragment(fragmentItem);

			return fragmentItem.Render();
		}

		protected virtual string RenderPackageItemList(IWishListItem wishListItem, out bool isPackageValid)
		{
			isPackageValid = true;

			var stringBuilder = new StringBuilder();

			// Wish list item not a package.
			if (wishListItem.PackageItems == null || wishListItem.PackageItems.Count <= 0)
			{
				return stringBuilder.ToString();
			}

			var productService = (ILekmerProductService)IoC.Resolve<IProductService>();

			ProductIdCollection originalProductIds = ObjectCopier.Clone(productService.GetIdAllByPackageMasterProduct(UserContext.Current, wishListItem.ProductId));
			var productIds = new ProductIdCollection(wishListItem.PackageItems.Select(packageItem => packageItem.ProductId));

			// Wishlist package and original package have different items.
			if (originalProductIds.Count != productIds.Count)
			{
				isPackageValid = false;
				return stringBuilder.ToString();
			}

			foreach (int productId in productIds)
			{
				int originalId = originalProductIds.FirstOrDefault(id => id == productId);
				if (originalId > 0)
				{
					originalProductIds.Remove(originalId);
				}
				else
				{
					isPackageValid = false;
					return stringBuilder.ToString();
				}
			}

			ProductCollection packageItems = productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
			foreach (IWishListPackageItem packageItem in wishListItem.PackageItems)
			{
				IProduct productView = packageItems.FirstOrDefault(p => p.Id == packageItem.ProductId);
				if (productView == null) continue;

				var product = (ILekmerProductView) productView;
				IProductSize productSize = null;
				int? sizeId = null;
				if (packageItem.SizeId.HasValue)
				{
					sizeId = packageItem.SizeId.Value;
					productSize = product.ProductSizes.FirstOrDefault(size => size.SizeInfo.Id == sizeId.Value);
				}

				stringBuilder.AppendLine(RenderPackageItem(product, productSize, sizeId).Render());
			}

			return stringBuilder.ToString();
		}
		protected virtual Fragment RenderPackageItem(ILekmerProductView product, IProductSize size, int? sizeId)
		{
			Fragment fragment = Template.GetFragment("PackageItem");
			fragment.AddEntity((IProductView) product);

			bool hasSize = size != null;
			fragment.AddCondition("PackageItem.HasSize", hasSize);
			if (hasSize)
			{
				fragment.AddEntity(size);
			}

			fragment.AddVariable("Iterate:PackageItemSizeOption", RenderPackageItemSizeOptions(product, sizeId), VariableEncoding.None);

			return fragment;
		}


		protected virtual string RenderProductSizeOptions(ILekmerProductView product, int? sizeId)
		{
			var optionBuilder = new StringBuilder();
			foreach (IProductSize size in product.ProductSizes)
			{
				optionBuilder.AppendLine(RenderProductSizeOption(size, sizeId).Render());
			}
			return optionBuilder.ToString();
		}
		protected virtual Fragment RenderProductSizeOption(IProductSize productSize, int? sizeId)
		{
			Fragment fragmentOption = Template.GetFragment("ProductSizeOption");
			fragmentOption.AddCondition("selected", sizeId.HasValue && sizeId.Value == productSize.SizeInfo.Id);
			fragmentOption.AddEntity(productSize);
			return fragmentOption;
		}

		protected virtual string RenderPackageItemSizeOptions(ILekmerProductView product, int? sizeId)
		{
			var optionBuilder = new StringBuilder();
			foreach (IProductSize size in product.ProductSizes)
			{
				optionBuilder.AppendLine(RenderPackageItemSizeOption(size, sizeId).Render());
			}
			return optionBuilder.ToString();
		}
		protected virtual Fragment RenderPackageItemSizeOption(IProductSize productSize, int? sizeId)
		{
			Fragment fragmentOption = Template.GetFragment("PackageItemSizeOption");
			fragmentOption.AddCondition("selected", sizeId.HasValue && sizeId.Value == productSize.SizeInfo.Id);
			fragmentOption.AddEntity(productSize);
			return fragmentOption;
		}
	}
}