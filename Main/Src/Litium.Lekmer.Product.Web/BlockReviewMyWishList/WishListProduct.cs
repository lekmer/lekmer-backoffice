﻿using System;

namespace Litium.Lekmer.Product.Web.BlockReviewMyWishList
{
	[Serializable]
	public class WishListProduct
	{
		public ILekmerProductView Product { get; set; }
		public IWishListItem WishListItem { get; set; }

		public WishListProduct(ILekmerProductView product, IWishListItem item)
		{
			Product = product;
			WishListItem = item;
		}
	}
}
