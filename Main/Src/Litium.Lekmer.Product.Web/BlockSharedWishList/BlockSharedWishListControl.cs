﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Web.BlockReviewMyWishList;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockSharedWishListControl : BlockControlBase<IBlockSharedWishList>
	{
		private readonly ILekmerProductService _productService;
		private readonly IBlockSharedWishListService _blockSharedWishList;
		private readonly IWishListService _wishListService;
		private readonly IWishListPackageItemService _wishListPackageItemService;
		private readonly IWishListCookieService _wishListCookieService;

		private Guid _wishListKey;
		private IWishList _wishList;
		private WishListProductUpdateSizeForm _productUpdateSizeForm;
		private WishListPackagetUpdateSizeForm _packageUpdateSizeForm;
		private Collection<WishListProduct> _wishListProducts;
		private string _postUrl;

		public bool IsEditMode { get; set; }

		protected IPagingControl PagingControl { get; set; }
		protected IFormatter Formatter { get; private set; }

		public BlockSharedWishListControl(
			ITemplateFactory templateFactory,
			IFormatter formatter,
			IBlockSharedWishListService blockSharedWishList,
			IProductService productService,
			IWishListService wishListService,
			IWishListPackageItemService wishListPackageItemService,
			IWishListCookieService wishListCookieService) : base(templateFactory)
		{
			_productService = (ILekmerProductService)productService;
			_blockSharedWishList = blockSharedWishList;
			_wishListService = wishListService;
			_wishListPackageItemService = wishListPackageItemService;
			_wishListCookieService = wishListCookieService;

			Formatter = formatter;
		}

		protected virtual bool IsPostMode(string mode)
		{
			return PostMode.Equals(mode, StringComparison.CurrentCultureIgnoreCase);
		}

		protected virtual string PostUrl
		{
			get
			{
				if (_postUrl.IsEmpty())
				{
					_postUrl = ContentNodeTreeItem.Url.HasValue() ? ResolveUrl(ContentNodeTreeItem.Url) : ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
				}
				return _postUrl;
			}
		}

		protected virtual WishListProductUpdateSizeForm ProductUpdateSizeForm
		{
			get { return _productUpdateSizeForm ?? (_productUpdateSizeForm = new WishListProductUpdateSizeForm(PostUrl)); }
		}

		protected virtual WishListPackagetUpdateSizeForm PackageUpdateSizeForm
		{
			get { return _packageUpdateSizeForm ?? (_packageUpdateSizeForm = new WishListPackagetUpdateSizeForm(PostUrl)); }
		}

		protected override IBlockSharedWishList GetBlockById(int blockId)
		{
			return _blockSharedWishList.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");

			RenderWishList(fragmentContent, pagingContent);

			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual void RenderWishList(Fragment fragment, PagingContent pagingContent)
		{
			fragment.AddCondition("WishListIsEmpty", _wishListProducts.Count == 0);

			fragment.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);

			fragment.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);

			Price price = GetActualPriceSummary();
			fragment.AddVariable("WishList.PriceIncludingVatSummary", Formatter.FormatPrice(Channel.Current, price.IncludingVat));
			fragment.AddVariable("WishList.PriceExcludingVatSummary", Formatter.FormatPrice(Channel.Current, price.ExcludingVat));

			fragment.AddVariable("WishList.TotalProductCount", Formatter.FormatNumber(Channel.Current, _wishListProducts.Count));

			fragment.AddVariable("WishListKey", _wishListKey.ToString());

			fragment.AddCondition("WishList.HasTitle", _wishList != null && !string.IsNullOrEmpty(_wishList.Title));
			fragment.AddVariable("WishList.Title", _wishList != null ? _wishList.Title : null);
		}

		protected virtual string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderProductList()
		{
			var items = new Collection<WishListProduct>();
			var products = _wishListProducts
				.Skip((PagingControl.SelectedPage - 1) * PagingControl.PageSize)
				.Take(PagingControl.PageSize);
			foreach (var wishListProduct in products)
			{
				items.Add(wishListProduct);
			}

			return new WishListGridControl
			{
				Items = items,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace",
				EditMode = false,
				WishListProductUpdateSizeForm = ProductUpdateSizeForm,
				WishListPackagetUpdateSizeForm = PackageUpdateSizeForm
			}.Render();
		}

		protected virtual void Initialize()
		{
			string wishListKey = GetWishListKeyFromQuery();

			try
			{
				_wishListKey = new Guid(wishListKey);
			}
			catch
			{
				_wishListKey = Guid.Empty;
			}

			_wishList = _wishListKey != Guid.Empty ? _wishListService.GetByKey(UserContext.Current, _wishListKey) : null;

			if (_wishList == null)
			{
				//
			}
			else if (IsPostMode(ProductUpdateSizeForm.PostModeValue))
			{
				ProcessUpdateProductSizeMode();
			}
			else if (IsPostMode(PackageUpdateSizeForm.PostModeValue))
			{
				ProcessUpdatePackageSizesMode();
			}

			_wishListProducts = GetProducts();

			PagingControl = CreatePagingControl(_wishListProducts.Count);
		}

		protected virtual void ProcessUpdateProductSizeMode()
		{
			if (_wishList == null)
				return;

			if (!_wishList.GetItems().Any())
				return;

			bool updated = false;
			if (ProductUpdateSizeForm.MapFromRequest())
			{
				int itemId = ProductUpdateSizeForm.WishListItemId;
				int productId = ProductUpdateSizeForm.ProductId;
				int sizeId = ProductUpdateSizeForm.ProductSizeId;

				updated = _wishList.UpdateProductSize(itemId, productId, sizeId);
			}

			if (updated)
			{
				_wishListService.Save(UserContext.Current, _wishList);
				_wishListCookieService.SetWishListKey(_wishListKey);
			}
		}

		protected virtual void ProcessUpdatePackageSizesMode()
		{
			if (_wishList == null)
				return;

			if (!_wishList.GetItems().Any())
				return;

			bool updated = false;
			IWishListItem wishListItem = null;
			if (PackageUpdateSizeForm.MapFromRequest())
			{
				int itemId = PackageUpdateSizeForm.WishListItemId;
				List<string> productIds = PackageUpdateSizeForm.ProductIds.Split(',').ToList();
				List<string> sizeIds = PackageUpdateSizeForm.ProductSizeIds.Split(',').ToList();

				updated = _wishList.UpdatePackageSize(itemId, productIds, sizeIds, out wishListItem);
			}

			if (updated)
			{
				foreach (var packageItem in wishListItem.PackageItems)
				{
					_wishListPackageItemService.Update(packageItem);
				}
				_wishListService.Save(UserContext.Current, _wishList);
				_wishListCookieService.SetWishListKey(_wishListKey);
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<WishListProduct> GetProducts()
		{
			var productCollection = new Collection<WishListProduct>();

			if (_wishList == null || !_wishList.GetItems().Any())
			{
				return productCollection;
			}

			var items = _wishList.GetItems();
			var wishListItems = items as List<IWishListItem> ?? items.ToList();

			var productIdCollection = new ProductIdCollection(wishListItems.Select(x => x.ProductId));
			var products = _productService.PopulateViewProducts(UserContext.Current, productIdCollection);
			foreach (var wishListItem in wishListItems)
			{
				var productView = products.FirstOrDefault(p => p.Id == wishListItem.ProductId);
				if (productView == null) continue;

				var product = (ILekmerProductView)ObjectCopier.Clone(productView);
				if (wishListItem.SizeId.HasValue)
				{
					var productSize = ((ILekmerProductView)productView).ProductSizes.FirstOrDefault(size => size.SizeInfo.Id == wishListItem.SizeId && size.NumberInStock > 0);
					product.DefaultProductSize = productSize;
					productCollection.Add(new WishListProduct(product, wishListItem));
				}
				else
				{
					productCollection.Add(new WishListProduct(product, wishListItem));
				}
			}
			return productCollection;
		}

		protected virtual IPagingControl CreatePagingControl(int totalCount)
		{
			PagingControl = IoC.Resolve<IPagingControl>();
			PagingControl.PageBaseUrl = PostUrl;
			PagingControl.PageQueryStringParameterName = BlockId + "-page";
			PagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			PagingControl.Initialize();
			PagingControl.TotalCount = totalCount;
			return PagingControl;
		}

		public virtual Price GetActualPriceSummary()
		{
			var priceSummary = new Price();
			foreach (var wishListProduct in _wishListProducts)
			{
				priceSummary += GetActualPrice(wishListProduct.Product);
			}
			return priceSummary;
		}

		protected virtual Price GetActualPrice(ILekmerProduct product)
		{
			decimal priceIncludingVat;
			decimal priceExcludingVat;

			if (product.CampaignInfo != null)
			{
				priceIncludingVat = product.CampaignInfo.Price.IncludingVat;
				priceExcludingVat = product.CampaignInfo.Price.ExcludingVat;
			}
			else
			{
				priceIncludingVat = product.Price.PriceIncludingVat;
				priceExcludingVat = product.Price.PriceExcludingVat;
			}

			var price = new Price(priceIncludingVat, priceExcludingVat);
			return price;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetWishListKeyFromQuery()
		{
			return Request.QueryString["wishlist"];
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetWishListItemId()
		{
			return Request.QueryString.GetInt32OrNull("itemid");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetProductId()
		{
			return Request.QueryString.GetInt32OrNull("id");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetSizeId()
		{
			return Request.QueryString.GetInt32OrNull("sizeid");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetOrdinal()
		{
			return Request.QueryString.GetInt32OrNull("ordinal");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetPackageItemsHashCode()
		{
			return Request.QueryString.GetInt32OrNull("packageItemsHashCode");
		}
	}
}