﻿using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Media;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	internal class IconHelper
	{
		private const string _imageUrlMatchPattern = @"\[Icon.ImageUrl\(""(.*?)""\)\]";
		private const string _imageWidthMatchPattern = @"\[Icon.ImageWidth\(""(.*?)""\)\]";
		private const string _imageHeightMatchPattern = @"\[Icon.ImageHeight\(""(.*?)""\)\]";
		private const int _imageUrlMatchCommonNamePosition = 1;

		internal static void AddImageVariables(Fragment fragment, IImage image, string iconTitle)
		{
			string originalSizeImageUrl = null;
			string alternativeText = null;
			string mediaUrl = null;

			if (image != null)
			{
				alternativeText = image.AlternativeText;
				mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(Channel.Current, image);
				originalSizeImageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image, iconTitle);
			}

			fragment.AddCondition("Icon.HasImage", image != null);
			fragment.AddVariable("Icon.ImageUrl", originalSizeImageUrl);
			fragment.AddCondition("Icon.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
			fragment.AddVariable("Icon.ImageAlternativeText", alternativeText);
			fragment.AddVariable("Icon.ImageWidth", image != null ? image.Width.ToString(CultureInfo.CurrentCulture) : null);
			fragment.AddVariable("Icon.ImageHeight", image != null ? image.Height.ToString(CultureInfo.CurrentCulture) : null);

			fragment.AddRegexVariable(
				_imageUrlMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

					return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, image, iconTitle, commonName);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageWidthMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageHeightMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}
	}
}