﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Lekmer.Product.Web.BlockWishListSenderReceiver;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockWishListSenderMultiReceiverControl : BlockControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		private readonly IWishListCookieService _wishListCookieService;
		private BlockWishListSenderMultiReceiverControlForm _wishListSenderMultiReceiverControlForm;
		private bool _wasSent;
		private readonly Guid _wishListKey;
		private readonly string _wishListKeyString;

		public BlockWishListSenderMultiReceiverControl(ITemplateFactory templateFactory, IBlockService blockService, IWishListCookieService wishListCookieService)
			: base(templateFactory)
		{
			_blockService = blockService;
			_wishListCookieService = wishListCookieService;

			_wishListKeyString = Request.QueryString["wishlist"];
			if (string.IsNullOrEmpty(_wishListKeyString))
			{
				_wishListKey = _wishListCookieService.GetWishListKey();
				_wishListKeyString = _wishListKey.ToString();
			}
			else
			{
				try
				{
					_wishListKey = new Guid(_wishListKeyString);
				}
				catch
				{
					_wishListKey = Guid.Empty;
				}
			}
		}

		public BlockWishListSenderMultiReceiverControlForm WishListSenderMulitReceiverForm
		{
			get
			{
				return _wishListSenderMultiReceiverControlForm ??
					(_wishListSenderMultiReceiverControlForm = new BlockWishListSenderMultiReceiverControlForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "BlockWishListSenderMultiReceiver"; }
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			WishListSenderMulitReceiverForm.WishListKey = _wishListKey;

			if (WishListSenderMulitReceiverForm.IsFormPostBack)
			{
				WishListSenderMulitReceiverForm.MapFromRequest();
				validationResult = WishListSenderMulitReceiverForm.Validate();
				if (validationResult.IsValid)
				{
					SendWishList(GetWishListMessageArgs());
					_wasSent = true;
				}
			}
			else
			{
				WishListSenderMulitReceiverForm.ClearFrom();
			}
			return RenderWishListSenderForm(validationResult);
		}

		private WishListMessageArgs GetWishListMessageArgs()
		{
			WishListMessageArgs messageArgs = new WishListMessageArgs();
			messageArgs.Channel = UserContext.Current.Channel;
			messageArgs.Senders = WishListSenderMulitReceiverForm.Sender;
			messageArgs.Message = WishListSenderMulitReceiverForm.Message;
			messageArgs.WishListPageUrl = WishListSenderMulitReceiverForm.WishListPageUrl;
			messageArgs.Receivers = new List<string>(WishListSenderMulitReceiverForm.Receivers.Distinct());
			messageArgs.SenderEmail = WishListSenderMulitReceiverForm.SenderEmail;
			messageArgs.SenderName = WishListSenderMulitReceiverForm.SenderName;
			messageArgs.MailToMysef = WishListSenderMulitReceiverForm.MailToMyself;
			messageArgs.WishListKey = _wishListKeyString;
			return messageArgs;
		}

		private BlockContent RenderWishListSenderForm(ValidationResult validationResult)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			WishListSenderMulitReceiverForm.MapFieldsToFragment(Template, fragmentContent);
			WishListSenderMulitReceiverForm.MapFieldsValueToFragment(Template, fragmentContent);
			RenderValidationResult(fragmentContent, validationResult);
			fragmentContent.AddEntity(Block);
			fragmentContent.AddCondition("WasSent", _wasSent);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			WishListSenderMulitReceiverForm.MapFieldsToFragment(Template, fragment);
			return fragment.Render();
		}

		private static void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		private static void SendWishList(WishListMessageArgs messageArgs)
		{
			var wishListService = IoC.Resolve<IWishListService>();
			wishListService.SendWishList(UserContext.Current, messageArgs);
		}
	}
}