﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Framework.Statistics;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Statistics.TrackerEntity;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.ProductSearch
{
	public class BlockProductSearchResultControl : BlockControlBase<IBlockProductSearchResult>
	{
		private readonly IBlockProductSearchResultService _blockProductSearchResultService;
		private readonly IProductSearchService _productSearchService;

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ProductCollection Products { get; set; }
		protected IPagingControl PagingControl { get; set; }
		protected string SearchQuery
		{
			get { return Request.QueryString["q"]; }
		}

		public BlockProductSearchResultControl(ITemplateFactory templateFactory, IBlockProductSearchResultService blockProductSearchResultService, IProductSearchService productSearchService)
			: base(templateFactory)
		{
			_blockProductSearchResultService = blockProductSearchResultService;
			_productSearchService = productSearchService;
		}

		protected override IBlockProductSearchResult GetBlockById(int blockId)
		{
			return _blockProductSearchResultService.GetById(UserContext.Current, blockId);
		}

		protected virtual void Initialize()
		{
			PagingControl = CreatePagingControl();

			string searchQuery = SearchQuery;
			if (searchQuery.HasValue())
			{
				searchQuery = Regex.Replace(searchQuery, @"[^a-z0-9åäö\s]", "", RegexOptions.IgnoreCase);
			}

			Products = searchQuery.HasValue() ? SearchProducts(searchQuery) : new ProductCollection();
		}

		protected virtual ProductCollection SearchProducts(string searchQuery)
		{
			var searchResult = _productSearchService.Search(UserContext.Current, searchQuery, PagingControl.SelectedPage, PagingControl.PageSize);

			var searchStatisticsEntity = new Search
			{
				ChannelId = Channel.Current.Id,
				Date = DateTime.Now,
				HitCount = searchResult.TotalCount,
				Query = searchQuery
			};
			StatisticsTracker.Track(searchStatisticsEntity);

			return searchResult;
		}

		/// <summary>
		/// Initializes the paging control.
		/// </summary>
		/// <returns>Instance of <see cref="IPagingControl"/>.</returns>
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Page url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("ProductTotalCount", Products.TotalCount.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("ProductDisplayedCount", Products.Count.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageSize", (Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount).ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageCount", ((int)Math.Ceiling((decimal)Products.TotalCount / PagingControl.PageSize)).ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddEntity(Block);

			string searchQuery = SearchQuery.HasValue() ? SearchQuery : string.Empty;
			string head = RenderFragment("Head", searchQuery, pagingContent);
			string footer = RenderFragment("Footer", searchQuery, pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName, string searchQuery, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddVariable("SearchQuery", searchQuery.EncodeHtml(), VariableEncoding.JavaScriptStringEncode);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		/// <summary>
		/// Renders the list of products.
		/// </summary>
		/// <returns>Rendered products.</returns>
		protected virtual string RenderProductList()
		{
			return CreateGridControl().Render();
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual GridControl<IProduct> CreateGridControl()
		{
			return new GridControl<IProduct>
			{
				Items = Products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "BlockProductSearch"; }
		}
	}
}