﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Framework.Statistics;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.ProductFilter.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Statistics.TrackerEntity;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.ProductSearch
{
	public class BlockProductSearchEsalesResultV2Control : BlockControlBase<IBlockProductSearchEsalesResult>
	{
		private readonly Regex _tagGroupParameterRegex = new Regex(@"^taggroup(\d+)-tag-id$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		private readonly IBlockProductSearchEsalesResultService _blockProductSearchEsalesResultService;
		private readonly IEsalesProductSearchServiceV2 _esalesProductSearchService;
		private readonly ITemplateFactory _templateFactory;
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IPriceIntervalService _priceIntervalService;
		private readonly ITagGroupService _tagGroupService;
		private readonly ILekmerProductService _lekmerProductService;

		private readonly SelectedFilterOptions _selectedFilterOptions = new SelectedFilterOptions();
		private readonly AvailableFilterOptions _availableFilterOptions = new AvailableFilterOptions();

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ProductCollection Products { get; set; }
		protected Collection<ISearchSuggestionInfo> Corrections { get; set; }
		protected Collection<ISearchSuggestionInfo> TopSearches { get; set; }
		protected IPagingControl PagingControl { get; set; }
		protected ISearchFacet SearchFacetByFilter { get; set; }
		protected ISearchFacet SearchFacetGeneral { get; set; }

		private string _searchPhrase;
		protected string SearchPhrase
		{
			get
			{
				if (string.IsNullOrEmpty(_searchPhrase))
				{
					_searchPhrase = IsXmlHttpRequest(Request) ? HttpUtility.HtmlDecode(Request.QueryString["q"]) : Request.QueryString["q"];
				}
				return _searchPhrase;
			}
		}

		private EsalesFilterFormControl _filterForm;
		protected virtual EsalesFilterFormControl FilterForm
		{
			get { return _filterForm ?? (_filterForm = CreateFilterFormControl()); }
		}

		private ICommonSession _commonSession;
		public ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
			set { _commonSession = value; }
		}

		/// <summary>
		/// Initializes the <see cref="BlockProductSearchEsalesResultControl"/>.
		/// </summary>
		public BlockProductSearchEsalesResultV2Control(ITemplateFactory templateFactory,
			IBlockProductSearchEsalesResultService blockProductSearchEsalesResultService, IEsalesProductSearchServiceV2 esalesProductSearchService,
			IAgeIntervalService ageIntervalService, IPriceIntervalService priceIntervalService, ITagGroupService tagGroupService, ILekmerProductService lekmerProductService)
			: base(templateFactory)
		{
			_templateFactory = templateFactory;
			_blockProductSearchEsalesResultService = blockProductSearchEsalesResultService;
			_esalesProductSearchService = esalesProductSearchService;
			_ageIntervalService = ageIntervalService;
			_priceIntervalService = priceIntervalService;
			_tagGroupService = tagGroupService;
			_lekmerProductService = lekmerProductService;
		}

		protected override string ModelCommonName
		{
			get { return "BlockProductSearchEsalesV2"; }
		}

		protected override IBlockProductSearchEsalesResult GetBlockById(int blockId)
		{
			return _blockProductSearchEsalesResultService.GetById(UserContext.Current, blockId);
		}

		protected override Template CreateTemplate()
		{
			if (_selectedFilterOptions.UseSecondaryTemplate && Block.SecondaryTemplateId.HasValue)
			{
				return _templateFactory.Create(Block.SecondaryTemplateId.Value);
			}
			return base.CreateTemplate();
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			string searchQuery = SearchPhrase.HasValue() ? SearchPhrase : string.Empty;
			var hasProductResults = Products.TotalCount > 0;
			var hasCorrections = Corrections != null && Corrections.Count > 0;
			var hasTopSearches = TopSearches != null && TopSearches.Count > 0;

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddCondition("HasProductResults", hasProductResults);
			fragmentContent.AddCondition("HasCorrections", hasCorrections);
			fragmentContent.AddCondition("HasTopSearches", hasTopSearches);
			fragmentContent.AddVariable("FilterForm", FilterForm.Render(), VariableEncoding.None);
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("ProductTotalCount", Products.TotalCount.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("ProductDisplayedCount", Products.Count.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageSize", _selectedFilterOptions.PageSize.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageCount", ((int)Math.Ceiling((decimal)Products.TotalCount / PagingControl.PageSize)).ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddVariable("CorrectionList", RenderCorrectionList(), VariableEncoding.None);
			fragmentContent.AddVariable("TopSearchesList", RenderTopSearchesList, VariableEncoding.None);
			fragmentContent.AddVariable("SearchQuery", searchQuery);
			fragmentContent.AddEntity(Block);

			string head;
			string footer;
			RenderHeadAndFooter(searchQuery, pagingContent, out head, out footer);

			return new BlockContent(head, fragmentContent.Render(), footer);
		}
		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#"), SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
		protected virtual void RenderHeadAndFooter(string searchQuery, PagingContent pagingContent, out string head, out string footer)
		{
			var idsBuilder = new StringBuilder();
			var idsCriteoBuilder = new StringBuilder();
			var erpIdsBuilder = new StringBuilder();
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			for (int i = 0; i < Math.Min(Products.Count, 3); i++)
			{
				var product = Products[i];

				Fragment headProductItem = Template.GetFragment("HeadProductItem");
				Fragment footerProductItem = Template.GetFragment("FooterProductItem");
				AddVariable("ProductItem.Id", product.Id.ToString(CultureInfo.InvariantCulture), headProductItem, footerProductItem);
				AddVariable("ProductItem.ErpId", product.ErpId, headProductItem, footerProductItem);
				headItemBuilder.AppendLine(headProductItem.Render());
				footerItemBuilder.AppendLine(footerProductItem.Render());

				idsCriteoBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0},", product.Id));
				erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "\'{0}\',", product.ErpId));
			}

			foreach (var product in Products)
			{
				idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0},", product.Id));
			}

			var ids = RemovelastSeparator(idsBuilder.ToString());
			var idsCriteo = RemovelastSeparator(idsCriteoBuilder.ToString());
			var erpIds = RemovelastSeparator(erpIdsBuilder.ToString());
			var headProductItems = headItemBuilder.ToString();
			var footerProductItems = footerItemBuilder.ToString();

			head = RenderFragment("Head", searchQuery, ids, idsCriteo, erpIds, headProductItems, pagingContent.Head);
			footer = RenderFragment("Footer", searchQuery, ids, idsCriteo, erpIds, footerProductItems, pagingContent.Head);
		}
		protected virtual void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}
		protected virtual string RenderFragment(string fragmentName, string searchQuery, string ids, string idsCriteo, string erpIds, string productItems, string pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", ids);
			fragment.AddVariable("Param.ProductIds.Criteo", idsCriteo);
			fragment.AddVariable("Param.ProductErpIds", erpIds, VariableEncoding.None);
			fragment.AddVariable("SearchQuery", searchQuery.EncodeHtml(), VariableEncoding.JavaScriptStringEncode);
			fragment.AddVariable(fragmentName == "Head" ? "Iterate:HeadProductItem" : "Iterate:FooterProductItem", productItems, VariableEncoding.None);
			return (fragment.Render() ?? string.Empty) + (pagingContent ?? string.Empty);
		}
		/// <summary>
		/// Renders the list of products.
		/// </summary>
		protected virtual string RenderProductList()
		{
			return CreateGridControl().Render();
		}
		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		protected virtual GridControl<IProduct> CreateGridControl()
		{
			string settingName = CommonSession.IsViewportMobile ? "ColumnCountMobile" : "ColumnCount";
			int columnCount; 
			if (!int.TryParse(Template.GetSettingOrNull(settingName), out columnCount))
			{
				columnCount = 1;
			}

			return new ColorVarialtsGridControl
			{
				LekmerProductService = _lekmerProductService,
				Items = Products,
				ColumnCount = columnCount,
				RowCount = (int)Math.Ceiling((decimal)_selectedFilterOptions.PageSize / columnCount),
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};
		}

		protected virtual string RenderCorrectionList()
		{
			Fragment fragment = Template.GetFragment("CorrectionList");

			var stringBuilder = new StringBuilder();
			if (Corrections != null)
			{
				foreach (var corrections in Corrections)
				{
					stringBuilder.AppendLine(RenderSearchSuggestionInfoItem(corrections, "CorrectionItem").Render());
				}
			}
			fragment.AddVariable("Iterate:CorrectionItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment.Render();
		}

		protected virtual string RenderTopSearchesList()
		{
			Fragment fragment = Template.GetFragment("TopSearchesList");

			var stringBuilder = new StringBuilder();
			if (TopSearches != null)
			{
				foreach (var topSearch in TopSearches)
				{
					stringBuilder.AppendLine(RenderSearchSuggestionInfoItem(topSearch, "TopSearchesItem").Render());
				}
			}
			fragment.AddVariable("Iterate:TopSearchesItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment.Render();
		}

		protected virtual Fragment RenderSearchSuggestionInfoItem(ISearchSuggestionInfo searchSuggestionInfo, string fragmentName)
		{
			Fragment fragment = Template.GetFragment(fragmentName);
			fragment.AddVariable("Item.Text", searchSuggestionInfo.Text);
			fragment.AddVariable("Item.Text.UrlEncode", searchSuggestionInfo.Text, VariableEncoding.UrlEncode);
			fragment.AddVariable("Item.Ticket", searchSuggestionInfo.Ticket);
			return fragment;
		}

		private string RemovelastSeparator(string value)
		{
			if (string.IsNullOrEmpty(value)) return value;

			var position = value.Length - 1;
			if (position < 0) return value;

			if (value[position] == ',')
			{
				value = value.Substring(0, position);
			}
			return value;
		}

		protected virtual void Initialize()
		{
			SearchFacetByFilter = new SearchFacet();
			SearchFacetGeneral = new SearchFacet();

			PopulateSelectedFilterOptions();
			PopulateAvailableAgeAndPriceFilterOptions();
			PagingControl = CreatePagingControl();

			Products = SearchPhrase.HasValue() ? SearchProducts(SearchPhrase) : new ProductCollection();

			PopulateAvailableFilterOptions();
		}

		// Selected filter options
		private void PopulateSelectedFilterOptions()
		{
			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			_selectedFilterOptions.UseSecondaryTemplate = useSecondaryTemplate ?? false;

			int? pageSize = Request.QueryString.GetInt32OrNull("pagesize");
			_selectedFilterOptions.PageSize = pageSize ?? GetDefaultPageSize();

			_selectedFilterOptions.SetSortOption(SortOption.Popularity.ToString());

			string mode = Request.QueryString[FilterForm.ModeFormName];
			if (mode != null && mode.Equals(FilterForm.ModeValueFilter, StringComparison.InvariantCultureIgnoreCase))
			{
				SetSelectedFromQuery(_selectedFilterOptions.BrandIds, "brand-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level1CategoryIds, "level1category-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level2CategoryIds, "level2category-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level3CategoryIds, "level3category-id");
				SetSelectedFromQuery(_selectedFilterOptions.SizeIds, "size-id");
				SetSelectedAgeFromQuery(_selectedFilterOptions, "age_month-from", "age_month-to");
				SetSelectedPriceFromQuery(_selectedFilterOptions, "price-from", "price-to");
				SetSelectedTagIdsFromQuery();
				SetSelectedSortOrderFromQuery();
			}
		}
		private void SetSelectedAgeFromQuery(SelectedFilterOptions selectedFilterOptions, string parameterFrom, string parameterTo)
		{
			int? fromMonth = Request.QueryString.GetInt32OrNull(parameterFrom);
			int? toMonth = Request.QueryString.GetInt32OrNull(parameterTo);

			if (fromMonth.HasValue || toMonth.HasValue)
			{
				selectedFilterOptions.AgeInterval = _ageIntervalService.GetAgeInterval(fromMonth, toMonth);
			}
		}
		private void SetSelectedPriceFromQuery(SelectedFilterOptions selectedFilterOptions, string parameterFrom, string parameterTo)
		{
			decimal? from = null;
			decimal? to = null;
			decimal parsedValue;

			if (!Request.QueryString[parameterFrom].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterFrom], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					from = parsedValue;
				}
			}

			if (!Request.QueryString[parameterTo].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterTo], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					to = parsedValue;
				}
			}

			if (from.HasValue || to.HasValue)
			{
				selectedFilterOptions.PriceInterval = _priceIntervalService.GetPriceInterval(from, to);
			}
		}
		private void SetSelectedTagIdsFromQuery()
		{
			var parameters = Request.QueryString.AllKeys.Distinct();
			foreach (string parameter in parameters)
			{
				if (parameter.IsEmpty()) continue;

				Match match = _tagGroupParameterRegex.Match(parameter);
				if (!match.Success) continue;

				int tagGroupId;
				if (!int.TryParse(match.Groups[1].Value, out tagGroupId)) continue;

				IEnumerable<int> tagIds = GetSelectedFromQuery(parameter);
				_selectedFilterOptions.GroupedTagIds.Add(tagIds);
			}
		}
		private void SetSelectedSortOrderFromQuery()
		{
			_selectedFilterOptions.SetSortOption(Request.QueryString["sort"]);
		}
		private void SetSelectedFromQuery(ICollection<int> selectedItems, string parameter)
		{
			IEnumerable<int> items = GetSelectedFromQuery(parameter);
			foreach (int item in items)
			{
				selectedItems.Add(item);
			}
		}
		private IEnumerable<int> GetSelectedFromQuery(string parameter)
		{
			if (Request.QueryString[parameter].IsNullOrTrimmedEmpty())
			{
				return new int[0];
			}

			return Request.QueryString[parameter]
				.Split(new[] { ',' })
				.Select(value =>
				{
					int intValue;
					return int.TryParse(value, out intValue) ? intValue : 0;
				})
				.Where(value => value != 0).Distinct();
		}

		// Available filter options
		private void PopulateAvailableFilterOptions()
		{
			_availableFilterOptions.Brands = SearchFacetGeneral.AllBrandsWithoutFilter;
			_availableFilterOptions.Sizes = SearchFacetGeneral.AllSizesWithoutFilter;
			_availableFilterOptions.TagGroups = _tagGroupService.GetAll(UserContext.Current);

			// Level 1 categories.
			_availableFilterOptions.Level1Categories = SearchFacetGeneral.AllMainCategoriesWithoutFilter;

			// Level 2 categories.
			if (_selectedFilterOptions.Level1CategoryIds.Count == 1)
			{
				_availableFilterOptions.Level2Categories = SearchFacetGeneral.AllParentCategoriesWithoutFilter;
			}

			// Level 3 categories.
			if (_selectedFilterOptions.Level2CategoryIds.Count == 1)
			{
				_availableFilterOptions.Level3Categories = SearchFacetGeneral.AllCategoriesWithoutFilter;
			}

			int? secondaryTemplateId = Block.SecondaryTemplateId;
			_availableFilterOptions.HasSecondaryTemplate = secondaryTemplateId.HasValue;

			// Page size options.
			_availableFilterOptions.PageSizeOptions = GetPageSizeOptions();

			// Sort options.
			_availableFilterOptions.SortOptions = new Dictionary<SortOption, string>
				{
					{SortOption.Popularity, AliasHelper.GetAliasValue("ProductFilter.SortOption.Popularity")},
					{SortOption.IsNewFrom, AliasHelper.GetAliasValue("ProductFilter.SortOption.IsNewFrom")},
					{SortOption.TitleAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleAsc")},
					{SortOption.TitleDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleDesc")},
					{SortOption.PriceAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceAsc")},
					{SortOption.PriceDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceDesc")},
					{SortOption.DiscountPercentDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.DiscountPercentDesc")}
				};
		}
		private void PopulateAvailableAgeAndPriceFilterOptions()
		{
			var context = UserContext.Current;
			Collection<IAgeInterval> ageIntervals = _ageIntervalService.GetAll(context);
			Collection<IPriceInterval> priceIntervals = _priceIntervalService.GetAll(context);
			_availableFilterOptions.AgeIntervalOuter = _ageIntervalService.GetAgeIntervalOuter(ageIntervals);
			_availableFilterOptions.PriceIntervalOuter = _priceIntervalService.GetPriceIntervalOuter(priceIntervals);
		}

		// Page size
		private IEnumerable<int> GetPageSizeOptions()
		{
			string settingName = CommonSession.IsViewportMobile ? "PageSizeOptionsMobile" : "PageSizeOptions";
			string settingPageSizeOptions = Template.GetSettingOrNull(settingName) ?? string.Empty;
			IEnumerable<int> pageSizeOptions = settingPageSizeOptions
				.Split(new[] { ',' })
				.Select(pageSize =>
				{
					int intPageSize;
					return int.TryParse(pageSize, out intPageSize) ? intPageSize : 0;
				})
				.Where(pageSize => pageSize > 0);

			return !pageSizeOptions.Any()
				? new List<int> { GetDefaultPageSize() }
				: pageSizeOptions;
		}
		private int GetDefaultPageSize()
		{
			string settingName = CommonSession.IsViewportMobile ? "DefaultPageSizeMobile" : "DefaultPageSize";
			string settingDefaultPageSize = Template.GetSettingOrNull(settingName);
			int defaultPageSize;
			if (!int.TryParse(settingDefaultPageSize, out defaultPageSize))
			{
				defaultPageSize = 20;
			}
			return defaultPageSize;
		}

		// Paging control
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = _selectedFilterOptions.PageSize;
			pagingControl.Initialize();
			return pagingControl;
		}

		// Search
		protected virtual ProductCollection SearchProducts(string searchPhrase)
		{
			var query = _selectedFilterOptions.BuildEsalesQuery(_availableFilterOptions);

			ISearchSuggestions searchSuggestions = _esalesProductSearchService.SearchProducts(UserContext.Current, searchPhrase, query, PagingControl.SelectedPage, PagingControl.PageSize);
			SearchFacetGeneral = searchSuggestions.SearchFacetWithoutFilter;
			Corrections = searchSuggestions.Corrections;
			TopSearches = searchSuggestions.TopSearches;

			FilterForm.FilterStatistics = SearchFacetByFilter = searchSuggestions.SearchFacetWithFilter;
			ProductCollection products = searchSuggestions.Products;

			PopulateSearchStatistics(searchPhrase, products.TotalCount);

			return products;
		}
		private void PopulateSearchStatistics(string searchPhrase, int totalHitCount)
		{
			var searchStatisticsEntity = new Search
			{
				ChannelId = Channel.Current.Id,
				Date = DateTime.Now,
				HitCount = totalHitCount,
				Query = searchPhrase
			};
			StatisticsTracker.Track(searchStatisticsEntity);
		}

		private EsalesFilterFormControl CreateFilterFormControl()
		{
			return new EsalesFilterFormControl
			{
				Template = Template,
				AvailableFilterOptions = _availableFilterOptions,
				FilterStatistics = SearchFacetByFilter,
				SelectedFilterOptions = _selectedFilterOptions
			};
		}
		private bool IsXmlHttpRequest(HttpRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			string header = request.Headers["X-Requested-With"];
			if (header == null) return false;

			return header.Equals("XMLHttpRequest", StringComparison.OrdinalIgnoreCase);
		}
	}
}