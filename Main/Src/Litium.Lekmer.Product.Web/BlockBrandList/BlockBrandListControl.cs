﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockBrandList
{
	public class BlockBrandListControl : BlockControlBase<IBlockBrandList>
	{
		private readonly IBlockBrandListService _blockBrandListService;
		private readonly IBrandService _brandService;

		protected IPagingControl PagingControl { get; set; }
		protected BrandCollection Brands { get; set; }

		public BlockBrandListControl(
			ITemplateFactory templateFactory,
			IBlockBrandListService blockBrandListService,
			IBrandService brandService)
			: base(templateFactory)
		{
			_blockBrandListService = blockBrandListService;
			_brandService = brandService;
		}

		protected override IBlockBrandList GetBlockById(int blockId)
		{
			return _blockBrandListService.GetById(UserContext.Current, blockId);
		}

		private void Initialize()
		{
			PagingControl = CreatePagingControl();
			Brands = _brandService.GetAllByBlock(UserContext.Current, Block, PagingControl.SelectedPage, PagingControl.PageSize);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Brands.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("BrandList", RenderBrandList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("BrandTotalQuantity", GetBrandsTotalQuantity(), VariableEncoding.None);
			fragmentContent.AddVariable("BrandTotalCount", Brands.TotalCount.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("BrandDisplayedCount", Brands.Count.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageSize", (Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount).ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageCount", ((int)Math.Ceiling((decimal)Brands.TotalCount / PagingControl.PageSize)).ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderBrandList()
		{
			var grid = new GridControl<IBrand>
			{
				Items = Brands,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "BrandList",
				RowFragmentName = "BrandRow",
				ItemFragmentName = "Brand",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Product url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			if (ContentNodeTreeItem.Url.HasValue())
			{
				return ResolveUrl(ContentNodeTreeItem.Url);
			}

			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		private static string GetBrandsTotalQuantity()
		{
			var brandService = IoC.Resolve<IBrandService>();
			var allBrands = brandService.GetAll(UserContext.Current);
			return allBrands != null
				? allBrands.Where(t=>!t.IsOffline).ToList().Count.ToString(CultureInfo.InvariantCulture)
				: string.Empty;
		}
	}
}