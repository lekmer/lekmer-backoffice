﻿using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockImageRotator
{
	public class ImageHelper
	{
		private const string _mainImageUrlMatchPattern = @"\[MainImage.Url\(""(.*?)""\)\]";
		private const string _thumbnailImageUrlMatchPattern = @"\[ThumbnailImage.Url\(""(.*?)""\)\]";
		private const int _imageUrlMatchCommonNamePosition = 1;

		public static void AddMainImageVariables(Fragment fragment, IImage item)
		{
			fragment.AddCondition("MainImage.HasImage", item != null);
			if (item == null)
				return;

			fragment.AddVariable("MainImage.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("MainImage.Title", item.Title);
			fragment.AddVariable("MainImage.AlternativeText", item.AlternativeText);

			string mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(Channel.Current, item);

			fragment.AddVariable("MainImage.Url", MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, item));

			fragment.AddRegexVariable(
				_mainImageUrlMatchPattern,
				delegate(Match match)
				{
					string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

					return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, item, commonName);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);
		}

		public static void AddThumbnailImageVariables(Fragment fragment, IImage item)
		{
			fragment.AddCondition("ThumbnailImage.HasImage", item != null);
			if (item == null)
				return;

			fragment.AddVariable("ThumbnailImage.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("ThumbnailImage.Title", item.Title);
			fragment.AddVariable("ThumbnailImage.AlternativeText", item.AlternativeText);

			string mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(Channel.Current, item);

			fragment.AddVariable("ThumbnailImage.Url", MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, item));

			fragment.AddRegexVariable(
				_thumbnailImageUrlMatchPattern,
				delegate(Match match)
				{
					string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

					return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, item, commonName);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);
		}
	}
}
