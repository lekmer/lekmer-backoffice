﻿using Litium.Lekmer.Esales;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Product.Web
{
	public abstract class LekmerProductPageHandler : ProductPageHandler, IEsalesGeneralControlResponse
	{
		protected override AreaHandler CreateAreaHandlerInstance(IContentAreaFull contentArea)
		{
			return LekmerAreaHandlerFactory.CreateAreaHandlerInstance(ContentPage, ContentNodeTreeItem, contentArea, this);
		}

		/// <summary>
		/// Esales data that could be displayed on page.
		/// </summary>
		public IEsalesResponse EsalesGeneralResponse { get; set; }
	}
}