using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockProductRelationListEntityMapper : BlockEntityMapper<IBlockProductRelationList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductRelationList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));

			LekmerBlockHelper.AddLekmerBlockVariables(fragment, item);
		}
	}
}