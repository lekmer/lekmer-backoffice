﻿using System;
using Litium.Lekmer.Esales;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Web
{
	/// <summary>
	/// Base class for block controls which should be displayed on a product page.
	/// </summary>
	public abstract class LekmerBlockProductPageControlBase<TBlock> : BlockProductPageControlBase<TBlock> where TBlock : class, IBlock
	{
		/// <summary>
		/// Initializes the <see cref="LekmerBlockProductPageControlBase{TBlock}"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		protected LekmerBlockProductPageControlBase(ITemplateFactory templateFactory)
			: base(templateFactory)
		{
		}

		[Obsolete("Obsolete since 2.1. Use constructor with templateFactory parameter.")]
		protected LekmerBlockProductPageControlBase()
		{
		}

		private IEsalesResponse _generalEsalesResponse;

		/// <summary>
		/// Esales data that could be displayed on page.
		/// </summary>
		public IEsalesResponse GeneralEsalesResponse 
		{
			get
			{
				if (_generalEsalesResponse == null)
				{
					var productPageHandler = ContentPageHandler as LekmerProductPageHandler;
					if (productPageHandler == null) return null;
					_generalEsalesResponse = productPageHandler.GeneralEsalesResponse;
				}
				return _generalEsalesResponse;
			}
			set
			{
				var productPageHandler = ContentPageHandler as LekmerProductPageHandler;
				if (productPageHandler != null)
				{
					productPageHandler.GeneralEsalesResponse = value;
				}
			}
		}
	}
}
