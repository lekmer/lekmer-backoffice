﻿using System.Globalization;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class ProductSizeEntityMapper : EntityMapper<IProductSize>
	{
		public override void AddEntityVariables(Fragment fragment, IProductSize item)
		{
			var size = item.SizeInfo;
			if (size != null)
			{
				fragment.AddVariable("Size.ProductId", item.ProductId.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.Id", size.Id.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.EU", FormatSize(item.OverrideEu ?? size.Eu));
				fragment.AddVariable("Size.EUTitle", size.EuTitle);
				fragment.AddVariable("Size.USMale", FormatSize(size.UsMale));
				fragment.AddVariable("Size.USFemale", FormatSize(size.UsFemale));
				fragment.AddVariable("Size.UKMale", FormatSize(size.UkMale));
				fragment.AddVariable("Size.UKFemale", FormatSize(size.UkFemale));
				fragment.AddVariable("Size.Millimeter", (item.OverrideMillimeter ?? size.Millimeter + (item.MillimeterDeviation ?? 0)).ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.NumberInStock", item.NumberInStock.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.ErpId", item.ErpId);
				fragment.AddCondition("Size.IsInStock", item.NumberInStock > 0);
				fragment.AddCondition("Size.IsPassive", item.IsPassiveSize());
				AddStockRange(fragment, item);
			}
		}

		private static string FormatSize(decimal size)
		{
			var intSize = decimal.ToInt32(size);
			return size - intSize == 0
				? intSize.ToString(CultureInfo.CurrentCulture)
				: size.ToString(CultureInfo.CurrentCulture);
		}

		private void AddStockRange(Fragment fragment, IProductSize item)
		{
			bool hasStockRange = false;
			string stockRangeValue = string.Empty;

			if (item.NumberInStock > 0)
			{
				var stockRangeService = IoC.Resolve<IStockRangeService>();
				var stockRanges = stockRangeService.GetAll(UserContext.Current);
				foreach (var stockRange in stockRanges)
				{
					if ((stockRange.StartValue == null || item.NumberInStock >= stockRange.StartValue)
						&& (stockRange.EndValue == null || item.NumberInStock <= stockRange.EndValue))
					{
						hasStockRange = true;
						stockRangeValue = stockRange.CommonName;
					}
				}
			}

			fragment.AddCondition("Size.HasStockRange", hasStockRange);
			fragment.AddVariable("Size.StockRangeValue", stockRangeValue);
		}
	}
}