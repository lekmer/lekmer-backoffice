﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Product.Web.BlockMonitorProduct
{
	public class BlockMonitorProductControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IBlockService _blockService;

		public BlockMonitorProductControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory)
		{
			_blockService = blockService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);

			var control = new MonitorProductControl((ILekmerProduct) Product, fragmentContent);
			control.Render();

			string content = fragmentContent.Render();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");

			return new BlockContent(head, content, footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}
	}
}