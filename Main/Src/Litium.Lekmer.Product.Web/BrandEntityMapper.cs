﻿using System;
using System.Globalization;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BrandEntityMapper : EntityMapper<IBrand>
	{
		public override void AddEntityVariables(Fragment fragment, IBrand item)
		{
			fragment.AddVariable("Brand.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Brand.Title", item.Title);
			fragment.AddVariable("Brand.ExternalUrl", item.ExternalUrl);

			string brandUrl = GetBrandUrl(item);
			fragment.AddCondition("Brand.HasUrl", !brandUrl.IsNullOrEmpty());
			fragment.AddVariable("Brand.Url", brandUrl);

			fragment.AddVariable("Brand.Description", item.Description, VariableEncoding.None);

			BrandHelper.AddImageVariables(fragment, item.Image, item.Title);
		}

		private static string GetBrandUrl(IBrand brand)
		{
			if (brand == null) throw new ArgumentNullException("brand");
			if (!brand.ContentNodeId.HasValue)
			{
				return null;
			}

			var contentNodeService = IoC.Resolve<IContentNodeService>();
			var contentNodeTreeItem = contentNodeService.GetTreeItemById(UserContext.Current, brand.ContentNodeId.Value);
			if (contentNodeTreeItem == null || contentNodeTreeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return UrlHelper.ResolveUrlHttp(contentNodeTreeItem.Url);
		}
	}
}