﻿using Litium.Lekmer.Campaign;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class FlagEntityMapper : EntityMapper<IFlag>
	{
		public override void AddEntityVariables(Fragment fragment, IFlag item)
		{
			fragment.AddVariable("Flag.Title", item.Title);
			fragment.AddVariable("Flag.Class", item.Class);
		}
	}
}