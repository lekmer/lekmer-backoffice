﻿using System;
using System.Diagnostics;
using System.Reflection;
using Litium.Lekmer.GFK.Contract;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.GFK.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.InfoFormat("GFK.Console started.");

				var gfkStatisticsExporter = IoC.Resolve<IExporter>("GfkStatisticsExporter");
				gfkStatisticsExporter.Execute();

				_log.InfoFormat("GFK.Console completed.");
				stopwatch.Stop();
				_log.InfoFormat("GFK.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch (Exception ex)
			{
				_log.Error("GFK.Console failed.", ex);
			}
		}
	}
}