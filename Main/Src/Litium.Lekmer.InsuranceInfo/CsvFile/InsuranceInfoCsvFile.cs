﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Reflection;
using Litium.Lekmer.InsuranceInfo.Contract;

namespace Litium.Lekmer.InsuranceInfo
{
	public class InsuranceInfoCsvFile
	{
		private IInsuranceInfoExportSetting _insuranceInfoExportSetting;
		private string _csvFileDelimiter;
		private Collection<IInsuranceInfo> _insuranceInfoCollection;

		private bool _isInitialized;

		public void Initialize(IInsuranceInfoExportSetting insuranceInfoExportSetting, Collection<IInsuranceInfo> insuranceInfoCollection)
		{
			if (insuranceInfoExportSetting == null)
			{
				throw new ArgumentNullException("insuranceInfoExportSetting");
			}

			_insuranceInfoExportSetting = insuranceInfoExportSetting;
			_csvFileDelimiter = insuranceInfoExportSetting.FileDelimiter;
			_insuranceInfoCollection = insuranceInfoCollection;

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) and should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap, System.Text.Encoding.GetEncoding(_insuranceInfoExportSetting.Encoding));

			// write field titles.
			writer.WriteLine(AddHeaderRow());

			foreach (IInsuranceInfo insuranceInfo in _insuranceInfoCollection)
			{
				writer.WriteLine(AddProductRow(insuranceInfo));
			}

			writer.Flush();
			writer.Close();
		}

		protected void AddValue(ref string row, string value)
		{
			row += value + _csvFileDelimiter;
		}

		protected string AddHeaderRow()
		{
			string headerRow = string.Empty;

			foreach (FieldNames type in Enum.GetValues(typeof(FieldNames)))
			{
				AddValue(ref headerRow, GetEnumDescription(type));
			}

			int lastDelimiterPosition = headerRow.LastIndexOf(_csvFileDelimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				headerRow = headerRow.Substring(0, lastDelimiterPosition);
			}

			return headerRow;
		}

		protected string AddProductRow(IInsuranceInfo insuranceInfo)
		{
			string productRow = string.Empty;

			// FirstName
			AddValue(ref productRow, insuranceInfo.FirstName);
			// LastName
			AddValue(ref productRow, insuranceInfo.LastName);
			// CivicNumber
			string civicNumber = insuranceInfo.CivicNumber != null ? insuranceInfo.CivicNumber.Replace("-", "") : insuranceInfo.CivicNumber;
			AddValue(ref productRow, civicNumber);
			// Adress
			AddValue(ref productRow, insuranceInfo.Adress);
			// PostalCode
			AddValue(ref productRow, insuranceInfo.PostalCode);
			// City
			AddValue(ref productRow, insuranceInfo.City);
			// PhoneNumber
			AddValue(ref productRow, insuranceInfo.PhoneNumber);
			// PurchaseDate
			AddValue(ref productRow, insuranceInfo.PurchaseDate.ToString("yyyy-MM-dd"));
			// Säljare
			AddValue(ref productRow, string.Empty);
			// Category
			AddValue(ref productRow, insuranceInfo.ParentCategory);
			// SubCategory
			AddValue(ref productRow, insuranceInfo.Category);
			// SubCategoryId
			AddValue(ref productRow, insuranceInfo.CategoryId);
			// Brand
			AddValue(ref productRow, insuranceInfo.Brand);
			// Modell
			AddValue(ref productRow, insuranceInfo.Modell);
			// Serienummer
			AddValue(ref productRow, string.Empty);
			// PurchaseAmount
			decimal check = Math.Round(insuranceInfo.PurchaseAmount * 100m) % 100m;
			var numberFormatInfo = new NumberFormatInfo {NumberDecimalDigits = check != 0 ? 2 : 0, NumberDecimalSeparator = ","};
			AddValue(ref productRow, insuranceInfo.PurchaseAmount.ToString(numberFormatInfo));
			// Premie
			AddValue(ref productRow, insuranceInfo.Premie.ToString(CultureInfo.InvariantCulture));
			// Försäkringsnummer
			AddValue(ref productRow, string.Empty);
			// Löptid
			AddValue(ref productRow, string.Empty);
			// Försäkringsprodukt
			AddValue(ref productRow, string.Empty);
			// Antal
			AddValue(ref productRow, string.Empty);
			//AddValue(ref productRow, insuranceInfo.Quantity.ToString(CultureInfo.InvariantCulture));
			// Netto premie
			AddValue(ref productRow, string.Empty);
			// Inköpsorder
			AddValue(ref productRow, string.Empty);
			// Försäljningorder
			AddValue(ref productRow, string.Empty);
			// Email
			AddValue(ref productRow, insuranceInfo.Email);

			int lastDelimiterPosition = productRow.LastIndexOf(_csvFileDelimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				productRow = productRow.Substring(0, lastDelimiterPosition);
			}

			return productRow;
		}

		protected virtual string GetEnumDescription(Enum value)
		{
			FieldInfo fieldInfo = value.GetType().GetField(name: value.ToString());
			var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return attributes.Length > 0 ? attributes[0].Description : value.ToString();
		}
	}
}