using System.Collections.ObjectModel;
using Litium.Framework.Messaging;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface ITrustPilotService
	{
		ITrustPilotSetting TrustPilotSetting { get; set; }

		void InscribeMirrorRecipient(IChannel channel, Collection<Recipient> recipients);
	}
}