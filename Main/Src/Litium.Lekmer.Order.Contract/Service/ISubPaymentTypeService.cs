﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Order
{
	public interface ISubPaymentTypeService
	{
		Collection<ISubPaymentType> GetByPayment(int paymentTypeId);
	}
}