﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Order
{
	public interface ISavedCartObjectService
	{
		ISavedCartObject Create(int cartid, int channelId, string email);
		void InsertSavedCart(ISavedCartObject savedCart);
		void UpdateSavedCart(ISavedCartObject savedCart);
		Collection<ISavedCartObject> GetSavedCartForReminder(int savedCartReminderInDays);
	}
}