﻿using System.Collections.ObjectModel;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public interface ILekmerOrderProductCampaignService : IOrderProductCampaignService
	{
		Collection<IOrderCampaign> GetAllByOrderItem(int orderItemId);
	}
}