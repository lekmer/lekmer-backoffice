﻿using System.Collections.ObjectModel;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public interface ILekmerOrderCartCampaignService : IOrderCartCampaignService
	{
		Collection<IOrderCampaign> GetAllByOrder(int orderId);
	}
}
