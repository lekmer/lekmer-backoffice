using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderSharedService
	{
		void Save(IOrderFull order);
	}
}