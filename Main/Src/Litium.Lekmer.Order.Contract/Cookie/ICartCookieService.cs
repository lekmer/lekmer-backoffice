﻿using System;

namespace Litium.Lekmer.Order.Cookie
{
	public interface ICartCookieService
	{
		Guid GetShoppingCardGuid();
		void SetShoppingCardGuid(Guid shoppingCardGuid);
		void DeleteShoppingCardGuid();
	}
}