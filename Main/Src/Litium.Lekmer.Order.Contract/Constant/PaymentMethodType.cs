﻿using System.ComponentModel;

namespace Litium.Lekmer.Order
{
	public enum PaymentMethodType
	{
		[Description("CreditCard")]
		DibsCreditCard = 1,

		KlarnaInvoice = 1000002,
		KlarnaPartPayment = 1000003,
		KlarnaSpecialPartPayment = 1000008,

		KlarnaAdvanced = 1000009,
		KlarnaAdvancedPartPayment = 1000010,
		KlarnaAdvancedSpecialPartPayment = 1000011,

		KlarnaCheckout = 1000006,

		MoneybookersCarteBlue = 1000004,
		MoneybookersIDeal = 1000005,

		Maksuturva = 1000007,

		QliroInvoice = 1000012,
		QliroPartPayment = 1000013,
		QliroSpecialPartPayment = 1000014,
		QliroCompanyInvoice = 1000015,

		CollectorInvoice = 1000016,
		CollectorPartPayment = 1000017,
		CollectorSpecialPartPayment = 1000018,

		[Description("iDeal")]
		DibsiDeal = 1000019
	}
}