using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public static class OrderStatusName
	{
		public static string PaymentPending = "PaymentPending";
		public static string PaymentOnHold = "PaymentOnHold";
		public static string PaymentConfirmed = "PaymentConfirmed";
		public static string Canceled = "Canceled";
		public static string OrderInHy = "OrderInHY";
		public static string OrderError = "OrderError";
		public static string OrderRowItemOverflow = "OrderRowItemOverflow";
		public static string RejectedByPaymentProvider = "RejectedByPaymentProvider";
		public static string KcoDeleted = "KcoDeleted";
		public static string KcoPaymentPending = "KcoPaymentPending";
		public static string PaymentTimeout = "PaymentTimeout";
	}
}