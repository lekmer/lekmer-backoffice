namespace Litium.Lekmer.Order
{
	public interface IOrderItemSize
	{
		int OrderItemId { get; set; }
		int? SizeId { get; set; }
		string ErpId { get; set; }
	}
}