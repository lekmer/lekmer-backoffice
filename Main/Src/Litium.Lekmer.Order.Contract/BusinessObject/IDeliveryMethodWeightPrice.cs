﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public interface IDeliveryMethodWeightPrice : IBusinessObjectBase
	{
		decimal? WeightFrom { get; set; }
		decimal? WeightTo { get; set; }
		decimal FreightCost { get; set; }
	}
}