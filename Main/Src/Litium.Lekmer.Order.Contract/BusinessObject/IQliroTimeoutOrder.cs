﻿using Litium.Lekmer.Payment.Qliro;

namespace Litium.Lekmer.Order
{
	public interface IQliroTimeoutOrder
	{
		int TransactionId { get; set; }
		QliroTransactionStatus? StatusCode { get; set; }
		QliroInvoiceStatus? InvoiceStatus { get; set; }
		int OrderId { get; set; }
		string ReservationNumber { get; set; }
		int ChannelId { get; set; }
	}
}
