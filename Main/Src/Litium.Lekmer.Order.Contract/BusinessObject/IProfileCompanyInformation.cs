﻿namespace Litium.Lekmer.Order
{
	public interface IProfileCompanyInformation
	{
		string Name { get; set; }
		string FullName { get; set; }
		string CellPhoneNumber { get; set; }
		string Email { get; set; }
	}
}