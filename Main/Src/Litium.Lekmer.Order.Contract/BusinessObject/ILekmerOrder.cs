using System;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrder : IOrder
	{
		decimal PaymentCost { get; set; }

		string CustomerIdentificationKey { get; set; }

		Guid? FeedbackToken { get; set; }

		int? AlternateAddressId { get; set; }

		string CivicNumber { get; set; }

		int? OptionalDeliveryMethodId { get; set; }

		decimal? OptionalFreightCost { get; set; }

		int? DiapersDeliveryMethodId { get; set; }

		decimal? DiapersFreightCost { get; set; }

		string KcoId { get; set; }

		string UserAgent { get; set; }
	}
}
