﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	public interface ILekmerCartFull : ICartFull
	{
		Guid VersionToken { get; set; }
		OrderItemsType ItemsType { get; set; }
		ICartVoucherInfo VoucherInfo { get; set; }
		DateTime? UpdatedDate { get; set; }
		string IPAddress { get; set; }

		int GetGroupedCartItemsCount();
		Collection<ILekmerCartItem> GetGroupedCartItemsByProductAndPriceAndSize();

		/// <summary>
		/// Changes the quantity of the product in the cart (adds or subtracts 'quantity' from its current quantity).
		/// If the product does not exist in the cart yet, it will be added.
		/// If the product has new size and cart already has same product with old size,
		/// then cartItem with old size will be delete.
		/// </summary>
		void ChangeItemQuantity(IUserContext userContext, IProduct product, int quantity, int? sizeId, Collection<ICartItemPackageElement> cartItemPackageElements);
		/// <summary>
		/// Changes the quantity of the product in the cart (adds or subtracts 'quantity' from its current quantity).
		/// If the product does not exist in the cart yet, it will be added.
		/// </summary>
		void ChangeItemQuantityForBlockExtendedCartControl(IProduct product, int quantity, int? sizeId, bool isAffectedByCampaign, Collection<ICartItemPackageElement> cartItemPackageElements);

		void AddItem(IProduct product, int quantity, int? sizeId, bool isAffectedByCampaign, int cartItemId, Collection<ICartItemPackageElement> cartItemPackageElements);

		void DeleteItem(int productId, decimal campaignInfoPrice, int quantity, int? sizeId, int? cartItemPackageElements);

		Price GetBaseActualPriceSummary();

		void DeleteItemsAffectedByCampaign();

		Price GetActualPriceSummary(ICurrency currency);

		bool GetTotalFixedDiscount(IUserContext userContext, out decimal totalFixedDiscount);
		bool GetTotalPercentageDiscount(bool isFixedDiscount, decimal totalFixedDiscount, out decimal totalPercentageDiscount);

		ICurrency GetCurrency();

		decimal CalculateCartItemsWeight(IUserContext context);

		Collection<ICartItemOption> GetFreeCartItems(IUserContext context);

		bool IsTotalOverPredefinedValue(decimal actualPrice);

		bool IsItemsLimitExceeded();
	}
}