using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public interface ISavedCartObject : IBusinessObjectBase
	{
		int Id { get; set; }
		int CartId { get; set; }
		Guid CartGuid { get; set; }
		int ChannelId { get; set; }
		string Email { get; set; }
		bool IsReminderSent { get; set; }
		bool IsNeedReminder { get; set; }
	}
}