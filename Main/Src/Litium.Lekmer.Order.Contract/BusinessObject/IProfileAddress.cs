﻿namespace Litium.Lekmer.Order
{
	public interface IProfileAddress
	{
		string Addressee { get; set; }
		string StreetAddress { get; set; }
		string StreetAddress2 { get; set; }
		string PostalCode { get; set; }
		string City { get; set; }
		string PhoneNumber { get; set; }

		string HouseNumber { get; set; }
		string HouseExtension { get; set; }

		string Reference { get; set; }

		string DoorCode { get; set; }
	}
}