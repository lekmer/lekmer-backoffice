﻿namespace Litium.Lekmer.Order
{
	public interface IProfileCustomerInformation
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		string PhoneNumber { get; set; }
		string CellPhoneNumber { get; set; }
		string Email { get; set; }
		int GenderTypeId { get; set; }
	}
}