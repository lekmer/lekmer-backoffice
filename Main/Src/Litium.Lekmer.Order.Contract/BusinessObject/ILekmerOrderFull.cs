using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderFull : ILekmerOrder, IOrderFull
	{
		IOrderVoucherInfo VoucherInfo { get; set; }

		ILekmerOrderAddress AlternateAddress { get; set; }


		decimal? GetActualVoucherDiscount();

		decimal GetPaymentCostVat();

		Price GetBaseActualPriceSummary();

		decimal GetOptionalFreightCost();

		decimal GetOptionalFreightCostVat();

		decimal GetDiapersFreightCost();

		decimal GetDiapersFreightCostVat();

		decimal GetTotalFreightCost();

		decimal GetTotalFreightCostVat();

		/// <summary>
		/// Gets the original price summary before any campaign and voucher discounts.
		/// </summary>
		/// <returns></returns>
		Price GetOriginalPriceSummary();

		/// <summary>
		/// Gets the dicount price summary (campaign and voucher discounts).
		/// </summary>
		/// <returns></returns>
		Price GetDiscountPriceSummary();

		void MergeWith(IDeliveryMethod deliveryMethod, IDeliveryMethod optionalDeliveryMethod, IDeliveryMethod diapersDeliveryMethod);

		void MergeWith(IUserContext context, Collection<IOrderPayment> payments);

		void MergeWith(ICartVoucherInfo cartVoucherInfo);

		void MergeWith(ILekmerOrderAddress orderAlternateAddress);

		void GetTotalDiscount(
			IChannel channel,
			IOrderCampaignInfo orderCampaignInfo,
			out bool isFixedDiscount,
			out decimal totalFixedDiscount,
			out bool isPercentageDiscount,
			out decimal totalPercentageDiscount);

		bool IsProductAbove60LExist(IUserContext context, IEnumerable<IOrderItem> orderItems);

		bool CheckForMixedItems(IUserContext context, IEnumerable<IOrderItem> orderItems);

		bool IsDropShipItemExist(IEnumerable<IOrderItem> orderItems);

		bool CheckForTaxFreeZone();

		/// <summary>
		/// Average VAT % (over order items).
		/// </summary>
		/// <returns>Example: 20.47m</returns>
		decimal GetOrderItemsAverageVatPercentage();
	}
}