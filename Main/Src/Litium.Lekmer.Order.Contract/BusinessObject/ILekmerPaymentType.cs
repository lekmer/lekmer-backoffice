﻿using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerPaymentType : IPaymentType
	{
		bool IsPersonDefault { get; set; }
		bool IsCompanyDefault { get; set; }
		decimal Cost { get; set; }
	}
}
