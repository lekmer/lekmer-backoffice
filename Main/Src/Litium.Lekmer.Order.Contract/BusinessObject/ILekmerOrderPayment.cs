﻿using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderPayment : IOrderPayment
	{
		bool? Captured { get; set; }
		int? KlarnaEid { get; set; }
		int? KlarnaPClass { get; set; }
		string MaksuturvaCode { get; set; }
		string QliroClientRef { get; set; }
		string QliroPaymentCode { get; set; }
		int? CollectorStoreId { get; set; }
		string CollectorPaymentCode { get; set; }
	}
}
