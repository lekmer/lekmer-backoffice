using Litium.Scensum.Core;

namespace Litium.Lekmer.Order.Setting
{
	public interface ITrustPilotSetting
	{
		bool Active(IChannel channel);
		string RecipientName(IChannel channel);
		string RecipientAddress(IChannel channel);
	}
}