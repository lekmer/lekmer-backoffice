﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerDeliveryMethodSecureService : IDeliveryMethodSecureService
	{
		Collection<IDeliveryMethod> GetAll();
	}
}