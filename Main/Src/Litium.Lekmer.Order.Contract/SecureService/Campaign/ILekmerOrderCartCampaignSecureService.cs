﻿using Litium.Scensum.Core;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public interface ILekmerOrderCartCampaignSecureService : IOrderCartCampaignSecureService
	{
		void DeleteByOrder(ISystemUserFull systemUserFull, int orderId);
	}
}