﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface IPackageOrderItemSecureService
	{
		int Save(ISystemUserFull systemUserFull, IPackageOrderItem packageOrderItem);
		void Delete(ISystemUserFull systemUserFull, int orderItemId);
		Collection<IPackageOrderItem> GetAllByOrder(int orderId);
		Collection<IPackageOrderItem> GetAllByOrderItem(int orderItemId);
	}
}