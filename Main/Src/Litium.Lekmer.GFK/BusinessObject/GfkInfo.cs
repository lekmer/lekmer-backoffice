using System;
using Litium.Lekmer.GFK.Contract;

namespace Litium.Lekmer.GFK
{
	[Serializable]
	public class GfkInfo : IGfkInfo
	{
		public string Period { get; set; }
		public string GroupNo { get; set; }
		public string GroupText { get; set; }
		public string ArticleText { get; set; }
		public string Brand { get; set; }
		public string ArticleNo { get; set; }
		public string ArticleNoUniversal { get; set; }
		public string ShopNo { get; set; }
		public int SalesUnits { get; set; }
		public decimal SalesValue { get; set; }
		public int StockUnits { get; set; }
	}
}