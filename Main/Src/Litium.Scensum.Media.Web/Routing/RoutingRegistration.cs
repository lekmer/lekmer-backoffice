using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Media.Web
{
	public static class RoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			routes.Add(
				new ImageRoute(
					@"{id}/{m}/{sizecommonname}/{whatever}.{extension}", null,
					new RouteValueDictionary { { "id", @"\d+" }, { "m", @"^m$" } },
					new RouteHandler("~/Image.ashx")));
			routes.Add(
				new ImageRoute(
					@"{id}/{sizecommonname}/{whatever}.{extension}", null,
					new RouteValueDictionary { { "id", @"\d+" } },
					new RouteHandler("~/Image.ashx")));
			routes.Add(
				new MediaRoute(
					@"{id}/{whatever}.{extension}", null,
					new RouteValueDictionary { { "id", @"\d+" } },
					new RouteHandler("~/MediaItem.ashx")));

			// Workaround to ignore strange url's endings like /mediaarchive/1053451/p8097122.jpg/MGuOeyFFienBOIAN.gxnSFoZQJq6NzoEuVbdHXBhmPs-
			routes.Add(
				new ImageRoute(
					@"{id}/{m}/{sizecommonname}/{whatever}.{extension}/{*hash}", null,
					new RouteValueDictionary { { "id", @"\d+" }, { "m", @"^m$" } },
					new RouteHandler("~/Image.ashx")));
			routes.Add(
				new ImageRoute(
					@"{id}/{sizecommonname}/{whatever}.{extension}/{*hash}", null,
					new RouteValueDictionary { { "id", @"\d+" } },
					new RouteHandler("~/Image.ashx")));
			routes.Add(
				new MediaRoute(
					@"{id}/{whatever}.{extension}/{*hash}", null,
					new RouteValueDictionary { { "id", @"\d+" } },
					new RouteHandler("~/MediaItem.ashx")));
		}
	}
}