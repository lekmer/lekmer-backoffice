using System;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;

namespace Litium.Scensum.Media.Web
{
	public class RouteHandler : IRouteHandler
	{
		private static readonly Type _pageType = typeof (IHttpHandler);

		public RouteHandler(string virtualPath)
		{
			VirtualPath = virtualPath;
		}

		public string VirtualPath { get; private set; }

		public IHttpHandler GetHttpHandler(RequestContext requestContext)
		{
			return BuildManager.CreateInstanceFromVirtualPath(VirtualPath, _pageType) as IHttpHandler;
		}
	}
}