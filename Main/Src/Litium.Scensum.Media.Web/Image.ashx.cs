using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Lekmer.Media;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Scensum.Media.Web
{
	[DebuggerStepThrough]
	public class Image : IHttpHandler
	{
		private static int _bufferSizeBytes = 8 * 1024;
		private static string _lastModified = "Sat, 01 Jan 2000 00:00:00 GMT";

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var mediaId = (int) context.Items["MediaId"];
			var extension = (string) context.Items["Extension"];
			var imageSizeCommonName = (string) context.Items["ImageSizeCommonName"];
			var mobileVersion = (bool) context.Items["MobileVersion"];

			LoadImage(context, mediaId, extension, imageSizeCommonName, mobileVersion);
		}

		private static void LoadImage(HttpContext context, int mediaId, string extension, string imageSizeCommonName, bool mobileVersion)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (string.IsNullOrEmpty(extension)) throw new ArgumentNullException("extension");
			if (string.IsNullOrEmpty(imageSizeCommonName)) throw new ArgumentNullException("imageSizeCommonName");

			string etag = string.Concat("\"", mediaId.ToString(CultureInfo.InvariantCulture), "-", imageSizeCommonName, "\"");

			if (HasMatchingETag(context, etag))
			{
				SendNotModifiedHeader(context);
				return;
			}

			if (HasMatchingLastModified(context))
			{
				SendNotModifiedHeader(context);
				return;
			}

			var imageService = (ILekmerImageService)IoC.Resolve<IImageService>();

			string mime;
			using (Stream imageStream = imageService.LoadImage(mediaId, extension, imageSizeCommonName, mobileVersion, out mime))
			{
				if (imageStream == null)
				{
					throw new HttpException(404, "Page not found");
				}

				context.Response.AppendHeader("ETag", etag);
				context.Response.AppendHeader("Last-Modified", _lastModified);
				context.Response.ContentType = mime;

				StreamAppender.AppendToStream(imageStream, context.Response.OutputStream, _bufferSizeBytes);
			}
		}

		private static bool HasMatchingETag(HttpContext context, string etag)
		{
			string ifNoneMatchHeader = context.Request.Headers["If-None-Match"];
			return ifNoneMatchHeader != null && ifNoneMatchHeader.Equals(etag);
		}

		private static bool HasMatchingLastModified(HttpContext context)
		{
			string lastModifiedHeader = context.Request.Headers["If-Modified-Since"];
			return lastModifiedHeader != null && lastModifiedHeader.Equals(_lastModified);
		}

		private static void SendNotModifiedHeader(HttpContext context)
		{
			context.Response.StatusCode = 304;
			context.Response.StatusDescription = "Not modified";
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}