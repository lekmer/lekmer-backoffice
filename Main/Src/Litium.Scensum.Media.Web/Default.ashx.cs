using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core.Web;

namespace Litium.Scensum.Media.Web
{
	[SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Default")]
	public class Default : HandlerBase
	{
		protected override void ProcessRequest()
		{
			Response.Write("Default.ashx");
		}
	}
}