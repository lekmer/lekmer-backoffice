﻿using Litium.Scensum.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Litium.Lekmer.Order.MonitorWeb.Models
{

    public class OrderStatisticItem
    {
        public string Country { get; set; }
        public int OrdersToday { get; set; }
        public int OrdersYesterday { get; set; }
        public decimal OrdersValueToday { get; set; }
        public decimal OrdersValueYesterday { get; set; }
        public int OrdersHourToday{ get; set; }
        public int OrdersHourYesterday{ get; set; }

    }
}