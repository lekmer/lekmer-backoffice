﻿using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order.MonitorWeb.Models;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using log4net;

namespace Litium.Lekmer.Order.MonitorWeb.Controllers
{
    public class OrderStatisticController : ApiController
    {
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public IEnumerable<OrderStatisticItem> Get([FromUri] string start, [FromUri] string end)
        {
	        IEnumerable<OrderStatisticItem> result = null;
	        try
	        {
				var repository = new OrderMonitorRepository();

				DateTime currentDate;

				if (!DateTime.TryParseExact(end, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out currentDate))
				{
					currentDate = DateTime.Now;
				}

				DateTime yesterdayDate;

				if (!DateTime.TryParseExact(start, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out yesterdayDate))
				{
					yesterdayDate = currentDate.AddDays(-1).Date + new TimeSpan(0, 0, 0);
				}

				var collection = repository.GetOrderStatistics(yesterdayDate, currentDate);
				var itemsList = new SortedList<string, OrderStatisticItem>(collection.Count);

				if (collection.Any())
				{

					var statisticsToday = collection.Where(s => s.Date.Year == currentDate.Year && s.Date.Month == currentDate.Month && s.Date.Day == currentDate.Day);
					var statisticsYesterday = collection.Where(s => s.Date.Year == yesterdayDate.Year && s.Date.Month == yesterdayDate.Month && s.Date.Day == yesterdayDate.Day);


					foreach (var statistic in statisticsToday)
					{

						if ((!itemsList.ContainsKey(statistic.Channel)) && (!string.IsNullOrEmpty(statistic.Channel)))
						{
							itemsList.Add(statistic.Channel, new OrderStatisticItem() { OrdersToday = statistic.NumberOfOrders, OrdersValueToday = statistic.TotalOrderValue, Country = statistic.Channel });
						}
						else if (itemsList.ContainsKey(statistic.Channel))
						{
							itemsList[statistic.Channel].OrdersToday += statistic.NumberOfOrders;
							itemsList[statistic.Channel].OrdersValueToday += statistic.TotalOrderValue;
						}

					}

					foreach (var statistic in statisticsYesterday)
					{

						if ((!itemsList.ContainsKey(statistic.Channel)) && (!string.IsNullOrEmpty(statistic.Channel)))
						{
							itemsList.Add(statistic.Channel, new OrderStatisticItem() { OrdersYesterday = statistic.NumberOfOrders, OrdersValueYesterday = statistic.TotalOrderValue, Country = statistic.Channel });
						}
						else if (itemsList.ContainsKey(statistic.Channel))
						{
							itemsList[statistic.Channel].OrdersYesterday += statistic.NumberOfOrders;
							itemsList[statistic.Channel].OrdersValueYesterday += statistic.TotalOrderValue;
						}
					}

				}
				result = itemsList.Values.ToArray();
	        }
	        catch (Exception exp)
	        {		        
		        _log.Error(exp);
	        }
	        return result;

        }


    }
}