﻿using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order.MonitorWeb.BusinessObject;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using log4net;
using Litium.Lekmer.Product;
using OrderItem = Litium.Lekmer.Order.MonitorWeb.BusinessObject.OrderItem;

namespace Litium.Lekmer.Order.MonitorWeb.Controllers
{
	public class OrderItemsController : ApiController
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		public IEnumerable<OrderItems> Get([FromUri] string daytime, [FromUri]int? minutesAfterPurchase)
		{
			IEnumerable<OrderItems>collection = null;
			try
			{
				DateTime date;
				
				if (!DateTime.TryParseExact(daytime, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
				{
					date = DateTime.Now;
				}

				if (!minutesAfterPurchase.HasValue)
				{
					minutesAfterPurchase = Setting.MinutesAfterPurchase;
				}
				collection = GetOrders(date, minutesAfterPurchase.Value);
			}
			catch (Exception exp)
			{

				_log.Error(exp);
			}
			
		 return collection;
	


		}

		[HttpGet]
		public IEnumerable<OrderItems> GetItemsWithImages([FromUri] string daytime, [FromUri]int? minutesAfterPurchase)
		{

			IEnumerable<OrderItems> collection = null;
			try
			{

				DateTime date;

				if (!DateTime.TryParseExact(daytime, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
				{
					date = DateTime.Now;
				}

				if (!minutesAfterPurchase.HasValue)
				{
					minutesAfterPurchase = Setting.MinutesAfterPurchase;
				}
				collection = GetOrdersWithImages(date, minutesAfterPurchase.Value);
			}
			catch (Exception exp)
			{
				_log.Error(exp);
			}

			return collection;

		}


		private Collection<OrderItems> GetOrders(DateTime date, int minutesAfterPurchase)
		{
			var repository = new OrderMonitorRepository();

			return repository.GetOrders(date, minutesAfterPurchase, false);
		}


		private Collection<OrderItems> GetOrdersWithImages(DateTime date, int minutesAfterPurchase)
		{
			var repository = new OrderMonitorRepository();

			var collection = repository.GetOrders(date, minutesAfterPurchase, false);
			var orderItemService = IoC.Resolve<IOrderItemService>();
			foreach (var order in collection)
			{

				var channel = new LekmerChannel() { Id = order.ChannelId, ApplicationName = order.ApplicationName };
				
				ISubDomain mediaSubDomain = RetrieveMediaArchiveExternalSubDomain(channel);

				var mediaUrl = mediaSubDomain != null
					? LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain)
					: LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);

				// only absolute url
				if (LekmerUrlHelper.HasUriScheme(mediaUrl) == false)
				{
					mediaUrl = "http://" + channel.ApplicationName + "/" + mediaUrl.Trim('/');
				}
				UserContext.Channel = Channels.SingleOrDefault(i => i.Id == order.ChannelId);
				var orderItems = orderItemService.GetAllByOrder(UserContext, order.OrderId);
				if (orderItems.Any())
				{
					order.Items = new List<OrderItem>(orderItems.Count);

					foreach (var orderItem in orderItems)
					{

						var product = (ILekmerProduct) ProductService.GetByIdWithoutStatusFilter(UserContext, orderItem.ProductId);
						if (product != null)
						{
							var imageUrl = string.Empty;
							if (product.Image != null)
							{
								var image = product.Image;

								imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image);


							}
							order.Items.Add(new OrderItem()
							{
								Id = orderItem.Id,
								Name = orderItem.Title,
								Price = orderItem.OriginalPrice.IncludingVat,
								Quantity = orderItem.Quantity,
								ProductUrl = string.Format("http:/{0}/{1}", order.ApplicationName, product.LekmerUrl),
								ImageUrl = imageUrl
							});
						}
					}
				}
				

			}

			return collection;
		}


		private ISubDomain RetrieveMediaArchiveExternalSubDomain(IChannel channel)
		{
			var mediaSubDomains = IoC.Resolve<ISubDomainService>().GetAllByContentType(channel, (int)ContentType.MediaArchiveExternal);

			return mediaSubDomains.FirstOrDefault();
		}
		private static OrderMonitorSetting _setting;
		private static OrderMonitorSetting Setting
		{
			get { return _setting ?? (_setting = new OrderMonitorSetting()); }
		}

		private static Collection<IChannel> _channels;
		private static Collection<IChannel> Channels
		{
			get
			{
				if (_channels == null)
				{
					var channelService = IoC.Resolve<IChannelSecureService>();
					_channels = channelService.GetAll();
				}

				return _channels;
			}
		}

		private static IUserContext _userContext;
		private static IUserContext UserContext
		{
			get
			{
				if (_userContext == null)
				{
					_userContext = IoC.Resolve<IUserContext>();
					_userContext.Channel = IoC.Resolve<IChannel>();
					_userContext.Channel.Id = 1;
					_userContext.Channel.Language = IoC.Resolve<ILanguage>();
					_userContext.Channel.Language.Id = 1;
				}

				return _userContext;
			}
		}


		private static ILekmerProductService _productService;
		private static ILekmerProductService ProductService
		{
			get { return _productService ?? (_productService = IoC.Resolve<ILekmerProductService>()); }
		}
	}
}