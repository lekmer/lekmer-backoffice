﻿using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListBrand : IBusinessObjectBase
	{
		int BlockId { get; set; }
		int Position { get; set; }
		IBrand Brand { get; set; }
	}
}