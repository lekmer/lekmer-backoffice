﻿using System.Collections.ObjectModel;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopList : IBlock
	{
		int OrderStatisticsDayCount { get; set; }
		bool IncludeAllCategories { get; set; }
		int? LinkContentNodeId { get; set; }
		IBlockSetting Setting { get; set; }

		Collection<IBlockBrandTopListBrand> BrandTopListBrands { get; set; }
		Collection<IBlockBrandTopListCategory> BlockBrandTopListCategory { get; set; }
	}
}