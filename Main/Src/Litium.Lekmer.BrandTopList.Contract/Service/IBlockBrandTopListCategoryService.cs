using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListCategoryService
	{
		Collection<IBlockBrandTopListCategory> GetAllByBlock(IUserContext context, IBlockBrandTopList block);
		
		Collection<ICategoryView> GetViewAllByBlockWithChildren(IUserContext context, IBlockBrandTopList block);
		Collection<ICategoryView> GetViewAllByBlockWithoutChildren(IUserContext context, IBlockBrandTopList block);
	}
}