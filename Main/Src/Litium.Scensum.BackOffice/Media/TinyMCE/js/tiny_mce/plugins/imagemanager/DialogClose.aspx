﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DialogClose.aspx.cs" Inherits="Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager.DialogClose" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add image</title>
<link href="~/Media/Css/content.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../tiny_mce_popup.js"></script>

    <script type="text/javascript" src="js/dialog.js"></script>

    <script type="text/javascript" src="../../tiny_mce_popup.js"></script>

    <script type="text/javascript" src="../../utils/mctabs.js"></script>

    <script type="text/javascript" src="../../utils/form_utils.js"></script>

    <script type="text/javascript" src="../../utils/validate.js"></script>

    <script type="text/javascript" src="../../utils/editable_selects.js"></script>
    
    <script src="<%=ResolveUrl("~/Media/Scripts/jquery-1.2.6.js") %>" type="text/javascript"></script>


    <script src="../advimage/langs/en_dlg.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/image.js"></script>
            <script type="text/javascript">
                function SetImageDimensions(source) {
                    $("input#imageSize").val(source);
                }
            </script>
    
    <link href="~/Media/Css/Assortment.css" rel="stylesheet" type="text/css" />
    <link href="css/imagemanager.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">

            <div class="tabs">
                <ul>
                    <li id="general_tab" class="current"><span><a href="javascript:mcTabs.displayTab('general_tab','general_panel');"
                        onmousedown="return false;">Settings</a></span></li>
                    <%--<li id="appearance_tab"><span><a href="javascript:mcTabs.displayTab('appearance_tab','appearance_panel');" onmousedown="return false;">Appearance</a></span></li>
				<li id="advanced_tab"><span><a href="javascript:mcTabs.displayTab('advanced_tab','advanced_panel');" onmousedown="return false;">Advanced</a></span></li>--%>
                </ul>
            </div>

            <div class="panel_wrapper">
                <div id="general_panel" class="panel current">
                    <div style="height: 260px;">
                        <div style="float: left">
                        <br />
                            <label id="altlabel" for="alt">
                                Alternative text</label><br />
                            <input id="alt" name="alt" type="text" value="" runat="server" style="width: 300px;" />
                            <br /><br />
                            <label id="sizeLabel" for="imgSize">
                                Image size</label><br />
                                <select runat="server" id="imgSize" name="imgSize" onchange="SetImageDimensions(this.value); ImageDialog.changeSize();">
                                </select>
                        </div>
                        <div style="float: right; padding: 10px 10px;">
                            <div id="prev"></div>
                        </div>
                    </div>
                    <br />
                    <div>
                        <div style="width: 108px;" class="right">
                            <div class="button-container" onclick="ImageDialog.insert();return false;" style="cursor: pointer">
                                <span class="button-left-border"></span><span class="button-middle">OK</span> <span
                                    class="button-right-border"></span>
                            </div>
                            <div class="button-container" onclick="tinyMCEPopup.close();" style="cursor: pointer">
                                <span class="button-left-border"></span><span class="button-middle">Cancel</span>
                                <span class="button-right-border"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="appearance_panel" class="panel">
                    <fieldset>
                        <legend>Appearance</legend>
                        <label id="titlelabel" for="title">
                            Title</label>
                        <input id="title" name="title" type="text" value="" runat="server" />
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                </td>
                                <td id="srcbrowsercontainer">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <input name="src" type="text" id="src" value="" class="mceFocus" onchange="ImageDialog.showPreviewImage(this.value);"
                            runat="server" />
                        <table border="0" cellpadding="4" cellspacing="0">
                            <tr>
                                <td class="column1">
                                    <label id="alignlabel" for="align">
                                        Alignment</label>
                                </td>
                                <td>
                                    <select id="align" name="align" onchange="ImageDialog.updateStyle('align');ImageDialog.changeAppearance();">
                                        <option value="">not set</option>
                                        <option value="baseline">Baseline</option>
                                        <option value="top">Top</option>
                                        <option value="middle">Middle</option>
                                        <option value="bottom">Align bottom</option>
                                        <option value="text-top">Text top</option>
                                        <option value="text-bottom">Text bottom</option>
                                        <option value="left">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </td>
                                <td rowspan="6" valign="top">
                                    <div class="alignPreview">
                                        <img id="alignSampleImg" src="img/sample.gif" alt="{#advimage_dlg.example_img}" />
                                        Lorem ipsum, Dolor sit amet, consectetuer adipiscing loreum ipsum edipiscing elit,
                                        sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Loreum
                                        ipsum edipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                        magna aliquam erat volutpat.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="widthlabel" for="width">
                                        Dimensions</label>
                                </td>
                                <td nowrap="nowrap">
                                <input name="imageSize" type="text" id="imageSize" value="" size="50" maxlength="50" class="size"
                                        onchange="ImageDialog.changeSize();" />
                                    <input name="width" type="text" id="width" value="" size="5" maxlength="5" class="size"
                                        onchange="ImageDialog.changeHeight();" />
                                    x
                                    <input name="height" type="text" id="height" value="" size="5" maxlength="5" class="size"
                                        onchange="ImageDialog.changeWidth();" />
                                    px
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input id="constrain" type="checkbox" name="constrain" class="checkbox" />
                                            </td>
                                            <td>
                                                <label id="constrainlabel" for="constrain">
                                                    Constrain proportions</label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="vspacelabel" for="vspace">
                                        Vertical space</label>
                                </td>
                                <td>
                                    <input name="vspace" type="text" id="vspace" value="" size="3" maxlength="3" class="number"
                                        onchange="ImageDialog.updateStyle('vspace');ImageDialog.changeAppearance();"
                                        onblur="ImageDialog.updateStyle('vspace');ImageDialog.changeAppearance();" />
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="hspacelabel" for="hspace">
                                        Horizontal space</label>
                                </td>
                                <td>
                                    <input name="hspace" type="text" id="hspace" value="" size="3" maxlength="3" class="number"
                                        onchange="ImageDialog.updateStyle('hspace');ImageDialog.changeAppearance();"
                                        onblur="ImageDialog.updateStyle('hspace');ImageDialog.changeAppearance();" />
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="borderlabel" for="border">
                                        Border</label>
                                </td>
                                <td>
                                    <input id="border" name="border" type="text" value="" size="3" maxlength="3" class="number"
                                        onchange="ImageDialog.updateStyle('border');ImageDialog.changeAppearance();"
                                        onblur="ImageDialog.updateStyle('border');ImageDialog.changeAppearance();" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="class_list">
                                        class name</label>
                                </td>
                                <td colspan="2">
                                    <select id="class_list" name="class_list" class="mceEditableSelect">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="stylelabel" for="style">
                                        Style</label>
                                </td>
                                <td colspan="2">
                                    <input id="style" name="style" type="text" value="" onchange="ImageDialog.changeAppearance();" />
                                </td>
                            </tr>
                            <!-- <tr>
							<td class="column1"><label id="classeslabel" for="classes">{#advimage_dlg.classes}</label></td> 
							<td colspan="2"><input id="classes" name="classes" type="text" value="" onchange="selectByValue(this.form,'classlist',this.value,true);" /></td> 
						</tr> -->
                        </table>
                    </fieldset>
                </div>
                <div id="advanced_panel" class="panel">
                    <fieldset>
                        <legend>swap image</legend>
                        <input type="checkbox" id="onmousemovecheck" name="onmousemovecheck" class="checkbox"
                            onclick="ImageDialog.setSwapImage(this.checked);" />
                        <label id="onmousemovechecklabel" for="onmousemovecheck">
                            Alt image</label>
                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                            <tr>
                                <td class="column1">
                                    <label id="onmouseoversrclabel" for="onmouseoversrc">
                                        for mouse over</label>
                                </td>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <input id="onmouseoversrc" name="onmouseoversrc" type="text" value="" />
                                            </td>
                                            <td id="onmouseoversrccontainer">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="over_list">
                                        Image_list</label>
                                </td>
                                <td>
                                    <select id="over_list" name="over_list" onchange="document.getElementById('onmouseoversrc').value=this.options[this.selectedIndex].value;">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="onmouseoutsrclabel" for="onmouseoutsrc">
                                        for mouse out</label>
                                </td>
                                <td class="column2">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <input id="onmouseoutsrc" name="onmouseoutsrc" type="text" value="" />
                                            </td>
                                            <td id="onmouseoutsrccontainer">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="out_list">
                                        Image_list</label>
                                </td>
                                <td>
                                    <select id="out_list" name="out_list" onchange="document.getElementById('onmouseoutsrc').value=this.options[this.selectedIndex].value;">
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset>
                        <legend>{#advimage_dlg.misc}</legend>
                        <table border="0" cellpadding="4" cellspacing="0">
                            <tr>
                                <td class="column1">
                                    <label id="idlabel" for="id">
                                        Id</label>
                                </td>
                                <td>
                                    <input id="id" name="id" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="dirlabel" for="dir">
                                        Language direction</label>
                                </td>
                                <td>
                                    <select id="dir" name="dir" onchange="ImageDialog.changeAppearance();">
                                        <option value="">not set</option>
                                        <option value="ltr">Left to right</option>
                                        <option value="rtl">Right to left</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="langlabel" for="lang">
                                        Language code</label>
                                </td>
                                <td>
                                    <input id="lang" name="lang" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="usemaplabel" for="usemap">
                                        Image Map</label>
                                </td>
                                <td>
                                    <input id="usemap" name="usemap" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td class="column1">
                                    <label id="longdesclabel" for="longdesc">
                                        Long description link</label>
                                </td>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <input id="longdesc" name="longdesc" type="text" value="" />
                                            </td>
                                            <td id="longdesccontainer">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>

            </div>

    </div>
    </form>
</body>
</html>
