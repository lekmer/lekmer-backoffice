﻿
function ShowImage(imageContainerId) {
    HideImages(['image-details-large', 'image-details-medium', 'image-details-thumbnail']);

    document.getElementById(imageContainerId).style.display = 'block';
}

function HideImages(imageIdsArray) {
    for (var i = 0; i < imageIdsArray.length; i++) {
        var imageStyle = document.getElementById(imageIdsArray[i]).style;
        imageStyle.display = 'none';
        imageStyle.height = 0;
    }
}

function UncheckCheckboxes() {

    $('div.image-catalog-container').find('input').attr('checked', '');
}

function ShowMissingImageWarning() {
    
    var inputs = $('div.image-catalog-delete').find('input');
    var hasimage = false;
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].checked == true) {
            hasimage = true;
        }
    }
    if (!hasimage)
    {
        document.getElementById('divMessages').style.display = 'block';
        
        document.getElementById('poput-images-content').style.height = '700px';
    }
    return hasimage;
}

function HideMissingImageWarning() {
    document.getElementById('divMessages').style.display = 'none';
    document.getElementById('poput-images-content').style.height = '600px';
}