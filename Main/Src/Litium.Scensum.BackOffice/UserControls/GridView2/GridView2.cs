﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Litium.Scensum.BackOffice.UserControls.GridView2
{
    public class GridViewWithCustomPager : System.Web.UI.WebControls.GridView
    {
        protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
        {
            if (!UseCustomPagerBehaviour)
            {
                base.InitializePager(row, columnSpan, pagedDataSource);
            }
            else
            {
                TableCell cell = new TableCell();
                if (columnSpan > 1)
                {
                    cell.ColumnSpan = columnSpan;
                }

                HtmlGenericControl divPaging = new HtmlGenericControl("div");
                divPaging.Attributes.Add("class", "paging");
                

                Table child = new Table();
                child.Attributes.Add("cellpadding", "0");
                child.Attributes.Add("cellspacing", "0");
                TableRow row2 = new TableRow();

                CreateCustomPager(row2, pagedDataSource);

                divPaging.Controls.Add(child);
                cell.Controls.Add(divPaging);
                child.Rows.Add(row2);
                row.Cells.Add(cell);
            }
        }

        private void CreateCustomPager(TableRow row, PagedDataSource pagedDataSource)
        {
            int startDisplay, endDisplay;


            int pageCount = pagedDataSource.PageCount;
            int maxDisplayCount = PagerSettings.PageButtonCount;

            int displayCount = Math.Min(maxDisplayCount, pageCount);

            int maxDisplayBefore = (int)Math.Ceiling(displayCount / 2M) - 1;
            int maxDisplayAfter = (displayCount - (maxDisplayBefore + 1));

            int selectedPage = pagedDataSource.CurrentPageIndex + 1;

            if (pageCount <= maxDisplayCount)
            {
                startDisplay = 1;
                endDisplay = pageCount;
            }
            else if (selectedPage > displayCount)
            {
                endDisplay = Math.Min(selectedPage + maxDisplayAfter, pageCount);
                startDisplay = Math.Max(endDisplay - displayCount + 1, 1);
            }
            else
            {
                startDisplay = Math.Max(selectedPage - maxDisplayBefore, 1);
                endDisplay = Math.Min(startDisplay + displayCount - 1, pageCount);
            }

            CreateFirstPreviousButtons(row, selectedPage);

            for (int i = startDisplay; i <= endDisplay; i++)
            {
                TableCell cell = new TableCell();
                row.Cells.Add(cell);

                string number = i.ToString(NumberFormatInfo.InvariantInfo);

                if (i == selectedPage)
                {
                    cell.Controls.Add(new Label { Text = number });
                }
                else
                {
                    LinkButton pageLink = new LinkButton {Text = number, CommandName = "Page", CommandArgument = number};
                    pageLink.Controls.Add(new HtmlGenericControl("span") { InnerText = number });
                    cell.Controls.Add(pageLink);
                }
            }


            CreateLastNextButtons(row, selectedPage, pageCount);

        }

        private void CreateFirstPreviousButtons(TableRow row, int currentPageNumber)
        {
            //----------------First------------------------
            TableCell cell = new TableCell();
            row.Cells.Add(cell);

            ImageButton controlImageButton = new ImageButton();
            if (currentPageNumber == 1)
            {
                controlImageButton.ImageUrl = CustomPagerSettings.InactiveFirstPageImageUrl;
                controlImageButton.Enabled = false;
                controlImageButton.CssClass = "inactive";
            }
            else
            {
                controlImageButton.ImageUrl = PagerSettings.FirstPageImageUrl;
            }
            controlImageButton.AlternateText = HttpUtility.HtmlDecode(PagerSettings.FirstPageText);
            controlImageButton.CommandName = "Page";
            controlImageButton.CommandArgument = "First";
            cell.Controls.Add(controlImageButton);

            //----------------Previous------------------------
            TableCell cell2 = new TableCell();
            row.Cells.Add(cell2);

            ImageButton buttonPrev = new ImageButton();

            if (currentPageNumber == 1)
            {
                buttonPrev.ImageUrl = CustomPagerSettings.InactivePreviousPageImageUrl;
                buttonPrev.Enabled = false;
                buttonPrev.CssClass = "inactive";
            }
            else
            {
                buttonPrev.ImageUrl = PagerSettings.PreviousPageImageUrl;
            }
            buttonPrev.CommandName = "Page";
            buttonPrev.CommandArgument = (currentPageNumber - 1).ToString(NumberFormatInfo.InvariantInfo);
            cell2.Controls.Add(buttonPrev);
        }

        private void CreateLastNextButtons(TableRow row, int currentPageNumber, int pageCount)
        {
            //----------------Next------------------------
            TableCell cell4 = new TableCell();
            row.Cells.Add(cell4);
            ImageButton buttonNext = new ImageButton();

            if (currentPageNumber == pageCount)
            {
                buttonNext.ImageUrl = CustomPagerSettings.InactiveNextPageImageUrl;
                buttonNext.Enabled = false;
                buttonNext.CssClass = "inactive";
            }
            else
            {
                buttonNext.ImageUrl = PagerSettings.NextPageImageUrl;
            }
            buttonNext.CommandName = "Page";
            buttonNext.CommandArgument = (currentPageNumber + 1).ToString(NumberFormatInfo.InvariantInfo);
            cell4.Controls.Add(buttonNext);

            //----------------Last------------------------
            TableCell cell5 = new TableCell();
            row.Cells.Add(cell5);

            ImageButton buttonLast = new ImageButton();
            if (currentPageNumber == pageCount)
            {
                buttonLast.ImageUrl = CustomPagerSettings.InactiveLastPageImageUrl;
                buttonLast.Enabled = false;
                buttonLast.CssClass = "inactive";
            }
            else
            {
                buttonLast.ImageUrl = PagerSettings.LastPageImageUrl;
            }
            buttonLast.AlternateText = HttpUtility.HtmlDecode(PagerSettings.LastPageText);
            buttonLast.CommandName = "Page";
            buttonLast.CommandArgument = "Last";
            cell5.Controls.Add(buttonLast);
        }

        [Description("GridView_UseCustomPagerBehaviour"), Category("Behavior"), DefaultValue(false)]
        public virtual bool UseCustomPagerBehaviour
        {
            get
            {
                object obj2 = ViewState["UseCustomPagerBehaviour"];
                return ((obj2 != null) && ((bool)obj2));
            }
            set
            {
                bool useCustomPagerBehaviour = UseCustomPagerBehaviour;
                if (value != useCustomPagerBehaviour)
                {
                    ViewState["UseCustomPagerBehaviour"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        private CustomPagerSettings _customPagerSettings;

        [Category("CustomPaging"), PersistenceMode(PersistenceMode.InnerProperty), Description("GridView_CustomPagerSettings"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), NotifyParentProperty(true)]
        public virtual CustomPagerSettings CustomPagerSettings
        {
            get
            {
                if (_customPagerSettings == null)
                {
                    _customPagerSettings = new CustomPagerSettings();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_customPagerSettings).TrackViewState();
                    }
                    _customPagerSettings.PropertyChanged += OnCustomPagerPropertyChanged;
                }
                return _customPagerSettings;
            }
        }

        private void OnCustomPagerPropertyChanged(object sender, EventArgs e)
        {
            if (Initialized)
            {
                RequiresDataBinding = true;
            }
        }
    }

    [TypeConverter(typeof(ExpandableObjectConverter)), AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public sealed class CustomPagerSettings : IStateManager
    {
        // Fields
        private bool _isTracking;
        private readonly StateBag _viewState = new StateBag();

        // Events
        [Browsable(false)]
        public event EventHandler PropertyChanged;

        // Methods
        private void OnPropertyChanged()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, EventArgs.Empty);
            }
        }

        void IStateManager.LoadViewState(object state)
        {
            if (state != null)
            {
                ((IStateManager)ViewState).LoadViewState(state);
            }
        }

        object IStateManager.SaveViewState()
        {
            return ((IStateManager)ViewState).SaveViewState();
        }

        void IStateManager.TrackViewState()
        {
            _isTracking = true;
            //this.ViewState.TrackViewState();
        }


        // Properties
        [Description("CustomPagerSettings_InactiveFirstPageImageUrl"), Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), UrlProperty, Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
        public string InactiveFirstPageImageUrl
        {
            get
            {
                object obj2 = ViewState["InactiveFirstPageImageUrl"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                if (InactiveFirstPageImageUrl != value)
                {
                    ViewState["InactiveFirstPageImageUrl"] = value;
                    OnPropertyChanged();
                }
            }
        }

        [Description("CustomPagerSettings_InactiveLastPageImageUrl"), Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), UrlProperty, Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
        public string InactiveLastPageImageUrl
        {
            get
            {
                object obj2 = ViewState["InactiveLastPageImageUrl"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                if (InactiveLastPageImageUrl != value)
                {
                    ViewState["InactiveLastPageImageUrl"] = value;
                    OnPropertyChanged();
                }
            }
        }

        [Description("CustomPagerSettings_InactiveNextPageImageUrl"), Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), UrlProperty, Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
        public string InactiveNextPageImageUrl
        {
            get
            {
                object obj2 = ViewState["InactiveNextPageImageUrl"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                if (InactiveNextPageImageUrl != value)
                {
                    ViewState["InactiveNextPageImageUrl"] = value;
                    OnPropertyChanged();
                }
            }
        }

        [Description("CustomPagerSettings_InactivePreviousPageImageUrl"), Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), UrlProperty, Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
        public string InactivePreviousPageImageUrl
        {
            get
            {
                object obj2 = ViewState["InactivePreviousPageImageUrl"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                if (InactivePreviousPageImageUrl != value)
                {
                    ViewState["InactivePreviousPageImageUrl"] = value;
                    OnPropertyChanged();
                }
            }
        }


        [UrlProperty, DefaultValue(""), NotifyParentProperty(true), Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), Category("Appearance"), Description("PagerSettings_PreviousPageImageUrl")]
        public string PreviousPageImageUrl
        {
            get
            {
                object obj2 = ViewState["PreviousPageImageUrl"];
                if (obj2 != null)
                {
                    return (string)obj2;
                }
                return string.Empty;
            }
            set
            {
                if (PreviousPageImageUrl != value)
                {
                    ViewState["PreviousPageImageUrl"] = value;
                    OnPropertyChanged();
                }
            }
        }


        bool IStateManager.IsTrackingViewState
        {
            get
            {
                return _isTracking;
            }
        }

        private StateBag ViewState
        {
            get
            {
                return _viewState;
            }
        }

        [NotifyParentProperty(true), Description("PagerStyle_Visible"), DefaultValue(true), Category("Appearance")]
        public bool Visible
        {
            get
            {
                object obj2 = ViewState["PagerVisible"];
                if (obj2 != null)
                {
                    return (bool)obj2;
                }
                return true;
            }
            set
            {
                ViewState["PagerVisible"] = value;
            }
        }
    }
}
