using System;
using System.Globalization;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	public class ControlBase : System.Web.UI.UserControl
	{
		/// <summary>
		/// Gets or sets the display mode of the control (list or icons).
		/// </summary>
		/// <value>The display mode.</value>
		public DisplayMode DisplayMode
		{
			get
			{
				return ViewState["dmode"] == null ? DisplayMode.Icons : (DisplayMode)Convert.ToInt32(ViewState["dmode"], CultureInfo.CurrentCulture);
			}
			set
			{
				ViewState["dmode"] = value;
			}
		}
	}

	public enum DisplayMode
	{
		Icons,
		List
	}
}