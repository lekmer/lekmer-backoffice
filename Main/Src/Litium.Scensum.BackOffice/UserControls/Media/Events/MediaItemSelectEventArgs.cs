﻿using System;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Media.Events
{
	public class MediaItemSelectEventArgs : EventArgs
	{
		public IMediaItem MediaItem { get; set; }
	}
}