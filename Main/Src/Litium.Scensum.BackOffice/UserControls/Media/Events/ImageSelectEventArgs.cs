﻿using System;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Media.Events
{
	public class ImageSelectEventArgs : EventArgs
	{
		public IImage Image { get; set; }
	}
}
