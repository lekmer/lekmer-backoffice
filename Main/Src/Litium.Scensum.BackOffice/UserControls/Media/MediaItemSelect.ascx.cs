﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;
using Resources;
using Image=System.Web.UI.WebControls.Image;
using System.Web.UI;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	public partial class MediaItemSelect : StateUserControlController<string>
	{
		public event EventHandler<MediaItemSelectEventArgs> Selected;
		public event EventHandler FileUploadError;

		private ICollection<IMediaFormat> _mediaFormats;
		protected ICollection<IMediaFormat> MediaFormats
		{
			get { return _mediaFormats ?? (_mediaFormats = IoC.Resolve<IMediaFormatSecureService>().GetAll()); }
		}

		[SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.String.Format(System.String,System.Object)"), SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public string GetSetTabScript()
		{
			return string.Format("$(\'#{0}\').tabs();", divTabsAddImages.ClientID);
		}

		protected override void SetEventHandlers()
		{
			btnUpload.Click += BtnUploadClick;
			ucNodeSelector.NodeCommand += OnNodeSelectorCommand;

			mediaTree.NodeCommand += OnMediaTreeNodeCommand;
			gvImages.RowDataBound += GVMediaItemsRowDataBound;
			btnSaveArchiveImage.Click += IlbSaveArchiveMediaItemClick;
		}

		protected override void PopulateControl()
		{
			int? mediaTreeSelectedNode = mediaTree.SelectedNodeId;

			PopulateMediaItemFolderTree(null);
			PopulateNodeSelectTree(null);
			if (mediaTreeSelectedNode.HasValue)
			{
				mediaTree.SelectedNodeId = mediaTreeSelectedNode;
				PopulateArchiveMediaItems(mediaTreeSelectedNode.Value);
				gvImages.Visible = true;
			}
			else
			{
				gvImages.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CheckPrivileges();
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual void CheckPrivileges()
		{
			var accessValidator = IoC.Resolve<IAccessValidator>();
			liUploadImageTab.Visible = accessValidator.HasAccess(SignInHelper.SignedInSystemUser, PrivilegeConstant.Media);
		}

		private void RegisterScriptOpenFirstTab()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), "open first tab" + ClientID, string.Format(CultureInfo.InvariantCulture, "{0}UploadImageTabClick();", ClientID), true);
		}

		private void ShowErrorMessage(string error)
		{
			messager.Add(error);
			messagerPanel.Update();
		}

		// Upload tab

		protected virtual void OnNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			PopulateNodeSelectTree(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
		}

		protected void PopulateNodeSelectTree(int? nodeId)
		{
			ucNodeSelector.DataSource = GetMediaTreeDataSource(nodeId);
			ucNodeSelector.DataBind();
		}

		protected virtual void BtnUploadClick(object sender, EventArgs e)
		{
			HttpPostedFile file = fileUpload.PostedFile;
			if (file.ContentLength == 0)
			{
				ShowErrorMessage(MediaMessage.FileIsEmpty);
				RegisterScriptOpenFirstTab();
				RaiseUploadError();
				return;
			}

			if (!ucNodeSelector.SelectedNodeId.HasValue)
			{
				ShowErrorMessage(MediaMessage.MediaFolderIsNotSelected);
				RegisterScriptOpenFirstTab();
				RaiseUploadError();
				return;
			}

			string extension = Path.GetExtension(file.FileName);
			IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension.TrimStart(new[] { '.' }));
			if (mediaFormat == null)
			{
				ShowErrorMessage(MediaMessage.IncorrectFileFormat);
				RegisterScriptOpenFirstTab();
				RaiseUploadError();
				return;
			}

			int maxFileSize = HttpRuntimeSetting.MaxRequestLength;
			if (maxFileSize <= 0)
			{
				ShowErrorMessage(MediaMessage.NoConfirmMaxFileSize);
				RegisterScriptOpenFirstTab();
				RaiseUploadError();
				return;
			}

			if (fileUpload.PostedFile.ContentLength / 1024 > maxFileSize)
			{
				ShowErrorMessage(MediaMessage.LargeZipFile);
				RegisterScriptOpenFirstTab();
				RaiseUploadError();
				return;
			}

			var service = IoC.Resolve<IMediaItemSecureService>();
			int mediaId;
			try
			{
				mediaId = service.Insert(SignInHelper.SignedInSystemUser, file.InputStream, ucNodeSelector.SelectedNodeId.Value, file.FileName);
			}
			catch (UploadException ex)
			{
				ShowErrorMessage(ex.Reason == UploadExceptionReason.IncorrectFormat
					? string.Format(CultureInfo.CurrentCulture, MediaMessage.IncorrectFileFormat)
					: string.Format(CultureInfo.CurrentCulture, "{0} {1} {2}", MediaMessage.UploadFailed, ex.Message, ex.InnerException.Message));
				RegisterScriptOpenFirstTab();
				RaiseUploadError();
				return;
			}

			RaiseMediaItemSelectedEvent(mediaId);
		}

		private void RaiseUploadError()
		{
			if (FileUploadError != null)
			{
				FileUploadError(this, EventArgs.Empty);
			}
		}

		// Archive tab

		protected void PopulateMediaItemFolderTree(int? folderId)
		{
			mediaTree.DataSource = GetMediaTreeDataSource(folderId);
			mediaTree.DataBind();
			mediaTree.SelectedNodeId = folderId;
		}

		protected void PopulateArchiveMediaItems(int folderId)
		{
			ids.SelectParameters.Clear();
			ids.SelectParameters.Add("id", folderId.ToString(CultureInfo.CurrentCulture));
		}

		protected virtual void OnMediaTreeNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateMediaItemFolderTree(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					mediaTree.DenySelection = true;
					break;
				case "Navigate":
					PopulateArchiveMediaItems(e.Id);
					gvImages.PageIndex = 0;
					gvImages.Visible = true;
					break;
			}
		}

		protected virtual void GVMediaItemsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var mediaItem = (IMediaItem) row.DataItem;

			var mediaFormat = MediaFormats.FirstOrDefault(i => i.Id == mediaItem.MediaFormatId);
			var lblArchiveImageFileType = (Label)row.FindControl("lblArchiveImageFileType");
			if (mediaFormat != null)
			{
				lblArchiveImageFileType.Text = mediaFormat.Extension;
			}

			var imgArchiveImage = (Image)row.FindControl("imgArchiveImage");
			var image = IoC.Resolve<IImageSecureService>().GetById(mediaItem.Id);
			if (image != null)
			{
				var lblArchiveImageDimensions = (Label)row.FindControl("lblArchiveImageDimensions");
				lblArchiveImageDimensions.Text = image.Width + " x " + image.Height;
				imgArchiveImage.ImageUrl = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
			}
			else
			{
				imgArchiveImage.ImageUrl = ResolveUrl(PathHelper.Media.GetImageThumbnailLoaderUrl(mediaItem.Id, mediaFormat.Extension));
			}

			var rbSelect = (HtmlInputRadioButton) row.FindControl("rbSelect");
			rbSelect.Attributes.Add("onclick", string.Format(CultureInfo.CurrentCulture, "{0}UnCheckOther(this);", ClientID));
		}

		protected virtual void IlbSaveArchiveMediaItemClick(object sender, EventArgs e)
		{
			foreach (GridViewRow row in gvImages.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var rbSelect = (HtmlInputRadioButton) row.FindControl("rbSelect");
				if (rbSelect.Checked)
				{
					var hfId = (HiddenField) row.FindControl("hfId");
					int id = int.Parse(hfId.Value, CultureInfo.CurrentCulture);
					RaiseMediaItemSelectedEvent(id);
					return;
				}
			}

			RaiseUploadError();
		}

		[SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")]
		protected void RaiseMediaItemSelectedEvent(int? id)
		{
			ucNodeSelector.Clear();
			PopulateNodeSelectTree(null);
			PopulateMediaItemFolderTree(null);
			gvImages.Visible = false;

			if (id.HasValue)
			{
				if (Selected != null)
				{
					var mediaItemService = IoC.Resolve<IMediaItemSecureService>();
					IMediaItem mediaItem = mediaItemService.GetById((int) id);
					Selected(this, new MediaItemSelectEventArgs { MediaItem = mediaItem });
				}
			}
		}

		// Common

		protected virtual IEnumerable<INode> GetMediaTreeDataSource(int? folderId)
		{
			return IoC.Resolve<IMediaFolderSecureService>().GetTree(folderId);
		}
	}
}