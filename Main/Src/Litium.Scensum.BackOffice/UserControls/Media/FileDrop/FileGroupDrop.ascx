﻿<%@ control language="C#" autoeventwireup="true" codebehind="FileGroupDrop.ascx.cs" inherits="Litium.Scensum.BackOffice.UserControls.Media.DrungAndDrop.FileGroupDrop" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.30512.20315, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx" %><%@ Register TagPrefix="uc" TagName="ImageGroupCollapsiblePanel" Src="~/UserControls/Assortment/ImageGroupCollapsiblePanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>

<script src="<%=ResolveUrl("~/Media/Scripts/dropzone.js") %>" type="text/javascript"></script>
<link href="<%=ResolveUrl("~/Media/Css/dropzone.css") %>" rel="stylesheet" type="text/css" />
<link href='<%=ResolveUrl("~/Media/Css/filedrop.css") %>' rel="stylesheet" type="text/css" />
<script src="<%=ResolveUrl("~/Media/Scripts/draggable.js") %>" type="text/javascript"></script>
<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>

<script type="text/javascript">
	function ResetDefaultMediaMessages() {
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
	}

	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

	function EndRequestHandler(sender, args) {

		if (args.get_error() == undefined) {
			var previewNode = document.querySelector(".group-list");
			if (previewNode != null) {
				if (previewNode.innerText === "") {
					loadData();
				}
			}

		}

	}

	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
	}

	var previewTemplate = "";
	var myDropzone;
	$(document).ready(function () {

		
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);

		myDropzone = new Dropzone("div#actions", {
			url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerUpload.ashx") %>',
			thumbnailWidth: 100,
			thumbnailHeight: 100,
			parallelUploads: 20,
			previewTemplate: previewTemplate,
			previewsContainer: "#previews",
			acceptedFiles: '<%= AcceptedTypes %>',
			maxFilesize: '<%= AcceptedFileSize %>',
			autoDiscover: false,
			clickable: "#actions"

		});

		myDropzone.on("addedfile", function (file) {
			var zoomId = null;
			if ((file.extension === "pdf") || ((file.type !=null)&&(file.type.match('application/pdf')))) {
				var id = guid();
				file.previewElement.id = id;
				file.previewElement.querySelector(".name").value = file.name;

				var groupid = file.groupId;
				if (groupid != null) {
					file.previewElement.querySelector(".mediaid").value = file.id;
					file.previewElement.querySelector(".size").innerText ="";
					file.previewElement.querySelector(".preview img").className = "imagethum";
				} else {

					groupid = '<%=MediaGroupId %>';
					file.groupId = groupid;
				}
				file.previewElement.querySelector(".defaultButton").style.display = "none";

				var newParent = document.getElementById(groupid).querySelector('.group-list');

				newParent.appendChild(file.previewElement);
				
			} 
			else if ((file.type == null) || (file.type.match(/image.*/))) {
				var id = guid();
				file.previewElement.id = id;
				file.previewElement.querySelector(".name").value = file.name;

				var groupid = file.groupId;
				if (groupid != null) {
					file.previewElement.querySelector(".mediaid").value = file.id;
					file.previewElement.querySelector(".size").innerText = file.size + " (" + file.extension + ")";
					file.previewElement.querySelector(".preview img").className = "imagethum";

					zoomId = guid();
					
					file.previewElement.querySelector('.imagelink').id = zoomId;
					file.previewElement.querySelector('.imagelink').href = "/Modules/Media/MediaOriginalLoader.ashx?MediaId=" + file.id + "&Extension=jpg";
					file.previewElement.querySelector('.zoom-image').src = "/Media/Images/Common/zoom.png";
						

				} else {

					groupid = '<%=MediaGroupId %>';
					file.groupId = groupid;
				}

				var newParent = document.getElementById(groupid).querySelector('.group-list');
				
				
				newParent.appendChild(file.previewElement);
				if (zoomId != null) {
					var classname = "#" + zoomId;
					$(classname).lightBox();

				}


			} else {
				this.removeFile(file);
			}
		});

		myDropzone.on("totaluploadprogress", function (progress) {
			document.querySelector("#progressBar").value = progress;
		});

		myDropzone.on("sending", function (file, xhr, formData) {
			var title = file.previewElement.querySelector(".name").value;
			var groupId = file.previewElement.parentNode.parentNode.id;
			var index = Array.prototype.indexOf.call(file.previewElement.parentNode.children, file.previewElement);
			var productId = document.getElementById("product_id").value;
			formData.append("groupId", groupId);
			formData.append("index", index);
			formData.append("title", title);
			formData.append("imageFolderId", '<%= ImageFolderId %>');
			formData.append("pdfFolderId", '<%= PdfFolderId %>');
			formData.append("userName", '<%= UserName %>');
			formData.append("productId", productId);

			document.querySelector("#progressBar").style.display = 'block';
		});

		myDropzone.on("queuecomplete", function (progress) {
			document.querySelector("#progressBar").value = "0";
			document.querySelector("#progressBar").style.display = 'none';
		});
		myDropzone.on("success", function (file, response) {
			file.previewElement.querySelector(".mediaid").value = response["mediaId"];
			var zoomId = guid();
			file.previewElement.querySelector('.imagelink').id = zoomId;
			file.previewElement.querySelector('.imagelink').href = "/Modules/Media/MediaOriginalLoader.ashx?MediaId=" + response["mediaId"] + "&Extension=jpg";
			var classname = "#" + zoomId;
			$(classname).lightBox();
		});


		$(".image-block").click(function () {

			var header = $(this)[0].parentNode.parentNode;
			var content = header.querySelector('.group-list');
			if ((content.style.display == "block") || (content.style.display == "")) {
				content.style.display = "none";
			}
			else {
				content.style.display = "block";
			}


		});

		loadData();
		
	});

	function loadData() {
		var previewNode = document.querySelector("#template");
		if (previewNode != null) {
			previewNode.parentNode.removeChild(previewNode);
		}


		myDropzone.removeAllFiles(true);
		var productId = document.getElementById("product_id").value;
		$.ajax({
			type: "POST",
			url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandler.ashx") %>',
			data: { productId: productId },
			dataType: "json",
			success: function (response) {

				if (response != null) {
					var files = response;
					previewNode = document.querySelector(".group-list");
					if (previewNode != null) {
						previewNode.InnerHtml = "";
					}
					for (var i = 0; i < files.length; i++) {
						var mockFile = { name: files[i].Title, size: files[i].Size, groupId: files[i].GroupId, extension: files[i].Extension, id: files[i].Id };

							myDropzone.emit("addedfile", mockFile);
							myDropzone.emit("thumbnail", mockFile, files[i].Url);
							myDropzone.emit("complete", mockFile);

					}
				}
			}
		});


	}


	function edit(e) {

		e.style.display = 'none';
		var inputcontrol = e.parentNode.querySelector('.name_entry');
		inputcontrol.style.display = 'block';
		inputcontrol.value = e.innerText;
		inputcontrol.focus();
	};

	function save(e) {

		var control = e.parentNode.querySelector('.name');
		control.style.display = 'block';
		e.style.display = 'none';
		var oldText = control.innerText;
		var newText = e.value;
		control.innerText = newText;
		if (oldText !== newText) {
			var editMediaId = e.parentNode.parentNode.parentNode.querySelector(".mediaid").value;
			$.ajax({
				type: "POST",
				url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerEdit.ashx") %>',
				data: { editMediaId: editMediaId, title: newText, userName: '<%= UserName %>' },
				dataType: "json"
			});
		}

	};

	function deleteImage(e) {
		var name = e.parentNode.parentNode.parentNode.querySelector('.name').innerText;
		var text = "Delete image '" + name + "'?";
		var productId = document.getElementById("product_id").value;
		var deleteMediaId = e.parentNode.parentNode.parentNode.querySelector(".mediaid").value;
		var confirmation = confirm(text);
		if (confirmation) {
			$.ajax({
				type: "POST",
				url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerDelete.ashx") %>',
				data: { deleteMediaId: deleteMediaId, productId: productId },
				dataType: "json",
				success: function (response) {
					e.parentNode.parentNode.parentNode.remove();
				}

			});
		}

	};

	function change(e) {
		var groups = $(".group_container-selected input:radio");
		groups[0].parentNode.parentNode.className = "group_container";
		groups[0].parentNode.parentNode.querySelector(".title-check").innerText = groups[0].parentNode.parentNode.querySelector(".title-check").innerText.replace(" (Active)", "");

		groups = $(".group_container input:radio");
		for (var i = 0; i < groups.length; i++) {
			if (groups[i].checked) {
				groups[i].parentNode.parentNode.className = "group_container-selected";
				groups[i].parentNode.parentNode.querySelector(".title-check").innerText = groups[i].parentNode.parentNode.querySelector(".title-check").innerText + " (Active)";
			} else {
				groups[i].parentNode.parentNode.className = "group_container";
				groups[i].parentNode.parentNode.querySelector(".title-check").innerText = groups[i].parentNode.parentNode.querySelector(".title-check").innerText.replace(" (Active)", "");

			}
		}

	};

	function changeImage(e) {
		var mediaId = e.parentNode.parentNode.parentNode.querySelector(".mediaid").value;
		__doPostBack(null, mediaId);
		return true;
	}

	function up(e) {
		
	var listElement = e.parentNode.parentNode.parentNode;
		var nextElement = listElement.previousSibling;
		var productId = document.getElementById("product_id").value;
		var moveMediaId1 = listElement.querySelector(".mediaid").value;
		var groupId = listElement.parentNode.parentNode.id;

		if (nextElement && nextElement.id) {
			var moveMediaId2 = nextElement.querySelector(".mediaid").value;
			listElement.parentNode.insertBefore(listElement, nextElement);
			$.ajax({
				type: "POST",
				url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerMove.ashx") %>',
				data: { moveMediaId1: moveMediaId1, moveMediaId2: moveMediaId2, productId: productId, groupId: groupId },
				dataType: "json"
			});
		}
		else {
			var groupContainer = listElement.parentNode.parentNode;
			var nextGroupId = parseInt(groupContainer.id) - 1;
			var nextGroupContainer = document.getElementById(nextGroupId);
			if (nextGroupContainer) {
				nextGroupContainer.querySelector(".group-list").appendChild(listElement);
				$.ajax({
					type: "POST",
					url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerMove.ashx") %>',
					data: { moveMediaId1: moveMediaId1, nextGroupId: nextGroupId, productId: productId, groupId: groupId, moveUp: 'true' },
					dataType: "json"
				});
			}
		}
	};


	function down(e) {
		var listElement = e.parentNode.parentNode.parentNode;
		var nextElement = listElement.nextSibling;
		var moveMediaId1 = listElement.querySelector(".mediaid").value;
		var productId = document.getElementById("product_id").value;
		var groupId = listElement.parentNode.parentNode.id;
		if (nextElement && nextElement.id) {
			var moveMediaId2 = nextElement.querySelector(".mediaid").value;
			listElement.parentNode.insertBefore(nextElement, listElement);
			$.ajax({
				type: "POST",
				url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerMove.ashx") %>',
				data: { moveMediaId1: moveMediaId1, moveMediaId2: moveMediaId2, productId: productId, groupId: groupId },
				dataType: "json"

			});
		} else {
			var groupContainer = listElement.parentNode.parentNode;
			var nextGroupId = parseInt(groupContainer.id) + 1;
			var nextGroupContainer = document.getElementById(nextGroupId);
			if (nextGroupContainer) {
				var groupList = nextGroupContainer.querySelector(".group-list");
				groupList.insertBefore(listElement, groupList.firstChild);
				$.ajax({
					type: "POST",
					url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileGroupDropHandlerMove.ashx") %>',
					data: { moveMediaId1: moveMediaId1, nextGroupId: nextGroupId, productId: productId, groupId: groupId, moveUp: 'false' },
					dataType: "json"
				});
			}

		}
	};

</script>




<asp:updatepanel id="ImagesUpdatePanel" runat="server" updatemode="Conditional">	
	<ContentTemplate>
		<div class="dropcontainer">
	<div id="actions" class="dropzone">
		<div>
			 <asp:Literal runat="server" Text="" />
			<div>
				<progress id="progressBar" max="100" value="0" style="display: none">0% complete</progress>
			</div>
			<div class="uplaodbuttoncontainer">				 		

				</div>
		</div>
	</div>
	<div class="folderbutton">
		<uc:ImageLinkButton  runat="server" ID="ImgBrowseButton" Text="<%$ Resources:Media, Literal_SelectMediaFiles%>" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClick="UploadFromFolder"/>	
	</div>								
	<div class="table table-striped" class="files" id="previews">

		<div id="template" dragclass="DragDragBox" overclass="OverDragBox" class="DragBox" dragobj="0">
			<div class="list-item-img">
				<span class="preview">
				<a target="blank"  class="imagelink" >
					<img data-dz-thumbnail />
					 <img  class="zoom-image" />
				</a> 
</span>
			</div>
			<input type="hidden" class="fileguid"></input>
			<input type="hidden" class="mediaid"></input>
			<div class="list-item-button">

				<a class="linkbutton">
					<img src="<%=ResolveUrl("~/Media/Images/Common/up.gif") %>" onclick="up(this)"/>
				</a>
				<a class="linkbutton" class="movedownbutton">
					<img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" onclick="deleteImage(this)"/>
				</a>
				<a class="linkbutton" class="movedownbutton">
					<img src="<%=ResolveUrl("~/Media/Images/Common/down.gif") %>" onclick="down(this)"/>
				</a>

			</div>
			<div class="list-item-size">
				<p data-dz-size class="size"></p>

				
			</div>
			<div class="list-item">
				<div>
					<p class="name" data-dz-name onclick="edit(this)"></p>
					<input class="name_entry" name="amount" style="display: none;" onblur="save(this)"></input>
				</div>
				<div class ="defaultButton">
					<uc:ImageLinkButton  runat="server" ID="DeleteImageButton" CommandArgument='<%# Eval("ID") %>' Text="<%$ Resources:Media, Literal_MakeDefault%>"  SkinID="DefaultButton" OnClientClick="changeImage(this)" />

				</div>
			</div>

			<div>
			</div>
		</div>

	</div>

</div>
				<asp:Repeater ID="ImageGroupsRepeater" runat="server">
			<ItemTemplate> 
				<div id='<%# Eval("Id") %>' dropobj="0" class="super-scroller">
					<div class="group_container" style="display: none">
					</div>
					<div class="group-list">
						</div>
				</div> 
			</ItemTemplate>
		 </asp:Repeater> 

	</ContentTemplate> 
</asp:updatepanel>


		











