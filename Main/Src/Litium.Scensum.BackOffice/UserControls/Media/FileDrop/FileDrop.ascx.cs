﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Litium.Scensum.BackOffice.UserControls.Media.DrungAndDrop
{
	public partial class FileDrop : ControlBase
	{
		private const string FolderIdKey = "folderId";
		private const string UserIdKey = "userId";
		private const string AcceptedTypesKey = "acceptedFiles";
		private const string AcceptedFileSizeKey = "acceptedFileSize";

		public object FolderId
		{
			get { return ViewState[FolderIdKey]; }
			set { ViewState[FolderIdKey] = value; }
		}
		public object UserName
		{
			get { return ViewState[UserIdKey]; }
			set { ViewState[UserIdKey] = value; }
		}

		public object AcceptedTypes
		{
			get { return ViewState[AcceptedTypesKey]; }
			set { ViewState[AcceptedTypesKey] = value; }
		}

		public object AcceptedFileSize
		{
			get { return ViewState[AcceptedFileSizeKey]; }
			set { ViewState[AcceptedFileSizeKey] = value; }
		}
	}
}