﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Media;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media.DrungAndDrop
{
	public partial class FileGroupDrop : StateUserControlController<ProductImageGroupsState>
	{
		private const string ImageFolderIdKey = "imageFolderId";
		private const string PdfFolderIdKey = "pdfFolderId";
		private const string UserIdKey = "userId";
		private const string AcceptedTypesKey = "acceptedFiles";
		private const string AcceptedFileSizeKey = "acceptedFileSize";
		public event EventHandler<ImageSelectEventArgs> DefaltImageSet;
		public event EventHandler UploadMediaFromFolder;
		public event EventHandler ImageUploaded;

		public object ImageFolderId
		{
			get { return ViewState[ImageFolderIdKey]; }
			set { ViewState[ImageFolderIdKey] = value; }
		}

		public object PdfFolderId
		{
			get { return ViewState[PdfFolderIdKey]; }
			set { ViewState[PdfFolderIdKey] = value; }
		}

		public object UserName
		{
			get { return ViewState[UserIdKey]; }
			set { ViewState[UserIdKey] = value; }
		}

		public object AcceptedTypes
		{
			get { return ViewState[AcceptedTypesKey]; }
			set { ViewState[AcceptedTypesKey] = value; }
		}

		public object AcceptedFileSize
		{
			get { return ViewState[AcceptedFileSizeKey]; }
			set { ViewState[AcceptedFileSizeKey] = value; }
		}

		protected override void SetEventHandlers()
		{
			Page.Load += Page_Load;
			//ImageSelectControl.Selected += ImageSelected;
			//ImageSelectControl.FileUploaded += ImageSelectFileUploated;
		}

		void Page_Load(object sender, EventArgs e)
		{
			if (ProductId > 0)
			{
				Page.ClientScript.RegisterHiddenField("product_id", ProductId.ToString());
			}
		}

		private void ImageSelectFileUploated(object sender, EventArgs e)
		{
			//ImagesPopup.Show();
			//ImagesPopup.X = 227;
			if (ImageUploaded != null)
			{
				ImageUploaded(this, EventArgs.Empty);
			}
		}

		public readonly int StepMove = 10;
		public readonly int MediaGroupId = Int32.Parse(ConfigurationManager.AppSettings["ImageGroupId"]);
		private void ImageSelected(object sender, ImageSelectEventArgs e)
		{
			if (ImageUploaded != null)
			{
				ImageUploaded(this, EventArgs.Empty);
			}

			var productImageSecureService = IoC.Resolve<IProductImageSecureService>();
			int groupId = MediaGroupId;
			

			IProductImage productImage = productImageSecureService.Create(e.Image, ProductId, groupId);
			IProductImageGroupFull imageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == groupId);
			if (imageGroup.ProductImages.Count > 0)
			{
				productImage.Ordinal = imageGroup.ProductImages.Max(item => item.Ordinal) + StepMove;
			}

			imageGroup.ProductImages.Add(productImage);
		}

		protected override void PopulateControl()
		{
			var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
			ProductImageGroups = productImageGroupSecureService.GetAllFullByProduct(ProductId);
			Page.ClientScript.RegisterHiddenField("product_id", ProductId.ToString());

			ImageGroupsRepeater.DataSource = ProductImageGroups.Where(m => m.Id == Int32.Parse(ConfigurationManager.AppSettings["ImageGroupId"]));
			ImageGroupsRepeater.DataBind();
		}



		public int ProductId
		{
			get
			{
				EnsureState();
				return State.ProductId;
			}
			set
			{
				EnsureState();
				State.ProductId = value;
			}
		}

		private static Collection<IProductImage> Sort(IEnumerable<IProductImage> list, int ordinalsStep)
		{
			var productImages = new List<IProductImage>(list);
			productImages.Sort(delegate(IProductImage image1, IProductImage image2)
			{
				if (image1.Ordinal < image2.Ordinal)
					return -1;
				if (image1.Ordinal > image2.Ordinal)
					return 1;
				return 0;
			});
			for (int i = 0; i < productImages.Count; i++)
			{
				productImages[i].Ordinal = (i + 1) * ordinalsStep;
			}
			return new Collection<IProductImage>(productImages);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public Collection<IProductImageGroupFull> GetProductImageGroups()
		{
			SortProductImageGroup();
			return ProductImageGroups;
		}

		private void SortProductImageGroup()
		{
			foreach (var imageGroup in ProductImageGroups)
			{
				imageGroup.ProductImages = Sort(imageGroup.ProductImages, 1);
			}
		}

		private Collection<IProductImageGroupFull> ProductImageGroups
		{
			get { return State.ProductImageGroups; }
			set
			{
				EnsureState();
				State.ProductImageGroups = value;
			}
		}

		protected void EnsureState()
		{
			if (State == null)
			{
				State = new ProductImageGroupsState();
			}
		}

		public void SetAsDefaultImage(int mediaId)
		{
			var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
			ProductImageGroups = productImageGroupSecureService.GetAllFullByProduct(ProductId);
			Page.ClientScript.RegisterHiddenField("product_id", ProductId.ToString());
			if (DefaltImageSet != null)
			{
				IProductImage image;
				foreach (var group in ProductImageGroups)
				{
					image = group.ProductImages.SingleOrDefault(m => ((m.MediaId == mediaId) && (m.ProductId == ProductId)));
					if (image != null)
					{
						DefaltImageSet(this, new ImageSelectEventArgs { Image = image.Image });
						break;
					}
				}
			}
		}

		protected void UploadFromFolder(object sender, EventArgs e)
		{
			if (UploadMediaFromFolder != null)
			{
				UploadMediaFromFolder(this, null);
			}
		}


	}


}