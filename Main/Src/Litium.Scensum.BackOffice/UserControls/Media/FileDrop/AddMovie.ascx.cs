﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Litium.Scensum.BackOffice.UserControls.Media.FileDrop
{
	public partial class AddMovie : System.Web.UI.UserControl
	{
		private const string FolderIdKey = "folderId";
		private const string UserIdKey = "userId";
		public object UserName
		{
			get { return ViewState[UserIdKey]; }
			set { ViewState[UserIdKey] = value; }
		}

		public object FolderId
		{
			get { return ViewState[FolderIdKey]; }
			set { ViewState[FolderIdKey] = value; }
		}
		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}