﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media.FileDrop.Handlers
{
	/// <summary>
	/// Summary description for FileDropMovieHandler
	/// </summary>
	public class FileDropMovieHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			int productId;
			if (Int32.TryParse(context.Request["productId"], out productId))
			{
				var imageSecureService = (ILekmerImageSecureService)IoC.Resolve<IImageSecureService>();
				var liteImageList = new List<LekmerImageLite>();

				var mediaList = imageSecureService.GetMoviesByProductId(productId);
				liteImageList.AddRange(mediaList.Select(media => new LekmerImageLite()
				{
					Id = media.ProductMedia.MediaId,
					Link = media.LekmerImage.Link,
					Parameters = media.LekmerImage.Parameter,
					ThumbnailUrl = media.LekmerImage.HasImage ?  ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(media.ProductMedia.MediaId, media.ProductMedia.Extension)) : string.Empty,

				}));
				var js = new JavaScriptSerializer();
				context.Response.ContentType = "application/json";
				context.Response.Write(js.Serialize(liteImageList));
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		public static string ResolveUrl(string originalUrl)
		{
			if (originalUrl == null)
				return null;

			if (originalUrl.IndexOf("://") != -1)
				return originalUrl;


			if (originalUrl.StartsWith("~"))
			{
				string newUrl = "";
				if (HttpContext.Current != null)
				{
					Uri originalUri = HttpContext.Current.Request.Url;
					newUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host +
							 originalUrl.Substring(1).Replace("\\", "/");
				}
				else
					throw new ArgumentException("Invalid URL: Relative URL not allowed.");
				return newUrl;
			}

			return originalUrl;
		}
	}

	public class LekmerImageLite
	{
		public int Id { get; set; }
		public string Link { get; set; }
		public string Parameters { get; set; }
		public string ThumbnailUrl { get; set; }

	}
}