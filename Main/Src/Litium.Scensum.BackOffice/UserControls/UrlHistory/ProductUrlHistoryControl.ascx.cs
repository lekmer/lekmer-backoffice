﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.UserControls.UrlHistory
{
	public partial class ProductUrlHistoryControl : StateUserControlController<UrlHistoryState>
	{
		private Collection<ILanguage> _languages;
		private Collection<IProductUrlHistory> _productUrlHistoryList;


		protected virtual UrlHistoryState SafeState
		{
			get
			{
				return State ?? (State = new UrlHistoryState());
			}
		}
		public int ProductId
		{
			get
			{
				return SafeState.BusinessObjectId;
			}
			set
			{
				SafeState.BusinessObjectId = value;
			}
		}
		public ImageButton TriggerImageButton
		{
			get
			{
				return UrlHistoryButton;
			}
		}


		public void DataBindManual()
		{
			base.DataBind();

			SetUrlHistory();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			UrlHistoryPopup.BehaviorID = ClientID + "_urlHistoryPopup";
			UrlHistoryPopup.PopupControlID = UrlHistoryDiv.ClientID;

			var clientScript = Page.ClientScript;

			if (!clientScript.IsClientScriptBlockRegistered("closePopup"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "closePopup", @"
					function closePopup(b) {
						$find(b).hide();
						return false;
					};
				", true);
			}
		}
		protected void Cancel(object sender, EventArgs e)
		{
			DataBindManual();

			ClosePopup();
		}
		protected void AddNewOldUrlHistory(object sender, EventArgs e)
		{
			Collection<IProductUrlHistory> items = GetUrlHistoryItems(false);

			var imageLinkButton = (ImageLinkButton)sender;

			var languageIdControl = imageLinkButton.Parent.FindControl("LangIdHidden") as HiddenField;
			if (languageIdControl == null)
			{
				return;
			}

			int languageId;
			if (!int.TryParse(languageIdControl.Value, out languageId))
			{
				return;
			}

			var newOldUrlTitleControl = imageLinkButton.Parent.FindControl("NewOldUrlTitle") as TextBox;
			if (newOldUrlTitleControl == null)
			{
				return;
			}

			var newOldUrlTitle = newOldUrlTitleControl.Text;

			if (newOldUrlTitle.IsEmpty())
			{
				return;
			}

			if (items.FirstOrDefault(u => u.UrlTitleOld == newOldUrlTitle && u.LanguageId == languageId) == null)
			{
				var newUrlHistory = IoC.Resolve<IProductUrlHistorySecureService>().Create(ProductId, languageId, newOldUrlTitle, (int)HistoryLifeIntervalType.Month);
				items.Add(newUrlHistory);

				_productUrlHistoryList = items;

				DataBindManual();
			}
			else
			{
				newOldUrlTitleControl.Text = string.Empty;
			}
		}
		protected void Delete(object sender, EventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var urlHistoryIdControl = imageButton.Parent.FindControl("UrlHistoryId") as HiddenField;
			if (urlHistoryIdControl == null)
			{
				return;
			}

			int urlHistoryId;
			if (!int.TryParse(urlHistoryIdControl.Value, out urlHistoryId))
			{
				return;
			}

			Collection<IProductUrlHistory> items = GetUrlHistoryItems(false);

			if (urlHistoryId > 0)
			{
				IoC.Resolve<IProductUrlHistorySecureService>().DeleteById(SignInHelper.SignedInSystemUser, urlHistoryId);
				_productUrlHistoryList = new Collection<IProductUrlHistory>(items.Where(u => u.Id != urlHistoryId).ToList());
			}
			else
			{
				var urlHistoryControl = imageButton.Parent.FindControl("OldUrlTitle") as TextBox;
				if (urlHistoryControl == null)
				{
					return;
				}

				string urlTitle = urlHistoryControl.Text;

				var languageIdControl = imageButton.Parent.Parent.Parent.FindControl("LangIdHidden") as HiddenField;
				if (languageIdControl == null)
				{
					return;
				}

				int languageId;
				if (!int.TryParse(languageIdControl.Value, out languageId))
				{
					return;
				}

				_productUrlHistoryList = new Collection<IProductUrlHistory>(items.Where(u => (u.LanguageId != languageId || u.UrlTitleOld != urlTitle)).ToList());
			}

			DataBindManual();
		}
		protected void Save(object sender, EventArgs e)
		{
			Collection<IProductUrlHistory> items = GetUrlHistoryItems(true);

			if (items.Count > 0)
			{
				IoC.Resolve<IProductUrlHistorySecureService>().Save(SignInHelper.SignedInSystemUser, items);

				_productUrlHistoryList = null;
				DataBindManual();
			}

			ClosePopup();
		}
		protected void LanguageItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
			{
				return;
			}

			var urlHistoryRepeater = e.Item.FindControl("UrlHistoryRepeater") as Repeater;
			if (urlHistoryRepeater == null)
			{
				return;
			}

			var languageIdControl = e.Item.FindControl("LangIdHidden") as HiddenField;
			if (languageIdControl == null)
			{
				return;
			}

			int languageId;
			if (!int.TryParse(languageIdControl.Value, out languageId))
			{
				return;
			}

			urlHistoryRepeater.DataSource = GetAllUrlHistoryByLanguageId(languageId);
			urlHistoryRepeater.DataBind();
		}
		protected override void DataBindChildren()
		{
			SetUrlHistory();

			base.DataBindChildren();
		}


		protected virtual void SetUrlHistory()
		{
			var languages = GetAllLanguages();

			LanguageRepeater.DataSource = languages;
			LanguageRepeater.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ILanguage> GetAllLanguages()
		{
			return _languages ?? (_languages = IoC.Resolve<ILanguageSecureService>().GetAll());
		}

		protected virtual string GetLanguageTitle(object languageId)
		{
			var id = System.Convert.ToInt32(languageId, CultureInfo.CurrentCulture);
			var language = GetAllLanguages().FirstOrDefault(l => l.Id == id);

			return language != null ? language.Title : string.Empty;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IProductUrlHistory> GetAllUrlHistoryByProduct()
		{
			return _productUrlHistoryList ?? (_productUrlHistoryList = IoC.Resolve<IProductUrlHistorySecureService>().GetAllByProduct(ProductId));
		}

		protected virtual Collection<IProductUrlHistory> GetAllUrlHistoryByLanguageId(int languageId)
		{
			return new Collection<IProductUrlHistory>(GetAllUrlHistoryByProduct().Where(u => u.LanguageId == languageId).ToList());
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IProductUrlHistory> GetUrlHistoryItems(bool getOnlyNew)
		{
			var items = new Collection<IProductUrlHistory>();

			foreach (RepeaterItem repeaterItem in LanguageRepeater.Items)
			{
				if (repeaterItem.ItemType != ListItemType.Item && repeaterItem.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var urlHistoryList = GetUrlHistoryItems(repeaterItem, getOnlyNew);
				if (urlHistoryList != null)
				{
					items.AddRange(urlHistoryList);
				}
			}

			return items;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IProductUrlHistory> GetUrlHistoryItems(RepeaterItem repeaterItem, bool getOnlyNew)
		{
			var urlHistoryRepeater = repeaterItem.FindControl("UrlHistoryRepeater") as Repeater;
			if (urlHistoryRepeater == null)
			{
				return null;
			}

			var languageIdControl = repeaterItem.FindControl("LangIdHidden") as HiddenField;
			if (languageIdControl == null)
			{
				return null;
			}

			int languageId;
			if (!int.TryParse(languageIdControl.Value, out languageId))
			{
				return null;
			}

			var items = new Collection<IProductUrlHistory>();

			foreach (RepeaterItem urlHistoryItem in urlHistoryRepeater.Items)
			{
				var urlHistory = GetUrlHistory(urlHistoryItem, languageId, getOnlyNew);
				if (urlHistory != null)
				{
					items.Add(urlHistory);
				}
			}

			return items;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
		protected virtual IProductUrlHistory GetUrlHistory(Control item, int languageId, bool getOnlyNew)
		{
			var urlHistoryIdControl = item.FindControl("UrlHistoryId") as HiddenField;
			if (urlHistoryIdControl == null)
			{
				return null;
			}

			int urlHistoryId;
			if (!int.TryParse(urlHistoryIdControl.Value, out urlHistoryId))
			{
				return null;
			}

			if (getOnlyNew && urlHistoryId > 0) // if urlHistoryId > 0 => this is new item.
			{
				return null;
			}

			var urlHistoryControl = item.FindControl("OldUrlTitle") as TextBox;
			if (urlHistoryControl == null)
			{
				return null;
			}

			string urlTitle = urlHistoryControl.Text;

			var productUrlHistory = IoC.Resolve<IProductUrlHistorySecureService>().Create(ProductId, languageId, urlTitle, (int)HistoryLifeIntervalType.Month);
			productUrlHistory.Id = urlHistoryId;

			return productUrlHistory;
		}

		protected override void SetEventHandlers()
		{
		}
		protected override void PopulateControl()
		{
		}

		private void ClosePopup()
		{
			string startupScript = "closePopup('" + UrlHistoryPopup.BehaviorID + "')";
			ScriptManager.RegisterStartupScript(UrlHistoryUpdatePanel, GetType(), "close", startupScript, true);
		}
	}
}