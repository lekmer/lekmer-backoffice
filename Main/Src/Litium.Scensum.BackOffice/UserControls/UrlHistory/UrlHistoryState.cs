﻿using System;

namespace Litium.Scensum.BackOffice.UserControls.UrlHistory
{
	[Serializable]
	public sealed class UrlHistoryState
	{
		public int BusinessObjectId { get; set; }
	}
}