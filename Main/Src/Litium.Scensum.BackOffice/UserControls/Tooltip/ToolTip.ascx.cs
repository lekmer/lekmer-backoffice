﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.UserControls.Tooltip
{
    public partial class ToolTip : System.Web.UI.UserControl
    {
        public string AliasName { get; set; }
        public string Style { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            IAlias alias = IoC.Resolve<IAliasSharedService>().GetByCommonName(ChannelHelper.CurrentChannel, AliasName);
            if (alias!=null)
            {
                c_toolTip.Text = alias.Value;
            }
            else
            {
                c_toolTip.Text = "Missing alias: " + AliasName;
            }

            if (!string.IsNullOrEmpty(Style))
            {
                var s = Style.Split(':');
                if (s.Length > 1)
                {
                    TooltipTag.Attributes.CssStyle.Add(s[0].Trim(), s[1].Trim().Replace(";", ""));
                }
            }

        }
    }
}