﻿using System;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.UserControls.Translation
{
	[Serializable]
	public class TranslationItem
	{
		public ILanguage Language { get; set; }
		public string Value { get; set; }
	}
}