﻿<%@ Control Language="C#" CodeBehind="GenericTranslator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Translation.GenericTranslator" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:ImageButton runat="server" ID="TranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
<ajaxToolkit:ModalPopupExtender ID="TranslatePopup" runat="server" PopupControlID="TranslateDiv" BackgroundCssClass="popup-background" Y="100" TargetControlID="TranslateButton" />
<div id="TranslateDiv" runat="server" class="translation-popup-container" style="z-index: 10010; display: none;">
	<div id="translation-popup-header">
		<div id="translation-popup-header-left">
		</div>
		<div id="translation-popup-header-center">
			<span>Translate</span>
			<asp:Button ID="CloseTranslationButton" runat="server" Text="x" />
		</div>
		<div id="translation-popup-header-right">
		</div>
	</div>
	<div id="translation-popup-content">
		<div id="SimpleDiv" runat="server">
			<span>Default</span>
			<br />
			<asp:TextBox ID="SimpleValueTextBox" runat="server" />
			<asp:HiddenField ID="SimpleValueHiddenField" runat="server" />
			<div runat="server" id="TranslatinListDiv">
				<asp:Repeater ID="SimpleTranslationRepeater" runat="server">
					<ItemTemplate>
						<div id="value-translate" class="translation-item">
							<asp:Label ID="LanguageTitleSimpleLabel" runat="server" Text='<%# GetLanguageTitle(Eval("LanguageId")) %>' />
							<asp:Image ID="ExclamationSimpleImg" runat="server" ImageUrl="~/Media/Images/Interface/exclamation.gif" style='<%# GetExclamationVisibility(Eval("Value")) %>' />
							<br />
							<asp:TextBox ID="TranslateValueSimpleText" runat="server" Text='<%# Eval("Value") %>' />
							<asp:HiddenField ID="TranslateValueSimpleHidden" runat="server" Value='<%# Eval("Value") %>' />
							<asp:HiddenField ID="LangIdSimpleHidden" runat="server" Value='<%# Eval("LanguageId").ToString() %>' />
						</div>
					</ItemTemplate>
				</asp:Repeater>
			</div>
		</div>
		<div class="translation-popup-acton-button">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" UseSubmitBehaviour="true" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="Cancel" SkinID="DefaultButton" />
		</div>
	</div>
</div>