﻿<%@ Control Language="C#" CodeBehind="GenericMultilineTranslator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Translation.GenericMultilineTranslator" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="IABContros" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<ajaxToolkit:ModalPopupExtender ID="TranslatePopup" runat="server" BackgroundCssClass="popup-background" Y="100" />
<div id="MultiLineTranslateDiv" runat="server" class="translation-popup-container" style="z-index: 10010; display: none;">
	<div id="translation-popup-header">
		<div id="translation-popup-header-left">
		</div>
		<div id="translation-popup-header-center">
			<span>Translate</span>
			<asp:Button ID="CloseTranslationButton" runat="server" Text="x" />
		</div>
		<div id="translation-popup-header-right">
		</div>
	</div>
	<div id="translation-popup-content">
		<div id="MultiLineDiv" runat="server">
			<span>Default</span>
			<br />
			<asp:TextBox ID="ValueTextBox" runat="server" TextMode="MultiLine" Rows="5" />
			<asp:HiddenField ID="ValueHiddenField" runat="server" />
			<div runat="server" id="MultiLineTabsDiv" class="translations-multi-line">
				<ul>
					<asp:Repeater ID="MultiLanguageRepeater" runat="server">
						<ItemTemplate>
							<li>
								<a id="tabLnkMulti" href='<%# string.Concat("#multiLine_", ID, Eval("Language.Id")) %>'>
									<asp:Label ID="lblLanguageTitleMulti" runat="server" Text='<%# Eval("Language.Title") %>' />
									&nbsp;
									<asp:Image ID="imgExclamationMulti" runat="server" ImageUrl="~/Media/Images/Interface/exclamation.gif" ImageAlign="AbsMiddle" style='<%# GetExclamationVisibility((string)Eval("Value")) %>' />
								</a>
							</li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<br clear="all" />
				<div runat="server" id="TranslatinListDiv" class="translation-popup-tabs-container">
					<asp:Repeater ID="MultiTranslationRepeater" runat="server">
						<ItemTemplate>
							<div id='<%# string.Concat("multiLine_", ID, Eval("Language.Id")) %>'>
								<asp:TextBox ID="tbTranslateValueMulti" runat="server" TextMode="MultiLine" Text='<%# Eval("Value") %>' Rows="5" />
								<asp:HiddenField ID="hfTranslateMulti" runat="server" Value='<%# Eval("Value") %>' />
								<asp:HiddenField ID="hfLangIdMulti" runat="server" Value='<%# Eval("Language.Id").ToString() %>' />
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
		</div>
		<br class="clear"/>
		<div class="translation-popup-acton-button">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="Cancel" SkinID="DefaultButton" />
		</div>
	</div>
</div>