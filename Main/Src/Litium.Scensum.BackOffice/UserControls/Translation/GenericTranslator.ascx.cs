﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Translation
{
	public partial class GenericTranslator : StateUserControlController<GenericTranslatorState>
	{
		private Collection<ILanguage> _languages;

		public ImageButton TriggerImageButton
		{
			get
			{
				return TranslateButton;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ITranslationGeneric> DataSource
		{
			get; set;
		}
		
		public string DefaultValueControlClientId
		{
			get
			{
				return SafeState.DefaultValueControlClientId;
			}
			set
			{
				SafeState.DefaultValueControlClientId = value;
			}
		}
		
		public int BusinessObjectId
		{
			get
			{
				return SafeState.BusinessObjectId;
			}
			set
			{
				SafeState.BusinessObjectId = value;
			}
		}

		protected virtual GenericTranslatorState SafeState
		{
			get
			{
				return State ?? (State = new GenericTranslatorState());
			}
		}

        public bool UseManualDataBinding { get; set; }

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
		}

        protected override void DataBindChildren()
        {
            if (!UseManualDataBinding)
            {
                SetTranslations(DataSource);
            }
            base.DataBindChildren();
        }

        public void DataBindManual()
        {
            base.DataBind();
            SetTranslations(DataSource);
        }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

            TranslatePopup.BehaviorID = ClientID + "_translatorPopup";
		    TranslatePopup.PopupControlID = TranslateDiv.ClientID;

			CloseTranslationButton.OnClientClick += "javascript:clearPopup('" + SimpleValueTextBox.ClientID + "', '" + SimpleValueHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + TranslatinListDiv.ClientID + "'); return false;";
			SaveButton.OnClientClick += "javascript:changeDefaultAndTranslations('" + SimpleValueTextBox.ClientID + "', '" + DefaultValueControlClientId + "', '" + SimpleValueHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + TranslatinListDiv.ClientID + "'); return false;";
			CancelButton.OnClientClick += "javascript:clearPopup('" + SimpleValueTextBox.ClientID + "', '" + SimpleValueHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + TranslatinListDiv.ClientID + "'); return false;";
			TranslateButton.OnClientClick += "SetDefaultValue('" + DefaultValueControlClientId + "', '" + SimpleValueTextBox.ClientID + "', '" + SimpleValueHiddenField.ClientID + "');";

			var clientScript = Page.ClientScript;

			if (!clientScript.IsClientScriptBlockRegistered("RemoveRedundantSpaces"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "RemoveRedundantSpaces", @"
					function RemoveRedundantSpaces(str) {
						return str.replace(/(&nbsp;)+|\s+/g, "" "").trim();
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("closePopup"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "closePopup", @"
					function closePopup(b) {
						$find(b).hide();
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("clearPopup"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "clearPopup", @"
					function clearPopup(t,h,b,l) {
						document.getElementById(t).value = document.getElementById(h).value;
						var rptTranslations = document.getElementById(l)
						var rptTranslationValues = rptTranslations.getElementsByTagName('input');
						for (var i = 0; i < rptTranslationValues.length; i++) {
							var translationValue = rptTranslationValues[i];
							if (translationValue.type === ""text"" && translationValue.nextSibling.nextSibling.type === ""hidden"") {
								translationValue.value = translationValue.nextSibling.nextSibling.value;
							}
						}
						closePopup(b);
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("changeDefaultAndTranslations"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "changeDefaultAndTranslations", @"
					function changeDefaultAndTranslations(t,d,h,b,l) {
						var defValue = RemoveRedundantSpaces(document.getElementById(t).value);
						document.getElementById(t).value = defValue;
						document.getElementById(d).value = defValue;
						document.getElementById(h).value = defValue;
						var rptTranslations = document.getElementById(l)
						var rptTranslationValues = rptTranslations.getElementsByTagName('input');
						var rptTranslationImages = rptTranslations.getElementsByTagName('img');
						var imgCount = 0;
						for (var i = 0; i < rptTranslationValues.length; i++) {
							var translationValue = rptTranslationValues[i];
							if (translationValue.type === ""text"" && translationValue.nextSibling.nextSibling.type === ""hidden"") {
								var translationVal = RemoveRedundantSpaces(translationValue.value);
								translationValue.value = translationVal;
								translationValue.nextSibling.nextSibling.value = translationVal;
								var translationImg = rptTranslationImages[imgCount];
								imgCount = imgCount + 1;
								if (translationValue.value === """") {
									translationImg.style.display = 'inline-block';
								}
								else {
									translationImg.style.display = 'none';
								}
							}
						}
						closePopup(b);
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("SetDefaultValue"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "SetDefaultValue", @"
					function SetDefaultValue(d,t,h) {
						var defValue = RemoveRedundantSpaces(document.getElementById(d).value);
						document.getElementById(d).value = defValue;
						document.getElementById(t).value = defValue;
						document.getElementById(h).value = defValue;
					};
				", true);
			}
		}

		protected static string GetExclamationVisibility(object value)
		{
			return string.IsNullOrEmpty(value as string) 
				? "display:inline-block" 
				: "display:none";
		}

		public virtual void ClearTranslations()
		{
			SetTranslations(GetEmptyTranslations());
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<ITranslationGeneric> GetTranslations()
		{
			var translations = new Collection<ITranslationGeneric>();
			foreach (RepeaterItem item in SimpleTranslationRepeater.Items)
			{
				var translation = GetTranslation(item);
				translations.Add(translation);
			}
			return translations;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly")]
		protected virtual ITranslationGeneric GetTranslation(Control item)
		{
			const string tbTranslateValue = "TranslateValueSimpleText";
			const string hfLangId = "LangIdSimpleHidden";

			var hiddenField = item.FindControl(hfLangId) as HiddenField;
			if (hiddenField == null) throw new ArgumentNullException(hfLangId);
			var languageId = Int32.Parse(hiddenField.Value, CultureInfo.CurrentCulture);

			var tbValue = item.FindControl(tbTranslateValue) as TextBox;
			if (tbValue == null) throw new ArgumentNullException(tbTranslateValue);
			var value = tbValue.Text;

			return CreateTranslation(languageId, value);
		}

		protected virtual void SetTranslations(ICollection<ITranslationGeneric> translations)
		{
			var languages = GetAllLanguages();
			foreach (var language in languages)
			{
				var languageId = language.Id;
				if (translations.FirstOrDefault(t => t.LanguageId == languageId) == null)
				{
					translations.Add(CreateTranslation(languageId, string.Empty));
				}
			}
			SimpleTranslationRepeater.DataSource = translations;
            if (UseManualDataBinding)
            {
                SimpleTranslationRepeater.DataBind();
            }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ITranslationGeneric> GetEmptyTranslations()
		{
			var languages = GetAllLanguages();
			var translations = new Collection<ITranslationGeneric>();
			foreach (var language in languages)
			{
				translations.Add(CreateTranslation(language.Id, string.Empty));
			}
			return translations;
		}

		protected virtual ITranslationGeneric CreateTranslation(int languageId, string value)
		{
			var translation = IoC.Resolve<ITranslationGeneric>();
			translation.Id = BusinessObjectId;
			translation.LanguageId = languageId;
			translation.Value = string.IsNullOrEmpty(value) ? null : value;
			return translation;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ILanguage> GetAllLanguages()
		{
			return _languages ?? 
				(_languages = IoC.Resolve<ILanguageSecureService>().GetAll());
		}

		protected virtual string GetLanguageTitle(object languageId)
		{
			var id = System.Convert.ToInt32(languageId, CultureInfo.CurrentCulture);
			var language = GetAllLanguages().FirstOrDefault(l => l.Id == id);
			return language != null ? language.Title : string.Empty;
		}
	}

	[Serializable]
	public sealed class GenericTranslatorState
	{
		public string DefaultValueControlClientId { get; set; }
		public int BusinessObjectId { get; set; }
	}
}
