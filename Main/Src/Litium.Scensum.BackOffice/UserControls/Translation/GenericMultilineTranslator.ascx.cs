﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Translation
{
	public partial class GenericMultilineTranslator : StateUserControlController<GenericTranslatorState>
	{
		public ImageButton TriggerImageButton { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ITranslationGeneric> DataSource
		{
			get;
			set;
		}

		public string DefaultValueControlClientId
		{
			get
			{
				return SafeState.DefaultValueControlClientId;
			}
			set
			{
				SafeState.DefaultValueControlClientId = value;
			}
		}

		public int BusinessObjectId
		{
			get
			{
				return SafeState.BusinessObjectId;
			}
			set
			{
				SafeState.BusinessObjectId = value;
			}
		}

		protected virtual GenericTranslatorState SafeState
		{
			get
			{
				return State ?? (State = new GenericTranslatorState());
			}
		}

		public virtual void Populate()
		{
			PopulateControl();
		}

		protected override void SetEventHandlers(){ }
		protected override void PopulateControl()
		{
			SetMultiTranslations(GetTranslationItems());
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			TranslatePopup.BehaviorID = ClientID + "_translatorPopup";
			TranslatePopup.PopupControlID = MultiLineTranslateDiv.ClientID;
			TranslatePopup.TargetControlID = TriggerImageButton.ID;

			CloseTranslationButton.OnClientClick += "clearPopupMultiline" + ID + @"('" + ValueTextBox.ClientID + "', '" + ValueHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + TranslatinListDiv.ClientID + "'); return false;";
			SaveButton.OnClientClick += "changeDefaultAndTranslationsMultiline" + ID + @"('" + ValueTextBox.ClientID + "', '" + DefaultValueControlClientId + "', '" + ValueHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + TranslatinListDiv.ClientID + "', '" + MultiLineTabsDiv.ClientID + "'); return false;";
			CancelButton.OnClientClick += "clearPopupMultiline" + ID + @"('" + ValueTextBox.ClientID + "', '" + ValueHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + TranslatinListDiv.ClientID + "'); return false;";
			TriggerImageButton.OnClientClick += "SetDefaultValueMultiline" + ID + @"('" + DefaultValueControlClientId + "', '" + ValueTextBox.ClientID + "', '" + ValueHiddenField.ClientID + "');";

			var clientScript = Page.ClientScript;

			string documentReadyKey = "JQueryTabs" + ID;
			string documentReadyScript = @"$(document).ready(function() { $('#" + MultiLineTabsDiv.ClientID + "').tabs(); });";

			var scriptManager = ScriptManager.GetCurrent(Page);
			if (scriptManager != null && scriptManager.IsInAsyncPostBack)
			{
				ScriptManager.RegisterClientScriptBlock(this, GetType(), documentReadyKey, documentReadyScript, true);
			}
			else
			{
				clientScript.RegisterClientScriptBlock(GetType(), documentReadyKey, documentReadyScript, true);
			}

			//if (!clientScript.IsClientScriptBlockRegistered("RemoveRedundantSpaces"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "RemoveRedundantSpaces" + ID, @"
					function RemoveRedundantSpaces" + ID + @"(str) {
						return str.replace(/(&nbsp;)+|\s+/g, "" "").trim();
					};
				", true);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("closePopup"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "closePopup" + ID, @"
					function closePopup" + ID + @"(b) {
						$find(b).hide();
						return false;
					}
				", true);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("clearPopupMultiline"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "clearPopupMultiline" + ID, @"
					function clearPopupMultiline" + ID + @"(t,h,b,l) {
						document.getElementById(t).value = document.getElementById(h).value;
						var rptTranslations = document.getElementById(l)
						var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
						for (var i = 0; i < rptTranslationValues.length; i++) {
							var translationValue = rptTranslationValues[i];
							if (translationValue.type === ""textarea"" && translationValue.nextSibling.nextSibling.type === ""hidden"") {
								translationValue.value = translationValue.nextSibling.nextSibling.value;
							}
						}
						closePopup" + ID + @"(b);
						return false;
					};
				", true);
			}
		//	if (!clientScript.IsClientScriptBlockRegistered("changeDefaultAndTranslationsMultiline"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "changeDefaultAndTranslationsMultiline" + ID, @"
					function changeDefaultAndTranslationsMultiline" + ID + @"(t,d,h,b,l,ts) {
						var defValue = RemoveRedundantSpaces" + ID + @"(document.getElementById(t).value);
						document.getElementById(t).value = defValue;
						document.getElementById(d).value = defValue;
						document.getElementById(h).value = defValue;
						var rptTranslations = document.getElementById(l)
						var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
						var rptLanguages = document.getElementById(ts)
						var rptTranslationImages = rptLanguages.getElementsByTagName('img');
						var imgCount = 0;
						for (var i = 0; i < rptTranslationValues.length; i++) {
							var translationValue = rptTranslationValues[i];
							if (translationValue.type === ""textarea"" && translationValue.nextSibling.nextSibling.type === ""hidden"") {
								var translationVal = RemoveRedundantSpaces" + ID + @"(translationValue.value);
								translationValue.value = translationVal;
								translationValue.nextSibling.nextSibling.value = translationVal;
								var translationImg = rptTranslationImages[imgCount];
								imgCount = imgCount + 1;
								if (translationValue.value === """") {
									translationImg.style.display = 'inline-block';
								}
								else {
									translationImg.style.display = 'none';
								}
							}
						}
						closePopup" + ID + @"(b);
						return false;
					};
				", true);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("SetDefaultValueMultiline"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "SetDefaultValueMultiline" + ID, @"
					function SetDefaultValueMultiline" + ID + @"(d,t,h) {
						var defValue = RemoveRedundantSpaces" + ID + @"(document.getElementById(d).value);
						document.getElementById(d).value = defValue;
						document.getElementById(t).value = defValue;
						document.getElementById(h).value = defValue;
					};
				", true);
			}
		}

		protected static string GetExclamationVisibility(string value)
		{
			return string.IsNullOrEmpty(value) ? "display:inline-block" : "display:none";
		}

		public Collection<ITranslationGeneric> GetTranslations()
		{
			return GetTranslations(MultiTranslationRepeater, "tbTranslateValueMulti", "hfLangIdMulti");
		}

		private void SetMultiTranslations(Collection<TranslationItem> translationItems)
		{
			MultiLanguageRepeater.DataSource = translationItems;
			MultiLanguageRepeater.DataBind();
			
			MultiTranslationRepeater.DataSource = translationItems;
			MultiTranslationRepeater.DataBind();
		}

		private Collection<TranslationItem> GetTranslationItems()
		{
			Collection<ILanguage> languages = IoC.Resolve<ILanguageSecureService>().GetAll();

			var translationItems = new Collection<TranslationItem>();
			foreach (ILanguage language in languages)
			{
				ILanguage lang = language;
				ITranslationGeneric translation = DataSource.FirstOrDefault(item => item.LanguageId == lang.Id);
				var translationItem = new TranslationItem
				{
					Language = lang,
					Value = (translation != null ? translation.Value : string.Empty)
				};

				translationItems.Add(translationItem);
			}
			return translationItems;
		}
		private Collection<ITranslationGeneric> GetTranslations(Repeater rptTranslations, string tbTranslateValue, string hfLangId)
		{
			var translations = new Collection<ITranslationGeneric>();
			foreach (RepeaterItem item in rptTranslations.Items)
			{
				ITranslationGeneric translation = GetTranslation(item, tbTranslateValue, hfLangId);
				translations.Add(translation);
			}
			return translations;
		}
		private ITranslationGeneric GetTranslation(Control item, string tbTranslateValue, string hfLangId)
		{
			string value;
			int languageId;
			GetControlsValue(item, tbTranslateValue, hfLangId, out value, out languageId);
			var translation = IoC.Resolve<ITranslationGeneric>();
			translation.Id = BusinessObjectId;
			translation.LanguageId = languageId;
			translation.Value = string.IsNullOrEmpty(value) ? null : value;
			return translation;
		}
		private static void GetControlsValue(Control control, string tbTranslateValue, string hfLanguageId, out string value, out int languageId)
		{
			var hiddenField = control.FindControl(hfLanguageId) as HiddenField;
			if (hiddenField == null)
			{
				throw new ArgumentNullException(hfLanguageId);
			}
			languageId = Int32.Parse(hiddenField.Value, CultureInfo.CurrentCulture);

			var tbValue = control.FindControl(tbTranslateValue) as TextBox;
			if (tbValue == null)
			{
				throw new ArgumentNullException(tbTranslateValue);
			}
			value = tbValue.Text;
		}
	}
}
