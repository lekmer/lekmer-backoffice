using System;
using System.Web.UI.WebControls;

namespace Litium.Scensum.BackOffice.UserControls
{
	public class ScensumValidationSummary : ValidationSummary
	{
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			HeaderText = "<h2><img src='" + ResolveUrl("~/Media/Images/SystemMessage/icon-warning.gif") + "'/><span>Invalid input</span></h2>";
		}  
	}
}
