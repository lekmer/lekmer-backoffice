﻿using System.Collections.Generic;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.Lekmer
{
	public class LekmerDefaultProcessor : ILekmerSelectorProcessor
	{
		private int SiteStructureRegistryId { get; set; }

		public LekmerDefaultProcessor(int siteStructureRegistryId)
		{
			SiteStructureRegistryId = siteStructureRegistryId;
		}

		public bool IsAvailable(ISiteStructureNode node)
		{
			return !node.IsMasterPage;
		}

		public IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId)
		{
			return IoC.Resolve<IContentNodeSecureService>().GetTree(selectedId ?? -1 * SiteStructureRegistryId, false);
		}
	}
}