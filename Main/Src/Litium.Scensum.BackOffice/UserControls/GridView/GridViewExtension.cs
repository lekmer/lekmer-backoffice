using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.Web.Controls.Exceptions;

namespace Litium.Scensum.BackOffice.UserControls.GridView
{
	public static class GridViewExtension
	{
		public static IEnumerable<int> GetSelectedIds(this System.Web.UI.WebControls.GridView gridView)
		{
			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = row.FindControl("SelectCheckBox") as CheckBox;
				if (selectCheckBox == null)
				{
					throw new ControlIntegrationException("Selectable grid should have TemplateField column with check box with ID = 'SelectCheckBox'.");
				}

				var hiddenFiled = row.FindControl("IdHiddenField") as HiddenField;
				if (hiddenFiled == null)
				{
					throw new ControlIntegrationException("Selectable grid should have TemplateField column with hidden field with ID = 'Id'.");
				}

				if (!selectCheckBox.Checked)
				{
					continue;
				}

				var id = Convert.ToInt32(hiddenFiled.Value, CultureInfo.CurrentCulture);
				yield return id;
			}
		}

		public static IEnumerable<string> GetSelectedStringIds(this System.Web.UI.WebControls.GridView gridView)
		{
			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = row.FindControl("SelectCheckBox") as CheckBox;
				if (selectCheckBox == null)
				{
					throw new ControlIntegrationException("Selectable grid should have TemplateField column with check box with ID = 'SelectCheckBox'.");
				}

				var hiddenFiled = row.FindControl("IdHiddenField") as HiddenField;
				if (hiddenFiled == null)
				{
					throw new ControlIntegrationException("Selectable grid should have TemplateField column with hidden field with ID = 'Id'.");
				}

				if (!selectCheckBox.Checked)
				{
					continue;
				}

				var id = hiddenFiled.Value;
				yield return id;
			}
		}
	}
}