﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderAuditListControl.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Customer.Order.OrderAuditListControl" %>
	<div class="control-container">
	<div><%= Resources.Customer.Label_OrderAuditHistory%></div>
<asp:Repeater runat="server" ID="AuditListRepeater">
	
	<ItemTemplate>			
		<div class="order-audit-history-item">
				<span class="date"><%#((DateTime)Eval("CreatedDate")).ToString("yyyy-MM-dd HH:mm")%></span><br />
				<span class="author"><%#Eval("AuditType.Title")%></span><br />
		</div>
	</ItemTemplate>
</asp:Repeater>
</div>