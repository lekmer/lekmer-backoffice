﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderAddressEdit.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Customer.Order.OrderAddressEdit" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<div class="order-input-box input-box" style="margin-top:3px; overflow:hidden;">
	<div class="column">
		<asp:Label ID="AddresseeCaptionLabel" runat="server" Text="<%$ Resources:Customer, Label_Addressee %>" />
		<br />
		<asp:Literal Mode="Encode" ID="AddresseeLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>" ></asp:Literal>
		<br />
	
		<asp:Label ID="PhoneCaptionLabel" runat="server" Text="<%$ Resources:Customer, Label_Phone %>" />
		<asp:Literal Mode="Encode" ID="PhoneLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>" ></asp:Literal>
		<br />

		<asp:Label ID="HouseNumberCaptionLabel" runat="server" Text="<%$ Resources:Customer, Label_HouseNumber %>" />
		<asp:Literal Mode="Encode" ID="HouseNumberLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>" ></asp:Literal>
		<br />
		
		<asp:Label ID="ReferenceCaptionLabel" runat="server" Text="<%$ Resources:Lekmer, Order_Reference %>" />:
		<asp:Literal Mode="Encode" ID="ReferenceLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>" ></asp:Literal>
		<br />
	</div>
	<div class="column">
		<asp:Label runat="server" Text="<%$ Resources:Customer, Literal_Address %>" />
		<br />
		<uc:LiteralEncoded ID="StreetAddressLiteral" runat="server" Text="<%$ Resources:Customer, Label_None %>" />
		<br />
		<asp:Literal Mode="Encode" ID="PostalCodeLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>"></asp:Literal>, <asp:Literal Mode="Encode" ID="CityLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>"></asp:Literal>
		<br />
		<asp:Literal Mode="Encode" ID="CountryLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>"></asp:Literal>
		<br />

		<asp:Label ID="HouseExtensionCaptionLabel" runat="server" Text="<%$ Resources:Customer, Label_HouseExtension %>" />
		<asp:Literal Mode="Encode" ID="HouseExtensionLabel" runat="server" Text="<%$ Resources:Customer, Label_None %>" ></asp:Literal>
		<br />
	</div>

	<br clear="all"/>
	<asp:LinkButton runat="server" ID="EditAddressLink" Text="Edit address" CssClass="blue"></asp:LinkButton><br />
</div>

<asp:HiddenField runat="server" ID="FakeControl" />
<div id="AddressViewDiv" runat="server" class="order-address-mainDiv" style="z-index: 10010;
	display: none;">
	<div id="order-address-edit-header">
		<div id="order-address-edit-header-left">
		</div>
		<div id="order-address-edit-header-center">
			<span>
				<%= Resources.Customer.Label_OrderAddress %></span>
			<asp:Button ID="AddressViewCancelButton" runat="server" Text="X" UseSubmitBehavior="false" />
		</div>
		<div id="order-address-edit-header-right">
		</div>
	</div>
	<div id="order-address-subMainDiv">
	<asp:Panel runat="server" DefaultButton="OkButton">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
						runat="server" />
		<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server"
			CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="AddressValidationGroup" />
		<div class="column1">
			<div class="order-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Addressee %></span>
				<asp:RequiredFieldValidator ID="AddresseeValidator" runat="server" ControlToValidate="AddresseeEditTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyAddressee%>" Display="None"
					ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="AddresseeEditTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Address %></span>
				<asp:RequiredFieldValidator ID="AddressValidator" runat="server" ControlToValidate="AddressTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyAddress%>" Display="None"
					ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="AddressTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box input-box-zip">
				<span>
					<%= Resources.Customer.Literal_PostalCode %></span>
				<asp:RequiredFieldValidator ID="ZipValidator" runat="server" ControlToValidate="ZipteTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyZip%>" Display="None" ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="ZipteTextBox" runat="server" CssClass="zip" />
			</div>
			<div class="order-input-box input-box input-box-city">
				<span>
					<%= Resources.Customer.Literal_City %></span>
				<asp:RequiredFieldValidator ID="CityValidator" runat="server" ControlToValidate="CityTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyCity%>" Display="None" ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="CityTextBox" runat="server" CssClass="city" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Country %></span><br />
				<asp:DropDownList runat="server" ID="Country" DataTextField="Title" DataValueField="Id">
				</asp:DropDownList>
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span>
					<%= Resources.Lekmer.Order_Reference %>
				</span>
				<br />
				<asp:TextBox ID="ReferenceTextBox" runat="server" />
			</div>
			<br clear="all" />
		</div>
		<div class="column2">
			<div class="order-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_PhoneNumber %></span>
				<br />
				<asp:TextBox ID="PhoneNumberTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Address2 %></span><br />
				<asp:TextBox ID="Address2TextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span><%= Resources.Customer.Literal_HouseNumber %></span>
				<br />
				<asp:TextBox ID="HouseNumberTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span><%= Resources.Customer.Literal_HouseExtension %></span>
				<br />
				<asp:TextBox ID="HouseExtensionTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="order-input-box input-box">
				<span><%= Resources.Customer.Literal_DoorCode %></span>
				<br />
				<asp:TextBox ID="DoorCodeTextBox" runat="server" />
			</div>
			<br clear="all" />
		</div>
		<br clear="all" />
		<br clear="all" />
		<div class="order-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="OkButton" runat="server" Text="<%$ Resources:General, Button_Ok %>"
				SkinID="DefaultButton" ValidationGroup="AddressValidationGroup" />
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				SkinID="DefaultButton" />
		</div>
		</asp:Panel>
	</div>
	<ajaxToolkit:ModalPopupExtender ID="AddressPopup" runat="server" TargetControlID="FakeControl"
		PopupControlID="AddressViewDiv" CancelControlID="AddressViewCancelButton" BackgroundCssClass="popup-background" />
</div>
