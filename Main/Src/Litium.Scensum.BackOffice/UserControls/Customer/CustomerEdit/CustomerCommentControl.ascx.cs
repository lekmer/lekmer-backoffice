﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	public partial class CustomerCommentControl : StateUserControlController<Collection<ICustomerComment>>
	{
		private int? _customerId;
		public int CustomerId
		{
			set
			{
				ViewState["CustomerId"] = _customerId = value;
			}
			get
			{
				if (_customerId.HasValue)
				{
					return _customerId.Value;
				}
				_customerId = System.Convert.ToInt32(ViewState["CustomerId"], CultureInfo.InvariantCulture);
				return _customerId.Value;
			}
		}
		protected void AddNewCommentButton_Click(object sender, EventArgs e)
		{
			CommentEditTextBox.Text = string.Empty;
			if (IoC.Resolve<ICustomerCommentSecureService>().CanAdd(IoC.Resolve<ICustomerSecureService>().GetById(CustomerId)))
			{
				CommentPopup.Show();
			}
			else
			{
				if (null != CannotAddComment)
					CannotAddComment(this, new EventArgs());
			}
		}

		public event EventHandler CannotAddComment;

		protected override void SetEventHandlers()
		{
			AddNewCommentButon.Click += AddNewCommentButton_Click;
			SaveButton.Click += SaveButton_Click;
			CommentList.ItemDataBound += CommentList_ItemDataBound;
		}

		void CommentList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			
		}
		public Collection<ICustomerComment> GetCustomerComments()
		{
			return State;
		}
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			var commentText = CommentEditTextBox.Text;
			if (commentText.Length > 250)
			{
				var message = string.Format(CultureInfo.InvariantCulture, Resources.Customer.Literal_CommentsTextTooLong, 250, commentText.Length);
				SystemMessageContainer.Add(message);
				CommentPopup.Show();
				return;
			}

			ICustomerComment customerComment = IoC.Resolve<ICustomerCommentSecureService>().Create();
			customerComment.Author = SignInHelper.SignedInSystemUser;
			customerComment.Comment = commentText;
			customerComment.CreationDate = DateTime.Now;
			customerComment.CustomerId = CustomerId;
			if (State.Where(item => item.Id <= 0).Count() > 0)//there already are new comment
			{
				customerComment.Id = State.Min(item => item.Id) - 1;
			}
			State.Add(customerComment);
			CommentList.DataSource = State;
			CommentList.DataBind();
		}

		protected override void PopulateControl()
		{
			CommentList.DataSource = State = IoC.Resolve<ICustomerCommentSecureService>().GetAllByCustomer(CustomerId);
			CommentList.DataBind();
		}
	}
}