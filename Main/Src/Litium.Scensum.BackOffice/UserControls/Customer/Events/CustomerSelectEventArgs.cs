﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Events
{
	public class CustomerSelectEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ICustomer> Customers
		{
			get;
			set;
		}
	}
}
