﻿<%@ Control Language="C#" CodeBehind="UserSearchCriteria.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.User.UserSearchCriteria" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<script type='text/javascript' language='javascript'>
	function ResetSerchForm(d) {
		if (!confirm("Search criterions will be cleared.")) return;
		d = document;
		d.getElementById('<%= tbUserName.ClientID %>').value = '';
		d.getElementById('<%= tbName.ClientID %>').value = '';
		d.getElementById('<%= ddlStatus.ClientID %>').value = '';
		d.getElementById('<%= tbActivationDate.ClientID %>').value = '';
		d.getElementById('<%= tbExpDate.ClientID %>').value = '';
	};
</script>
<asp:Panel ID="panel" runat="server" DefaultButton="btnSearch">
<div style=" float:left;  width:984px; ">
		<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" DisplayMode="List" ForeColor="Black" CssClass="advance-validation-summary" ValidationGroup="vgUserSearch" />
	</div>
	<div id="user-search">
		<div id="user-search-container">
			<label class="assortment-header">
				<asp:Literal runat="server" Text="<%$ Resources:General, Literal_Search %>"/></label>
				<br/>
			<div class="input-box">
				<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_Name%>" /> </span><br />
				<asp:TextBox ID="tbName" runat="server" />
			</div>
			<div class="input-box">
				<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_UserName %>" /></span><br />
				<asp:TextBox ID="tbUserName" runat="server" />
			</div>
			
			<div class="input-box">
				<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_UserStatus %>" /></span><br />
				<asp:DropDownList runat="server" ID="ddlStatus"/>
			</div>
			<div class="input-box-date">
				<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_ActivationDate %>" /></span>
				<asp:CustomValidator ID="cvActivationDate" runat="server" ControlToValidate="tbActivationDate" ErrorMessage="<%$ Resources:GeneralMessage, IncorectDateType %>" Text=" *" ValidationGroup="vgUserSearch" />
				<br />
				<asp:TextBox ID="tbActivationDate" runat="server" />
				&nbsp;
				<asp:ImageButton ID="ibActivationDate" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
				<ajaxToolkit:CalendarExtender ID="ceActivationDate" runat="server" TargetControlID="tbActivationDate" PopupButtonID="ibActivationDate" />
				&nbsp;&nbsp;&nbsp;-&nbsp;
			</div>
			<div class="input-box-date">
				<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_ExpirationDate %>" /></span>
				<asp:CustomValidator ID="cvExpDate" runat="server" ControlToValidate="tbExpDate" ErrorMessage="<%$ Resources:GeneralMessage, IncorectDateType %>" Text=" *" ValidationGroup="vgUserSearch" />
				<asp:CustomValidator ID="cvDateCompare" runat="server" ControlToValidate="tbExpDate" ErrorMessage="<%$ Resources:GeneralMessage, DateCreateFromGreaterCreateTo %>" Text=" *" ValidationGroup="vgUserSearch" />
				<br />
				<asp:TextBox ID="tbExpDate" runat="server" />
				&nbsp;
				<asp:ImageButton ID="ibExpDate" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
				<ajaxToolkit:CalendarExtender ID="ceExpDate" runat="server" TargetControlID="tbExpDate" PopupButtonID="ibExpDate" />
			</div>
		</div>
		<br />
		<div class="buttons-container">
			<uc:ImageLinkButton ID="btnSearch" runat="server" Text="<%$ Resources:General, Button_Search %>" ValidationGroup="vgUserSearch"
				SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="btnReset" runat="server" Text="<%$ Resources:General, Button_ClearAll %>" OnClientClick='ResetSerchForm();'
				SkinID="DefaultButton" />
			
		</div>
		
		<div class="product-horizontal-separator">
		</div>
		<br />
			<label class="assortment-header">
				<asp:Literal runat="server" Text="<%$ Resources:General, Literal_SearchResults %>"/></label>
	</div>

</asp:Panel>