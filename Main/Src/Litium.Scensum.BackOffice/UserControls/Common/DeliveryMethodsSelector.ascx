﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeliveryMethodsSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Common.DeliveryMethodsSelector" %>

<div class="delivery-methods-selector-wrapper">
	<asp:Repeater runat="server" ID="DeliveryMethodsRepeater">
		<HeaderTemplate>
			<div class="delivery-methods-header-wrapper">
				<span><%=Resources.Campaign.Literal_DeliveryMethodSelector%></span>
			</div>
		</HeaderTemplate>
		<ItemTemplate>
			<div>
				<asp:HiddenField runat="server" ID="DeliveryMethodIdHidden" Value='<%# Eval("Id") %>' />
				<asp:CheckBox runat="server" ID="DeliveryMethodCheckbox" Checked='<%# Eval("IsSelected") %>' Text='<%# Eval("Title") %>' />
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>