﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.UserControls.Common
{
	public class CampaignCurrencyValueHelper
	{
		public string GetChannelsCountryIso(IEnumerable<IChannel> channels, Collection<ICountry> countries, int currencyId)
		{
			string channelsCountryIso = string.Empty;
			var currencyChannels = channels.Where(c => c.Currency.Id == currencyId);
			foreach (var currencyChannel in currencyChannels)
			{
				var country = countries.FirstOrDefault(c => c.Id == currencyChannel.Country.Id);
				if (country != null)
				{
					channelsCountryIso = channelsCountryIso + country.Iso + " / ";
				}
			}

			var currencyChannelsIso = string.IsNullOrEmpty(channelsCountryIso)
				? string.Empty
				: string.Format(" ({0})", channelsCountryIso.Substring(0, channelsCountryIso.Length - 3));

			return currencyChannelsIso;
		}
	}
}