﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartContainsCondition;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Lekmer.Campaign;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Helper
{
	public class CampaignConfiguratorHelper
	{
		public readonly string SCRIPT = "$(function() {" +
									"ResizePopup('ActionPopupDiv', 0.8, 0.8, 0.06);" +
									"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
									"ResizePopup('ProductExcludePopupDiv', 0.8, 0.7, 0.07);" +
									"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('BrandIncludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('BrandExcludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('SupplierIncludePopupDiv', 0.4, 0.45, 0.09);" +
									"});";
		public readonly string DOWN_IMG = "~/Media/Images/Common/down.gif";
		public readonly string UP_IMG = "~/Media/Images/Common/up.gif";

		public Collection<int> GetSelectedIdsFromGrid(System.Web.UI.WebControls.GridView gridView, string idHiddenFieldName)
		{
			var ids = new Collection<int>();

			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					int id = Convert.ToInt32(((HiddenField)row.FindControl(idHiddenFieldName)).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}

			return ids;
		}

		public void ClearSelectedIdsFromGrid(System.Web.UI.WebControls.GridView gridView)
		{
			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox != null)
				{
					selectCheckBox.Checked = false;
				}
			}

			GridViewRow headerRow = gridView.HeaderRow;
			if (headerRow != null)
			{
				var selectAllCheckBox = (CheckBox)headerRow.FindControl("SelectAllCheckBox");
				if (selectAllCheckBox != null)
				{
					selectAllCheckBox.Checked = false;
				}
			}
		}

		public bool HasAnySelectedItem(System.Web.UI.WebControls.GridView grid)
		{
			foreach (GridViewRow row in grid.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					return true;
				}
			}

			return false;
		}

		public void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row, HtmlGenericControl applyAllDiv, bool isVoucherControl)
		{
			string scriptShowBlockOption = applyAllDiv != null ? @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');" : string.Empty;

			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", @"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');" + scriptShowBlockOption);
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", @"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');" + scriptShowBlockOption);

				if (isVoucherControl)
				{
					var batchLiteral = (Label)row.FindControl("BatchIdLiteral");
					batchLiteral.Text = ((int)row.DataItem).ToString(CultureInfo.CurrentCulture);

					var batchIdHiddenField = (HiddenField)row.FindControl("BatchIdHiddenField");
					batchIdHiddenField.Value = ((int)row.DataItem).ToString(CultureInfo.CurrentCulture);
				}
			}
		}

		public Collection<ICategory> ResolveCategories(CategoryIdDictionary categoryIds)
		{
			var categories = new Collection<ICategory>();
			foreach (var id in categoryIds)
			{
				var category = IoC.Resolve<ICategorySecureService>().GetById(id.Value);
				if (category == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Category with ID {0} could not be found.",
						id));
				}
				categories.Add(category);
			}
			return categories;
		}

		public Collection<ICategory> ResolveCategories(CategoryDictionary categoryIds)
		{
			return ResolveCategories(new CategoryIdDictionary(categoryIds.Keys));
		}

		public Collection<ICategory> ResolveCategories(CampaignCategoryDictionary categoryIds)
		{
			return ResolveCategories(new CategoryIdDictionary(categoryIds.Keys));
		}

		public Collection<IBrand> ResolveBrands(BrandIdDictionary brandIds)
		{
			var brands = new Collection<IBrand>();

			foreach (var id in brandIds)
			{
				var brand = IoC.Resolve<IBrandSecureService>().GetById(id.Value);
				if (brand == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Brand with ID {0} could not be found.",
						id.Value));
				}
				brands.Add(brand);
			}

			return brands;
		}

		public Collection<ISupplier> ResolveSuppliers(IdDictionary supplierIds)
		{
			var suppliers = new Collection<ISupplier>();

			foreach (var id in supplierIds)
			{
				var supplier = IoC.Resolve<ISupplierSecureService>().GetById(id.Value);
				if (supplier == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Supplier with ID {0} could not be found.",
						id.Value));
				}
				suppliers.Add(supplier);
			}

			return suppliers;
		}
	}
}