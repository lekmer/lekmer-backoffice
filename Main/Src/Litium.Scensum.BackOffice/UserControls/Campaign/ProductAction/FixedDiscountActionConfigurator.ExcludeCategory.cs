using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class FixedDiscountActionConfigurator
	{
		private void SetExcludeCategoryEventHandlers()
		{
			CategoryExcludeGrid.RowDataBound += OnCategoryExcludeGridRowDataBound;
			CategoryExcludeGrid.RowCommand += OnCategoryExcludeGridRowCommand;
			CategoryExcludePopupOkButton.Click += OnExcludeCategories;
			RemoveSelectionFromCategoryExcludeGridButton.Click += OnRemoveSelectionFromCategoryExcludeGrid;
		}

		private void DataBindExcludeCategories(IFixedDiscountAction action)
		{
			CategoryExcludePopupCategoryTree.Selector = CategorySelector;
			foreach (int value in action.ExcludeCategories.Values)
			{
				CategoryExcludePopupCategoryTree.SelectedIds.Add(value);
			}
			CategoryExcludePopupCategoryTree.DataBind();
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(action.ExcludeCategories));
		}

		protected virtual void OnCategoryExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CategoryExcludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnCategoryExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				RemoveExcludedCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveExcludedCategory(int productId)
		{
			State.ExcludeCategories.Remove(productId);
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
		}

		protected virtual void OnExcludeCategories(object sender, EventArgs e)
		{
			foreach (var categoryId in CategoryExcludePopupCategoryTree.SelectedIds)
			{
				if (!State.ExcludeCategories.ContainsKey(categoryId))
				{
					State.ExcludeCategories.Add(categoryId);
				}
			}
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
		}

		protected virtual void OnRemoveSelectionFromCategoryExcludeGrid(object sender, EventArgs e)
		{
			foreach (var categoryId in GetSelectedCategoriesFromExcludeGrid())
			{
				State.ExcludeCategories.Remove(categoryId);
			}
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCategoriesFromExcludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CategoryExcludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindCategoryExcludeGrid(Collection<ICategory> categories)
		{
			CategoryExcludeGrid.DataSource = categories;
			CategoryExcludeGrid.DataBind();
			CategoryExcludeGridUpdatePanel.Update();
		}
	}
}