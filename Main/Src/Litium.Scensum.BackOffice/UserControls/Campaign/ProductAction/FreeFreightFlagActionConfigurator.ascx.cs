﻿using System;
using System.Collections.ObjectModel;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class FreeFreightFlagActionConfigurator : ProductActionControl<IFreeFreightFlagAction>
	{
		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
		}

		public override void DataBind(IProductAction dataSource)
		{
			var action = (IFreeFreightFlagAction)dataSource;

			ProductPricesManager.DataSource = action.ProductPrices;
			ProductPricesManager.DataBind();

			State = action;
		}

		public override bool TryGet(out IProductAction entity)
		{
			if (IsValid())
			{
				var action = State;
				action.ProductPrices = ProductPricesManager.CollectCurrencyValues();
				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected bool IsValid()
		{
			bool isValid = true;

			Collection<string> validationMessages;
			if (!ProductPricesManager.IsValid(out validationMessages))
			{
				isValid = false;
				foreach (string validationMessage in validationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			const string script = "$(function() {" +
				"var popupDiv = $(\"div[id$='ActionPopupDiv']\");" +
				"popupDiv.attr('class','campaign-popup-container small-campaign-popup-container campaign-action-free-freight-flag');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-header-center']:first\").attr('class','campaign-popup-header-center small-campaign-popup-header-center');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-content']:first\").attr('class','campaign-popup-content small-campaign-popup-content');" +
				"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_fvac_smallPopup", script, true);
		}
	}
}