using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class PercentagePriceDiscountActionConfigurator
	{
		private void SetExcludeBrandEventHandlers()
		{
			BrandExcludeGrid.RowDataBound += OnBrandExcludeGridRowDataBound;
			BrandExcludeGrid.RowCommand += OnBrandExcludeGridRowCommand;

			BrandExcludeSearchGrid.RowDataBound += OnBrandExcludeSearchGridRowDataBound;

			BrandExcludePopupOkButton.Click += OnExcludeBrands;
			RemoveSelectionFromBrandExcludeGridButton.Click += OnRemoveSelectionFromBrandExcludeGrid;
		}

		private void DataBindExcludeBrands(ILekmerPercentagePriceDiscountAction action)
		{
			DataBindBrandExcludeSearchGrid();
			DataBindBrandExcludeGrid(CampaignConfiguratorHelper.ResolveBrands(action.ExcludeBrands));
		}

		protected virtual void OnBrandExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, BrandExcludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnBrandExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveBrand"))
			{
				RemoveExcludedBrand(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OnBrandExcludeSearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, null, false);
		}

		protected virtual void RemoveExcludedBrand(int brandId)
		{
			State.ExcludeBrands.Remove(brandId);

			DataBindBrandExcludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.ExcludeBrands));
		}

		protected virtual void OnExcludeBrands(object sender, EventArgs e)
		{
			var brandIds = CampaignConfiguratorHelper.GetSelectedIdsFromGrid(BrandExcludeSearchGrid, "IdHiddenField");

			foreach (var brandId in brandIds)
			{
				if (!State.ExcludeBrands.ContainsKey(brandId))
				{
					State.ExcludeBrands.Add(brandId);
				}
			}

			DataBindBrandExcludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.ExcludeBrands));

			CampaignConfiguratorHelper.ClearSelectedIdsFromGrid(BrandExcludeSearchGrid);
		}

		protected virtual void OnRemoveSelectionFromBrandExcludeGrid(object sender, EventArgs e)
		{
			foreach (var brandId in GetSelectedBrandsFromExcludeGrid())
			{
				State.ExcludeBrands.Remove(brandId);
			}

			DataBindBrandExcludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.ExcludeBrands));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedBrandsFromExcludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(BrandExcludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindBrandExcludeGrid(Collection<IBrand> brands)
		{
			BrandExcludeGrid.DataSource = brands;
			BrandExcludeGrid.DataBind();

			BrandExcludeGridUpdatePanel.Update();
		}

		protected virtual void DataBindBrandExcludeSearchGrid()
		{
			BrandExcludeSearchGrid.DataSource = BrandCollection;
			BrandExcludeSearchGrid.DataBind();

			BrandExcludePopupUpdatePanel.Update();
		}
	}
}