﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="FixedCartDiscountActionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.FixedCartDiscountActionConfigurator" %>

<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>

<br style="clear:both;" />
<scensum:CurrencyValueManager runat="server" id="AmountsManager" />