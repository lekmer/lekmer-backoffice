﻿using System;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class VoucherDiscountActionConfigurator :
		CartActionControl<IVoucherDiscountAction>
	{
		public override void DataBind(ICartAction dataSource)
		{
			var voucherDiscountAction = (IVoucherDiscountAction)dataSource;
			State = voucherDiscountAction;

			//FixedRadioButton.Checked = State.FixedDiscount;
			//PercentageRadioButton.Checked = State.PercentageDiscount;
			//DiscountValueTextBox.Text = State.DiscountValue.HasValue ? State.DiscountValue.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
			//AmountsManager.DataSource = voucherDiscountAction.Amounts;
			//AmountsManager.DataBind();

			AllowSpecialOfferCheckBox.Checked = State.AllowSpecialOffer;
		}

		public override bool TryGet(out ICartAction entity)
		{
			//if (IsValid())
			//{
			//    State.FixedDiscount = FixedRadioButton.Checked;
			//    State.PercentageDiscount = PercentageRadioButton.Checked;
			//    if (FixedRadioButton.Checked)
			//    {
			//        State.Amounts = AmountsManager.CollectCurrencyValues();
			//        State.DiscountValue = null;
			//    }
			//    else
			//    {
			//        State.Amounts = new LekmerCurrencyValueDictionary();
			//        State.DiscountValue = decimal.Parse(DiscountValueTextBox.Text.Trim());
			//    }
			//    entity = State;
			//    entity.Status = BusinessObjectStatus.Edited;
			//    return true;
			//}
			//entity = null;
			//return false;

			State.AllowSpecialOffer = AllowSpecialOfferCheckBox.Checked;

			entity = State;
			entity.Status = BusinessObjectStatus.Edited;
			return true;
		}

		//protected virtual bool IsValid()
		//{
		//    bool isValid = true;

		//    if (FixedRadioButton.Checked && PercentageRadioButton.Checked)
		//    {
		//        ValidationMessages.Add(Resources.LekmerMessage.Campaigns_FixedAndPercentageChecked);
		//        isValid = false;
		//    }

		//    if (!FixedRadioButton.Checked && !PercentageRadioButton.Checked)
		//    {
		//        ValidationMessages.Add(Resources.LekmerMessage.Campaigns_FixedAndPercentageChecked);
		//        isValid = false;
		//    }

		//    if (PercentageRadioButton.Checked)
		//    {
		//        decimal discountValue;
		//        if (DiscountValueTextBox.Text.Trim().Length == 0)
		//        {
		//            ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.DefaultPriceFromValueShouldNotBeEmpty));
		//            isValid = false;
		//        }
		//        if (!decimal.TryParse(DiscountValueTextBox.Text.Trim(), out discountValue))
		//        {
		//            ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.DiscountValueShouldBeDecimal));
		//            isValid = false;
		//        }
		//    }

		//    if (FixedRadioButton.Checked)
		//    {
		//        Collection<string> amountsValidationMessages;
		//        if (!AmountsManager.IsValid(out amountsValidationMessages))
		//        {
		//            isValid = false;
		//            foreach (string validationMessage in amountsValidationMessages)
		//            {
		//                ValidationMessages.Add(validationMessage);
		//            }
		//        }
		//    }
		//    return isValid;
		//}

		protected override void PopulateControl()
		{
		}

		protected override void SetEventHandlers()
		{
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			//const string script = "$(function() {" +
			//    "var popupDiv = $(\"div[id$='ActionPopupDiv']\");" +
			//    "popupDiv.attr('class','campaign-popup-container small-campaign-popup-container campaign-action-voucher');" +
			//    "$(popupDiv).find(\"div[class*='campaign-popup-header-center']:first\").attr('class','campaign-popup-header-center small-campaign-popup-header-center');" +
			//    "$(popupDiv).find(\"div[class*='campaign-popup-content']:first\").attr('class','campaign-popup-content small-campaign-popup-content');" +
			//    "});";
			//ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_fvac_smallPopup", script, true);

			const string script = "$(function() {ResizePopup('ActionPopupDiv', 0.4, 0.25, 0.19);});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_vdac_popupResize", script, true);
		}
	}
}