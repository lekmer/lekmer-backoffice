﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherDiscountActionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.VoucherDiscountActionConfigurator" %>
<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>

<br style="clear:both;" />
<div class="column">
	<%--<asp:RadioButton ID="PercentageRadioButton" runat="server" GroupName="ValueType" />
	<b><span><%= Resources.Lekmer.Literal_PercentageType%></span></b>
	<br />
	<br />
	<span><%= Resources.Lekmer.Literal_DiscountValue%></span>
	<br />
	<asp:TextBox ID="DiscountValueTextBox" runat="server" />
	<br />
	<br />
	<asp:RadioButton ID="FixedRadioButton" GroupName="ValueType" runat="server" />
	<b><span><%= Resources.Lekmer.Literal_FixedType%></span></b>
	<br />
	<scensum:CurrencyValueManager ID="AmountsManager" runat="server" />--%>
	<p style="color: red;">Discount value will get from the Voucher that will be used.</p>
	<br />
	<asp:CheckBox ID="AllowSpecialOfferCheckBox" runat="server" />
	<b><span><%= Resources.Lekmer.Literal_AllowSpecialOffer%></span></b>
</div>
