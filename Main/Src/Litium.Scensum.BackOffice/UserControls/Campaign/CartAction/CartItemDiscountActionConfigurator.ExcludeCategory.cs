﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemDiscountActionConfigurator
	{
		private void SetExcludeCategoryEventHandlers()
		{
			CategoryExcludeGrid.RowDataBound += OnCategoryExcludeGridRowDataBound;
			CategoryExcludeGrid.RowCommand += OnCategoryExcludeGridRowCommand;
			CategoryExcludePopupOkButton.Click += OnExcludeCategories;
			RemoveSelectionFromCategoryExcludeGridButton.Click += OnRemoveSelectionFromCategoryExcludeGrid;
		}

		private void DataBindExcludeCategories(ICartItemDiscountCartAction action)
		{
			CategoryExcludePopupCategoryTree.Selector = CategorySelector;
			CategoryExcludePopupCategoryTree.DataBind();
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(action.ExcludeCategories));
		}

		protected virtual void OnCategoryExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CategoryExcludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnCategoryExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				RemoveExcludedCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveExcludedCategory(int productId)
		{
			State.ExcludeCategories.Remove(productId);
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
		}

		protected virtual void OnExcludeCategories(object sender, EventArgs e)
		{
			foreach (var categoryId in CategoryExcludePopupCategoryTree.SelectedIds)
			{
				if (!State.ExcludeCategories.ContainsKey(categoryId))
				{
					State.ExcludeCategories.Add(categoryId, false);
				}
			}
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
			CategoryExcludePopupCategoryTree.SelectedIds.Clear();
			CategoryExcludePopupCategoryTree.DataBind();
		}

		protected virtual void OnRemoveSelectionFromCategoryExcludeGrid(object sender, EventArgs e)
		{
			foreach (var categoryId in GetSelectedCategoriesFromExcludeGrid())
			{
				State.ExcludeCategories.Remove(categoryId);
			}
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCategoriesFromExcludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CategoryExcludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindCategoryExcludeGrid(Collection<ICategory> categories)
		{
			CategoryExcludeGrid.DataSource = categories;
			CategoryExcludeGrid.DataBind();
			CategoryExcludeGridUpdatePanel.Update();
		}

		protected virtual bool GetExcludeSubcategories(object id)
		{
			var categoryId = Convert.ToInt32(id, CultureInfo.CurrentCulture);
			bool include;
			return State.ExcludeCategories.TryGetValue(categoryId, out include) && include;
		}

		protected virtual void OnExcludeSubcategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox)sender;
			var row = (GridViewRow)checkBox.Parent.Parent;
			var idHidden = (HiddenField)row.FindControl("IdHiddenField");
			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);
			bool include;
			if (!State.ExcludeCategories.TryGetValue(categoryId, out include)) return;
			State.ExcludeCategories[categoryId] = checkBox.Checked;
			DataBindCategoryExcludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.ExcludeCategories));
		}
	}
}