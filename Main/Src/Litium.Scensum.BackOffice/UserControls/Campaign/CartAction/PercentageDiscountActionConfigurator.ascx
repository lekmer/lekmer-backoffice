﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="PercentageDiscountActionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.PercentageDiscountActionConfigurator" %>

<br style="clear:both;" />

<div class="column">
	<span class="bold">
		<%= Resources.Lekmer.Literal_PercentageType%>
	</span>

	<br /><br />

	<span><%= Resources.Lekmer.Literal_DiscountValue%>* </span>
	<br />
	<asp:TextBox ID="DiscountValueTextBox"  runat="server" />
</div>