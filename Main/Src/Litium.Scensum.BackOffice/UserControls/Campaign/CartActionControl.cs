using System;
using System.Collections.ObjectModel;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
    public abstract class CartActionControl<TCartAction> : StateUserControlController<TCartAction>, ICampaignPluginControl<ICartAction>
	{
		private Collection<string> _validationMessages;
		public Collection<string> ValidationMessages
		{
			get
			{
				if (_validationMessages == null)
				{
					_validationMessages = new Collection<string>();
				}
				return _validationMessages;
			}
		}

        public string AdditionalValidationMessage
        {
            get { return string.Empty; }
        }

        public abstract void DataBind(ICartAction dataSource);
        public abstract bool TryGet(out ICartAction entity);
    }
}