﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="CustomerGroupConditionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.Condition.CustomerGroupConditionConfigurator" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<script type="text/javascript">
	$(function () {
		$("div[id$='ConditionPopupDiv']").attr('class', 'campaign-popup-container');
		ResizePopup('ConditionPopupDiv', 0.75, 0.5, 0.095);
		$("div[class^='campaign-popup-content']").css('overflow-y', 'scroll');
	});

	function confirmDeleteCustomerGroups() {
		return DeleteConfirmation("<%= Resources.Campaign.Literal_CustomerGroupsConfirmRemove %>");
	}
</script>

<div class="campaign-full-left">

<!-- Include Toolbar Start -->

	<div id="includeToolbar">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenIncludeCustomerGroupSearchPopupButton" Text="<%$Resources:Product, Literal_CustomerGroups%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content">
			<asp:Panel ID="IncludePanel" runat="server">
				<div style="padding-top:25px;clear:both;"></div>
				<asp:Label Text="<%$Resources:Product, Literal_CustomerGroups%>" runat="server" CssClass="text-bold" />
				<br />
				<asp:UpdatePanel ID="CustomerGroupIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="CustomerGroupIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCustomerGroup" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmCustomerGroupRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="CustomerGroupIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left; padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCustomerGroupIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteCustomerGroups();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>

<!-- Include Toolbar End -->

	<br class="clear"/>
	<br />

<!-- Exclude Toolbar Start -->

	<div id="excludeToolbar">
		<div class="collapsible-items left">
			<div id="ExcludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="ExcludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/exclude.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Exclude%></span>
				</div>
				<div id="ExcludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenExcludeCustomerGroupSearchPopupButton" Text="<%$Resources:Product, Literal_CustomerGroups%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ExcludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="ExcludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<asp:UpdatePanel ID="ExcludeUpdatePanel" UpdateMode="Always" runat="server">
			<ContentTemplate>
				<div id="ExcludePanelDiv" runat="server" class="campaign-include-content">
					<asp:Panel ID="ExcludePanel" runat="server">
						<div style="padding-top:25px;clear:both;"></div>
						<asp:Label Text="<%$Resources:Product, Literal_CustomerGroups%>" runat="server" CssClass="text-bold" />
						<br />
						<asp:UpdatePanel ID="CustomerGroupExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
							<ContentTemplate>
								<asp:GridView ID="CustomerGroupExcludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<HeaderTemplate>
												<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox ID="SelectCheckBox" runat="server" />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>

										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>' />
											</ItemTemplate>
										</asp:TemplateField>

										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCustomerGroup" CommandArgument='<%# Eval("Id") %>'
													ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
													OnClientClick="return ConfirmCustomerGroupRemove();" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
								<div runat="server" id="CustomerGroupExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
									<div style="float:left;padding-top:6px;">
										<div class="apply-to-all">
											<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
										</div>
									</div>
									<div style="float:left">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCustomerGroupExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteCustomerGroups();" runat="server" SkinID="DefaultButton"/>
									</div>
								</div>
							</ContentTemplate>
						</asp:UpdatePanel>
					</asp:Panel>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>

<!-- Exclude Toolbar End -->

</div>

<br class="clear"/>
<br />

<!-- CustomerGroup Search Popup Start -->
	<!-- Include Start -->

<div id="CustomerGroupIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="CustomerGroupIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable" style="padding: 10px">
		<asp:UpdatePanel ID="CustomerGroupIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<asp:GridView ID="CustomerGroupIncludeSearchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server" />
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>

						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CustomerGroupIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CustomerGroupIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="CustomerGroupIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeCustomerGroupSearchPopupButton"
	PopupControlID="CustomerGroupIncludePopupDiv" 
	CancelControlID="CustomerGroupIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />

	<!-- Include End -->

	<!-- Exclude Start -->

<div id="CustomerGroupExcludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="CustomerGroupExcludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable" style="padding: 10px">
		<asp:UpdatePanel ID="CustomerGroupExcludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<asp:GridView ID="CustomerGroupExcludeSearchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server" />
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>

						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CustomerGroupExcludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CustomerGroupExcludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="CustomerGroupExcludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenExcludeCustomerGroupSearchPopupButton"
	PopupControlID="CustomerGroupExcludePopupDiv" 
	CancelControlID="CustomerGroupExcludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />

	<!-- Exclude End -->
<!-- CustomerGroup Search Popup End -->