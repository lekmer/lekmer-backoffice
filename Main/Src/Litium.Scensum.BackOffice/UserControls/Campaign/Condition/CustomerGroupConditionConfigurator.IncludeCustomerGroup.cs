using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CustomerGroupConditionConfigurator
	{
		private void SetIncludeCustomerGroupEventHandlers()
		{
			CustomerGroupIncludeGrid.RowDataBound += OnCustomerGroupIncludeGridRowDataBound;
			CustomerGroupIncludeGrid.RowCommand += OnCustomerGroupIncludeGridRowCommand;

			CustomerGroupIncludeSearchGrid.RowDataBound += OnCustomerGroupIncludeSearchGridRowDataBound;

			CustomerGroupIncludePopupOkButton.Click += OnIncludeCustomerGroups;
			RemoveSelectionFromCustomerGroupIncludeGridButton.Click += OnRemoveSelectionFromCustomerGroupIncludeGrid;
		}

		private void DataBindIncludeCustomerGroups(ICustomerGroupCondition condition)
		{
			DataBindCustomerGroupIncludeSearchGrid();
			DataBindCustomerGroupIncludeGrid(ResolveCustomerGroups(condition.IncludeCustomerGroups));
		}

		protected virtual void OnCustomerGroupIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CustomerGroupIncludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnCustomerGroupIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCustomerGroup"))
			{
				RemoveIncludedCustomerGroup(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void  OnCustomerGroupIncludeSearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, null, false);
		}

		protected virtual void RemoveIncludedCustomerGroup(int customerGroupId)
		{
			State.IncludeCustomerGroups.Remove(customerGroupId);

			DataBindCustomerGroupIncludeGrid(ResolveCustomerGroups(State.IncludeCustomerGroups));
		}

		protected virtual void OnIncludeCustomerGroups(object sender, EventArgs e)
		{
			var customerGroupIds = CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CustomerGroupIncludeSearchGrid, "IdHiddenField");

			foreach (var customerGroupId in customerGroupIds)
			{
				if (!State.IncludeCustomerGroups.ContainsKey(customerGroupId))
				{
					State.IncludeCustomerGroups.Add(customerGroupId);
				}
			}

			DataBindCustomerGroupIncludeGrid(ResolveCustomerGroups(State.IncludeCustomerGroups));

			CampaignConfiguratorHelper.ClearSelectedIdsFromGrid(CustomerGroupIncludeSearchGrid);
		}

		protected virtual void OnRemoveSelectionFromCustomerGroupIncludeGrid(object sender, EventArgs e)
		{
			foreach (var customerGroupId in GetSelectedCustomerGroupsFromIncludeGrid())
			{
				State.IncludeCustomerGroups.Remove(customerGroupId);
			}

			DataBindCustomerGroupIncludeGrid(ResolveCustomerGroups(State.IncludeCustomerGroups));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCustomerGroupsFromIncludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CustomerGroupIncludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindCustomerGroupIncludeGrid(Collection<ICustomerGroup> customerGroups)
		{
			CustomerGroupIncludeGrid.DataSource = customerGroups;
			CustomerGroupIncludeGrid.DataBind();
			CustomerGroupIncludeGridUpdatePanel.Update();
		}
		
		protected virtual void DataBindCustomerGroupIncludeSearchGrid()
		{
			CustomerGroupIncludeSearchGrid.DataSource = CustomerGroupCollection;
			CustomerGroupIncludeSearchGrid.DataBind();
			CustomerGroupIncludePopupUpdatePanel.Update();
		}
	}
}