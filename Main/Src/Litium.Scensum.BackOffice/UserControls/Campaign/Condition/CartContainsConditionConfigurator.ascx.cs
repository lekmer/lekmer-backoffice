﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartContainsCondition;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CartContainsConditionConfigurator : ConditionControl<ICartContainsCondition>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		private BrandCollection _brandCollection;
		public BrandCollection BrandCollection
		{
			get { return _brandCollection ?? (_brandCollection = IoC.Resolve<IBrandSecureService>().GetAll()); }
		}

		protected override void SetEventHandlers()
		{
			SetIncludeProductEventHandlers();
			SetExcludeProductEventHandlers();

			SetIncludeCategoryEventHandlers();
			SetExcludeCategoryEventHandlers();

			SetIncludeBrandEventHandlers();
			SetExcludeBrandEventHandlers();

			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		protected override void PopulateControl() { }

		public override void DataBind(ICondition dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var condition = (ICartContainsCondition)dataSource;
			State = condition;

			MinQuantityTextBox.Text = condition.Id > 0 || condition.MinQuantity > 0 ? condition.MinQuantity.ToString(CultureInfo.CurrentCulture) : "1";

			AllowDuplicatesCheckBox.Checked = condition.AllowDuplicates;
			IncludeAllProductsCheckbox.Checked = condition.IncludeAllProducts;

			DataBindIncludeProducts(condition);
			DataBindExcludeProducts(condition);

			DataBindIncludeCategories(condition);
			DataBindExcludeCategories(condition);

			DataBindIncludeBrands(condition);
			DataBindExcludeBrands(condition);

			TargetProductTypeSelectorControl.DataSource = condition.TargetProductTypes;
			TargetProductTypeSelectorControl.DataBind();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				DataBindProductIncludeGrid(new ProductIdDictionary());
				DataBindCategoryIncludeGrid(new Collection<ICategory>());
				DataBindBrandIncludeGrid(new Collection<IBrand>());
			}
			else
			{
				DataBindProductIncludeGrid(State.IncludeProducts);
				DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
				DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.IncludeBrands));
			}
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			DataBindProductIncludeGrid(State.IncludeProducts);

			ProductExcludeGrid.PageIndex = 0;
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		public override bool TryGet(out ICondition entity)
		{
			if (IsValid())
			{
				if (IncludeAllProductsCheckbox.Checked)
				{
					State.IncludeProducts.Clear();
					State.IncludeCategories.Clear();
					State.IncludeBrands.Clear();
				}

				var condition = State;
				condition.MinQuantity = int.Parse(MinQuantityTextBox.Text, CultureInfo.CurrentCulture);
				condition.AllowDuplicates = AllowDuplicatesCheckBox.Checked;
				condition.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;
				condition.TargetProductTypes = TargetProductTypeSelectorControl.CollectSelectedValues();

				entity = condition;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;
			int minQuantity;
			if (!int.TryParse(MinQuantityTextBox.Text, out minQuantity) || minQuantity <= 0)
			{
				isValid = false;
				ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.CartContainsCondition.Message_MinQuantityInvalid, 1, int.MaxValue));
			}

			Collection<string> targetProductTypesValidationMessages;
			if (!TargetProductTypeSelectorControl.IsValid(out targetProductTypesValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in targetProductTypesValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmBrandRemove", "function ConfirmBrandRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteBrand + "');}", true);

			const string script = "$(function() {" +
									"ResizePopup('ProductIncludePopupDiv', 0.8, 0.6, 0.07);" +
									"ResizePopup('ProductExcludePopupDiv', 0.8, 0.6, 0.07);" +
									"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('BrandIncludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('BrandExcludePopupDiv', 0.4, 0.45, 0.09);" +
									"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);

			string targetCollapsiblescript = "OpenClose('" + TargetPanelDiv.ClientID + "','" + TargetPanelStateHidden.ClientID + "','" + TargetPanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			TargetCollapsibleDiv.Attributes.Add("onclick", targetCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			BrandIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandIncludeGrid) ? "block" : "none";

			ProductExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			BrandExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandExcludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0 || BrandIncludeGrid.Rows.Count > 0 ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 || BrandExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}
	}
}