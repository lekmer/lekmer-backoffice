﻿using System;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class VoucherConditionConfigurator : ConditionControl<IVoucherCondition>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get
			{
				if (_campaignConfiguratorHelper == null)
				{
					_campaignConfiguratorHelper = new CampaignConfiguratorHelper();
				}

				return _campaignConfiguratorHelper;
			}
		}

		protected override void SetEventHandlers()
		{
			SetEventHandlersInclude();
			SetEventHandlersExclude();
		}

		protected override void PopulateControl()
		{
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterCollapsibleIncludePanelScripts();
			RegisterCollapsibleExcludePanelScripts();

			SetVisibilityInclude();
			SetVisibilityExclude();
		}

		public override void DataBind(ICondition dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var condition = (IVoucherCondition)dataSource;

			IncludeAllBatchesCheckbox.Checked = condition.IncludeAllBatchIds;
			DataBindBatchIncludeGrid(condition.BatchIdsInclude);
			DataBindBatchExcludeGrid(condition.BatchIdsExclude);

			State = condition;
		}

		public override bool TryGet(out ICondition entity)
		{
			if (IncludeAllBatchesCheckbox.Checked)
			{
				State.BatchIdsInclude.Clear();
			}

			var condition = State;
			condition.IncludeAllBatchIds = IncludeAllBatchesCheckbox.Checked;

			entity = condition;
			return true;
		}
	}
}