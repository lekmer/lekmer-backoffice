﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignGeneral.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CampaignGeneral" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ToolTip" Src="~/UserControls/ToolTip/ToolTip.ascx" %>

<div class="campaign-general">
	<div class="campaign-general-row">
		<div class="input-box  label-row campaign-title">
			<asp:Label runat="server" AssociatedControlID="TitleTextBox"><%=Resources.General.Literal_Title%> *</asp:Label>
			<asp:TextBox ID="TitleTextBox" runat="server"></asp:TextBox>
		</div>
		<div class="input-box label-row">
			<asp:Label runat="server" AssociatedControlID="StatusList"><%=Resources.General.Literal_Status%> *</asp:Label>
			<asp:DropDownList ID="StatusList" runat="server"></asp:DropDownList>
		</div>
		<div class="input-box">
			<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
			<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
		</div>
	</div>
	<br />
	<div class="campaign-general-row">
		<div class="input-box label-row">
			<asp:Label runat="server" AssociatedControlID="StartDateTextBox"><%=Resources.General.Literal_StartDate%></asp:Label>
			<asp:TextBox ID="StartDateTextBox" runat="server"></asp:TextBox>
			<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle"/>
			<ajaxToolkit:CalendarExtender ID="StartDateCalendarControl" runat="server" TargetControlID="StartDateTextBox" PopupButtonID="StartDateButton" />&nbsp;&nbsp;&nbsp;-&nbsp;
		</div>
		<div class="input-box label-row">
			<asp:Label runat="server" AssociatedControlID="EndDateTextBox"><%=Resources.General.Literal_EndDate%></asp:Label>
			<asp:TextBox ID="EndDateTextBox" runat="server"></asp:TextBox>
			<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle"/>
			<ajaxToolkit:CalendarExtender ID="EndDateCalendarControl" runat="server" TargetControlID="EndDateTextBox" PopupButtonID="EndDateButton" />
		</div>
		<div class="input-box label-row">
			<asp:Label runat="server" AssociatedControlID="ExclusiveCheckBox"><%=Resources.Campaign.Literal_Exclusive%> <uc:ToolTip ID="ToolTip1" runat="server" AliasName="Backoffice.Tooltip.CampaignExclusiveCheckBox" /></asp:Label>
			<asp:CheckBox ID="ExclusiveCheckBox" runat="server" />
		</div>
	</div>
</div>