using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
	public partial class ActionList : CampaignConfigurationList<IAction>
	{
		private const int MoveUpStep = -15;
		private const int MoveDownStep = 15;
		private const int DifferenceStep = 10;
		private const int MinStep = 1;

		public event EventHandler<ActionOrdinalEventArgs> ListItemOrdinalChange;

		private Dictionary<int, string> _currencies;
		public Dictionary<int, string> Currencies
		{
			get
			{
				if (_currencies != null)
				{
					return _currencies;
				}

				_currencies = new Dictionary<int, string>();

				var currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
				foreach (var currency in currencies)
				{
					_currencies.Add(currency.Id, currency.Iso);
				}

				return _currencies;
			}
		}

		protected virtual void OnListItemOrdinalChange(ActionOrdinalEventArgs e)
		{
			if (ListItemOrdinalChange != null)
			{
				ListItemOrdinalChange(this, e);
			}
		}

		protected override void SetEventHandlers()
		{
			ActionsGrid.RowDataBound += OnGridRowDataBound;
			ActionsGrid.RowCommand += OnGridRowCommand;
		}

		protected override void PopulateControl() { }

		public override void DataBind(Collection<IAction> items)
		{
			var sortedActions = Sort(items, DifferenceStep);

			ActionsGrid.DataSource = sortedActions.Where(a => !a.IsDeleted);
			ActionsGrid.DataBind();

			State = sortedActions;
		}

		protected virtual void OnGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType.Equals(DataControlRowType.DataRow))
			{
				var action = (IAction)e.Row.DataItem;

				if (action.Status.Equals(BusinessObjectStatus.Deleted))
				{
					e.Row.Visible = false;
					return;
				}

				var descriptionLiteral = (Literal)e.Row.FindControl("DescriptionLiteral");
				descriptionLiteral.Text = GetFriendlyDescription(action);

				var removeButton = (ImageButton)e.Row.FindControl("RemoveButton");
				removeButton.CommandArgument = action.Guid.ToString();

				string actionTypeTitle;

				if (e.Row.DataItem is ICartAction)
				{
					actionTypeTitle = ((ICartAction)action).ActionType.Title;
				}
				else if (e.Row.DataItem is IProductAction)
				{
					actionTypeTitle = ((IProductAction)action).ActionType.Title;
				}
				else
				{
					throw new InvalidOperationException();
				}

				var editButton = (LinkButton)e.Row.FindControl("EditButton");
				editButton.Text = actionTypeTitle;
				editButton.CommandArgument = action.Guid.ToString();
			}
		}

		protected virtual void OnGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			var argument = new CommandEventArgs("ActionGuid", e.CommandArgument);
			switch (e.CommandName)
			{
				case "Configure":
					OnListItemEdit(argument);
					break;
				case "Remove":
					OnListItemRemove(argument);
					break;
				case "UpOrdinal":
					Move(e.CommandArgument, MoveUpStep);
					break;
				case "DownOrdinal":
					Move(e.CommandArgument, MoveDownStep);
					break;
				case "RefreshOrdinal":
					RefreshOrdinal();
					break;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
		protected virtual string GetFriendlyDescription(IAction action)
		{
			string actionTypeCommonName;
			string applyRange = string.Empty;

			if (action is ICartAction)
			{
				actionTypeCommonName = ((ICartAction)action).ActionType.CommonName;

				if (action is ICartItemDiscountCartAction)
				{
					var cartItemDiscountCartAction = action as ICartItemDiscountCartAction;
					applyRange = cartItemDiscountCartAction.ApplyForCheapest
								? Resources.CampaignPlugin.CheapestProduct
								: Resources.CampaignPlugin.MostExpensive;
				}
			}
			else if (action is IProductAction)
			{
				actionTypeCommonName = ((IProductAction)action).ActionType.CommonName;

				if (action is ILekmerPercentagePriceDiscountAction)
				{
					var percentagePriceDiscountAction = action as ILekmerPercentagePriceDiscountAction;
					applyRange = percentagePriceDiscountAction.CampaignConfig.IncludeAllProducts
								? Resources.CampaignPlugin.AllProducts
								: Resources.CampaignPlugin.SelectionOfProducts;
				}
				else if(action is IFixedDiscountAction)
				{
					var fixedDiscountAction = action as IFixedDiscountAction;
					applyRange = fixedDiscountAction.IncludeAllProducts
								? Resources.CampaignPlugin.AllProducts
								: Resources.CampaignPlugin.SelectionOfProducts;
				}
                else if (action is IFixedPriceAction)
                {
                    var fixedPriceAction = action as IFixedPriceAction;
                    applyRange = fixedPriceAction.IncludeAllProducts
                                ? Resources.CampaignPlugin.AllProducts
                                : Resources.CampaignPlugin.SelectionOfProducts;
                }
			}
			else
			{
				throw new InvalidOperationException();
			}

			string formatString = Resources.CampaignPlugin.ResourceManager.GetString(actionTypeCommonName);

			if (formatString == null)
			{
				throw new InvalidOperationException(
					string.Format(
						CultureInfo.CurrentCulture,
						"Resource missing in CampaignPlugin.resx resource file, for action with common name {0}.",
						actionTypeCommonName));
			}

			var actionArguments = action.GetInfoArguments();
			int currencyId;
			if (actionArguments.Length > 1 && actionArguments[1] != null && int.TryParse(actionArguments[1].ToString(), out currencyId))
			{
				actionArguments[1] = Currencies[currencyId] ?? actionArguments[1];
			}

			if (!string.IsNullOrEmpty(applyRange))
			{
				actionArguments = actionArguments.Concat(new object[] { applyRange }).ToArray();
			}
			return string.Format(
				CultureInfo.CurrentCulture,
				formatString,
				actionArguments);
		}

		protected virtual void Move(object elementId, int moveStep)
		{
			var actions = State;
			var action = actions.FirstOrDefault(item => item.Guid.Equals(new Guid(elementId.ToString())));
			if (action == null) return;

			action.Ordinal += moveStep;
			var sortedActions = Sort(actions, MinStep);
			OnListItemOrdinalChange(new ActionOrdinalEventArgs(sortedActions));
		}

		protected virtual void RefreshOrdinal()
		{
			if (!ValidateOrdinal())
			{
				return;
			}

			var actions = State;
			foreach (GridViewRow row in ActionsGrid.Rows)
			{
				var actionGuid = ((HiddenField)row.FindControl("IdHiddenField")).Value;
				var action = actions.FirstOrDefault(item => item.Guid.Equals(new Guid(actionGuid)));
				if (action == null) continue;
				action.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
			}

			var sortedActions = Sort(actions, MinStep);
			OnListItemOrdinalChange(new ActionOrdinalEventArgs(sortedActions));
		}

		protected virtual Collection<IAction> Sort(IEnumerable<IAction> list, int moveStep)
		{
			var elements = new List<IAction>(list);
			elements.Sort(delegate(IAction element1, IAction element2)
			{
				if (element1.Ordinal < element2.Ordinal)
					return -1;
				if (element1.Ordinal > element2.Ordinal)
					return 1;
				return 0;
			});

			for (int i = 0; i < list.Count(); i++)
			{
				elements[i].Ordinal = (i + 1) * moveStep;
			}

			return new Collection<IAction>(elements);
		}

		protected virtual bool ValidateOrdinal()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}
	}

	public class ActionOrdinalEventArgs : EventArgs
	{
		private readonly Dictionary<Guid, int> _actionOrdinals;
		public Dictionary<Guid, int> ActionOrdinals
		{
			get { return _actionOrdinals; }
		}

		public ActionOrdinalEventArgs(IEnumerable<IAction> actions)
		{
			_actionOrdinals = new Dictionary<Guid, int>();
			foreach (var action in actions)
			{
				_actionOrdinals.Add(action.Guid, action.Ordinal);
			}
		}
	}
}