using System.Collections.Generic;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public class DefaultProcessor : ISelectorProcessor
	{
		private int SiteStructureRegistryId { get; set; }

		public DefaultProcessor(int siteStructureRegistryId)
		{
			SiteStructureRegistryId = siteStructureRegistryId;
		}

		public IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId)
		{
			return IoC.Resolve<IContentNodeSecureService>().GetTree(selectedId ?? -1 * SiteStructureRegistryId, false);
		}

		public bool IsAvailable(ISiteStructureNode node)
		{
			return !node.IsMasterPage;
		}
	}
}