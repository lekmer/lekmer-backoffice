﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlockTimeLimiter.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.BlockTimeLimiter" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<span class="block-header"><%=Resources.Lekmer.Block_ActivationDateTime%></span>
<fieldset>
	<div class="time-limit-wrapper">
		<div class="time-limit-container">
			<div class="time-limit-start-date">
				<span><%=Resources.Lekmer.Block_StartDateTime%></span>
				<br />
				<asp:TextBox ID="StartDateTextBox" runat="server"></asp:TextBox>
				<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png"
					ImageAlign="AbsMiddle" />
				<ajaxToolkit:CalendarExtender ID="StartDateCalendarControl" runat="server" TargetControlID="StartDateTextBox"
					PopupButtonID="StartDateButton" />
				&nbsp;-&nbsp;
			</div>
			<div class="time-limit-end-date">
				<span><%=Resources.Lekmer.Block_EndDateTime%></span>
				<br />
				<asp:TextBox ID="EndDateTextBox" runat="server"></asp:TextBox>
				<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png"
					ImageAlign="AbsMiddle" />
				<ajaxToolkit:CalendarExtender ID="EndDateCalendarControl" runat="server" TargetControlID="EndDateTextBox"
					PopupButtonID="EndDateButton" />
			</div>
			<div class="time-limit-daily-interval">
				<span><%=Resources.Lekmer.Block_DailyInterval%></span>
				<br />
				<asp:TextBox ID="StartIntervalTextBox" runat="server"></asp:TextBox>
				&nbsp;-&nbsp;
				<asp:TextBox ID="EndIntervalTextBox" runat="server"></asp:TextBox>
			</div>
			<div class="time-limit-comment">
				<span><%=Resources.Lekmer.Block_ActivationDateTimeComment%></span>
			</div>
		</div>
	</div>
</fieldset>