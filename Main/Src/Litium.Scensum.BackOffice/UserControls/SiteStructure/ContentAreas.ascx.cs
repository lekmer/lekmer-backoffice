using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Common;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class ContentAreas : StateUserControlController<ContentAreasState>
	{
		public int TemplateId
		{
			get { return State.TemplateId; }
			set
			{
				State.TemplateId = value;
				if (State.ContentAreas != null)
				{
					foreach (IContentArea contentArea in State.ContentAreas)
					{
						contentArea.TemplateId = value;
					}
				}

				if (State.ContentAreasToDelete != null)
				{
					foreach (IContentArea contentArea in State.ContentAreasToDelete)
					{
						contentArea.TemplateId = value;
					}
				}
			}
		}

		public int? CopyOfId
		{
			get
			{
				return State.CopyOfId;
			}
			set
			{
				State.CopyOfId = value;
			}
		}

		public void DeleteAllContentAreas()
		{
			DeleteContentAreas();
			DeleteCustomContentAreas(State.ContentAreas);
		}

		public void DeleteContentAreas()
		{
			DeleteCustomContentAreas(State.ContentAreasToDelete);
		}

		private static void DeleteCustomContentAreas(ICollection<IContentArea> contentAreas)
		{
			var contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
			foreach (IContentArea contentArea in contentAreas)
			{
				if (contentArea.Id > 0)
				{
					contentAreaSecureService.Delete(SignInHelper.SignedInSystemUser, contentArea);
				}
			}

			contentAreas.Clear();
		}

		public Collection<string> SaveContentAreas()
		{
			var contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
			var errorsList = new Collection<string>();
			foreach (IContentArea area in State.ContentAreas)
			{
				if (!area.IsEdited && !area.IsNew)
				{
					continue;
				}

				bool canSave = true;
				Collection<IContentArea> contentAreas = contentAreaSecureService.GetAllByTemplate(State.TemplateId);
				IContentArea areaLocal = area; // to prevent access to modified closure 
				
				if (contentAreas.Any(contentArea => contentArea.CommonName == areaLocal.CommonName && contentArea.Id != areaLocal.Id))
				{
					errorsList.Add(Resources.SiteStructureMessage.ContentNodeCommomNameExist);
					canSave = false;
				}

				if (contentAreas.Any(contentArea => contentArea.Title == areaLocal.Title && contentArea.Id != areaLocal.Id))
				{
					errorsList.Add(Resources.SiteStructureMessage.ContentAreaTitleExist);
					canSave = false;
				}

				if (canSave)
				{
					area.CommonName = area.CommonName;
					area.Title = area.Title;
					area.TemplateId = State.TemplateId;
					area.Id = contentAreaSecureService.Save(SignInHelper.SignedInSystemUser, area);
				}
				else
				{
					break;
				}
			}
			PopulateContentAreaGrid();
			return errorsList;
		}

		protected override void SetEventHandlers()
		{
			AddAreaButton.Click += BtnAddAreaClick;
			ModifyAreaButton.Click += BtnModifyAreaClick;
			ContentAreaGrid.RowDataBound += ContentAreaGridRowDataBound;
			ContentAreaGrid.RowCommand += ContentAreaGridRowCommand;

			State = new ContentAreasState();
		}

		protected override void PopulateControl()
		{
			Collection<IContentArea> areasCollection;
			if (State.TemplateId > 0)
			{
				var contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
				areasCollection = contentAreaSecureService.GetAllByTemplate(State.TemplateId);
			}
			else if (State.CopyOfId.HasValue)
			{
				var contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
				areasCollection = contentAreaSecureService.GetAllByTemplate(State.CopyOfId.Value);
				foreach (IContentArea area in areasCollection)
				{
					area.Id = 0;
					area.TemplateId = TemplateId;
				}
			}
			else
			{
				areasCollection = new Collection<IContentArea>();
			}

			foreach (IContentArea contentArea in areasCollection)
			{
				State.ContentAreas.Add(contentArea);
			}

			PopulateContentAreaGrid();
		}

		private void PopulateContentAreaGrid()
		{
			ContentAreaGrid.DataSource = State.ContentAreas;
			ContentAreaGrid.DataBind();
		}

		private void BtnModifyAreaClick(object sender, EventArgs e)
		{
			int indexSelf;
			if (!int.TryParse(ContentAreaIndexHiddenField.Value, out indexSelf))
			{
				indexSelf = State.ContentAreas.Count;
			}

			string commonName = CommonNameTextBox.Text = Foundation.Convert.RemoveRedundantSpaces(CommonNameTextBox.Text);
			string title = TitleTextBox.Text = Foundation.Convert.RemoveRedundantSpaces(TitleTextBox.Text);
			bool canSave = true;

			for (int i = 0; i < State.ContentAreas.Count; i++)
			{
				if (i == indexSelf) continue;

				if (State.ContentAreas[i].CommonName == commonName)
				{
					ContentAreaMessageContainer.Add(Resources.SiteStructureMessage.ContentNodeCommomNameExist);
					canSave = false;
				}

				if (State.ContentAreas[i].Title != title) continue;

				ContentAreaMessageContainer.Add(Resources.SiteStructureMessage.ContentAreaTitleExist);
				canSave = false;
			}

			if (!canSave)
			{
				ContentAreaPopup.Show();
				return;
			}

			IContentArea contentArea;
			if (indexSelf < State.ContentAreas.Count)
			{
				contentArea = State.ContentAreas[indexSelf];
			}
			else
			{
				var contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
				contentArea = contentAreaSecureService.Create();
			}

			contentArea.Id = System.Convert.ToInt32(ContentAreaIdHiddenField.Value, CultureInfo.CurrentCulture);
			contentArea.TemplateId = State.TemplateId;
			contentArea.Title = title;
			contentArea.CommonName = commonName;

			if (indexSelf < State.ContentAreas.Count)
			{
				State.ContentAreas[indexSelf] = contentArea;
			}
			else
			{
				State.ContentAreas.Add(contentArea);
			}

			PopulateContentAreaGrid();
		}

		private void BtnAddAreaClick(object sender, EventArgs e)
		{
			AreaEditionModeLabel.Text = Resources.SiteStructure.Literal_AddArea;
			TitleTextBox.Text = string.Empty;
			CommonNameTextBox.Text = string.Empty;
			ContentAreaIdHiddenField.Value = "-1";
			ContentAreaIndexHiddenField.Value = State.ContentAreas.Count.ToString(CultureInfo.CurrentCulture);
			PopulateContentAreaGrid();
			ContentAreaPopup.Show();
		}

		private void ContentAreaGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow) return;

			string index = e.Row.DataItemIndex.ToString(CultureInfo.CurrentCulture);

			var ib = e.Row.FindControl("imbDelete") as ImageButton;
			if (ib != null)
			{
				if (State.ConfirmationMessageTypes.Count <= e.Row.DataItemIndex)
				{
					var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
					State.ConfirmationMessageTypes.Add(contentPageSecureService.GetByAreaId(State.ContentAreas[e.Row.DataItemIndex].Id).Count > 0);
				}

				ib.CommandArgument = index;
				ib.Attributes.Add("onclick", State.ConfirmationMessageTypes[e.Row.DataItemIndex]
												? @"javascript: return DeleteAreaConfirmation();"
												: @"javascript: return DeleteConfirmation('area');");
			}

			var lbTitle = e.Row.FindControl("lbTitle") as LinkButtonTrimmed;
			if (lbTitle == null) return;
			lbTitle.CommandArgument = index;
		}

		private void ContentAreaGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ContentAreaDelete":
					DeleteContentArea(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
					PopulateContentAreaGrid();
					break;
				case "EditContentArea":
					PopulateEditContentAreaFields(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
					break;
			}
		}

		private void PopulateEditContentAreaFields(int index)
		{
			IContentArea contentArea = State.ContentAreas[index];
			AreaEditionModeLabel.Text = Resources.SiteStructure.Literal_EditArea;
			TitleTextBox.Text = contentArea.Title;
			CommonNameTextBox.Text = contentArea.CommonName;
			ContentAreaIdHiddenField.Value = contentArea.Id.ToString(CultureInfo.CurrentCulture);
			ContentAreaIndexHiddenField.Value = index.ToString(CultureInfo.CurrentCulture);
			ContentAreaPopup.Show();
			AreaPopupUpdatePanel.Update();
		}
		
		private void DeleteContentArea(int index)
		{
			State.ContentAreasToDelete.Add(State.ContentAreas[index]);
			State.ContentAreas.RemoveAt(index);
			State.ConfirmationMessageTypes.RemoveAt(index);
		}
	}
}
