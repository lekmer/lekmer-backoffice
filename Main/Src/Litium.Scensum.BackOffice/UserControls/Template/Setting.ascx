<%@ Control Language="C#"  CodeBehind="Setting.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Template.Setting" %>
<div class="input-box">
    <asp:HiddenField ID="ModelSettingIdHiddenField" runat="server" />
    <asp:HiddenField ID="SettingTypeHiddenField" runat="server" />
    <asp:Label ID="HeaderLabel" runat="server" />
    <asp:Label ID="TypeLabel" runat="server" />
	<asp:TextBox ID="ValueTextBox" runat="server" />
	<asp:RadioButtonList ID = "ValueRadioButtonList" runat="server">
		<asp:ListItem Text ="True" Value ="True" />
		<asp:ListItem Text ="False" Value ="False" />
	</asp:RadioButtonList>
	<asp:RegularExpressionValidator ID="ValueValidator" runat="server" ValidationGroup="vgTemplate" ControlToValidate="ValueTextBox" ValidationExpression="\d*" Text="<%$ Resources:InterfaceMessage, OnlyNumericData  %>"/>
</div>