using System;
using System.Globalization;
using System.Web.UI;
using Litium.Scensum.BackOffice.UserControls.Template.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.UserControls.Template
{
	public partial class Fragment : UserControl
	{
		#region Property

		public string Name { get; set; }
		public string Content { get; set; }
		public int ModelFragmentId { get; set; }
		public bool IsAlternate { get; set; }
		public int Size { get; set; }

		public event EventHandler<FunctionAddEventArgs> FunctionAdded;

		#endregion

		#region Page Events

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			AddButton.Click += AddButtonClick;
			AddButton.OnClientClick += "openFunctionPopup('" + ContentTextBox.ClientID + "')";
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!Page.IsPostBack)
			{
				HeaderLabel.Text = Name;
				ContentTextBox.Text = Content;
				ModelFragmentIdHiddenField.Value = ModelFragmentId.ToString(CultureInfo.CurrentCulture);
				AddAliasDiv.Attributes.Add("onclick", "openAliasPopup('" + ContentTextBox.ClientID + "');");
				AddIncludeDiv.Attributes.Add("onclick", "openIncludePopup('" + ContentTextBox.ClientID + "');");
				AddComponentDiv.Attributes.Add("onclick", "openComponentPopup('" + ContentTextBox.ClientID + "');");
			}
			if (Size > 0)
			{
				if (Request.Browser.Browser == "Firefox")
				{
					ContentTextBox.Rows = Size == 1 ? Size : Size - 1;
				}
				else
				{
					ContentTextBox.Rows = Size;
				}
			}
		}

		private void AddButtonClick(object sender, EventArgs e)
		{
			int modelFragmentId;
			if (FunctionAdded != null && int.TryParse(ModelFragmentIdHiddenField.Value, out modelFragmentId))
			{
				FunctionAdded(this, new FunctionAddEventArgs {ModelFragmentId = modelFragmentId});
			}
				
		}


		#endregion

		#region Public Methods

		public ITemplateFragment GetFragment()
		{
			var templateFragmentService = IoC.Resolve<ITemplateFragmentSecureService>();
			ITemplateFragment templateFragment = templateFragmentService.Create();
			templateFragment.ModelFragmentId = System.Convert.ToInt32(ModelFragmentIdHiddenField.Value, CultureInfo.CurrentCulture);

			if (IsAlternate)
			{
				templateFragment.AlternateContent = ContentTextBox.Text;
			}
			else
			{
				templateFragment.Content = ContentTextBox.Text;
			}
			return templateFragment;
		}

		#endregion
	}
}