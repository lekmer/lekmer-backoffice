using System;
using System.Globalization;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.UserControls.Template
{
	public partial class Setting : System.Web.UI.UserControl
	{
		#region Property

		public string Header { get; set; }
		public string Value { get; set; }
		public int ModelSettingId { get; set; }
		public bool IsAlternate { get; set; }
		public int SettingType { get; set; }
		public string SettingTypeName { get; set; }

		#endregion

		#region Page Events

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!Page.IsPostBack)
			{
				HeaderLabel.Text = Header;
				ModelSettingIdHiddenField.Value = ModelSettingId.ToString(CultureInfo.CurrentCulture);
				SettingTypeHiddenField.Value = SettingType.ToString(CultureInfo.CurrentCulture);
				ValueValidator.Enabled = false;
				ValueRadioButtonList.Visible = false;
				switch (SettingType)
				{
					case 3://bool
						bool boolValue;
						if (Boolean.TryParse(Value, out boolValue))
						{
							if (boolValue)
								ValueRadioButtonList.SelectedIndex = 0;
							else
								ValueRadioButtonList.SelectedIndex = 1;
							
						}
						ValueRadioButtonList.Visible = true;

						ValueTextBox.Visible = false;
						break;
					case 2://int
						ValueValidator.Enabled = true;
						ValueTextBox.Text = Value;
						break;
					default://string

						ValueTextBox.Text = Value;
						break;
				}
				TypeLabel.Text = string.Format(CultureInfo.CurrentCulture, " ({0})", SettingTypeName);
			}
		}

		#endregion

		#region Public Methods

		public ITemplateSetting GetSetting()
		{
			var templateSettingSecureService = IoC.Resolve<ITemplateSettingSecureService>();
			ITemplateSetting templateSetting = templateSettingSecureService.Create();
			templateSetting.ModelSettingId = System.Convert.ToInt32(ModelSettingIdHiddenField.Value, CultureInfo.CurrentCulture);
			SettingType = System.Convert.ToInt32(SettingTypeHiddenField.Value, CultureInfo.CurrentCulture);
			string settingValue;
			switch (SettingType)
			{
				case 3://bool
					settingValue = ValueRadioButtonList.SelectedValue;
					break;
				case 2://int		
				default://string
					settingValue = ValueTextBox.Text;
					break;
			}
			if (IsAlternate)
			{
				templateSetting.AlternateValue = settingValue;
			}
			else
			{
				templateSetting.Value = settingValue;
			}
			return templateSetting;
		}

		#endregion
	}
}