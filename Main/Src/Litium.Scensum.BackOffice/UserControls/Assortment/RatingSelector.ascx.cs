using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class RatingSelector : StateUserControlController<RatingSelectorState>
	{
		public bool DenyMultipleSelection { get; set; }

		private IEnumerable<INode> _nodes;

		public event EventHandler<RatingSelectEventArgs> SelectEvent;

		protected override void SetEventHandlers()
		{
			RatingFoldersTree.NodeCommand += OnNodeCommand;

			RatingsGrid.DataBound += OnDataBound;
			RatingsGrid.RowDataBound += OnRowDataBound;
			RatingsGrid.PageIndexChanging += OnPageIndexChanging;

			AddButton.Click += OnAdd;
		}

		protected override void PopulateControl()
		{
			PopulateTree(null);
			PopulateGrid(null);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (!IsPostBack)
			{
				State = new RatingSelectorState();

				SingleSelectionMessage.Visible = DenyMultipleSelection;
			}

			if (State.DenySelection.HasValue)
			{
				RatingFoldersTree.DenySelection = State.DenySelection.Value;
			}
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);

			switch (e.EventName)
			{
				case "Expand":
					SetSelection(false);

					break;

				case "Navigate":
					PopulateGrid(e.Id);

					SetSelection(true);
					State.SelectedNodeId = e.Id;

					break;
			}
		}

		protected virtual void OnDataBound(object sender, EventArgs e)
		{
			AddButton.Enabled = RatingsGrid.Rows.Count > 0;
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingsGrid.PageIndex = e.NewPageIndex;

			PopulateGrid(State.SelectedNodeId);
		}

		protected virtual void OnAdd(object sender, EventArgs e)
		{
			var ids = RatingsGrid.GetSelectedIds();

			if (DenyMultipleSelection && ids.Count() > 1)
			{
				SystemMessageContainer.Add(Resources.RatingReview.Message_SelectOneRating, InfoType.Warning);
				return;
			}

			var ratings = new Collection<RatingState>();
			var service = IoC.Resolve<IRatingSecureService>();

			foreach (var id in ids)
			{
				ratings.Add(new RatingState(service.GetById(id)));
			}

			if (ratings.Count > 0 && SelectEvent != null)
			{
				SelectEvent(this, new RatingSelectEventArgs { Ratings = ratings });
				RatingSelectPopup.Hide();
			}
		}

		protected virtual void SetSelection(bool isSelected)
		{
			State.DenySelection = RatingFoldersTree.DenySelection = !isSelected;
		}

		protected virtual void PopulateTree(int? folderId)
		{
			RatingFoldersTree.DataSource = GetFolders(folderId);
			RatingFoldersTree.RootNodeTitle = Resources.RatingReview.Literal_Ratings;
			RatingFoldersTree.DataBind();
			RatingFoldersTree.SelectedNodeId = folderId ?? RatingFoldersTree.RootNodeId;
		}

		protected virtual void PopulateGrid(int? folderId)
		{
			int? id = folderId != RatingFoldersTree.RootNodeId ? folderId : -1;

			RatingsGrid.DataSource = id.HasValue ? IoC.Resolve<IRatingSecureService>().GetAllByFolder(id.Value) : new Collection<IRating>();
			RatingsGrid.DataBind();
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				
				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"')");
			}
		}

		protected virtual IEnumerable<INode> GetFolders(int? nodeId)
		{
			return _nodes ?? (_nodes = IoC.Resolve<IRatingFolderSecureService>().GetTree(nodeId));
		}
	}

	[Serializable]
	public sealed class RatingSelectorState
	{
		public int? SelectedNodeId { get; set; }
		public bool? DenySelection { get; set; }
	}
}