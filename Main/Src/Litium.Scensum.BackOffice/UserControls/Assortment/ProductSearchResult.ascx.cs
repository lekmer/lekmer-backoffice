﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class ProductSearchResult : UserControlController
	{
		private Collection<IProductStatus> _productStatuses;
		private Collection<IProductType> _productTypes;

		protected override void SetEventHandlers()
		{
			ProductGrid.PageIndexChanging += ProductGridPageIndexChanging;
			ProductGrid.RowDataBound += ProductGridRowDataBound;
			ProductGrid.DataBound += ProductGridDataBound;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public Collection<IProduct> GetSelectedProducts()
		{
			var products = new Collection<IProduct>();
			//bool isAnyRowSelected = false;
			if (ProductGrid.Rows.Count > 0)
			{
				foreach (GridViewRow row in ProductGrid.Rows)
				{
					var selectCheckBox = (CheckBox)row.FindControl("cbSelect");
					if (selectCheckBox.Checked)
					{
						//isAnyRowSelected = true;
						int productId = System.Convert.ToInt32(((HiddenField)row.FindControl("hfId")).Value, CultureInfo.CurrentCulture);
						if (products.FirstOrDefault(item => item.Id == productId) == null)
						{
							var productSecureService = IoC.Resolve<IProductSecureService>();
							IProduct product = productSecureService.GetById(ChannelHelper.CurrentChannel.Id, productId);
							products.Add(product);
						}
						selectCheckBox.Checked = false;
					}
				}
				var chkAllSearched = (CheckBox)ProductGrid.HeaderRow.FindControl("chkAllSearched");
				chkAllSearched.Checked = false;
			}
			return products;
		}

		public virtual void DataBind(IProductSearchCriteria searchCriteria)
		{
			var lekmerProductSearchCriteria = (ILekmerProductSearchCriteria) searchCriteria;

			productDataSource.SelectParameters.Clear();
			productDataSource.SelectParameters.Add("maximumRows", ProductGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			productDataSource.SelectParameters.Add("startRowIndex", (ProductGrid.PageIndex * ProductGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			productDataSource.SelectParameters.Add("categoryId", searchCriteria.CategoryId);
			productDataSource.SelectParameters.Add("erpId", searchCriteria.ErpId);
			productDataSource.SelectParameters.Add("title", searchCriteria.Title);
			productDataSource.SelectParameters.Add("statusId", searchCriteria.StatusId);
			productDataSource.SelectParameters.Add("priceFrom", searchCriteria.PriceFrom);
			productDataSource.SelectParameters.Add("priceTo", searchCriteria.PriceTo);
			productDataSource.SelectParameters.Add("eanCode", searchCriteria.EanCode);
			productDataSource.SelectParameters.Add("brandId", lekmerProductSearchCriteria.BrandId);
			productDataSource.SelectParameters.Add("productTypeId", lekmerProductSearchCriteria.ProductTypeId);
			productDataSource.SelectParameters.Add("supplierArticleNumber", lekmerProductSearchCriteria.SupplierArticleNumber);
		}

		protected virtual void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var chkAllSearched = (CheckBox)row.FindControl("chkAllSearched");
				chkAllSearched.Checked = false;
				chkAllSearched.Attributes.Add("onclick", "SelectAll1('" + chkAllSearched.ClientID + @"','" + ProductGrid.ClientID + @"')");
			}
			if (row.RowType != DataControlRowType.DataRow)
				return;
			var product = (IProduct)row.DataItem;

			var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
			lblProductStatus.Text = ResolveStatusTitle(product.ProductStatusId);

			var lblType = (Label)e.Row.FindControl("lblType");
			lblType.Text = ResolveTypeTitle(((ILekmerProduct)product).ProductTypeId);

			var lblPrice = (Label)e.Row.FindControl("lblPrice");
			if (lblPrice != null && product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}
		}

		protected virtual void ProductGridDataBound(object sender, EventArgs e)
		{
			//btnAdd.Enabled = ProductGrid.Rows.Count > 0;
		}

		protected virtual void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductGrid.PageIndex = e.NewPageIndex;
		}

		protected override void PopulateControl()
		{
		}

		public int Count()
		{
			return ProductGrid.Rows.Count;
		}

		protected virtual string ResolveStatusTitle(int statusId)
		{
			if (_productStatuses == null)
			{
				var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
				_productStatuses = productStatusSecureService.GetAll();
			}
			if (_productStatuses == null)
			{
				_productStatuses = new Collection<IProductStatus>();
			}

			var productStatus = _productStatuses.First(item => item.Id == statusId).Title;

			return productStatus;
		}

		protected virtual string ResolveTypeTitle(int typeId)
		{
			if (_productTypes == null)
			{
				var productTypeSecureService = IoC.Resolve<IProductTypeSecureService>();
				_productTypes = productTypeSecureService.GetAll();
			}
			if (_productTypes == null)
			{
				_productTypes = new Collection<IProductType>();
			}

			var productType = _productTypes.First(item => item.Id == typeId).Title;

			return productType;
		}
	}
}