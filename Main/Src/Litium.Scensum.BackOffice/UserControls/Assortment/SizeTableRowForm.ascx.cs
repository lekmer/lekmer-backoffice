﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Sizes;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.Translation;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class SizeTableRowForm : StateUserControlController<SizeTableRowState>
	{
		public List<ISize> AvaliableSizes { get; set; }

		public event EventHandler<SizeTableRowSelectEventArgs> SaveEvent;

		public ModalPopupExtender Popup
		{
			get
			{
				return SizeTableRowPopup;
			}
		}

		protected override void SetEventHandlers()
		{
			AddButton.Click += OnAdd;
			CancelButton.ServerClick += OnCancel;
			SizesList.DataBound += SizesListDataBound;
		}

		protected override void PopulateControl()
		{
			PopulateSizes();
			BindData(null);
		}

		public virtual void Populate(SizeTableRowItem sizeTableRowItem, ISize sizeToEdit)
		{
			if (AvaliableSizes.FirstOrDefault(s => s.Id == sizeToEdit.Id) == null)
			{
				AvaliableSizes.Add(sizeToEdit);
			}
			State.EditetSizeId = sizeTableRowItem.SizeTableRow.SizeId;
			PopulateSizes();
			BindData(sizeTableRowItem);
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			BindData(null);
			RemoveEditedSize();
		}

		protected virtual void OnAdd(object sender, EventArgs e)
		{
			var id = IdHiddenField.Value;

			var sizeTableRow = IoC.Resolve<ISizeTableRow>();
			sizeTableRow.Id = !string.IsNullOrEmpty(id) ? int.Parse(id) : 0;
			sizeTableRow.SizeId = Convert.ToInt32(SizesList.SelectedValue);
			sizeTableRow.Column1Value = Col1TextBox.Text;
			sizeTableRow.Column2Value = Col2TextBox.Text;

			var sizeTableRowItem = new SizeTableRowItem
			{
				Id = !string.IsNullOrEmpty(State.Id) ? State.Id : Guid.NewGuid().ToString(),
				SizeTableRow = sizeTableRow,
				SizeTableRowTranslation = GetTranslations()
			};

			SaveEvent(this, new SizeTableRowSelectEventArgs
				{
					SizeTableRowItem = sizeTableRowItem
				});

			PopulateSizes();
			BindData(null);
			State.EditetSizeId = -1;
			SizeTableRowPopup.Hide();
		}

		protected void SizesListDataBound(object sender, EventArgs e)
		{
			foreach (ListItem item in SizesList.Items)
			{
				var itemSize = AvaliableSizes.FirstOrDefault(s => s.Id.ToString(CultureInfo.InvariantCulture) == item.Value);
				if (itemSize != null)
				{
					item.Text = string.Format("{0} - {1}", itemSize.ErpId, itemSize.ErpTitle);
				}
			}
		}

		protected virtual void BindData(SizeTableRowItem sizeTableRowItem)
		{
			if (State == null)
			{
				State = new SizeTableRowState();
			}

			if (sizeTableRowItem != null)
			{
				State.Id = sizeTableRowItem.Id;
				IdHiddenField.Value = sizeTableRowItem.SizeTableRow.Id.ToString(CultureInfo.InvariantCulture);
				SizesList.Items.FindByValue(sizeTableRowItem.SizeTableRow.SizeId.ToString(CultureInfo.CurrentCulture)).Selected = true;
				Col1TextBox.Text = sizeTableRowItem.SizeTableRow.Column1Value;
				Col2TextBox.Text = sizeTableRowItem.SizeTableRow.Column2Value;
			}
			else
			{
				State.Id = string.Empty;
				IdHiddenField.Value = string.Empty;
				SizesList.SelectedValue = "0";
				Col1TextBox.Text = string.Empty;
				Col2TextBox.Text = string.Empty;
			}

			PopulateTranslations(sizeTableRowItem);
			SizeTableRowFormUpdatePanel.Update();
		}

		public void PopulateSizes()
		{
			SizesList.Items.Clear();

			if (AvaliableSizes != null)
			{
				AvaliableSizes.Sort(delegate(ISize element1, ISize element2)
				{
					if (string.Compare(element1.ErpId, element2.ErpId, StringComparison.Ordinal) < 0)
						return -1;
					if (string.Compare(element1.ErpId, element2.ErpId, StringComparison.Ordinal) > 0)
						return 1;
					return 0;
				});
			}
			
			SizesList.DataSource = AvaliableSizes;
			SizesList.DataTextField = "EuTitle";
			SizesList.DataValueField = "Id";
			SizesList.DataBind();
			SizesList.Items.Insert(0, new ListItem("Select Size", "0"));
			SizeTableRowFormUpdatePanel.Update();
		}

		protected void RemoveEditedSize()
		{
			if (State.EditetSizeId <= 0) return;

			var item = SizesList.Items.FindByValue(State.EditetSizeId.ToString(CultureInfo.InvariantCulture));
			if (item != null)
			{
				SizesList.Items.Remove(item);
			}
			
			State.EditetSizeId = -1;
		}

		protected virtual void PopulateTranslations(SizeTableRowItem sizeTableRowItem)
		{
			Col1Translator.DefaultValueControlClientId = Col1TextBox.ClientID;
			Col2Translator.DefaultValueControlClientId = Col2TextBox.ClientID;

			if (sizeTableRowItem != null)
			{
				var column1Translations = new Collection<ITranslationGeneric>();
				var column2Translations = new Collection<ITranslationGeneric>();
				foreach (var translation in sizeTableRowItem.SizeTableRowTranslation)
				{
					var column1Translation = BuildTranslationGeneric(translation);
					column1Translation.Value = translation.Column1Value;
					column1Translations.Add(column1Translation);

					var column2Translation = BuildTranslationGeneric(translation);
					column2Translation.Value = translation.Column2Value;
					column2Translations.Add(column2Translation);
				}

				DataBindTranslator(Col1Translator, 1, column1Translations);
				DataBindTranslator(Col2Translator, 1, column2Translations);
			}
			else
			{
				DataBindTranslator(Col1Translator, 0, new Collection<ITranslationGeneric>());
				DataBindTranslator(Col2Translator, 0, new Collection<ITranslationGeneric>());
			}

			Col1Translator.DataBind();
			Col2Translator.DataBind();
		}

		protected virtual ITranslationGeneric BuildTranslationGeneric(ISizeTableRowTranslation sizeTableRowTranslation)
		{
			var translation = IoC.Resolve<ITranslationGeneric>();
			translation.Id = sizeTableRowTranslation.SizeTableRowId;
			translation.LanguageId = sizeTableRowTranslation.LanguageId;
			return translation;
		}

		protected virtual void DataBindTranslator(GenericTranslator translator, int objectId, Collection<ITranslationGeneric> dataSource)
		{
			translator.BusinessObjectId = objectId;
			translator.DataSource = dataSource;
		}

		protected virtual Collection<ISizeTableRowTranslation> GetTranslations()
		{
			var translations = new Collection<ISizeTableRowTranslation>();

			var column1Translations = Col1Translator.GetTranslations();
			var column2Translations = Col2Translator.GetTranslations();
			column1Translations.ReplaceEmptyStringValuesWithNull();
			column2Translations.ReplaceEmptyStringValuesWithNull();

			for (int i = 0; i < column1Translations.Count; i++)
			{
				var translation = IoC.Resolve<ISizeTableRowTranslation>();
				translation.SizeTableRowId = column1Translations[i].Id;
				translation.LanguageId = column1Translations[i].LanguageId;
				translation.Column1Value = column1Translations[i].Value;
				translation.Column2Value = column2Translations[i].Value;
				translations.Add(translation);
			}

			return translations;
		}
	}

	[Serializable]
	public sealed class SizeTableRowState
	{
		public string Id { get; set; }
		public int EditetSizeId { get; set; }
	}
}