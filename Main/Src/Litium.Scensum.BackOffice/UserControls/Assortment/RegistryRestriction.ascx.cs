﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class RegistryRestriction : UserControlController
	{
		public bool IsContentPageManage { get; set; }
		public Dictionary<int, string> RegistryRestrictions { get; set; }
		public Dictionary<int, IBrandSiteStructureProductRegistryWrapper> BrandSiteStructureProductRegistryWrappersAll { get; set; }

		[Description("Width of grid"), Category("Data")]
		public Unit Width
		{
			get { return ProductRegistryGrid.Width; }
			set { ProductRegistryGrid.Width = value; }
		}

		protected override void SetEventHandlers()
		{
			ProductRegistryGrid.RowDataBound += ProductRegistryGridRowDataBound;
		}

		protected void ProductRegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;

			row.Cells[2].Visible = IsContentPageManage;
			if (row.RowType != DataControlRowType.DataRow || RegistryRestrictions == null)
			{
				return;
			}

			var registry = (IProductRegistry)row.DataItem;

			var restrictionReasonText = (TextBox)row.FindControl("RestrictionReasonText");
			if (restrictionReasonText != null && RegistryRestrictions.ContainsKey(registry.ProductRegistryId))
			{
				restrictionReasonText.Text = RegistryRestrictions[registry.ProductRegistryId];
			}

			var restrictionCheckBox = (CheckBox)row.FindControl("RestrictionCheckBox");
			if (restrictionCheckBox != null)
			{
				restrictionCheckBox.Checked = RegistryRestrictions.ContainsKey(registry.ProductRegistryId);
				if (IsContentPageManage)
				{
					restrictionCheckBox.Attributes["onClick"] = "Enable(this);";
				}
			}

			if (IsContentPageManage)
			{
				var offlineCheckBox = (CheckBox)row.FindControl("OfflineCheckBox");
				if (offlineCheckBox != null && BrandSiteStructureProductRegistryWrappersAll.ContainsKey(registry.ProductRegistryId))
				{
					offlineCheckBox.Checked = BrandSiteStructureProductRegistryWrappersAll[registry.ProductRegistryId].ContentNodeStatusId == (int) ContentNodeStatusInfo.Offline;
				}

				var contentNodeIdHidden = (HiddenField)row.FindControl("ContentNodeIdHidden");
				if (contentNodeIdHidden != null && BrandSiteStructureProductRegistryWrappersAll.ContainsKey(registry.ProductRegistryId))
				{
					contentNodeIdHidden.Value = BrandSiteStructureProductRegistryWrappersAll[registry.ProductRegistryId].ContentNodeId.ToString(CultureInfo.InvariantCulture);
				}
			}
		}

		protected override void PopulateControl()
		{
		}

		public void BindData()
		{
			ProductRegistryGrid.DataSource = IoC.Resolve<IProductRegistrySecureService>().GetAll();
			ProductRegistryGrid.DataBind();
		}

		public Dictionary<int, string> GetRestrictions()
		{
			var restrictions = new Dictionary<int, string>();
			foreach (GridViewRow row in ProductRegistryGrid.Rows)
			{
				var restrictionCheckBox = (CheckBox) row.FindControl("RestrictionCheckBox");
				if (restrictionCheckBox != null && restrictionCheckBox.Checked)
				{
					var productRegistryIdHidden = (HiddenField) row.FindControl("ProductRegistryIdHidden");
					var restrictionReasonText = (TextBox) row.FindControl("RestrictionReasonText");
					if (productRegistryIdHidden != null && !string.IsNullOrEmpty(productRegistryIdHidden.Value) && restrictionReasonText != null)
					{
						int productRegistryId = Convert.ToInt32(productRegistryIdHidden.Value);
						if (!restrictions.ContainsKey(productRegistryId))
						{
							restrictions.Add(productRegistryId, restrictionReasonText.Text);
						}
					}
				}
			}

			return restrictions;
		}

		public Dictionary<int, int> GetOfflineContentPages()
		{
			var offlinePages = new Dictionary<int, int>();
			foreach (GridViewRow row in ProductRegistryGrid.Rows)
			{
				var offlineCheckBox = (CheckBox)row.FindControl("OfflineCheckBox");
				if (offlineCheckBox != null && offlineCheckBox.Checked)
				{
					var productRegistryIdHidden = (HiddenField)row.FindControl("ProductRegistryIdHidden");
					var contentNodeIdHidden = (HiddenField)row.FindControl("ContentNodeIdHidden");
					if (productRegistryIdHidden != null && !string.IsNullOrEmpty(productRegistryIdHidden.Value)
						&& contentNodeIdHidden != null && !string.IsNullOrEmpty(contentNodeIdHidden.Value))
					{
						int productRegistryId = Convert.ToInt32(productRegistryIdHidden.Value);
						int contentNodeId = Convert.ToInt32(contentNodeIdHidden.Value);
						if (!offlinePages.ContainsKey(productRegistryId))
						{
							offlinePages.Add(productRegistryId, contentNodeId);
						}
					}
				}
			}

			return offlinePages;
		}
	}
}