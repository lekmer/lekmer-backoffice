﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class IncludeItemsConfigurator : StateUserControlController<IIncludeItems>
	{
		public IIncludeItems DataSource { get; set; }

		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		private CampaignConfiguratorProduct _campaignConfiguratorIncludeProduct;
		private CampaignConfiguratorProduct CampaignConfiguratorIncludeProduct
		{
			get { return _campaignConfiguratorIncludeProduct ?? (_campaignConfiguratorIncludeProduct = new CampaignConfiguratorProduct()); }
		}

		private CampaignConfiguratorCategory _campaignConfiguratorIncludeCategory;
		private CampaignConfiguratorCategory CampaignConfiguratorIncludeCategory
		{
			get { return _campaignConfiguratorIncludeCategory ?? (_campaignConfiguratorIncludeCategory = new CampaignConfiguratorCategory()); }
		}

		private CampaignConfiguratorBrand _campaignConfiguratorIncludeBrand;
		private CampaignConfiguratorBrand CampaignConfiguratorIncludeBrand
		{
			get { return _campaignConfiguratorIncludeBrand ?? (_campaignConfiguratorIncludeBrand = new CampaignConfiguratorBrand()); }
		}

		private CampaignConfiguratorSupplier _campaignConfiguratorIncludeSupplier;
		private CampaignConfiguratorSupplier CampaignConfiguratorIncludeSupplier
		{
			get { return _campaignConfiguratorIncludeSupplier ?? (_campaignConfiguratorIncludeSupplier = new CampaignConfiguratorSupplier()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			if (State != null)
			{
				CampaignConfiguratorIncludeProduct.Products = State.IncludeProducts;
				CampaignConfiguratorIncludeCategory.Categories = State.IncludeCategories;
				CampaignConfiguratorIncludeBrand.Brands = State.IncludeBrands;
				CampaignConfiguratorIncludeSupplier.Suppliers = State.IncludeSuppliers;
			}

			base.OnLoad(e);
		}

		protected override void SetEventHandlers()
		{
			InitializeConfiguratorIncludeProducts();
			InitializeConfiguratorIncludeCategories();
			InitializeConfiguratorIncludeBrands();
			InitializeConfiguratorIncludeSuppliers();

			CampaignConfiguratorIncludeProduct.SetEventHandlers();
			CampaignConfiguratorIncludeCategory.SetEventHandlers();
			CampaignConfiguratorIncludeBrand.SetEventHandlers();
			CampaignConfiguratorIncludeSupplier.SetEventHandlers();
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		protected override void PopulateControl()
		{
			BindData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void BindData()
		{
			if (DataSource == null)
			{
				throw new ArgumentNullException("DataSource");
			}

			var includeItems = DataSource;
			State = includeItems;

			CampaignConfiguratorIncludeProduct.DataBind(includeItems.IncludeProducts);
			CampaignConfiguratorIncludeCategory.DataBind(includeItems.IncludeCategories);
			CampaignConfiguratorIncludeBrand.DataBind(includeItems.IncludeBrands);
			CampaignConfiguratorIncludeSupplier.DataBind(includeItems.IncludeSuppliers);

			State = includeItems;
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			CampaignConfiguratorIncludeProduct.DataBindGrid(State.IncludeProducts);
		}

		public IIncludeItems GetResults()
		{
			var includeItems = State;
			return includeItems;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmBrandRemove", "function ConfirmBrandRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteBrand + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmSupplierRemove", "function ConfirmSupplierRemove() {return DeleteConfirmation('" + Resources.Lekmer.Literal_DeleteSupplier + "');}", true);

			const string script = "$(function() {" +
								"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.55, 0.09);" +
								"ResizePopup('BrandIncludePopupDiv', 0.4, 0.55, 0.09);" +
								"ResizePopup('SupplierIncludePopupDiv', 0.4, 0.55, 0.09);" +
								"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		protected virtual string GetCategoryPathIncludeGrid(object cat)
		{
			return CampaignConfiguratorIncludeCategory.GetCategoryPath(cat);
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			BrandIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandIncludeGrid) ? "block" : "none";
			SupplierIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(SupplierIncludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = ProductIncludeGrid.Rows.Count > 0
											|| CategoryIncludeGrid.Rows.Count > 0
											|| BrandIncludeGrid.Rows.Count > 0
											|| SupplierIncludeGrid.Rows.Count > 0 
											? "block" : "none";
		}

		private void InitializeConfiguratorIncludeProducts()
		{
			CampaignConfiguratorIncludeProduct.Grid = ProductIncludeGrid;
			CampaignConfiguratorIncludeProduct.DataSource = IncludeProductDataSource;
			CampaignConfiguratorIncludeProduct.GridPageSizeSelect = ProductGridPageSizeSelect;
			CampaignConfiguratorIncludeProduct.SearchResultControl = ProductIncludeSearchResultControl;
			CampaignConfiguratorIncludeProduct.SearchFormControl = ProductIncludeSearchFormControl;
			CampaignConfiguratorIncludeProduct.SearchButton = ProductIncludePopupSearchButton;
			CampaignConfiguratorIncludeProduct.OkButton = ProductIncludePopupOkButton;
			CampaignConfiguratorIncludeProduct.AddAllButton = ProductIncludePopupAddAllButton;
			CampaignConfiguratorIncludeProduct.RemoveSelectionGridButton = RemoveSelectionFromProductIncludeGridButton;
			CampaignConfiguratorIncludeProduct.GridUpdatePanel = ProductIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeProduct.ApplyToAllDiv = ProductIncludeApplyToAllSelectedDiv;
		}

		private void InitializeConfiguratorIncludeCategories()
		{
			CampaignConfiguratorIncludeCategory.Grid = CategoryIncludeGrid;
			CampaignConfiguratorIncludeCategory.OkButton = CategoryIncludePopupOkButton;
			CampaignConfiguratorIncludeCategory.RemoveSelectionGridButton = RemoveSelectionFromCategoryIncludeGridButton;
			CampaignConfiguratorIncludeCategory.GridUpdatePanel = CategoryIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeCategory.PopupCategoryTree = CategoryIncludePopupCategoryTree;
			CampaignConfiguratorIncludeCategory.ApplyToAllDiv = CategoryIncludeApplyToAllSelectedDiv;
		}

		private void InitializeConfiguratorIncludeBrands()
		{
			CampaignConfiguratorIncludeBrand.Grid = BrandIncludeGrid;
			CampaignConfiguratorIncludeBrand.SearchGrid = BrandIncludeSearchGrid;
			CampaignConfiguratorIncludeBrand.OkButton = BrandIncludePopupOkButton;
			CampaignConfiguratorIncludeBrand.RemoveSelectionGridButton = RemoveSelectionFromBrandIncludeGridButton;
			CampaignConfiguratorIncludeBrand.GridUpdatePanel = BrandIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeBrand.PopupUpdatePanel = BrandIncludePopupUpdatePanel;
			CampaignConfiguratorIncludeBrand.ApplyToAllDiv = BrandIncludeApplyToAllSelectedDiv;
		}

		private void InitializeConfiguratorIncludeSuppliers()
		{
			CampaignConfiguratorIncludeSupplier.Grid = SupplierIncludeGrid;
			CampaignConfiguratorIncludeSupplier.SearchGrid = SupplierIncludeSearchGrid;
			CampaignConfiguratorIncludeSupplier.OkButton = SupplierIncludePopupOkButton;
			CampaignConfiguratorIncludeSupplier.RemoveSelectionGridButton = RemoveSelectionFromSupplierIncludeGridButton;
			CampaignConfiguratorIncludeSupplier.GridUpdatePanel = SupplierIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeSupplier.PopupUpdatePanel = SupplierIncludePopupUpdatePanel;
			CampaignConfiguratorIncludeSupplier.ApplyToAllDiv = SupplierIncludeApplyToAllSelectedDiv;
		}
	}
}