﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RatingSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.RatingSelector" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" TagPrefix="CustomControls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type="text/javascript">
	function CloseRatingsPopup() {
		$("div#popup-ratings-header-center input[name*='CancelButton']").click();
		return false;
	}
</script>

<div id="AddRatingsDiv" runat="server" class="popup-ratings-container" style="z-index: 10010; display: none;">
	<div id="popup-ratings-header">
		<div id="popup-ratings-header-left"></div>

		<div id="popup-ratings-header-center">
			<span><%= Resources.RatingReview.Literal_SelectRatings%></span>
			<input type="button" id="CancelButton" runat="server" value="x" />
		</div>

		<div id="popup-ratings-header-right"></div>
	</div>

	<div id="popup-ratings-content">
		
		<asp:UpdatePanel runat="server" ID="MainUpdatePanel" UpdateMode="Conditional">
			<ContentTemplate>
				<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="AddButton" />
				<div class="popup-ratings-body">
					<div class="left">
						<CustomControls:TemplatedTreeView 
							ID="RatingFoldersTree"
							runat="server"
							DisplayTextControl="TitleLabel"
							UseRootNode="true"
							NodeExpanderHiddenCssClass="tree-item-expander-hidden"
							NodeImgCssClass="tree-node-img"
							MainContainerCssClass="treeview-main-container"
							NodeChildContainerCssClass="treeview-node-child"
							NodeExpandCollapseControlCssClass="tree-icon"
							NodeMainContainerCssClass="treeview-node"
							NodeParentContainerCssClass="treeview-node-parent"
							NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
							NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
							MenuCallerElementCssClass="tree-menu-caller"
							MenuContainerElementId="node-menu"
							MenuCloseElementId="menu-close">
							<NodeTemplate>
								<div class="tree-item-cell-expand">
									<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
									<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden" />
								</div>

								<div class="tree-item-cell-main">
									<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
									<asp:LinkButton runat="server" ID="TitleLabel" CommandName="Navigate" />
								</div>
							</NodeTemplate>
						</CustomControls:TemplatedTreeView>
					</div>

					<div class="popup-rating-grid">
						<span id="SingleSelectionMessage" runat="server" class="bold"><%= Resources.RatingReview.Message_SelectOneRating %></span>
						<sc:GridViewWithCustomPager
							ID="RatingsGrid"
							runat="server"
							SkinID="grid"
							AutoGenerateColumns="false"
							AllowPaging="true"
							PageSize="<%$AppSettings:DefaultGridPageSize%>"
							Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="64%" >
									<ItemTemplate>
										<uc:LiteralEncoded ID="GroupTitleLiteral" runat="server" Text='<%# Eval("Title") %>' ></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
									<ItemTemplate>
										<uc:LiteralEncoded ID="CommonNameLiteral" runat="server" Text='<%# Eval("CommonName") %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
					</div>
				</div>

				<br />

				<div class="popup-rating-buttons" style="vertical-align:bottom">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="AddButton" runat="server" Text="<%$ Resources:General, Button_Ok %>" Enabled="false" SkinID="DefaultButton" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelBottomButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" OnClientClick="CloseRatingsPopup();return false;" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</div>

<div class="left">
	<uc:ImageLinkButton ID="AddRatingsButton" runat="server" Text="<%$ Resources:RatingReview, Button_AddRatings %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
	<ajaxToolkit:ModalPopupExtender 
		runat="server" 
		ID="RatingSelectPopup" 
		TargetControlID="AddRatingsButton"
		PopupControlID="AddRatingsDiv"
		BackgroundCssClass="popup-background"
		Y="20"
		CancelControlID="CancelButton"/>
</div>