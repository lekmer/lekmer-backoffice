﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RatingItemForm.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.RatingItemForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericMultilineTranslator" Src="~/UserControls/Translation/GenericMultilineTranslator.ascx" %>

<script type="text/javascript">
	function ClosePopup() {
		$("div#popup-rating-item-header-center input[name*='CancelButton']").click();
		return false;
	}
</script>

<asp:UpdatePanel ID="RatingItemFormUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<div id="RatingItemDiv" runat="server" class="popup-rating-item-container" style="z-index: 10010; display: none;">
			<div id="popup-rating-item-header">
				<div id="popup-rating-item-header-left">
				</div>

				<div id="popup-rating-item-header-center">
					<span><%= Resources.RatingReview.Literal_RatingItem %></span>
					<input type="button" id="CancelButton" runat="server" value="x"/>
				</div>

				<div id="popup-rating-item-header-right">
				</div>
			</div>

			<div id="popup-rating-item-content">
				<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="RatingItemValidationGroup" />

				<div>
					<div class="column">
						<div class="input-box">
							<span><%=Resources.General.Literal_Title%></span>
							<uc:GenericTranslator ID="TitleTranslator" runat="server" />
							<br />
							<asp:HiddenField ID="IdHiddenField" runat="server" />
							<asp:TextBox ID="TitleTextBox" runat="server" MaxLength="50" />
						</div>

						<div class="input-box">
							<span><%=Resources.General.Literal_Description%></span>
							<asp:ImageButton runat="server" ID="DescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
							<uc:GenericMultilineTranslator ID="DescriptionTranslator" runat="server" />
							<br />
							<asp:TextBox ID="DescriptionTextBox" runat="server" TextMode="MultiLine" Rows="5" MaxLength="255" />
						</div>

						<div class="input-box">
							<span><%=Resources.RatingReview.Literal_Score%> *</span>
							<br />
							<asp:TextBox ID="ScoreTextBox" runat="server" />
							<asp:RequiredFieldValidator ID="ScoreTextBoxValidator" runat="server" ControlToValidate="ScoreTextBox" Display ="None" ErrorMessage="<%$ Resources:RatingReview, ScoreEmpty%>" ValidationGroup="RatingItemValidationGroup" />
							<asp:RangeValidator ID="ScoreTextBoxRangeValidator" runat="server" ControlToValidate="ScoreTextBox" Type="Integer" MaximumValue="10000" MinimumValue="0" ErrorMessage="<%$ Resources:RatingReview, ScoreInvalid%>" ValidationGroup="RatingItemValidationGroup" />
						</div>
					</div>
				</div>
				<div class="clear right">
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="AddButton" runat="server" Text="<%$ Resources:General, Button_Ok %>" SkinID="DefaultButton" ValidationGroup="RatingItemValidationGroup" />
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelBottomButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" OnClientClick="ClosePopup();return false;" />
				</div>
			</div>
		</div>

		<div style="float:left;">
			<uc:ImageLinkButton ID="AddRatingItemButton" runat="server" Text="<%$ Resources:RatingReview, Literal_AddRatingItem %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
			<ajaxToolkit:ModalPopupExtender
				ID="RatingItemPopup"
				runat="server"
				TargetControlID="AddRatingItemButton"
				PopupControlID="RatingItemDiv"
				BackgroundCssClass="popup-background"
				Y="20"
				CancelControlID="CancelButton"/>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>