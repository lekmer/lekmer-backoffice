﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Handlers
{
	/// <summary>
	/// Summary description for ImageRotatorGroupLoadHandler
	/// </summary>
	public class ImageRotatorGroupLoadHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var blockId = Int32.Parse(context.Request["blockId"]);
			var userName = context.Request["userName"];

			var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
			var userFull = systemUserSecureService.GetFullByUserName(userName);

			var imageSecureService = IoC.Resolve<IImageSecureService>();
			var productImageGroupSecureService = IoC.Resolve<IImageRotatorGroupSecureService>();
			
			var imageRotatorGroups = productImageGroupSecureService.GetByBlockId(userFull, blockId);
			var result = new Collection<ImageRotatotMediaLite>();
			foreach (var group in imageRotatorGroups)
			{	
				result.Add(new ImageRotatotMediaLite()
				{
					ImageType = "main",
					GroupId = group.ImageGroupId,
					Url =  group.MainImageId.HasValue ?  ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(group.MainImageId.Value, imageSecureService.GetById(group.MainImageId.Value).FormatExtension)) : ""
					
				});
				result.Add(new ImageRotatotMediaLite()
				{
					ImageType = "thumbnailimage",
					GroupId = group.ImageGroupId,
					Url =  group.ThumbnailImageId.HasValue ?  ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(group.ThumbnailImageId.Value, imageSecureService.GetById(group.ThumbnailImageId.Value).FormatExtension)) : ""			
				});
			}
		
			var js = new JavaScriptSerializer();
			context.Response.ContentType = "application/json";
			context.Response.Write(js.Serialize(result));

		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		public static string ResolveUrl(string originalUrl)
		{
			if (originalUrl == null)
				return null;

			if (originalUrl.IndexOf("://") != -1)
				return originalUrl;


			if (originalUrl.StartsWith("~"))
			{
				string newUrl = "";
				if (HttpContext.Current != null)
				{
					Uri originalUri = HttpContext.Current.Request.Url;
					newUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host +
					         originalUrl.Substring(1).Replace("\\", "/");
				}
				else		
					throw new ArgumentException("Invalid URL: Relative URL not allowed.");
				return newUrl;
			}

			return originalUrl;
		}


	}
		public class ImageRotatotMediaLite
		{
			public string Url { get; set; }
			public string ImageType { get; set; }
			public int GroupId { get; set; }
		}
}