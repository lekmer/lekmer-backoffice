﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class RelationListProducts : StateUserControlController<Collection<IProduct>>
	{
		private ICategory _category;

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IProduct> Products
		{
			get
			{
				object productsStored = State;
				return (Collection<IProduct>)productsStored;
			}
			set
			{
				State = value;
			}
		}

		protected override void SetEventHandlers()
		{
			GVSave.RowDataBound += ProductGridRowDataBound;
			GVSave.RowCommand += ProductGridRowCommand;
			GVSave.PageIndexChanging += ProductGridPageIndexChanging;
			productSearch.SearchEvent += ProductSearchSearchEvent;
			RemoveSelectedButton.Click += RemoveSelectedClick;
		}

		protected override void PopulateControl() { GridDataBind(); }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			GridDataBind();
		}

		private void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GVSave.PageIndex = e.NewPageIndex;
			GridDataBind();
		}

		private void ProductSearchSearchEvent(object sender, ProductSearchEventArgs e)
		{
			foreach (IProduct product in e.Products)
			{
			    int productId = product.Id;
				if (Products.FirstOrDefault(item => item.Id == productId) == null)
				{
					Products.Add(product);
				}
			}
			
			GridDataBind();
			upRelationList.Update();
		}

		private void GridDataBind()
		{
			GVSave.DataSource = Products;
			GVSave.DataBind();
			SetClientFunction();
		}

		private void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var product = (IProduct)row.DataItem;
			var lblPrice = (Label)e.Row.FindControl("lblPrice");
			if (product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}
			var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
			var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
			var productStatus = productStatusSecureService.GetAll().First(item => item.Id == product.ProductStatusId).Title;
			lblProductStatus.Text = productStatus;
		}

		private void ProductGridRowCommand(object sender, CommandEventArgs e)
		{
			int productId;

			if (!int.TryParse(e.CommandArgument.ToString(), out productId))
			{
				productId = 0;
			}

			switch (e.CommandName)
			{
				case "DeleteProduct":
					IProduct product = Products.First(item => item.Id == productId);
					Products.Remove(product);
					break;
			}
			GridDataBind();
		}

		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		protected virtual void RemoveSelectedClick(object sender, EventArgs e)
		{
			var ids = GVSave.GetSelectedIds();
			if (ids.Count() == 0) return;

			foreach (var id in ids)
			{
				var productId = id;
				var product = Products.First(item => item.Id == productId);
				if (product == null) continue;

				Products.Remove(product);
			}
			GridDataBind();
		}

		protected virtual void SetClientFunction()
		{
			if (GVSave.HeaderRow == null) return;
			var selectAllCheckBox = (CheckBox)GVSave.HeaderRow.FindControl("SelectAllCheckBox");
			selectAllCheckBox.Checked = false;
			selectAllCheckBox.Attributes.Add("onclick", 
				"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + GVSave.ClientID + @"'); ShowBulkUpdatePanel('"
				+ selectAllCheckBox.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			foreach (GridViewRow row in GVSave.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick", 
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" 
					+ cbSelect.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}
	}
}