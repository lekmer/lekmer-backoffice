﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class DeliveryTimeConfigurator : StateUserControlController<DeliveryTimeConfiguratorState>
	{
		private Collection<IProductRegistry> _productRegistries;
		public Collection<IProductRegistry> ProductRegistries 
		{
			get { return _productRegistries ?? (_productRegistries = IoC.Resolve<IProductRegistrySecureService>().GetAll()); }
		}

		public ImageButton TriggerImageButton
		{
			get { return ManageButton; }
		}

		public Collection<IDeliveryTime> DataSource { get; set; }

		public string DefaultFromValueControlClientId
		{
			get { return SafeState.DefaultFromValueControlClientId; }
			set { SafeState.DefaultFromValueControlClientId = value; }
		}

		public string DefaultToValueControlClientId
		{
			get { return SafeState.DefaultToValueControlClientId; }
			set { SafeState.DefaultToValueControlClientId = value; }
		}

		public string DefaultFormatValueControlClientId
		{
			get { return SafeState.DefaultFormatValueControlClientId; }
			set { SafeState.DefaultFormatValueControlClientId = value; }
		}

		public int DefaultRegistryId
		{
			get { return SafeState.DefaultRegistryId; }
			set { SafeState.DefaultRegistryId = value; }
		}

		public int BusinessObjectId
		{
			get { return SafeState.BusinessObjectId; }
			set { SafeState.BusinessObjectId = value; }
		}

		protected virtual DeliveryTimeConfiguratorState SafeState
		{
			get { return State ?? (State = new DeliveryTimeConfiguratorState()); }
		}


		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
		}

		protected override void DataBindChildren()
		{
			SetTranslations(DataSource);
			base.DataBindChildren();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			DeliveryTimePopup.BehaviorID = ClientID + "_translatorPopup";
			DeliveryTimePopup.PopupControlID = DeliveryTimeDiv.ClientID;

			CloseDeliveryTimeButton.OnClientClick += "javascript:clearPopup('" + ErorLabel.ClientID + "', '" + DeliveryTimePopup.BehaviorID + "', '" + DeliveryTimeListDiv.ClientID + "'); return false;";
			SaveButton.OnClientClick += "javascript:changeDeliveryTimes('" + DefaultFromValueControlClientId + "', '" + DefaultToValueControlClientId + "', '" + DefaultFormatValueControlClientId + "', '" + DefaultRegistryId + "', '" + ErorLabel.ClientID + "', '" + DeliveryTimePopup.BehaviorID + "', '" + DeliveryTimeListDiv.ClientID + "'); return false;";
			CancelButton.OnClientClick += "javascript:clearPopup('" + ErorLabel.ClientID + "', '" + DeliveryTimePopup.BehaviorID + "', '" + DeliveryTimeListDiv.ClientID + "'); return false;";

			var clientScript = Page.ClientScript;

			if (!clientScript.IsClientScriptBlockRegistered("RemoveRedundantSpaces"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "RemoveRedundantSpaces", @"
					function RemoveRedundantSpaces(str) {
						return str.replace(/(&nbsp;)+|\s+/g, "" "").trim();
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("closePopup"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "closePopup", @"
					function closePopup(b) {
						$find(b).hide();
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("clearRptItem"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "clearRptItem", @"
					function clearRptItem(rptItem) {
						var sibling = rptItem.nextSibling.nextSibling;
						if (rptItem.type === ""text"" && sibling.type === ""hidden"") {
							rptItem.value = sibling.value;
						}
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("setRptTextItem"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "setRptTextItem", @"
					function setRptTextItem(rptItem) {
						var val;
						if (rptItem.type === ""text"") {
								val = RemoveRedundantSpaces(rptItem.value)
								rptItem.value = val;
							}
						return val;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("setRptHiddenItem"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "setRptHiddenItem", @"
					function setRptHiddenItem(rptItem) {
						rptItem.nextSibling.nextSibling.value = rptItem.value;
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("clearPopup"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "clearPopup", @"
					function clearPopup(e,b,l) {
						var errorLabel = document.getElementById(e)
						errorLabel.innerHTML ="""";
						errorLabel.style.display = ""none"";

						var rptDeliveryTimes = document.getElementById(l)
						var rptDeliveryTimeFromValues = rptDeliveryTimes.getElementsByClassName('fromValue');
						var rptDeliveryTimeToValues = rptDeliveryTimes.getElementsByClassName('toValue');
						var rptDeliveryTimeFormatValues = rptDeliveryTimes.getElementsByClassName('formatValue');
						for (var i = 0; i < rptDeliveryTimeFromValues.length; i++) {
							clearRptItem(rptDeliveryTimeFromValues[i]);
							clearRptItem(rptDeliveryTimeToValues[i]);
							clearRptItem(rptDeliveryTimeFormatValues[i]);
						}
						closePopup(b);
						return false;
					};
				", true);
			}
			if (!clientScript.IsClientScriptBlockRegistered("changeDeliveryTimes"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "changeDeliveryTimes", @"
					function changeDeliveryTimes(f,t,ft,r,e,b,l) {
						var isFromNumber = true,isFromInRange = true,isToNumber = true,isToInRange = true,isValidRange = true;
						var defFromValue, defToValue, defFormatValue;

						var rptDeliveryTimes = document.getElementById(l)
						var rptDeliveryTimeFromValues = rptDeliveryTimes.getElementsByClassName('fromValue');
						var rptDeliveryTimeToValues = rptDeliveryTimes.getElementsByClassName('toValue');
						var rptDeliveryTimeFormatValues = rptDeliveryTimes.getElementsByClassName('formatValue');
						for (var i = 0; i < rptDeliveryTimeFromValues.length; i++) {
							var fromVal,toVal,formatVal,regId;

							fromVal = setRptTextItem(rptDeliveryTimeFromValues[i]);
							toVal = setRptTextItem(rptDeliveryTimeToValues[i]);
							formatVal = setRptTextItem(rptDeliveryTimeFormatValues[i]);

							var fromValPreviousSib = rptDeliveryTimeFromValues[i].previousSibling.previousSibling;
							if (rptDeliveryTimeFromValues[i].type === ""text"" && fromValPreviousSib.type === ""hidden"") {
								regId = fromValPreviousSib.value;
							}

							if (regId === r) {
								defFromValue = fromVal;
								defToValue = toVal;
								defFormatValue = formatVal;
							}

							if(fromVal !== '') {
								if(isNaN(fromVal)) {
									isFromNumber = false;
								}
								else if (fromVal <= 0 || fromVal > 100) {
									isFromInRange = false;
								}
							}
							if(toVal !== '') {
								if(isNaN(toVal)) {
									isToNumber = false;
								}
								else if (toVal <= 0 || toVal > 100) {
									isToInRange = false;
								}
							}
							if (fromVal !== '' && toVal !== '' && isFromNumber && isToNumber && (toVal/1) < (fromVal/1)) {
								isValidRange = false;
							}
						}
						if (isFromNumber && isFromInRange && isToNumber && isToInRange && isValidRange) {
							if (r > 0) {
								document.getElementById(f).value = defFromValue;
								document.getElementById(t).value = defToValue;
								document.getElementById(ft).value = defFormatValue;
							}

							for (var i = 0; i < rptDeliveryTimeFromValues.length; i++) {
								setRptHiddenItem(rptDeliveryTimeFromValues[i]);
								setRptHiddenItem(rptDeliveryTimeToValues[i]);
								setRptHiddenItem(rptDeliveryTimeFormatValues[i]);
							}

							closePopup(b);
						}
						else {
							var errorLabel = document.getElementById(e)
							errorLabel.innerHTML = """";
							if (isFromNumber === false || isFromInRange === false) {
								errorLabel.innerHTML = errorLabel.innerHTML  + '" + Resources.LekmerMessage.DeliveryTimeFromIncorrect + @"' + ""<br />"";
							}
							if (isToNumber === false || isToInRange === false) {
								errorLabel.innerHTML = errorLabel.innerHTML  + '" + Resources.LekmerMessage.DeliveryTimeToIncorrect + @"' + ""<br />"";
							}
							if (isValidRange === false) {
								errorLabel.innerHTML = errorLabel.innerHTML  + '" + Resources.LekmerMessage.DeliveryTimeFromToIncorrect + @"' + ""<br />"";
							}
							errorLabel.style.display = ""block"";
						}
						return false;
					};
				", true);
			}
		}


		public virtual void ClearTranslations()
		{
			SetTranslations(GetEmptyDeliveryTimeList());
		}

		public virtual Collection<IDeliveryTime> GetDeliveryTimeList()
		{
			var deliveryTimeList = new Collection<IDeliveryTime>();
			foreach (RepeaterItem item in SimpleDeliveryTimeRepeater.Items)
			{
				var translation = GetDeliveryTime(item);
				deliveryTimeList.Add(translation);
			}
			return deliveryTimeList;
		}

		protected virtual IDeliveryTime GetDeliveryTime(Control item)
		{
			const string tbFromValue = "DeliveryTimeFromValue";
			const string tbToValue = "DeliveryTimeToValue";
			const string tbFormatValue = "DeliveryTimeFormatValue";
			const string hfRegId = "ProductRegistryHidden";

			var hiddenField = item.FindControl(hfRegId) as HiddenField;
			if (hiddenField == null) throw new ArgumentNullException(hfRegId);
			var registryId = Int32.Parse(hiddenField.Value, CultureInfo.CurrentCulture);

			var tbFrom = item.FindControl(tbFromValue) as TextBox;
			if (tbFrom == null) throw new ArgumentNullException(tbFromValue);
			var fromValue = tbFrom.Text.IsNullOrTrimmedEmpty() ? (int?) null : Int32.Parse(tbFrom.Text, CultureInfo.CurrentCulture);

			var tbTo = item.FindControl(tbToValue) as TextBox;
			if (tbTo == null) throw new ArgumentNullException(tbToValue);
			var toValue = tbTo.Text.IsNullOrTrimmedEmpty() ? (int?) null : Int32.Parse(tbTo.Text, CultureInfo.CurrentCulture);

			var tbFormat = item.FindControl(tbFormatValue) as TextBox;
			if (tbFormat == null) throw new ArgumentNullException(tbFormatValue);
			var formatValue = tbFormat.Text;

			return CreateDeliveryTime(registryId, fromValue, toValue, formatValue);
		}

		protected virtual void SetTranslations(Collection<IDeliveryTime> deliveryTimeList)
		{
			foreach (var registry in ProductRegistries)
			{
				var registryId = registry.ProductRegistryId;
				if (deliveryTimeList.FirstOrDefault(dt => dt.ProductRegistryid == registryId) == null)
				{
					deliveryTimeList.Add(CreateDeliveryTime(registryId, null, null, null));
				}
			}

			SimpleDeliveryTimeRepeater.DataSource = deliveryTimeList;
		}

		protected virtual Collection<IDeliveryTime> GetEmptyDeliveryTimeList()
		{
			var deliveryTimeList = new Collection<IDeliveryTime>();
			foreach (var registry in ProductRegistries)
			{
				deliveryTimeList.Add(CreateDeliveryTime(registry.ProductRegistryId, null, null, null));
			}
			return deliveryTimeList;
		}

		protected virtual IDeliveryTime CreateDeliveryTime(int registryId, int? fromValue, int? toValue, string formatValue)
		{
			var deliveryTime = IoC.Resolve<IDeliveryTime>();
			deliveryTime.Id = BusinessObjectId;
			deliveryTime.ProductRegistryid = registryId;
			deliveryTime.FromDays = fromValue;
			deliveryTime.ToDays = toValue;
			deliveryTime.Format = formatValue;
			return deliveryTime;
		}

		protected virtual string GetProductRegistryTitle(object productRegistryId)
		{
			var id = System.Convert.ToInt32(productRegistryId, CultureInfo.CurrentCulture);
			var registry = ProductRegistries.FirstOrDefault(r => r.ProductRegistryId == id);
			return registry != null ? registry.Title : string.Empty;
		}
	}

	[Serializable]
	public sealed class DeliveryTimeConfiguratorState
	{
		public string DefaultFromValueControlClientId { get; set; }
		public string DefaultToValueControlClientId { get; set; }
		public string DefaultFormatValueControlClientId { get; set; }
		public int BusinessObjectId { get; set; }
		public int DefaultRegistryId { get; set; }
	}
}