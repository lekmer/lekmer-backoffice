﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="SizeTableIncludeItemsConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.SizeTableIncludeItemsConfigurator" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>

<div class="campaign-full-left">
<!-- Include Toolbar Start -->
	<div id="includeToolbar" style="width: 100%;">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.png" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenIncludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
						<asp:Button ID="OpenIncludeCategoryBrandPairSearchPopupButton" Text="<%$Resources:Product, Literal_CategoryAndBrand%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content" style="width: 97%;">
			<div class="column">
				<div class="input-box">
					<asp:Label ID="ProductGridPageSize" Text="<%$Resources:Campaign, Literal_ProductGridPageSize%>" runat="server" />
					<br />
					<asp:DropDownList ID="ProductGridPageSizeSelect" runat="server" AutoPostBack="True">
						<asp:ListItem Value="10">10</asp:ListItem>
						<asp:ListItem Value="100">100</asp:ListItem>
						<asp:ListItem Selected="True" Value="250">250</asp:ListItem>
						<asp:ListItem Value="500">500</asp:ListItem>
					</asp:DropDownList>
				</div>
			</div>
			<asp:Panel ID="IncludePanel" runat="server">
				<div style="padding-top:10px;clear:both;"></div>
				<asp:Label ID="LabelProducts" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="ProductIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<sc:GridViewWithCustomPager
							ID="ProductIncludeGrid"
							SkinID="grid"
							runat="server"
							AllowPaging="true"
							AutoGenerateColumns="false"
							Width="100%"
							DataSourceID="IncludeProductDataSource">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="13%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="49%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Category %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TypeLiteral" Text='<%# GetProductType(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmProductRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
						<asp:ObjectDataSource ID="IncludeProductDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethodWithSortingByErpId" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />

						<div runat="server" id="ProductIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected products');" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>

				<div style="padding-top:10px;clear:both;"></div>
				<asp:Label Text="<%$Resources:Product, Literal_CategoryAndBrandList%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="CategoryBrandPairIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="CategoryBrandPairIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Category %>" ItemStyle-Width="25%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryTitleLiteral" Text='<%# Eval("Category.Title")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CategoryPath %>" ItemStyle-Width="55%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryPathLiteral" Text='<%# GetCategoryPathIncludeGrid(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Brand %>" ItemStyle-Width="25%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="BrandTitleLiteral" Text='<%# Eval("Brand.Title")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCategoryBrandPair" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmCategoryBrandPairRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="CategoryBrandPairIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCategoryBrandPairIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected category/brand pairs');" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>
<!-- Include Toolbar End -->
</div>

<!-- Product Search Popup Start -->
	<!-- Include Start -->
<div id="ProductIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="ProductIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="ProductIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="campaign-product-search">
					<div class="content-box">
						<asp:Panel ID="SearchPanel" DefaultButton="ProductIncludePopupSearchButton" runat="server">
						<uc:ProductSearchForm ID="ProductIncludeSearchFormControl" runat="server" />
						<br style="clear:both;" />
						<br />
						<div class="campaign-popup-buttons no-padding">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
						</div>
						</asp:Panel>
						<br class="clear"/>
						<br />
						<uc:ProductSearchResult ID="ProductIncludeSearchResultControl" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="ProductIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeProductSearchPopupButton"
	PopupControlID="ProductIncludePopupDiv" 
	CancelControlID="ProductIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	<!-- Include End -->
<!-- Product Search Popup End -->

<!-- Category/Brand Search Popup Start -->
	<!-- Include Start -->
<div id="CategoryBrandPairIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.Product.Literal_CategoryAndBrand%></span>
			<input type="button" id="CategoryBrandPairIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="CategoryBrandPairIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="input-box">
					<asp:Label ID="ErrorLabel" runat="server" Text="<%$ Resources:GeneralMessage,SizeTableCategoryEmpty%>" style="color: red;" Visible="False" />
				</div>
				<div class="column" style="width: 50%;">
					<div class="input-box campaign-product-search">
						<span><%= Resources.Product.Label_CategoryChoose%></span><br />
						<uc:NodeSelect ID="CategoryNodeSelector" runat="server" AllowClearSelection="true" />
					</div>
				</div>
				<div class="column" style="width: 50%;">
					<div class="input-box">
						<span><%= Resources.Lekmer.Choose_Brand%></span><br />
						<asp:DropDownList ID="ddlBrand" DataTextField="Title" DataValueField="Id" runat="server" Width="100%" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br class="clear" />
		<br class="clear" />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryBrandPairIncludePopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryBrandPairIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="CategoryBrandPairIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeCategoryBrandPairSearchPopupButton"
	PopupControlID="CategoryBrandPairIncludePopupDiv" 
	CancelControlID="CategoryBrandPairIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	<!-- Include End -->
<!-- Category/Brand Search Popup End -->