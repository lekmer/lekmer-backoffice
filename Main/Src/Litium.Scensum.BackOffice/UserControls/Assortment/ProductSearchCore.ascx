﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ProductSearchCore.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ProductSearchCore" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Assortment/ProductSearchCriteria.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<div id="divSearchProducts" style="top:85px;left:95px;width:1036px;overflow:hidden;height:800px;">
	<div id="poput-images-header">
		<div id="poput-images-header-left">
		</div>
		<div id="poput-images-header-center">
			<span><%= Resources.General.Literal_Search %></span>
			<input type="button" id="btnCancel" runat="server" value="x"/>
		</div>
		<div id="poput-images-header-right">
		</div>
	</div>
	<div id="poput-images-content" style="height: 690px;">
		<div id="rl-block">
			<div id="variation-search" class="content-box">
				<span class="assortment-header"><%= Resources.General.Literal_Search %></span>
				<uc:Search ID="search" ShowSearch="true" runat="server" />
			</div>
		</div>
		<div id="rl-poducts" class="clear left">
			<div class="relation-product-buttons">
				<span class="assortment-header"><%= Resources.General.Literal_SearchResults %></span>
				<div class="relation-product-buttonsx">
					<sc:GridViewWithCustomPager
						ID="GVSearch" 
						SkinID="grid" 
						runat="server"
						AllowPaging="true" 
						PageSize="<%$AppSettings:DefaultGridPageSize%>" 
						DataSourceID="productDataSource"
						AutoGenerateColumns="false" 
						Width="100%">
						<Columns>
							<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<HeaderTemplate>
									<asp:CheckBox id="chkAllSearched" runat="server" />
								</HeaderTemplate>
								<ItemTemplate>
									<asp:CheckBox ID="cbSelect" runat="server" />
									<asp:HiddenField ID="hfId" Value='<%#Eval("Id") %>' runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
							
							<asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="10%" />
							<asp:BoundField HeaderText="<%$ Resources:General, Literal_Title %>" DataField="DisplayTitle" ItemStyle-Width="60%" />
							<asp:BoundField HeaderText="<%$ Resources:Product, Literal_Ean %>" DataField="EanCode" ItemStyle-Width="10%" />
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
								<ItemTemplate>
									<asp:Label id="lblProductStatus" runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
								<ItemTemplate>
									<asp:Label ID="lblType" runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Right">
								<ItemTemplate>
									<asp:Label id="lblPrice" runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>
					<asp:ObjectDataSource ID="productDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />
				</div>
				<br style="clear:both;" />
				<div id="AllSelectedDiv" runat="server" class="left" style="display: none;">
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnAdd" runat="server" Text="<%$ Resources:Product, Button_Select %>" SkinID="DefaultButton" />
				</div>
			</div>
		</div>
	 </div>
</div>