﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Helper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class SizeTableIncludeItemsConfigurator : StateUserControlController<ISizeTableIncludeItems>
	{
		public ISizeTableIncludeItems DataSource { get; set; }

		private SizeTableConfiguratorHelper _sizeTableConfiguratorHelper;
		protected SizeTableConfiguratorHelper SizeTableConfiguratorHelper
		{
			get { return _sizeTableConfiguratorHelper ?? (_sizeTableConfiguratorHelper = new SizeTableConfiguratorHelper()); }
		}

		private SizeTableConfiguratorProduct _sizeTableConfiguratorProduct;
		private SizeTableConfiguratorProduct SizeTableConfiguratorProduct
		{
			get { return _sizeTableConfiguratorProduct ?? (_sizeTableConfiguratorProduct = new SizeTableConfiguratorProduct()); }
		}

		private SizeTableConfiguratorCategoryBrandPair _sizeTableConfiguratorCategoryBrandPair;
		private SizeTableConfiguratorCategoryBrandPair SizeTableConfiguratorCategoryBrandPair
		{
			get { return _sizeTableConfiguratorCategoryBrandPair ?? (_sizeTableConfiguratorCategoryBrandPair = new SizeTableConfiguratorCategoryBrandPair()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			if (State != null)
			{
				SizeTableConfiguratorProduct.Products = State.IncludeProducts;
				SizeTableConfiguratorCategoryBrandPair.CategoryBrandPairs = State.IncludeCategoryBrandPairs;
			}

			base.OnLoad(e);
		}

		protected override void SetEventHandlers()
		{
			InitializeConfiguratorIncludeProducts();
			InitializeConfiguratorIncludeCategoryBrandPairs();

			SizeTableConfiguratorProduct.SetEventHandlers();
			SizeTableConfiguratorCategoryBrandPair.SetEventHandlers();

			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		protected override void PopulateControl()
		{
			BindData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void BindData()
		{
			if (DataSource == null)
			{
				throw new ArgumentNullException("DataSource");
			}

			var includeItems = DataSource;
			State = includeItems;

			SizeTableConfiguratorProduct.DataBind(includeItems.IncludeProducts);
			SizeTableConfiguratorCategoryBrandPair.DataBind(includeItems.IncludeCategoryBrandPairs);

			State = includeItems;
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			SizeTableConfiguratorProduct.DataBindGrid(State.IncludeProducts);

			ScriptManager.RegisterStartupScript(this, GetType(), "include_items", "$(document).ready(function() { $('#tabs').tabs('select', 1); });", true);
		}

		public ISizeTableIncludeItems GetResults()
		{
			var includeItems = State;
			return includeItems;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryBrandPairRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryAndBrand + "');}", true);

			const string script = "$(function() {" +
								"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('CategoryBrandPairIncludePopupDiv', 0.4, 0.4, 0.1);" +
								"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(SizeTableConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(SizeTableConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		protected virtual string GetCategoryPathIncludeGrid(object cat)
		{
			return SizeTableConfiguratorCategoryBrandPair.GetCategoryPath(cat);
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = SizeTableConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryBrandPairIncludeApplyToAllSelectedDiv.Style["display"] = SizeTableConfiguratorHelper.HasAnySelectedItem(CategoryBrandPairIncludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = ProductIncludeGrid.Rows.Count > 0 || CategoryBrandPairIncludeGrid.Rows.Count > 0 ? "block" : "none";
		}

		private void InitializeConfiguratorIncludeProducts()
		{
			SizeTableConfiguratorProduct.Grid = ProductIncludeGrid;
			SizeTableConfiguratorProduct.DataSource = IncludeProductDataSource;
			SizeTableConfiguratorProduct.GridPageSizeSelect = ProductGridPageSizeSelect;
			SizeTableConfiguratorProduct.SearchResultControl = ProductIncludeSearchResultControl;
			SizeTableConfiguratorProduct.SearchFormControl = ProductIncludeSearchFormControl;
			SizeTableConfiguratorProduct.SearchButton = ProductIncludePopupSearchButton;
			SizeTableConfiguratorProduct.OkButton = ProductIncludePopupOkButton;
			SizeTableConfiguratorProduct.AddAllButton = ProductIncludePopupAddAllButton;
			SizeTableConfiguratorProduct.CancelButton = ProductIncludePopupCancelButton;
			SizeTableConfiguratorProduct.RemoveSelectionGridButton = RemoveSelectionFromProductIncludeGridButton;
			SizeTableConfiguratorProduct.GridUpdatePanel = ProductIncludeGridUpdatePanel;
			SizeTableConfiguratorProduct.ApplyToAllDiv = ProductIncludeApplyToAllSelectedDiv;
		}

		private void InitializeConfiguratorIncludeCategoryBrandPairs()
		{
			SizeTableConfiguratorCategoryBrandPair.Grid = CategoryBrandPairIncludeGrid;
			SizeTableConfiguratorCategoryBrandPair.OkButton = CategoryBrandPairIncludePopupOkButton;
			SizeTableConfiguratorCategoryBrandPair.CancelButton = CategoryBrandPairIncludePopupCancelButton;
			SizeTableConfiguratorCategoryBrandPair.RemoveSelectionGridButton = RemoveSelectionFromCategoryBrandPairIncludeGridButton;
			SizeTableConfiguratorCategoryBrandPair.GridUpdatePanel = CategoryBrandPairIncludeGridUpdatePanel;
			SizeTableConfiguratorCategoryBrandPair.PopupUpdatePanel = CategoryBrandPairIncludePopupUpdatePanel;
			SizeTableConfiguratorCategoryBrandPair.PopupCategoryTree = CategoryNodeSelector;
			SizeTableConfiguratorCategoryBrandPair.PopupBrands = ddlBrand;
			SizeTableConfiguratorCategoryBrandPair.ApplyToAllDiv = CategoryBrandPairIncludeApplyToAllSelectedDiv;
			SizeTableConfiguratorCategoryBrandPair.Popup = CategoryBrandPairIncludeSearchPopup;
			SizeTableConfiguratorCategoryBrandPair.ErrorLabel = ErrorLabel;
		}
	}
}