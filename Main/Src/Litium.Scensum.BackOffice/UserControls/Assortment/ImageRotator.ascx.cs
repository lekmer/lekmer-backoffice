﻿using System;
using System.Collections.ObjectModel;
using System.Web.UI;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
    public partial class ImageRotator : UserControlController
    {
        public event EventHandler<ImageSelectEventArgs> Selected;
        public event EventHandler FileUploaded;
        public event EventHandler DeleteImage;

        public int? ImageId
        {
            get;
            set;
        }

        public Collection<IImageRotatorGroup> ProductImageGroups
        {
            get
            {
                return ImageGroupControl.GetImageRotatorGroups();
            }
        }

        public Collection<string> Validate()
        {
            return ImageGroupControl.Validate();
        }
        public int ImageRotatorId
        {
            get;
            set;
        }

        protected override void SetEventHandlers()
        {
            ImageSelectControl.Selected += ImageSelected;
            ImageSelectControl.FileUploaded += ImageUnloaded;
            ImageSelectControl.Canceled += ImageSelectCanceled;

            ImageGroupControl.ImageUploaded += ImageGroupImageUploaded;
            ImageGroupControl.DefaltImageSet += ImageGroupControlDefaltImageSet;

        }

        protected override void PopulateControl()
        {
            PopulateImage(ImageId);
            ImageGroupControl.ImageRotatorId = ImageRotatorId;
        }

        protected void PopulateImage(IImage image)
        {

            string imageUrl = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(image.Id, image.FormatExtension));

          
        }

        protected void PopulateImage(int? imageId)
        {
           

            if (imageId.HasValue)
            {
                var imageService = IoC.Resolve<IImageSecureService>();
                IImage image = imageService.GetById((int)imageId);
                PopulateImage(image);
            }
        }

        private void ImageSelectCanceled(object sender, EventArgs e)
        {
        }

        private void ImageUnloaded(object sender, EventArgs e)
        {
            if (FileUploaded != null)
            {
                FileUploaded(this, EventArgs.Empty);
            }
        }

        private void ImageGroupImageUploaded(object sender, EventArgs e)
        {
            if (FileUploaded != null)
            {
                FileUploaded(this, EventArgs.Empty);
            }
        }

        private void ImageGroupControlDefaltImageSet(object sender, ImageSelectEventArgs e)
        {
            PopulateImage(e.Image);
            if (Selected != null)
            {
                Selected(this, e);
            }
        }

        private void ImageSelected(object sender, ImageSelectEventArgs e)
        {
            PopulateImage(e.Image);

            if (Selected != null)
            {
                Selected(this, e);
            }
        }

        protected virtual void DeleteImageButtonClick(object sender, EventArgs e)
        {
            PopulateImage((int?)null);
            if (DeleteImage != null)
            {
                DeleteImage(this, EventArgs.Empty);
            }
        }
    }
}
