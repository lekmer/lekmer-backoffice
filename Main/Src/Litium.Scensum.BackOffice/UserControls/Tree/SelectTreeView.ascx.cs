﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.Tree
{
	public partial class SelectTreeView : UserControl
	{
		public bool RootNodeCheckable { get; set; }
		public string RootNodeTitle { get; set; }
		public Action<int> Navigate;

		private Collection<int> _selectedIds;
		public Collection<int> SelectedIds
		{
			get
			{
				if (_selectedIds == null)
				{
					object ids = ViewState["SelectedIds"];
					_selectedIds = ids != null ? (Collection<int>)ids : new Collection<int>();
					ViewState["SelectedIds"] = _selectedIds;
				}
				return _selectedIds;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
		public Func<int?, Collection<INode>> Selector
		{
			get
			{
				object selector = ViewState["Selector"];
				return (Func<int?, Collection<INode>>)selector;
			}
			set
			{
				ViewState["Selector"] = value;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Tree.NodeCommand += OnNodeCommand;
			Tree.NodeDataBound += OnNodeDataBound;
		}
		public override void DataBind()
		{
			base.DataBind();
			PopulateTreeView(Tree.SelectedNodeId);
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			ScriptManager.RegisterStartupScript(TreeUpdatePanel, TreeUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", Tree.ClientID), true);
		}


		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					Tree.DenySelection = true;
					break;
				case "Navigate":
					if (Navigate != null)
					{
						Navigate(e.Id);
					}
					break;
			}
		}
		private void OnNodeDataBound(object sender, TemplatedTreeViewNodeEventArgs e)
		{
			Control container = ((TreeViewNode) e.Node).ParentContainer;

			var idHiddenField = (HiddenField)container.FindControl("IdHiddenField");
			idHiddenField.Value = e.NodeId.ToString(CultureInfo.InvariantCulture);

			var checkBox = (CheckBox)container.FindControl("SelectCheckBox");
			checkBox.Checked = IsChecked(e.NodeId);
			
			if (e.NodeId == Tree.RootNodeId)
			{
				checkBox.Visible = RootNodeCheckable;
			}
		}
		protected void TreeNodeCheckedChanged(object sender, EventArgs e)
		{
			var selectCheckBox = (CheckBox)sender;
			var idHiddenField = (HiddenField)(selectCheckBox.Parent.FindControl("IdHiddenField"));
			int value = int.Parse(idHiddenField.Value, CultureInfo.InvariantCulture);
			if (selectCheckBox.Checked)
			{
				SelectedIds.Add(value);
			}
			else
			{
				SelectedIds.Remove(value);
			}
		}

		private bool IsChecked(int nodeId)
		{
			foreach (int id in SelectedIds)
			{
				if (nodeId == id)
				{
					return true;
				}
			}
			return false;
		}
		private void PopulateTreeView(int? itemId)
		{
			var id = itemId != Tree.RootNodeId ? itemId : null;
			Tree.DataSource = Selector(id);
			Tree.RootNodeTitle = RootNodeTitle;
			Tree.DataBind();
			Tree.SelectedNodeId = itemId ?? Tree.RootNodeId;
			Tree.DenySelection = true;
		}
	}
}