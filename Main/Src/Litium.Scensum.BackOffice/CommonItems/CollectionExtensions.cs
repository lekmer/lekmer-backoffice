using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.CommonItems
{
	public static class CollectionExtensions
	{
		public static void AddRange<T>(this Collection<T> collection, IEnumerable<T> values)
		{
			foreach (var item in values)
			{
				collection.Add(item);
			}
		}

		public static void ReplaceNullValuesWithEmptyString(this Collection<ITranslationGeneric> collection)
		{
			if (collection != null)
			{
				foreach (var translation in collection)
				{
					if (translation.Value == null)
					{
						translation.Value = string.Empty;
					}
				}
			}
		}

		public static void ReplaceEmptyStringValuesWithNull(this Collection<ITranslationGeneric> collection)
		{
			if (collection != null)
			{
				foreach (var translation in collection)
				{
					if (translation.Value == string.Empty)
					{
						translation.Value = null;
					}
				}
			}
		}

		public static void ReplaceTrimmedEmptyStringValuesWithNull(this Collection<ITranslationGeneric> collection)
		{
			if (collection != null)
			{
				foreach (var translation in collection)
				{
					translation.Value = translation.Value.NullWhenTrimmedEmpty();
				}
			}
		}

		public static void ReplaceTrimmedEmptyStringValuesWithNull(this Collection<ISizeTableRowTranslation> collection)
		{
			if (collection != null)
			{
				foreach (var translation in collection)
				{
					translation.Column1Value = translation.Column1Value.NullWhenTrimmedEmpty();
					translation.Column2Value = translation.Column2Value.NullWhenTrimmedEmpty();
				}
			}
		}
	}
}
