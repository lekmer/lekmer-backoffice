using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Scensum.BackOffice.WebControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls;

namespace Litium.Scensum.BackOffice.CommonItems
{
	public static class TinyMceEditorExtensions
	{
		/// <summary>
		/// Retrun value from editor with extra filtering.
		/// </summary>
		/// <param name="editor"></param>
		/// <returns></returns>
		public static string GetValue(this TinyMceEditor editor)
		{
			string value = RemoveBogusLine(editor.Value);

			value = ApplyActionBeforeGetValue(value);

			return value;
		}

		/// <summary>
		/// Retrun value from editor with extra filtering.
		/// </summary>
		/// <param name="editor"></param>
		/// <returns></returns>
		public static string GetValue(this LekmerTinyMceEditor editor)
		{
			string value = RemoveBogusLine(editor.Value);

			value = ApplyActionBeforeGetValue(value);

			return value;
		}

		/// <summary>
		/// Set editor value with extra filtering.
		/// </summary>
		/// <param name="editor"></param>
		/// <param name="newValue"></param>
		/// <returns></returns>
		public static void SetValue(this TinyMceEditor editor, string newValue)
		{
			editor.Value = ApplyActionBeforeSetValue(newValue);
		}

		/// <summary>
		/// Set editor value with extra filtering.
		/// </summary>
		/// <param name="editor"></param>
		/// <param name="newValue"></param>
		/// <returns></returns>
		public static void SetValue(this LekmerTinyMceEditor editor, string newValue)
		{
			editor.Value = ApplyActionBeforeSetValue(newValue);
		}

		/// <summary>
		/// Get editor 'set' value with extra filtering.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string EditorSetValue(string value)
		{
			return ApplyActionBeforeSetValue(value);
		}

		private static string RemoveBogusLine(string text)
		{
			return text.HasValue() ? text.Replace(Constants.MceBogusLine, string.Empty) : text;
		}

		private static string ApplyActionBeforeGetValue(string text)
		{
			var mediaUrlFormer = IoC.Resolve<IMediaUrlSecureService>();

			return text.HasValue() ? text.Replace(mediaUrlFormer.GetBackOfficeMediaUrl(), "[Channel.MediaArchiveUrl]") : text;
		}

		private static string ApplyActionBeforeSetValue(string text)
		{
			var mediaUrlFormer = IoC.Resolve<IMediaUrlSecureService>();

			return text.HasValue() ? text.Replace("[Channel.MediaArchiveUrl]", mediaUrlFormer.GetBackOfficeMediaUrl()) : text;
		}
	}
}
