using System;
using System.Globalization;
using System.Web;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Base
{
	public static class LekmerPathHelper
	{
		public static string GetReferrerUrl(string referrer)
		{
			return string.Format(CultureInfo.CurrentCulture, "{0}{1}", new object[]
				{
					"~/Modules/",
					referrer
				});
		}

		public static class RatingGroup
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/RatingGroups/Default.aspx";
			}

			public static string GetSearchResultUrl()
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?IsSearchResult=true", new[]
				{
					(object) GetDefaultUrl()
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/RatingGroups/RatingGroupEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/RatingGroups/RatingGroupEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}

			public static string GetEditUrlForCategoryEdit(int groupId, int categoryId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}{2}", GetEditUrl(groupId), HttpUtility.UrlEncode("Assortment/Categories/CategoryEdit.aspx?Id="), categoryId);
			}

			public static string GetEditUrlForProductEdit(int groupId, int productId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}{2}", GetEditUrl(groupId), HttpUtility.UrlEncode("Assortment/Products/ProductEdit.aspx?Id="), productId);
			}

			public static string GetEditUrlForBlockProductRatingSummaryEdit(int groupId, int pageId, int blockId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}", GetEditUrl(groupId),
					HttpUtility.UrlEncode(
						string.Format(CultureInfo.CurrentCulture, "Assortment/Blocks/BlockProductRatingSummaryEdit.aspx?Id={0}&BlockId={1}", pageId, blockId)
					)
				);
			}
		}

		public static class Rating
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/Ratings/Default.aspx";
			}

			public static string GetSearchResultUrl()
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?IsSearchResult=true", new[]
				{
					(object) GetDefaultUrl()
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/Ratings/RatingEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Ratings/RatingEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}

			public static string GetEditUrlForRatingGroupEdit(int id, int ratingGroupId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}{2}", GetEditUrl(id), HttpUtility.UrlEncode("Assortment/RatingGroups/RatingGroupEdit.aspx?Id="), ratingGroupId);
			}

			public static string GetEditUrlForBlockProductRatingSummaryEdit(int ratingId, int pageId, int blockId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}", GetEditUrl(ratingId), 
					HttpUtility.UrlEncode(
						string.Format(CultureInfo.CurrentCulture, "Assortment/Blocks/BlockProductRatingSummaryEdit.aspx?Id={0}&BlockId={1}", pageId, blockId)
					)
				);
			}

			public static string GetEditUrlForBlockProductMostHelpfulRatingEdit(int ratingId, int pageId, int blockId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}", GetEditUrl(ratingId),
					HttpUtility.UrlEncode(
						string.Format(CultureInfo.CurrentCulture, "Assortment/Blocks/BlockProductMostHelpfulRatingEdit.aspx?Id={0}&BlockId={1}", pageId, blockId)
					)
				);
			}

			public static string GetEditUrlForBlockProductFilterEdit(int ratingId, int pageId, int blockId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}", GetEditUrl(ratingId),
					HttpUtility.UrlEncode(
						string.Format(CultureInfo.CurrentCulture, "Assortment/Blocks/BlockProductFilterEdit.aspx?Id={0}&BlockId={1}", pageId, blockId)
					)
				);
			}

			public static string GetEditUrlForBlockBestRatedProductListEdit(int ratingId, int pageId, int blockId)
			{
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}", GetEditUrl(ratingId),
					HttpUtility.UrlEncode(
						string.Format(CultureInfo.CurrentCulture, "Assortment/Blocks/BlockBestRatedProductListEdit.aspx?Id={0}&BlockId={1}", pageId, blockId)
					)
				);
			}
		}

		public static class RatingGroupFolder
		{
			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/RatingGroups/RatingGroupFolderEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/RatingGroups/RatingGroupFolderEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class RatingFolder
		{
			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/Ratings/RatingFolderEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Ratings/RatingFolderEdit.aspx", (object) id
				});
			}
		}

		public static class Category
		{
			public static string GetEditUrlForProductEdit(int id, int productId)
			{
				var editUrl = PathHelper.Assortment.Category.GetEditUrl(id);

				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}{2}", editUrl, HttpUtility.UrlEncode("Assortment/Products/ProductEdit.aspx?Id="), productId);
			}
		}

		public static class Package
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/Packages/Default.aspx";
			}

			public static string GetEditUrl()
			{
				return "~/Modules/Assortment/Packages/PackageEdit.aspx";
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					GetEditUrl(), (object) id
				});
			}
		}


		public static class Ad
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/SiteStructure/EsalesAds/Default.aspx";
			}

			public static string GetSearchResultUrl()
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?IsSearchResult=true", new[]
				{
					(object) GetDefaultUrl()
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				if (parentFolderId > 0)
				{
					return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
						{
							"~/Modules/SiteStructure/EsalesAds/AdEdit.aspx", (object) parentFolderId
						});
				}

				return "~/Modules/SiteStructure/EsalesAds/AdEdit.aspx";
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/SiteStructure/EsalesAds/AdEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class AdFolder
		{
			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				if (parentFolderId > 0)
				{
					return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
						{
							"~/Modules/SiteStructure/EsalesAds/AdFolderEdit.aspx", (object) parentFolderId
						});
				}

				return "~/Modules/SiteStructure/EsalesAds/AdFolderEdit.aspx";
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/SiteStructure/EsalesAds/AdFolderEdit.aspx", (object) id
				});
			}
		}

		public static class Icon
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Media/Icons/Default.aspx";
			}

			public static string GetEditUrl()
			{
				return "~/Modules/Media/Icons/IconEdit.aspx";
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					GetEditUrl(), (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class Sizes
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/Sizes/SizeSorting.aspx";
			}

			public static string GetEditUrl()
			{
				return "~/Modules/Assortment/Sizes/SizesEdit.aspx";
			}

			public static string GetSizeCreateUrl()
			{
				return "~/Modules/Assortment/Sizes/SizeEdit.aspx";

			}

			public static string GetSizeEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Sizes/SizeEdit.aspx", (object) id
				});
			}
		}

		public static class SizeTable
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/Sizes/Default.aspx";
			}

			public static string GetSearchResultUrl()
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?IsSearchResult=true", new[]
				{
					(object) GetDefaultUrl()
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/Sizes/SizeTableEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Sizes/SizeTableEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class SizeTableFolder
		{
			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/Sizes/SizeTableFolderEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Sizes/SizeTableFolderEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class Newsletter
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Customers/Newsletter/Default.aspx";
			}

			public static string GetEditReferrerUrl(string pageUrl)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Customers/{0}", pageUrl);
			}

			public static string GetEditUrlForDefault(int subscriberId)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Customers/Newsletter/SubscriberEdit.aspx?Id={0}&Referrer={1}", subscriberId, HttpUtility.UrlEncode("Newsletter/Default.aspx"));
			}
		}

		public static class ContentMessage
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/Messages/Default.aspx";
			}

			public static string GetSearchResultUrl()
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?IsSearchResult=true", new[]
				{
					(object) GetDefaultUrl()
				});
			}

			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/Messages/ContentMessageEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Messages/ContentMessageEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class ContentMessageFolder
		{
			public static string GetCreateUrl(int parentFolderId)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?parentFolderId={1}", new[]
				{
					"~/Modules/Assortment/Messages/ContentMessageFolderEdit.aspx", (object) parentFolderId
				});
			}

			public static string GetEditUrl(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?Id={1}", new[]
				{
					"~/Modules/Assortment/Messages/ContentMessageFolderEdit.aspx", (object) id
				});
			}

			public static string GetEditUrlWithMessage(int id)
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}&HasMessage=true", new[]
				{
					(object) GetEditUrl(id)
				});
			}
		}

		public static class Review
		{
			public static string GetDefaultUrl()
			{
				return "Assortment/Reviews/Default.aspx";
			}

			public static string GetEditUrl(int id)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Assortment/Reviews/ReviewEdit.aspx?Id={0}&Referrer={1}", id, HttpUtility.UrlEncode(GetDefaultUrl()));
			}
		}

		public static class Tags
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/ProductTags/Default.aspx";
			}

			public static string GetTagGroupEditUrl(int id)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Assortment/ProductTags/TagGroupEdit.aspx?Id={0}&Referrer={1}", id, HttpUtility.UrlEncode("Assortment/ProductTags/Default.aspx"));
			}
			public static string GetTagEditUrl(int id, int tagGroupId)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Assortment/ProductTags/TagEdit.aspx?Id={0}&Referrer={1}{2}", id, HttpUtility.UrlEncode("Assortment/ProductTags/TagGroupEdit.aspx?Id="), tagGroupId);
			}
		}

		public static class Suppliers
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Assortment/Suppliers/Default.aspx";
			}
			public static string GetEditUrl(int id)
			{
				return GetEditUrl(id, HttpUtility.UrlEncode("Assortment/Suppliers/Default.aspx"));
			}
			public static string GetEditUrlFromSearch(int id)
			{
				return GetEditUrl(id, HttpUtility.UrlEncode("Assortment/Suppliers/Default.aspx?IsSearchResult=true"));
			}
			private static string GetEditUrl(int id, string referer)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Assortment/Suppliers/SupplierEdit.aspx?Id={0}&Referrer={1}", id, referer);
			}
			public static string GetSearchResultUrl()
			{
				return String.Format(CultureInfo.CurrentCulture, "{0}?IsSearchResult=true", new[]
				{
					(object) GetDefaultUrl()
				});
			}
		}

		public static class DropShip
		{
			public static string GetDefaultUrl()
			{
				return "~/Modules/Customers/DropShip/Default.aspx";
			}
			private static string GetEditUrl(int orderId)
			{
				return string.Format(CultureInfo.CurrentCulture, "~/Modules/Customers/DropShip/DropShipInfoEdit.aspx?OrderId={0}", orderId);
			}
			public static string GetEditUrlForDefault(int orderId)
			{
				var editUrl = GetEditUrl(orderId);
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}", editUrl, "Customers/DropShip/Default.aspx");
			}
			public static string GetEditUrlForOrderEdit(int orderId)
			{
				var editUrl = GetEditUrl(orderId);
				return string.Format(CultureInfo.CurrentCulture, "{0}&Referrer={1}{2}", editUrl, HttpUtility.UrlEncode("Customers/Orders/OrderEdit.aspx?Id="), orderId);
			}
		}
	}
}