using Litium.Framework.Setting;

namespace Litium.Scensum.BackOffice.Setting
{
	public class SiteStructureBlockSetting : SettingBase
	{
		private const string _defaultGroupName = "Default";
		private readonly string _groupName;

		public SiteStructureBlockSetting(string groupName)
		{
			_groupName = groupName;
		}

		protected override string StorageName
		{
			get { return "SiteStructureBlock"; }
		}

		public string EditPageName
		{
			get
			{
				const string settingName = "EditPageName";
				string connectionString = GetString(_groupName, settingName, null);
				if (connectionString != null) return connectionString;
				return GetString(_defaultGroupName, settingName);
			}
		}
	}
}