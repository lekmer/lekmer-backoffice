using Litium.Framework.Setting;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.Setting
{
	/// <summary>
	/// Settings for the media module.
	/// </summary>
	public sealed class TemplateImageSetting : SettingBase
	{
		#region Singleton

		private TemplateImageSetting()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static TemplateImageSetting Instance
		{
			get { return SingletonCreator.CreatorInstance; }
		}

		private static class SingletonCreator
		{
			private static readonly TemplateImageSetting _instance = new TemplateImageSetting();

			public static TemplateImageSetting CreatorInstance
			{
				get { return _instance; }
			}
		}

		#endregion

		private const string _groupName = "TemplateImage.BackOffice";

		/// <summary>
		/// Name of setting storage.
		/// </summary>
		protected override string StorageName
		{
			get { return "TemplateImage.BackOffice"; }
		}

		/// <summary>
		/// Gets the normal quality.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
		public int NormalQuality
		{
			get
			{
				int quality = GetInt32(_groupName, "NormalQuality", 90);
				if (quality < 1 || quality > 100)
				{
					throw new SettingConfigurationException("Incorect quality value (quality must be 1-100).");
				}
				return quality;
			}
		}

		/// <summary>
		/// Gets the normal width.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
		public int NormalWidth
		{
			get
			{
				int normalWidth = GetInt32(_groupName, "NormalWidth", 150);
				if (normalWidth < 1)
				{
					throw new SettingConfigurationException("Incorect NormalWidth value (normal width must be > 0).");
				}
				return normalWidth;
			}
		}

		/// <summary>
		/// Gets the normal height.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
		public int NormalHeight
		{
			get
			{
				int normalHeight = GetInt32(_groupName, "NormalHeight", 150);
				if (normalHeight < 1)
				{
					throw new SettingConfigurationException("Incorect NormalHeight value (normal height must be > 0).");
				}
				return normalHeight;
			}
		}
		/// <summary>
		/// Gets the thumbnail quality.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
		public int ThumbnailQuality
		{
			get
			{
				int quality = GetInt32(_groupName, "ThumbnailQuality", 80);
				if (quality < 1 || quality > 100)
				{
					throw new SettingConfigurationException("Incorect quality value (quality must be 1-100).");
				}
				return quality;
			}
		}

		/// <summary>
		/// Gets the thumbnail width.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
		public int ThumbnailWidth
		{
			get
			{
				int thumbnailWidth = GetInt32(_groupName, "ThumbnailWidth", 50);
				if (thumbnailWidth < 1)
				{
					throw new SettingConfigurationException("Incorect ThumbnailWidth value (thumbnail width must be > 0).");
				}
				return thumbnailWidth;
			}
		}

		/// <summary>
		///  Gets the thumbnail height.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
		public int ThumbnailHeight
		{
			get
			{
				int thumbnailHeight = GetInt32(_groupName, "ThumbnailHeight", 50);
				if (thumbnailHeight < 1)
				{
					throw new SettingConfigurationException("Incorect ThumbnailHeight value (thumbnail height must be > 0).");
				}
				return thumbnailHeight;
			}
		}
	}
}
