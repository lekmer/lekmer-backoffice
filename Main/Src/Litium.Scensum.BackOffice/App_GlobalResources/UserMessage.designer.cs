//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UserMessage {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserMessage() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.UserMessage", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activation date should not be empty.
        /// </summary>
        internal static string ActivationDateEmpty {
            get {
                return ResourceManager.GetString("ActivationDateEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expiration date should be greater than activation date.
        /// </summary>
        internal static string ActivationDateGreaterExpirationDate {
            get {
                return ResourceManager.GetString("ActivationDateGreaterExpirationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Incorrect format of activation date.
        /// </summary>
        internal static string ActivationDateIncorectFormat {
            get {
                return ResourceManager.GetString("ActivationDateIncorectFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This system role can&apos;t be deleted because it&apos;s assigned to the following system users:.
        /// </summary>
        internal static string CantDeleteRole {
            get {
                return ResourceManager.GetString("CantDeleteRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expiration date should not be empty.
        /// </summary>
        internal static string ExpirationDateEmpty {
            get {
                return ResourceManager.GetString("ExpirationDateEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Incorrect format of expiration date.
        /// </summary>
        internal static string ExpirationDateIncorrectFormat {
            get {
                return ResourceManager.GetString("ExpirationDateIncorrectFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name should not be empty.
        /// </summary>
        internal static string NameEmpty {
            get {
                return ResourceManager.GetString("NameEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role with this title exist!.
        /// </summary>
        internal static string RoleWithSameTitle {
            get {
                return ResourceManager.GetString("RoleWithSameTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User with this username exist!.
        /// </summary>
        internal static string UserWithSameUserName {
            get {
                return ResourceManager.GetString("UserWithSameUserName", resourceCulture);
            }
        }
    }
}
