//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SiteStructure {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SiteStructure() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.SiteStructure", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add area.
        /// </summary>
        internal static string Button_AddArea {
            get {
                return ResourceManager.GetString("Button_AddArea", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear Selection.
        /// </summary>
        internal static string Button_ClearSelection {
            get {
                return ResourceManager.GetString("Button_ClearSelection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Link.
        /// </summary>
        internal static string Button_CreateLink {
            get {
                return ResourceManager.GetString("Button_CreateLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page.
        /// </summary>
        internal static string Button_CreatePage {
            get {
                return ResourceManager.GetString("Button_CreatePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shortcut.
        /// </summary>
        internal static string Button_CreateShortcut {
            get {
                return ResourceManager.GetString("Button_CreateShortcut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move.
        /// </summary>
        internal static string Button_Move {
            get {
                return ResourceManager.GetString("Button_Move", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access.
        /// </summary>
        internal static string Literal_Access {
            get {
                return ResourceManager.GetString("Literal_Access", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add area.
        /// </summary>
        internal static string Literal_AddArea {
            get {
                return ResourceManager.GetString("Literal_AddArea", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        internal static string Literal_All {
            get {
                return ResourceManager.GetString("Literal_All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bring online.
        /// </summary>
        internal static string Literal_BringOnline {
            get {
                return ResourceManager.GetString("Literal_BringOnline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change master page.
        /// </summary>
        internal static string Literal_ChangeMasterPage {
            get {
                return ResourceManager.GetString("Literal_ChangeMasterPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change page template.
        /// </summary>
        internal static string Literal_ChangePageTemplate {
            get {
                return ResourceManager.GetString("Literal_ChangePageTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Child pages.
        /// </summary>
        internal static string Literal_ChildPages {
            get {
                return ResourceManager.GetString("Literal_ChildPages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Common name (navigation).
        /// </summary>
        internal static string Literal_CommonNameNavigation {
            get {
                return ResourceManager.GetString("Literal_CommonNameNavigation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Content.
        /// </summary>
        internal static string Literal_Content {
            get {
                return ResourceManager.GetString("Literal_Content", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Content Nodes.
        /// </summary>
        internal static string Literal_ContentNodes {
            get {
                return ResourceManager.GetString("Literal_ContentNodes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create block.
        /// </summary>
        internal static string Literal_CreateBlock {
            get {
                return ResourceManager.GetString("Literal_CreateBlock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create child.
        /// </summary>
        internal static string Literal_CreateChild {
            get {
                return ResourceManager.GetString("Literal_CreateChild", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create link.
        /// </summary>
        internal static string Literal_CreateLink {
            get {
                return ResourceManager.GetString("Literal_CreateLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create master page.
        /// </summary>
        internal static string Literal_CreateMasterPage {
            get {
                return ResourceManager.GetString("Literal_CreateMasterPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create page.
        /// </summary>
        internal static string Literal_CreatePage {
            get {
                return ResourceManager.GetString("Literal_CreatePage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create page from master.
        /// </summary>
        internal static string Literal_CreatePageFromMaster {
            get {
                return ResourceManager.GetString("Literal_CreatePageFromMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create shortcut.
        /// </summary>
        internal static string Literal_CreateShortcut {
            get {
                return ResourceManager.GetString("Literal_CreateShortcut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current master page.
        /// </summary>
        internal static string Literal_CurrentMasterPage {
            get {
                return ResourceManager.GetString("Literal_CurrentMasterPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current page template.
        /// </summary>
        internal static string Literal_CurrentPageTemplate {
            get {
                return ResourceManager.GetString("Literal_CurrentPageTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Destination.
        /// </summary>
        internal static string Literal_Destination {
            get {
                return ResourceManager.GetString("Literal_Destination", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit area.
        /// </summary>
        internal static string Literal_EditArea {
            get {
                return ResourceManager.GetString("Literal_EditArea", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit page.
        /// </summary>
        internal static string Literal_EditPage {
            get {
                return ResourceManager.GetString("Literal_EditPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Folder.
        /// </summary>
        internal static string Literal_Folder {
            get {
                return ResourceManager.GetString("Literal_Folder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hidden.
        /// </summary>
        internal static string Literal_Hidden {
            get {
                return ResourceManager.GetString("Literal_Hidden", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to inherit from parent.
        /// </summary>
        internal static string Literal_InheritFromParent {
            get {
                return ResourceManager.GetString("Literal_InheritFromParent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Link.
        /// </summary>
        internal static string Literal_Link {
            get {
                return ResourceManager.GetString("Literal_Link", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Make hidden.
        /// </summary>
        internal static string Literal_MakeHidden {
            get {
                return ResourceManager.GetString("Literal_MakeHidden", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master page.
        /// </summary>
        internal static string Literal_MasterPage {
            get {
                return ResourceManager.GetString("Literal_MasterPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This master page is used by following pages:.
        /// </summary>
        internal static string Literal_MasterPageIsUsedBy {
            get {
                return ResourceManager.GetString("Literal_MasterPageIsUsedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master template.
        /// </summary>
        internal static string Literal_MasterTemplate {
            get {
                return ResourceManager.GetString("Literal_MasterTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to META description, for search engine indexing. Enter a describing text for this
        ///								page, not longer than 500 characters..
        /// </summary>
        internal static string Literal_MetaDescription {
            get {
                return ResourceManager.GetString("Literal_MetaDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to META keywords, for search engine indexing. Enter a comma separated list of keywords..
        /// </summary>
        internal static string Literal_MetaKeywords {
            get {
                return ResourceManager.GetString("Literal_MetaKeywords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move.
        /// </summary>
        internal static string Literal_Move {
            get {
                return ResourceManager.GetString("Literal_Move", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move block.
        /// </summary>
        internal static string Literal_MoveBlock {
            get {
                return ResourceManager.GetString("Literal_MoveBlock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New master page.
        /// </summary>
        internal static string Literal_NewMasterPage {
            get {
                return ResourceManager.GetString("Literal_NewMasterPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New page template.
        /// </summary>
        internal static string Literal_NewPageTemplate {
            get {
                return ResourceManager.GetString("Literal_NewPageTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NotSignedIn.
        /// </summary>
        internal static string Literal_NotSignedIn {
            get {
                return ResourceManager.GetString("Literal_NotSignedIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offline.
        /// </summary>
        internal static string Literal_Offline {
            get {
                return ResourceManager.GetString("Literal_Offline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Online.
        /// </summary>
        internal static string Literal_Online {
            get {
                return ResourceManager.GetString("Literal_Online", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page.
        /// </summary>
        internal static string Literal_Page {
            get {
                return ResourceManager.GetString("Literal_Page", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page from master.
        /// </summary>
        internal static string Literal_PageFromMaster {
            get {
                return ResourceManager.GetString("Literal_PageFromMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pages.
        /// </summary>
        internal static string Literal_Pages {
            get {
                return ResourceManager.GetString("Literal_Pages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page template.
        /// </summary>
        internal static string Literal_PageTemplate {
            get {
                return ResourceManager.GetString("Literal_PageTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page title.
        /// </summary>
        internal static string Literal_PageTitle {
            get {
                return ResourceManager.GetString("Literal_PageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The page title (TITLE tag) is important for search engine optimization..
        /// </summary>
        internal static string Literal_PageTitleImportanSEO {
            get {
                return ResourceManager.GetString("Literal_PageTitleImportanSEO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Page type.
        /// </summary>
        internal static string Literal_PageType {
            get {
                return ResourceManager.GetString("Literal_PageType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Path.
        /// </summary>
        internal static string Literal_Path {
            get {
                return ResourceManager.GetString("Literal_Path", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search engine optimization.
        /// </summary>
        internal static string Literal_SearchEngineOptimization {
            get {
                return ResourceManager.GetString("Literal_SearchEngineOptimization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to selected blocks.
        /// </summary>
        internal static string Literal_selectedBlocksConfirmDelete {
            get {
                return ResourceManager.GetString("Literal_selectedBlocksConfirmDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selection container panel.
        /// </summary>
        internal static string Literal_SelectionContainerPanel {
            get {
                return ResourceManager.GetString("Literal_SelectionContainerPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select placement.
        /// </summary>
        internal static string Literal_SelectPlacement {
            get {
                return ResourceManager.GetString("Literal_SelectPlacement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SEO.
        /// </summary>
        internal static string Literal_SEO {
            get {
                return ResourceManager.GetString("Literal_SEO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set offline.
        /// </summary>
        internal static string Literal_SetOffline {
            get {
                return ResourceManager.GetString("Literal_SetOffline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set start page.
        /// </summary>
        internal static string Literal_SetStartPage {
            get {
                return ResourceManager.GetString("Literal_SetStartPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        internal static string Literal_Settings {
            get {
                return ResourceManager.GetString("Literal_Settings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shortcut.
        /// </summary>
        internal static string Literal_Shortcut {
            get {
                return ResourceManager.GetString("Literal_Shortcut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SignedIn.
        /// </summary>
        internal static string Literal_SignedIn {
            get {
                return ResourceManager.GetString("Literal_SignedIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Site Structure.
        /// </summary>
        internal static string Literal_SiteStructure {
            get {
                return ResourceManager.GetString("Literal_SiteStructure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sort children.
        /// </summary>
        internal static string Literal_SortChildren {
            get {
                return ResourceManager.GetString("Literal_SortChildren", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specify page title.
        /// </summary>
        internal static string Literal_SpecifyPageTitle {
            get {
                return ResourceManager.GetString("Literal_SpecifyPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start page.
        /// </summary>
        internal static string Literal_StartPage {
            get {
                return ResourceManager.GetString("Literal_StartPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Title (navigation).
        /// </summary>
        internal static string Literal_TitleNavigation {
            get {
                return ResourceManager.GetString("Literal_TitleNavigation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to URL.
        /// </summary>
        internal static string Literal_URL {
            get {
                return ResourceManager.GetString("Literal_URL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to URL title.
        /// </summary>
        internal static string Literal_URLTitle {
            get {
                return ResourceManager.GetString("Literal_URLTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use master page.
        /// </summary>
        internal static string Literal_UseMaster {
            get {
                return ResourceManager.GetString("Literal_UseMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use page template.
        /// </summary>
        internal static string Literal_UseTemplate {
            get {
                return ResourceManager.GetString("Literal_UseTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use theme.
        /// </summary>
        internal static string Literal_UseTheme {
            get {
                return ResourceManager.GetString("Literal_UseTheme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pages.
        /// </summary>
        internal static string Tab_Pages {
            get {
                return ResourceManager.GetString("Tab_Pages", resourceCulture);
            }
        }
    }
}
