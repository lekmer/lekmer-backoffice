using System;
using System.Globalization;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.Master
{
	public partial class Start : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			ChannelList.SelectedIndexChanged += DdlChannels_SelectedIndexChanged;
			ChannelEditButton.Click += BtnEditChannel_Click;
			SignOutButton.Click += LBSignOut_Click;
			Page.ClientScript.RegisterClientScriptInclude("somescript", ResolveUrl("~/Media/Scripts/common.js"));
		}
        protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack)
			{
				PopulateForm();
			}
			Response.AddHeader("Cache-Control", "no-cache; no-store;");
		}

		public virtual void SetActiveTab(string activeMainTab,string activeSubTab)
		{
			MainTabMenu.ActiveMainTab = activeMainTab;
			MainTabMenu.ActiveSubTab = activeSubTab;
		}

		protected virtual void DdlChannels_SelectedIndexChanged(object sender, EventArgs e)
		{
			ChannelHelper.CurrentChannel = IoC.Resolve<IChannelSecureService>().GetById(int.Parse(ChannelList.SelectedValue, CultureInfo.CurrentCulture));
			ViewPublicSiteButton.HRef = "http://" + ChannelHelper.CurrentChannel.ApplicationName + "/";
			Response.Redirect(Request.Url.PathAndQuery);
		}
		protected virtual void BtnEditChannel_Click(object sender, EventArgs e)
		{
            Response.Redirect(PathHelper.Channel.GetDefaultUrl());
		}
		protected virtual void LBSignOut_Click(object sender, EventArgs e)
		{
			Session.Abandon();
            Response.Redirect(PathHelper.GetStartUrl());
		}

		protected virtual void PopulateForm()
		{
			ChannelList.DataSource = IoC.Resolve<IChannelSecureService>().GetAll();
			ChannelList.DataBind();
			ChannelList.SelectedValue = ChannelHelper.CurrentChannel.Id.ToString(CultureInfo.CurrentCulture);

			if (!IoC.Resolve<IAccessValidator>().HasAccess(SignInHelper.SignedInSystemUser, PrivilegeConstant.Channel))
			{
				ChannelEditButton.Visible = false;
			}
			UserNameLabel.Text = SignInHelper.SignedInSystemUser.Name;
			ViewPublicSiteButton.HRef = "http://" + ChannelHelper.CurrentChannel.ApplicationName + "/";
		}
	}
}
