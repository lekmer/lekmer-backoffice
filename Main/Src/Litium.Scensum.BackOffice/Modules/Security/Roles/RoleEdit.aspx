﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Security/Roles/Role.Master" CodeBehind="RoleEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Security.Roles.RoleEdit" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">.

		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
								runat="server" />
							<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
								ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgRoleEdit" />
								
					<uc:MessageContainer ID="SaveMesagerContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
						runat="server" />
				</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="RoleEditTabContent" ContentPlaceHolderID="RolePlaceHolder" runat="server">
	<asp:UpdatePanel ID="EditUpdatePanel" runat="server">
		<ContentTemplate>		
			<div id="role-edit">
				<div id="role-edit-container">
					<asp:Label id="RoleEditCreateLabel" class="assortment-header" runat="server" />
					<br/>
					<div class="input-box">
						<span><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Title%>" /> *</span><br />
						<asp:TextBox ID="TitleTextBox" runat="server" />
						<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>"  Display="None" ValidationGroup="vgRoleEdit" />
					</div>
					<br/>
					<div class= "user-roles">
						<div class = "user-roles-list-box">
						  <label><asp:Literal runat="server" Text="<%$ Resources:User, Literal_AvailablePrivileges%>" /></label>
						  <br />
						  <asp:ListBox id="PrivilegesForSelectionListBox" DataTextField="CommonName" DataValueField="Id"
						   Rows="10"
						   Width="230px"
						   SelectionMode ="Multiple"
						   runat="server">
						   </asp:ListBox>
						</div>
						<div class = "user-roles-button">
						<uc:ImageLinkButton ID="AddPrivilegesButton" runat="server" Text=">>" SkinID="DefaultButton" />
						<br />
						<br />
						<uc:ImageLinkButton ID="RemovePrivilegesButton" runat="server" Text="<<" SkinID="DefaultButton" />
						</div>
						<div class = "user-roles-list-box">
						  <label> <asp:Literal runat="server" Text="<%$ Resources:User, Literal_SelectedPrivileges%>" /></label>
						  <br />
						  <asp:ListBox id="SelectedPrivilegesListBox" DataTextField="CommonName" DataValueField="Id"
						   Rows="10"
						   Width="230px"
						   SelectionMode ="Multiple"
						   runat="server">
						   </asp:ListBox>
						</div>
					</div>
					<br class="clear" />
					<br />
					<asp:UpdatePanel ID="ButtonUpdatePanel" runat="server">
						<ContentTemplate>
							<div class="buttons left">
                        <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('role');" />
                    </div>
                    <div class="buttons right">
								<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgRoleEdit"
									SkinID="DefaultButton" />
								<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" CausesValidation="false"
									SkinID="DefaultButton" />
							</div>						
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>