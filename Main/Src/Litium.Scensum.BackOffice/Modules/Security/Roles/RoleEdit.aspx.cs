﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;


namespace Litium.Scensum.BackOffice.Modules.Security.Roles
{
	public partial class RoleEdit : LekmerStatePageController<RoleState>, IEditor
	{
		protected virtual Collection<IPrivilege> GetRolePrivilegesState(int? roleId)
		{
			Collection<IPrivilege> privilegesList;
			object privilegesListStored = State == null ? null : State.TempRolesPrivileges;
			if (privilegesListStored != null)
			{
				privilegesList = (Collection<IPrivilege>)privilegesListStored;
			}
			else
			{
				privilegesList = roleId != null ? (IoC.Resolve<IPrivilegeSecureService>().GetAllByRole((int)roleId)?? new Collection<IPrivilege>()) : new Collection<IPrivilege>();
				SetTempRolesPrivilege(privilegesList);
			}
			return privilegesList;
		}
		protected override void SetEventHandlers()
		{
			AddPrivilegesButton.Click += AddPrivilegesClick;
			RemovePrivilegesButton.Click += RemovePrivilegesClick;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            var users = IoC.Resolve<ISystemUserSecureService>().GetAllByAssignedRole(GetId());

            if (users.Count > 0)
            {
                var sb = new StringBuilder(Resources.UserMessage.CantDeleteRole);
                foreach (var user in users)
                {
                    sb.Append("'");
                    sb.Append(user.UserName);
                    sb.Append("', ");
                }
                messager.Add(sb.Remove(sb.Length - 2, 2).Append('.').ToString());
            }
            else
            {
                IoC.Resolve<ISystemRoleSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
                Response.Redirect(PathHelper.Role.GetDefaultUrl());
            }
            
        }

		protected void SetTempRolesPrivilege(Collection<IPrivilege> privileges)
		{
			EnsureState();
			State.TempRolesPrivileges = privileges;
		}
		protected void EnsureState()
		{
			if (State == null)
			{
				State = new RoleState();
			}
		}

		private void AddPrivilegesClick(object sender, EventArgs e)
		{
			Collection<IPrivilege> privilegesList = GetRolePrivilegesState(GetIdOrNull());

			foreach (ListItem item in PrivilegesForSelectionListBox.Items)
			{
				if (item.Selected)
				{
					IPrivilege privilege = IoC.Resolve<IPrivilegeSecureService>().Create();
					privilege.Id = int.Parse(item.Value, CultureInfo.CurrentCulture);
					privilege.Title = item.Text;
					privilege.CommonName = item.Text;
					privilegesList.Add(privilege);
				}
			}
			SetTempRolesPrivilege(privilegesList);
			FillPrivileges();
		}

		private void RemovePrivilegesClick(object sender, EventArgs e)
		{
			Collection<IPrivilege> privileges = GetRolePrivilegesState(GetIdOrNull());

			foreach (ListItem item in SelectedPrivilegesListBox.Items)
			{
				if (item.Selected)
				{
					ListItem listItem = item;
					IPrivilege privilege = privileges.FirstOrDefault(p => p.Id == int.Parse(listItem.Value, CultureInfo.CurrentCulture));
					privileges.Remove(privilege);
				}
			}
			SetTempRolesPrivilege(privileges);
			FillPrivileges();
		}

		protected override void PopulateForm()
		{
			if (GetIdOrNull() == null)
			{
				RoleEditCreateLabel.Text = Resources.User.Literal_CreateRole;
			}
			else
			{
				RoleEditCreateLabel.Text = Resources.User.Literal_EditRole;
				IRole role = IoC.Resolve<ISystemRoleSecureService>().GetById(GetId());
				TitleTextBox.Text = role.Title;
				// If page save redierected on save, display success message
				if (Request.QueryString.GetBooleanOrNull("HasMessage") ?? false)
				{
					SaveMesagerContainer.MessageType = InfoType.Success;
					SaveMesagerContainer.Add(Resources.GeneralMessage.SaveSuccessRole);
				}
			}
			FillPrivileges();

            DeleteButton.Visible = GetIdOrNull().HasValue;
		}
		private void FillPrivileges()
		{
			Collection<IPrivilege> allPrivilege = IoC.Resolve<IPrivilegeSecureService>().GetAll();
			Collection<IPrivilege> rolesPrivilege = GetRolePrivilegesState(GetIdOrNull());
			var notUsePrivilege = new Collection<IPrivilege>();
			foreach (var privilege in allPrivilege)
			{
				IPrivilege privilegeCopy = privilege;
				if (rolesPrivilege.FirstOrDefault(r => r.Id == privilegeCopy.Id) == null)
				{
					notUsePrivilege.Add(privilege);
				}
			}
			SelectedPrivilegesListBox.DataSource = rolesPrivilege;
			SelectedPrivilegesListBox.DataBind();
			PrivilegesForSelectionListBox.DataSource = notUsePrivilege;
			PrivilegesForSelectionListBox.DataBind();
		}
		
		public void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public void OnSave(object sender, EventArgs e)
		{
			int? roleId = GetIdOrNull();
			ISystemUserFull admin = SignInHelper.SignedInSystemUser;
			var roleService = IoC.Resolve<ISystemRoleSecureService>();
			IRole role = roleId != null ? roleService.GetById((int)roleId) : roleService.Create();
			role.Title = TitleTextBox.Text;
			role.Id = roleService.Save(admin, role, GetRolePrivilegesState(roleId));

			if (role.Id == -1)
			{
				SaveMesagerContainer.MessageType = InfoType.Failure;
				SaveMesagerContainer.Add(Resources.UserMessage.RoleWithSameTitle);
				return;
			}
			if (!roleId.HasValue)
			{
				var roleUrl = string.Format(CultureInfo.CurrentCulture, PathHelper.Role.GetEditUrlWithSuccesMessage(role.Id));
				Response.Redirect(roleUrl);
			}
			if(!SaveMesagerContainer.HasMessages)
			{
				SaveMesagerContainer.MessageType = InfoType.Success;
				SaveMesagerContainer.Add(Resources.GeneralMessage.SaveSuccessRole);
			}
		}
		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Role.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Role.GetDefaultUrl());
			}
		}
	}
}
