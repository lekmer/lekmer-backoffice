﻿using System;

namespace Litium.Scensum.BackOffice.Modules.Security.Roles
{
	[Serializable]
	public class RoleState
	{
		public object TempRolesPrivileges { get; set; }
	}
}