﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Security/Users/User.Master"  CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Security.Users.Default" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="UserSearch" Src="~/UserControls/User/UserSearchCriteria.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="AdvancedSearchTab" ContentPlaceHolderID="UserPlaceHolder" runat="server">
	<script type="text/javascript">
        function confirmDelete() {
            return DeleteConfirmation('<%= Resources.User.Literal_user%>');
        }
    </script>
	<asp:UpdatePanel ID="upSearch" runat="server">
	<ContentTemplate>
	<uc:UserSearch ID="UserSearchCriteriaControl" runat="server" />
	<div id="user-search-result" >
		<Scensum:Grid runat="server" ID="UsersGrid" FixedColumns="User name" >
			<sc:GridViewWithCustomPager 
				ID="SearchGrid" 
				SkinID="grid" 
				runat="server" 
				DataSourceID="UserObjectDataSource" 
				Width="100%"				
				AutoGenerateColumns="false"
				AllowPaging="true"
				PageSize="<%$AppSettings:DefaultGridPageSize%>">
				<Columns>
					<asp:TemplateField HeaderText="<%$ Resources:User, Literal_UserName%>" ItemStyle-Width="30%">
						<ItemTemplate>
							<uc:HyperLinkEncoded runat="server" ID="EditLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.User.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' Text='<%# Eval("UserName")%>' />
						</ItemTemplate >
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:User, Literal_Name%>" ItemStyle-Width="25%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="NameLiteral" runat="server" Text='<%# Eval("Name")%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status%>" ItemStyle-Width="10%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="StatusLiteral" runat="server" Text='<%# Eval("UserStatus.Title")%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:User, Literal_InvalidSignIns%>" ItemStyle-Width="10%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="InvalidLoginsLiteral" runat="server" Text='<%# Convert.ToInt32(Eval("FailedSignInCounter")) != 0 ? Eval("FailedSignInCounter") : string.Empty  %>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:User, Literal_ActivationDate%>" ItemStyle-Width="11%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="ActDateLiteral" runat="server" Text='<%# Convert.ToDateTime(Eval("ActivationDate")).ToShortDateString()%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:User, Literal_ExpirationDate%>" ItemStyle-Width="11%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="ExpDateLiteral" runat="server" Text='<%# Convert.ToDateTime(Eval("ExpirationDate")).ToShortDateString()%>' /> 
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:ImageButton runat="server" OnClientClick="javascript:return confirmDelete();" ID="ibtnDeleteUser" CommandName="DeleteUser" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="Delete" />
							</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</sc:GridViewWithCustomPager>
		</Scensum:Grid>
		<asp:ObjectDataSource ID="UserObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Security.SecurityDataSource" />
	</div>
	</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
