using System;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Security.Roles
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
	public partial class User : MasterPage
	{
		protected override void OnLoad(EventArgs e)
		{
			CreateUserButton.Click += CreateUserClick;
			base.OnLoad(e);
		}

		private void CreateUserClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.User.GetEditUrl());
		}
	}
}