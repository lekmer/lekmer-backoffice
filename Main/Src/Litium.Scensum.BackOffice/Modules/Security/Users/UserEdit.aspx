﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Security/Users/User.Master" CodeBehind="UserEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Security.Users.UserEdit" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="UserCredential" Src="~/UserControls/Security/UserCredential.ascx" %>
<asp:Content ID="UserEditTab" ContentPlaceHolderID="UserPlaceHolder" runat="server">
	<asp:UpdatePanel ID="EditUpdatePanel" runat="server">
		<ContentTemplate>
		<br /><div style=" float:left;  width:984px; ">
		<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
								runat="server" />
							<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
								ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgUserEdit" />
		
					<uc:MessageContainer ID="SaveMesagerContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
						runat="server" />
		</div>
			<div id="user-edit">
				<div id="user-edit-container">
					<asp:Label id="UserEditCreateLabel" class="assortment-header" runat="server" />
					<br/>
					<label><h5><asp:Literal runat="server" Text="<%$ Resources:User, Literal_AccontDetails %>"/></label></h5></label>
					<div class="input-box">
						<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_Name %>"/> *</span><br />
						<asp:TextBox ID="NameTextBox" runat="server" />
						<asp:RequiredFieldValidator runat="server" ID="NameValidator" ControlToValidate="NameTextBox" ErrorMessage="<%$ Resources:UserMessage, NameEmpty %>"  Display="None" ValidationGroup="vgUserEdit" />
					</div>
					<div class="input-box">
						<span><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Status %>"/> *</span><br />
						<asp:DropDownList runat="server" ID="StatusList"/><br />
						<asp:RequiredFieldValidator runat="server" ControlToValidate="StatusList" InitialValue="0" ErrorMessage="<%$ Resources:GeneralMessage, StatusIsNotSelected %>" Display="None" ValidationGroup="vgUserEdit" />
					</div>
					<div class="input-box-reset-password">
						<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_InvalidSignIns %>"/></span><br />
						<div style="float:left">
						<asp:TextBox ID="InvalidLoginsTextBox" ReadOnly="true" runat="server" />&nbsp;&nbsp;
						</div>
						<div style="float:left">
						<uc:ImageLinkButton ID="ResetLoginsAttemptButton" runat="server" Text="Reset" SkinID="DefaultButton" />
						</div>
					</div>
					
					<div class="input-box-date">
						<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_ActivationDate %>"/> *</span>
						<asp:RangeValidator runat="server" id="ActivationDateRangeValidator" Type="Date" ControlToValidate="ActivationDateTextBox" ErrorMessage="<%$ Resources:UserMessage, ActivationDateIncorectFormat %>" Display="None" ValidationGroup="vgUserEdit" />
						<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ActivationDateTextBox" ErrorMessage="<%$ Resources:UserMessage, ActivationDateEmpty %>" Display="None" ValidationGroup="vgUserEdit" />
						<br />
						<asp:TextBox ID="ActivationDateTextBox" runat="server" />
						<asp:ImageButton ID="ActivationDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
						<ajaxToolkit:CalendarExtender ID="ActivationDateCalendarControl" runat="server" TargetControlID="ActivationDateTextBox" PopupButtonID="ActivationDateButton" />
						&nbsp;-&nbsp;
					</div>
					<div class="input-box-date">
						<span><asp:Literal runat="server" Text="<%$ Resources:User, Literal_ExpirationDate %>"/> *</span>
						<asp:RangeValidator runat="server" id="ExpirationDateRangeValidator" Type="Date" ControlToValidate="ExpDateTextBox" ErrorMessage="<%$ Resources:UserMessage, ExpirationDateIncorrectFormat %>" Display="None" ValidationGroup="vgUserEdit" />
						<asp:CustomValidator ID="DateCompareValidator" runat="server" ControlToValidate="ExpDateTextBox" ErrorMessage="<%$ Resources:UserMessage, ActivationDateGreaterExpirationDate %>" Display="None" ValidationGroup="vgUserEdit" />
						<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ExpDateTextBox" ErrorMessage="<%$ Resources:UserMessage, ExpirationDateEmpty %>" Display="None" ValidationGroup="vgUserEdit" />
						<br />
						<asp:TextBox ID="ExpDateTextBox" runat="server" />
						<asp:ImageButton ID="ExpDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
						<ajaxToolkit:CalendarExtender ID="ExpDateCalendarControl" runat="server" TargetControlID="ExpDateTextBox" PopupButtonID="ExpDateButton" />
					</div>
					<br class="clear"/>
					<div class="input-box">
						<asp:Label ID="CultureLabel" Text="Culture" runat="server" />
						<br />
						<asp:DropDownList ID="CultureList" runat="server" DataValueField="Id" DataTextField="Title"></asp:DropDownList>
					</div>		
					<br/>
					<br/>
					<br/>
					<br/>
				</div>					
					<br/>
					<div>
					<asp:CheckBox ID="ChangePasswordCheckBox" runat ="server" AutoPostBack="true" Text="<%$ Resources:User, Literal_ChangePassword %>" Checked="false" />
					</div>
				     <div id="user-credential">
                        <uc:UserCredential ID="UserCredential" runat="server" AllowChangeUserName="true"/>
				     </div>
					<br class="clear" />
					<br/>
					<br/>
					<div class= "user-roles">
					<label><h5><asp:Literal runat="server" Text="<%$ Resources:User, Literal_UserAccess %>"/></h5></label>
						<div class = "user-roles-list-box">
						  <label><asp:Literal runat="server" Text="<%$ Resources:User, Literal_AvailableRoles %>"/></label>
						  <br />
						  <asp:ListBox id="RolesForSelectionListBox" DataTextField="Title" DataValueField="Id"
						   Rows="10"
						   Width="230px"
						   SelectionMode ="Multiple"
						   runat="server">
						   </asp:ListBox>
						</div>
						<div class = "user-roles-button">
						<uc:ImageLinkButton ID="AddRolesButton" runat="server" Text=">>" SkinID="DefaultButton" />
						<br />
						<br />
						<uc:ImageLinkButton ID="RemoveRolesButton" runat="server" Text="<<" SkinID="DefaultButton" />
						</div>
						<div class = "user-roles-list-box">
						  <label><asp:Literal runat="server" Text="<%$ Resources:User, Literal_SelectedRoles %>" /></label>
						  <br />
						  <asp:ListBox id="SelectedRolesListBox" DataTextField="Title" DataValueField="Id"
						   Rows="10"
						   Width="230px"
						   SelectionMode ="Multiple"
						   runat="server">
						   </asp:ListBox>
						</div>
					</div>
					<br class="clear" />
					<br />
					<asp:UpdatePanel ID="ButtonUpdatePanel" runat="server">
						<ContentTemplate>
						<div class="buttons left">
                        <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('user');" />
                    </div>
							<div class="buttons right">
								<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgUserEdit"
									SkinID="DefaultButton" />
								<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" CausesValidation="false"
									SkinID="DefaultButton" />
							</div>
							
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>