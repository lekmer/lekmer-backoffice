<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="PageSeo.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.PageSeo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div style="clear:both;">
		<uc:MessageContainer ID="SaveMesageContainer" MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="PageSEOContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="PageSEOPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div class="content-box">
				<div class="column">
					<div class="input-box">
						<span class="bold"><%= Resources.SiteStructure.Literal_SpecifyPageTitle %></span><br />
						<span><%= Resources.SiteStructure.Literal_PageTitleImportanSEO %></span><br />
						<asp:TextBox ID="SeoTitleTextBox" MaxLength="500" onkeydown="return CheckTextBoxLength(event, this, 500);" TextMode="MultiLine" Rows="5" runat="server" />
					</div>
				
					<div class="input-box">
						<span class="bold"><%= Resources.General.Literal_Description %></span><br />
						<span><%= Resources.SiteStructure.Literal_MetaDescription %></span><br />
						<asp:TextBox ID="SeoDescriptionTextBox" MaxLength="500" onkeydown="return CheckTextBoxLength(event, this, 500);" TextMode="MultiLine" Rows="5" runat="server" />
					</div>
				
					<div class="input-box">
						<span class="bold"><%= Resources.General.Literal_Keywords %></span><br />
						<span><%= Resources.SiteStructure.Literal_MetaKeywords %></span><br />
						<asp:TextBox ID="SeoKeywordsTextBox" MaxLength="500" onkeydown="return CheckTextBoxLength(event, this, 500);" TextMode="MultiLine" Rows="5" runat="server" />
					</div>
				</div>
			</div>
		</div>
		<br class="clear" />
		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
		</div>
	</asp:Panel>
</asp:Content>