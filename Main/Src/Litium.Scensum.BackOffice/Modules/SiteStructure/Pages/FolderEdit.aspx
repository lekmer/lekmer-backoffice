<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="FolderEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.FolderEdit" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>	
<%@ Register Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" TagName="SiteStructureNodeSelector" TagPrefix="uc" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
					runat="server" />
				<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
					ID="ValidationSummary" DisplayMode="List" ValidationGroup="vg" />

</asp:Content>
<asp:Content ID="FolderEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="FolderEditPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div class="content-box">
				<div class="column">
					<div class="input-box">
						<span><%= Resources.General.Literal_Title %> &nbsp;*</span>
						<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox"  Display ="None"
							ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" ValidationGroup="vg" />
						<br />
						<asp:TextBox ID="TitleTextBox" runat="server" />
					</div>
					<div class="input-box">
						<label>	<%= Resources.General.Literal_CommonName %></label>
						<br />
						<asp:TextBox ID="CommonNameTextBox" runat="server" />
					</div>
					<div class="input-box">
						<div id="AccessDiv" runat="server">
							<Scensum:GroupBox ID="AccessPanel" runat="server" Text="<%$ Resources:SiteStructure,Literal_Access %>" Width="221">
								<asp:RadioButton ID="AllRadioButton" runat="server" GroupName="Access" Checked="true" /><%= Resources.SiteStructure.Literal_All %>
								<asp:RadioButton ID="SignedInRadioButton" runat="server" GroupName="Access"/><%= Resources.SiteStructure.Literal_SignedIn %>
								<asp:RadioButton ID="NotSignedInRadioButton" runat="server" GroupName="Access" /><%= Resources.SiteStructure.Literal_NotSignedIn %>
							</Scensum:GroupBox>
						</div>
						<br style="clear: both" />
					</div>
					<br />
					<asp:Panel ID="ParentPanel" runat="server" class="right" Style="margin-top: -67px;">
						<label>
							<%= Resources.SiteStructure.Literal_SelectPlacement %></label><asp:Label runat="server" ID="lblPlasmentasterix" ForeColor="Red"
								Visible="false">&nbsp*</asp:Label>
						<div id="link-placement-tree" class="site-structure-placement-tree">						
							<uc:SiteStructureNodeSelector ID="ParentTreeViewControl" runat="server" AllowClearSelection="false"/>						
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		 <div class="buttons left">
                    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmationContentNode();" />
                </div>
				<div class="buttons right">
					<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" ValidationGroup="vg"
						SkinID="DefaultButton" />
					<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" CausesValidation="false"
						SkinID="DefaultButton" />
				</div>
	</asp:Panel>
</asp:Content>
