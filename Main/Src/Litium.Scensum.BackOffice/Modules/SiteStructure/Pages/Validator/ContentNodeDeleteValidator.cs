using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.DeleteValidator
{
	public class ContentNodeDeleteValidator
	{
		private readonly int _id;

		private readonly Collection<string> _messages = new Collection<string>();
		public Collection<string> Messages
		{
			get { return _messages; }
		}

		private bool _isValid = true;
		public bool IsValid
		{
			get { return _isValid; }
			set { _isValid = value; }
		}

		public ContentNodeDeleteValidator(int contentNodeId)
		{
			_id = contentNodeId;
		}

		public ContentNodeDeleteValidator HasReferencingShortcutValidate(Func<string, string> func)
		{
			var contentNodeShortcutLinkSecureService = IoC.Resolve<IContentNodeShortcutLinkSecureService>();
			var contentNodeShortcutLinkList = contentNodeShortcutLinkSecureService.GetAllByReferentContentNode(_id);
			if (contentNodeShortcutLinkList.Count > 0)
			{
				IsValid = false;
				Messages.Add(string.Format(
								CultureInfo.CurrentCulture,
								" * is referenced by following shortcut links: {0};",
								ContentNodeDeleteHelper.GetReferences(contentNodeShortcutLinkList, GetReference, func)
								));
			}
			return this;
		}
		public ContentNodeDeleteValidator IsStartPageValidate()
		{
			var contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
			var startContentPageId = contentNodeSecureService.GetContentPageStartIdByContentNode(_id);
			if (startContentPageId != 0)
			{
				IsValid = false;
				string title = contentNodeSecureService.GetById(startContentPageId).Title;
				Messages.Add(string.Format(
								CultureInfo.CurrentCulture,
								" * {0} is  start page for current registry;",
								title
								));
			}
			return this;
		}
		//public ContentNodeDeleteValidator HasReferencingProductSiteStructureRegistryValidate()
		//{
		//    var productSiteStructureRegistrySecureService = IoC.Resolve<IProductSiteStructureRegistrySecureService>();
		//    var productSiteStructureRegistryList = productSiteStructureRegistrySecureService.GetAllByReferentContentNode(_id);
		//    if (productSiteStructureRegistryList.Count > 0)
		//    {
		//        IsValid = false;
		//        Messages.Add(string.Format(
		//                        CultureInfo.CurrentCulture,
		//                        " * is referenced by following parent or template content node of products: {0};",
		//                        ContentNodeDeleteHelper.GetTitles(productSiteStructureRegistryList, GetReferencingProductTitleCallback)
		//                        ));
		//    }
		//    return this;
		//}
		//public ContentNodeDeleteValidator HasReferencingCategorySiteStructureRegistryValidate()
		//{
		//    var categorySiteStructureRegistrySecureService = IoC.Resolve<ICategorySiteStructureRegistrySecureService>();
		//    var categorySiteStructureRegistryList = categorySiteStructureRegistrySecureService.GetAllByReferentContentNode(_id);
		//    if (categorySiteStructureRegistryList.Count > 0)
		//    {
		//        IsValid = false;
		//        Messages.Add(string.Format(
		//                        CultureInfo.CurrentCulture,
		//                        " * is referenced by following parent or template content node of categories: {0};",
		//                        ContentNodeDeleteHelper.GetTitles(categorySiteStructureRegistryList, GetReferencingCategoryTitleCallback)
		//                        ));
		//    }
		//    return this;
		//}
		//public ContentNodeDeleteValidator HasReferencingBlockRegisterValidate()
		//{
		//	var blockRegisterSecureService = IoC.Resolve<IBlockRegisterSecureService>();
		//	var blockRegisterList = blockRegisterSecureService.GetAllByReferentContentNode(_id);
		//	if (blockRegisterList.Count > 0)
		//	{
		//		IsValid = false;
		//		Messages.Add(string.Format(
		//						CultureInfo.CurrentCulture,
		//						" * is referenced by following redirect content node of RegisterBlocks: {0};",
		//						ContentNodeDeleteHelper.GetTitles(blockRegisterList, GetBlockTitleCallback)
		//						));
		//	}
		//	return this;
		//}
		//public ContentNodeDeleteValidator HasReferencingBlocksSignInValidate()
		//{
		//	var blockRegisterSecureService = IoC.Resolve<IBlockSignInSecureService>();
		//	var blockSingInList = blockRegisterSecureService.GetAllByReferentContentNode(_id);
		//	if (blockSingInList.Count > 0)
		//	{
		//		IsValid = false;
		//		Messages.Add(string.Format(
		//						CultureInfo.CurrentCulture,
		//						" * is referenced by following redirect content node of SignInBlocks: {0};",
		//						ContentNodeDeleteHelper.GetTitles(blockSingInList, GetBlockTitleCallback)
		//						));
		//	}
		//	return this;
		//}
		public ContentNodeDeleteValidator HasReferencingBlockCheckoutValidate()
		{
			var blockCheckoutSecureService = IoC.Resolve<IBlockCheckoutSecureService>();
			var blockCheckoutList = blockCheckoutSecureService.GetAllByReferentContentNode(_id);
			if (blockCheckoutList.Count > 0)
			{
				IsValid = false;
				Messages.Add(string.Format(
								CultureInfo.CurrentCulture,
								" * is referenced by following redirect content node of CheckOutBlocks: {0};",
								ContentNodeDeleteHelper.GetTitles(blockCheckoutList, GetBlockTitleCallback)
								));
			}
			return this;
		}
		public ContentNodeDeleteValidator HasReferencingProductModuleChannelValidate()
		{
			var productModuleChannelSecureService = IoC.Resolve<IProductModuleChannelSecureService>();
			var productModuleChannelList = productModuleChannelSecureService.GetAllByReferentContentNode(_id);
			if (productModuleChannelList.Count > 0)
			{
				IsValid = false;
				Messages.Add(string.Format(
								CultureInfo.CurrentCulture,
								" * is referenced by following product template content node of channels: {0}",
								ContentNodeDeleteHelper.GetTitles(productModuleChannelList, GetReferencingChannelTitleCallback)
								));
			}
			return this;
		}
		public ContentNodeDeleteValidator HasReferencingOrderModuleChannelValidate()
		{
			var orderModuleChannelSecureService = IoC.Resolve<IOrderModuleChannelSecureService>();
			var orderModuleChannelList = orderModuleChannelSecureService.GetAllByReferentContentNode(_id);
			if (orderModuleChannelList.Count > 0)
			{
				IsValid = false;
				Messages.Add(string.Format(
								CultureInfo.CurrentCulture,
								" * is referenced by following order template content node of channels: {0}",
								ContentNodeDeleteHelper.GetTitles(orderModuleChannelList, GetReferencingChannelTitleCallback)
								));
			}
			return this;
		}
		public ContentNodeDeleteValidator IsMasterWithSubPagesValidate(Func<string, string> func)
		{
			var service = IoC.Resolve<IContentPageSecureService>();
			var subPages = service.GetAllFromMasterByReferentContentNode(_id);
			if (subPages == null || subPages.Count == 0) return this;

			IsValid = false;

			Messages.Add(string.Format(
								CultureInfo.CurrentCulture,
								" {0} {1};", Resources.SiteStructureMessage.DeleteMaster,
								ContentNodeDeleteHelper.GetReferences(subPages, GetReference, func)
								));
			return this;
		}

		private static string GetReference(IContentNode node, Func<string, string> func)
		{
			var sb = new StringBuilder();
			sb.Append("<a href=");

			string url = string.Empty;
			switch (node.ContentNodeTypeId)
			{
				case (int)ContentNodeTypeInfo.Page:
					url = PathHelper.SiteStructure.Page.GetPageEditUrl(node.Id);
					break;
				case (int)ContentNodeTypeInfo.ShortcutLink:
					url = PathHelper.SiteStructure.Page.GetShortcutEditUrl(node.Id);
					break;
			}
			sb.Append(func(url));
			sb.Append(" >");
			sb.Append(node.Title);
			sb.Append("</a>");
			return sb.ToString();
		}


		//private static string GetReferencingProductTitleCallback(IProductSiteStructureRegistry productSiteStructureRegistry)
		//{
		//    IProduct product = IoC.Resolve<IProductSecureService>().GetById(ChannelHelper.CurrentChannel.Id, productSiteStructureRegistry.ProductId);
		//    return product.Title;
		//}
		//private static string GetReferencingCategoryTitleCallback(ICategorySiteStructureRegistry categorySiteStructureRegistry)
		//{
		//    ICategory category = IoC.Resolve<ICategorySecureService>().GetById(SignInHelper.SignedInSystemUser, categorySiteStructureRegistry.CategoryId);
		//    return category.Title;
		//}
		private static string GetBlockTitleCallback(IBlock block)
		{
			return block.Title;
		}
		private static string GetReferencingChannelTitleCallback(IProductModuleChannel productModuleChannel)
		{
			IChannel channel = IoC.Resolve<IChannelSecureService>().GetById(productModuleChannel.Id);
			return channel.Title;
		}
		private static string GetReferencingChannelTitleCallback(IOrderModuleChannel orderModuleChannel)
		{
			IChannel channel = IoC.Resolve<IChannelSecureService>().GetById(orderModuleChannel.Id);
			return channel.Title;
		}
	}
}