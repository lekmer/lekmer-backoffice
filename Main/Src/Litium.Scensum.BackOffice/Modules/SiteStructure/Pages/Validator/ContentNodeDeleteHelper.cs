﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.DeleteValidator
{
	public static class ContentNodeDeleteHelper
	{
		public static string GetTitles<T>(IEnumerable<T> enumerable, CallbackHandler<T> callback)
		{
			var sb = new StringBuilder();
			foreach (T item in enumerable)
			{
				sb.Append("'");
				sb.Append(callback(item));
				sb.Append("', ");
			}
			if (sb.ToString().EndsWith(", ", StringComparison.OrdinalIgnoreCase))
			{
				sb.Remove(sb.Length - 2, 2);
			}
			return sb.ToString();
		}

		public static string GetReferences<T>(IEnumerable<T> enumerable, ReferenceCallbackHandler<T> callback, Func<string, string> func)
		{
			var sb = new StringBuilder();
			foreach (T item in enumerable)
			{
				sb.Append("'");
				sb.Append(callback(item, func));
				sb.Append("', ");
			}
			if (sb.ToString().EndsWith(", ", StringComparison.OrdinalIgnoreCase))
			{
				sb.Remove(sb.Length - 2, 2);
			}
			return sb.ToString();
		}
	}

	public delegate string CallbackHandler<T>(T obj);
	public delegate string ReferenceCallbackHandler<T>(T obj, Func<string, string> func);
}