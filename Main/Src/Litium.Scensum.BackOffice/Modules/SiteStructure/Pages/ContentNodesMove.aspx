<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="ContentNodesMove.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.ContentNodesMove" %>
<%@ Register assembly="Litium.Scensum.Web.Controls" namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree.SiteStructure" tagprefix="TemplatedTreeView" %>

<asp:Content ID="NodesMoveContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
<asp:UpdatePanel runat="server" ID="NodesMoveUpdatePanel"><ContentTemplate>
	<asp:Panel ID="NodesMovePanel" runat="server" DefaultButton="SaveButton">
	<div class="pages-full-content">
		<div class="content-box">
			<div class="left link-destination">
				<label><%=Resources.SiteStructure.Literal_SelectPlacement%></label>
				<div id="link-destination-tree">					
					<TemplatedTreeView:SiteStructureTreeView ID="DestinationTreeViewControl" runat="server" DisplayTextControl="lbName"
								NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="ss-node-type"
								MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
								NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
								NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
								NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
								MenuContainerElementId="node-menu" FolderImageUrlOnline="~/Media/Images/SiteStructure/folder.png"
								FolderImageUrlOffline="~/Media/Images/SiteStructure/folder-off.png" FolderImageUrlHidden="~/Media/Images/SiteStructure/folder-hid.png"
								MasterPageImageUrl="~/Media/Images/SiteStructure/master.png" SubPageImageUrlOnline="~/Media/Images/SiteStructure/sub-page.png"
								SubPageImageUrlOffline="~/Media/Images/SiteStructure/sub-page-off.png" SubPageImageUrlHidden="~/Media/Images/SiteStructure/sub-page-hid.png"
								PageImageUrlOnline="~/Media/Images/SiteStructure/page.png" PageImageUrlOffline="~/Media/Images/SiteStructure/page-off.png"
								PageImageUrlHidden="~/Media/Images/SiteStructure/page-hid.png" ShortcutImageUrlOnline="~/Media/Images/SiteStructure/shortcut.png"
								ShortcutImageUrlOffline="~/Media/Images/SiteStructure/shortcut-off.png" ShortcutImageUrlHidden="~/Media/Images/SiteStructure/shortcut-hid.png"
								UrlLinkImageUrlOnline="~/Media/Images/SiteStructure/link.png" UrlLinkImageUrlOffline="~/Media/Images/SiteStructure/link-off.png"
								UrlLinkImageUrlHidden="~/Media/Images/SiteStructure/link-hid.png" TypeImageContainerId="img"
								TypeImageContainerCssClass="ss-node-type" MenuToOnlineContainerId="to-online"
								MenuToOfflineContainerId="to-offline" MenuToHiddenContainerId="to-hidden" MenuContainerElementCssClass="menu-container"
								MenuContainerShadowElementCssClass="menu-shadow" MenuContainerElementTypeOfPageCssClass="menu-container-typeof-page"
								MenuContainerShadowElementTypeOfPageCssClass="menu-shadow-typeof-page" MenuContainerShadowElementId="menu-shadow"
								MenuPageSpecificContainerId="menu-pages-container" 
								MenuItemsContainerElementId="menu-container" NodeNavigationAnchorId="aName" NodeSelectedCssClass="treeview-node-selected">
								<HeaderTemplate>
								</HeaderTemplate>
								<NodeTemplate>
									<div class="tree-item-cell-expand">
										<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
										<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
									</div>
									<div class="tree-item-cell-main">
										<img runat="server" id="img" class="ss-node-type tree-node-img" />
										<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
									</div>
									<br />
								</NodeTemplate>
							</TemplatedTreeView:SiteStructureTreeView>
				</div>
			</div>		
		</div>
		<div class="buttons right">
		<div class = "left">
			<asp:UpdatePanel id="ContentNodeSaveUpdatePanel" runat="server">
				<ContentTemplate>
					<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="Save" SkinID="DefaultButton"/>
					&nbsp;
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton"  />
				
		</div>
	</div>
	</asp:Panel>
	</ContentTemplate></asp:UpdatePanel>
</asp:Content>