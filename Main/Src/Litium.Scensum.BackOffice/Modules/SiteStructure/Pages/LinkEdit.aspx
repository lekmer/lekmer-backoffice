<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="LinkEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.LinkEdit" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" TagName="SiteStructureNodeSelector" TagPrefix="uc" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

						<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
					runat="server" />
				<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
					ID="ValidationSummary" DisplayMode="List" ValidationGroup="vg" />
			
</asp:Content>
<asp:Content ID="LinkEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div class="content-box">
				<div class="column">
					<div class="input-box">
						<span><%= Resources.General.Literal_Title %>&nbsp; *</span>
						<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox" Display ="None"
							ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" ValidationGroup="vg" />
						<br />
						<asp:TextBox ID="TitleTextBox" runat="server" />
					</div>
					<div class="input-box">
						<span><%= Resources.General.Literal_CommonName %></span>
						
						<br />
						<asp:TextBox ID="CommonNameTextBox" runat="server" />
					</div>
					<div class="input-box">
						<span><%= Resources.SiteStructure.Literal_URL %> &nbsp;*</span>
						<asp:RequiredFieldValidator runat="server" ID="LinkUrlValidator" ControlToValidate="LinkUrlTextBox"
							Display ="None" ErrorMessage="<%$ Resources:SiteStructureMessage, LinkFormatNotCorrect %>" ValidationGroup="vg" />
						<asp:RequiredFieldValidator runat="server" ID="LinkUrlValidator2" ControlToValidate="LinkUrlTextBox"
							InitialValue="http://" Display ="None" ErrorMessage="<%$ Resources:SiteStructureMessage, LinkFormatNotCorrect %>"
							ValidationGroup="vg" />
						<br />
						<asp:TextBox ID="LinkUrlTextBox" runat="server" />
					</div>
					<div id="SettingsDiv" runat="server" class="input-box">
						<div id="AccessDiv" runat="server">
							<Scensum:GroupBox ID="AccessGroupBox" runat="server" Text="<%$ Resources:SiteStructure, Literal_Access %>" Width="221">
								<asp:RadioButton ID="AllRadioButton" runat="server" GroupName="Access" Checked="true"  /><%= Resources.SiteStructure.Literal_All %>
								<asp:RadioButton ID="SignedInRadioButton" runat="server" GroupName="Access"  /><%= Resources.SiteStructure.Literal_SignedIn %>
								<asp:RadioButton ID="NotSignedInRadioButton" runat="server" GroupName="Access"  /><%= Resources.SiteStructure.Literal_NotSignedIn %>
							</Scensum:GroupBox>
						</div>
						<div class="link-settings-separator">
							&nbsp;</div>
						<div id="StatusDiv" runat="server">
							<Scensum:GroupBox ID="StatusGroupBox" runat="server" Text="<%$Resources:General, Literal_Status %>" Width="149">
								<asp:RadioButton ID="OnLineRadioButton" runat="server" CssClass="padding-px" GroupName="Status" /><%= Resources.SiteStructure.Literal_Online %>
								<asp:RadioButton ID="OffLineRadioButton" runat="server" GroupName="Status" Checked="true" /><%= Resources.SiteStructure.Literal_Offline%>
							</Scensum:GroupBox>
						</div>
						<br class="clear" />
					</div>
					<br />
					<asp:Panel ID="ParentPanel" runat="server" CssClass="right" Style="margin-top: -73px;">
						<label>
							<%= Resources.SiteStructure.Literal_SelectPlacement %></label><asp:Label runat="server" ID="PlasmentasterixLabel" ForeColor="Red"
								Visible="false">&nbsp*</asp:Label>
						<div id="link-placement-tree" class="site-structure-link-placement-tree">						
							<uc:SiteStructureNodeSelector ID="ParentTreeViewControl" runat="server" AllowClearSelection="false"/>						
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<br class="clear" />
		<div class="buttons left">
                    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmationContentNode();" />
                </div>
					<div class="buttons right">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
						SkinID="DefaultButton" ValidationGroup="vg" />
					<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
						SkinID="DefaultButton" CausesValidation="false" />
				</div>
	</asp:Panel>
</asp:Content>
