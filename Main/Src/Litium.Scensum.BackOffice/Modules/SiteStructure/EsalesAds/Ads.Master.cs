﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds
{
	public partial class AdsMaster : MasterPage
	{
		// Private Members.

		private Collection<string> _breadcrumbAppend;


		// Public Properties.

		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				return _breadcrumbAppend ?? (_breadcrumbAppend = new Collection<string>());
			}
		}

		public virtual bool DenySelection
		{
			get
			{
				return AdFoldersTree.DenySelection;
			}
			set
			{
				AdFoldersTree.DenySelection = value;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session["SelectedAdFolderId"];
			}
			set
			{
				Session["SelectedAdFolderId"] = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return AdFoldersTree.RootNodeId;
			}
		}

		public virtual string RootNodeTitle
		{
			get
			{
				return AdFoldersTree.RootNodeTitle;
			}
		}


		// Protected Properties.

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}


		// Protected Methods.

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			//SearchButton.Click += OnSearch; TODO
			CreateButton.Click += OnAdCreateFromPanel;
			AdFoldersTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (SelectedFolderId == null)
			{
				SelectedFolderId = RootNodeId;
			}

			if (!IsPostBack)
			{
				PopulateTree(null);
				RestoreSearchField();
				BuildBreadcrumbs();
			}

			SetupTabAndPanel();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (AdFoldersTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}

			ScriptManager.RegisterStartupScript(
				LeftUpdatePanel,
				LeftUpdatePanel.GetType(),
				"root menu",
				string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", AdFoldersTree.ClientID),
				true);
		}

		protected virtual void RestoreSearchField()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(LekmerPathHelper.Ad.GetSearchResultUrl());

			if (!string.IsNullOrEmpty(searchName))
			{
				//SearchTextBox.Text = searchName; TODO
			}
		}

		protected virtual void BuildBreadcrumbs()
		{
			if (IsSearchResult)
			{
				Breadcrumbs.Text = Resources.General.Literal_SearchResults;
				return;
			}

			string breadcrumbs = string.Empty;
			const string bredcrumbsSeperator = " > ";

			if (SelectedFolderId.HasValue && SelectedFolderId.Value != RootNodeId)
			{
				var folders = GetNodes(SelectedFolderId);
				var folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
				while (folder != null)
				{
					breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", bredcrumbsSeperator, folder.Title, breadcrumbs);

					int? parentNodeId = folder.ParentId;

					folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
				}
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, bredcrumbsSeperator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", RootNodeTitle, breadcrumbs);
			Breadcrumbs.Text = breadcrumbs;
		}

		protected virtual void OnAdCreateFromPanel(object sender, EventArgs e)
		{
			SelectedFolderId = null;

			Response.Redirect(LekmerPathHelper.Ad.GetCreateUrl(AdFoldersTree.RootNodeId));
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			SelectedFolderId = null;

			var searchUrl = LekmerPathHelper.Ad.GetSearchResultUrl();
			//SearchCriteriaState<string>.Instance.Save(searchUrl, SearchTextBox.Text); TODO

			Response.Redirect(searchUrl);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);

			if (e.Id == -1)
			{
				OnFolderCreate(sender, e);
			}
			else
			{
				switch (e.EventName)
				{
					case "Expand":
						DenySelection = true;
						break;
					case "Navigate":
						SelectedFolderId = e.Id;
						RedirectToDefaultPage();
						break;
				}
			}
		}

		protected virtual void OnFolderCreate(object sender, EventArgs e)
		{
			SelectedFolderId = AdFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.AdFolder.GetCreateUrl(SelectedFolderId.Value));
			}
		}

		protected virtual void OnFolderEdit(object sender, EventArgs e)
		{
			SelectedFolderId = AdFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.AdFolder.GetEditUrl(SelectedFolderId.Value));
			}
		}

		protected virtual void OnFolderDelete(object sender, EventArgs e)
		{
			if (AdFoldersTree.MenuLastClickedNodeId == null)
			{
				return;
			}

			var folderId = AdFoldersTree.MenuLastClickedNodeId.Value;

			var folderSecureService = IoC.Resolve<IAdFolderSecureService>();

			int? parentFolderId = folderSecureService.GetById(folderId).ParentFolderId;

			if (!folderSecureService.TryDelete(SignInHelper.SignedInSystemUser, folderId))
			{
				SystemMessageContainer.Add(Resources.EsalesAds.AdFolderDeleteFailed);
				return;
			}

			SystemMessageContainer.MessageType = InfoType.Success;
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);

			SelectedFolderId = parentFolderId.HasValue ? parentFolderId.Value : RootNodeId;

			PopulateTree(null);

			RedirectToDefaultPage();
		}

		protected virtual void OnAdCreate(object sender, EventArgs e)
		{
			SelectedFolderId = AdFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.Ad.GetCreateUrl(SelectedFolderId.Value));
			}
		}


		// Public Methods.

		public virtual void PopulateTree(int? folderId)
		{
			AdFoldersTree.DataSource = GetNodes(folderId);
			AdFoldersTree.RootNodeTitle = Resources.EsalesAds.Literal_AdFolders;
			AdFoldersTree.DataBind();

			LeftUpdatePanel.Update();

			AdFoldersTree.SelectedNodeId = folderId ?? SelectedFolderId;
		}

		public virtual void SetupTabAndPanel()
		{
			if (Master != null)
			{
				var topMaster = (Master.Start)(Master).Master;
				if (topMaster != null)
				{
					topMaster.SetActiveTab("SiteStructure", "EsalesAds");
				}
			}

			AdPanel.Text = Resources.EsalesAds.Literal_Ads;
			CreateButton.Text = Resources.EsalesAds.Literal_Ad;
		}

		public virtual Collection<INode> GetNodes(int? folderId)
		{
			var id = folderId ?? ((SelectedFolderId == RootNodeId) ? null : SelectedFolderId);

			return IoC.Resolve<IAdFolderSecureService>().GetTree(id);
		}

		public virtual void UpdateSelection(int nodeId)
		{
			AdFoldersTree.SelectedNodeId = SelectedFolderId = nodeId;
			LeftUpdatePanel.Update();
		}

		public virtual void UpdateBreadcrumbs(string value)
		{
			Breadcrumbs.Text = value;
		}

		public virtual void UpdateBreadcrumbs(int? folderId)
		{
			SelectedFolderId = folderId;
			BuildBreadcrumbs();
		}

		public virtual void RedirectToDefaultPage()
		{
			Response.Redirect(LekmerPathHelper.Ad.GetDefaultUrl());
		}
	}
}