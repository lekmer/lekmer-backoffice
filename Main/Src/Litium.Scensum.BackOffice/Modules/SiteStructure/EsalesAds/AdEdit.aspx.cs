﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds.Controls;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using System.Web.UI;
using log4net;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds
{
	public partial class AdEdit : LekmerStatePageController<AdState>, IEditor
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private List<string> _validationMessage = new List<string>();

		private Collection<IEsalesRegistry> _esalesRegistries;

		protected virtual AdsMaster MasterPage
		{
			get
			{
				return (AdsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			DeleteButton.Click += OnDelete;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			FolderSelector.NodeCommand += OnFolderSelectorCommand;
			FolderSelector.SelectedFolderChangedEvent += OnSelectedFolderChanged;

			AdChannelRepeater.ItemDataBound += OnAdChannelRepeaterItemDataBound;
		}

		protected override void PopulateForm()
		{
			InitializeEsalesRegistryList();

			PopulateAdChannels();

			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var ad = IoC.Resolve<IAdSecureService>().GetById(id.Value);

				if (ad == null)
				{
					throw new InvalidOperationException(string.Format("The ad with id ={0} could not be found.", id.Value));
				}

				InitializeState(ad.FolderId);

				PopulateAdInfo(ad);
				PopulateAdChannelsInfo(ad);

				MasterPage.BreadcrumbAppend.Add(Resources.EsalesAds.Literal_EditAd);
			}
			else
			{
				InitializeState(null);

				MasterPage.BreadcrumbAppend.Add(Resources.EsalesAds.Literal_CreateAd);
			}

			PopulatePath(State.FolderId);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.EsalesAds.AdSaveSuccess);
				}
			}

			if (IsPostBack)
			{
				RegisterDefaultImageLitboxScript();
			}

			MasterPage.SetupTabAndPanel();
		}


		public virtual void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				MasterPage.RedirectToDefaultPage();
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}

				SystemMessageContainer.MessageType = InfoType.Warning;

				return;
			}

			int? id = GetIdOrNull();

			var adSecureService = IoC.Resolve<IAdSecureService>();

			IAd ad;

			if (id.HasValue)
			{
				ad = adSecureService.GetById(id.Value);
				if (ad == null)
				{
					throw new InvalidOperationException(string.Format("The ad with id ={0} could not be found.", id.Value));
				}
			}
			else
			{
				ad = adSecureService.Create();
			}

			ComposeAdFromForm(ad);
			ComposeAdChannelFromForm(ad);

			try
			{
				ad = adSecureService.Save(SignInHelper.SignedInSystemUser, ad);
			}
			catch (Exception ex)
			{
				SystemMessageContainer.Add(Resources.EsalesAds.AdUpdateFailed);
				SystemMessageContainer.Add("ERROR: " + ex.Message);
				_log.Error(ex);
				return;
			}

			if (ad.Id == -1)
			{
				SystemMessageContainer.Add(Resources.EsalesAds.KeyNotUnique);
			}
			else
			{
				MasterPage.SelectedFolderId = State.FolderId;
				MasterPage.PopulateTree(State.FolderId);
				MasterPage.UpdateBreadcrumbs(State.FolderId);

				Response.Redirect(LekmerPathHelper.Ad.GetEditUrlWithMessage(ad.Id));
			}
		}

		protected virtual void OnDelete(object sender, EventArgs e)
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var adSecureService = IoC.Resolve<IAdSecureService>();

				adSecureService.Delete(SignInHelper.SignedInSystemUser, id.Value);
			}

			Response.Redirect(LekmerPathHelper.Ad.GetDefaultUrl());
		}

		protected virtual void OnFolderSelectorCommand(object sender, CommandEventArgs e)
		{
			FolderSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			FolderSelector.DataBind();
		}

		protected virtual void OnSelectedFolderChanged(object sender, UserControls.Media.Events.SelectedFolderChangedEventArgs e)
		{
			if (e.SelectedFolderId != null)
			{
				State.FolderId = e.SelectedFolderId.Value;
			}
		}

		protected virtual void OnAdChannelRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
			{
				return;
			}

			// Collapsible script
			var collapsibleHeaderDiv = (HtmlGenericControl)e.Item.FindControl("CollapsibleHeaderDiv");
			var collapsibleStateHidden = (HiddenField)e.Item.FindControl("CollapsibleStateHidden");
			var collapsibleIndicatorImage = (Image)e.Item.FindControl("CollapsibleIndicatorImage");
			var collapsibleContentDiv = (HtmlGenericControl)e.Item.FindControl("CollapsibleContentDiv");

			string collapsibleScript = string.Format("OpenClose('{0}','{1}','{2}','{3}','{4}');",
				collapsibleContentDiv.ClientID,
				collapsibleStateHidden.ClientID,
				collapsibleIndicatorImage.ClientID,
				ResolveClientUrl("~/Media/Images/Common/down.gif"),
				ResolveClientUrl("~/Media/Images/Common/up.gif")
			);

			collapsibleHeaderDiv.Attributes.Add("onclick", collapsibleScript);

			// ContentNodeSelector
			if (((IEsalesRegistry)e.Item.DataItem).SiteStructureRegistryId.HasValue)
			{
				var contentNodeSelector = (LekmerContentPageSelector)e.Item.FindControl("ContentNodeSelector");
				var siteStructureRegistryId = ((IEsalesRegistry)e.Item.DataItem).SiteStructureRegistryId;
				if (siteStructureRegistryId != null)
				{
					contentNodeSelector.SiteStructureRegistryId = siteStructureRegistryId.Value;
				}
				contentNodeSelector.SetContentPageTitle();
			}
		}


		protected virtual void InitializeEsalesRegistryList()
		{
			EsalesRegistryRepeater.DataSource = GetEsalesRegistries();
			EsalesRegistryRepeater.DataBind();
		}

		protected virtual void InitializeState(int? folderId)
		{
			if (State == null)
			{
				State = new AdState();
			}

			if (folderId.HasValue)
			{
				State.FolderId = folderId;
			}
			else
			{
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					State.FolderId = MasterPage.SelectedFolderId.Value;
				}
			}
		}

		protected virtual void RegisterDefaultImageLitboxScript()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), "DefaultImageLitbox_" + ClientID, "DefaultImageLitbox();", true);
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			return IoC.Resolve<IAdFolderSecureService>().GetTree(folderId);
		}

		protected virtual void PopulatePath(int? adFolderId)
		{
			FolderSelector.SelectedNodeId = adFolderId;

			FolderSelector.DataSource = GetPathSource(adFolderId);
			FolderSelector.DataBind();

			FolderSelector.PopulatePath(adFolderId);
		}

		protected virtual void PopulateAdInfo(IAd ad)
		{
			ad.FolderId = State.FolderId ?? -1;

			KeyTextBox.Text = ad.Key;
			TitleTextBox.Text = ad.Title;

			StartDateTextBox.Text = ad.DisplayFrom.HasValue ? ad.DisplayFrom.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
			EndDateTextBox.Text = ad.DisplayTo.HasValue ? ad.DisplayTo.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;

			ProductCategoryTextBox.Text = ad.ProductCategory;
			GenderTextBox.Text = ad.Gender;
			FormatTextBox.Text = ad.Format;
			SearchTagsTextBox.Text = ad.SearchTags;
		}

		protected virtual void PopulateAdChannelsInfo(IAd ad)
		{
			var adChannelList = ad.AdChannelList ?? new Collection<IAdChannel>();

			// Set 'ShowOnSite'
			foreach (RepeaterItem item in EsalesRegistryRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var channelIdHidden = (HiddenField)item.FindControl("ChannelIdHidden");
				var channelCheckbox = (CheckBox)item.FindControl("ChannelCheckbox");

				int esalesRegistryId = int.Parse(channelIdHidden.Value, CultureInfo.InvariantCulture);

				IAdChannel adChannel = adChannelList.FirstOrDefault(ac => ac.RegistryId == esalesRegistryId);
				if (adChannel != null)
				{
					channelCheckbox.Checked = adChannel.ShowOnSite;
				}
			}

			// Set other properties
			foreach (RepeaterItem item in AdChannelRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var channelIdHidden = (HiddenField)item.FindControl("ChannelIdHidden");
				var channelTitleTextBox = (TextBox)item.FindControl("ChannelTitleTextBox");
				var landingPageUrlTextBox = (TextBox)item.FindControl("LandingPageUrlTextBox");
				var contentNodeSelector = (LekmerContentPageSelector)item.FindControl("ContentNodeSelector");
				var imageUrlTextBox = (TextBox)item.FindControl("ImageUrlTextBox");
				var adImage = (AdImage)item.FindControl("AdImage");

				int esalesRegistryId = int.Parse(channelIdHidden.Value, CultureInfo.InvariantCulture);

				IAdChannel adChannel = adChannelList.FirstOrDefault(ac => ac.RegistryId == esalesRegistryId);

				if (adChannel != null)
				{
					channelTitleTextBox.Text = adChannel.Title;
					channelTitleTextBox.Text = adChannel.Title;
					landingPageUrlTextBox.Text = adChannel.LandingPageUrl;
					contentNodeSelector.SelectedNodeId = adChannel.LandingPageId;
					if (contentNodeSelector.SelectedNodeId.HasValue)
					{
						contentNodeSelector.SetContentPageTitle();
					}
					imageUrlTextBox.Text = adChannel.ImageUrl;
					adImage.ImageId = adChannel.ImageId;
				}
			}
		}


		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidKey())
			{
				_validationMessage.Add(Resources.EsalesAds.KeyInvalid);
				isValid = false;
			}

			if (!IsValidUniqueKey())
			{
				_validationMessage.Add(Resources.EsalesAds.KeyNotUnique);
				isValid = false;
			}

			if (!IsValidTitle())
			{
				_validationMessage.Add(Resources.EsalesAds.TitleEmpty);
				isValid = false;
			}

			if (!IsValidFolder())
			{
				_validationMessage.Add(Resources.EsalesAds.AdFolderEmpty);
				isValid = false;
			}

			if (!IsValidRegistry())
			{
				_validationMessage.Add(Resources.EsalesAds.AdShowOnSiteEmpty);
				isValid = false;
			}

			if (!IsValidStartDate())
			{
				_validationMessage.Add(Resources.EsalesAds.StartDateInvalid);
				isValid = false;
			}

			if (!IsValidEndDate())
			{
				_validationMessage.Add(Resources.EsalesAds.EndDateInvalid);
				isValid = false;
			}

			if (!IsValidDateRange())
			{
				_validationMessage.Add(Resources.EsalesAds.EndDateGreaterStartDate);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidKey()
		{
			string key = KeyTextBox.Text.Trim();

			if (key.IsEmpty())
			{
				return false;
			}

			if (key.Contains(" "))
			{
				return false;
			}

			if (key.Contains(","))
			{
				return false;
			}

			return key.Length <= 50;
		}

		protected virtual bool IsValidUniqueKey()
		{
			string key = KeyTextBox.Text.Trim();
			int? id = GetIdOrNull();

			var adSecureService = IoC.Resolve<IAdSecureService>();

			return adSecureService.IsUniqueKey(id, key);
		}

		protected virtual bool IsValidTitle()
		{
			string title = TitleTextBox.Text.Trim();

			if (string.IsNullOrEmpty(title))
			{
				return false;
			}

			return title.Length <= 50;
		}

		protected virtual bool IsValidFolder()
		{
			return State.FolderId > 0;
		}

		protected virtual bool IsValidRegistry()
		{
			foreach (RepeaterItem item in EsalesRegistryRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var channelCheckbox = (CheckBox)item.FindControl("ChannelCheckbox");

				if (channelCheckbox.Checked)
				{
					return true;
				}
			}

			return false;
		}

		protected virtual bool IsValidStartDate()
		{
			string dateTimeString = StartDateTextBox.Text.Trim();
			DateTime date;

			if (string.IsNullOrEmpty(dateTimeString)) return true;
			if (!DateTime.TryParse(dateTimeString, out date)) return false;

			return true;
		}

		protected virtual bool IsValidEndDate()
		{
			string dateTimeString = EndDateTextBox.Text.Trim();
			DateTime date;

			if (string.IsNullOrEmpty(dateTimeString)) return true;
			if (!DateTime.TryParse(dateTimeString, out date)) return false;

			return true;
		}

		protected virtual bool IsValidDateRange()
		{
			DateTime startDate;
			DateTime endDate;

			if (DateTime.TryParse(StartDateTextBox.Text.Trim(), out startDate)
				&& DateTime.TryParse(EndDateTextBox.Text.Trim(), out endDate)
				&& endDate < startDate) return false;

			return true;
		}


		protected virtual void PopulateAdChannels()
		{
			AdChannelRepeater.DataSource = GetEsalesRegistries();
			AdChannelRepeater.DataBind();
		}

		protected virtual Collection<IEsalesRegistry> GetEsalesRegistries()
		{
			if (_esalesRegistries == null)
			{
				var esalesRegistrySecureService = IoC.Resolve<IEsalesRegistrySecureService>();
				_esalesRegistries = esalesRegistrySecureService.GetAll();
			}

			if (_esalesRegistries == null)
			{
				_esalesRegistries = new Collection<IEsalesRegistry>();
			}

			return _esalesRegistries;
		}


		protected virtual void ComposeAdFromForm(IAd ad)
		{
			ad.FolderId = State.FolderId ?? -1;

			ad.Key = KeyTextBox.Text.NullWhenTrimmedEmpty();
			ad.Title = TitleTextBox.Text.NullWhenTrimmedEmpty();

			ad.DisplayFrom = StartDateTextBox.Text.IsEmpty() ? null : (DateTime?)DateTime.Parse(StartDateTextBox.Text, CultureInfo.CurrentCulture);
			ad.DisplayTo = EndDateTextBox.Text.IsEmpty() ? null : (DateTime?)DateTime.Parse(EndDateTextBox.Text, CultureInfo.CurrentCulture);

			ad.ProductCategory = ProductCategoryTextBox.Text.NullWhenTrimmedEmpty();
			ad.Gender = GenderTextBox.Text.NullWhenTrimmedEmpty();
			ad.Format = FormatTextBox.Text.NullWhenTrimmedEmpty();
			ad.SearchTags = SearchTagsTextBox.Text.NullWhenTrimmedEmpty();
		}

		protected virtual void ComposeAdChannelFromForm(IAd ad)
		{
			var adChannelSecureService = IoC.Resolve<IAdChannelSecureService>();

			Collection<IEsalesRegistry> esalesRegistries = GetEsalesRegistries();

			if (ad.AdChannelList == null)
			{
				ad.AdChannelList = new Collection<IAdChannel>();
			}

			// Create new AdChannel if not exists
			foreach (var esalesRegistry in esalesRegistries)
			{
				if (ad.AdChannelList.Any(ac => ac.RegistryId == esalesRegistry.Id) == false)
				{
					IAdChannel adChannel = adChannelSecureService.Create();
					adChannel.RegistryId = esalesRegistry.Id;
					ad.AdChannelList.Add(adChannel);
				}
			}

			// Set 'ShowOnSite'
			foreach (RepeaterItem item in EsalesRegistryRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var channelIdHidden = (HiddenField)item.FindControl("ChannelIdHidden");
				var channelCheckbox = (CheckBox)item.FindControl("ChannelCheckbox");

				int esalesRegistryId = int.Parse(channelIdHidden.Value, CultureInfo.InvariantCulture);

				ad.AdChannelList.First(ac => ac.RegistryId == esalesRegistryId).ShowOnSite = channelCheckbox.Checked;
			}

			// Set other properties
			foreach (RepeaterItem item in AdChannelRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var channelIdHidden = (HiddenField)item.FindControl("ChannelIdHidden");
				var channelTitleTextBox = (TextBox)item.FindControl("ChannelTitleTextBox");
				var landingPageUrlTextBox = (TextBox)item.FindControl("LandingPageUrlTextBox");
				var contentNodeSelector = (LekmerContentPageSelector)item.FindControl("ContentNodeSelector");
				var imageUrlTextBox = (TextBox)item.FindControl("ImageUrlTextBox");
				var adImage = (AdImage)item.FindControl("AdImage");

				int esalesRegistryId = int.Parse(channelIdHidden.Value, CultureInfo.InvariantCulture);

				IAdChannel adChannel = ad.AdChannelList.First(ac => ac.RegistryId == esalesRegistryId);

				adChannel.Title = channelTitleTextBox.Text.NullWhenTrimmedEmpty();
				adChannel.LandingPageUrl = landingPageUrlTextBox.Text.NullWhenTrimmedEmpty();
				adChannel.LandingPageId = contentNodeSelector.SelectedNodeId;
				adChannel.ImageUrl = imageUrlTextBox.Text.NullWhenTrimmedEmpty();
				adChannel.ImageId = adImage.ImageId;
			}
		}
	}

	[Serializable]
	public sealed class AdState
	{
		public int? FolderId { get; set; }
	}
}