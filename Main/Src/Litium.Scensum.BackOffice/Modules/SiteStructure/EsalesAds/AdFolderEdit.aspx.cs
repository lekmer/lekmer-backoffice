﻿using System;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds
{
	public partial class AdFolderEdit : LekmerPageController, IEditor
	{
		protected virtual AdsMaster MasterPage
		{
			get
			{
				return (AdsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();
			if (id.HasValue)
			{
				var folder = IoC.Resolve<IAdFolderSecureService>().GetById(id.Value);
				AdFolderTitleTextBox.Text = folder.Title;

				MasterPage.BreadcrumbAppend.Add(Resources.EsalesAds.Literal_EditAdFolder);

				PopulateTreeView(null);
			}
			else
			{
				MasterPage.BreadcrumbAppend.Add(Resources.EsalesAds.Literal_CreateAdFolder);

				DestinationDiv.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.EsalesAds.AdFolderSaveSuccess);
				}
			}

			System.Web.UI.ScriptManager.RegisterStartupScript(
				this,
				GetType(),
				string.Format(CultureInfo.CurrentCulture, "root menu {0}", FolderTree.ClientID),
				string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", FolderTree.ClientID),
				true);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			int? id = GetIdOrNull();
			int? parentId = Request.QueryString.GetInt32OrNull("parentFolderId");

			var folderSecureService = IoC.Resolve<IAdFolderSecureService>();

			var folder = id.HasValue ? folderSecureService.GetById(id.Value) : IoC.Resolve<IAdFolder>();
			folder.Title = AdFolderTitleTextBox.Text;

			if (id == null)
			{
				folder.ParentFolderId = parentId != MasterPage.RootNodeId ? parentId : null;
			}
			else if (FolderTree.SelectedNodeId.HasValue)
			{
				folder.ParentFolderId = FolderTree.SelectedNodeId.Value == FolderTree.RootNodeId ? null : FolderTree.SelectedNodeId;
			}

			folder = folderSecureService.Save(SignInHelper.SignedInSystemUser, folder);

			if (folder.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.FolderTitleExist);
			}
			else
			{
				MasterPage.PopulateTree(null);
				MasterPage.SelectedFolderId = folder.Id;

				Response.Redirect(LekmerPathHelper.AdFolder.GetEditUrlWithMessage(folder.Id));
			}
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}

		protected void PopulateTreeView(int? folderId)
		{
			var folders = MasterPage.GetNodes(folderId ?? MasterPage.SelectedFolderId);

			if (folders == null || folders.Count <= 0)
			{
				return;
			}

			var editedFolder = folders.FirstOrDefault(f => f.Id == MasterPage.SelectedFolderId);

			FolderTree.DataSource = editedFolder != null
				? FolderTree.GetFilteredSource(folders, editedFolder.Id)
				: folders;
			FolderTree.RootNodeTitle = Resources.EsalesAds.Literal_AdFolders;
			FolderTree.DataBind();

			FolderTree.SelectedNodeId = folderId ?? (editedFolder != null ? editedFolder.ParentId : FolderTree.RootNodeId) ?? FolderTree.RootNodeId;
		}
	}
}