using System;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Translation;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Aliases
{
	public partial class AliasEdit : LekmerPageController, IEditor
	{
		protected void PopulateAliasFields(IAlias alias)
		{
			AliasDescriptionTextBox.Text = alias.Description;
			SystemNameTextBox.Text = alias.CommonName;
		}
		protected void PopulateTranslator(IAlias alias)
		{
			int aliasTypeId = System.Convert.ToInt32(AliasTypeList.SelectedValue, CultureInfo.CurrentCulture);
			AliasTranslatorControl.Alias = alias;
			AliasTranslatorControl.Mode = GetTranslationItemMode(aliasTypeId);
			AliasTranslatorControl.DefaultValueControlClientId = GetDefaultValueControlClientId(alias != null ? alias.AliasTypeId : aliasTypeId);
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			AliasTypeList.SelectedIndexChanged += AliasTypeListSelectedIndexChanged;
			ParentAliasTree.NodeCommand += OnNodeCommand;
			DeleteButton.Click += DeleteButton_Click;
		}

		protected void DeleteButton_Click(object sender, EventArgs e)
		{
			IoC.Resolve<IAliasSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
			Response.Redirect(PathHelper.Template.Alias.GetDefaultUrl());
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			IAlias alias = null;
			if (GetIdOrNull() != null)
			{
				alias = IoC.Resolve<IAliasSecureService>().GetById(GetId());
			}
			PopulateTranslator(alias);
			PopulateBreadcrumb(alias);
		}
		protected void PopulateBreadcrumb(IAlias alias)
		{
			var master = Master as Alias;
			if (master == null) return;
			master.BreadcrumbAppend.Clear();
			if (alias != null)
			{
				master.BreadcrumbAppend.Add(alias.CommonName);
			}
			else
			{
				master.BreadcrumbAppend.Add(Resources.Interface.Literal_CreateAlias);
			}
		}

		protected override void PopulateForm()
		{
			PopulateAliasType();
			if (GetIdOrNull() != null)
			{
				IAlias alias = IoC.Resolve<IAliasSecureService>().GetById(GetId());
				PopulateAliasFields(alias);
				PopulateTreeView(alias.AliasFolderId);
				PopulateAliasValue(alias);
				UpdateMasterSelectedFolder(alias.AliasFolderId);
			}
			else
			{
				PopulateAliasValue(null);
				TreeViewFoldersUpdatePanel.Visible = false;
			}
			DeleteButton.Visible = GetIdOrNull().HasValue;
			if (JustCreated() == null) return;
			if (!((bool)JustCreated())) return;
			messager.MessageType = InfoType.Success;
			messager.Add(Resources.GeneralMessage.SaveSuccessAlias);

		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var aliasService = IoC.Resolve<IAliasSecureService>();
			IAlias alias;
			bool justCreated = false;
			if (GetIdOrNull() != null)
			{
				alias = aliasService.GetById(GetId());
			}
			else
			{
				alias = aliasService.Create();
				justCreated = true;
			}
			alias.CommonName = SystemNameTextBox.Text;
			alias.Description = string.IsNullOrEmpty(AliasDescriptionTextBox.Text) ? null : AliasDescriptionTextBox.Text;
			alias.Value = GetAliasValue();
			alias.AliasTypeId = System.Convert.ToInt32(AliasTypeList.SelectedValue, CultureInfo.CurrentCulture);
			alias.AliasFolderId = justCreated ? ((Alias)Master).SelectedFolderId.Value : ParentAliasTree.SelectedNodeId.Value;

			ValidationResult result = aliasService.Validate(alias);
			if (!result.IsValid)
			{
				messager.AddRange(result.Errors);
				return;
			}

			int aliasId = aliasService.Save(SignInHelper.SignedInSystemUser, alias);


			IoC.Resolve<IAliasTranslationSecureService>().SaveAll(SignInHelper.SignedInSystemUser, AliasTranslatorControl.GetTranslations(aliasId));
			if (justCreated)
			{
				Response.Redirect(PathHelper.Template.Alias.GetEditUrlForJustCreated(aliasId));
			}
			PopulateBreadcrumb(alias);
			messager.MessageType = InfoType.Success;
			messager.Add(Resources.GeneralMessage.SaveSuccessAlias);

			UpdateMasterSelectedFolder(alias.AliasFolderId);
		}

		protected void UpdateMasterSelectedFolder(int folderId)
		{
			var master = Master as Alias;
			if (master != null && master.SelectedFolderId != folderId)
			{
				master.UpdateSelectedFolder(folderId);
			}
		}
		private void AliasTypeListSelectedIndexChanged(object sender, EventArgs e)
		{
			SelectedAliasTypeIndexHiddenField.Value = AliasTypeList.SelectedIndex.ToString(CultureInfo.CurrentCulture);
			int aliasTypeId = System.Convert.ToInt32(AliasTypeList.SelectedValue, CultureInfo.CurrentCulture);
			switch (aliasTypeId)
			{
				case (int)AliasTypeConstant.Simple:
					AreaValueTextBox.Attributes.Add("style", "display:none");
					TinyMceDiv.Attributes.Add("style", "display:none");
					ValueTextBox.Attributes.Add("style", "display:block");
					ValueTextBox.Text = string.Empty;
					break;
				case (int)AliasTypeConstant.Multiline:
				case (int)AliasTypeConstant.JavaScript:
				case (int)AliasTypeConstant.Wysiwyg:
					ValueTextBox.Attributes.Add("style", "display:none");
					TinyMceDiv.Attributes.Add("style", "display:none");
					AreaValueTextBox.Attributes.Add("style", "display:block");
					AreaValueTextBox.Text = string.Empty;
					break;
				/*case (int)AliasTypeConstant.Wysiwyg:
					ValueTextBox.Attributes.Add("style", "display:none");
					AreaValueTextBox.Attributes.Add("style", "display:none");
					TinyMceDiv.Attributes.Add("style", "display:block");
					TinyMceValueTextArea.Value = string.Empty;
					break;*/
			}

			AliasTranslatorControl.Mode = GetTranslationItemMode(aliasTypeId);
			AliasTranslatorControl.DefaultValueControlClientId = GetDefaultValueControlClientId(aliasTypeId);
			AliasTranslatorControl.ClearTranslations();
		}

		protected void PopulateAliasValue(IAlias alias)
		{
			string aliasValue = string.Empty;
			if (alias != null)
			{
				AliasTypeList.Items.FindByValue(alias.AliasTypeId.ToString(CultureInfo.CurrentCulture)).Selected = true;
				aliasValue = alias.Value;
			}
			SelectedAliasTypeIndexHiddenField.Value = AliasTypeList.SelectedIndex.ToString(CultureInfo.CurrentCulture);
			int aliasTypeId = System.Convert.ToInt32(AliasTypeList.SelectedValue, CultureInfo.CurrentCulture);
			switch ((AliasTypeConstant)aliasTypeId)
			{
				case AliasTypeConstant.Simple:
					ValueTextBox.Attributes.Add("style", "display:block");
					ValueTextBox.Text = aliasValue;
					AreaValueTextBox.Attributes.Add("style", "display:none");
					// TinyMceDiv.Attributes.Add("style", "display:none");
					break;
				case AliasTypeConstant.Multiline:
				case AliasTypeConstant.JavaScript:
				case AliasTypeConstant.Wysiwyg:
					ValueTextBox.Attributes.Add("style", "display:none");
					AreaValueTextBox.Attributes.Add("style", "display:block");
					AreaValueTextBox.Text = aliasValue;
					// TinyMceDiv.Attributes.Add("style", "display:none");
					break;

				//ValueTextBox.Attributes.Add("style", "display:none");
				//AreaValueTextBox.Attributes.Add("style", "display:none");
				//TinyMceDiv.Attributes.Add("style", "display:block");
				//TinyMceValueTextArea.Value = aliasValue;
				//break;
			}
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var master = (Alias)Page.Master;
			var folders = master.GetTreeDataSource(folderId ?? master.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;

			ParentAliasTree.DataSource = folders;
			ParentAliasTree.DataBind();
			ParentAliasTree.SelectedNodeId = folderId;
		}

		protected void PopulateAliasType()
		{
			var aliasTypeSecureService = IoC.Resolve<IAliasTypeSecureService>();
			AliasTypeList.DataSource = aliasTypeSecureService.GetAll();
			AliasTypeList.DataBind();
		}

		protected string GetAliasValue()
		{
			switch ((AliasTypeConstant)System.Convert.ToInt32(AliasTypeList.SelectedValue, CultureInfo.CurrentCulture))
			{
				case AliasTypeConstant.Simple:
					return ValueTextBox.Text;
				case AliasTypeConstant.Multiline:
				case AliasTypeConstant.JavaScript:
				case AliasTypeConstant.Wysiwyg:
					return AreaValueTextBox.Text;

				//return TinyMceValueTextArea.Value;
				default:
					return string.Empty;
			}
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Template.Alias.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Template.Alias.GetDefaultUrlWithShowFolderContent());
			}
		}

		protected static TranslationItemMode GetTranslationItemMode(int aliasTypeId)
		{
			switch ((AliasTypeConstant)aliasTypeId)
			{
				case AliasTypeConstant.Simple:
					{
						return TranslationItemMode.Simple;
					}
				case AliasTypeConstant.Multiline:
				case AliasTypeConstant.JavaScript:
				case AliasTypeConstant.Wysiwyg:
					{
						return TranslationItemMode.Multiline;
					}
				//case AliasTypeConstant.Wysiwyg:
				//    {
				//        return TranslationItemMode.Wysiwyg;
				//    }
				default:
					{
						throw new ArgumentException("Argument is not valid: aliasTypeId. No default aliasTypeId.");
					}
			}
		}

		protected string GetDefaultValueControlClientId(int aliasTypeId)
		{
			switch ((AliasTypeConstant)aliasTypeId)
			{
				case AliasTypeConstant.Simple:
					{
						return ValueTextBox.ClientID;
					}
				case AliasTypeConstant.Multiline:
				case AliasTypeConstant.JavaScript:
				case AliasTypeConstant.Wysiwyg:
					{
						return AreaValueTextBox.ClientID;
					}
				//case AliasTypeConstant.Wysiwyg:
				//	{
				//		return TinyMceValueTextArea.ClientID;
				//	}
				default:
					{
						throw new ArgumentException("Argument is not valid: aliasTypeId. No default aliasTypeId.");
					}
			}
		}

		protected virtual bool? JustCreated()
		{
			return Request.QueryString.GetBooleanOrNull("JustCreated");
		}
	}
}
