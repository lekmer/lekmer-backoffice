using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Aliases
{
	public partial class Default : LekmerPageController
    {
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				var master = Master as Alias;
				if (master == null) return;
				master.BreadcrumbAppend.Clear();
				master.BreadcrumbAppend.Add("Aliases");
				master.BreadcrumbAppend.Add("Search");
			}
		}

		protected override void SetEventHandlers()
		{
			AliasGrid.RowDataBound += AliasGridRowDataBound;
			AliasGrid.RowCommand += AliasGridRowCommand;
		}

		protected override void PopulateForm() { }

		protected virtual bool? ShowFolderContent()
		{
			return Request.QueryString.GetBooleanOrNull("ShowFolderContent");
		}

		protected virtual bool? IsSearchResult()
		{
			return Request.QueryString.GetBooleanOrNull("IsSearchResult");
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (ShowFolderContent().HasValue)
			{
				if (ShowFolderContent().Value)
				{
					((Alias) Master).ShowFolderContent = true;
					PopulateGrid();
					return;
				}
			}
			if (IsSearchResult().HasValue)
			{
				if (IsSearchResult().Value)
				{
					PopulateGrid();
					return;
				}
			}
			if (((Alias)Master).ShowFolderContent)
			{
				PopulateGrid();
			}
			else
			{
				((Alias)Master).DenySelection = true;
			}
		}

		protected virtual void AliasGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var hiddenId = (HiddenField)e.Row.FindControl("IdHiddenField");
			var hlEdit = (HyperLink)e.Row.FindControl("EditLink");
			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				hlEdit.NavigateUrl = PathHelper.Template.Alias.GetEditUrlForSearch(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
			}
			else
			{
				hlEdit.NavigateUrl = PathHelper.Template.Alias.GetEditUrlForDefault(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
			}
			
		}

		protected virtual void AliasGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "DeleteAlias") return;
			DeleteAlias(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
		}

		protected void PopulateGrid()
        {
			var aliasService = IoC.Resolve<IAliasSecureService>();
			if (((Alias) Master).SelectedFolderId.HasValue)
			{
				Collection<IAlias> aliases = aliasService.GetAllByAliasFolder(((Alias)Master).SelectedFolderId.Value);
				AliasGrid.DataSource = aliases;
				AliasGrid.Columns[2].Visible = false;
				AliasGrid.DataBind();
			}
			else
			{
				string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Template.Alias.GetSearchResultUrl());
				Collection<IAlias> aliases = aliasService.Search(searchName);
				AliasGrid.DataSource = aliases;
				AliasGrid.DataBind();
			}
        }

		protected virtual string GetPath(int aliasFolderId)
		{
			Collection<IAliasFolder> aliasFolders = ((Alias)Master).GetAllAliasFolders();
			IAliasFolder aliasFolder = aliasFolders.FirstOrDefault(f => f.Id == aliasFolderId);
			var stack = new Stack<string>();
			stack.Push(aliasFolder.Title);
			while (aliasFolder.ParentAliasFolderId.HasValue)
			{
				IAliasFolder aliasFolderCopy = aliasFolder;
				aliasFolder = aliasFolders.FirstOrDefault(f => f.Id == aliasFolderCopy.ParentAliasFolderId.Value);
				stack.Push(aliasFolder.Title);
			}
			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString();
		}

		protected static void DeleteAlias(int id)
        {
            var aliasService = IoC.Resolve<IAliasSecureService>();
            aliasService.Delete(SignInHelper.SignedInSystemUser, id);
        }
	}
}