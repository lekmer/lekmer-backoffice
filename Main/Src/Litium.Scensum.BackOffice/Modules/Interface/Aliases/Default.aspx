<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Aliases/Alias.Master"  CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Aliases.Default" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="AliasTabContent" ContentPlaceHolderID="AliasContent" runat="server">
<asp:UpdatePanel ID="AliasUpdatePanel" runat="server" UpdateMode="Always">
	<ContentTemplate>		
		<div id="alias-list">
				<asp:GridView
					ID="AliasGrid" 
					SkinID="grid" 
					runat="server" 
					AutoGenerateColumns="false"
					Width="100%">
					<Columns>
						<asp:TemplateField HeaderText="<%$ Resources:Interface, Literal_SystemName %>" ItemStyle-Width="34%">
							<ItemTemplate>
								 <asp:Image ID="Image" runat="server" ImageUrl="~/Media/Images/Interface/alias.png" />
								<asp:HiddenField ID="IdHiddenField" runat="server" Value='<%# Eval("Id") %>' />
								<uc:HyperLinkEncoded ID="EditLink" runat="server" Text='<%# Eval("CommonName")%>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Description %>" ItemStyle-Width="33%">
							<ItemTemplate>
								<uc:LiteralEncoded ID="DescriptionLiteral" runat="server" Text='<%# Eval("Description") != null && Eval("Description").ToString().Length > 50 ? Eval("Description").ToString().Substring(0, 50) : Eval("Description") %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Path" ItemStyle-Width="30%">
							<ItemTemplate>
								<asp:Label ID="PathLabel" runat="server" Text='<%# GetPath(int.Parse(Eval("AliasFolderId").ToString())) %>' ></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:ImageButton runat="server" OnClientClick='<%# "return DeleteConfirmation(\""+ Resources.Interface.Literal_aliasConfirmDelete+"\");"%>' ID="ibtnDeleteAlias" CommandName="DeleteAlias" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
	</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
