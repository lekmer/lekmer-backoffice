﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Themes/Theme.Master" CodeBehind="Default.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Interface.Themes.Default" %>

<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="ThemeDefaultContent" ContentPlaceHolderID="ThemeHolder" runat="server">
	<div class="theme">
		<div class="theme-grid">
			<sc:GridViewWithCustomPager ID="ThemeGrid" SkinID="grid" runat="server" Width="100%" AllowPaging="true"
				PageSize="<%$AppSettings:DefaultGridPageSize%>" AutoGenerateColumns="false" PagerSettings-Mode="NumericFirstLast">
				<Columns>
					<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="85%">
						<ItemTemplate>
							<div style="width: 100%; overflow: hidden;">
								<div style="float: left; max-width: 96%;">
									<asp:HiddenField ID="ThemeIdHidden" runat="server" Value='<%# Eval("Id") %>' />
									<uc:HyperLinkEncoded ID="ThemeTitleLink" runat="server" Text='<%# Eval("Title")%>'
										NavigateUrl='<%# PathHelper.Template.Theme.GetEditUrl(Convert.ToInt32(Eval("Id"))) %>' />
								</div>
								<div style="float: left;">
									<uc:ContextMenu ID="ThemeContextMenu" runat="server" CallerImageSrc="~/Media/Images/Common/context-menu.png"
										CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png" MenuContainerCssClass="context-menu-container"
										MenuShadowCssClass="context-menu-shadow" MenuCallerCssClass="context-menu-caller">
										<div class="context-menu-header">
											<%= Resources.General.Literal_Manage %>
										</div>
										<div class="menu-row-separator">
										</div>
										<div class="context-menu-row">
											<img src="<%=ResolveUrl("~/Media/Images/interface/move-copy.png") %>" />
											<asp:LinkButton ID="ThemeMenuCopyButton" runat="server" CommandArgument='<%# Eval("Id") %>'
												CommandName="CopyTheme" Text="<%$ Resources:Interface,Button_Copy %>" />
										</div>
										<div id="contextMenuDeleteEnabled" class="context-menu-row" runat="server" visible='<%# Convert.ToInt32(Eval("Id")) != 0 %>'>
											<img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
											<asp:LinkButton ID="ThemeMenuDeleteButton" runat="server" OnClientClick='<%# "return DeleteConfirmation(\""+Resources.Interface.Literal_theme+"\");"%>'
												CommandArgument='<%# Eval("Id") %>' CommandName="DeleteTheme" Text="<%$ Resources:General, Button_Delete%>" />
										</div>
										<div id="contextMenuDeleteDisabled" class="context-menu-row" runat="server" visible='<%# Convert.ToInt32(Eval("Id")) == 0 %>'>
											<img src="<%=ResolveUrl("~/Media/Images/Common/delete-disabled.gif") %>" />
											<asp:Literal ID="ThemeMenuDeleteLiteral" runat="server" Text="<%$ Resources:General, Literal_Delete %>" />
										</div>
									</uc:ContextMenu>
								</div>
							</div>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Interface,Literal_DefaultTheme %>" ItemStyle-Width="12%">
						<ItemTemplate>
							<asp:Label ID="ThemeDefaultLabel" runat="server" Text='<%# Convert.ToInt32(Eval("Id")) == 0 ? "Yes" : string.Empty %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
						<ItemTemplate>
							<asp:ImageButton ID="ThemeDeleteButton" runat="server" CommandName="DeleteTheme"
								CommandArgument='<%# Eval("Id") %>' ImageUrl='<%# Convert.ToInt32(Eval("Id")) == 0 ? "~/Media/Images/Common/delete-disabled.gif" : "~/Media/Images/Common/delete.gif" %>'
								Enabled='<%# Convert.ToInt32(Eval("Id")) != 0 %>' OnClientClick='<%# "return DeleteConfirmation(\""+Resources.Interface.Literal_theme+"\");"%>' />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</sc:GridViewWithCustomPager>
		</div>
	</div>
</asp:Content>
