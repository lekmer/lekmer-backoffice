<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Templates/Template.Master"  CodeBehind="TemplateEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Templates.TemplateEdit" %>
<%@ Register TagPrefix="uc" TagName="FragmentRegion" Src="~/UserControls/Template/FragmentRegion.ascx"  %>
<%@ Register TagPrefix="uc" TagName="TemplateSetting" Src="~/UserControls/Template/Setting.ascx"  %>
<%@ Register TagPrefix="uc" TagName="SettingRegion" Src="~/UserControls/Template/SettingRegion.ascx"  %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer ID="errors"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />	
				<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="PageValidationSummary" DisplayMode="List" ValidationGroup="vgTemplate"  />
				<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="FileValidationSummary" DisplayMode="List" ValidationGroup="FileUpload"  />
				<a name="bottom" ></a>
				<uc:MessageContainer ID="success"  MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />	</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="TemplateTabContent" ContentPlaceHolderID="TemplateContent" runat="server">
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>
	<script type='text/javascript' language='javascript'>
		function RegisterPopupClose() {
			$('div.popup-background').click(function() {$("div.popup-header-center input").click();});
		}
		
		function openAliasPopup(obj) {
			document.getElementById('<%=AliasContentBoxIdHiddenField.ClientID%>').value = obj;
			$find('aliasPopup').show();
		}

		function closeAliasPopup(aliasName) {
			var insertedAlias = '[Alias("' + aliasName + '")]';

			var contentBoxId = document.getElementById('<%=AliasContentBoxIdHiddenField.ClientID%>');
			var contentBox = document.getElementById(contentBoxId.value);

			// insert in cursor position.
			insert(contentBox, insertedAlias);
			$find('aliasPopup').hide();
		}

		function openIncludePopup(obj) {
			document.getElementById('<%=IncludeContentBoxIdHiddenField.ClientID%>').value = obj;
			$find('includePopup').show();
		}

		function closeIncludePopup(includeTitle) {
			var insertedInclude = '[Include("' + includeTitle + '")]';

			var contentBoxId = document.getElementById('<%=IncludeContentBoxIdHiddenField.ClientID%>');
			var contentBox = document.getElementById(contentBoxId.value);

			// insert in cursor position.
			insert(contentBox, insertedInclude);
			$find('includePopup').hide();
		}

		function openComponentPopup(obj) {
			document.getElementById('<%=ComponentContentBoxIdHiddenField.ClientID%>').value = obj;
			$find('componentPopup').show();
		}

		function closeComponentPopup(insertedComponent) {
			var contentBoxId = document.getElementById('<%=ComponentContentBoxIdHiddenField.ClientID%>');
			var contentBox = document.getElementById(contentBoxId.value);

			// insert in cursor position.
			insert(contentBox, insertedComponent);
			$find('componentPopup').hide();
		}

		function openFunctionPopup(obj) {
			document.getElementById('<%=FunctionContentBoxIdHiddenField.ClientID%>').value = obj;
		}

		function closeFunctionPopup(functionType, componentName) {
			var functionTypePrefix;
			var functionTypeSufix;
			if (functionType == 'Variable') {
				functionTypePrefix = '';
				functionTypeSufix = '';
			}
			else {
				functionTypePrefix = 'If ';
				functionTypeSufix = '][End If';
			}
			var insertedFunction = '[' + functionTypePrefix + componentName + functionTypeSufix + ']';

			var contentBoxId = document.getElementById('<%=FunctionContentBoxIdHiddenField.ClientID%>');
			var contentBox = document.getElementById(contentBoxId.value);

			// insert in cursor position.
			insert(contentBox, insertedFunction);
			$find('functionPopup').hide();
		}
	</script>
	
	<asp:Panel ID="TemplateEditPanel" runat="server" DefaultButton="SaveButton" CssClass="template-panel">

			<div id="template-create">
				<div class="column">
					<div class="input-box">
						<span><%= Resources.General.Literal_Title %>&nbsp; *</span>&nbsp;
						<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox" ErrorMessage="<%$Resources:GeneralMessage, TitleEmpty %>"  Display="None" ValidationGroup="vgTemplate" />
						<asp:RegularExpressionValidator runat="server" ID="RegularValidator" ControlToValidate="TitleTextBox" 
							ValidationExpression="^[\w\s-\.]+$" ErrorMessage="<%$ Resources:GeneralMessage, TitleContainsSpecialSymbols %>" 
							Display="None" ValidationGroup="vgTemplate"></asp:RegularExpressionValidator>
						<br />
						<asp:TextBox ID="TitleTextBox" runat="server" MaxLength="50" />						
					</div>
				</div>
				<asp:UpdatePanel ID="TemplateIdPanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div class="column" id="IdDiv" runat="server">
							<div class="input-box">
								<span><%= Resources.General.Literal_Id %></span>
								<br />
								<asp:TextBox ID="IdTextBox" runat="server" ReadOnly="true" ForeColor="#707070" />						
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
				<div class="column">
					<div id="is-alternate" class="input-box">
						<asp:CheckBox ID="ShowAlternateCheckBox" runat="server" AutoPostBack="true" />
						<span><%= Resources.Interface.Literal_UseAlternateTemplate %></span>
					</div>
				</div>
				<br class="clear" />
				<div class="template-image">
					<div class="image-upload">
						<div class="image-upload-caption">
							<%= Resources.Interface.Literal_TemplateImage %>
						</div>
						<div class="image-show">
							<asp:Image ID="TemplateImage" runat="server" AlternateText="None" class="template-img" />
							<asp:HiddenField ID="ImgHiddenField" runat="server" />
						</div>
						<div class="upload-box">
							<%= Resources.Interface.Literal_UploadImage %>
							<asp:RequiredFieldValidator ID="FileNameValidator" runat="server" ControlToValidate="FileUploadControl" ErrorMessage="File should be selected." Text="*" ValidationGroup="FileUpload" />
							<br />
							<div class="upload-row">
								<asp:FileUpload ID="FileUploadControl" runat="server" CssClass="file-upload" ContentEditable="false" />
							</div>
							<br />
							<div class="upload-row">
								<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="UploadButton" runat="server" Text="<%$ Resources:Interface,Button_Upload %>" CssClass="upload-buttons" SkinID="DefaultButton" ValidationGroup="FileUpload" />
								
								<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="RemoveButton" runat="server" Text="<%$ Resources:Interface,Button_Remove %>" CssClass="upload-buttons" SkinID="DefaultButton"/>
							</div>
						</div>
						<br class="clear" />
					</div>
				</div>
				<br class="clear" />				
			</div>
			
			<asp:UpdatePanel ID="FragmentsUpdatePanel" runat="server" UpdateMode="Always">
			<ContentTemplate>	
			
			<div id="template-fragment">
				<uc:SettingRegion ID="SettingRegionControl" runat="server" />
				<br class="clear" />
				<asp:Repeater ID="FragmentRegionGrid" runat="server">
					<ItemTemplate>
						<uc:FragmentRegion ID="FragmentRegionControl" runat="server"
							OnFunctionAdded="OnFragmentRegionFunctionAdded"
						/>
					</ItemTemplate>
				</asp:Repeater>
			</div>
			<asp:PlaceHolder ID="ContentAreasPlaceHolder" runat="server" />
			
			<div id="AliasAddDiv" runat="server" class="popup-container" style="display: none;" onmouseout="RegisterPopupClose();">
				<div class="popup-header">
					<div class="popup-header-left">
					</div>
					<div class="popup-header-center">
						<span><%= Resources.Interface.Literal_Aliases %></span>
						<asp:Button ID="CancelBlockAliasButton" runat="server" Text="X" />
					</div>
					<div class="popup-header-right">
					</div>
				</div>
				<div class="popup-content">
					<asp:UpdatePanel ID="AliasUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="column">
								<CustomControls:TemplatedTreeView runat="server" ID="AliasTreeViewControl"
									NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"				
									DisplayTextControl="lbName"  MainContainerCssClass="treeview-main-container"
									NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"
									NodeMainContainerCssClass="treeview-node" NodeParentContainerCssClass="treeview-node-parent" 
									NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png" NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
									MenuCallerElementCssClass="tree-menu-caller" MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
									<HeaderTemplate>
									</HeaderTemplate>
									<NodeTemplate>
										  <div class="tree-item-cell-expand">
												<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
												<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
									      </div>
										  <div class="tree-item-cell-main">
												<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
												<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton></div>
										  <br />
									</NodeTemplate>
								</CustomControls:TemplatedTreeView>
							</div>
							<div class="column">
								<div class="template-variables-repeater">
									<asp:Repeater ID="AliasRepeater" runat="server">
										<ItemTemplate>
											<div class="template-variables-item">
												<uc:HyperLinkEncoded  ID="NameButton" runat="server" Text='<%# Eval("CommonName")%>' NavigateUrl='<%# "javascript:closeAliasPopup(\"" + Eval("CommonName") + "\");" %>' />
												<br />
												<uc:LiteralEncoded ID="DescriptionLiteral" runat="server" Text='<%# Eval("Description")%>'></uc:LiteralEncoded>
												<br />
											</div>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<asp:HiddenField ID="AliasContentBoxIdHiddenField" runat="server" />
					<br class="clear">
				</div>
			</div>
			<asp:HiddenField ID="AliasHiddenField" runat="server" />
			<ajaxtoolkit:modalpopupextender 
				id="AliasPopup" 
				BehaviorID="aliasPopup"
				runat="server" 
				popupcontrolid="AliasAddDiv"
				targetcontrolid="AliasHiddenField" 
				cancelcontrolid="CancelBlockAliasButton" 
				backgroundcssclass="popup-background" />
				
			<div id="IncludeAddDiv" runat="server" class="popup-container" style="display: none;" onmouseout="RegisterPopupClose();">
				<div class="popup-header">
					<div class="popup-header-left">
					</div>
					<div class="popup-header-center">
						<span><%= Resources.Interface.Literal_Includes %></span>
						<asp:Button ID="CancelBlockIncludeButton" runat="server" Text="X" />
					</div>
					<div class="popup-header-right">
					</div>
				</div>
				<div class="popup-content">
					<asp:UpdatePanel ID="IncludeUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="column">
								<CustomControls:TemplatedTreeView runat="server" ID="IncludeTreeViewControl"
									NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
									DisplayTextControl="lbTitle"  MainContainerCssClass="treeview-main-container"
									NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"
									NodeMainContainerCssClass="treeview-node" NodeParentContainerCssClass="treeview-node-parent" 
									NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png" NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
									MenuCallerElementCssClass="tree-menu-caller" MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
									<HeaderTemplate>
									</HeaderTemplate>
									<NodeTemplate>
										  <div class="tree-item-cell-expand">
												<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
												<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
										  </div>
										  <div class="tree-item-cell-main">
												<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
												<asp:LinkButton runat="server" ID="lbTitle" CommandName="Navigate"></asp:LinkButton></div>
										  <br />
									</NodeTemplate>
								</CustomControls:TemplatedTreeView>
							</div>
							<div class="column">
								<div class="template-variables-repeater">
									<asp:Repeater ID="IncludeRepeater" runat="server">
										<ItemTemplate>
											<div class="template-variables-item">
												<uc:HyperLinkEncoded  ID="NameButton" runat="server" Text='<%# Eval("CommonName")%>' NavigateUrl='<%# "javascript:closeIncludePopup(\"" + Eval("CommonName") + "\");" %>' />
												<br />
												<uc:LiteralEncoded ID="DescriptionLiteral" runat="server" Text='<%# Eval("Description")%>'></uc:LiteralEncoded>
												<br />
											</div>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<asp:HiddenField ID="IncludeContentBoxIdHiddenField" runat="server" />
					<br class="clear">
				</div>
			</div>
			<asp:HiddenField ID="IncludeHiddenField" runat="server" />
	
			<ajaxtoolkit:modalpopupextender 
				id="IncludePopup" 
				BehaviorID="includePopup"
				runat="server" 
				popupcontrolid="IncludeAddDiv"
				targetcontrolid="IncludeHiddenField" 
				cancelcontrolid="CancelBlockIncludeButton" 
				backgroundcssclass="popup-background" />
		
		
			<div id="FunctionAddDiv" runat="server" class="popup-container" style="display: none;" onmouseout="RegisterPopupClose();">
				<div class="popup-header">
					<div class="popup-header-left">
					</div>
					<div class="popup-header-center">
						<span><%= Resources.Interface.Literal_Functions %></span>
						<asp:Button ID="CancelBlockFunctionButton" runat="server" Text="X" />
					</div>
					<div class="popup-header-right">
					</div>
				</div>
				<div class="popup-content">
					<asp:UpdatePanel ID="FunctionUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="column">
								<CustomControls:TemplatedTreeView runat="server" ID="FunctionTreeViewControl"
									NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
									DisplayTextControl="lbName"  MainContainerCssClass="treeview-main-container"
									NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"
									NodeMainContainerCssClass="treeview-node" NodeParentContainerCssClass="treeview-node-parent" 
									NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png" NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
									MenuCallerElementCssClass="tree-menu-caller" MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
									<HeaderTemplate>
									</HeaderTemplate>
									<NodeTemplate>
										  <div class="tree-item-cell-expand">
												<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
												<asp:Button ID="Expander" runat="server" CssClass="tree-item-expander-hidden"/>
										  </div>
										  <div class="tree-item-cell-main">
												<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
												<asp:LinkButton runat="server" ID="lbName"></asp:LinkButton></div>
										  <br />
									</NodeTemplate>
								</CustomControls:TemplatedTreeView>
								<asp:Label ID="FunctionsLabel" runat="server" Text="<%$ Resources:Interface, Literal_NoItemsForCurrentFragmentModel %>"></asp:Label>
							</div>
							<div class="column">
								<div class="template-variables-repeater">
									<asp:Repeater ID="FunctionsRepeater" runat="server">
										<ItemTemplate>
											<div class="template-variables-item">
												<uc:HyperLinkEncoded  ID="NameButton" runat="server" Text='<%# Eval("CommonName")%>' NavigateUrl='<%# "javascript:closeFunctionPopup(\"" + Eval("FunctionType") + "\", \"" + Eval("CommonName").ToString().Replace("\"","\\\"") + "\");" %>' />
												<br />
												<uc:LiteralEncoded ID="ValueLiteral" runat="server" Text='<%# Eval("Description")%>'></uc:LiteralEncoded>
												<br />
											</div>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<asp:HiddenField ID="FunctionContentBoxIdHiddenField" runat="server" />
					<br class="clear">
				</div>
			</div>
			<asp:HiddenField ID="FunctionHiddenField" runat="server" />
			<ajaxtoolkit:modalpopupextender
				id="FunctionPopup" 
				BehaviorID="functionPopup"
				runat="server" 
				popupcontrolid="FunctionAddDiv"
				targetcontrolid="FunctionHiddenField" 
				cancelcontrolid="CancelBlockFunctionButton" 
				backgroundcssclass="popup-background" />
				
						
			<div id="ComponentAddDiv" runat="server" class="popup-container" style="display: none;" onmouseout="RegisterPopupClose();">
				<div class="popup-header">
					<div class="popup-header-left">
					</div>
					<div class="popup-header-center">
						<span><%= Resources.Interface.Literal_Components %></span>
						<asp:Button ID="CancelBlockComponentButton" runat="server" Text="X" />
					</div>
					<div class="popup-header-right">
					</div>
				</div>
				<div class="popup-content">
					<asp:UpdatePanel ID="ComponentUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="column">
								<div class="template-variables-repeater-wide">
									<asp:Repeater ID="ComponentRepeater" runat="server">
										<ItemTemplate>
											<div class="template-variables-item">
												<uc:HyperLinkEncoded  ID="TitleLink" runat="server"/>
												<br />
												<asp:Repeater ID="ComponentParameterRepeater" runat="server">
													<ItemTemplate>
														<uc:LiteralEncoded ID="DescriptionLiteral" runat="server" Text='<%# Eval("Title") + " - " + Eval("Description") %>'></uc:LiteralEncoded>
														<br />
													</ItemTemplate>
												</asp:Repeater>
											</div>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
					<asp:HiddenField ID="ComponentContentBoxIdHiddenField" runat="server" />
					<br class="clear">
				</div>
			</div>
			<asp:HiddenField ID="ComponentHiddenField" runat="server" />
	
			<ajaxtoolkit:modalpopupextender
				id="ComponentPopup" 
				BehaviorID="componentPopup"
				runat="server" 
				popupcontrolid="ComponentAddDiv"
				targetcontrolid="ComponentHiddenField" 
				cancelcontrolid="CancelBlockComponentButton" 
				backgroundcssclass="popup-background" />

				
			</ContentTemplate>
		</asp:UpdatePanel>
		<br class="clear"/>		
		<asp:UpdatePanel id="UpdateFragmentUpdatePanel" runat="server">
			<ContentTemplate>
				<br class="clear" />
				<div class="buttons left">
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"  SkinID="DefaultButton" />
				</div>
				<div class="buttons right">
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" ValidationGroup="vgTemplate" SkinID="DefaultButton" 	/>		
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
				</div>
					
			</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>
