
using Litium.Scensum.BackOffice.Setting;


namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
	/// <summary>
	/// Template Image Handler
	/// </summary>
	public class TemplateImageLoader : TemplateImageLoaderBase
	{
		protected override int Width
		{
			get { return TemplateImageSetting.Instance.NormalWidth; }
		}

		protected override int Height
		{
			get { return TemplateImageSetting.Instance.NormalHeight; }
		}

		protected override int Quality
		{
			get { return TemplateImageSetting.Instance.NormalQuality; }
		}
	}
}
