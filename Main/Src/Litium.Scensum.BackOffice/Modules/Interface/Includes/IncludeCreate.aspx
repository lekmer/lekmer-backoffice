<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Includes/Include.Master"
	CodeBehind="IncludeCreate.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Includes.IncludeCreate" %>

<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree"
	Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer ID="message" MessageType="Failure" HideMessagesControlId="SaveButton"
							runat="server" />
						<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary "
							ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgInclude" />	</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="IncludeCreateContent" ContentPlaceHolderID="IncludeContent" runat="server">
	<asp:Panel ID="IncludeCreatePanel" runat="server" DefaultButton="SaveButton">
		<div class="include-create-edit">
			<div id="create-column-first" class="column">
				<div class="input-box">
					<span><%= Resources.Interface.Literal_SystemName %>&nbsp; *</span>&nbsp
					<asp:RequiredFieldValidator runat="server" ID="SystemNameValidator" ControlToValidate="SystemNameTextBox"
						ErrorMessage="<%$ Resources:InterfaceMessage, SystemNameEmpty %>" Display="None" ValidationGroup="vgInclude" />
					<asp:RegularExpressionValidator runat="server" ID="RegularValidator" ControlToValidate="SystemNameTextBox" 
						ValidationExpression="^[\w\s-\.]+$" ErrorMessage="<%$ Resources:GeneralMessage, SystemNameContainsSpecialSymbols %>" 
						Display="None" ValidationGroup="vgInclude"></asp:RegularExpressionValidator>
					<br />
					<asp:TextBox ID="SystemNameTextBox" runat="server" MaxLength="50" />
				</div>
				<div class="input-box">
					<span><%= Resources.General.Literal_Description %></span>
					<br />
					<asp:TextBox ID="DescriptionTextBox" runat="server" TextMode="MultiLine" Rows="3"/>				
				</div>
				<div class="input-box">
					<div id="include-source-code">
						<span><%= Resources.Interface.Literal_IncludeSourceCode %></span>
					</div>
					<asp:TextBox ID="ValueTextBox" runat="server" TextMode="MultiLine" Rows="15" />
				</div>
				<asp:UpdatePanel ID="FooterUpdatePanel" runat="server">
					<ContentTemplate>
					<br />
						<div class="buttons right">
								<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgInclude"
									SkinID="DefaultButton" />
								<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
							</div>						
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
