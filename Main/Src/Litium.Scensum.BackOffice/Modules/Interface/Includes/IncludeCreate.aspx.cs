using System;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Includes
{
	public partial class IncludeCreate : LekmerPageController,IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			Include master = Master as Include;
			if (master == null) return;
			master.BreadcrumbAppend.Clear();
			master.BreadcrumbAppend.Add(Resources.Interface.Literal_CreateInclude);
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}
		protected override void PopulateForm() { }

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.Include.GetDefaultUrlWithShowFolderContent());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (((Include)Page.Master).SelectedFolderId == null) return;

			IIncludeSecureService includeService = IoC.Resolve<IIncludeSecureService>();

			IInclude include = includeService.Create();
			include.CommonName = SystemNameTextBox.Text.Trim();
			include.Description = string.IsNullOrEmpty(DescriptionTextBox.Text) ? null : DescriptionTextBox.Text;
			include.Content = ValueTextBox.Text;
			include.IncludeFolderId = ((Include)Page.Master).SelectedFolderId.Value;

			ValidationResult validationResult = includeService.Validate(include);

			if (!validationResult.IsValid)
			{
				message.AddRange(validationResult.Errors);
				return;
			}

			includeService.Save(SignInHelper.SignedInSystemUser, include);
			Response.Redirect(PathHelper.Template.Include.GetDefaultUrlForJustCreated());
		}
	}
}
