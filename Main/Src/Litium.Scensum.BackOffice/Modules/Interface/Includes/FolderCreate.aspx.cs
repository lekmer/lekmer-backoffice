using System;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Includes
{
	public partial class FolderCreate : LekmerPageController,IEditor
	{
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			PutIncludeTreeControl.NodeCommand += OnNodeCommand;
		}
		protected override void PopulateForm()
		{
			if (Master.SelectedFolderId == null)
			{
				Master.DenySelection = true;
				PopulateTreeView(null);
			}
			else
			{
				if (Master.SelectedFolderId == Master.RootNodeId)
				{
					Master.DenySelection = true;
					Master.IsMainNode = true;
				}
				PutIncludeTreeControl.Enabled = false;
				PutIncludeTreeDiv.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			Master.BreadcrumbAppend.Clear();
			Master.BreadcrumbAppend.Add(Resources.Interface.Literal_CreateFolder);
			if (Master.DenySelection)
			{
				Master.BuildBreadcrumbs();
			}
			Include.RegisterStartupScript(PutIncludeTreeUpdatePanel, PutIncludeTreeUpdatePanel.GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", PutIncludeTreeControl.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", PutIncludeTreeControl.ClientID), true);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.Include.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			var includeFolderSecureService = IoC.Resolve<IIncludeFolderSecureService>();

			IIncludeFolder includeFolder = includeFolderSecureService.Create();
			includeFolder.Title = TitleTextBox.Text.Trim();
            if (Master.SelectedFolderId == null)
			{
				if (PutIncludeTreeControl.SelectedNodeId.HasValue)
				{
					includeFolder.ParentIncludeFolderId = PutIncludeTreeControl.SelectedNodeId.Value == PutIncludeTreeControl.RootNodeId
															? null
															: PutIncludeTreeControl.SelectedNodeId;
				}
			}
			else
			{
				includeFolder.ParentIncludeFolderId = Master.SelectedFolderId == Master.RootNodeId
															? null
															: Master.SelectedFolderId;
			}

			int includeFolderId;
			try
			{
				includeFolderId = includeFolderSecureService.Save(SignInHelper.SignedInSystemUser, includeFolder);
			}
			catch (ArgumentException exception)
			{
				errors.Add(exception.Message);
				return;
			}

			Master.SelectedFolderId = includeFolderId;
			Master.UpdateMasterTree();
			Response.Redirect(PathHelper.Template.Include.Folder.GetEditUrl(true));
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var folders = Master.GetTreeDataSource(folderId);
			if (folders == null || folders.Count <= 0) return;

			PutIncludeTreeControl.DataSource = folders;
			PutIncludeTreeControl.RootNodeTitle = Resources.Interface.Literal_Includes;
			PutIncludeTreeControl.DataBind();
			PutIncludeTreeControl.SelectedNodeId = folderId;
		}
	}
}
