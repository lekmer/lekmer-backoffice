using System.Globalization;

namespace Litium.Scensum.BackOffice.Modules.Media
{
	public abstract class ImageLoaderResizableBase : MediaLoaderBase
	{
		protected abstract int Width { get; }

		protected abstract int Height { get; }

		protected abstract int Quality { get; }

		protected override string GetETag(string mediaId)
		{
			return string.Format(CultureInfo.CurrentCulture, "{0}-{1}-{2}-{3}", mediaId, Width, Height, Quality);
		}
	}
}
