﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium.Lekmer.Media;
using Litium.Scensum.Media;
using Microsoft.SqlServer.Server;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaFolder
{
	public class MediaFolderHepler
	{
		private const string ProductsFolder = "Products";
		private const string BannersFolder = "Banners";
		private const string BrandsFolder = "Brands";
		private const string IconsFolder = "Icons";
		private const string ImagesFolder = "Images";
		private const string PdfFolder = "Pdf";
		private const string MoviesFolder = "Movies";
		public static MediaFolderType GetMediaFolderType(IMediaFolder folder)
		{
			var result = MediaFolderType.Other;
			if (folder != null)
			{
					if (folder.Title == ProductsFolder)
					{
						result = MediaFolderType.Products;
					}
					else if(folder.Title == BannersFolder)
					{
						result = MediaFolderType.Banners;
					}
					else if(folder.Title == BrandsFolder)
					{
						result = MediaFolderType.Brands;
					}
					else if(folder.Title == IconsFolder)
					{
						result = MediaFolderType.Icons;
					}
					else if (folder.Title == ImagesFolder)
					{
						result = MediaFolderType.Images;
					}
					else if (folder.Title == PdfFolder)
					{
						result = MediaFolderType.Pdf;
					}
					else if (folder.Title == MoviesFolder)
					{
						result = MediaFolderType.Movies;
					}
			}
			return result;
		}

		public static IMediaFolder GetFolderByType(ILekmerMediaFolderSecureService service, MediaFolderType mediaFolderType)
		{
			IMediaFolder result = null;
			switch (mediaFolderType)
			{
					case MediaFolderType.Products:
						var folderList = service.GetAllByTitle(ProductsFolder);
						if (folderList.Any())
						{
							result = folderList[0];
						}
					break;
					case MediaFolderType.Banners:
						folderList = service.GetAllByTitle(BannersFolder);
						if (folderList.Any())
						{
							var bannerFolder = folderList[0];
							var children = service.GetAllByParentId(bannerFolder.Id);
							if (children.Any())
							{
								result = children.SingleOrDefault(item => item.Title == ImagesFolder);
							}
						}
					break;
					case MediaFolderType.Brands:
						folderList = service.GetAllByTitle(BrandsFolder);
						if (folderList.Any())
						{
							var brandsFolder = folderList[0];
							var children = service.GetAllByParentId(brandsFolder.Id);
							if (children.Any())
							{
								result = children.SingleOrDefault(item => item.Title == ImagesFolder);
							}
						}
					break;
					case MediaFolderType.Icons:
						folderList = service.GetAllByTitle(IconsFolder);
						if (folderList.Any())
						{
							var iconsFolder = folderList[0];
							var children = service.GetAllByParentId(iconsFolder.Id);
							if (children.Any())
							{
								result = children.SingleOrDefault(item => item.Title == ImagesFolder);
							}
						}
					break;
					case MediaFolderType.Movies:
						folderList = service.GetAllByTitle(ProductsFolder);
						if (folderList.Any())
						{
							var productsFolder = folderList[0];
							var children = service.GetAllByParentId(productsFolder.Id);
							if (children.Any())
							{
								result = children.SingleOrDefault(item => item.Title == MoviesFolder);
							}
						}
					break;
					case MediaFolderType.Pdf:
						folderList = service.GetAllByTitle(ProductsFolder);
						if (folderList.Any())
						{
							var productsFolder = folderList[0];
							var children = service.GetAllByParentId(productsFolder.Id);
							if (children.Any())
							{
								result = children.SingleOrDefault(item => item.Title == PdfFolder);
							}
						}
					break;
					case MediaFolderType.Images:
						folderList = service.GetAllByTitle(ProductsFolder);
						if (folderList.Any())
						{
							var productsFolder = folderList[0];
							var children = service.GetAllByParentId(productsFolder.Id);
							if (children.Any())
							{
								result = children.SingleOrDefault(item => item.Title == ImagesFolder);
							}
						}
					break;
			}
			return result;
		}
	}
	 
	



	public enum MediaFolderType
	{
		Banners,
		Brands, 
		Icons,
		Products,
		Images,
		Pdf,
		Movies,
		Other

	}

}