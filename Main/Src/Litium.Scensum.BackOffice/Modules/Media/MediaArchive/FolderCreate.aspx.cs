using System;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class FolderCreate : LekmerPageController,IEditor 
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			Media.RegisterStartupScript(this, GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", PutMediaTree.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", PutMediaTree.ClientID), true);
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			PutMediaTree.NodeCommand += OnNodeCommand;
		}
		protected override void PopulateForm()
		{
			var master = Page.Master as Media;
			if (master == null) return;

			master.BreadcrumbAppend.Clear();
			master.BreadcrumbAppend.Add(Resources.General.Literal_CreateFolder);
			if (master.SelectedFolderId != null)
			{
				PutMediaTreeDiv.Visible = false;
				PutMediaTree.SelectedNodeId = null;
				if (master.SelectedFolderId == PutMediaTree.RootNodeId)
				{
					master.DenySelection = true;
					master.IsMainNode = true;
				}
				return;
			}
			PutMediaTreeDiv.Visible = true;
			PopulateTreeView(master.SelectedFolderId);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Media.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			var mediaFolderSecureService = IoC.Resolve<IMediaFolderSecureService>();
			var mediaFolder = IoC.Resolve<IMediaFolder>();

			mediaFolder.MediaFolderParentId = Master.SelectedFolderId == Master.GetMediaTree().RootNodeId ? null : Master.SelectedFolderId;
			
			if (mediaFolder == null)
			{
				throw new BusinessObjectNotExistsException((int)((Media)Page.Master).SelectedFolderId);
			}
			mediaFolder.Title = TitleTextBox.Text.Trim();
			if (PutMediaTree.SelectedNodeId.HasValue)
			{
				mediaFolder.MediaFolderParentId = PutMediaTree.SelectedNodeId.Value == PutMediaTree.RootNodeId ? null : PutMediaTree.SelectedNodeId;
			}
			try
			{
				mediaFolder.Id = mediaFolderSecureService.Save(SignInHelper.SignedInSystemUser, mediaFolder);
			}
			catch (ArgumentException exception)
			{
				errors.Add(exception.Message);
				return;
			}

			((Media)Page.Master).SelectedFolderId = mediaFolder.Id;
			Response.Redirect(PathHelper.Media.GetEditUrl(true));
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var master = (Media)Page.Master;
			var folders = master.GetMediaFolders(folderId ?? master.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;

			PutMediaTree.DataSource = folders;
			PutMediaTree.RootNodeTitle = Resources.Media.Literal_Media;
			PutMediaTree.DataBind();
			PutMediaTree.SelectedNodeId = folderId ?? PutMediaTree.RootNodeId;
		}
	}
}
