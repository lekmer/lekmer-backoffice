<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modules/Media/MediaArchive/Media.Master" CodeBehind="FolderEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.MediaArchive.FolderEdit" %>

<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="messager"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />								
						<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgMediaFolder"  />
					</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="FolderEditContent" ContentPlaceHolderID="MediaContent" runat="server">
	<asp:Panel ID="FolderEditPanel" runat="server" DefaultButton="SaveButton">
	<div class="include-create-edit">	
		<div class="column">
			<div class="input-box">
				<span><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Title%>" /> *</span>
				<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty%>" Display="None" ValidationGroup="vgMediaFolder"  />
				<br />
				<asp:TextBox ID="TitleTextBox" runat="server" />
			</div>	
			<asp:UpdatePanel id="FolderEditUpdatePanel" runat="server">
				<ContentTemplate>
					<div class="input-box">
						<div class="buttons">
							<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" ValidationGroup="vgMediaFolder"  Text="<%$ Resources:General, Button_Save%>" SkinID="DefaultButton" />
							
							<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel%>" SkinID="DefaultButton" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
		<div class="column">
			<div class="input-box">
				<span><asp:Literal runat="server" Text="<%$ Resources:General, Literal_PutInto%>" /></span>
				<asp:UpdatePanel ID="PutMediaTreeUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<CustomControls:TemplatedTreeView runat="server" ID="PutMediaTree"
							UseRootNode="true" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
							DisplayTextControl="lbName"  MainContainerCssClass="treeview-main-container"
							NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"
							NodeMainContainerCssClass="treeview-node" NodeParentContainerCssClass="treeview-node-parent" 
							NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png" NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
							MenuCallerElementCssClass="tree-menu-caller" MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
							<HeaderTemplate>
								<div class="treeview-header"><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Title%>" /></div>
							</HeaderTemplate>              
							<NodeTemplate>
								<div class="tree-item-cell-expand">
									<img src='<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>' alt="" class="tree-icon" />
									<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
								</div>
								<div class="tree-item-cell-main">
									<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt=""  class="tree-node-img"/>
									<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
								</div>                 
								<br />
							</NodeTemplate>
						</CustomControls:TemplatedTreeView>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
	</div>
	</asp:Panel>
</asp:Content>