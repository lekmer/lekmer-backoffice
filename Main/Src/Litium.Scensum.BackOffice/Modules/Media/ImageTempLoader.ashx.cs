using System.IO;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using MediaBackOfficeSetting = Litium.Scensum.BackOffice.Setting.MediaSetting;

namespace Litium.Scensum.BackOffice.Modules.Media
{
	public class ImageTempLoader : ImageLoaderResizableBase
	{
		protected override int Width
		{
			get { return MediaBackOfficeSetting.Instance.ThumbnailWidth; }
		}

		protected override int Height
		{
			get { return MediaBackOfficeSetting.Instance.ThumbnailHeight; }
		}

		protected override int Quality
		{
			get { return MediaBackOfficeSetting.Instance.ThumbnailQuality; }
		}

		protected override Stream GetMedia(string mediaId, string extension, out string mime)
		{
			if (string.IsNullOrEmpty(mediaId))
			{
				mime = string.Empty;
				return null;
			}
			return IoC.Resolve<IImageSecureService>().LoadTemporaryImage(mediaId, extension, Width, Height, Quality, out mime);
		}
	}
}
