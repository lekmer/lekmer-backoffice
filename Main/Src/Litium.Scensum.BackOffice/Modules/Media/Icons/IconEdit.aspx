﻿<%@ Page Language="C#" MasterPageFile="Icons.Master" CodeBehind="IconEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.Icons.IconEdit" AutoEventWireup="true"%>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="IconMedia" Src="~/Modules/Media/Icons/IconMedia.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="IconEditContent" runat="server" ContentPlaceHolderID="IconsPlaceHolder">
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>

	<div style="padding: 10px; float: left;">
		<div class="brand-edit-header">
			<uc:LiteralEncoded ID="IconHeader" runat="server"></uc:LiteralEncoded>
		</div>

		<div class="column">
			<div class="input-box">
				<span><%=Resources.General.Literal_Title%> *</span>
				<uc:GenericTranslator ID="IconTitleTranslator" runat="server" />
				<br />
				<asp:TextBox ID="TitleTextBox" runat="server" MaxLength="50" Width="500px" />
			</div>
			<div class="input-box">
				<span><%=Resources.General.Literal_Description%></span>
				<asp:ImageButton runat="server" ID="IconDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
				<uc:GenericWysiwygTranslator ID="IconDescriptionTranslator" runat="server" />
				<br />
				<uc:LekmerTinyMceEditor ID="DescriptionTextBox" runat="server" SkinID="tinyMCE" Width="530px"/>
			</div>
		</div>

		<div class="column">
			<div class="input-box">
				<uc:IconMedia runat="server" Id="IconMediaControl" />
			</div>
		</div>
	</div>

	<br class="clear" />

	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
	</div>
</asp:Content>