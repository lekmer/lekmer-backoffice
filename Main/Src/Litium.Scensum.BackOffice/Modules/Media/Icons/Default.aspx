﻿<%@ Page Language="C#" MasterPageFile="Icons.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.Icons.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="IconsTabContent" ContentPlaceHolderID="MessageContainer" runat="server">
</asp:Content>

<asp:Content ID="IconsDefaultContent" runat="server" ContentPlaceHolderID="IconsPlaceHolder">
	<div>
		<sc:GridViewWithCustomPager
			ID="IconsGrid"
			SkinID="grid"
			runat="server"
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			DataSourceID="IconObjectDataSource"
			Width="100%"
			PagerSettings-Mode="NumericFirstLast">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="95%">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="hlIcon" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# string.Format("IconEdit.aspx?Id={0}", int.Parse(Eval("Id").ToString())) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<ItemTemplate>
						<asp:ImageButton ID="IconDeleteButton" runat="server" CommandName="DeleteIcon"
							CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
							OnClientClick='<%# "return DeleteConfirmation(\""+Resources.Media.Literal_IconSmall+"\");"%>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
	<asp:ObjectDataSource ID="IconObjectDataSource" runat="server" 
		EnablePaging="true" 
		SelectCountMethod="SelectCount" 
		SelectMethod="SelectMethod" 
		TypeName="Litium.Scensum.BackOffice.Modules.Media.Icons.IconDataSource" />
</asp:Content>