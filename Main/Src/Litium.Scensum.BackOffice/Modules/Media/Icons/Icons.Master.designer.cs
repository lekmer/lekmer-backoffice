﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Litium.Scensum.BackOffice.Modules.Media.Icons {
    
    
    public partial class IconsMaster {
        
        /// <summary>
        /// IconsToolBoxPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.ToolBoxPanel IconsToolBoxPanel;
        
        /// <summary>
        /// CreateIconButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton CreateIconButton;
        
        /// <summary>
        /// MessageContainer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder MessageContainer;
        
        /// <summary>
        /// IconsPlaceHolder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder IconsPlaceHolder;
    }
}
