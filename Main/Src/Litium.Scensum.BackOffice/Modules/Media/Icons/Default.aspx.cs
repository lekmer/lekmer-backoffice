﻿using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Media.Icons
{
	public partial class Default : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			IconsGrid.PageIndexChanging += OnPageIndexChanging;
			IconsGrid.RowCommand += OnRowCommand;
		}

		protected override void PopulateForm()
		{
			PopulateGrid();
		}

		protected void OnRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteIcon") return;

			var service = IoC.Resolve<IIconSecureService>();
			service.Delete(SignInHelper.SignedInSystemUser, Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			PopulateGrid();
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			IconsGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void PopulateGrid()
		{
			IconObjectDataSource.SelectParameters.Clear();
			IconObjectDataSource.SelectParameters.Add("maximumRows", IconsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			IconObjectDataSource.SelectParameters.Add("startRowIndex", (IconsGrid.PageIndex*IconsGrid.PageSize).ToString(CultureInfo.CurrentCulture));

			IconsGrid.DataBind();
		}
	}
}