﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Media.Icons
{
	public partial class IconEdit : LekmerStatePageController<IconState>, IEditor
	{
		private List<string> _validationMessage = new List<string>();

		protected virtual IconsMaster MasterPage
		{
			get { return (IconsMaster) Master; }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			IconMediaControl.Selected += OnIconMediaSelected;
			IconMediaControl.DeleteImage += OnIconMediaDeleted;

			IconDescriptionTranslator.TriggerImageButton = IconDescriptionTranslateButton;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();

			EnsureState();

			if (id.HasValue)
			{
				var icon = IoC.Resolve<IIconSecureService>().GetByIdSecure(id.Value);
				TitleTextBox.Text = icon.Title;
				DescriptionTextBox.SetValue(icon.Description ?? string.Empty);
				SetMediaId(icon.Image.Id);
				IconMediaControl.ImageId = icon.Image.Id;

				IconHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", Resources.Media.Literal_EditIcon, icon.Title);
			}
			else
			{
				IconHeader.Text = Resources.Media.Literal_CreateIcon;
			}

			PopulateTranslations(id);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.Media.IconSaveSuccess);
				}
			}

			MasterPage.SetupTabAndPanel();
		}


		private void OnIconMediaDeleted(object sender, EventArgs e)
		{
			SetMediaId(null);
		}

		private void OnIconMediaSelected(object sender, ImageSelectEventArgs e)
		{
			SetMediaId(e.Image.Id);
		}

		public void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				Response.Redirect(LekmerPathHelper.Icon.GetDefaultUrl());
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}

				SystemMessageContainer.MessageType = InfoType.Warning;

				return;
			}

			int? id = GetIdOrNull();
			
			var iconSecureService = IoC.Resolve<IIconSecureService>();

			var icon = id.HasValue ? iconSecureService.GetByIdSecure(id.Value) : IoC.Resolve<IIcon>();
			icon.Title = TitleTextBox.Text;
			icon.Description = DescriptionTextBox.GetValue();
			icon.MediaId = State.MediaId.Value;

			try
			{
				icon = iconSecureService.Save(SignInHelper.SignedInSystemUser, icon, IconTitleTranslator.GetTranslations(), IconDescriptionTranslator.GetTranslations());
			}
			catch
			{
				SystemMessageContainer.Add(Resources.Media.IconUpdateFailed);
				return;
			}

			if (icon.Id > 0)
			{
				IconHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", Resources.Media.Literal_EditIcon, icon.Title);
				Response.Redirect(LekmerPathHelper.Icon.GetEditUrlWithMessage(icon.Id));
			}
		}


		protected void SetMediaId(int? id)
		{
			EnsureState();
			State.MediaId = id;
			State.SetNewImage = true;
		}

		protected void EnsureState()
		{
			if (State == null)
			{
				State = new IconState();
			}
		}

		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidTitle())
			{
				_validationMessage.Add(Resources.GeneralMessage.TitleEmpty);
				isValid = false;
			}

			if (!IsValidImage())
			{
				_validationMessage.Add(Resources.Media.DefaultImageEmpty);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidTitle()
		{
			string title = TitleTextBox.Text.Trim();

			if (string.IsNullOrEmpty(title))
			{
				return false;
			}

			return title.Length <= 50;
		}

		protected virtual bool IsValidImage()
		{
			return State.MediaId.HasValue;
		}

		protected virtual void PopulateTranslations(int? id)
		{
			IconTitleTranslator.DefaultValueControlClientId = TitleTextBox.ClientID;
			IconDescriptionTranslator.DefaultValueControlClientId = DescriptionTextBox.ClientID;

			if (id.HasValue)
			{
				var iconSecureService = IoC.Resolve<IIconSecureService>();

				IconTitleTranslator.BusinessObjectId = id.Value;
				IconDescriptionTranslator.BusinessObjectId = id.Value;

				IconTitleTranslator.DataSource = iconSecureService.GetAllTitleTranslations(id.Value);
				IconDescriptionTranslator.DataSource = iconSecureService.GetAllDescriptionTranslations(id.Value);
			}
			else
			{
				IconTitleTranslator.BusinessObjectId = 0;
				IconDescriptionTranslator.BusinessObjectId = 0;

				IconTitleTranslator.DataSource = new Collection<ITranslationGeneric>();
				IconDescriptionTranslator.DataSource = new Collection<ITranslationGeneric>();
			}

			IconTitleTranslator.DataBind();
			IconDescriptionTranslator.DataBind();
		}
	}

	[Serializable]
	public class IconState
	{
		public bool SetNewImage { get; set; }
		public int? MediaId { get; set; }
	}
}