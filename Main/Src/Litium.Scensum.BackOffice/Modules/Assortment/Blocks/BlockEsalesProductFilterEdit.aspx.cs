﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Contract;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using System.Globalization;
using Litium.Scensum.Product;
using Litium.Scensum.Foundation;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Template;
using Litium.Scensum.Core;
using Litium.Scensum.BackOffice.UserControls;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockEsalesProductFilterEdit : LekmerStatePageController<ProductEsalesFilterState>, IEditor
	{
		private Collection<IRatingFolder> _allRatingFolders;

		private const string EmptyItemValue = "0";
		private const string SortOptionEmptyItemValue = "";

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		private IBlockEsalesProductFilter _block;
		private IBlockEsalesProductFilter GetBlock()
		{
			if (_block == null)
			{
				_block = IoC.Resolve<IBlockEsalesProductFilterSecureService>().GetByIdSecure(GetBlockId());
			}

			return _block;
		}

		protected override void OnInit(EventArgs e)
		{
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnInit(e);

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.Lekmer.Literal_EditEsalesProductFilterBlock);
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			CategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
			AddBrandsButton.Click += AddBrandsClick;
			RemoveBrandsButton.Click += RemoveBrandsClick;
			TagRepeater.ItemDataBound += OnTagRepeaterItemDataBound;

			RatingGrid.RowCommand += OnRowCommand;
			RatingGrid.PageIndexChanging += OnPageIndexChanging;
			RatingSelector.SelectEvent += OnRatingsAdd;
		}

		protected override void PopulateForm()
		{
			InitializeState();

			var block = GetBlock();

			State.BrandIds = block.DefaultBrandIds;
			PopulateSortOptionDropDownList(SortList, Enum.GetValues(typeof(SortOption)).Cast<SortOption>().Where(so => so != SortOption.Unknown), block.DefaultSort);
			BlockTitleTextBox.Text = block.Title;
			PopulateCategory(block.DefaultCategoryId);

			var templates = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockEsalesProductFilter");
			PopulateDropDownList(PrimaryTemplateList, templates, block.TemplateId);
			PopulateDropDownList(SecondaryTemplateList, templates, block.SecondaryTemplateId);

			var ageIntervals = IoC.Resolve<IAgeIntervalSecureService>().GetAll();
			PopulateDropDownList(AgeIntervalList, ageIntervals, block.DefaultAgeIntervalId);

			PopulatePriceIntervalList(block.DefaultPriceIntervalId);

			PopulateBrands(block.DefaultBrandIds);

			PopulateTags();

			var sizes = IoC.Resolve<ISizeSecureService>().GetAll();
			PopulateDropDownList(SizeList, sizes, block.DefaultSizeId);

			PopulateRating();

			PopulateProductTypes(block.TargetProductTypeIds);

			TimeLimiter.Block = block;

			TargetDevice.Block = block;

			BlockEsalesSetting.BlockTypeId = block.BlockTypeId;
			BlockEsalesSetting.EsalesSetting = block.EsalesSetting;
		}

		private void PopulateCategory(int? id)
		{
			if (id != null)
			{
				CategoryNodeSelector.SelectedNodeId = id;
			}

			CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(id);
			CategoryNodeSelector.DataBind();
			CategoryNodeSelector.PopulatePath(id);
		}

		private static void PopulateDropDownList(DropDownList dropDownList, object list, int? selectedId)
		{
			dropDownList.DataSource = list;
			dropDownList.DataBind();
			var emptyItem = new ListItem(string.Empty, EmptyItemValue);
			dropDownList.Items.Insert(0, emptyItem);
			if (!selectedId.HasValue)
			{
				emptyItem.Selected = true;
			}
			else
			{
				var selectedItem = dropDownList.Items.FindByValue(selectedId.ToString());
				if (selectedItem != null)
				{
					selectedItem.Selected = true;
				}
			}
		}

		private static void PopulateSortOptionDropDownList(DropDownList dropDownList, object list, string selectedId)
		{
			dropDownList.DataSource = list;
			dropDownList.DataBind();
			var emptyItem = new ListItem(string.Empty, SortOptionEmptyItemValue);
			dropDownList.Items.Insert(0, emptyItem);

			if (string.IsNullOrEmpty(selectedId))
			{
				emptyItem.Selected = true;
			}
			else
			{
				var selectedItem = dropDownList.Items.FindByValue(selectedId);
				if (selectedItem != null)
				{
					selectedItem.Selected = true;
				}
			}
		}

		private void PopulatePriceIntervalList(int? selectedId)
		{
			var priceIntervals = IoC.Resolve<IPriceIntervalSecureService>().GetAll();

			PriceIntervalExtendedList.DataSource = priceIntervals;
			PriceIntervalExtendedList.DataBind();
			var emptyItem = new ExtendedListItem(string.Empty, EmptyItemValue, ListItemGroupingType.None);
			PriceIntervalExtendedList.ExtendedItems.Insert(0, emptyItem);

			if (!selectedId.HasValue)
			{
				emptyItem.Selected = true;
			}
			else
			{
				var selectedItem = PriceIntervalExtendedList.Items.FindByValue(selectedId.ToString());
				if (selectedItem != null)
				{
					selectedItem.Selected = true;
				}
			}
		}

		private void PopulateBrands(Collection<int> selectedBrandIds)
		{
			var brandSecureService = IoC.Resolve<IBrandSecureService>();
			var allBrands = brandSecureService.GetAll();

			var availableBrands = allBrands.Where(brand => !selectedBrandIds.Contains(brand.Id));
			AvailableBrandsListBox.DataSource = availableBrands;
			AvailableBrandsListBox.DataBind();

			var selectedBrands = allBrands.Where(brand => selectedBrandIds.Contains(brand.Id));
			SelectedBrandsListBox.DataSource = selectedBrands;
			SelectedBrandsListBox.DataBind();
		}

		private void PopulateTags()
		{
			var tagGroups = IoC.Resolve<ITagGroupSecureService>().GetAll();
			TagRepeater.DataSource = tagGroups;
			TagRepeater.DataBind();
		}

		protected virtual void PopulateProductTypes(Collection<int> targetProductTypeIds)
		{
			TargetProductTypeSelectorControl.DataSource = new IdDictionary(targetProductTypeIds);
			TargetProductTypeSelectorControl.DataBind();
		}

		private void OnTagRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var tagGroup = (TagGroup)e.Item.DataItem;
			var tags = IoC.Resolve<ITagSecureService>().GetAllByTagGroup(tagGroup.Id);
			var taglist = (DropDownList)e.Item.FindControl("TagsList");
			taglist.DataSource = tags;
			taglist.DataBind();
			var emptyItem = new ListItem(string.Empty, EmptyItemValue);
			taglist.Items.Insert(0, emptyItem);

			var selectedTag = tags.SingleOrDefault(t => GetBlock().DefaultTagIds.Contains(t.Id));
			if (selectedTag == null)
			{
				emptyItem.Selected = true;
			}
			else
			{
				var selectedListItem = taglist.Items.FindByValue(selectedTag.Id.ToString(CultureInfo.InvariantCulture));
				if (selectedListItem != null)
				{
					selectedListItem.Selected = true;
				}
			}
		}

		protected void OnCategoryNodeCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(nodeId);
			CategoryNodeSelector.DataBind();
		}

		private void AddBrandsClick(object sender, EventArgs e)
		{
			Collection<int> brandsList = State.BrandIds;
			foreach (ListItem item in AvailableBrandsListBox.Items)
			{
				if (item.Selected)
				{
					var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					brandsList.Add(brandId);
				}
			}
			State.BrandIds = brandsList;
			PopulateBrands(brandsList);
		}

		private void RemoveBrandsClick(object sender, EventArgs e)
		{
			Collection<int> brandsList = State.BrandIds;
			foreach (ListItem item in SelectedBrandsListBox.Items)
			{
				if (item.Selected)
				{
					ListItem listItem = item;
					var brandId = int.Parse(listItem.Value, CultureInfo.CurrentCulture);
					brandsList.Remove(brandId);
				}
			}
			State.BrandIds = brandsList;
			PopulateBrands(brandsList);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockEsalesProductFilterSecureService = IoC.Resolve<IBlockEsalesProductFilterSecureService>();
			var blockEsalesProductFilter = blockEsalesProductFilterSecureService.GetByIdSecure(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockEsalesProductFilter.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (IsValidProductTypes() == false)
			{
				return;
			}
			
			int blockId = GetBlockId();

			var blockEsalesProductFilterSecureService = IoC.Resolve<IBlockEsalesProductFilterSecureService>();
			var blockProductFilter = blockEsalesProductFilterSecureService.GetByIdSecure(blockId);
			if (blockProductFilter == null)
			{
				throw new BusinessObjectNotExistsException(blockId);
			}

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(blockProductFilter);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			TargetDevice.SetTargetDevices(blockProductFilter);

			BlockEsalesSetting.SetSettings(blockProductFilter.BlockTypeId, blockProductFilter.EsalesSetting);

			blockProductFilter.DefaultSort = SortList.SelectedValue;

			blockProductFilter.Title = BlockTitleTextBox.Text;
			blockProductFilter.DefaultCategoryId = CategoryNodeSelector.SelectedNodeId;
			blockProductFilter.TemplateId = GetDropDownListValue(PrimaryTemplateList);
			blockProductFilter.SecondaryTemplateId = GetDropDownListValue(SecondaryTemplateList);
			blockProductFilter.DefaultAgeIntervalId = GetDropDownListValue(AgeIntervalList);
			blockProductFilter.DefaultPriceIntervalId = GetDropDownListValue(PriceIntervalExtendedList);
			blockProductFilter.DefaultBrandIds = State.BrandIds;
			blockProductFilter.DefaultTagIds = GetSelectedTags();
			blockProductFilter.DefaultSizeId = GetDropDownListValue(SizeList);
			blockProductFilter.BlockRating = GetRating();
			blockProductFilter.TargetProductTypeIds = GetProductTypes();

			blockProductFilter.Id = blockEsalesProductFilterSecureService.Save(SignInHelper.SignedInSystemUser, blockProductFilter);
			if (blockProductFilter.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				SystemMessageContainer.Add(Resources.LekmerMessage.SaveSuccessBlockProductFilter);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		private static int? GetDropDownListValue(DropDownList dropDownList)
		{
			var selectedValue = dropDownList.SelectedValue;
			if (selectedValue == EmptyItemValue)
			{
				return null;
			}

			int value;
			if (int.TryParse(dropDownList.SelectedValue, out value))
			{
				return value;
			}

			return null;
		}

		private Collection<int> GetSelectedTags()
		{
			var tags = new Collection<int>();
			foreach (RepeaterItem repeaterItem in TagRepeater.Items)
			{
				var tagsList = (DropDownList)repeaterItem.FindControl("TagsList");
				var selectedValue = tagsList.SelectedValue;
				if (selectedValue != EmptyItemValue)
				{
					tags.Add(int.Parse(selectedValue, CultureInfo.InvariantCulture));
				}
			}

			return tags;
		}

		protected virtual bool IsValidProductTypes()
		{
			bool isValid = true;

			Collection<string> messages;
			if (!TargetProductTypeSelectorControl.IsValid(out messages))
			{
				isValid = false;
				SystemMessageContainer.AddRange(messages, InfoType.Warning);
			}

			return isValid;
		}


		// Ratings grid.

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteRating":
					var ratingToDelate = State.RatingList.FirstOrDefault(r => r.Id == id);
					if (ratingToDelate != null)
					{
						State.RatingList.Remove(ratingToDelate);
						PopulateRatingGrid();
					}
					break;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGrid.PageIndex = e.NewPageIndex;

			PopulateRatingGrid();
		}

		protected virtual void OnRatingsAdd(object sender, UserControls.Assortment.Events.RatingSelectEventArgs e)
		{
			var rating = e.Ratings[0];

			if (State.RatingList.FirstOrDefault(r => r.Id == rating.Id) == null)
			{
				State.RatingList.Clear();
				State.RatingList.Add(rating);
				PopulateRatingGrid();
			}
		}

		protected virtual IRating PopulateRating()
		{
			if (State.RatingList == null)
			{
				State.RatingList = new List<IRating>();
			}

			var blockRatings = IoC.Resolve<IBlockRatingSecureService>().GetAllByBlock(GetBlockId());
			if (blockRatings != null && blockRatings.Count > 0)
			{
				IRating rating = IoC.Resolve<IRatingSecureService>().GetById(blockRatings[0].RatingId);
				State.RatingList.Add(rating);
			}

			PopulateRatingGrid();

			return State.RatingList.Count > 0 ? State.RatingList[0] : null;
		}

		protected virtual void PopulateRatingGrid()
		{
			RatingGrid.DataSource = State.RatingList;
			RatingGrid.DataBind();

			RatingUpdatePanel.Update();
		}

		protected virtual string GetRatingEditUrl(object ratingId)
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				int blockId = GetBlockId();

				return LekmerPathHelper.Rating.GetEditUrlForBlockProductFilterEdit(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture), id.Value, blockId);
			}

			return LekmerPathHelper.Rating.GetEditUrl(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture));
		}

		protected virtual string GetRatingPath(object folderId)
		{
			if (_allRatingFolders == null)
			{
				var service = IoC.Resolve<IRatingFolderSecureService>();
				_allRatingFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allRatingFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual IBlockRating GetRating()
		{
			IBlockRating blockRating = null;

			if (State.RatingList.Count > 0)
			{
				blockRating = IoC.Resolve<IBlockRating>();
				blockRating.BlockId = GetBlockId();
				blockRating.RatingId = State.RatingList[0].Id;
			}

			return blockRating;
		}

		protected virtual Collection<int> GetProductTypes()
		{
			IdDictionary selectedValues = TargetProductTypeSelectorControl.CollectSelectedValues();

			return new Collection<int>(selectedValues.Keys.ToList());
		}

		protected virtual void InitializeState()
		{
			if (State == null)
			{
				State = new ProductEsalesFilterState
				{
					RatingList = new List<IRating>(),
					BrandIds = new Collection<int>()
				};
			}
		}
	}

	[Serializable]
	public sealed class ProductEsalesFilterState
	{
		public List<IRating> RatingList { get; set; }
		public Collection<int> BrandIds { get; set; }
	}
}