﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockSharedWishListEdit : LekmerPageController, IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductBlockSharedWishList);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			var block = GetBlock();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockSharedWishList");
			TemplateList.DataBind();
			BlockTitleTextBox.Text = block.Title;
			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);
			ListItem item = TemplateList.Items.FindByValue(GetBlock().TemplateId.ToString());
			if (item != null)
				item.Selected = true;
			BlockSetting.Setting = block.Setting;

			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockSharedWishList blockProductRelationList = GetBlock();

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(
				blockProductRelationList.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockSharedWishListSecureService>();
			IBlockSharedWishList blockSharedWishList = blockService.GetById(GetBlockId());

			if (blockSharedWishList == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockSharedWishList.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockSharedWishList.Title = BlockTitleTextBox.Text;
			BlockSetting.SetSettings(blockSharedWishList.Setting);

			blockService.Save(SignInHelper.SignedInSystemUser, blockSharedWishList);
			if (blockSharedWishList.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockSharedWishList);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		private IBlockSharedWishList _block;
		private IBlockSharedWishList GetBlock()
		{
			if (_block == null)
			{
				_block = IoC.Resolve<IBlockSharedWishListSecureService>().GetById(GetBlockId());
			}
			return _block;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}