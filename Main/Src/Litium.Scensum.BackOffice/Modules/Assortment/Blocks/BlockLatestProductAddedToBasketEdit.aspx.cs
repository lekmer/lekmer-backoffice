﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
    public partial class BlockLatestProductAddedToBasketEdit : LekmerPageController, IEditor
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

            var master = Master as Pages;
            if (master != null)
            {
                master.Breadcrumbs.Clear();
                master.Breadcrumbs.Add(Resources.ProductMessage.BlockLatestProductAddedToBasket);
            }
        }

        protected override void SetEventHandlers()
        {
            SaveButton.Click += OnSave;
            CancelButton.Click += OnCancel;
        }

        protected override void PopulateForm()
        {
            var templateSecureService = IoC.Resolve<ITemplateSecureService>();
            var block = GetBlock();
            TemplateList.DataSource = templateSecureService.GetAllByModel("BlockLatestProductAddedToBasket");
            TemplateList.DataBind();
            BlockTitleTextBox.Text = block.Title;
            ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
            useThemeItem.Attributes.Add("class", "use-theme");
            TemplateList.Items.Insert(0, useThemeItem);
            ListItem item = TemplateList.Items.FindByValue(GetBlock().TemplateId.ToString());
            if (item != null)
                item.Selected = true;
            PopulateTranslation(GetBlockId());
            RedirectToStartPageRadioButton.Checked = block.RedirectToStartPage;
            RedirectToCategoryRadioButton.Checked = block.RedirectToCategory;
            RedirectToMainCategoryRadioButton.Checked = block.RedirectToMainCategory;
        }

        public virtual void OnCancel(object sender, EventArgs e)
        {
            IBlockLatestProductAddedToBasket blockLatestProductAddedToBasket = GetBlock();

            Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(
                blockLatestProductAddedToBasket.ContentNodeId));
        }

        public virtual void OnSave(object sender, EventArgs e)
        {
            var blockService = IoC.Resolve<IBlockLatestProductAddedToBasketSecureService>();
            IBlockLatestProductAddedToBasket block = blockService.GetById(GetBlockId());

            if (block == null)
            {
                throw new BusinessObjectNotExistsException(GetBlockId());
            }

            int templateId;
            block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
            block.Title = BlockTitleTextBox.Text;
            block.RedirectToStartPage = RedirectToStartPageRadioButton.Checked;
            block.RedirectToCategory = RedirectToCategoryRadioButton.Checked;
            block.RedirectToMainCategory = RedirectToMainCategoryRadioButton.Checked;
            blockService.Save(SignInHelper.SignedInSystemUser, block);
            if (block.Id == -1)
            {
                SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
            }
            else
            {
                var translations = Translator.GetTranslations();
                IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

                SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockLatestProductAddedToBasket);
                SystemMessageContainer.MessageType = InfoType.Success;
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual int GetBlockId()
        {
            return Request.QueryString.GetInt32("BlockId");
        }

        private IBlockLatestProductAddedToBasket _block;
        private IBlockLatestProductAddedToBasket GetBlock()
        {
            if (_block == null)
            {
                _block = IoC.Resolve<IBlockLatestProductAddedToBasketSecureService>().GetById(GetBlockId());
            }
            return _block;
        }

        protected virtual void PopulateTranslation(int blockId)
        {
            Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
            Translator.BusinessObjectId = blockId;
            Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
            Translator.DataBind();
        }
    }

}