﻿<%@ Page Language="C#" 
	AutoEventWireup="true" 
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" 
	CodeBehind="BlockEsalesRecommendEdit.aspx.cs" 
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockEsalesRecommendEdit" 
%>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgBlockEsalesRecommend" />
</asp:Content>

<asp:Content ID="cBPRL" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<script type="text/javascript">
		$(function () {
			var select = $("select[id*='RecommendationTypeList']");
			AdjustTemplateList(select);
			select.change(function () {
				AdjustTemplateList($(this));
			});
		})
	</script>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box esales-recommend">
			<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_Title %>"/></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator 
				runat="server" 
				ID="BlockTitleValidator"
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"
				Display ="None" ValidationGroup="vgBlockEsalesRecommend" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br /><br />
			<div class="input-box full-width">
				<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_RecommendationType %>" /></span>
				<br />
				<asp:DropDownList ID="RecommendationTypeList" runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" />
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="RecommendationTypeListValidator"
					ControlToValidate="RecommendationTypeList" 
					InitialValue=""
					ErrorMessage="<%$ Resources:Lekmer,Literal_RecommendationTypeEmpty %>"
					Display ="None" ValidationGroup="vgBlockEsalesRecommend" />
			</div>
			<br />
			<div class="input-box full-width">
				<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_EsalesPanelPath %>" /></span>
				<br />
				<asp:TextBox ID="EsalesPanelPathTextBox" runat="server" />
			</div>
			<div class="input-box full-width">
				<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_EsalesFallbackPanelPath %>" /></span>
				<br />
				<asp:TextBox ID="EsalesFallbackPanelPathTextBox" runat="server" />
			</div>
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
							<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate %>" /></span>
							<br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span>Can be used as fallback option</span>
							<br />
							<asp:CheckBox ID="CanBeUsedAsFallbackOptionCheckBox" runat="server" />
						</div>
					</div>
					<br class="clear" />
					<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgBlockEsalesRecommend"/>
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgBlockEsalesRecommend" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>
