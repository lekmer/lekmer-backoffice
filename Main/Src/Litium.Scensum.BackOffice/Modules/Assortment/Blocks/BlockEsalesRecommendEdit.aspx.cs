﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockEsalesRecommendEdit : LekmerPageController, IEditor
	{
		private IBlockEsalesRecommend _block;
		private IBlockEsalesRecommendSecureService _blockEsalesRecommendSecureService;
		private IEsalesRecommendationTypeSecureService _esalesRecommendationTypeSecureService;

		private IBlockEsalesRecommend Block
		{
			get { return _block ?? (_block = BlockService.GetById(GetBlockId())); }
		}

		private IBlockEsalesRecommendSecureService BlockService
		{
			get { return _blockEsalesRecommendSecureService ?? (_blockEsalesRecommendSecureService = IoC.Resolve<IBlockEsalesRecommendSecureService>()); }
		}

		private IEsalesRecommendationTypeSecureService EsalesRecommendationTypeService
		{
			get
			{
				return _esalesRecommendationTypeSecureService
				?? (_esalesRecommendationTypeSecureService = IoC.Resolve<IEsalesRecommendationTypeSecureService>());
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockEsalesRecommend);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var block = Block;
			BlockTitleTextBox.Text = block.Title;
			BlockSetting.Setting = block.Setting;
			EsalesPanelPathTextBox.Text = block.PanelPath;
			EsalesFallbackPanelPathTextBox.Text = block.FallbackPanelPath;
			CanBeUsedAsFallbackOptionCheckBox.Checked = block.CanBeUsedAsFallbackOption ?? false;

			PopulateTemplateList();
			PopulateRecommendationTypeList();
			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockEsalesRecommend blockEsalesRecommend = BlockService.GetById(GetBlockId());

			if (blockEsalesRecommend == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockEsalesRecommend.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockEsalesRecommend.Title = BlockTitleTextBox.Text;
			BlockSetting.SetSettings(blockEsalesRecommend.Setting);
			int recommendationTypeId;
			blockEsalesRecommend.RecommendationType = int.TryParse(RecommendationTypeList.SelectedValue, out recommendationTypeId) ? (int?)recommendationTypeId : null;
			blockEsalesRecommend.PanelPath = EsalesPanelPathTextBox.Text;
			blockEsalesRecommend.FallbackPanelPath = EsalesFallbackPanelPathTextBox.Text;
			blockEsalesRecommend.CanBeUsedAsFallbackOption = CanBeUsedAsFallbackOptionCheckBox.Checked;

			BlockService.Save(SignInHelper.SignedInSystemUser, blockEsalesRecommend);
			if (blockEsalesRecommend.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductAvailCombine);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTemplateList()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockEsalesRecommend");
			TemplateList.DataBind();

			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);

			if (Block.TemplateId.HasValue)
			{
				ListItem item = TemplateList.Items.FindByValue(Block.TemplateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}

		protected virtual void PopulateRecommendationTypeList()
		{
			RecommendationTypeList.DataSource = EsalesRecommendationTypeService.GetAll();
			RecommendationTypeList.DataBind();

			ListItem useThemeItem = new ListItem(Resources.Lekmer.Literal_SelectRecommendationType, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			RecommendationTypeList.Items.Insert(0, useThemeItem);

			if (Block.RecommendationType.HasValue)
			{
				ListItem item = RecommendationTypeList.Items.FindByValue(Block.RecommendationType.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}