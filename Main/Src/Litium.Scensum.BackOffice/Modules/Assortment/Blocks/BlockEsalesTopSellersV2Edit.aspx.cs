﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockEsalesTopSellersV2Edit : LekmerStatePageController<EsalesTopSellersV2State>, IEditor
	{
		private readonly Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private int? _id;
		protected int? Id
		{
			get { return _id ?? (_id = GetIdOrNull()); }
		}

		private int _blockId;
		protected int BlockId
		{
			get { return _blockId > 0 ? _blockId : _blockId = Request.QueryString.GetInt32("BlockId"); }
		}

		private IBlockEsalesTopSellersV2 _block;
		private IBlockEsalesTopSellersV2 Block
		{
			get { return _block ?? (_block = BlockService.GetById(BlockId)); }
		}

		private Collection<ILanguage> _languages;
		public Collection<ILanguage> Languages
		{
			get { return _languages ?? (_languages = LanguageService.GetAll()); }
		}

		private ILanguageSecureService _languageService;
		protected ILanguageSecureService LanguageService
		{
			get { return _languageService ?? (_languageService = IoC.Resolve<ILanguageSecureService>()); }
		}

		private ICategorySecureService _categorySecureService;
		protected ICategorySecureService CategorySecureService
		{
			get { return _categorySecureService ?? (_categorySecureService = IoC.Resolve<ICategorySecureService>()); }
		}

		private IBlockEsalesTopSellersSecureServiceV2 _blockService;
		protected IBlockEsalesTopSellersSecureServiceV2 BlockService
		{
			get { return _blockService ?? (_blockService = IoC.Resolve<IBlockEsalesTopSellersSecureServiceV2>()); }
		}

		private IBlockEsalesTopSellersCategorySecureServiceV2 _blockCategoryService;
		protected IBlockEsalesTopSellersCategorySecureServiceV2 BlockCategoryService
		{
			get { return _blockCategoryService ?? (_blockCategoryService = IoC.Resolve<IBlockEsalesTopSellersCategorySecureServiceV2>()); }
		}

		private IBlockEsalesTopSellersProductSecureServiceV2 _blockProductService;
		protected IBlockEsalesTopSellersProductSecureServiceV2 BlockProductService
		{
			get { return _blockProductService ?? (_blockProductService = IoC.Resolve<IBlockEsalesTopSellersProductSecureServiceV2>()); }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			CategoriesRadio.CheckedChanged += OnCategoriesUseChanged;
			WholeRangeRadio.CheckedChanged += OnCategoriesUseChanged;
			CategoriesGrid.RowDataBound += OnCategoriesRowDataBound;
			CategoriesGrid.RowCommand += OnCategoriesRowCommand;
			CategoriesGrid.PageIndexChanging += OnCategoriesPageIndexChanging;
			CategoriesOkPopupButton.Click += OnCategoriesAdd;
			CategoriesRemoveSelectedButton.Click += OnCategoriesRemoveSelected;

			ProductsGrid.RowDataBound += OnProductsRowDataBound;
			ProductsGrid.RowCommand += OnProductsRowCommand;
			ProductsGrid.PageIndexChanging += OnProductsPageIndexChanging;
			ProductsSearch.SearchEvent += OnProductsAdd;
			ProductsRemoveSelectedButton.Click += OnProductsRemoveSelected;

			LinkNodeSelector.NodeCommand += OnLinkNodeSelectorCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start) (Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.ProductMessage.BlockEsalesTopSellersV2);
		}

		protected override void PopulateForm()
		{
			InitializeState();

			PopulateData();
			PopulateCategoriesTree();
			PopulateCategoriesGrid();
			PopulateProductsGrid();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var block = BlockService.GetById(BlockId);
			if (block == null)
			{
				throw new BusinessObjectNotExistsException(BlockId);
			}

			UpdateForcePositions();
			if (!ValidateForcePositions())
			{
				SystemMessageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.TopList.Message_ForcePositionInvalid, int.MaxValue), InfoType.Warning);
				return;
			}

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(block);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			TargetDevice.SetTargetDevices(block);

			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			block.Title = BlockTitleTextBox.Text;
			block.PanelName = EsalesSubPanelNameTextBox.Text;
			block.WindowLastEsalesValue = int.Parse(WindowLastEsalesValueTextBox.Text, CultureInfo.CurrentCulture);
			block.LinkContentNodeId = !NavLink.Checked || LinkNodeSelector.SelectedNodeId <= 0 ? null : LinkNodeSelector.SelectedNodeId;
			var customUrl = CustomUrlTexBox.Text.Trim();
			block.CustomUrl = !CustomUrl.Checked || string.IsNullOrEmpty(customUrl) ? null : customUrl;
			block.UrlTitle = UrlTitleTextBox.Text;
			block.IncludeAllCategories = WholeRangeRadio.Checked;
			BlockSetting.SetSettings(block.Setting);

			if (block.IncludeAllCategories)
			{
				State.Categories.Clear();
			}

			BlockService.Save(SignInHelper.SignedInSystemUser, block, State.Categories, State.Products, UrlTitleTranslator.GetTranslations());
			if (block.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, Translator.GetTranslations());
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlock, InfoType.Success);

				if (block.LinkContentNodeId != null && block.LinkContentNodeId > 0)
				{
					NavLink.Checked = true;
				}
				else
				{
					NavLink.Checked = false;
					LinkNodeSelector.ClearFolderTextBox();
				}

				CustomUrlTexBox.Text = block.CustomUrl;
				CustomUrl.Checked = !string.IsNullOrEmpty(block.CustomUrl);
			}
		}

		protected void OnLinkNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
		}


		protected virtual void InitializeState()
		{
			if (Id.HasValue)
			{
				var blockCategories = BlockCategoryService.GetAllByBlock(BlockId);
				ExtendCategoryTitles(blockCategories);

				var blockProducts = BlockProductService.GetAllByBlock(BlockId, ChannelHelper.CurrentChannel.Id);
				ExtendProductCategoryTitles(blockProducts);

				State = new EsalesTopSellersV2State(blockCategories, blockProducts);
			}
			else
			{
				State = new EsalesTopSellersV2State();
			}
		}

		protected void PopulateData()
		{
			var block = Block;
			BlockTitleTextBox.Text = block.Title;
			BlockSetting.Setting = block.Setting;
			EsalesSubPanelNameTextBox.Text = block.PanelName;
			WindowLastEsalesValueTextBox.Text = block.WindowLastEsalesValue.ToString(CultureInfo.CurrentCulture);
			UrlTitleTextBox.Text = block.UrlTitle;

			PopulateLinkNodeSelector(block.LinkContentNodeId);
			NavLink.Checked = block.LinkContentNodeId != null && block.LinkContentNodeId > 0;

			CustomUrlTexBox.Text = block.CustomUrl;
			CustomUrl.Checked = !string.IsNullOrEmpty(block.CustomUrl);

			TimeLimiter.Block = block;
			TargetDevice.Block = block;

			PopulateTemplateList(block.TemplateId);
			PopulateTranslation(block.Id);

			if (block.IncludeAllCategories)
			{
				WholeRangeRadio.Checked = true;
			}
			else
			{
				CategoriesRadio.Checked = true;
			}
		}

		protected virtual void PopulateTemplateList(int? templateId)
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockEsalesTopSellersV2");
			TemplateList.DataBind();

			var useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);

			if (templateId.HasValue)
			{
				var item = TemplateList.Items.FindByValue(templateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}
		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(BlockId);
			Translator.DataBind();

			UrlTitleTranslator.DefaultValueControlClientId = UrlTitleTextBox.ClientID;
			UrlTitleTranslator.BusinessObjectId = blockId;
			UrlTitleTranslator.DataSource = BlockService.GetAllUrlTitleTranslationsByBlock(BlockId);
			UrlTitleTranslator.DataBind();
		}
		protected virtual void PopulateLinkNodeSelector(int? nodeId)
		{
			LinkNodeSelector.SelectedNodeId = nodeId;
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
			LinkNodeSelector.PopulatePath(nodeId);
		}

		#region CATEGORIES

		protected virtual void ExtendCategoryTitles(Collection<IBlockEsalesTopSellersCategoryV2> blockCategories)
		{
			foreach (var blockCategory in blockCategories)
			{
				blockCategory.Category.Title = GetCategoryPath(blockCategory.Category);
			}
		}
		
		protected virtual string GetCategoryPath(ICategory category)
		{
			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = GetCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}
		
		protected virtual ICategory GetCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = CategorySecureService.GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}
		
		protected virtual void PopulateCategoriesTree()
		{
			CategoriesTree.Selector = CategorySelector;
			CategoriesTree.DataBind();
		}
		
		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		protected virtual void PopulateCategoriesGrid()
		{
			if (WholeRangeRadio.Checked)
			{
				CategoriesDiv.Visible = false;
				return;
			}

			CategoriesDiv.Visible = true;
			CategoriesGrid.DataSource = State.Categories;
			CategoriesGrid.DataBind();
			CategoriesGrid.Style.Add("display", CategoriesGrid.Rows.Count > 0 ? "table" : "none");
		}

		protected virtual void OnCategoriesUseChanged(object sender, EventArgs e)
		{
			PopulateCategoriesGrid();
		}

		protected virtual void OnCategoriesRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
														 "javascript:SelectAllBelow('" + selectAllCheckBox.ClientID + @"','" +
														 CategoriesGrid.ClientID +
														 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
														 CategoriesGrid.ClientID + "', '" + CategoriesAllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox)CategoriesGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
												"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID +
												@"'); ShowBulkUpdatePanel('" +
												cbSelect.ID + "', '" + CategoriesGrid.ClientID + "', '" +
												CategoriesAllSelectedDiv.ClientID + @"');");
					}
					break;
			}
		}

		protected virtual void OnCategoriesRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteCategory") return;

			var categoryId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
			var category = State.Categories.FirstOrDefault(c => c.Category.Id == categoryId);
			State.Categories.Remove(category);

			PopulateCategoriesGrid();
		}

		protected virtual void OnCategoriesPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CategoriesGrid.PageIndex = e.NewPageIndex;
			PopulateCategoriesGrid();
		}

		protected virtual void OnCategoriesAdd(object sender, EventArgs e)
		{
			var categoryIds = CategoriesTree.SelectedIds;
			if (categoryIds.Count == 0) return;

			foreach (var categoryId in categoryIds)
			{
				if (State.Categories.FirstOrDefault(c => c.Category.Id == categoryId) != null) continue;

				var blockCategory = IoC.Resolve<IBlockEsalesTopSellersCategoryV2>();
				blockCategory.BlockId = BlockId;
				blockCategory.Category = CategorySecureService.GetById(categoryId);
				blockCategory.Category.Title = GetCategoryPath(blockCategory.Category);
				State.Categories.Add(blockCategory);
			}

			PopulateCategoriesGrid();
			CategoriesTree.SelectedIds.Clear();
		}

		protected virtual void OnCategoriesRemoveSelected(object sender, EventArgs e)
		{
			var categoryIds = CategoriesGrid.GetSelectedIds();
			if (categoryIds.Count() == 0) return;

			foreach (var categoryId in categoryIds)
			{
				var category = State.Categories.FirstOrDefault(c => c.Category.Id == categoryId);
				State.Categories.Remove(category);
			}

			PopulateCategoriesGrid();
		}

		protected virtual void OnIncludeSubcategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox)sender;
			var row = (GridViewRow)checkBox.Parent.Parent;
			var idHidden = (HiddenField)row.FindControl("IdHiddenField");
			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);
			var category = State.Categories.First(c => c.Category.Id == categoryId);
			category.IncludeSubcategories = checkBox.Checked;

			PopulateCategoriesGrid();
		}


		#endregion

		#region PRODUCTS

		protected virtual void ExtendProductCategoryTitles(Collection<IBlockEsalesTopSellersProductV2> blockProducts)
		{
			foreach (var blockProduct in blockProducts)
			{
				blockProduct.Product.ShortDescription = GetCategoryPath(blockProduct.Product.CategoryId);
			}
		}
		
		protected virtual string GetCategoryPath(int categoryId)
		{
			var category = GetCategory(categoryId);
			return GetCategoryPath(category);
		}

		protected virtual void PopulateProductsGrid()
		{
			ProductsGrid.DataSource = State.Products;
			ProductsGrid.DataBind();
			ProductsGrid.Style.Add("display", ProductsGrid.Rows.Count > 0 ? "table" : "none");
		}


		protected virtual void OnProductsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
														 "javascript:SelectAllBelow('" + selectAllCheckBox.ClientID + @"','" +
														 ProductsGrid.ClientID +
														 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
														 ProductsGrid.ClientID + "', '" + ProductsAllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox)ProductsGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
												"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID +
												@"'); ShowBulkUpdatePanel('" +
												cbSelect.ID + "', '" + ProductsGrid.ClientID + "', '" + ProductsAllSelectedDiv.ClientID +
												@"');");
					}
					break;
			}
		}

		protected virtual void OnProductsRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteProduct") return;

			var productId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
			var product = State.Products.FirstOrDefault(p => p.Product.Id == productId);
			State.Products.Remove(product);

			PopulateProductsGrid();
		}

		protected virtual void OnProductsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductsGrid.PageIndex = e.NewPageIndex;
			PopulateProductsGrid();
		}

		protected virtual void OnProductsAdd(object sender, ProductSearchEventArgs e)
		{
			foreach (var product in e.Products)
			{
				var productId = product.Id;
				if (State.Products.Any(p => p.Product.Id == productId)) continue;

				var blockProduct = IoC.Resolve<IBlockEsalesTopSellersProductV2>();
				blockProduct.BlockId = GetIdOrNull() ?? -1;
				blockProduct.Product = product;
				blockProduct.Product.ShortDescription = GetCategoryPath(product.CategoryId);
				State.Products.Add(blockProduct);
			}

			PopulateProductsGrid();
			ProductsUpdatePanel.Update();
		}

		protected virtual void OnProductsRemoveSelected(object sender, EventArgs e)
		{
			var ids = ProductsGrid.GetSelectedIds();
			if (ids.Count() == 0) return;

			foreach (var id in ids)
			{
				var product = State.Products.FirstOrDefault(p => p.Product.Id == id);
				State.Products.Remove(product);
			}

			PopulateProductsGrid();
		}

		protected virtual void UpdateForcePositions()
		{
			foreach (GridViewRow row in ProductsGrid.Rows)
			{
				var idHidden = row.FindControl("IdHiddenField") as HiddenField;
				if (idHidden == null) continue;

				int id;
				if (!int.TryParse(idHidden.Value, out id)) continue;

				var product = State.Products.FirstOrDefault(p => p.Product.Id == id);
				if (product == null) continue;

				var positionBox = row.FindControl("PositionTextBox") as TextBox;
				if (positionBox == null) continue;

				int position;
				product.Position = int.TryParse(positionBox.Text, out position) ? position : 0;
			}
		}

		protected virtual bool ValidateForcePositions()
		{
			if (State.Products.Any(p => p.Position < 1)) return false;

			foreach (var product in State.Products)
			{
				var clone = product;
				if (State.Products.Any(p => p.Position == clone.Position && p.Product.Id != clone.Product.Id))
				{
					return false;
				}
			}
			return true;
		}

		#endregion
	}

	[Serializable]
	public sealed class EsalesTopSellersV2State
	{
		public EsalesTopSellersV2State()
		{
			Categories = new Collection<IBlockEsalesTopSellersCategoryV2>();
			Products = new Collection<IBlockEsalesTopSellersProductV2>();
		}

		public EsalesTopSellersV2State(Collection<IBlockEsalesTopSellersCategoryV2> categories, Collection<IBlockEsalesTopSellersProductV2> products)
		{
			Categories = categories;
			Products = products;
		}

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockEsalesTopSellersCategoryV2> Categories { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockEsalesTopSellersProductV2> Products { get; set; }
	}
}