﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockImageRotatorEdit : LekmerPageController, IEditor
	{
		private const string EmptyItemValue = "0";

		private IBlockImageRotator _block;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockImageRotator);
			}
		}

		protected override void SetEventHandlers()
		{
			ImageRotatorControl.Selected += ImageSelected;
			ImageRotatorControl.FileUploaded += ImageSelectUnload;
			ImageRotatorControl.DeleteImage += ProductMediaDeleteImage;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		private void ImageSelectUnload(object sender, EventArgs e)
		{
		}

		protected virtual void ProductMediaDeleteImage(object sender, EventArgs e)
		{
		}

		private void ImageSelected(object sender, ImageSelectEventArgs e)
		{
		}

		protected virtual void BtnCancelSaveImageClick(object sender, EventArgs e)
		{
		   
		}

		protected override void PopulateForm()
		{
			var block = GetBlock();
			ImageRotatorControl.ImageRotatorId = block.Id;
			
			var templates = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockImageRotator");
			TemplateList.DataSource = templates;
			TemplateList.DataBind();
			BlockTitleTextBox.Text = block.Title;
			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);
			ListItem item = TemplateList.Items.FindByValue(GetBlock().TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}

			PopulateDropDownList(SecondaryTemplateList, templates, block.SecondaryTemplateId);
			PopulateTranslation(GetBlockId());

			TimeLimiter.Block = block;

			TargetDevice.Block = block;
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockImageRotator blockProductRelationList = GetBlock();

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(
				blockProductRelationList.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockImageRotatorSecureService>();
			IBlockImageRotator blockImageRotator = blockService.GetById(GetBlockId());

			if (blockImageRotator == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(blockImageRotator);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			TargetDevice.SetTargetDevices(blockImageRotator);

			int templateId;
			blockImageRotator.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockImageRotator.Title = BlockTitleTextBox.Text;
			blockImageRotator.SecondaryTemplateId = GetDropDownListValue(SecondaryTemplateList);

			var imageRotatorErrors = ImageRotatorControl.Validate();
			if (imageRotatorErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(imageRotatorErrors, InfoType.Warning);
				return;
			}
			blockService.Save(SignInHelper.SignedInSystemUser, blockImageRotator, ImageRotatorControl.ProductImageGroups);
			if (blockImageRotator.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockImageRotator);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		private static int? GetDropDownListValue(DropDownList dropDownList)
		{
			var selectedValue = dropDownList.SelectedValue;
			if (selectedValue == EmptyItemValue)
			{
				return null;
			}
			int value;
			if (int.TryParse(dropDownList.SelectedValue, out value))
			{
				return value;
			}
			return null;
		}

		private static void PopulateDropDownList(DropDownList dropDownList, object list, int? selectedId)
		{
			dropDownList.DataSource = list;
			dropDownList.DataBind();
			var emptyItem = new ListItem(string.Empty, EmptyItemValue);
			dropDownList.Items.Insert(0, emptyItem);
			if (!selectedId.HasValue)
			{
				emptyItem.Selected = true;
			}
			else
			{
				var selectedItem = dropDownList.Items.FindByValue(selectedId.ToString());
				if (selectedItem != null)
				{
					selectedItem.Selected = true;
				}
			}
		}

		private IBlockImageRotator GetBlock()
		{
			if (_block == null)
			{
				_block = IoC.Resolve<IBlockImageRotatorSecureService>().GetById(GetBlockId());
			}
			return _block;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}