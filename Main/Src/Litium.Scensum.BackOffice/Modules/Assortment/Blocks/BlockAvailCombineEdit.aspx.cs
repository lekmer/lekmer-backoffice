﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI.WebControls;
using Litium.Lekmer.Avail;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockAvailCombineEdit : LekmerPageController, IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductAvailCombine);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			RelationsGrid.RowDataBound += RelationsGridRowDataBound;
		}

		protected virtual void RelationsGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var cbAll = (CheckBox)row.FindControl("AllRelationsSelectCheckBox");
				cbAll.Checked = false;
				cbAll.Attributes.Add("onclick", "SelectAll1('" + cbAll.ClientID + @"','" + RelationsGrid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var blockAvailCombineWrapper = (BlockAvailCombineWrapper)row.DataItem;
				var cbSelect = (CheckBox)row.FindControl("RelationSelectCheckBox");
				var availType = (Label)row.FindControl("AvailType");
				cbSelect.Checked = blockAvailCombineWrapper.Use;
				availType.Text = blockAvailCombineWrapper.AvailType.ToString();
			}
		}

		protected override void PopulateForm()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			var block = GetBlock();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockAvailCombine");
			TemplateList.DataBind();
			BlockTitleTextBox.Text = block.Title;
			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);
			ListItem item = TemplateList.Items.FindByValue(GetBlock().TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			BlockSetting.Setting = block.Setting;
			GetClickHistoryFromCookieCheckBox.Checked = block.GetClickHistoryFromCookie;
			RelationsGrid.DataSource = GetBlockAvailCombineTypes();
			RelationsGrid.DataBind();
			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockAvailCombine blockProductRelationList = GetBlock();

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(
				blockProductRelationList.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockAvailCombineSecureService>();
			IBlockAvailCombine blockAvailCombine = blockService.GetById(GetBlockId());

			if (blockAvailCombine == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockAvailCombine.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockAvailCombine.Title = BlockTitleTextBox.Text;
			blockAvailCombine.GetClickHistoryFromCookie = GetClickHistoryFromCookieCheckBox.Checked;
			BlockSetting.SetSettings(blockAvailCombine.Setting);
			SetAvailTypes(blockAvailCombine);

			blockService.Save(SignInHelper.SignedInSystemUser, blockAvailCombine);
			if (blockAvailCombine.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductAvailCombine);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		private IBlockAvailCombine _block;
		private IBlockAvailCombine GetBlock()
		{
			if (_block == null)
			{
				_block = IoC.Resolve<IBlockAvailCombineSecureService>().GetById(GetBlockId());
			}
			return _block;
		}

		private void SetAvailTypes(IBlockAvailCombine blockAvailCombine)
		{
			List<BlockAvailCombineWrapper> blockAvailCombineWrapperList = GetRelationListItemsFromGrid();
			foreach (BlockAvailCombineWrapper availType in blockAvailCombineWrapperList)
			{
				switch (availType.AvailType)
				{
					case AvailType.CartPredictions:
						blockAvailCombine.UseCartPredictions = availType.Use;
						break;
					case AvailType.ClickStreamPredictions:
						blockAvailCombine.UseClickStreamPredictions = availType.Use;
						break;
					case AvailType.LogPurchase:
						blockAvailCombine.UseLogPurchase = availType.Use;
						break;
					case AvailType.PersonalPredictions:
						blockAvailCombine.UsePersonalPredictions = availType.Use;
						break;
					case AvailType.ProductSearchPredictions:
						blockAvailCombine.UseProductSearchPredictions = availType.Use;
						break;
					case AvailType.ProductsPredictions:
						blockAvailCombine.UseProductsPredictions = availType.Use;
						break;
					case AvailType.ProductsPredictionsFromClicksCategory:
						blockAvailCombine.UseProductsPredictionsFromClicksCategory = availType.Use;
						break;
					case AvailType.ProductsPredictionsFromClicksProduct:
						blockAvailCombine.UseProductsPredictionsFromClicksProduct = availType.Use;
						break;
					case AvailType.LogClickedOn:
						blockAvailCombine.UseLogClickedOn = availType.Use;
						break;
				}
			}
		}

		private List<BlockAvailCombineWrapper> GetBlockAvailCombineTypes()
		{
			List<BlockAvailCombineWrapper> availCombineWrapperList = new List<BlockAvailCombineWrapper>();
			IBlockAvailCombine blockAvailCombine = GetBlock();
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseCartPredictions, AvailType.CartPredictions));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseClickStreamPredictions, AvailType.ClickStreamPredictions));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseLogPurchase, AvailType.LogPurchase));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UsePersonalPredictions, AvailType.PersonalPredictions));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseProductSearchPredictions, AvailType.ProductSearchPredictions));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseProductsPredictions, AvailType.ProductsPredictions));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseProductsPredictionsFromClicksCategory, AvailType.ProductsPredictionsFromClicksCategory));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseProductsPredictionsFromClicksProduct, AvailType.ProductsPredictionsFromClicksProduct));
			availCombineWrapperList.Add(new BlockAvailCombineWrapper(blockAvailCombine.UseLogClickedOn, AvailType.LogClickedOn));
			return availCombineWrapperList;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		private List<BlockAvailCombineWrapper> GetRelationListItemsFromGrid()
		{
			var items = new List<BlockAvailCombineWrapper>();
			foreach (GridViewRow row in RelationsGrid.Rows)
			{
				BlockAvailCombineWrapper item = new BlockAvailCombineWrapper();
				var cbSelect = (CheckBox)row.FindControl("RelationSelectCheckBox");
				item.Use = cbSelect.Checked;
				var availTypeLabel = (Label)row.FindControl("AvailType");
				AvailType availType = (AvailType)Enum.Parse(typeof(AvailType), availTypeLabel.Text);
				item.AvailType = availType;
				items.Add(item);
			}
			return items;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}

	class BlockAvailCombineWrapper
	{
		public BlockAvailCombineWrapper()
		{
		}
		public BlockAvailCombineWrapper(bool use, AvailType availType)
		{
			Use = use;
			AvailType = availType;
		}
		public bool Use;
		public AvailType AvailType;
	}
	enum AvailType
	{
		CartPredictions,
		ClickStreamPredictions,
		LogPurchase,
		PersonalPredictions,
		ProductSearchPredictions,
		ProductsPredictions,
		ProductsPredictionsFromClicksCategory,
		ProductsPredictionsFromClicksProduct,
		LogClickedOn
	}
}