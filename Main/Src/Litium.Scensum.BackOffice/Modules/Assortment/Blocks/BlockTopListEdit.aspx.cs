﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Template;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Litium.Scensum.TopList;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockTopListEdit : LekmerStatePageController<TopListState>, IEditor
	{
		private readonly Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private Collection<ILanguage> _languages;
		public Collection<ILanguage> Languages
		{
			get { return _languages ?? (_languages = LanguageService.GetAll()); }
		}

		private ILanguageSecureService _languageService;
		protected ILanguageSecureService LanguageService
		{
			get { return _languageService ?? (_languageService = IoC.Resolve<ILanguageSecureService>()); }
		}

		private IAliasSecureService _aliasService;
		protected IAliasSecureService AliasService
		{
			get { return _aliasService ?? (_aliasService = IoC.Resolve<IAliasSecureService>()); }
		}

		private IAliasTranslationSecureService _aliasTranslationService;
		protected IAliasTranslationSecureService AliasTranslationService
		{
			get { return _aliasTranslationService ?? (_aliasTranslationService = IoC.Resolve<IAliasTranslationSecureService>()); }
		}

		private IBlockTopListSecureService _blockService;
		protected IBlockTopListSecureService BlockService
		{
			get { return _blockService ?? (_blockService = IoC.Resolve<IBlockTopListSecureService>()); }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			CategoriesRadio.CheckedChanged += OnCategoriesUseChanged;
			WholeRangeRadio.CheckedChanged += OnCategoriesUseChanged;
			CategoriesGrid.RowDataBound += OnCategoriesRowDataBound;
			CategoriesGrid.RowCommand += OnCategoriesRowCommand;
			CategoriesGrid.PageIndexChanging += OnCategoriesPageIndexChanging;
			CategoriesOkPopupButton.Click += OnCategoriesAdd;
			CategoriesRemoveSelectedButton.Click += OnCategoriesRemoveSelected;

			ProductsGrid.RowDataBound += OnProductsRowDataBound;
			ProductsGrid.RowCommand += OnProductsRowCommand;
			ProductsGrid.PageIndexChanging += OnProductsPageIndexChanging;
			ProductsSearch.SearchEvent += OnProductsAdd;
			ProductsRemoveSelectedButton.Click += OnProductsRemoveSelected;

			LinkNodeSelector.NodeCommand += OnLinkNodeSelectorCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			((Master.Start) (Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnLoad(e);

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.TopList.Literal_EditTopListBlock);
		}

		protected override void PopulateForm()
		{
			InitializeState();
			PopulateData();
			PopulateCategoriesTree();
			PopulateCategoriesGrid();
			PopulateProductsGrid();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockProductReview = BlockService.GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductReview.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var block = BlockService.GetById(GetBlockId());
			if (block == null) throw new BusinessObjectNotExistsException(GetBlockId());

			UpdateForcePositions();
			if (!ValidateForcePositions())
			{
				SystemMessageContainer.Add(string.Format(CultureInfo.CurrentCulture, 
					Resources.TopList.Message_ForcePositionInvalid, int.MaxValue), InfoType.Warning);
				return;
			}

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(block);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			TargetDevice.SetTargetDevices(block);

			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?) templateId : null;
			block.Title = BlockTitleTextBox.Text;
			block.LinkContentNodeId = !NavLink.Checked || LinkNodeSelector.SelectedNodeId <= 0 ? null : LinkNodeSelector.SelectedNodeId;
			var customUrl = CustomUrlTexBox.Text.Trim();
			block.CustomUrl = !CustomUrl.Checked || string.IsNullOrEmpty(customUrl) ? null : customUrl;
			block.IncludeAllCategories = WholeRangeRadio.Checked;
			block.OrderStatisticsDayCount = int.Parse(OrderStatisticsDayCountTextBox.Text, CultureInfo.CurrentCulture);

			BlockSetting.SetSettings(block.Setting);

			if (block.IncludeAllCategories)
			{
				State.Categories.Clear();
			}
			BlockService.Save(SignInHelper.SignedInSystemUser, ChannelHelper.CurrentChannel, block, State.Categories, State.Products);
			if (block.Id == -1)
			{
				SystemMessageContainer.Add(GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SaveAlias(block);
				SystemMessageContainer.Add(Resources.TopList.Message_BlockTopListSavedSuccessfully, InfoType.Success);

				if (block.LinkContentNodeId != null && block.LinkContentNodeId > 0)
				{
					NavLink.Checked = true;
				}
				else
				{
					NavLink.Checked = false;
					LinkNodeSelector.ClearFolderTextBox();
				}

				CustomUrlTexBox.Text = block.CustomUrl;
				CustomUrl.Checked = !string.IsNullOrEmpty(block.CustomUrl);
			}
		}
		protected virtual void SaveAlias(IBlockTopList block)
		{
			var alias = ((ILekmerAliasSecureService)AliasService).GetByCommonName(string.Format(block.AliasDefaultCommonName, block.Id));
			if (alias == null)
			{
				alias = AliasService.Create();
				alias.AliasFolderId = BackOfficeSetting.Instance.BlockAliasFolderId;
				alias.CommonName = string.Format(block.AliasDefaultCommonName, block.Id);
				alias.AliasTypeId = BackOfficeSetting.Instance.BlockAliasTypeId;
			}
			alias.Value = UrlTitleTextBox.Text ?? string.Empty;

			var translations = new Collection<IAliasTranslation>();
			foreach (var aliasTranslation in UrlTitleTranslator.GetTranslations())
			{
				IAliasTranslation translation = AliasTranslationService.Create();
				translation.Language = Languages.FirstOrDefault(language => language.Id == aliasTranslation.LanguageId);
				translation.Value = string.IsNullOrEmpty(aliasTranslation.Value) ? null : aliasTranslation.Value;

				translations.Add(translation);
			}

			if (alias.Value.IsNullOrEmpty() && translations.All(t => t.Value.IsNullOrEmpty()))
			{
				if (alias.Id > 0)
				{
					AliasService.Delete(SignInHelper.SignedInSystemUser, alias.Id);
				}
			}
			else
			{
				alias.Id = AliasService.Save(SignInHelper.SignedInSystemUser, alias);
				foreach (var translation in translations)
				{
					translation.AliasId = alias.Id;
				}
				AliasTranslationService.SaveAll(SignInHelper.SignedInSystemUser, translations);
			}
		}

		protected void OnLinkNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
		}

		protected void PopulateData()
		{
			var blockId = GetBlockId();
			var block = BlockService.GetById(blockId);

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockProductTopList");
			TemplateList.DataBind();
			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			var item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			BlockSetting.Setting = block.Setting;
			OrderStatisticsDayCountTextBox.Text = block.OrderStatisticsDayCount.ToString(CultureInfo.CurrentCulture);

			if (block.IncludeAllCategories)
			{
				WholeRangeRadio.Checked = true;
			}
			else
			{
				CategoriesRadio.Checked = true;
			}

			BlockTitleTextBox.Text = block.Title;
			PopulateTranslation(blockId);

			var alias = ((ILekmerAliasSecureService)AliasService).GetByCommonName(string.Format(block.AliasDefaultCommonName, blockId));
			if (alias != null)
			{
				UrlTitleTextBox.Text = alias.Value;
				PopulateAliasTranslation(alias.Id, AliasTranslationService.GetAllByAlias(alias.Id));
			}
			else
			{
				PopulateAliasTranslation(0, AliasTranslationService.GetAllByAlias(0));
			}

			TimeLimiter.Block = block;

			TargetDevice.Block = block;

			DataBindLinkNodeSelector(block.LinkContentNodeId);
			NavLink.Checked = block.LinkContentNodeId != null && block.LinkContentNodeId > 0;

			CustomUrlTexBox.Text = block.CustomUrl;
			CustomUrl.Checked = !string.IsNullOrEmpty(block.CustomUrl);
		}

		protected void InitializeState()
		{
			if (GetIdOrNull().HasValue)
			{
				var blockCategoryService = IoC.Resolve<IBlockTopListCategorySecureService>();
				var blockCategories = blockCategoryService.GetAllByBlock(GetBlockId());
				ExtendCategoryTitles(blockCategories);

				var blockProductService = IoC.Resolve<IBlockTopListProductSecureService>();
				var blockProducts = blockProductService.GetAllByBlock(ChannelHelper.CurrentChannel.Id, GetBlockId());
				ExtendProductCategoryTitles(blockProducts);

				State = new TopListState(blockCategories, blockProducts);
			}
			else
			{
				State = new TopListState();
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected virtual void PopulateAliasTranslation(int aliasId, Collection<IAliasTranslation> aliasTranslations)
		{
			var translations = new Collection<ITranslationGeneric>();
			foreach (var aliasTranslation in aliasTranslations)
			{
				var translationGeneric = IoC.Resolve<ITranslationGeneric>();
				translationGeneric.Id = aliasTranslation.AliasId;
				translationGeneric.LanguageId = aliasTranslation.Language.Id;
				translationGeneric.Value = aliasTranslation.Value;

				translations.Add(translationGeneric);
			}

			UrlTitleTranslator.DefaultValueControlClientId = UrlTitleTextBox.ClientID;
			UrlTitleTranslator.BusinessObjectId = aliasId;
			UrlTitleTranslator.DataSource = translations;
			UrlTitleTranslator.DataBind();
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		#region CATEGORIES

		protected virtual void OnCategoriesUseChanged(object sender, EventArgs e)
		{
			PopulateCategoriesGrid();
		}

		protected void OnCategoriesRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
														 "javascript:SelectAllBelow('" + selectAllCheckBox.ClientID + @"','" +
														 CategoriesGrid.ClientID +
														 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
														 CategoriesGrid.ClientID + "', '" + CategoriesAllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox) CategoriesGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox) row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
												"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID +
												@"'); ShowBulkUpdatePanel('" +
												cbSelect.ID + "', '" + CategoriesGrid.ClientID + "', '" +
												CategoriesAllSelectedDiv.ClientID + @"');");
					}
					break;
			}
		}

		protected void OnCategoriesRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteCategory") return;
			var categoryId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
			var category = State.Categories.FirstOrDefault(c => c.Category.Id == categoryId);
			State.Categories.Remove(category);
			PopulateCategoriesGrid();
		}

		private void OnCategoriesPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CategoriesGrid.PageIndex = e.NewPageIndex;
			PopulateCategoriesGrid();
		}

		public virtual void OnCategoriesAdd(object sender, EventArgs e)
		{
			var categories = CategoriesTree.SelectedIds;
			if (categories.Count == 0) return;
			var categoryService = IoC.Resolve<ICategorySecureService>();
			foreach (var categoryId in categories)
			{
				var id = categoryId;
				if (State.Categories.FirstOrDefault(c => c.Category.Id == id) != null) continue;

				var blockCategory = IoC.Resolve<IBlockTopListCategory>();
				blockCategory.BlockId = GetIdOrNull() ?? -1;
				blockCategory.Category = categoryService.GetById(id);
				blockCategory.Category.Title = GetCategoryPath(blockCategory.Category);
				State.Categories.Add(blockCategory);
			}
			PopulateCategoriesGrid();
			CategoriesTree.SelectedIds.Clear();
		}

		protected virtual void OnCategoriesRemoveSelected(object sender, EventArgs e)
		{
			var ids = CategoriesGrid.GetSelectedIds();
			if (ids.Count() == 0) return;
			foreach (var id in ids)
			{
				var categoryId = id;
				var category = State.Categories.FirstOrDefault(c => c.Category.Id == categoryId);
				State.Categories.Remove(category);
			}
			PopulateCategoriesGrid();
		}

		protected virtual void OnIncludeSubcategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox) sender;
			var row = (GridViewRow) checkBox.Parent.Parent;
			var idHidden = (HiddenField) row.FindControl("IdHiddenField");
			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);
			var category = State.Categories.First(c => c.Category.Id == categoryId);
			category.IncludeSubcategories = checkBox.Checked;
			PopulateCategoriesGrid();
		}

		protected void PopulateCategoriesGrid()
		{
			if (WholeRangeRadio.Checked)
			{
				CategoriesDiv.Visible = false;
				return;
			}
			CategoriesDiv.Visible = true;
			CategoriesGrid.DataSource = State.Categories;
			CategoriesGrid.DataBind();
			CategoriesGrid.Style.Add("display", CategoriesGrid.Rows.Count > 0 ? "table" : "none");
		}

		protected void PopulateCategoriesTree()
		{
			CategoriesTree.Selector = CategorySelector;
			CategoriesTree.DataBind();
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		protected virtual void ExtendCategoryTitles(Collection<IBlockTopListCategory> blockCategories)
		{
			foreach (var blockCategory in blockCategories)
			{
				blockCategory.Category.Title = GetCategoryPath(blockCategory.Category);
			}
		}

		protected virtual string GetCategoryPath(ICategory category)
		{
			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = GetCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}

		protected virtual ICategory GetCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}

		#endregion

		#region PRODUCTS

		protected void OnProductsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
														 "javascript:SelectAllBelow('" + selectAllCheckBox.ClientID + @"','" +
														 ProductsGrid.ClientID +
														 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
														 ProductsGrid.ClientID + "', '" + ProductsAllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox) ProductsGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox) row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
												"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID +
												@"'); ShowBulkUpdatePanel('" +
												cbSelect.ID + "', '" + ProductsGrid.ClientID + "', '" + ProductsAllSelectedDiv.ClientID +
												@"');");
					}
					break;
			}
		}

		protected void OnProductsRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteProduct") return;
			var productId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
			var product = State.Products.FirstOrDefault(p => p.Product.Id == productId);
			State.Products.Remove(product);
			PopulateProductsGrid();
		}

		private void OnProductsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductsGrid.PageIndex = e.NewPageIndex;
			PopulateProductsGrid();
		}

		private void OnProductsAdd(object sender, ProductSearchEventArgs e)
		{
			foreach (var product in e.Products)
			{
				var productId = product.Id;
				if (State.Products.Any(p => p.Product.Id == productId)) continue;

				var blockProduct = IoC.Resolve<IBlockTopListProduct>();
				blockProduct.BlockId = GetIdOrNull() ?? -1;
				blockProduct.Product = product;
				blockProduct.Product.ShortDescription = GetCategoryPath(product.CategoryId);
				State.Products.Add(blockProduct);
			}
			PopulateProductsGrid();
			ProductsUpdatePanel.Update();
		}

		protected virtual void OnProductsRemoveSelected(object sender, EventArgs e)
		{
			var ids = ProductsGrid.GetSelectedIds();
			if (ids.Count() == 0) return;
			foreach (var id in ids)
			{
				var productId = id;
				var product = State.Products.FirstOrDefault(p => p.Product.Id == productId);
				State.Products.Remove(product);
			}
			PopulateProductsGrid();
		}

		protected void PopulateProductsGrid()
		{
			ProductsGrid.DataSource = State.Products;
			ProductsGrid.DataBind();
			ProductsGrid.Style.Add("display", ProductsGrid.Rows.Count > 0 ? "table" : "none");
		}

		protected virtual void ExtendProductCategoryTitles(Collection<IBlockTopListProduct> blockProducts)
		{
			foreach (var blockProduct in blockProducts)
			{
				blockProduct.Product.ShortDescription = GetCategoryPath(blockProduct.Product.CategoryId);
			}
		}

		protected virtual string GetCategoryPath(int categoryId)
		{
			var category = GetCategory(categoryId);
			return GetCategoryPath(category);
		}

		protected virtual void UpdateForcePositions()
		{
			foreach (GridViewRow row in ProductsGrid.Rows)
			{
				HiddenField idHidden = row.FindControl("IdHiddenField") as HiddenField;
				if (idHidden == null) continue;

				int id;
				if (!int.TryParse(idHidden.Value, out id)) continue;

				var product = State.Products.FirstOrDefault(p => p.Product.Id == id);
				if (product == null) continue;

				TextBox positionBox = row.FindControl("PositionTextBox") as TextBox;
				if (positionBox == null) continue;

				int position;
				product.Position = int.TryParse(positionBox.Text, out position) ? position : 0;
			}
		}

		protected virtual bool ValidateForcePositions()
		{
			if (State.Products.Any(p => p.Position < 1)) return false;

			foreach (var product in State.Products)
			{
				var clone = product;
				if (State.Products.Any(p => p.Position == clone.Position && p.Product.Id != clone.Product.Id))
				{
					return false;
				}
			}
			return true;
		}

		#endregion

		protected void DataBindLinkNodeSelector(int? nodeId)
		{
			LinkNodeSelector.SelectedNodeId = nodeId;
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
			LinkNodeSelector.PopulatePath(nodeId);
		}
	}

	[Serializable]
	public sealed class TopListState
	{
		public TopListState()
		{
			Categories = new Collection<IBlockTopListCategory>();
			Products = new Collection<IBlockTopListProduct>();
		}

		public TopListState(Collection<IBlockTopListCategory> categories, Collection<IBlockTopListProduct> products)
		{
			Categories = categories;
			Products = products;
		}

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockTopListCategory> Categories { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockTopListProduct> Products { get; set; }
	}
}