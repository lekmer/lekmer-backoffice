﻿<%@ page title="" language="C#"
	masterpagefile="~/Modules/SiteStructure/Pages/Pages.Master"
	autoeventwireup="true"
	codebehind="BlockEsalesCartEdit.aspx.cs"
	inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockEsalesCartEdit" %>

<%@ register tagprefix="uc" tagname="GenericTranslator" src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ register tagprefix="uc" tagname="BlockSetting" src="~/UserControls/SiteStructure/BlockSetting.ascx" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgBlockEsalesRecommend" />
</asp:Content>

<asp:Content ID="cBPRL" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box esales-recommend">
			<span>
				<asp:Literal runat="server" Text="<%$ Resources:General,Literal_Title %>" /></span>&nbsp;
			<uc:generictranslator id="Translator" runat="server" />
			&nbsp;
			<asp:RequiredFieldValidator
				runat="server"
				ID="BlockTitleValidator"
				ControlToValidate="BlockTitleTextBox"
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"
				Display="None" ValidationGroup="vgBlockEsalesCart" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br />
			<br />

			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="column">
					<div class="input-box">
						<span>
							<asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate %>" /></span>
						<br />
						<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" Style="margin-top: 5px;" />
					</div>
				</div>
				<div class="block-setting-content" style="padding-top: 0">
					<br class="clear" />
					<uc:blocksetting runat="server" id="BlockSetting" validationgroup="vgBlockEsalesCart" />
				</div>
			</div>

		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:imagelinkbutton usesubmitbehaviour="true" id="SaveButton" runat="server" text="<%$ Resources:General,Button_Save %>" skinid="DefaultButton" validationgroup="vgBlockEsalesCart" />
			<uc:imagelinkbutton usesubmitbehaviour="true" id="CancelButton" runat="server" text="<%$ Resources:General,Button_Cancel %>" skinid="DefaultButton" causesvalidation="false" />
		</div>
	</asp:Panel>
</asp:Content>
