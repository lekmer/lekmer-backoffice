﻿<%@ page title="" language="C#" masterpagefile="~/Modules/Assortment/Sizes/SizeTables.Master" autoeventwireup="true" codebehind="SizeEdit.aspx.cs" inherits="Litium.Scensum.BackOffice.Modules.Assortment.Sizes.SizeEdit" %>


<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="SizeTableEditContent" runat="server" ContentPlaceHolderID="SizeTablePlaceHolder">
	<div class="size-tables">
		<div class="column">
			<div class="input-box">
				<span><%=Resources.General.Literal_ErpId%> *</span>

				<br />
				<asp:TextBox ID="ErpIdTextBox" runat="server" MaxLength="50"/>
	
			</div>
			<div class="input-box">
				<span><%=Resources.General.Literal_ErpTitle%> *</span>
				<br />
				<asp:TextBox ID="ErpTitleTextBox" runat="server" MaxLength="50" />
			</div>
		</div>
		<div class="column">
			<div class="input-box">
				<span><%=Resources.General.Literal_EuId%> *</span>

				<br />
				<input ID="EuTextBox" runat="server" typ="number" />
			</div>
			<div class="input-box">
				<span><%=Resources.General.Literal_EuTitle%> *</span>
				<br />
				<asp:TextBox ID="EuTitleTextBox" runat="server" MaxLength="50" />
			</div>
		</div>

	</div>

	
	<br class="clear" />
	<br class="clear" />

	<div class="buttons right">
		<uc:imagelinkbutton id="SaveButton" usesubmitbehaviour="true" runat="server" text="<%$ Resources:General, Button_Save %>" validationgroup="vgOrdinals" skinid="DefaultButton" />
		<uc:imagelinkbutton id="CancelButton" usesubmitbehaviour="true" runat="server" text="<%$ Resources:General, Button_Cancel %>" causesvalidation="false" skinid="DefaultButton" />
	</div>
</asp:Content>
