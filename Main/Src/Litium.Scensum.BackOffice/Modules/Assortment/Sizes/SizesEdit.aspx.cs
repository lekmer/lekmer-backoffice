﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Sizes
{
	public partial class SizesEdit : LekmerStatePageController<SizesEdit.SizesEditState>, IEditor
	{
		private const int DifferenceStep = 10;
		protected virtual SizeTablesMaster MasterPage
		{
			get { return (SizeTablesMaster)Master; }
		}
		protected override void SetEventHandlers()
		{
			SizeEditRowsGrid.RowCommand += OnGridCommand;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			PopulateData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			MasterPage.UpdateBreadcrumbs(Resources.Lekmer.Literal_EditDeleteSize);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (State.DeletedItems.Any())
			{
				foreach (var id in State.DeletedItems)
				{
					var result = _SizeSecureService.Delete(SignInHelper.SignedInSystemUser, id);
					if (result < 0)
					{
						SystemMessageContainer.Add(string.Format("{0} Id={1}.", Resources.LekmerMessage.Size_CannotDelete, id));
					}
				}
				PopulateData();
			}
		}

		private ISizeSecureService _sizeSecureService;

		private ISizeSecureService _SizeSecureService
		{
			get { return _sizeSecureService ?? IoC.Resolve<ISizeSecureService>(); }
		}

		protected void OnGridCommand(object sender, GridViewCommandEventArgs e)
		{
			var id = System.Convert.ToInt32(e.CommandArgument);
			var item = State.Sizes.FirstOrDefault(i => i.Id == id);
			if (item != null)
			{
				switch (e.CommandName)
				{
					case "DeleteSizeRow":
						State.Sizes.Remove(item);
						State.DeletedItems.Add(id);
						PopulateGrid();
						break;
				}
			}
		}


		protected virtual void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				MasterPage.RedirectToDefaultPage();
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		protected virtual string GetEditUrl(object sizeId)
		{
			return LekmerPathHelper.Sizes.GetSizeEditUrl(System.Convert.ToInt32(sizeId, CultureInfo.CurrentCulture));
		}

		protected void PopulateData()
		{
			Collection<ISize> sizes = _SizeSecureService.GetAll();

			State = new SizesEditState(Sort(sizes, DifferenceStep));
			PopulateGrid();
		}

		protected static Collection<ISize> Sort(Collection<ISize> list, int step)
		{
			var sizes = new List<ISize>(list);

			sizes.Sort((element1, element2) => element1.Ordinal.CompareTo(element2.Ordinal));

			for (int i = 0; i < list.Count(); i++)
			{
				sizes[i].Ordinal = (i + 1) * step;
			}

			return new Collection<ISize>(sizes);
		}

		protected void PopulateGrid()
		{
			SizeEditRowsGrid.DataSource = State.Sizes;
			SizeEditRowsGrid.DataBind();
		}
	

		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string FormatSize(object size)
		{
			var decSize = System.Convert.ToDecimal(size, CultureInfo.CurrentCulture);
			var intSize = decimal.ToInt32(decSize);
			return decSize - intSize == 0
				? intSize.ToString(CultureInfo.CurrentCulture)
				: decSize.ToString(CultureInfo.CurrentCulture);
		}

		[Serializable]
		public sealed class SizesEditState
		{
			[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
			public Collection<ISize> Sizes { get; set; }

			public SizesEditState(Collection<ISize> sizes)
			{
				Sizes = sizes;
				DeletedItems = new Collection<int>();
			}
			public Collection<int> DeletedItems { get; set; }


		}
	}


}