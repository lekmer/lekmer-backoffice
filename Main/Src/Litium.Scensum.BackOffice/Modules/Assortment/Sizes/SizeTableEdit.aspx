﻿<%@ Page Language="C#" MasterPageFile="SizeTables.Master" CodeBehind="SizeTableEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Sizes.SizeTableEdit" AutoEventWireup="true"%>

<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericMultilineTranslator" Src="~/UserControls/Translation/GenericMultilineTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="SizeTableIncludeItemsConfigurator" Src="~/UserControls/Assortment/SizeTableIncludeItemsConfigurator.ascx" %>
<%@ Register TagPrefix="uc" TagName="SizeTableRowForm" Src="~/UserControls/Assortment/SizeTableRowForm.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="MediaItemSelect" Src="~/UserControls/Media/MediaItemSelect.ascx"%>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="SizeTableEditContent" runat="server" ContentPlaceHolderID="SizeTablePlaceHolder">
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function () { $('#tabs').tabs(); });

		$(function () {
			media_url = '<%=ResolveUrl("~/Media") %>';
			$('div.light-box a.light-box').lightBox();
		});

		function confirmDelete() {
			return DeleteConfirmation("<%= Resources.Lekmer.Literal_SizeTableRow%>");
		}
		
		function confirmDeleteMediaItem() {
			return DeleteConfirmation("<%= Resources.Media.Literal_mediaItem%>");
		}

		function ResetDefaultMediaMessages() {
			$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
		}
	</script>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div id="tabs">
			<ul>
				<li><a href="#fragment-1"><span><%= Resources.Product.Tab_ProductEdit_Info %></span></a></li>
				<li><a href="#fragment-2"><span><%= Resources.Product.Tab_SizeTable_Includes %></span></a></li>
				<li><a href="#fragment-3"><span><%= Resources.Media.Literal_Media %></span></a></li>
			</ul>
			<br clear="all" />
			<div class="category-tabs-main-container" id="tabs-main-container">
				<div id="fragment-1">
					<div class="size-tables">
						<div class="column">
							<div class="input-box">
								<span><%=Resources.General.Literal_Title%> *</span>
								<uc:GenericTranslator ID="SizeTableTitleTranslator" runat="server" />
								<br />
								<asp:TextBox ID="TitleTextBox" runat="server" MaxLength="50" />
							</div>
							<div class="input-box">
								<span><%=Resources.General.Literal_CommonName%> *</span>
								<br />
								<asp:TextBox ID="CommonNameTextBox" runat="server" MaxLength="50" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
								<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
							</div>
						</div>
					</div>

					<br class="clear" />

					<div class="size-tables">
						<div class="input-box">
							<span><%=Resources.General.Literal_Description%></span>
							<asp:ImageButton runat="server" ID="SizeTableDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
							<br />
							<asp:TextBox ID="DescriptionTextBox" runat="server" TextMode="MultiLine" Rows="5" />
						</div>
					</div>

					<br class="clear" />
					<hr/>

					<span class="bold">Size table rows</span>
					<div class="size-tables">
						<div class="column">
							<div class="input-box">
								<span>First column title *</span>
								<uc:GenericTranslator ID="Column1TitleTranslator" runat="server" />
								<br />
								<asp:TextBox ID="Column1TitleTextBox" runat="server" MaxLength="50" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span>Second column title</span>
								<uc:GenericTranslator ID="Column2TitleTranslator" runat="server" />
								<br />
								<asp:TextBox ID="Column2TitleTextBox" runat="server" MaxLength="50" />
							</div>
						</div>
					</div>

					<br class="clear" />

					<div style="margin-top: 10px;">
						<asp:UpdatePanel ID="SizeTableRowsPanel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<sc:GridViewWithCustomPager
									ID="SizeTableRowsGrid"
									SkinID="grid"
									runat="server"
									AutoGenerateColumns="false"
									AllowPaging="true"
									PageSize="<%$AppSettings:DefaultGridPageSize%>"
									Width="100%">
									<Columns>
										<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Size %>">
											<ItemTemplate>
												<asp:LinkButton ID="SizeTableRowLink" runat="server" CommandName="EditSizeTableRow" CommandArgument='<%# Eval("Id") %>' Text='<%# GetSizeTitle(Eval("SizeTableRow.SizeId")) %>' />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Col1Value%>">
											<ItemTemplate>
												<uc:LiteralEncoded ID="Col1Value" runat="server" Text='<%# Eval("SizeTableRow.Column1Value") %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Col2Value%>">
											<ItemTemplate>
												<uc:LiteralEncoded ID="Col2Value" runat="server" Text='<%# Eval("SizeTableRow.Column2Value") %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
											<HeaderTemplate>
												<div class="inline">
													<%= Resources.General.Literal_SortOrder %>
													<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" CommandArgument="0" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
												</div>
											</HeaderTemplate>
											<ItemTemplate>
												<div class="inline">
													<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
													<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
													<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%# Eval("SizeTableRow.Ordinal") %>' Width="40px" />
													<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
													<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
												</div>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteSizeTableRow" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</sc:GridViewWithCustomPager>
							</ContentTemplate>
						</asp:UpdatePanel>
						<br class="clear" />
						<uc:SizeTableRowForm ID="SizeTableRowForm" runat="server" />
					</div>
				</div>
				<div id="fragment-2">
					<uc:SizeTableIncludeItemsConfigurator ID="SizeTableIncludeItemsConfigurator" runat="server"/>
				</div>
				<div id="fragment-3">
					<div class="media-catalog-container">
						<asp:UpdatePanel ID="MediaUpdatePanel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<sc:GridViewWithCustomPager 
									ID="MediaItemsGrid"
									SkinID="grid"
									runat="server"
									AutoGenerateColumns="false"
									AllowPaging="true"
									PageSize="<%$AppSettings:DefaultGridPageSize%>"
									Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-Height="50px" ItemStyle-Width="6%" >
											<ItemTemplate>
												<asp:Image id="imgArchiveImage" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
												<asp:HiddenField ID="IdHiddenField" Value='<%# Eval("Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="File info" ItemStyle-Width="52%">
											<ItemTemplate>
												<span class="bold"><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Title%>" />: </span>
												<asp:Label ID="Title" runat="server" Text='<%# Eval("MediaItem.Title") %>' /><br />
												<span class="bold"><asp:Literal runat="server" Text="<%$ Resources:Media, Literal_FileType%>" />: </span>
												<asp:Label ID="lblArchiveImageFileType" runat="server" ></asp:Label><br />
												<span class="bold"><asp:Literal runat="server" Text="<%$ Resources:Media, Literal_Dimensions%>" />: </span>
												<asp:Label ID="lblArchiveImageDimensions" runat="server" ></asp:Label><br />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Registry" ItemStyle-Width="20%">
											<ItemTemplate>
												<asp:Repeater runat="server" ID="RegistryRepeater">
													<ItemTemplate>
														<div>
															<div class="left" style="width: 120px;">
																<asp:HiddenField runat="server" ID="RegistryIdHidden" Value='<%# Eval("Id") %>' />
																<asp:CheckBox runat="server" ID="RegistryCheckbox" Checked='<%# Eval("IsSelected") %>' Text='<%# Eval("Title") %>' />
															</div>
														</div>
														<br class="clear" />
													</ItemTemplate>
												</asp:Repeater>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteMediaItem" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDeleteMediaItem();" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</sc:GridViewWithCustomPager>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>

					<hr />

					<uc:ImageLinkButton runat="server" ID="MediaItemBrowseButton" Text="Add media item" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();"/>
				</div>
			</div>

			<br class="clear" />
			<br class="clear" />

			<div class="buttons right">
				<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
				<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
			</div>
		</div>
	</asp:Panel>
	<uc:GenericMultilineTranslator ID="SizeTableDescriptionTranslator" runat="server" />
	
	<ajaxToolkit:ModalPopupExtender
		ID="MediaItemsPopup"
		runat="server"
		TargetControlID="MediaItemBrowseButton"
		PopupControlID="MediaItemsDiv" 
		BackgroundCssClass="popup-background"
		CancelControlID="_inpClose"
		OnCancelScript="ResetDefaultMediaMessages();">
	</ajaxToolkit:ModalPopupExtender>

	<div id="MediaItemsDiv" runat="server" class="product-popup-images-container" style="z-index: 10010; display: none;">
		<div id="product-popup-images-header">
			<div id="product-popup-images-header-left">
			</div>

			<div id="product-popup-images-header-center">
				<span>Add media item</span>
				<input type="button" id="_inpClose" class="_inpClose" value="x" />
			</div>

			<div id="product-popup-images-header-right">
			</div>
		</div>
		<br clear="all" />

		<div class="product-popup-images-body">
			<uc:MediaItemSelect id="MediaItemSelectControl" runat="server"/>
		</div>
	</div>
</asp:Content>