﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Sizes
{
	public partial class SizeSorting : LekmerStatePageController<SizeSortingState>, IEditor 
	{
		private const int MoveUpStep = -15;
		private const int MoveDownStep = 15;
		private const int DifferenceStep = 10;
		private const int MinStep = 1;

		protected virtual SizeTablesMaster MasterPage
		{
			get
			{
				return (SizeTablesMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			SizesGrid.RowCommand += OnRowCommand;
		}
		
		protected override void PopulateForm()
		{
			PopulateData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			MasterPage.UpdateBreadcrumbs(Resources.Lekmer.Literal_SizeSorting);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			RefreshOrdinals();
			var sizes = Sort(State.Sizes, MinStep);

			var service = IoC.Resolve<ISizeSecureService>();
			service.SetOrdinals(SignInHelper.SignedInSystemUser, sizes);

			SuccessMessager.Add(Resources.Lekmer.Literal_SetSizeOrdinalsSuccess);
		}

		protected virtual void OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id)) return;

			switch (e.CommandName)
			{
				case "UpOrdinal":
					Move(id, MoveUpStep);
					break;
				case "DownOrdinal":
					Move(id, MoveDownStep);
					break;
				case "RefreshOrdinal":
					RefreshOrdinals();
					break;
			}
		}

		protected void Move(int elementId, int step)
		{
			var sizes = State.Sizes;
			var size = sizes.FirstOrDefault(c => c.Id == elementId);
			if (size == null) return;
			size.Ordinal += step;
			State.Sizes = Sort(sizes, DifferenceStep);
			PopulateGrid();
		}
		
		protected void RefreshOrdinals()
		{
			if (!ValidateOrdinals())
			{
				ErrorMessager.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return;
			}

			var sizes = State.Sizes;

			foreach (GridViewRow row in SizesGrid.Rows)
			{
				var sizeId = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var size = sizes.FirstOrDefault(c => c.Id == sizeId);
				if (size == null) continue;
				size.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
			}

			State.Sizes = Sort(sizes, DifferenceStep);
			PopulateGrid();
		}
		
		protected void PopulateData()
		{
			var sizeSecureService = IoC.Resolve<ISizeSecureService>();

			Collection<ISize> sizes = sizeSecureService.GetAll();

			State = new SizeSortingState(Sort(sizes, DifferenceStep));
			PopulateGrid();
		}

		protected void PopulateGrid()
		{
			SizesGrid.DataSource = State.Sizes;
			SizesGrid.DataBind();
		}

		protected bool ValidateOrdinals()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}

		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string FormatSize(object size)
		{
			var decSize = System.Convert.ToDecimal(size, CultureInfo.CurrentCulture);
			var intSize = decimal.ToInt32(decSize);
			return decSize - intSize == 0
				? intSize.ToString(CultureInfo.CurrentCulture)
				: decSize.ToString(CultureInfo.CurrentCulture);
		}

		protected static Collection<ISize> Sort(Collection<ISize> list, int step)
		{
			var sizes = new List<ISize>(list);

			sizes.Sort((element1, element2) => element1.Ordinal.CompareTo(element2.Ordinal));

			for (int i = 0; i < list.Count(); i++)
			{
				sizes[i].Ordinal = (i + 1) * step;
			}

			return new Collection<ISize>(sizes);
		}
	}

	[Serializable]
	public sealed class SizeSortingState
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ISize> Sizes { get; set; }

		public SizeSortingState(Collection<ISize> sizes)
		{
			Sizes = sizes;
		}
	}
}