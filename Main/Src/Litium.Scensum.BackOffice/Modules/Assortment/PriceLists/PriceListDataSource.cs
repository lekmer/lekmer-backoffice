using System.Collections.ObjectModel;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.PriceLists
{
	public class PriceListDataSource
	{
		private int _rowCount;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "searchCriteria"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex, string searchCriteria)
		{
			return _rowCount;
		}
		public Collection<IPriceList> SearchMethod(int maximumRows, int startRowIndex, string searchCriteria)
		{
			IPriceListSecureService priceListSecureService = IoC.Resolve<IPriceListSecureService>();
			var pricelists = priceListSecureService.Search(searchCriteria, startRowIndex/maximumRows + 1, maximumRows);
			_rowCount = pricelists.TotalCount;
			return pricelists;
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "folderId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]

		public int SelectCount(int maximumRows, int startRowIndex, int folderId)
		{
			return _rowCount;
		}
		public Collection<IPriceList> GetAllByFolderMethod(int maximumRows, int startRowIndex, int folderId)
		{
			IPriceListSecureService priceListSecureService = IoC.Resolve<IPriceListSecureService>();
			var pricelists = priceListSecureService.GetAllByFolder(folderId, startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = pricelists.TotalCount;
			return pricelists;
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public Collection<IPriceList> GetAllByFolderMethod(int maximumRows, int startRowIndex)
		{

			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "title"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "statusId"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "currencyId")]
		public int SelectCount(int maximumRows, int startRowIndex, string title, string currencyId, string statusId)
		{
			return _rowCount;
		}

		public Collection<IPriceList> AdvancedSearchMethod(int maximumRows, int startRowIndex, string title, string currencyId, string statusId)
		{
			var searchCriteria = IoC.Resolve<IPriceListSearchCriteria>();
            searchCriteria.Title = title;
			searchCriteria.CurrencyId = currencyId;
			searchCriteria.StatusId = statusId;

			var service = IoC.Resolve<IPriceListSecureService>();
			var pricelists = service.SearchAdvanced(searchCriteria, maximumRows == 0 ? 0 : startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = pricelists.TotalCount;
			return pricelists;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public Collection<IPriceList> AdvancedSearchMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}
	}
}