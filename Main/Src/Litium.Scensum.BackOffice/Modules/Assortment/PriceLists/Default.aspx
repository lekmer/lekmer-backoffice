<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/PriceLists/PriceLists.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.Default" %>
<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Import Namespace="System.Linq"%>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
	
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="defaultContent" ContentPlaceHolderID="PriceListContentPlaceHolder" runat="server">
<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="SetStatusButton">
    <div id="PriceListSearchResultLabelPanel" runat="server" visible="false">
        <asp:Label ID="PriceListSearchResultLabel" runat="server" Text="<%$ Resources:General, Literal_SearchResults %>" CssClass="header-text"></asp:Label>
        <br />
        <br />
    </div>
    <div id="price-list">
		<sc:GridViewWithCustomPager
			ID="PriceListGrid" 
			SkinID="grid" 
			runat="server" 
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			DataSourceID="PriceListObjectDataSource"
			Width="100%">
			<Columns>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
					<HeaderTemplate>
						<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
					</HeaderTemplate>
					<ItemTemplate>
						<asp:CheckBox ID="SelectCheckBox" runat="server" />
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
					</ItemTemplate>
				</asp:TemplateField>	
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="35%">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="PriceEditLink" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Pricelist.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:General,Literal_StartDate %>" DataField="StartDateTime" ItemStyle-Width="20%" />
				<asp:BoundField HeaderText="<%$ Resources:General,Literal_EndDate %>" DataField="EndDateTime" ItemStyle-Width="20%" />
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Currency %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<%# GetCurrency((int)Eval("CurrencyId"))%>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Status %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<%# GetPriceListStatus((int)Eval("PriceListStatusId"))%>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
	<div id="AllSelectedDiv" runat="server" class="apply-to-selected-pricelists">
		<div style="float:left">
			<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
			<asp:DropDownList ID="PricelistStatusList" DataTextField="Title" Width="100px" DataValueField="Id" runat="server" /> &nbsp;
		</div> 
		<div style="float:left">
			<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="SetStatusButton" runat="server" Text="<%$ Resources:General,Button_Set%>" />
		</div> 
	</div>
	<asp:ObjectDataSource ID="PriceListObjectDataSource"  CacheDuration="300" CacheKeyDependency="AssortmentPriceListCacheKey" EnableCaching="true" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetAllByFolderMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListDataSource" />
	
</asp:Panel>
</asp:Content>