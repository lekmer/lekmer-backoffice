<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Assortment/PriceLists/PriceLists.Master"
	AutoEventWireup="true" CodeBehind="PriceListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListEdit" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree"
	TagPrefix="CustomControls" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register Src="~/UserControls/Customer/CustomerGroupSelector.ascx" TagName="CustomerGroupSelector"
	TagPrefix="uc" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>				
				<uc:MessageContainer ID="SystemMessageContainer" MessageType="Success" HideMessagesControlId="SaveButton"
					runat="server" />
				<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server"
					CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="vg" />
					</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="plec" ContentPlaceHolderID="PriceListContentPlaceHolder" runat="server">

	<script language="javascript" type="text/javascript">
		function ChangeStartDate() {
			ChangeDate('start');
		}
		function ChangeEndDate() {
			ChangeDate('end');
		}
		function ChangeDate(value) {
			var date = value == 'end' ? document.getElementById('<%= EndDateHiddenTextBox.ClientID %>') : document.getElementById('<%= StartDateHiddenTextBox.ClientID %>');
			var dateTime = value == 'end' ? document.getElementById('<%= EndDateTextBox.ClientID %>') : document.getElementById('<%= StartDateTextBox.ClientID %>');
			if (dateTime.value == null || dateTime.value.length == 0 || dateTime.value.indexOf(' ') == -1) {
				dateTime.value = date.value.concat(' 00:00:00');
				return;
			}
			var pos = dateTime.value.indexOf(' ');
			var time = dateTime.value.substr(pos, dateTime.value.length - pos);
			dateTime.value = date.value.concat(time);
		}
		function ConfirmCustomerGroupDelete() {
			return DeleteConfirmation("<%= Resources.Customer.Literal_customerGroup %>");
		}
	</script>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<asp:Label ID="PriceListEditLabel" runat="server" Text="<%$ Resources:Product, Literal_Edit_Pricelist %>" CssClass="header-text"></asp:Label>
		<br />
		<div id="pricelist-edit">
			<div class="input-box">
				<span>
					<%=Resources.General.Literal_Title%>&nbsp;*</span>
				<br />
				<asp:TextBox ID="TitleTextBox" runat="server" ReadOnly="true" CssClass="box" />
			</div>
			<div class="input-box">
				<span>
					<%= Resources.General.Literal_CommonName %></span>
				<br />
				<asp:TextBox ID="CommonNameTextBox" runat="server" ReadOnly="true" CssClass="box" />
			</div>
			<div class="input-box">
				<span>
					<%= Resources.General.Literal_Currency%></span>
				<br />
				<asp:TextBox ID="CurrencyTextBox" runat="server" ReadOnly="true" CssClass="box" />
			</div>
			<div class="input-box">
				<span>
					<%= Resources.General.Literal_PlaceInFolder %></span>
				<uc:NodeSelect ID="PlaceFolderNodeSelector" runat="server" UseRootNodeInPath="true" />
			</div>
			<br />
			<div id="date-edit">
				<div class="input-box">
					<span>
						<%= Resources.General.Literal_StartDate %></span>
					<asp:CustomValidator ID="StartDateValidator" runat="server" ControlToValidate="StartDateTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage, StartDateType %>" Text=" *" ValidationGroup="vg"></asp:CustomValidator>
					<br />
					<asp:TextBox ID="StartDateTextBox" runat="server" CssClass="box" />
					<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Assortment/date.png"
						ImageAlign="AbsMiddle" CausesValidation="False" />
					<asp:TextBox ID="StartDateHiddenTextBox" runat="server" CssClass="dateBox"></asp:TextBox>
					<ajaxToolkit:MaskedEditExtender ID="StartDateMaskedEdit" runat="server" TargetControlID="StartDateTextBox"
						MaskType="DateTime" />
					<ajaxToolkit:CalendarExtender ID="StartDateCalendarExtender" runat="server" TargetControlID="StartDateHiddenTextBox"
						OnClientDateSelectionChanged="ChangeStartDate" PopupButtonID="StartDateButton" />
				</div>
				<div class="input-box">
					<span>
						<%= Resources.General.Literal_EndDate %></span>
					<asp:CustomValidator ID="EndDateValidator" runat="server" ControlToValidate="EndDateTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage, EndDateType %>" Text=" *" ValidationGroup="vg" />
					<asp:CustomValidator ID="StartEndDateLimitValidator" runat="server" ControlToValidate="EndDateTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage, EndDateGreaterStartDate %>" Text=" *"
						ValidationGroup="vg" />
					<br />
					<asp:TextBox ID="EndDateTextBox" runat="server" CssClass="box" />
					<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Assortment/date.png"
						ImageAlign="AbsMiddle" CausesValidation="False" />
					<asp:TextBox ID="EndDateHiddenTextBox" runat="server" CssClass="dateBox"></asp:TextBox>
					<ajaxToolkit:MaskedEditExtender ID="EndDateMaskedEdit" runat="server" TargetControlID="EndDateTextBox"
						MaskType="DateTime" />
					<ajaxToolkit:CalendarExtender ID="EndDateCalendarExtender" runat="server" TargetControlID="EndDateHiddenTextBox"
						OnClientDateSelectionChanged="ChangeEndDate" PopupButtonID="EndDateButton" />
				</div>
				<div class="input-box">
					<span>
						<%= Resources.General.Literal_Status %></span>
					<br />
					<asp:DropDownList ID="StatusList" runat="server" DataTextField="Title" DataValueField="id">
					</asp:DropDownList>
				</div>
			</div>
		</div>
		<br class="clear" />
		<br class="clear" />
		<div id="price-list">
			<span>
				<%= Resources.Product.Literal_ProductPrice %></span>
			<sc:GridViewWithCustomPager ID="ProductGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
				AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" DataSourceID="PiceListItemsObjectDataSource"
				Width="100%">
				<Columns>
					<asp:TemplateField HeaderText="<%$ Resources:Product,Literal_ArticleId %>" ItemStyle-Width="10%">
						<ItemTemplate>
							<asp:Label ID="ProductArticleIdLabel" runat="server"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="50%">
						<ItemTemplate>
							<asp:Label ID="ProductTitleLabel" runat="server"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Status %>" ItemStyle-Width="10%">
						<ItemTemplate>
							<asp:Label ID="ProductStatusLabel" runat="server"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Product,Literal_PriceIncl %>" ItemStyle-HorizontalAlign="Right"
						ItemStyle-Width="10%">
						<ItemTemplate>
							<asp:Label ID="ProductPriceIncl" runat="server" Text='<%# GetPrice((int)Eval("PriceListID"), Convert.ToDecimal(Eval("PriceIncludingVat"))) %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Product,Literal_PriceExcl %>" ItemStyle-HorizontalAlign="Right"
						ItemStyle-Width="10%">
						<ItemTemplate>
							<asp:Label ID="ProductPriceExcl" runat="server" Text='<%# GetPrice((int)Eval("PriceListID"), Convert.ToDecimal(Eval("PriceExcludingVat"))) %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="<%$ Resources:Product,Literal_Vat %>" DataField="VatPercentage"
						ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" />
				</Columns>
			</sc:GridViewWithCustomPager>
			<asp:ObjectDataSource ID="PiceListItemsObjectDataSource" runat="server" EnablePaging="true"
				SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListItemDataSource" />
		</div>
		<br />
		<div id="customer-group-customer">
			<asp:UpdatePanel ID="CustomerGroupsUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<span>
						<%= Resources.Product.Literal_CustomerGroups %></span>
					<br />
					<Scensum:Grid runat="server" ID="CustomerGroupsScensumGrid" FixedColumns="Title">
						<sc:GridViewWithCustomPager ID="CustomerGroupsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
							AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
									ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Title %>" ItemStyle-Width="64%">
									<ItemTemplate>
										<uc:HyperLinkEncoded ID="GroupTitleLink" runat="server" Text='<%# Eval("Title") %>'
											NavigateUrl='<%# GetCustomerGropEditUrl(Eval("Id")) %>'> </uc:HyperLinkEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_ErpId %>" DataField="ErpId"
									ItemStyle-Width="15%" />
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Status %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="GroupStatusLiteral" runat="server" Text='<%# GetCustomerGropStatus(Eval("StatusId")) %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteGroup" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return ConfirmCustomerGroupDelete();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
					</Scensum:Grid>
					<div id="AllSelectedDiv" runat="server" style="display: none;" class="apply-to-all-selected-2">
						<uc:ImageLinkButton ID="RemoveCustomerGroupsButton" runat="server" Text="<%$ Resources:Customer, Button_RemoveSelected%>"
							SkinID="DefaultButton" OnClientClick="return ConfirmCustomerGroupDelete();" />
					</div>
					<br />
					<div class="right">
						<uc:CustomerGroupSelector ID="CustomerGroupSelector" runat="server" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
		<br class="clear" /><br/>
		<asp:UpdatePanel ID="FooterUpdatePanel" runat="server">
			<ContentTemplate>
				<div class="buttons right">
					<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save%>"
						ValidationGroup="vg" SkinID="DefaultButton" />
					<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel%>"
						CausesValidation="false" SkinID="DefaultButton" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>
