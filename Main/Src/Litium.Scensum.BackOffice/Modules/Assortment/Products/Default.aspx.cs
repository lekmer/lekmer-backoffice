using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Label = System.Web.UI.WebControls.Label;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	[Serializable]
	public class SearchItem
	{
		public virtual SearchType SearchType { get; set; }
		public virtual bool IsDeletable { get; set; }

		private IProductSearchCriteria productSearchCriteria;
		public virtual IProductSearchCriteria ProductSearchCriteria
		{
			get
			{
				if (productSearchCriteria == null)
				{
					productSearchCriteria = IoC.Resolve<IProductSearchCriteria>();
				}
				return productSearchCriteria;
			}
			set
			{
				productSearchCriteria = value;
			}
		}

		private Guid _uId;
		public virtual Guid UId
		{
			get
			{
				if (_uId == Guid.Empty)
				{
					_uId = Guid.NewGuid();
				}
				return _uId;
			}
		}
	}

	public enum SearchType
	{
		None = 0,
		Inclusive = 1,
		Exclusive = 2
	}

	public partial class Default : LekmerStatePageController<SearchItem>
	{
		private Collection<IProductStatus> _productStatuses;
		private Collection<IProductType> _productTypes;

		private ICategory _category;
		private SearchItem CriteriaDataSourceViewState
		{
			get
			{
				object criteriaDataSourceViewState = State;
				if (criteriaDataSourceViewState != null)
				{
					return (SearchItem)criteriaDataSourceViewState;
				}
				return new SearchItem();
			}
			set
			{
				State = value;
			}
		}

		private static SearchItem DefaultSearchItem
		{
			get
			{
				return new SearchItem
				{
					SearchType = SearchType.None,
					IsDeletable = false
				};
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			RegisterRequiresControlState(this);
		}
		protected override void SetEventHandlers()
		{
			SearchButton.Click += SearchButtonClick;
			SearchResultsGrid.RowDataBound += SearchResultsGridRowDataBound;
			SearchResultsGrid.PageIndexChanging += SearchResultsGridPageIndexChanging;
			Articles.ColumnsChanged += Articles_ColumnsChanged;
		}
		protected override void PopulateForm()
		{
			State = new SearchItem();
			RestoreSearchData();
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!IsPostBack)
			{
				BindSearchSection();
			}
		}
		protected virtual void SearchButtonClick(object sender, EventArgs e)
		{
			SeadrchResultLabel.Visible = Articles.Visible = true;
			SearchResultsGrid.PageIndex = 0;
			SaveSearchCriteria();
			if (!Page.IsValid) return;
			StoreSearchData();
			PopulateSearch();
		}
		protected virtual void SearchResultsGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			IProduct product = (IProduct)row.DataItem;
			Label lblPrice = (Label)e.Row.FindControl("PriceLabel");
			if (product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}

			var lblProductStatus = (Label)e.Row.FindControl("ProductStatusLabel");
			lblProductStatus.Text = ResolveStatusTitle(product.ProductStatusId);

			var lblType = (Label)e.Row.FindControl("ProductTypeLabel");
			lblType.Text = ResolveTypeTitle(((ILekmerProduct)product).ProductTypeId);
		}
		protected virtual void SearchResultsGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
		}
		protected virtual void Articles_ColumnsChanged(object sender, EventArgs e)
		{
			SaveSearchCriteria();
		}

		protected void RestoreSearchData()
		{
			SearchItem source = SearchCriteriaState<SearchItem>.Instance.Get();
			if (source != null)
			{
				CriteriaDataSourceViewState = source;
				if (!IsPostBack)
				{
					SearchResultsGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
					SeadrchResultLabel.Visible = Articles.Visible = true;
					PopulateSearch();
				}
			}
			else
			{
				CriteriaDataSourceViewState = new SearchItem();
				CriteriaDataSourceViewState = DefaultSearchItem;
				SeadrchResultLabel.Visible = Articles.Visible = false;
			}
		}
		protected void BindSearchSection()
		{
			//UserControls.Search.Search _search = (UserControls.Search.Search)upSearchCriteria.FindControl("search");
			SearchControl.RestoreSearchFields(CriteriaDataSourceViewState.ProductSearchCriteria, 1);
		}
		protected void SaveSearchCriteria()
		{
			//UserControls.Search.Search _search = (UserControls.Search.Search)upSearchCriteria.FindControl("search");
			CriteriaDataSourceViewState.ProductSearchCriteria = SearchControl.CreateSearchCriteria();
		}
		protected void StoreSearchData()
		{
			SearchCriteriaState<SearchItem>.Instance.Save(CriteriaDataSourceViewState);
		}
		protected void PopulateSearch()
		{
			Collection<IProductSearchCriteria> searchCriteriesInclusive = new Collection<IProductSearchCriteria>();
			Collection<IProductSearchCriteria> searchCriteriesExclusive = new Collection<IProductSearchCriteria>();

			searchCriteriesInclusive.Add(CriteriaDataSourceViewState.ProductSearchCriteria);

			//Handle multiple ERP-ids
			foreach (var criteria in searchCriteriesInclusive)
			{
				if (!criteria.ErpId.IsNullOrEmpty())
					criteria.ErpId = criteria.ErpId.Replace(" ", "").Replace(Environment.NewLine, ",").Replace(";", ",").Replace("\t", ",").Replace("\n", ",");

				if (!criteria.Title.IsNullOrEmpty())
					criteria.Title = CleanForFullTextSearch(criteria.Title);
			}

			ProductObjectDataSource.SelectParameters.Clear();
			ProductObjectDataSource.SelectParameters.Add("maximumRows", SearchResultsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			ProductObjectDataSource.SelectParameters.Add("startRowIndex", (SearchResultsGrid.PageIndex * SearchResultsGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			ProductObjectDataSource.SelectParameters.Add("searchCriteriaInclusive", ProductDataSource.ConvertToXml(searchCriteriesInclusive));
			ProductObjectDataSource.SelectParameters.Add("searchCriteriaExclusive", ProductDataSource.ConvertToXml(searchCriteriesExclusive));
			Cache.Remove(ProductObjectDataSource.CacheKeyDependency);
			Cache[ProductObjectDataSource.CacheKeyDependency] = new object();
		}

		private string CleanForFullTextSearch(string searchString)
		{
			var splitted = searchString.Replace("-", " ").Split(' ');
			if (splitted.Length > 1)
			{
				var newTitle = new string[splitted.Count()];

				var i = 0;
				foreach (var word in splitted)
				{
					if (word.Length > 1)
					{
						newTitle[i] = word;
						i++;
					}
				}
				return string.Join(" ", newTitle).Trim();
			}
			return searchString;
		}

		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		protected virtual string ResolveStatusTitle(int statusId)
		{
			if (_productStatuses == null)
			{
				var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
				_productStatuses = productStatusSecureService.GetAll();
			}
			if (_productStatuses == null)
			{
				_productStatuses = new Collection<IProductStatus>();
			}

			var productStatus = _productStatuses.First(item => item.Id == statusId).Title;

			return productStatus;
		}

		protected virtual string ResolveTypeTitle(int typeId)
		{
			if (_productTypes == null)
			{
				var productTypeSecureService = IoC.Resolve<IProductTypeSecureService>();
				_productTypes = productTypeSecureService.GetAll();
			}
			if (_productTypes == null)
			{
				_productTypes = new Collection<IProductType>();
			}

			var productType = _productTypes.First(item => item.Id == typeId).Title;

			return productType;
		}
	}
}