using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	public static class PriceListDataSource
	{
		public static Collection<IPriceListItem> SelectMethod(string productId)
		{
			IPriceListItemSecureService priceListItemSecureService = IoC.Resolve<IPriceListItemSecureService>();
			return priceListItemSecureService.GetAllByProduct(int.Parse(productId, CultureInfo.CurrentCulture));
		}
	}
}
