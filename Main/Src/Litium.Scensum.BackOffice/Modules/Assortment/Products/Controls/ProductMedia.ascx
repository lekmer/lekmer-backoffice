﻿<%@ control language="C#" autoeventwireup="true" codebehind="ProductMedia.ascx.cs" inherits="Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls.ProductMedia" %>
<%@ register tagprefix="Scensum" namespace="Litium.Scensum.Web.Controls" assembly="Litium.Scensum.Web.Controls" %>
<%@ register tagprefix="ajaxToolkit" namespace="AjaxControlToolkit" assembly="AjaxControlToolkit" %>
<%@ register assembly="Litium.Scensum.Web.Controls" namespace="Litium.Scensum.Web.Controls.Tree" tagprefix="TemplatedTreeView" %>
<%@ register src="~/UserControls/Tree/NodeSelector.ascx" tagname="NodeSelect" tagprefix="uc" %>
<%@ register tagprefix="uc" tagname="ContentPageSelector" src="~/UserControls/SiteStructure/ContentPageSelector.ascx" %>
<%@ register tagprefix="uc" tagname="ProductImageSelect" src="~/UserControls/Media/ProductImageSelect.ascx" %>
<%@ register tagprefix="uc" tagname="ImageGroup" src="~/Modules/Assortment/Products/Controls/ImageGroups.ascx" %>
<%@ register src="~/UserControls/Media/FileDrop/FileGroupDrop.ascx" tagname="FileDrop" tagprefix="uc" %>
<%@ register src="~/UserControls/Media/FileDrop/AddMovie.ascx" tagname="AddMovie" tagprefix="uc" %>
<script type="text/javascript">
	function ResetDefaultMediaMessages() {
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
	}
	function check(obj) {

		if (obj.className == "button-uncheck") {
			var obj2 = document.querySelector(".button-check");
			obj.className = "button-check";
			obj2.className = "button-uncheck";

			var movie = document.getElementById("moviedropcontainer");
			var media = document.getElementById("mediadropcontainer");
			if (movie.style.display == "none") {
				movie.style.display = "inline";
				media.style.display = "none";
			} else {
				media.style.display = "block";
				movie.style.display = "none";
			}

		}

	}

	
</script>

<asp:updatepanel id="ImagesUpdatePanel" runat="server" updatemode="Conditional">
	<ContentTemplate>
	<div>
	<div class="prodduct-image">
		<div class="left">
			<div class="input-box">
			<span class="bold"><%= Resources.Product.Literal_DefaultImage %></span>
				<div class="default-image">										        
                    <div id="image-show">
                        <a id="ZoomProductImageAnchor"  runat="server" target="blank">
                            <asp:Image ID="DefaultImage"  ImageUrl="~/Media/Images/Assortment/defaultProduct.jpg" runat="server" />											    
					        <img ID="ZoomProductImage"  src="~/Media/Images/Common/zoom.png" runat="server" class="zoom-image" />
					    </a> 
                    </div>
				</div>
			</div>
		</div>
		<div class="left">
		<div >
		<div class="left default-image-first-column" >
				
				<span class="bold"><%= Resources.General.Literal_Title %></span>	<br />	
				<uc:LiteralEncoded ID="ImageTitleLiteral"  runat="server" ></uc:LiteralEncoded>
				<br />		<br />							
				<span class="bold"><%= Resources.General.Literal_AlternativeText %></span>	<br />		
				<uc:LiteralEncoded ID="ImageAlternativeTextLiteral"  runat="server" ></uc:LiteralEncoded>
				<br />								
			
		</div>
			<div class="left default-image-second-column">
			
				<span class="bold"><%= Resources.General.Literal_FileExtension %></span>	<br />		
				<asp:Label ID="ImageFileExtensionLabel" runat="server"  ></asp:Label>
				<br />	<br />	
			
				<span class="bold"><%= Resources.General.Literal_Dimensions %></span>	<br />		
				<asp:Label ID="ImageDimensionsLabel" runat="server" ></asp:Label>
				<br />								
			<br />	
		  </div>
		  </div>
		  <div class="left default-image-bottom">
		  <br/>
		    <uc:ImageLinkButton  runat="server" ID="ImgBrowseButton" Text="<%$ Resources:Product,Button_ChangeImage %>" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();"/>									
			<uc:ImageLinkButton  runat="server" ID="DeleteImageButton" Text="<%$ Resources:Product,Button_DeleteImage %>"  SkinID="DefaultButton" />
		</div>	
		 </div>
	</div>
	<ajaxToolkit:ModalPopupExtender ID="ImagesPopup" runat="server" TargetControlID="ImgBrowseButton" PopupControlID="ImagesDiv" 
		BackgroundCssClass="popup-background" CancelControlID="_inpCloseImages" OnCancelScript="ResetDefaultMediaMessages();">
    </ajaxToolkit:ModalPopupExtender>
 
</div>
</ContentTemplate>
</asp:updatepanel>

<asp:panel id="UploadPanel" runat="server">
		<br /><br />
	<div class="mediaradiobutton">
								 
					 
					  <asp:RadioButton GroupName="Group1" id="UploadMediaButton"  value="true" Checked runat="server" />
					
					<asp:label AssociatedControlID="UploadMediaButton" id="UploadMediaButtonLabel" runat="server" CssClass="button-check" Text="<%$ Resources:Product,Button_UploadMedia %>" onmousedown="check(this)">
					
					</asp:label>
					 <asp:RadioButton  GroupName="Group1" id="UploadMovieButton" value="false" runat="server" onmousedown="clickCheck(this)"/>
					<asp:label AssociatedControlID="UploadMovieButton" id="UploadMovieButtonLabel" CssClass="button-uncheck" runat="server" Text="<%$ Resources:Product,Button_UploadMovie %>" onmousedown="check(this)">
						
					</asp:label>
					 </div>

		<div class="column">
			<br />
			<div id ="mediadropcontainer">
			<uc:FileDrop ID="FileDropControl" runat="server" UseRootNodeInPath="true" />
				</div>

		</div>
	</asp:panel>
<asp:panel id="MoviePanel" runat="server" >
				<div >
			<br />
					<div id ="moviedropcontainer" style="display: none">
			<uc:AddMovie ID="AddMovieControl" runat="server"  />
				</div>
		</div>
	</asp:panel>
<div id="ImagesDiv" runat="server" class="product-popup-images-container" style="z-index: 10010; display: none;">
	<div id="product-popup-images-header">
		<div id="product-popup-images-header-left">
		</div>
		<div id="product-popup-images-header-center">
			<span><%= Resources.Product.Literal_AddImages %></span>
			<input type="button" id="_inpCloseImages" class="_inpClose" value="x" />
		</div>
		<div id="product-popup-images-header-right">
		</div>
	</div>
	<br clear="all" />
	<div class="product-popup-images-body">
		<uc:productimageselect id="ImageSelectControl" runat="server" />
	</div>
</div>
