﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Ratings
{
	public partial class RatingEdit : LekmerStatePageController<RatingState>, IEditor
	{
		private List<string> _validationMessage = new List<string>();

		protected virtual RatingsMaster MasterPage
		{
			get
			{
				return (RatingsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderSelector.NodeCommand += OnFolderSelectorCommand;
			FolderSelector.SelectedFolderChangedEvent += OnSelectedFolderChanged;
			RatingItemForm.SaveEvent += OnRatingItemSave;
			RatingItemsGrid.RowCommand += OnRowCommand;
			RatingItemsGrid.PageIndexChanging += OnPageIndexChanging;

			RatingDescriptionTranslator.TriggerImageButton = RatingDescriptionTranslateButton;
		}

		protected override void PopulateForm()
		{
			InitializeRatingRegistryList();

			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var rating = IoC.Resolve<IRatingSecureService>().GetById(id.Value);
				TitleTextBox.Text = rating.Title;
				CommonNameTextBox.Text = rating.CommonName;
				DescriptionTextBox.Text = rating.Description ?? string.Empty;
				RatingRegistryList.SelectedValue = rating.RatingRegistryId.ToString(CultureInfo.CurrentCulture);
				CommonForVariantsCheckBox.Checked = rating.CommonForVariants;
				CommonForVariantsCheckBox.Enabled = false;

				InitializeState(rating.RatingFolderId);

				PopulateRatingItems(rating);

				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_EditRating);
			}
			else
			{
				InitializeState(null);

				PopulateRatingItemsGridDefault();

				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_CreateRating);
			}

			PopulatePath(State.FolderId);
			PopulateTranslations(id);

			MasterPage.SelectedFolderId = State.FolderId;
			MasterPage.PopulateTree(State.FolderId);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.RatingReview.RatingSaveSuccess);
				}
			}

			MasterPage.SetupTabAndPanel();
		}


		public void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				MasterPage.RedirectToDefaultPage();
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}

				SystemMessageContainer.MessageType = InfoType.Warning;

				return;
			}

			int? id = GetIdOrNull();
			
			var ratingSecureService = IoC.Resolve<IRatingSecureService>();

			var rating = id.HasValue ? ratingSecureService.GetById(id.Value) : IoC.Resolve<IRating>();
			rating.Title = TitleTextBox.Text;
			rating.CommonName = CommonNameTextBox.Text;
			rating.Description = DescriptionTextBox.Text;
			rating.RatingFolderId = State.FolderId ?? -1;
			rating.RatingRegistryId = int.Parse(RatingRegistryList.SelectedValue, CultureInfo.CurrentCulture);

			if (!id.HasValue)
			{
				rating.CommonForVariants = CommonForVariantsCheckBox.Checked;
			}

			if (rating.RatingItems == null)
			{
				rating.RatingItems = new Collection<IRatingItem>();
			}

			rating.RatingItems.Clear();

			if (State.RatingStateItems != null)
			{
				foreach (var ratingItemState in State.RatingStateItems)
				{
					rating.RatingItems.Add(ratingItemState.RatingItem);
				}
			}

			try
			{
				rating = ratingSecureService.Save(SignInHelper.SignedInSystemUser, rating, RatingTitleTranslator.GetTranslations(), RatingDescriptionTranslator.GetTranslations());
			}
			catch
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingUpdateFailed);
				return;
			}

			if (rating.Id == -1)
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingTitleExist);
			}
			else if (rating.Id == -2)
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingCommonNameExist);
			}
			else
			{
				MasterPage.SelectedFolderId = State.FolderId;
				MasterPage.PopulateTree(State.FolderId);
				MasterPage.UpdateBreadcrumbs(State.FolderId);

				Response.Redirect(LekmerPathHelper.Rating.GetEditUrlWithMessage(rating.Id));
			}
		}


		protected virtual void OnFolderSelectorCommand(object sender, CommandEventArgs e)
		{
			FolderSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			FolderSelector.DataBind();
		}

		protected virtual void OnSelectedFolderChanged(object sender, UserControls.Media.Events.SelectedFolderChangedEventArgs e)
		{
			if (e.SelectedFolderId != null)
			{
				State.FolderId = e.SelectedFolderId.Value;
			}
		}

		protected virtual void OnRatingItemSave(object sender, UserControls.Assortment.Events.RatingItemSelectEventArgs e)
		{
			var ratingStateItem = e.RatingStateItem;

			if (State.RatingStateItems == null)
			{
				State.RatingStateItems = new List<RatingStateItem>();
			}

			var existedRatingStateItem = State.RatingStateItems.FirstOrDefault(rsi => rsi.Id == ratingStateItem.Id);
			if (existedRatingStateItem == null)
			{
				State.RatingStateItems.Add(ratingStateItem);
			}
			else
			{
				existedRatingStateItem.RatingItem = ratingStateItem.RatingItem;
			}

			PopulateRatingItemsGrid();
		}

		protected virtual void OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			string id = e.CommandArgument.ToString();

			switch (e.CommandName)
			{
				case "DeleteRatingItem":
					var ratingItemToDelate = State.RatingStateItems.FirstOrDefault(rsi => rsi.Id == id);
					if (ratingItemToDelate != null)
					{
						State.RatingStateItems.Remove(ratingItemToDelate);
						PopulateRatingItemsGrid();
					}
					break;

				case "EditRatingItem":
					var ratingItemToEdit = State.RatingStateItems.FirstOrDefault(rsi => rsi.Id == id);
					if (ratingItemToEdit != null)
					{
						RatingItemForm.Populate(ratingItemToEdit);
						RatingItemForm.Popup.Show();
					}
					break;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingItemsGrid.PageIndex = e.NewPageIndex;

			PopulateRatingItemsGrid();
		}


		protected virtual void SortRatingItemsByScore()
		{
			// Sort by score.
			State.RatingStateItems.Sort(delegate(RatingStateItem element1, RatingStateItem element2)
			{
				if (element1.RatingItem.Score < element2.RatingItem.Score)
				{
					return -1;
				}

				if (element1.RatingItem.Score > element2.RatingItem.Score)
				{
					return 1;
				}

				return 0;
			});
		}

		protected virtual void InitializeRatingRegistryList()
		{
			RatingRegistryList.DataSource = IoC.Resolve<IRatingRegistrySecureService>().GetAll();
			RatingRegistryList.DataValueField = "Id";
			RatingRegistryList.DataTextField = "Title";
			RatingRegistryList.DataBind();
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			return IoC.Resolve<IRatingFolderSecureService>().GetTree(folderId);
		}

		protected virtual void PopulatePath(int? ratingFolderId)
		{
			FolderSelector.SelectedNodeId = ratingFolderId;

			FolderSelector.DataSource = GetPathSource(ratingFolderId);
			FolderSelector.DataBind();

			FolderSelector.PopulatePath(ratingFolderId);
		}

		protected virtual void InitializeState(int? ratingFolderId)
		{
			if (State == null)
			{
				State = new RatingState();
			}

			if(ratingFolderId.HasValue)
			{
				State.FolderId = ratingFolderId;
			}
			else
			{
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					State.FolderId = MasterPage.SelectedFolderId.Value;
				}
			}
		}

		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidTitle())
			{
				_validationMessage.Add(Resources.RatingReview.TitleEmpty);
				isValid = false;
			}

			if (!IsValidCommonName())
			{
				_validationMessage.Add(Resources.RatingReview.CommonNameEmpty);
				isValid = false;
			}

			if (!IsValidFolder())
			{
				_validationMessage.Add(Resources.RatingReview.RatingFolderEmpty);
				isValid = false;
			}

			if (!IsValidRegistry())
			{
				_validationMessage.Add(Resources.RatingReview.RatingRegistryEmpty);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidTitle()
		{
			string title = TitleTextBox.Text.Trim();

			if (string.IsNullOrEmpty(title))
			{
				return false;
			}

			return title.Length <= 50;
		}

		protected virtual bool IsValidCommonName()
		{
			string commonName = CommonNameTextBox.Text.Trim();

			if (string.IsNullOrEmpty(commonName))
			{
				return false;
			}

			return commonName.Length <= 50;
		}

		protected virtual bool IsValidFolder()
		{
			return State.FolderId > 0;
		}

		protected virtual bool IsValidRegistry()
		{
			return int.Parse(RatingRegistryList.SelectedValue, CultureInfo.CurrentCulture) > 0;
		}

		protected virtual void PopulateTranslations(int? id)
		{
			RatingTitleTranslator.DefaultValueControlClientId = TitleTextBox.ClientID;
			RatingDescriptionTranslator.DefaultValueControlClientId = DescriptionTextBox.ClientID;

			if (id.HasValue)
			{
				var ratingSecureService = IoC.Resolve<IRatingSecureService>();

				RatingTitleTranslator.BusinessObjectId = id.Value;
				RatingDescriptionTranslator.BusinessObjectId = id.Value;

				RatingTitleTranslator.DataSource = ratingSecureService.GetAllTitleTranslations(id.Value);
				RatingDescriptionTranslator.DataSource = ratingSecureService.GetAllDescriptionTranslations(id.Value);
			}
			else
			{
				RatingTitleTranslator.BusinessObjectId = 0;
				RatingDescriptionTranslator.BusinessObjectId = 0;

				RatingTitleTranslator.DataSource = new Collection<ITranslationGeneric>();
				RatingDescriptionTranslator.DataSource = new Collection<ITranslationGeneric>();
			}

			RatingTitleTranslator.DataBind();
			RatingDescriptionTranslator.DataBind();
		}

		protected virtual void PopulateRatingItems(IRating rating)
		{
			var ratingItemSecureService = IoC.Resolve<IRatingItemSecureService>();

			if (State.RatingStateItems == null)
			{
				State.RatingStateItems = new List<RatingStateItem>();
			}

			foreach (var ratingItem in rating.RatingItems)
			{
				ratingItem.TitleTranslations = ratingItemSecureService.GetAllTitleTranslations(ratingItem.Id);
				ratingItem.TitleTranslations.ReplaceEmptyStringValuesWithNull();

				ratingItem.DescriptionTranslations = ratingItemSecureService.GetAllDescriptionTranslations(ratingItem.Id);
				ratingItem.DescriptionTranslations.ReplaceEmptyStringValuesWithNull();

				var ratingItemState = new RatingStateItem
				{
					Id = ratingItem.Id.ToString(CultureInfo.InvariantCulture),
					RatingItem = ratingItem
				};

				State.RatingStateItems.Add(ratingItemState);
			}

			PopulateRatingItemsGrid();
		}

		protected virtual void PopulateRatingItemsGrid()
		{
			SortRatingItemsByScore();

			RatingItemsGrid.DataSource = State.RatingStateItems;
			RatingItemsGrid.DataBind();

			RatingItemsUpdatePanel.Update();
		}

		protected virtual void PopulateRatingItemsGridDefault()
		{
			RatingItemsGrid.DataSource = null;
			RatingItemsGrid.DataBind();
		}

		protected virtual string GetRatingItemTitle(object originalTitle)
		{
			if (((string)originalTitle).IsNullOrEmpty())
			{
				return Resources.RatingReview.Literal_RatingItemEmptyTitle;
			}

			return (string)originalTitle;
		}
	}

	[Serializable]
	public sealed class RatingState
	{
		public int? FolderId { get; set; }
		public List<RatingStateItem> RatingStateItems { get; set; }
	}

	[Serializable]
	public class RatingStateItem
	{
		public string Id { get; set; }
		public IRatingItem RatingItem { get; set; }
	}
}