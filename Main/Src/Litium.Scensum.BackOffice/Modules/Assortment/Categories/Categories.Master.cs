using System;
using System.Globalization;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Categories
{
	public partial class Categories : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CategoriesTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack)
			{
				PopulateTreeView(GetIdOrOrNull() ?? CategoriesTree.SelectedNodeId);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			ScriptManager.RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", CategoriesTree.ClientID), true);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					CategoriesTree.DenySelection = true;
					break;
				case "Navigate":
					Response.Redirect(PathHelper.Assortment.Category.GetEditUrl(e.Id));
					break;
			}
		}

		protected virtual void PopulateTreeView(int? itemId)
		{
			var id = itemId != CategoriesTree.RootNodeId ? itemId : null;
			CategoriesTree.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(id);
			CategoriesTree.RootNodeTitle = Resources.Product.Literal_Category;
			CategoriesTree.DataBind();
			CategoriesTree.SelectedNodeId = itemId ?? CategoriesTree.RootNodeId;

			if (!GetIdOrOrNull().HasValue || GetIdOrOrNull().Value != CategoriesTree.SelectedNodeId)
			{
				CategoriesTree.DenySelection = true;
			}
		}
		
		protected virtual int? GetIdOrOrNull()
		{
			return Request.QueryString.GetInt32OrNull("Id");
		}

		public void UpdateMasterTree(int selectedNodeId)
		{
			PopulateTreeView(selectedNodeId);
			LeftUpdatePanel.Update();
		}
	}
}
