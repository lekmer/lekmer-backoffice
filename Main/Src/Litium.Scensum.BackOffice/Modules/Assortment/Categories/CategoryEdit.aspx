<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/Categories/Categories.Master" AutoEventWireup="false" CodeBehind="CategoryEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Categories.CategoryEdit" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>

<%@ Import Namespace="Litium.Scensum.BackOffice.Controller" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="RatingGroupSelector" Src="~/UserControls/Assortment/RatingGroupSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="RegistryRestriction" Src="~/UserControls/Assortment/RegistryRestriction.ascx" %>
<%@ Register TagPrefix="uc" TagName="DeliveryTimeConfigurator" Src="~/UserControls/Assortment/DeliveryTimeConfigurator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
		<ContentTemplate>
			<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" ForeColor="Black" CssClass="advance-validation-summary attention-message warning" DisplayMode="List" ValidationGroup="CategoryValidationGroup"/>
			<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Success" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content runat="server" ID="categoryEditContent" ContentPlaceHolderID="CategoriesContentPlaceHolder">
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript">
		$(document).ready(function () { $('#tabs').tabs(); });

		function submitForm() {
			tinyMCE.triggerSave();
		}

		function ConfirmRatingGroupDelete() {
			return DeleteConfirmation("<%= Resources.RatingReview.Literal_RatingGroup %>");
		}

		$(function () {
			$('.multi-sizes-inherit input').click(function () {
				if ($(this).is(':checked')) {
					$('.multi-sizes-allow input, .multi-sizes-disallow input').removeAttr('checked');
				}
				else {
					$('.multi-sizes-disallow input').attr('checked', 'checked');
				}
			});
			$('.multi-sizes-allow input, .multi-sizes-disallow input').click(function () {
				$('.multi-sizes-inherit input').removeAttr('checked');
			});
		});
	</script>

	<asp:Label ID="CategoryTitleLabel" runat="server" CssClass="assortment-header" />

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div id="categories-block">
			<div id="tabs">
				<ul>
					<li><a href="#fragment-1"><span><%= Resources.Product.Tab_ProductEdit_Info %></span></a></li>
					<li><a href="#fragment-2"><span><%= Resources.Product.Tab_ProductEdit_SiteStructure %></span></a></li>
					<li><a href="#fragment-3"><span><%= Resources.Lekmer.Tab_ProductEdit_Settings %></span></a></li>
				</ul>
				<br clear="all" />
				<div class="category-tabs-main-container" id="tabs-main-container">
					<div id="fragment-1">
						<div class="headBlock">
							<div class="headColumn">
								<div class="input-box">
									<span><%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp;
									<asp:RequiredFieldValidator runat="server" ID="CategoryTitleValidator" ControlToValidate="CategoryTitleTextBox"
										Display="None" ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" ValidationGroup="CategoryValidationGroup" />
									<uc:GenericTranslator ID="CategoryTitleTranslator" runat="server" />
									<br />
									<asp:TextBox ID="CategoryTitleTextBox" runat="server" Width="100%"/>
								</div>
								<div class="input-box">
									<span><%= Resources.General.Literal_ErpId %></span>
									<br />
									<asp:TextBox ID="CategoryErpIdTextBox" runat="server" ReadOnly="true" Width="100%"/>
								</div>
								<div class="input-box">
									<span><%= Resources.Lekmer.Literal_MonitorThreshold %></span>&nbsp;
									<asp:RangeValidator ID="MonitorThresholdRangeValidator" runat="server" ControlToValidate="MonitorThresholdBox"
										Display="None" ErrorMessage="<%$ Resources:LekmerMessage, MonitorThresholdIncorrect %>" 
										MinimumValue="1" MaximumValue="1000" Type="Integer" ValidationGroup="CategoryValidationGroup">
									</asp:RangeValidator>
									<br />
									<asp:TextBox ID="MonitorThresholdBox" runat="server" Width="100%"/>
								</div>
								<div class="input-box">
									<span><%= Resources.Lekmer.Literal_MaxQuantityPerOrder %></span>&nbsp;
									<asp:RangeValidator ID="MaxQuantityPerOrderRangeValidator" runat="server" ControlToValidate="MaxQuantityPerOrderBox"
										Display="None" ErrorMessage="<%$ Resources:LekmerMessage, MaxQuantityPerOrderIncorrect %>" 
										MinimumValue="1" MaximumValue="1000000000" Type="Integer" ValidationGroup="CategoryValidationGroup">
									</asp:RangeValidator>
									<br />
									<asp:TextBox ID="MaxQuantityPerOrderBox" runat="server" Width="100%"/>
								</div>
							</div>
							<div class="headColumn" style="padding-left: 20px;">
								<asp:Panel ID="MultipleSizesPanel" runat="server" GroupingText="<%$ Resources:Product,Literal_MultipleSizes %>" style="padding-top: 20px;">
									<asp:CheckBox ID="InheritMultipleSizesCheckBox" runat="server" Text="<%$ Resources:Product,Literal_InheritFromParent %>" CssClass="multi-sizes-inherit"/>
									<asp:RadioButton ID="AllowMultipleSizesRadioButton" runat="server" Text="<%$ Resources:Product,Literal_Allow %>" GroupName="MultipleSizes" CssClass="multi-sizes-allow"/>
									<asp:RadioButton ID="DisallowMultipleSizesRadioButton" runat="server" Text="<%$ Resources:Product,Literal_Disallow %>" GroupName="MultipleSizes" CssClass="multi-sizes-disallow"/>
								</asp:Panel>
								<div class="input-box">
									<div style="float:left">
										<span><%=Resources.Lekmer.Literal_DeliveryTimeFrom%></span><br />
										<asp:TextBox ID="DeliveryTimeFromBox" runat="server" Width="100px" Enabled="False" />
									</div>
									<div style="float:left; padding-left: 5px">
										<span><%=Resources.Lekmer.Literal_DeliveryTimeTo%></span><br />
										<asp:TextBox ID="DeliveryTimeToBox" runat="server" Width="100px" Enabled="False" />
									</div>
									<div style="float:left; padding-left: 5px">
										<span><%=Resources.Lekmer.Literal_DeliveryTimeFormat%></span><br />
										<asp:TextBox ID="DeliveryTimeFormatBox" runat="server" Width="100px" Enabled="False" />
										<uc:DeliveryTimeConfigurator ID="DeliveryTimeConfigurator" runat="server" />
									</div>
								</div>
							</div>
							<br class="clear" />
							<div style="width: 100%;">
								<span><%= Resources.Product.Literal_PackageInfo %></span>&nbsp
								<asp:ImageButton runat="server" ID="CategoryPackageInfoTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
								<br />
								<uc:LekmerTinyMceEditor ID="CategoryPackageInfoEditor" runat="server" SkinID="tinyMCE" />
							</div>
							<br class="clear" />
							<hr />
							<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<div id="categories-poducts" class="clear left">
										<span class="assortment-header"><%= Resources.Product.Literal_Products %></span>
										<sc:GridViewWithCustomPager ID="ProductGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%" 
											DataSourceID="CategoryProductsObjectDataSource" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>">
											<Columns>
												<asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="20%" />
												<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="75%">
													<ItemTemplate>
														<uc:HyperLinkEncoded runat="server" ID="ProductEditLink" 
															NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Product.GetEditUrlForCategoryEdit(int.Parse(Eval("Id").ToString()), GetId()) %>'
															Text='<%# Eval("DisplayTitle")%>' />
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
										</sc:GridViewWithCustomPager>
										<asp:ObjectDataSource runat="server" ID="CategoryProductsObjectDataSource" EnablePaging="true"
											SelectCountMethod="SelectCount" SelectMethod="SelectMethod"
											TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Categories.CategoryProductsDataSource">
										</asp:ObjectDataSource>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
							<br class="clear" />
							<div id="category-rating-group">
								<asp:UpdatePanel ID="RatingGroupsUpdatePanel" runat="server" UpdateMode="Conditional">
									<ContentTemplate>
										<span class="assortment-header"><%= Resources.RatingReview.Literal_RatingGroups %></span>
										<br />
										<sc:GridViewWithCustomPager ID="RatingGroupsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
											AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" Width="100%">
											<Columns>
												<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
													<HeaderTemplate>
														<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
													</HeaderTemplate>
													<ItemTemplate>
														<asp:CheckBox ID="SelectCheckBox" runat="server" />
														<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("RatingGroup.Id") %>' runat="server" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
													<ItemTemplate>
														<uc:HyperLinkEncoded ID="HyperLinkEncoded1" runat="server" Text='<%# Eval("RatingGroup.Title") %>' NavigateUrl='<%# GetRatingGropEditUrl(Eval("RatingGroup.Id")) %>'/>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
													<ItemTemplate>
														<uc:LiteralEncoded ID="LiteralEncoded1" runat="server" Text='<%# Eval("RatingGroup.CommonName") %>' />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Path">
													<ItemTemplate>
														<asp:Label ID="Label1" runat="server" Text='<%# GetPath(Eval("RatingGroup.RatingGroupFolderId")) %>' />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="<%$ Resources:RatingReview, Literal_ApplyForSubCategories %>">
													<ItemTemplate>
														<asp:CheckBox runat="server" ID="ApplyForSubCategoriesCheckBox" Checked='<%# Eval("RatingGroupCategory.IncludeSubCategories") %>' CssClass="exclude-selection-function" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
													<ItemTemplate>
														<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteGroup" CommandArgument='<%# Eval("RatingGroup.Id") %>'
															ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return ConfirmRatingGroupDelete();" />
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
										</sc:GridViewWithCustomPager>
										<div id="AllSelectedDiv" runat="server" style="display: none;" class="apply-to-all-selected-2">
											<uc:ImageLinkButton runat="server" ID="RemoveRatingGroupsButton"
												Text="<%$ Resources:General, Button_RemoveSelected %>" SkinID="DefaultButton" OnClientClick="return ConfirmRatingGroupDelete();" />
										</div>
										<div class="right">
											<uc:RatingGroupSelector ID="RatingGroupSelector" runat="server" />
										</div>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
						</div>
					</div>
					<div id="fragment-2">
						<asp:UpdatePanel ID="CategoriesUpdatePanel" runat="server" UpdateMode="Conditional" >
							<ContentTemplate>
								<div id="category-parent-content-node" style="padding-top: 10px;">
									<span class="bold"><%= Resources.General.Literal_SiteStructureRegistryList %></span>
									<asp:GridView ID="RegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
										<Columns>
											<asp:TemplateField HeaderText="<%$ Resources:General,Literal_SiteStructureRegistry %>">
												<ItemTemplate>
													<asp:HiddenField ID="RegistryIdHiddenField" runat="server" Value='<%#Eval("SiteStructureRegistryId")%>' />
													<asp:Label ID="RegistryTitleLabel" runat="server"/>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General,Literal_ParentContentNode %>">
												<ItemTemplate>
													<uc:ContentPageSelector ID="ParentContentNodeSelector" runat="server" AllowClearSelection="true" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General,Literal_TemplateContentNode%>">
												<ItemTemplate>
													<uc:ContentPageSelector ID="TemplateContentNodeSelector" runat="server" OnlyProductPagesSelectable="true" AllowClearSelection="true" />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</div>
							</ContentTemplate>
						</asp:UpdatePanel>
						<br class="clear" />
						<uc:RegistryRestriction ID="ProductRegistryRestriction" runat="server" Width="100%" />
					</div>
					<div id="fragment-3">
						<div id="default-tag-groups" style="margin-top: 10px;">
								<asp:UpdatePanel ID="DefaultTagGroups" runat="server" UpdateMode="Conditional">
									<ContentTemplate>
										<span class="assortment-header"><%= Resources.LekmerMessage.CategoryEdit_DefaultTagGroups %></span>
										<div>
											<div class="user-roles-list-box" style="width:190px;">
												<asp:ListBox id="AvailableTagGroupsListBox" DataTextField="Title" DataValueField="Id"
													Rows="10"
													Width="185px"
													Height="180px"
													SelectionMode="Multiple"
													runat="server">
												</asp:ListBox>
											</div>
											<div class="user-roles-button" style="width:38px;">
												<uc:ImageLinkButton ID="AddTagGroupButton" runat="server" Text=">>" SkinID="DefaultButton" />
												<br />
												<br />
												<uc:ImageLinkButton ID="RemoveTagGroupButton" runat="server" Text="<<" SkinID="DefaultButton" />
											</div>
											<div class="user-roles-list-box" style="width:190px;">
												<asp:ListBox id="SelectedTagGroupsListBox" DataTextField="Title" DataValueField="Id"
													Rows="10"
													Width="185px"
													Height="180px"
													SelectionMode="Multiple"
													runat="server">
												</asp:ListBox>
											</div>
										</div>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
						<br class="clear" />
						<br class="clear" />
						<div style="width: 700px;">
							<span class="assortment-header"><%= Resources.Lekmer.Literal_Icons %></span>
							<br/>
							<asp:Repeater ID="IconRepeater" runat="server">
								<ItemTemplate>
									<div class="product-icon-wrapper">
										<asp:HiddenField ID="IconIdHidden" runat="server" Value='<%# Eval("Id")%>' />
										<asp:CheckBox ID="IconCheckBox" runat="server" />
										<asp:Image ID="IconImage" runat="server" ImageAlign="AbsMiddle" CssClass="product-icon-img"
											ImageUrl='<%# ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(Convert.ToInt32(Eval("Image.Id")), Eval("Image.FormatExtension").ToString())) %>' />
									</div>
								</ItemTemplate>
							</asp:Repeater>
						</div>
					</div>
				</div>

				<br class="clear" />
				<br class="clear" />

				<div id="categories-buttons" class="buttons right">
					<uc:ImageLinkButton UseSubmitBehaviour="true" runat="server" ID="SaveButton"
						Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" ValidationGroup="CategoryValidationGroup" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" runat="server" ID="CancelButton"
						Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
				</div>
			</div>
			<br clear="all" />
		</div>
	</asp:Panel>
	<uc:GenericWysiwygTranslator ID="CategoryPackageInfoTranslator" runat="server" />
</asp:Content>