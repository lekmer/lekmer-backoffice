using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Variations
{
	public partial class VariationGroupEdit : LekmerPageController,IEditor
	{
		private const string SelectedTabClass = "ui-tabs-selected";

		protected virtual bool? IsSuccessMessage()
		{
			return Request.QueryString.GetBooleanOrNull("HasMessage");
		}
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
            VariantsListControl.VariationGroupId = GetId();
			VariantsListControl.VariationTypes.Clear();
		    VariantsListControl.VariationTypes.AddRange(VariationTypeListControl.GetVariationTypeWrappers());
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			CreateVariationGroupButton.Click += CreateVariationGroupClick;
            CriteriaTabButton.Click += CriteriaTabButtonClick;
			VariantsTabButton.Click += VariantsTabButtonClick;
		    DeleteButton.Click += BtnDelete_Click;
		}
		protected override void PopulateForm()
		{
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			IVariationGroup variationGroup = variationGroupSecureService.GetById(GetId());
			VariationGroupTitle.Text = variationGroup.Title;
			VariantsListControl.DefaultProductId = variationGroup.DefaultProductId;
			var variationTypeSecureService = IoC.Resolve<IVariationTypeFullSecureService>();

			VariationTypeListControl.SetDataSource(variationTypeSecureService.GetAllByVariationGroup(GetId()));
			VariationTypeListControl.BindCriteria();

			PopulateStatusList(variationGroup.VariationGroupStatusId);

            SetVariationGroupBreadcrumbPath(variationGroup);

			if (IsSuccessMessage() == null || !((bool)IsSuccessMessage())) return;
			ShowSuccessMessage();
		}

        private void SetVariationGroupBreadcrumbPath(IVariationGroup variationGroup)
        {
            CaptionLabel.Text = variationGroup.Title;
        }

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Assortment.Variation.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!VariationTypeListControl.IsValid())
			{
				return;
			}
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			IVariationGroup variationGroup = variationGroupSecureService.GetById(GetId());
			if (variationGroup == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}
			variationGroup.VariationGroupStatusId = System.Convert.ToInt32(StatusList.SelectedItem.Value, CultureInfo.CurrentCulture);
			variationGroup.Title = VariationGroupTitle.Text;
			variationGroup.DefaultProductId = VariantsListControl.DefaultProductId;
			Collection<IVariationTypeFull> variations = VariationTypeListControl.GetVariationTypes();

			
			if (VariantsListControl.Products.Count == 0)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.AddProductToVariantsGroup);
				return;
			}

			Collection<IProductVariation> productVariations = VariantsListControl.ProductVariations;
			IProductVariation productVariation = productVariations.FirstOrDefault(variant => VariantsListControl.DefaultProductId == variant.ProductId);
			if (productVariation == null)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.SelectCriteriaForPproductOrChangeProduct);
				return;
			}

			int variationGroupSecureId = variationGroupSecureService.Save(SignInHelper.SignedInSystemUser, variationGroup, variations, productVariations);
			if (variationGroupSecureId == -1)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.VariationGroupTitleExists);
				return;
			}

            SetVariationGroupBreadcrumbPath(variationGroup);
            
            ShowSuccessMessage();
		}

		protected virtual void CriteriaTabButtonClick(object sender, EventArgs e)
		{
			CriteriaDiv.Visible = true;
			VariantsDiv.Visible = false;

			SetTabsStyle(SelectedTabClass, "");
		}
		protected virtual void VariantsTabButtonClick(object sender, EventArgs e)
		{
			if (VariationTypeListControl.IsValid())
			{
				VariantsListControl.VariationTypes.Clear();
				VariantsListControl.VariationTypes.AddRange(VariationTypeListControl.GetVariationTypeWrappers());
				CriteriaDiv.Visible = false;
				VariantsDiv.Visible = true;

				SetTabsStyle("", SelectedTabClass);
			}
			else
			{
				SetTabsStyle(SelectedTabClass, "");
				SystemMessageContainer.Add(Resources.ProductMessage.VariationTypeNotCorrectValue);	
			}
		}
		protected virtual void CreateVariationGroupClick(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.Assortment.Variation.VariationGroup.GetCreateUrl());
        }

		protected void PopulateStatusList(int statusId)
		{
			var productStatusSecureService = IoC.Resolve<IVariationGroupStatusSecureService>();
			StatusList.DataSource = productStatusSecureService.GetAll();
			StatusList.DataBind();
			ListItem item = StatusList.Items.FindByValue(statusId.ToString(CultureInfo.CurrentCulture));
			if (item != null)
			{
				item.Selected = true;
			}
		}
		protected void SetTabsStyle(string criteriaTabClass, string variationTabClass)
		{
			CriteriaLi.Attributes.Add("class", criteriaTabClass);
			VariationLi.Attributes.Add("class", variationTabClass);
		}
		private void ShowSuccessMessage()
		{
			SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessVariationGroup);
			SystemMessageContainer.MessageType = InfoType.Success;
		}

        protected virtual void BtnDelete_Click(object sender, EventArgs e)
        {
            IoC.Resolve<IVariationGroupSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
            Response.Redirect(PathHelper.Assortment.Variation.GetDefaultUrl());
        }
	}
}
