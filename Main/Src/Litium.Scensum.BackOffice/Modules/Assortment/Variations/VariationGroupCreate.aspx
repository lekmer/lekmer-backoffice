<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="VariationGroupCreate.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Variations.VariationGroupCreate" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="VariationTypeList" Src="~/UserControls/Assortment/VariationTypeList.ascx" %>
<%@ Register TagPrefix="uc" TagName="VariantsList" Src="~/UserControls/Assortment/VariantsList.ascx" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="AdvancedSearchTab" ContentPlaceHolderID="body" runat="server">
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<asp:Panel ID="CreatePanel" runat="server" DefaultButton="SaveButton">
		<div id="variation-group-create">
			<Scensum:ToolBoxPanel ID="VariationPanelPanel" runat="server" Text="Variations" ShowSeparator="true">
				<div class="item" style="width: 100px; color: #CBCBCB; float: left; padding-bottom: 8px;
					padding-top: 10px; padding-left: 15px;">
					<%= Resources.General.Literal_Create %></div>
				<div style="float: left; padding-bottom: 8px; padding-top: 8px; font-size: 0.7em;
					font-weight: normal;">
					<uc:ImageLinkButton ID="CreateVariationGroupButton" runat="server" Text="<%$ Resources:Product, Button_VariationGroup %>"
						ImageUrl="~/Media/Images/Assortment/variation.gif" SkinID="HeaderPanelButton"
						UseSubmitBehaviour="false" />
				</div>
			</Scensum:ToolBoxPanel>
			<div style="float: left; width:984px; margin-bottom:-10px; ">
    		<asp:UpdatePanel ID="ErrorsUpdatePanel" runat="server" UpdateMode="Always">
					<ContentTemplate>
						<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
							runat="server" />
						<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
							ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgVariationGroup" />
					</ContentTemplate>
				</asp:UpdatePanel>
		</div>
			<div id="variation-block" class="main" style="float: left;">
				<label id="CaptionLabel" runat="server" class="variation-header">
					<%= Resources.Product.Literal_CreateVariationGroup %></label>
				<br />
				<div class="first-column">
					<div class="input-box">
						<span>
							<%= Resources.General.Literal_Title %>&nbsp; *</span>&nbsp
						<asp:RequiredFieldValidator runat="server" ID="GroupTitleValidator" ControlToValidate="TitleTextBox"
							ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgVariationGroup" />
						<br />
						<asp:TextBox ID="TitleTextBox" runat="server" CssClass="variation-field" />
					</div>
				</div>
				<div class="column">
					<div class="input-box">
						<span>
							<%= Resources.General.Literal_Status %></span>
						<br />
						<asp:DropDownList ID="StatusList" DataTextField="Title" DataValueField="Id" runat="server"
							CssClass="variation-field">
						</asp:DropDownList>
					</div>
				</div>			
				<div id="TabsVariationDiv" runat="server" class="main-tabs">
					<asp:UpdatePanel ID="TabsUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<ul runat="server" class="ui-tabs-nav ui-tabs-nav-variations">
								<li id="CriteriaLi" runat="server" class="ui-tabs-selected">
									<asp:LinkButton ID="CriteriaTabButton" runat="server" Text="<%$Resources:Product, Button_Criteria %>" />
								</li>
								<li runat="server" id="VariationLi">
									<asp:LinkButton ID="VariantsTabButton" runat="server" Text="<%$Resources:Product, Button_Variants %>" />
								</li>
							</ul>
							<div id="CriteriaDiv" class="variation-group-criteria tab ui-tabs-panel ui-tabs-panel-variations"
								runat="server">
								<uc:VariationTypeList ID="VariationTypeListControl" runat="server" ValidationGroup="vgVariationGroup" />
							</div>
							<div id="VariantsDiv" class="variation-group-variants tab ui-tabs-panel ui-tabs-panel-variations"
								runat="server" visible="false">
								<uc:VariantsList runat="server" ID="VariantsListControl" />
							</div>
						</ContentTemplate>
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="CriteriaTabButton" EventName="Click" />
							<asp:AsyncPostBackTrigger ControlID="VariantsTabButton" EventName="Click" />
						</Triggers>
					</asp:UpdatePanel>
				</div>
				<asp:UpdatePanel ID="VariationGroupUpdatePanel" runat="server">
					<ContentTemplate>
						<div id="variant-action-buttons" class="buttons right">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
								ValidationGroup="vgVariationGroup" SkinID="DefaultButton" />
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel%>"
								SkinID="DefaultButton" />
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
