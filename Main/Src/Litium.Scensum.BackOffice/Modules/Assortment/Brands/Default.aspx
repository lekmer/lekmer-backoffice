﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/Brands/BrandMaster.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Brands.Default" %>
<%@ Import Namespace="Litium.Scensum.SiteStructure"%>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="sc"  Namespace="Litium.Scensum.BackOffice.UserControls.ContextMenu" Assembly="Litium.Scensum.BackOffice"%>
    
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="BrandDefaultContent" runat="server" ContentPlaceHolderID="BrandPlaceHolder">
	<div>
		<sc:GridViewWithCustomPager
			ID="BrandGrid"
			SkinID="grid"
			runat="server"
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			DataSourceID="BrandObjectDataSource"
			Width="100%"
			PagerSettings-Mode="NumericFirstLast">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="HyperLinkEncoded1" runat="server" Text='<%# Eval("Title")%>' 
						                     NavigateUrl='<%# string.Format("BrandEdit.aspx?Id={0}", int.Parse(Eval("Id").ToString())) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:Product,Literal_BrandErpId %>" DataField="ErpId" ItemStyle-Width="5%" />
				<asp:BoundField HeaderText="<%$ Resources:Product,Literal_BrandExternalUrl %>" DataField="ExternalUrl" ItemStyle-Width="24%" />
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<ItemTemplate>
						<asp:ImageButton ID="BrandDeleteButton" runat="server" CommandName="DeleteBrand"
							CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
							OnClientClick='<%# "return DeleteConfirmation(\""+Resources.Product.Literal_DeleteBrand+"\");"%>' />
							
					</ItemTemplate>
				</asp:TemplateField>
				
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
	<asp:ObjectDataSource ID="BrandObjectDataSource" runat="server" 
	    EnablePaging="true" 
	    SelectCountMethod="SelectCount" 
	    SelectMethod="SelectMethod" 
	    TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Brands.BrandDataSource" />
</asp:Content>