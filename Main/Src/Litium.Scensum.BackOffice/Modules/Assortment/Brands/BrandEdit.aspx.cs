﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
	public partial class BrandEdit : LekmerStatePageController<BrandState>
	{
		private IIconSecureService _iconSecureService;
		protected IIconSecureService IconSecureService
		{
			get { return _iconSecureService ?? (_iconSecureService = IoC.Resolve<IIconSecureService>()); }
		}

		private IDeliveryTimeSecureService _deliveryTimeSecureService;
		protected IDeliveryTimeSecureService DeliveryTimeSecureService
		{
			get { return _deliveryTimeSecureService ?? (_deliveryTimeSecureService = IoC.Resolve<IDeliveryTimeSecureService>()); }
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			BrandDescriptionTranslator.TriggerImageButton = BrandDescriptionTranslateButton;
			BrandPackageInfoTranslator.TriggerImageButton = BrandPackageInfoTranslateButton;
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			BrandMediaControl.Selected += OnBrandMediaSelected;
			BrandMediaControl.DeleteImage += OnBrandMediaDeleted;
			SiteStructureRegistryGrid.RowDataBound += SiteStructureRegistryGridRowDataBound;
		}

		private void OnBrandMediaDeleted(object sender, EventArgs e)
		{
			SetMediaId(null);
		}

		private void OnBrandMediaSelected(object sender, ImageSelectEventArgs e)
		{
			SetMediaId(e.Image.Id);
		}

		private void SiteStructureRegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var siteStructureRegistry = (ISiteStructureRegistry)row.DataItem;
			var lblTitle = (Label)row.FindControl("TitleLabel");
			lblTitle.Text = siteStructureRegistry.Title;
			var contentNodeSelector = (LekmerContentPageSelector)row.FindControl("ContentNodeSelector");
			contentNodeSelector.SiteStructureRegistryId = siteStructureRegistry.Id;

			var brandId = GetIdOrNull();
			if (brandId.HasValue)
			{
				var brandSiteStructureRegistries = IoC.Resolve<IBrandSiteStructureRegistrySecureService>().GetAllByBrand(brandId.Value);
				var brandSiteStructureRegistry = brandSiteStructureRegistries.SingleOrDefault(r => r.SiteStructureRegistryId == siteStructureRegistry.Id);
				if (brandSiteStructureRegistry != null)
				{
					contentNodeSelector.SelectedNodeId = brandSiteStructureRegistry.ContentNodeId;
				}
			}

			contentNodeSelector.SetContentPageTitle();
		}

		protected void SetMediaId(int? id)
		{
			EnsureState();
			State.MediaId = id;
			State.SetNewImage = true;
		}

		protected void EnsureState()
		{
			if (State == null)
			{
				State = new BrandState();
			}
		}

		protected override void PopulateForm()
		{
			// If redirected after successful saving new theme -> display success message
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
				Messager.Add(GeneralMessage.SaveSuccessBrand, InfoType.Success);

			EnsureState();
			PopulateBrandInfo();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx"); //PathHelper.Assortment.Store.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			Page.Validate("BrandValidationGroup");
			if (!Page.IsValid)
			{
				return;
			}

			var brandId = GetIdOrNull();

			var service = IoC.Resolve<IBrandSecureService>();
			var brand = brandId.HasValue ? service.GetById(brandId.Value) : service.Create();

			if (brandId.HasValue && brand == null)
			{
				throw new BusinessObjectNotExistsException(brandId.Value);
			}

			brand.Title = BrandTitleBox.Text;
			brand.ExternalUrl = string.IsNullOrEmpty(BrandUrlBox.Text) ? null : BrandUrlBox.Text;
			brand.MonitorThreshold = string.IsNullOrEmpty(MonitorThresholdBox.Text)
				? null
				: (int?)int.Parse(MonitorThresholdBox.Text, CultureInfo.CurrentCulture);
			brand.MaxQuantityPerOrder = string.IsNullOrEmpty(MaxQuantityPerOrderBox.Text)
				? null
				: (int?)int.Parse(MaxQuantityPerOrderBox.Text, CultureInfo.CurrentCulture);

			string description = BrandDescriptionEditor.GetValue();
			brand.Description = description.IsEmpty() ? null : description;

			string packageInfo = BrandPackageInfoEditor.GetValue();
			brand.PackageInfo = packageInfo.IsEmpty() ? null : packageInfo;

			brand.MediaId = State.MediaId;
			brand.IsOffline = OfflineCheckBox.Checked;

			var formBrandSiteStructureRegistries = GetSiteStructureRegistries(brand);
			var productRegistryRestrictions = GetProductRegistryRestrictions();
			var offlineContentPages = GetOfflineContentPages();

			int oldDeliveryTimeId;
			bool isNeedDeleteDeliveryTime = ManageDeliveryTime(brand, out oldDeliveryTimeId);

			brand.Id = service.Save(
				SignInHelper.SignedInSystemUser,
				brand,
				formBrandSiteStructureRegistries,
				BrandTitleTranslator.GetTranslations(),
				BrandDescriptionTranslator.GetTranslations(),
				BrandPackageInfoTranslator.GetTranslations(),
				productRegistryRestrictions,
				GetSelectedIcons());

			if (brand.Id < 0)
			{
				//Saving failed because brand with the same title already exists
				Messager.Add(ProductMessage.BrandTitleExists, InfoType.Failure);
			}
			else
			{
				// Update product registry product.
				var oldRestrictions = State.RegistryRestrictionBrands.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);
				var newRestrictions = productRegistryRestrictions.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);
				UpdateProductRegistryProduct(oldRestrictions, newRestrictions, brand.Id);

				// Update content page status.
				var oldOfflineContentPages = State.BrandSiteStructureProductRegistryWrappers.Where(wrapper => wrapper.ContentNodeStatusId == (int) ContentNodeStatusInfo.Offline).ToDictionary(wrapper => wrapper.ProductRegistryId, wrapper => wrapper.ContentNodeId);
				var newOfflineContentPages = offlineContentPages.ToDictionary(w => w.ProductRegistryId, w => w.ContentNodeId);
				UpdateBrandPageStatus(oldOfflineContentPages, newOfflineContentPages);

				// Delete DeliveryTime
				if (isNeedDeleteDeliveryTime)
				{
					DeliveryTimeSecureService.Delete(oldDeliveryTimeId);
				}

				if (!brandId.HasValue)
				{
					var editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", "~/Modules/Assortment/Brands/BrandEdit.aspx?id=" + brand.Id);
					Response.Redirect(editUrl);
				}
				else
				{
					//Brand was updated successfully
					Messager.Add(GeneralMessage.SaveSuccessBrand, InfoType.Success);
					PopulateFormTitle(brand);
					PopulateProductRegistryRestrictions();
					PopulateDeliveryTimes(brand);
				}
			}
		}

		private Collection<IBrandSiteStructureRegistry> GetSiteStructureRegistries(IBrand brand)
		{
			var formBrandSiteStructureRegistries = new Collection<IBrandSiteStructureRegistry>();
			var brandSiteStructureRegistrySecureService = IoC.Resolve<IBrandSiteStructureRegistrySecureService>();
			foreach (GridViewRow row in SiteStructureRegistryGrid.Rows)
			{
				var contentNodeSelector = ((LekmerContentPageSelector)row.FindControl("ContentNodeSelector"));
				if (contentNodeSelector.SelectedNodeId.HasValue)
				{
					var brandSiteStructureRegistry = brandSiteStructureRegistrySecureService.Create();
					brandSiteStructureRegistry.SiteStructureRegistryId = contentNodeSelector.SiteStructureRegistryId;
					brandSiteStructureRegistry.BrandId = brand.Id;
					brandSiteStructureRegistry.ContentNodeId = contentNodeSelector.SelectedNodeId.Value;
					formBrandSiteStructureRegistries.Add(brandSiteStructureRegistry);
				}
			}
			return formBrandSiteStructureRegistries;
		}

		protected void PopulateBrandInfo()
		{
			var icons = new Collection<IIcon>();
			IBrand brand = null;
			
			var brandId = GetIdOrNull();
			if (!brandId.HasValue)
			{
				BrandHeader.Text = Resources.Product.Literal_CreateBrand;
			}
			else
			{
				brand = IoC.Resolve<IBrandSecureService>().GetById(brandId.Value);

				PopulateFormTitle(brand);

				BrandTitleBox.Text = brand.Title;
				BrandUrlBox.Text = brand.ExternalUrl;
				MonitorThresholdBox.Text = brand.MonitorThreshold.HasValue ? brand.MonitorThreshold.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				MaxQuantityPerOrderBox.Text = brand.MaxQuantityPerOrder.HasValue ? brand.MaxQuantityPerOrder.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				BrandDescriptionEditor.SetValue(brand.Description);
				BrandPackageInfoEditor.SetValue(brand.PackageInfo);
				OfflineCheckBox.Checked = brand.IsOffline;
				SetMediaId(brand.MediaId);
				BrandMediaControl.ImageId = brand.MediaId;

				icons = IconSecureService.GetAllByBrand(brandId.Value);
			}

			PopulateIcons(icons);
			PopulateSiteStructure();
			PopulateTranslations();
			PopulateDeliveryTimes(brand);
			PopulateProductRegistryRestrictions();
		}

		protected void PopulateFormTitle(IBrand brand)
		{
			BrandHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"",
			                                 Resources.Product.Literal_EditBrand, brand.Title);
		}

		private void PopulateSiteStructure()
		{
			SiteStructureRegistryGrid.DataSource = IoC.Resolve<ISiteStructureRegistrySecureService>().GetAll();
			SiteStructureRegistryGrid.DataBind();
		}

		private void PopulateTranslations()
		{
			var brandId = GetIdOrNull();
			var service = IoC.Resolve<IBrandSecureService>();

			BrandTitleTranslator.DefaultValueControlClientId = BrandTitleBox.ClientID;
			BrandTitleTranslator.BusinessObjectId = brandId ?? 0;
			BrandTitleTranslator.DataSource = brandId.HasValue 
				? service.GetAllTitleTranslationsByBrand(brandId.Value)
				: new Collection<ITranslationGeneric>();
			BrandTitleTranslator.DataBind();

			BrandDescriptionTranslator.DefaultValueControlClientId = BrandDescriptionEditor.ClientID;
			BrandDescriptionTranslator.BusinessObjectId = brandId ?? 0;
			BrandDescriptionTranslator.DataSource = brandId.HasValue
				? service.GetAllDescriptionTranslationsByBrand(brandId.Value)
				: new Collection<ITranslationGeneric>();
			BrandDescriptionTranslator.DataBind();

			BrandPackageInfoTranslator.DefaultValueControlClientId = BrandPackageInfoEditor.ClientID;
			BrandPackageInfoTranslator.BusinessObjectId = brandId ?? 0;
			BrandPackageInfoTranslator.DataSource = brandId.HasValue
				? service.GetAllPageInfoTranslationsByBrand(brandId.Value)
				: new Collection<ITranslationGeneric>();
			BrandPackageInfoTranslator.DataBind();
		}

		private void PopulateDeliveryTimes(IBrand brand)
		{
			var service = IoC.Resolve<IDeliveryTimeSecureService>();
			var productRegistryDeliveryTimeList = brand != null && brand.DeliveryTimeId.HasValue 
				? service.GetAllProductRegistryDeliveryTimeById(brand.DeliveryTimeId.Value)
				: new Collection<IDeliveryTime>();

			var deliveryTime = productRegistryDeliveryTimeList.FirstOrDefault(dt => dt.ChannelId == ChannelHelper.CurrentChannel.Id);
			var from = string.Empty;
			var to = string.Empty;
			var format = string.Empty;
			if (deliveryTime != null)
			{
				from = deliveryTime.FromDays.HasValue ? deliveryTime.FromDays.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				to = deliveryTime.ToDays.HasValue ? deliveryTime.ToDays.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				format = deliveryTime.Format ?? string.Empty;
			}

			DeliveryTimeFromBox.Text = from;
			DeliveryTimeToBox.Text = to;
			DeliveryTimeFormatBox.Text = format;

			DeliveryTimeConfigurator.DefaultFromValueControlClientId = DeliveryTimeFromBox.ClientID;
			DeliveryTimeConfigurator.DefaultToValueControlClientId = DeliveryTimeToBox.ClientID;
			DeliveryTimeConfigurator.DefaultFormatValueControlClientId = DeliveryTimeFormatBox.ClientID;
			DeliveryTimeConfigurator.DefaultRegistryId = deliveryTime != null && deliveryTime.ProductRegistryid.HasValue ? deliveryTime.ProductRegistryid.Value : -1;
			DeliveryTimeConfigurator.BusinessObjectId = (brand != null && brand.DeliveryTimeId.HasValue) ? brand.DeliveryTimeId.Value : 0;
			DeliveryTimeConfigurator.DataSource = productRegistryDeliveryTimeList;
			DeliveryTimeConfigurator.DataBind();
		}

		// PopulateProductRegistryRestrictions.
		private void PopulateProductRegistryRestrictions()
		{
			var brandId = GetIdOrNull();
			if (brandId.HasValue)
			{
				var registryRestrictionBrands = IoC.Resolve<IProductRegistryRestrictionBrandSecureService>().GetAllByBrand(brandId.Value);
				var brandSiteStructureProductRegistryWrappers = IoC.Resolve<IBrandSiteStructureProductRegistryWrapperSecureService>().GetAllByBrand(brandId.Value);

				EnsureState();
				State.RegistryRestrictionBrands = registryRestrictionBrands;
				State.BrandSiteStructureProductRegistryWrappers = brandSiteStructureProductRegistryWrappers;

				ProductRegistryRestriction.RegistryRestrictions = registryRestrictionBrands.ToDictionary(restriction => restriction.ProductRegistryId, restriction => restriction.RestrictionReason);
				ProductRegistryRestriction.BrandSiteStructureProductRegistryWrappersAll = new Dictionary<int, IBrandSiteStructureProductRegistryWrapper>();
				foreach (var wrapper in brandSiteStructureProductRegistryWrappers)
				{
					if (!ProductRegistryRestriction.BrandSiteStructureProductRegistryWrappersAll.ContainsKey(wrapper.ProductRegistryId))
					{
						ProductRegistryRestriction.BrandSiteStructureProductRegistryWrappersAll.Add(wrapper.ProductRegistryId, wrapper);
					}
				}
			}

			ProductRegistryRestriction.IsContentPageManage = true;
			ProductRegistryRestriction.BindData();
		}

		private Collection<IProductRegistryRestrictionItem> GetProductRegistryRestrictions()
		{
			var productRegistryRestrictionBrandList = new Collection<IProductRegistryRestrictionItem>();

			var brandId = GetIdOrNull();
			if (brandId.HasValue)
			{
				var productRegistryRestrictionBrandSecureService = IoC.Resolve<IProductRegistryRestrictionBrandSecureService>();

				var restrictions = ProductRegistryRestriction.GetRestrictions();
				foreach (var restriction in restrictions)
				{
					var productRegistryRestrictionBrand = productRegistryRestrictionBrandSecureService.Create();
					productRegistryRestrictionBrand.ProductRegistryId = restriction.Key;
					productRegistryRestrictionBrand.RestrictionReason= restriction.Value;
					productRegistryRestrictionBrand.ItemId = brandId.Value;
					productRegistryRestrictionBrand.UserId = SignInHelper.SignedInSystemUser.Id;

					productRegistryRestrictionBrandList.Add(productRegistryRestrictionBrand);
				}
			}

			return productRegistryRestrictionBrandList;
		}

		private void UpdateProductRegistryProduct(Dictionary<int, int> oldRestrictions, Dictionary<int, int> newRestrictions, int brandId)
		{
			var toRemoveRestrictions = new Dictionary<int, int>();
			var toAddRestrictions = new Dictionary<int, int>();

			foreach (var oldRestriction in oldRestrictions)
			{
				if (!newRestrictions.ContainsKey(oldRestriction.Key))
				{
					toRemoveRestrictions.Add(oldRestriction.Key, oldRestriction.Key);
				}
			}

			foreach (var newRestriction in newRestrictions)
			{
				if (!oldRestrictions.ContainsKey(newRestriction.Key))
				{
					toAddRestrictions.Add(newRestriction.Key, newRestriction.Key);
				}
			}

			var productRegistryProductSecureService = IoC.Resolve<IProductRegistryProductSecureService>();

			if (toAddRestrictions.Count > 0)
			{
				productRegistryProductSecureService.DeleteByBrand(SignInHelper.SignedInSystemUser, brandId, toAddRestrictions.Keys.ToList());
			}

			if (toRemoveRestrictions.Count > 0)
			{
				productRegistryProductSecureService.Insert(SignInHelper.SignedInSystemUser, null, brandId, null, toRemoveRestrictions.Keys.ToList());
			}
		}

		private IEnumerable<IBrandSiteStructureProductRegistryWrapper> GetOfflineContentPages()
		{
			var brandSiteStructureProductRegistryWrappers = new Collection<IBrandSiteStructureProductRegistryWrapper>();

			var brandId = GetIdOrNull();
			if (brandId.HasValue)
			{
				var brandSiteStructureProductRegistryWrapperSecureService = IoC.Resolve<IBrandSiteStructureProductRegistryWrapperSecureService>();

				var offlineContentPages = ProductRegistryRestriction.GetOfflineContentPages();
				foreach (var offlinePage in offlineContentPages)
				{
					var brandSiteStructureProductRegistryWrapper = brandSiteStructureProductRegistryWrapperSecureService.Create();
					brandSiteStructureProductRegistryWrapper.BrandId = brandId.Value;
					brandSiteStructureProductRegistryWrapper.ProductRegistryId = offlinePage.Key;
					brandSiteStructureProductRegistryWrapper.ContentNodeId = offlinePage.Value;

					brandSiteStructureProductRegistryWrappers.Add(brandSiteStructureProductRegistryWrapper);
				}
			}

			return brandSiteStructureProductRegistryWrappers;
		}

		private void UpdateBrandPageStatus(Dictionary<int, int> oldOfflineContentPages, Dictionary<int, int> newOfflineContentPages)
		{
			var toSetOnline = new Dictionary<int, int>();
			var toSetOffline = new Dictionary<int, int>();

			foreach (var oldStatus in oldOfflineContentPages)
			{
				if (!newOfflineContentPages.ContainsKey(oldStatus.Key))
				{
					toSetOnline.Add(oldStatus.Value, oldStatus.Value);
				}
			}

			foreach (var newStatus in newOfflineContentPages)
			{
				if (!oldOfflineContentPages.ContainsKey(newStatus.Key))
				{
					toSetOffline.Add(newStatus.Value, newStatus.Value);
				}
			}

			var contentNodeSecureService = (ILekmerContentNodeSecureService)IoC.Resolve<IContentNodeSecureService>();

			if (toSetOffline.Count > 0)
			{
				contentNodeSecureService.ChangeStatus(SignInHelper.SignedInSystemUser, toSetOffline.Keys.ToList(), false);
			}

			if (toSetOnline.Count > 0)
			{
				contentNodeSecureService.ChangeStatus(SignInHelper.SignedInSystemUser, toSetOnline.Keys.ToList(), true);
			}
		}

		// Icons.
		private void PopulateIcons(Collection<IIcon> icons)
		{
			var iconService = IoC.Resolve<IIconSecureService>();
			IconRepeater.DataSource = iconService.GetAll();
			IconRepeater.DataBind();

			foreach (RepeaterItem item in IconRepeater.Items)
			{
				var iconId = int.Parse(((HiddenField) item.FindControl("IconIdHidden")).Value, CultureInfo.CurrentCulture);
				var iconCheck = (CheckBox) item.FindControl("IconCheckBox");
				iconCheck.Checked = icons.Any(i => i.Id == iconId);
			}
		}

		private Collection<int> GetSelectedIcons()
		{
			var ids = new Collection<int>();
			foreach (RepeaterItem item in IconRepeater.Items)
			{
				var iconId = int.Parse(((HiddenField) item.FindControl("IconIdHidden")).Value, CultureInfo.CurrentCulture);
				var iconCheck = (CheckBox) item.FindControl("IconCheckBox");
				if (!iconCheck.Checked) continue;
				ids.Add(iconId);
			}
			return ids;
		}

		// Delivery Time
		protected virtual bool ManageDeliveryTime(IBrand brand, out int oldDeliveryTimeId)
		{
			bool isNeedDeleteDeliveryTime = false;
			oldDeliveryTimeId = 0;

			int? deliveryTimeId = brand.DeliveryTimeId;
			var deliveryTimeList = DeliveryTimeConfigurator.GetDeliveryTimeList();
			bool isRegistryDeliveryTimeExist = deliveryTimeList.FirstOrDefault(dt => dt.FromDays != null || dt.ToDays != null || !string.IsNullOrEmpty(dt.Format)) != null;
			if (!deliveryTimeId.HasValue)
			{
				if (isRegistryDeliveryTimeExist)
				{
					deliveryTimeId = DeliveryTimeSecureService.Insert();
					foreach (var deliveryTime in deliveryTimeList)
					{
						DeliveryTimeSecureService.SaveProductRegistryDeliveryTime(deliveryTimeId.Value, deliveryTime.FromDays, deliveryTime.ToDays, deliveryTime.Format, deliveryTime.ProductRegistryid);
					}
					brand.DeliveryTimeId = deliveryTimeId;
				}
			}
			else
			{
				if (isRegistryDeliveryTimeExist)
				{
					foreach (var deliveryTime in deliveryTimeList)
					{
						DeliveryTimeSecureService.SaveProductRegistryDeliveryTime(deliveryTimeId.Value, deliveryTime.FromDays, deliveryTime.ToDays, deliveryTime.Format, deliveryTime.ProductRegistryid);
					}
				}
				else
				{
					oldDeliveryTimeId = deliveryTimeId.Value;
					brand.DeliveryTimeId = null;
					isNeedDeleteDeliveryTime = true;
				}
			}

			return isNeedDeleteDeliveryTime;
		}
	}
}