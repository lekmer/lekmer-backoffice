﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="SupplierEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Suppliers.SupplierEdit" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="SupplierEditTab" ContentPlaceHolderID="body" runat="server">
	<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValidatorSummary" DisplayMode="List" ValidationGroup="SupplierValidationGroup" />
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SaveButton" runat="server" />
	<sc:ToolBoxPanel ID="SupplierToolBoxPanel" runat="server" Text="Supplier" />

	<div class="stores-place-holder">
		<div class="main-caption">
			<uc:LiteralEncoded ID="SupplierHeader" runat="server" />
		</div>

		<div class="content-box">
			<div class="column" style="width: 35%;">
				<div class="input-box">
					<span><%=Resources.Lekmer.Literal_SupplierNo%></span><br />
					<asp:TextBox ID="SupplierNoTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 35%;">
				<div class="input-box">
					<span><%=Resources.Lekmer.Literal_ActivePassiveCode%></span><br />
					<asp:TextBox ID="ActivePassiveCodeTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 30%;">
				<div class="input-box">
					<span><%=Resources.Product.Literal_Vat%></span><br />
					<asp:TextBox ID="VatTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
		</div>

		<br class="clear-all" />

		<div class="content-box">
			<div class="column" style="width: 35%;">
				<div class="input-box">
					<span><%=Resources.Customer.ColumnHeader_Phone%>1</span><br />
					<asp:TextBox ID="Phone1TextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 35%;">
				<div class="input-box">
					<span><%=Resources.Customer.ColumnHeader_Phone%>2</span><br />
					<asp:TextBox ID="Phone2TextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 30%;">
				<div class="input-box">
					<span><%=Resources.Customer.ColumnHeader_Email%></span><br />
					<asp:TextBox ID="EmailTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
		</div>

		<br class="clear-all" />

		<div class="content-box">
			<div class="column" style="width: 20%;">
				<div class="input-box">
					<span><%=Resources.Customer.Literal_City%></span><br />
					<asp:TextBox ID="CityTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 20%;">
				<div class="input-box">
					<span><%=Resources.Customer.Literal_Address%></span><br />
					<asp:TextBox ID="AddressTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 20%;">
				<div class="input-box">
					<span><%=Resources.Customer.Literal_PostalCode%></span><br />
					<asp:TextBox ID="ZipTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 20%;">
				<div class="input-box">
					<span><%=Resources.Customer.Literal_Country%></span><br />
					<asp:TextBox ID="CountryTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 20%;">
				<div class="input-box">
					<span><%=Resources.Lekmer.Literal_Language%></span><br />
					<asp:TextBox ID="LanguageTextBox" runat="server" ReadOnly="True" Width="100%" />
				</div>
			</div>
		</div>

		<div class="content-box">
			<div class="column" style="width: 20%;">
				<div class="input-box">
					<span><%=Resources.Lekmer.Literal_DropShipEmail%></span><br />
					<asp:TextBox ID="DropShipEmailTextBox" runat="server" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 10%;">
				<div class="input-box">
					<span><%=Resources.Lekmer.Literal_IsDropShip%></span><br />
					<asp:CheckBox ID="IsDropShipCheckBox" runat="server" />
				</div>
			</div>
		</div>


		<br class="clear">

		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" ValidationGroup="SupplierValidationGroup" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
		</div>
	</div>
</asp:Content>