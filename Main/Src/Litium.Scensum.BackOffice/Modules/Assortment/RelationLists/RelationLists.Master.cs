using System;
using System.Web.UI;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RelationLists
{
	public partial class RelationLists : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CreateRelationListButton.Click += CreateRelationListClick;
		}

		protected virtual void CreateRelationListClick(object sender, EventArgs e)
		{
            Response.Redirect(Controller.PathHelper.Assortment.Relation.GetEditUrl());
		}
	}
}
