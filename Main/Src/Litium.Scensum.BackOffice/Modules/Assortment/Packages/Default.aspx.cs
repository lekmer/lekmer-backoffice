﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Packages
{
	public partial class Default : LekmerStatePageController<SearchItem>
	{
		private Collection<IProductStatus> _productStatuses;
		private Collection<IProductType> _productTypes;

		private SearchItem CriteriaDataSourceViewState
		{
			get { return State ?? new SearchItem(); }
			set { State = value; }
		}

		private static SearchItem DefaultSearchItem
		{
			get
			{
				return new SearchItem
					{
						SearchType = SearchType.None,
						IsDeletable = false
					};
			}
		}

		protected override void SetEventHandlers()
		{
			SearchButton.Click += SearchButtonClick;
			PackagesGrid.RowDataBound += PackagesGridRowDataBound;
			PackagesGrid.RowCommand += PackagesGridRowCommand;
			PackagesGrid.PageIndexChanging += PackagesGridPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			State = new SearchItem();
			RestoreSearchData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!IsPostBack)
			{
				BindSearchData();
			}
		}

		protected virtual void PackagesGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var product = ((IPackage) row.DataItem).MasterProduct;
			if (product.Price != null)
			{
				var lblPrice = (Label)e.Row.FindControl("PriceLabel");
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}

			var lblProductStatus = (Label)e.Row.FindControl("StatusLabel");
			lblProductStatus.Text = ResolveStatusTitle(product.ProductStatusId);

			var lblType = (Label)e.Row.FindControl("TypeLabel");
			lblType.Text = ResolveTypeTitle(((ILekmerProduct)product).ProductTypeId);
		}

		protected virtual void PackagesGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeletePackage")
			{
				DeletePackage(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			}
		}

		protected virtual void PackagesGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			PackagesGrid.PageIndex = e.NewPageIndex;
		}

		protected void RestoreSearchData()
		{
			SearchItem source = SearchCriteriaState<SearchItem>.Instance.Get();

			if (source != null)
			{
				CriteriaDataSourceViewState = source;
				if (!IsPostBack)
				{
					PackagesGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
					PopulateSearch();
				}
			}
			else
			{
				CriteriaDataSourceViewState = DefaultSearchItem;
			}
		}

		protected void BindSearchData()
		{
			SearchControl.RestoreSearchFields(CriteriaDataSourceViewState.ProductSearchCriteria, 1);
		}

		protected virtual void SearchButtonClick(object sender, EventArgs e)
		{
			PackagesGrid.PageIndex = 0;
			CriteriaDataSourceViewState.ProductSearchCriteria = SearchControl.CreateSearchCriteria();
			if (!Page.IsValid) return;
			SearchCriteriaState<SearchItem>.Instance.Save(CriteriaDataSourceViewState);
			PopulateSearch();
		}

		protected void PopulateSearch()
		{
			var searchCriteria = CriteriaDataSourceViewState.ProductSearchCriteria;

			//Handle multiple ERP-ids
			var erpIds = searchCriteria.ErpId;
			if (!erpIds.IsNullOrEmpty())
			{
				erpIds = erpIds.Replace(" ", "").Replace(Environment.NewLine, ",").Replace(";", ",").Replace("\t", ",").Replace("\n", ",");
			}

			var title = searchCriteria.Title;
			if (!title.IsNullOrEmpty())
			{
				title = CleanForFullTextSearch(title);
			}

			PackagesObjectDataSource.SelectParameters.Clear();
			PackagesObjectDataSource.SelectParameters.Add("maximumRows", PackagesGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			PackagesObjectDataSource.SelectParameters.Add("startRowIndex", (PackagesGrid.PageIndex * PackagesGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			PackagesObjectDataSource.SelectParameters.Add("categoryId", searchCriteria.CategoryId);
			PackagesObjectDataSource.SelectParameters.Add("title", title);
			PackagesObjectDataSource.SelectParameters.Add("brandId", ((ILekmerProductSearchCriteria)searchCriteria).BrandId);
			PackagesObjectDataSource.SelectParameters.Add("statusId", searchCriteria.StatusId);
			PackagesObjectDataSource.SelectParameters.Add("priceFrom", searchCriteria.PriceFrom);
			PackagesObjectDataSource.SelectParameters.Add("priceTo", searchCriteria.PriceTo);
			PackagesObjectDataSource.SelectParameters.Add("erpId", erpIds);
		}

		protected void DeletePackage(int id)
		{
			var packageSecureService = IoC.Resolve<IPackageSecureService>();
			var package = packageSecureService.GetByIdSecure(ChannelHelper.CurrentChannel.Id, id);
			packageSecureService.Delete(SignInHelper.SignedInSystemUser, package);

			if (PackagesGrid.Rows.Count == 1 && PackagesGrid.PageIndex > 0)
			{
				PackagesGrid.PageIndex -= 1;
			}

			PackagesGrid.DataBind();
		}

		protected string GetCategory(int categoryId)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			var category = categorySecureService.GetById(categoryId);
			return category.Title;
		}

		private string CleanForFullTextSearch(string searchString)
		{
			var splitted = searchString.Replace("-", " ").Split(' ');
			if (splitted.Length > 1)
			{
				var newTitle = new string[splitted.Count()];

				var i = 0;
				foreach (var word in splitted)
				{
					if (word.Length > 1)
					{
						newTitle[i] = word;
						i++;
					}
				}
				return string.Join(" ", newTitle).Trim();
			}
			return searchString;
		}

		protected virtual string ResolveStatusTitle(int statusId)
		{
			if (_productStatuses == null)
			{
				var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
				_productStatuses = productStatusSecureService.GetAll();
			}
			if (_productStatuses == null)
			{
				_productStatuses = new Collection<IProductStatus>();
			}

			var productStatus = _productStatuses.First(item => item.Id == statusId).Title;

			return productStatus;
		}

		protected virtual string ResolveTypeTitle(int typeId)
		{
			if (_productTypes == null)
			{
				var productTypeSecureService = IoC.Resolve<IProductTypeSecureService>();
				_productTypes = productTypeSecureService.GetAll();
			}
			if (_productTypes == null)
			{
				_productTypes = new Collection<IProductType>();
			}

			var productType = _productTypes.First(item => item.Id == typeId).Title;

			return productType;
		}
	}
}