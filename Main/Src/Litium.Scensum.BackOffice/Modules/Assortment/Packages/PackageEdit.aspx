﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/Packages/Packages.Master" CodeBehind="PackageEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Packages.PackageEdit" %>

<%@ Register TagPrefix="scensum" TagName="CategoryNodeSelector" Src="~/UserControls/Tree/CategoryNodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductListControl" Src="~/UserControls/Assortment/ProductListControlPackage.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
		<uc:ScensumValidationSummary ID="ValidatorSummary" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="PackageValidationGroup" ForeColor="Black" />
	</div>
</asp:Content>

<asp:Content ID="PackageEditContent" ContentPlaceHolderID="PackagesPlaceHolder" runat="server">

	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function () { $('div#assortment-variation').css({ visibility: 'visible' }); });
		$(document).ready(function () { $('#tabs').tabs(); });

		function ShowHide(cbUseWebShopTitleId, tbWebShopTitleId, tbWebShopTitleTranslationId, tbTitleId) {
			var cbUseWebShopTitle = document.getElementById(cbUseWebShopTitleId);
			var tbWebShopTitle = document.getElementById(tbWebShopTitleId);
			var tbWebShopTitleTranslation = document.getElementById(tbWebShopTitleTranslationId);
			var tbTitle = document.getElementById(tbTitleId);
			var rfvTitle = document.getElementById('<%= WebShopTitleValidator.ClientID %>');

			ValidatorEnable(rfvTitle, cbUseWebShopTitle.checked);

			if (cbUseWebShopTitle.checked) {
				tbTitle.style.display = "none";
				tbWebShopTitle.style.display = "block";
				tbWebShopTitleTranslation.style.display = "block";
			}
			else {
				tbTitle.style.display = "block";
				tbWebShopTitle.style.display = "none";
				tbWebShopTitleTranslation.style.display = "none";
			}
		}

		function submitForm() {
			tinyMCE.triggerSave();
		}

		function OverrideGeneralInfo(cbOverrideGeneralInfoId, generalPackageInfo) {
			var cbOverrideGeneralInfo = document.getElementById(cbOverrideGeneralInfoId);
			var divGeneralPackageInfo = document.getElementById(generalPackageInfo);
			if (cbOverrideGeneralInfo.checked) {
				divGeneralPackageInfo.style.display = "block";
			}
			else {
				divGeneralPackageInfo.style.display = "none";
			}
		}
	</script>
	
	<asp:Panel ID="PackageEditPanel" runat="server" DefaultButton="SaveButton">
		<div class="package-edit-container">
			<div class="package-edit-header">
				<uc:LiteralEncoded ID="PackageHeader" runat="server"></uc:LiteralEncoded>
			</div>

			<uc:GenericWysiwygTranslator ID="DefaultAliasTranslator" runat="server" />
			<uc:GenericWysiwygTranslator ID="ProductDescriptionTranslator" runat="server" />
			<uc:GenericWysiwygTranslator ID="GeneralInfoTranslator" runat="server" />

			<div id="tabs">
				<ul>
					<li><a href="#fragment-1"><span><%= Resources.Product.Tab_ProductEdit_Info %></span></a></li>
					<li><a href="#fragment-2"><span><%= Resources.Product.Tab_ProductEdit_SiteStructure %></span></a></li>
					<li><a href="#fragment-3"><span><%= Resources.General.Literal_Description %></span></a></li>
				</ul>
				<br class="clear" />
				<div class="package-tabs-main-container" id="tabs-main-container">
					<div id="fragment-1">
						<div class="column input-box">
							<span><%= Resources.General.Literal_Title %>&nbsp;*</span>
							<asp:CheckBox ID="UseWebShopTitleCheckBox" runat="server" />
							<span><%= Resources.Product.Literal_UseWebTitle %></span>
							<uc:GenericTranslator ID="WebShopTitleTranslator" runat="server" />
							<asp:RequiredFieldValidator runat="server" 
								ID="WebShopTitleValidator" 
								ControlToValidate="WebShopTitleBox"
								ErrorMessage="<%$ Resources:ProductMessage,WebShopTitleEmpty %>"
								Display ="None" 
								ValidationGroup="PackageValidationGroup" />
							<asp:TextBox ID="TitleBox" runat="server" MaxLength="100" Width="435px" />
							<asp:TextBox ID="WebShopTitleBox" runat="server" MaxLength="100" Width="435px" />

							<div class="column input-box">
								<span><%= Resources.General.Literal_Category %>&nbsp;*&nbsp;(<%= Resources.LekmerMessage.Package_CategorySelectionMessage %>)</span>
								<br />
								<scensum:CategoryNodeSelector ID="PackageCategoryNodeSelector" runat="server" Width="410" AllowOnlyThirdLevelSelection="True" />
							</div>
						</div>

						<div class="column input-box">
							<asp:UpdatePanel ID="upPackageInfo" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<div class="column">
										<div class="column input-box">
											<span><%= Resources.General.Literal_ErpId %></span>
											<br />
											<asp:TextBox ID="ErpIdBox" runat="server" ReadOnly="True" Width="95px" CssClass="readonly" />
										</div>
										<div class="column input-box">
											<span><%= Resources.Product.Literal_NumberInStock %></span>
											<br />
											<asp:TextBox ID="NumberInStockBox" runat="server" ReadOnly="True" Width="50px" CssClass="readonly" />
										</div>
										<div class="column input-box">
											<span><%= Resources.Lekmer.Literal_PackageMasterProduct %></span>
											<br />
											<uc:HyperLinkEncoded runat="server" ID="MasterProductLink" CssClass="blue" />
										</div>
									</div>
									<br />
									<div class="column">
										<div class="column input-box">
											<span><%= Resources.General.Literal_Status %></span>
											<br />
											<asp:DropDownList ID="StatusList" runat="server" DataValueField="Id" DataTextField="Title" Width="99px" />
										</div>
										<div class="column input-box">
											<span><%= Resources.Lekmer.Literal_CurrentStatus %></span>
											<br />
											<uc:LiteralEncoded ID="CurrentStatus" runat="server"></uc:LiteralEncoded>
										</div>
										<div class="column input-box">
											<span><%= Resources.Product.Literal_StockStatus %></span>
											<br />
											<asp:DropDownList ID="StockStatusList" runat="server" Width="99px" />
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>

						<br class="clear" />
						<hr/>

						<div>
							<span class="bold">Products in package</span> (<span><%= Resources.LekmerMessage.Package_ProductsLimit %></span>)
							<br />
							<uc:ProductListControl ID="PackageProducts" runat="server" />
						</div>

						<br class="clear" />
						<hr/>
						
						<div>
							<asp:UpdatePanel ID="upPrices" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<div id="RefreshDiv" runat="server" Visible="False">
										<span style="color: red;"><%= Resources.LekmerMessage.Package_ProductsPriceError %></span>
										<br />
									</div>
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RefreshPricesButton" runat="server" Text="<%$ Resources:General, Button_Refresh %>" SkinID="DefaultButton" />
									<br class="clear" />
									<span class="bold"><%= Resources.Product.Literal_PriceLists %></span><br />
									<asp:GridView ID="PriceListsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
										<Columns>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="40%">
												<ItemTemplate>
													<%# GetPriceList((int)Eval("PriceListID")).Title %>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="15%">
												<ItemTemplate>
													<%# GetPriceListStatus(GetPriceList((int)Eval("PriceListID")).PriceListStatusId)%>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" ItemStyle-Width="15%">
												<ItemTemplate>
													<%# GetPrice((int)Eval("PriceListID"), Convert.ToDecimal(Eval("PriceIncludingVat"))) %>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Vat %>" ItemStyle-Width="15%">
												<ItemTemplate>
													<%# Eval("VatPercentage") %>
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>

						<br class="clear" />
						<hr/>

						<div>
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="generateDescriptionBtn" runat="server" Text="Generate description" SkinID="DefaultButton" CssClass="right" />
							<span class="product-field-caption"><%= Resources.Product.Literal_LongArticleDescription %></span>&nbsp;
							<asp:ImageButton runat="server" ID="DescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
							<div class="right">
								<asp:Repeater runat="server" ID="LanguagesRepeater">
									<ItemTemplate>
										<asp:HiddenField ID="hfLangId" Value='<%#Eval("Id") %>' runat="server" />
										<asp:CheckBox ID="cbLang" runat="server" Checked="True" />
										<asp:Literal ID="langName" runat="server" Text='<%# Eval("Iso") %>' />
										&nbsp;&nbsp;
									</ItemTemplate>
								</asp:Repeater>
							</div>
							<uc:LekmerTinyMceEditor ID="LongArticleDescriptionEditor" runat="server" SkinID="tinyMCE"/>
						</div>
					</div>
					<div id="fragment-2">
						<div class="input-box">
							<asp:UpdatePanel ID="upRestrictions" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<span class="bold">Registry Restriction</span>
									<asp:GridView ID="ProductRegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
										<Columns>
											<asp:TemplateField HeaderText="Registry" ItemStyle-Width="20%">
												<ItemTemplate>
													<asp:Label ID="ProductRegistryTitleLabel" runat="server" Text='<%# Eval("Title") %>' />
													<asp:HiddenField ID="ProductRegistryIdHidden" runat="server" Value='<%# Eval("ProductRegistryId").ToString() %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Restriction" ItemStyle-Width="5%">
												<ItemStyle HorizontalAlign="Center" />
												<HeaderStyle HorizontalAlign="Center" />
												<ItemTemplate>
													<asp:CheckBox ID="RestrictionCheckBox" runat="server" Enabled="False" />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</div>
					<div id="fragment-3">
						<div style="padding-top: 10px;">
							<div>
								<span class="product-field-caption">Default alias for long description</span>&nbsp;
								<asp:ImageButton runat="server" ID="DefaultAliasTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
								<uc:LekmerTinyMceEditor ID="DefaultAliasEditor" runat="server" SkinID="tinyMCE" Width="100%"/>
							</div>

							<br class="clear" />
							<hr/>

							<asp:CheckBox ID="OverrideGeneralInfo" runat="server"/> <span>Override Brand/Category general info</span>
							<div id="generalPackageInfo" runat="server" style="display: none;">
								<span class="product-field-caption">General package info</span>&nbsp;
								<asp:ImageButton runat="server" ID="GeneralInfoTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
								<uc:LekmerTinyMceEditor ID="GeneralInfoEditor" runat="server" SkinID="tinyMCE" Width="100%"/>
							</div>
						</div>
					</div>
				</div>

				<br class="clear" />

				<div class="left" style="padding-top:10px;">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General, Button_Delete %>"
						SkinID="DefaultButton" OnClientClick="return DeleteConfirmation('package');" />
				</div>
				<div id="model-buttons" class="buttons right">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
						SkinID="DefaultButton" ValidationGroup="PackageValidationGroup" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
						SkinID="DefaultButton" />
				</div>
			</div>
		</div>
	</asp:Panel>

</asp:Content>