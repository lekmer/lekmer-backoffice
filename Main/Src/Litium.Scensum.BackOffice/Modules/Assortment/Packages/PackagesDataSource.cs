﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Packages
{
	public class PackagesDataSource
	{
		private int _rowCount;

		public int SelectCount()
		{
			return _rowCount;
		}

		public int SelectCount(int maximumRows, int startRowIndex)
		{
			return _rowCount;
		}

		public Collection<IPackage> SelectMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public int SelectCount(int maximumRows, int startRowIndex, string categoryId, string title, string brandId, string statusId, string priceFrom, string priceTo, string erpId)
		{
			return _rowCount;
		}

		public Collection<IPackage> SelectMethod(int maximumRows, int startRowIndex, string categoryId, string title, string brandId, string statusId, string priceFrom, string priceTo, string erpId)
		{
			var searchCriteria = (ILekmerProductSearchCriteria)IoC.Resolve<IProductSearchCriteria>();
			searchCriteria.CategoryId = categoryId;
			searchCriteria.Title = title;
			searchCriteria.PriceFrom = priceFrom;
			searchCriteria.PriceTo = priceTo;
			searchCriteria.ErpId = erpId;
			searchCriteria.StatusId = statusId;
			searchCriteria.BrandId = brandId;

			var packageSecureService = IoC.Resolve<IPackageSecureService>();
			var packages = packageSecureService.Search(ChannelHelper.CurrentChannel.Id, searchCriteria, startRowIndex / maximumRows + 1, maximumRows);

			_rowCount = packages.TotalCount;

			return packages;
		}
	}
}