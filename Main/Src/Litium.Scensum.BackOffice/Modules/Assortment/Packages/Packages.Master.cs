﻿using System;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Packages
{
	public partial class Packages : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CreatePackageButton.Click += CreatePackageClick;
		}

		protected virtual void CreatePackageClick(object sender, EventArgs e)
		{
			Response.Redirect(LekmerPathHelper.Package.GetEditUrl());
		}
	}
}