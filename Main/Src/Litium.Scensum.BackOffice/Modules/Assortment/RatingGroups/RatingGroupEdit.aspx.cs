﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups
{
	public partial class RatingGroupEdit : LekmerStatePageController<RatingGroupState>, IEditor
	{
		private const int MoveUpStep = -15;
		private const int MoveDownStep = 15;
		private const int DifferenceStep = 10;
		private const int MinStep = 1;

		private List<string> _validationMessage = new List<string>();

		private Collection<IRatingFolder> _allFolders;

		protected virtual RatingGroupsMaster MasterPage
		{
			get
			{
				return (RatingGroupsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderSelector.NodeCommand += OnFolderSelectorCommand;
			FolderSelector.SelectedFolderChangedEvent += OnSelectedFolderChanged;

			RatingsGrid.RowDataBound += OnRatingsRowDataBound;
			RatingsGrid.RowCommand += OnRowCommand;
			RatingsGrid.PageIndexChanging += OnPageIndexChanging;
			RemoveRatingsButton.Click += OnRatingsRemove;
			RatingSelector.SelectEvent += OnRatingsAdd;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var ratingGroup = IoC.Resolve<IRatingGroupSecureService>().GetById(id.Value);
				TitleTextBox.Text = ratingGroup.Title;
				CommonNameTextBox.Text = ratingGroup.CommonName;
				DescriptionTextBox.Text = ratingGroup.Description ?? string.Empty;

				InitializeState(ratingGroup.RatingGroupFolderId);

				PopulateRatings(id.Value);

				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_EditRatingGroup);
			}
			else
			{
				InitializeState(null);

				PopulateRatingsGridDefault();

				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_CreateRatingGroup);
			}

			PopulatePath(State.FolderId);
			MasterPage.SelectedFolderId = State.FolderId;
			MasterPage.PopulateTree(State.FolderId);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.RatingReview.RatingGroupSaveSuccess);
				}
			}

			MasterPage.SetupTabAndPanel();
		}

		// Page events.

		public void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}

				SystemMessageContainer.MessageType = InfoType.Warning;

				return;
			}

			int? id = GetIdOrNull();
			
			var ratingGroupSecureService = IoC.Resolve<IRatingGroupSecureService>();

			var ratingGroup = id.HasValue ? ratingGroupSecureService.GetById(id.Value) : IoC.Resolve<IRatingGroup>();
			ratingGroup.Title = TitleTextBox.Text;
			ratingGroup.CommonName = CommonNameTextBox.Text;
			ratingGroup.Description = DescriptionTextBox.Text;
			ratingGroup.RatingGroupFolderId = State.FolderId ?? -1;

			ratingGroup = ratingGroupSecureService.SaveWithRatings(SignInHelper.SignedInSystemUser, ratingGroup, GetRatings());

			if (ratingGroup.Id == -1)
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingGroupTitleExist);
			}
			else if (ratingGroup.Id == -2)
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingGroupCommonNameExist);
			}
			else
			{
				MasterPage.SelectedFolderId = State.FolderId;
				MasterPage.PopulateTree(State.FolderId);
				MasterPage.UpdateBreadcrumbs(State.FolderId);

				Response.Redirect(LekmerPathHelper.RatingGroup.GetEditUrlWithMessage(ratingGroup.Id));
			}
		}

		protected virtual void InitializeState(int? ratingGroupFolderId)
		{
			if (State == null)
			{
				State = new RatingGroupState { RatingStateList = new List<RatingState>() };
			}

			if (ratingGroupFolderId.HasValue)
			{
				State.FolderId = ratingGroupFolderId;
			}
			else
			{
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					State.FolderId = MasterPage.SelectedFolderId.Value;
				}
			}
		}

		protected virtual Collection<IRatingGroupRating> GetRatings()
		{
			Collection<IRatingGroupRating> ratingGroupRatings = new Collection<IRatingGroupRating>();

			RefreshOrdinal();

			var ratings = Sort(State.RatingStateList, MinStep);
			foreach (var ratingState in ratings)
			{
				var ratingGroupRating = IoC.Resolve<IRatingGroupRating>();
				ratingGroupRating.RatingId = ratingState.Id;
				ratingGroupRating.Ordinal = ratingState.Ordinal;

				ratingGroupRatings.Add(ratingGroupRating);
			}

			return ratingGroupRatings;
		}

		protected virtual void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				MasterPage.RedirectToDefaultPage();
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		// Parent folder functionality.

		protected virtual void OnFolderSelectorCommand(object sender, CommandEventArgs e)
		{
			FolderSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			FolderSelector.DataBind();
		}

		protected virtual void OnSelectedFolderChanged(object sender, UserControls.Media.Events.SelectedFolderChangedEventArgs e)
		{
			if (e.SelectedFolderId != null)
			{
				State.FolderId = e.SelectedFolderId.Value;
			}
		}

		protected virtual void PopulatePath(int? ratingGroupFolderId)
		{
			FolderSelector.SelectedNodeId = ratingGroupFolderId;

			FolderSelector.DataSource = GetPathSource(ratingGroupFolderId);
			FolderSelector.DataBind();

			FolderSelector.PopulatePath(ratingGroupFolderId);
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			return IoC.Resolve<IRatingGroupFolderSecureService>().GetTree(folderId);
		}

		// Ratings grid.

		protected virtual void OnRatingsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row, AllSelectedDivRating);
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteRating":
					var ratingItemToDelate = State.RatingStateList.FirstOrDefault(rsi => rsi.Id == id);
					if (ratingItemToDelate != null)
					{
						State.RatingStateList.Remove(ratingItemToDelate);
						PopulateRatingsGrid();
					}
					break;

				case "UpOrdinal":
					Move(id, MoveUpStep);
					break;

				case "DownOrdinal":
					Move(id, MoveDownStep);
					break;

				case "RefreshOrdinal":
					RefreshOrdinal();
					break;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingsGrid.PageIndex = e.NewPageIndex;

			PopulateRatingsGrid();
		}

		protected virtual void OnRatingsAdd(object sender, UserControls.Assortment.Events.RatingSelectEventArgs e)
		{
			var lastRating = State.RatingStateList.LastOrDefault();
			int ordinal = lastRating != null ? lastRating.Ordinal : 1;

			foreach(var group in e.Ratings)
			{
				var clone = group;

				if (State.RatingStateList.FirstOrDefault(c => c.Id == clone.Id) == null)
				{
					group.Ordinal =++ ordinal;
					State.RatingStateList.Add(group);
				}
			}

			PopulateRatingsGrid();
		}

		protected virtual void OnRatingsRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = RatingsGrid.GetSelectedIds();

			foreach (var id in ids)
			{
				var cloneId = id;
				var rating = State.RatingStateList.FirstOrDefault(r => r.Id == cloneId);
				if (rating != null)
				{
					State.RatingStateList.Remove(rating);
				}
			}

			PopulateRatingsGrid();
		}

		protected virtual void PopulateRatings(int ratingGroupId)
		{
			if (State.RatingStateList == null)
			{
				State.RatingStateList = new List<RatingState>();
			}

			var ratings = IoC.Resolve<IRatingSecureService>().GetAllByGroup(ratingGroupId);
			var ratingGroupRatings = IoC.Resolve<IRatingGroupRatingSecureService>().GetAllByGroup(ratingGroupId);

			foreach (var rating in ratings)
			{
				var ratingState = new RatingState(rating);

				var ratingGroupRating = ratingGroupRatings.FirstOrDefault(rgr => rgr.RatingId == rating.Id);
				if (ratingGroupRating != null)
				{
					ratingState.Ordinal = ratingGroupRating.Ordinal;
				}

				State.RatingStateList.Add(ratingState);
			}

			PopulateRatingsGrid();
		}

		protected virtual void PopulateRatingsGrid()
		{
			RatingsGrid.DataSource = Sort(State.RatingStateList, DifferenceStep);
			RatingsGrid.DataBind();

			RatingsUpdatePanel.Update();
		}

		protected virtual void PopulateRatingsGridDefault()
		{
			RatingsGrid.DataSource = null;
			RatingsGrid.DataBind();
		}

		protected virtual string GetEditUrl(object ratingId)
		{
			int? id = GetIdOrNull();
			if (id.HasValue)
			{
				return LekmerPathHelper.Rating.GetEditUrlForRatingGroupEdit(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture), id.Value);
			}
			
			return LekmerPathHelper.Rating.GetEditUrl(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture));
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<IRatingFolderSecureService>();
				_allFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row, HtmlGenericControl allSelectedDiv)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('"
					+ selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + allSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
					cbSelect.ID + "', '" + grid.ClientID + "', '" + allSelectedDiv.ClientID + @"');");
			}
		}

		// Ordinals.

		protected void Move(int elementId, int step)
		{
			var ratings = State.RatingStateList;

			var rating = ratings.FirstOrDefault(r => r.Id == elementId);

			if (rating == null)
			{
				return;
			}

			rating.Ordinal += step;

			PopulateRatingsGrid();
		}

		protected void RefreshOrdinal()
		{
			if (!ValidateOrdinals())
			{
				SystemMessageContainer.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return;
			}

			var ratings = State.RatingStateList;

			foreach (GridViewRow row in RatingsGrid.Rows)
			{
				var ratingId = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var rating = ratings.FirstOrDefault(r => r.Id == ratingId);
				if (rating == null)
				{
					continue;
				}

				rating.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
			}

			PopulateRatingsGrid();
		}

		protected bool ValidateOrdinals()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}

		protected static List<RatingState> Sort(List<RatingState> ratings, int step)
		{
			ratings.Sort(delegate(RatingState element1, RatingState element2)
			{
				if (element1.Ordinal < element2.Ordinal)
				{
					return -1;
				}

				if (element1.Ordinal > element2.Ordinal)
				{
					return 1;
				}

				return 0;
			});

			for (int i = 0; i < ratings.Count(); i++)
			{
				ratings[i].Ordinal = (i + 1) * step;
			}

			return ratings;
		}

		// Validation.

		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidTitle())
			{
				_validationMessage.Add(Resources.RatingReview.TitleEmpty);
				isValid = false;
			}

			if (!IsValidCommonName())
			{
				_validationMessage.Add(Resources.RatingReview.CommonNameEmpty);
				isValid = false;
			}

			if (!IsValidFolder())
			{
				_validationMessage.Add(Resources.RatingReview.RatingGroupFolderEmpty);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidTitle()
		{
			string title = TitleTextBox.Text.Trim();

			if (string.IsNullOrEmpty(title))
			{
				return false;
			}

			return title.Length <= 50;
		}

		protected virtual bool IsValidCommonName()
		{
			string commonName = CommonNameTextBox.Text.Trim();

			if (string.IsNullOrEmpty(commonName))
			{
				return false;
			}

			return commonName.Length <= 50;
		}

		protected virtual bool IsValidFolder()
		{
			return State.FolderId > 0;
		}
	}

	[Serializable]
	public sealed class RatingGroupState
	{
		public int? FolderId { get; set; }
		public List<RatingState> RatingStateList { get; set; }
	}

	[Serializable]
	public sealed class RatingState : Rating
	{
		public RatingState(IRating rating)
		{
			Id = rating.Id;
			RatingFolderId = rating.RatingFolderId;
			RatingRegistryId = rating.RatingRegistryId;
			CommonName = rating.CommonName;
			Title = rating.Title;
			Description = rating.Description;
		}

		public int Ordinal { get; set; }
	}
}