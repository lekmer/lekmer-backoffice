﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Review;
using Resources;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Reviews
{
	public partial class Default_addon : LekmerPageController
	{
		private const string _sortBy = "Review.CreatedDate";

		private Collection<IReviewStatus> _statuses;
		
		protected override void SetEventHandlers()
		{
			SearchButton.Click += Search;
			AscLink.Click += SortByAsc;
			DescLink.Click += SortByDesc;
			SetStatusButton.Click += SetStatuses;
			MultipleDeleteButton.Click += MultipleDelete;
			
			ReviewsGrid.RowDataBound += RowDataBound;
			ReviewsGrid.PageIndexChanging += PageIndexChanging;
			ReviewsGrid.RowCommand += RowCommand;
			ReviewsObjectDataSource.Selected += ReviewsObjectDataSourceSelected;
		}

		protected override void PopulateForm()
		{
			PopulateStatuses();
			PopulateDates();
			SetSortDirection(AscLink, DescLink);
		}

		protected virtual void Search(object sender, EventArgs e)
		{
			ReviewsGrid.PageIndex = 0;
			PopulateGrid();
		}

		protected virtual void SortByAsc(object sender, EventArgs e)
		{
			SetSortDirection(AscLink, DescLink);
			ReviewsGrid.PageIndex = 0;
			PopulateGrid();
		}

		protected virtual void SortByDesc(object sender, EventArgs e)
		{
			SetSortDirection(DescLink, AscLink);
			ReviewsGrid.PageIndex = 0;
			PopulateGrid();
		}

		protected virtual void SetStatuses(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(SetStatusContextMenu.SelectedValue))
			{
				Messager.Add(GeneralMessage.ChooseStatusValue, InfoType.Warning);
				return;
			}
			var ids = ReviewsGrid.GetSelectedIds();
			if (ids.Count() <= 0)
			{
				Messager.Add(Resources.Review.Message_NoReviewSelected, InfoType.Warning);
				return;
			}
			var statusId = int.Parse(SetStatusContextMenu.SelectedValue, CultureInfo.CurrentCulture);
			var service = IoC.Resolve<IReviewSecureService>();
			service.SetStatus(SignInHelper.SignedInSystemUser, ids, statusId);
			Messager.Add(GeneralMessage.StatusesChangedSuccessfully, InfoType.Success);
			PopulateGrid();
		}

		protected virtual void MultipleDelete(object sender, EventArgs e)
		{
			var ids = ReviewsGrid.GetSelectedIds();
			if (ids.Count() <= 0)
			{
				Messager.Add(Resources.Review.Message_NoReviewSelected, InfoType.Warning);
				return;
			}
			var service = IoC.Resolve<IReviewSecureService>();
			service.Delete(SignInHelper.SignedInSystemUser, ids);
			Messager.Add(GeneralMessage.DeleteSeccessful, InfoType.Success);
			if (ReviewsGrid.Rows.Count == ids.Count() && ReviewsGrid.PageIndex > 0)
			{
				ReviewsGrid.PageIndex--;
			}
			PopulateGrid();
		}

		protected void RowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
														 "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + ReviewsGrid.ClientID +
														 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
														 ReviewsGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox)ReviewsGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
												"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
												cbSelect.ID + "', '" + ReviewsGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
					}
					break;
			}
		}

		protected virtual void PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ReviewsGrid.PageIndex = e.NewPageIndex;
		}

		protected void RowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteReview") return;

			var service = IoC.Resolve<IReviewSecureService>();
			service.Delete(SignInHelper.SignedInSystemUser, Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			if (ReviewsGrid.Rows.Count == 1 && ReviewsGrid.PageIndex > 0)
			{
				ReviewsGrid.PageIndex--;
			}
			PopulateGrid();
		}

		protected virtual void ReviewsObjectDataSourceSelected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			var reviews = e.ReturnValue as ReviewRecordCollection;
			if (reviews == null) return;
			SearchResultLabel.Text = string.Format(CultureInfo.CurrentCulture, 
				Resources.Review.Literal_SearchResult, reviews.Count, reviews.TotalCount);
		}

		protected virtual void ReviewStatusChanged(object sender, ContextMenu3EventArgs e)
		{
			var row = ((GridViewRow)(((ContextMenu3)sender).Parent.Parent));
            var hiddenField = row.FindControl("IdHiddenField") as HiddenField;
			if (hiddenField == null) return;
			var reviewId = Convert.ToInt32(hiddenField.Value, CultureInfo.CurrentCulture);
			int newStatusId;
			if (!int.TryParse(e.SelectedValue, out newStatusId)) return;
			IoC.Resolve<IReviewSecureService>().SetStatus(SignInHelper.SignedInSystemUser, reviewId, newStatusId);
			Messager.Add(GeneralMessage.StatusesChangedSuccessfully, InfoType.Success);
			PopulateGrid();
		}

		protected virtual void PopulateStatuses()
		{
			var statuses = GetAllStatuses();
			StatusList.DataSource = statuses;
			StatusList.DataBind();
			StatusList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
			var defaultStatus = statuses.FirstOrDefault(s => s.CommonName == "Pending");
			if (defaultStatus != null)
			{
				StatusList.Items.FindByValue(defaultStatus.Id.ToString(CultureInfo.CurrentCulture)).Selected = true;
			}

			var contextMenuSource = GetStatusDataSource();
			contextMenuSource.Insert(0, new ContextMenuDataSourceItem { Value = string.Empty, Text = Resources.General.Literal_Status + "..." });
			SetStatusContextMenu.DataSource = contextMenuSource;
			SetStatusContextMenu.DataBind();
		}

		protected virtual void PopulateDates()
		{
			StartDateCalendarControl.Format = EndDateCalendarControl.Format = 
				CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

			StartDateTextBox.Text = DateTime.Today.AddMonths(-1).ToShortDateString();
			EndDateTextBox.Text = DateTime.Today.ToShortDateString();
		}

		protected virtual void PopulateGrid()
		{
			var searchCriteria = GetSearchCriteria();
			if (searchCriteria == null) return;

			ReviewsObjectDataSource.SelectParameters.Clear();
			ReviewsObjectDataSource.SelectParameters.Add("maximumRows", ReviewsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			ReviewsObjectDataSource.SelectParameters.Add("startRowIndex", (ReviewsGrid.PageIndex * ReviewsGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			ReviewsObjectDataSource.SelectParameters.Add("productTitle", searchCriteria.ProductTitle);
			ReviewsObjectDataSource.SelectParameters.Add("productId", searchCriteria.ProductId);
			ReviewsObjectDataSource.SelectParameters.Add("message", searchCriteria.Message);
			ReviewsObjectDataSource.SelectParameters.Add("author", searchCriteria.Author);
			ReviewsObjectDataSource.SelectParameters.Add("statusId", searchCriteria.StatusId);
			ReviewsObjectDataSource.SelectParameters.Add("dateFrom", searchCriteria.CreatedFrom);
			ReviewsObjectDataSource.SelectParameters.Add("dateTo", searchCriteria.CreatedTo);
			ReviewsObjectDataSource.SelectParameters.Add("sortBy", searchCriteria.SortBy);
			ReviewsObjectDataSource.SelectParameters.Add("sortByDescending", searchCriteria.SortByDescending);

			SearchResultDiv.Style.Add("display", "block");
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IReviewSearchCriteria GetSearchCriteria()
		{
			if (!ValidateSearchForm()) return null;

			var searchCriteria = IoC.Resolve<IReviewSearchCriteria>();
			searchCriteria.ProductTitle = ProductTitleTextBox.Text;
			searchCriteria.ProductId = ProductIdTextBox.Text;
			searchCriteria.Message = MessageTextBox.Text;
			searchCriteria.Author = AuthorTextBox.Text;
			searchCriteria.StatusId = StatusList.SelectedValue;
			searchCriteria.CreatedFrom = GetDate(StartDateTextBox.Text, false);
			searchCriteria.CreatedTo = GetDate(EndDateTextBox.Text, true);
			searchCriteria.SortBy = _sortBy;
			searchCriteria.SortByDescending = SortByDescending.ToString(CultureInfo.CurrentCulture);
			return searchCriteria;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ContextMenuDataSourceItem> GetStatusDataSource()
		{
			return new Collection<ContextMenuDataSourceItem>(GetAllStatuses().Select(
				item => new ContextMenuDataSourceItem
			        		{
			        			Text = item.Title,
			        			Value = item.Id.ToString(CultureInfo.CurrentCulture),
								ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture,
																				"Media/{0}.png",
																				item.CommonName))
			        		}).ToList());
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IReviewStatus> GetAllStatuses()
		{
			return _statuses ?? (_statuses = IoC.Resolve<IReviewStatusSecureService>().GetAll());
		}

		protected virtual void SetSortDirection(LinkButton current, LinkButton alternate)
		{
			current.Enabled = false;
			current.Style.Add("color", "#000000");
			current.Style.Remove("text-decoration");
			current.Font.Underline = false;

			alternate.Enabled = true;
			alternate.Style.Add("color", "#004CD5");
			alternate.Style.Add("text-decoration", "underline");
			alternate.Font.Underline = true;
		}

		protected virtual bool SortByDescending
		{
			get { return !DescLink.Enabled; }
		}

		protected virtual bool ValidateSearchForm()
		{
			var isValid = true;
			if (!IsValidProductId())
			{
				Messager.Add(Resources.Review.Message_InvalidProductId, InfoType.Warning);
				isValid = false;
			}
			if (!IsValidStartDate())
			{
				Messager.Add(Resources.Review.Message_InvalidStartDate, InfoType.Warning);
				isValid = false;
			}
			if (!IsValidEndDate())
			{
				Messager.Add(Resources.Review.Message_InvalidEndDate, InfoType.Warning);
				isValid = false;
			}
			if (!IsValidDateRange())
			{
				Messager.Add(Resources.Review.Message_InvalidDateRange, InfoType.Warning);
				isValid = false;
			}
			return isValid;
		}

		protected virtual bool IsValidProductId()
		{
			var productId = ProductIdTextBox.Text.Trim();
			int id;
			return string.IsNullOrEmpty(productId) || int.TryParse(productId, out id);
		}

		protected virtual bool IsValidStartDate()
		{
			var dateTimeString = StartDateTextBox.Text.Trim();
			DateTime date;
			return string.IsNullOrEmpty(dateTimeString) || DateTime.TryParse(dateTimeString, out date);
		}

		protected virtual bool IsValidEndDate()
		{
			var dateTimeString = EndDateTextBox.Text.Trim();
			DateTime date;
			return string.IsNullOrEmpty(dateTimeString) || DateTime.TryParse(dateTimeString, out date);
		}

		protected virtual bool IsValidDateRange()
		{
			DateTime startDate;
			DateTime endDate;
			return !DateTime.TryParse(StartDateTextBox.Text.Trim(), out startDate) 
				|| !DateTime.TryParse(EndDateTextBox.Text.Trim(), out endDate) 
				|| endDate >= startDate;
		}

		protected virtual string GetDate(string text, bool isEndDate)
		{
			if (string.IsNullOrEmpty(text)) 
				return string.Empty;

			DateTime dateTime;
			if (!DateTime.TryParse(text, out dateTime))
				return string.Empty;

			//If user write "from 2010-10-10 to 2010-10-10" it will be parsed as "from 2010-10-10 00:00:00 to 2010-10-10 00:00:00"
			//so he will see reviews posted at 2010-10-10 00:00:00 only, but he expect to see all reviews posted at this day.
			//Therefor date range should be transformed to following: "from 2010-10-10 00:00:00 to 2010-10-10 23:59:59"
			if (isEndDate && dateTime.Hour == 0 && dateTime.Minute == 0 && dateTime.Second == 0 && dateTime.ToShortDateString() == text)
				return dateTime.AddDays(1).AddSeconds(-1).ToString(CultureInfo.CurrentCulture);

			return text.ToString(CultureInfo.CurrentCulture);
		}
	}
}