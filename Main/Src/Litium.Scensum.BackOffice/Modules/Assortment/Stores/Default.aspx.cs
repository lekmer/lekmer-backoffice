﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Resources;
using Convert=System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Stores
{
	public partial class Default : LekmerPageController
	{
		private Collection<IStoreStatus> _statuses;

		protected override void SetEventHandlers()
		{
			StoresGrid.RowDataBound += OnRowDataBound;
			StoresGrid.PageIndexChanging += OnPageIndexChanging;
			StoresGrid.RowCommand += OnRowCommand;
			SetStatusButton.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			PopulateGrid();
			PopulateStatusList();
		}

		protected virtual void OnSave(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(StoreStatusList.SelectedValue))
			{
				messager.Add(GeneralMessage.ChooseStatusValue);
				return;
			}
			var ids = StoresGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				messager.Add(ProductMessage.NoStoreSelected);
				messager.MessageType = InfoType.Warning;
				return;
			}
			var statusId = int.Parse(StoreStatusList.SelectedValue, CultureInfo.CurrentCulture);
			var service = IoC.Resolve<IStoreSecureService>();
			service.SetStatus(SignInHelper.SignedInSystemUser, ids, statusId);
			messager.Add(GeneralMessage.StatusesChangedSuccessfully, InfoType.Success);
			PopulateGrid();
		}

		private static int GetStoreId(Control row)
		{
			var hiddenField = row.FindControl("IdHiddenField") as HiddenField;
			if (hiddenField != null) return Convert.ToInt32(hiddenField.Value, CultureInfo.CurrentCulture);
			throw new ArgumentNullException("row");
		}

		protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
						                                 "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + StoresGrid.ClientID +
						                                 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
						                                 StoresGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox)StoresGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
						                        "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
						                        cbSelect.ID + "', '" + StoresGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
					}
					break;
			}
		}

		protected void DdlStatusSelectedValueChanged(object sender, ContextMenu3EventArgs e)
		{
			var row = ((GridViewRow) (((ContextMenu3) sender).Parent.Parent));
			var storeId = GetStoreId(row);
			int newStatus;
			if (e.SelectedValue == "Offline")
			{
				newStatus = (int) BlockStatusInfo.Offline;
			}
			else if (e.SelectedValue == "Online")
			{
				newStatus = (int) BlockStatusInfo.Online;
			}
			else
			{
				return;
			}

			IoC.Resolve<IStoreSecureService>().SetStatus(SignInHelper.SignedInSystemUser, storeId, newStatus);
			messager.Add(GeneralMessage.StatusesChangedSuccessfully, InfoType.Success);
			PopulateGrid();
		}

		private Collection<ContextMenuDataSourceItem> _statusesDataSource;

		protected Collection<ContextMenuDataSourceItem> StatusesDataSource
		{
			get
			{
				if (null == _statusesDataSource)
				{
					var statusDataSource = Enum.GetNames(typeof (BlockStatusInfo));
					_statusesDataSource = new Collection<ContextMenuDataSourceItem>(
						statusDataSource.Select(
							item => new ContextMenuDataSourceItem
							        	{
							        		Text = item,
							        		Value = item,
							        		ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture,
							        		                                    "~/Media/Images/Common/{0}.gif",
							        		                                    item))
							        	}).ToList());
				}
				return _statusesDataSource;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			StoresGrid.PageIndex = e.NewPageIndex;
		}

		protected void OnRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteStore") return;

			var service = IoC.Resolve<IStoreSecureService>();
			service.Delete(SignInHelper.SignedInSystemUser, Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			PopulateGrid();
		}

		protected virtual void PopulateGrid()
		{
			StoresObjectDataSource.SelectParameters.Clear();
			StoresObjectDataSource.SelectParameters.Add("maximumRows", StoresGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			StoresObjectDataSource.SelectParameters.Add("startRowIndex",
			                                            (StoresGrid.PageIndex*StoresGrid.PageSize).ToString(
			                                            	CultureInfo.CurrentCulture));

			StoresGrid.DataBind();
			AllSelectedDiv.Visible = StoresGrid.Rows.Count > 0;
		}

		protected virtual void PopulateStatusList()
		{
			StoreStatusList.DataSource = GetStoreStatuses();
			StoreStatusList.DataBind();
			StoreStatusList.Items.Insert(0, new ListItem(Resources.General.Literal_Status + "...", string.Empty));
		}

		protected virtual string GetStoreStatus(int statusId)
		{
			var statuses = GetStoreStatuses();
			return statuses.FirstOrDefault(item => item.Id == statusId).Title;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IStoreStatus> GetStoreStatuses()
		{
			return _statuses ?? (_statuses = IoC.Resolve<IStoreStatusSecureService>().GetAll());
		}
	}
}
