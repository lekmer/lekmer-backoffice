﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Assortment/Stores/StoresMaster.Master"
	CodeBehind="StoreEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Stores.StoreEdit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton"
			runat="server" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
			ID="ValidatorSummary" DisplayMode="List" ValidationGroup="StoreValidationGroup" />
	</div>
</asp:Content>
<asp:Content ID="StoreEditContent" ContentPlaceHolderID="StoresPlaceHolder" runat="server">

	<script type="text/javascript">
		function submitForm() {
			tinyMCE.triggerSave();
		}

		function ConfirmStoreDelete() {
			return DeleteConfirmation('<%= Resources.Product.Literal_DeleteStore %>');
		}
	</script>

	<asp:Panel ID="StoreEditPanel" runat="server" DefaultButton="SaveButton">
		<div class="stores-edit-container">
			<div class="stores-edit-header">
				<uc:LiteralEncoded ID="StoreHeader" runat="server"></uc:LiteralEncoded>
			</div>
			<br />
			<div class="column">
				<div class="column input-box">
					<span>
						<%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp;
					<asp:RequiredFieldValidator runat="server" ID="StoreTitleValidator" ControlToValidate="StoreTitleBox"
						ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="StoreValidationGroup" />
					<br />
					<asp:TextBox ID="StoreTitleBox" runat="server" MaxLength="100" Width="465px"></asp:TextBox>
				</div>
				<div class="column input-box">
					<span>
						<%= Resources.General.Literal_ErpId %></span><br />
					<asp:TextBox ID="StoreErpBox" runat="server" Width="300px"></asp:TextBox>
				</div>
				<div class="column input-box">
					<span>
						<%= Resources.General.Literal_Status %></span><br />
					<asp:DropDownList ID="StoreStatusList" runat="server" DataValueField="Id" DataTextField="Title" Width="135px">
					</asp:DropDownList>
				</div>
			</div>
			<br />
			<div class="stores-edit-descrition">
				<span>
					<%= Resources.General.Literal_Description %></span><br />
				<uc:LekmerTinyMceEditor ID="StoreDescriptionEditor" runat="server" SkinID="tinyMCE" />
			</div>
			<br class="clear" />
			<div class="left" style="padding-top:10px;">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General, Button_Delete %>"
					SkinID="DefaultButton" OnClientClick="return ConfirmStoreDelete();" />
			</div>
			<div id="model-buttons" class="buttons right">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
					SkinID="DefaultButton" ValidationGroup="StoreValidationGroup" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
					SkinID="DefaultButton" />
			</div>
		</div>
	</asp:Panel>
</asp:Content>
