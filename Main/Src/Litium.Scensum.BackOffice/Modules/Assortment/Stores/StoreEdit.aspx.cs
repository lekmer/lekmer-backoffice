﻿using System;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Stores
{
	public partial class StoreEdit : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			DeleteButton.Click += OnDelete;
		}

		protected override void PopulateForm()
		{
			// If redirected after successful saving new theme -> display success message
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
			{
				Messager.Add(Resources.GeneralMessage.SaveSuccessStore, InfoType.Success);
			}
			PopulateStoreInfo();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Assortment.Store.GetDefaultUrl());
		}

		protected virtual void OnDelete(object sender, EventArgs e)
		{
			var storeId = GetIdOrNull();
			if (!storeId.HasValue) return;

			var service = IoC.Resolve<IStoreSecureService>();
			service.Delete(SignInHelper.SignedInSystemUser, storeId.Value);
			Response.Redirect(PathHelper.Assortment.Store.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			Page.Validate("StoreValidationGroup");
			if (!Page.IsValid) return;

			var storeId = GetIdOrNull();
			var service = IoC.Resolve<IStoreSecureService>();
			var store = storeId.HasValue ? service.GetById(storeId.Value) : service.Create();
			if (storeId.HasValue && store == null)
			{
				throw new BusinessObjectNotExistsException(storeId.Value);
			}

			store.Title = StoreTitleBox.Text;
			store.ErpId = string.IsNullOrEmpty(StoreErpBox.Text) ? null : StoreErpBox.Text;

			string description = StoreDescriptionEditor.GetValue();
			store.Description = description.IsEmpty() ? null : description;

			store.StatusId = System.Convert.ToInt32(StoreStatusList.SelectedValue, CultureInfo.CurrentCulture);
			store.Id = service.Save(SignInHelper.SignedInSystemUser, store);
			//Saving failed because store with the same title already exists
			if (store.Id < 0)
			{
				Messager.Add(Resources.ProductMessage.StoreTitleExists, InfoType.Failure);
			}
			//New store was saved successfully
			else if (!storeId.HasValue)
			{
				var editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", PathHelper.Assortment.Store.GetEditUrl(store.Id));
				Response.Redirect(editUrl);
			}
			//Store was updated successfully
			else
			{
				Messager.Add(Resources.GeneralMessage.SaveSuccessStore, InfoType.Success);
			}
		}

		protected void PopulateStoreInfo()
		{
			StoreStatusList.DataSource = IoC.Resolve<IStoreStatusSecureService>().GetAll();
			StoreStatusList.DataBind();

			var storeId = GetIdOrNull();
			//Create mode
			if (!storeId.HasValue)
			{
				StoreHeader.Text = Resources.Product.Literal_CreateStore;
				DeleteButton.Visible = false;
			}
			//Edit mode
			else
			{
				var store = IoC.Resolve<IStoreSecureService>().GetById(storeId.Value);
				StoreHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", 
					Resources.Product.Literal_EditStore, store.Title);
				StoreTitleBox.Text = store.Title;
				StoreErpBox.Text = store.ErpId;
				StoreDescriptionEditor.SetValue(store.Description);
				
				var status = StoreStatusList.Items.FindByValue(store.StatusId.ToString(CultureInfo.CurrentCulture));
				if (status != null)
				{
					status.Selected = true;
				}
			}
		}
	}
}
