﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/Stores/StoresMaster.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Stores.Default" %>
<%@ Import Namespace="Litium.Scensum.SiteStructure"%>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register Assembly="Litium.Scensum.BackOffice" Namespace="Litium.Scensum.BackOffice.UserControls.ContextMenu"
    TagPrefix="sc" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="StoresDefaultContent" runat="server" ContentPlaceHolderID="StoresPlaceHolder">
	<div>
		<sc:GridViewWithCustomPager
			ID="StoresGrid"
			SkinID="grid"
			runat="server"
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			DataSourceID="StoresObjectDataSource"
			Width="100%"
			PagerSettings-Mode="NumericFirstLast">
			<Columns>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<HeaderTemplate>
						<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
					</HeaderTemplate>
					<ItemTemplate>
						<asp:CheckBox ID="SelectCheckBox" runat="server"/>
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
					</ItemTemplate>
				</asp:TemplateField>	
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="60%">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="StoreEditLink" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Store.GetEditUrl(int.Parse(Eval("Id").ToString())) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:General,Literal_ErpId %>" DataField="ErpId" ItemStyle-Width="24%" />
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Status %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<sc:ContextMenu3 DataSource="<%# StatusesDataSource%>" SelectedValue='<%#((BlockStatusInfo) Eval("StatusId")).ToString() %>' ID="StatusList" runat="server"  ImageExpandSrc="~/Media/Images/Common/context-menu.png" ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png" MenuHeaderText="Status" OnSelectedValueChanged="DdlStatusSelectedValueChanged"></sc:ContextMenu3>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<ItemTemplate>
						<asp:ImageButton ID="StoreDeleteButton" runat="server" CommandName="DeleteStore"
							CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
							OnClientClick='<%# "return DeleteConfirmation(\""+Resources.Product.Literal_DeleteStore+"\");"%>' />
							
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
	<div id="AllSelectedDiv" runat="server" class="apply-to-selected-stores">
		<div style="float:left">
			<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
			<asp:DropDownList ID="StoreStatusList" DataTextField="Title" Width="100px" DataValueField="Id" runat="server" /> &nbsp;
		</div> 
		<div style="float:left">
			<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="SetStatusButton" runat="server" Text="<%$ Resources:General,Button_Set%>" />
		</div> 
	</div>
	<asp:ObjectDataSource ID="StoresObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Stores.StoresDataSource" />
</asp:Content>