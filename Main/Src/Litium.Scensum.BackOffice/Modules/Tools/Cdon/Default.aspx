﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Tools.Cdon.Default" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="uc" TagName="CategoryTree" Src="~/UserControls/Tree/SelectTreeView.ascx" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="CdonTab" ContentPlaceHolderID="body" runat="server">
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>

	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />

	<Scensum:ToolBoxPanel ID="CdonToolBoxPanel" runat="server" Text="Cdon export restrictions" />

	<div class="campaign-full-left">
		<div class="input-box campaign-action-value">
			<asp:Label ID="RestrictedProductsCountLabel" Text="Count of restricted products" runat="server" class="text-bold" />
			<br />
			<asp:Repeater runat="server" ID="RestrictedProductsCountGrid">
				<ItemTemplate>
					<asp:Literal ID="channelName" runat="server" Text='<%# GetChannel((int)Eval("Key"))%>' /> - 
					<asp:Literal ID="restrictedProductsCount" runat="server" Text='<%# Eval("Value") %>' />
					&nbsp;&nbsp;&nbsp;&nbsp;
				</ItemTemplate>
			</asp:Repeater>
		</div>
	</div>

	<div class="campaign-full-left">
		<div class="input-box campaign-action-value">
			<asp:Label ID="ProductGridPageSize" Text="<%$Resources:Campaign, Literal_ProductGridPageSize%>" runat="server" />
			<asp:DropDownList ID="ProductGridPageSizeSelect" runat="server" AutoPostBack="True">
				<asp:ListItem Value="10">10</asp:ListItem>
				<asp:ListItem Value="100">100</asp:ListItem>
				<asp:ListItem Selected="True" Value="250">250</asp:ListItem>
				<asp:ListItem Value="500">500</asp:ListItem>
			</asp:DropDownList>
		</div>
	</div>

	<br class="clear" />

	<div class="campaign-full-left">

	<!-- Include Toolbar Start -->
		
		<div id="excludeToolbar" style="margin-bottom: 10px;">
			<div class="collapsible-items left">
				<div id="ExcludeCollapsibleDiv" runat="server" class="collapsible-item left">
					<div class="collapsible-item-header left">
						<asp:Image ID="ExcludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/exclude.gif" />
						&nbsp; 
						<span><%=Resources.Campaign.Literal_Exclude%></span>
					</div>
					<div id="ExcludeCollapsibleCenterDiv" runat="server" class="action-block left">
						<div class="left">
							<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
						</div>
						<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<div class="link-button">
									<asp:Button ID="OpenExcludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
									<asp:Button ID="OpenExcludeCategorySearchPopupButton" Text="<%$Resources:Product, Button_Category%>" runat="server" />
									<asp:Button ID="OpenExcludeBrandSearchPopupButton" Text="<%$Resources:Product, Literal_Brand%>" runat="server" />
								</div>
							
								<!-- Category Search Popup Start -->
									<!-- Exclude Start -->
								<div id="CategoryExcludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
									<div class="campaign-popup-header">
										<div class="campaign-popup-header-left">
										</div>
										<div class="campaign-popup-header-center">
											<span><%= Resources.General.Literal_Search%></span>
											<input type="button" id="CategoryExcludePopupCloseButton" runat="server" value="x"/>
										</div>
										<div class="campaign-popup-header-right">
										</div>
									</div>
									<div class="campaign-popup-content-scrollable">
										<asp:UpdatePanel ID="CategoryExcludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
											<ContentTemplate>
												<uc:CategoryTree runat="server" ID="CategoryExcludePopupCategoryTree" />
											</ContentTemplate>
										</asp:UpdatePanel>
										<br />
										<div class="campaign-popup-buttons wide-padding">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryExcludePopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryExcludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</div>
								<ajaxToolkit:ModalPopupExtender 
									ID="CategoryExcludeSearchPopup" 
									runat="server" 
									TargetControlID="OpenExcludeCategorySearchPopupButton"
									PopupControlID="CategoryExcludePopupDiv" 
									CancelControlID="CategoryExcludePopupCloseButton" 
									BackgroundCssClass="PopupBackground" />
									<!-- Exclude End -->		
								<!-- Category Search Popup End -->

								<!-- Product Search Popup Start -->
									<!-- Exclude Start -->
								<div id="ProductExcludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
									<div class="campaign-popup-header">
										<div class="campaign-popup-header-left">
										</div>
										<div class="campaign-popup-header-center">
											<span><%= Resources.General.Literal_Search%></span>
											<input type="button" id="ProductExcludePopupCloseButton" runat="server" value="x"/>
										</div>
										<div class="campaign-popup-header-right">
										</div>
									</div>
									<div class="campaign-popup-content-scrollable">
										<asp:UpdatePanel ID="ProductExcludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
											<ContentTemplate>
												<div class="campaign-product-search">
													<div class="content-box">
														<asp:Panel ID="Panel2" DefaultButton="ProductExcludePopupSearchButton" runat="server">
														<uc:ProductSearchForm ID="ProductExcludeSearchFormControl" runat="server" />
														<br style="clear:both;" />
														<br />
														<div class="campaign-popup-buttons no-padding">
															<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
														</div>
														</asp:Panel>
														<br class="clear"/>
														<br />
														<uc:ProductSearchResult ID="ProductExcludeSearchResultControl" runat="server" />
													</div>
												</div>
											</ContentTemplate>
										</asp:UpdatePanel>
										<br />
										<div class="campaign-popup-buttons wide-padding">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</div>
								<ajaxToolkit:ModalPopupExtender 
									ID="ProductExcludeSearchPopup" 
									runat="server" 
									TargetControlID="OpenExcludeProductSearchPopupButton"
									PopupControlID="ProductExcludePopupDiv" 
									CancelControlID="ProductExcludePopupCloseButton" 
									BackgroundCssClass="PopupBackground" />
									<!-- Exclude End -->
								<!-- Product Search Popup End -->

								<!-- Brand Search Popup Start -->
									<!-- Exclude Start -->
								<div id="BrandExcludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
									<div class="campaign-popup-header">
										<div class="campaign-popup-header-left">
										</div>
										<div class="campaign-popup-header-center">
											<span><%= Resources.General.Literal_Search%></span>
											<input type="button" id="BrandExcludePopupCloseButton" runat="server" value="x"/>
										</div>
										<div class="campaign-popup-header-right">
										</div>
									</div>
									<div class="campaign-popup-content-scrollable" style="padding: 10px">
										<asp:UpdatePanel ID="BrandExcludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
											<ContentTemplate>
												<asp:GridView ID="BrandExcludeSearchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
													<Columns>
														<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
															<HeaderTemplate>
																<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
															</HeaderTemplate>
															<ItemTemplate>
																<asp:CheckBox ID="SelectCheckBox" runat="server" />
																<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
															</ItemTemplate>
														</asp:TemplateField>

														<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
															<ItemTemplate>
																<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
															</ItemTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>
											</ContentTemplate>
										</asp:UpdatePanel>
										<br />
										<div class="campaign-popup-buttons wide-padding">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandExcludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton" />
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandExcludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</div>
								<ajaxToolkit:ModalPopupExtender 
									ID="BrandExcludeSearchPopup" 
									runat="server" 
									TargetControlID="OpenExcludeBrandSearchPopupButton"
									PopupControlID="BrandExcludePopupDiv" 
									CancelControlID="BrandExcludePopupCloseButton" 
									BackgroundCssClass="PopupBackground" />
									<!-- Exclude End -->
								<!-- Brand Search Popup End -->
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
					<div class="image-block right">
						<asp:HiddenField ID="ExcludePanelStateHidden" runat="server" Value="true" />	
						<asp:Image ID="ExcludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
					</div>
				</div>
			</div>
			<br />
			<asp:UpdatePanel ID="ExcludeUpdatePanel" UpdateMode="Always" runat="server">
				<ContentTemplate>
					<div id="ExcludePanelDiv" runat="server" class="campaign-include-content">
						<asp:Panel ID="ExcludePanel" runat="server">
							<div style="padding-top:15px;clear:both;"></div>
							<asp:Label ID="Label3" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
							<br />
							<asp:UpdatePanel ID="ProductExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
								<ContentTemplate>
									<sc:GridViewWithCustomPager
										ID="ProductExcludeGrid"
										SkinID="grid"
										runat="server"
										AllowPaging="true"
										AutoGenerateColumns="false"
										Width="100%"
										DataSourceID="ExcludeProductDataSource">
										<Columns>
											<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<HeaderTemplate>
													<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox ID="SelectCheckBox" runat="server" />
													<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="10%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="27%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Category %>" ItemStyle-Width="13%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="3%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="3%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="TypeLiteral" Text='<%# GetProductType(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Channels" ItemStyle-Width="30%">
												<ItemTemplate>
													<asp:Repeater runat="server" ID="ChannelsRepeater">
														<ItemTemplate>
															<asp:HiddenField ID="hfChannelId" Value='<%#Eval("Id") %>' runat="server" />
															<asp:HiddenField ID="hfItemId" Value='<%#Eval("ItemId") %>' runat="server" />
															<asp:CheckBox ID="cbChannel" runat="server" Checked='<%# IsProductChannelExclude(DataBinder.GetDataItem(Container))%>' />
															<asp:Literal ID="channelName" runat="server" Text='<%# Eval("Country.Iso") %>' />
															&nbsp;&nbsp;&nbsp;
														</ItemTemplate>
													</asp:Repeater>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Reason" ItemStyle-Width="15%">
												<ItemTemplate>
													<asp:TextBox ID="reasonTextBox" runat="server" Text='<%# GetProductReasonExcludeGrid(DataBinder.GetDataItem(Container))%>' Width="100%" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<ItemTemplate>
													<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
														ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
														OnClientClick="return ConfirmProductRemove();" />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</sc:GridViewWithCustomPager>
									<asp:ObjectDataSource ID="ExcludeProductDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethodWithSortingByErpId" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />

									<div runat="server" id="ProductExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
										<div style="float:left;padding-top:6px;">
											<div class="apply-to-all">
												<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
											</div>
										</div>
										<div style="float:left">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected products');" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>

							<div style="padding-top:25px;clear:both;"></div>
							<asp:Label ID="Label4" Text="<%$Resources:Product, Label_Categories%>" runat="server" CssClass="text-bold"></asp:Label>
							<br />
							<asp:UpdatePanel ID="CategoryExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
								<ContentTemplate>
									<asp:GridView ID="CategoryExcludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
										<Columns>
											<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<HeaderTemplate>
													<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox ID="SelectCheckBox" runat="server" />
													<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Path %>">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="PathLiteral" Text='<%# GetCategoryPathExcludeGrid(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Channels" ItemStyle-Width="30%">
												<ItemTemplate>
													<asp:Repeater runat="server" ID="ChannelsRepeater">
														<ItemTemplate>
															<asp:HiddenField ID="hfChannelId" Value='<%#Eval("Id") %>' runat="server" />
															<asp:HiddenField ID="hfItemId" Value='<%#Eval("ItemId") %>' runat="server" />
															<asp:CheckBox ID="cbChannel" runat="server" Checked='<%# IsCategoryChannelExclude(DataBinder.GetDataItem(Container))%>' />
															<asp:Literal ID="channelName" runat="server" Text='<%# Eval("Country.Iso") %>' />
															&nbsp;&nbsp;&nbsp;
														</ItemTemplate>
													</asp:Repeater>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Reason" ItemStyle-Width="25%">
												<ItemTemplate>
													<asp:TextBox ID="reasonTextBox" runat="server" Text='<%# GetCategoryReasonExcludeGrid(DataBinder.GetDataItem(Container))%>' Width="100%" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<ItemTemplate>
													<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCategory" CommandArgument='<%# Eval("Id") %>'
														ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
														OnClientClick="return ConfirmCategoryRemove();" />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
									<div runat="server" id="CategoryExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
										<div style="float:left;padding-top:6px;">
											<div class="apply-to-all">
												<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
											</div>
										</div>
										<div style="float:left">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCategoryExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected categories');" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>

							<div style="padding-top:25px;clear:both;"></div>
							<asp:Label ID="Label5" Text="<%$Resources:Product, Literal_Brands%>" runat="server" CssClass="text-bold" />
							<br />
							<asp:UpdatePanel ID="BrandExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
								<ContentTemplate>
									<asp:GridView ID="BrandExcludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
										<Columns>
											<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<HeaderTemplate>
													<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox ID="SelectCheckBox" runat="server" />
													<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Channels" ItemStyle-Width="30%">
												<ItemTemplate>
													<asp:Repeater runat="server" ID="ChannelsRepeater">
														<ItemTemplate>
															<asp:HiddenField ID="hfChannelId" Value='<%#Eval("Id") %>' runat="server" />
															<asp:HiddenField ID="hfItemId" Value='<%#Eval("ItemId") %>' runat="server" />
															<asp:CheckBox ID="cbChannel" runat="server" Checked='<%# IsBrandChannelExclude(DataBinder.GetDataItem(Container))%>' />
															<asp:Literal ID="channelName" runat="server" Text='<%# Eval("Country.Iso") %>' />
															&nbsp;&nbsp;&nbsp;
														</ItemTemplate>
													</asp:Repeater>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Reason" ItemStyle-Width="25%">
												<ItemTemplate>
													<asp:TextBox ID="reasonTextBox" runat="server" Text='<%# GetBrandReasonExcludeGrid(DataBinder.GetDataItem(Container))%>' Width="100%" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<ItemTemplate>
													<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveBrand" CommandArgument='<%# Eval("Id") %>'
														ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
														OnClientClick="return ConfirmBrandRemove();" />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
									<div runat="server" id="BrandExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
										<div style="float:left;padding-top:6px;">
											<div class="apply-to-all">
												<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
											</div>
										</div>
										<div style="float:left">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromBrandExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected brands');" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</asp:Panel>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

	<!-- Include Toolbar End -->

		<br class="clear"/>
		<br />

	<!-- Esclude Toolbar Start -->

		<div id="includeToolbar">
			<div class="collapsible-items left">
				<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
					<div class="collapsible-item-header left">
						<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
						&nbsp; 
						<span><%=Resources.Campaign.Literal_Include%></span>
					</div>
					<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
						<div class="left">
							<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
						</div>
						<asp:UpdatePanel ID="IncludeUpdatePanel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<div class="link-button">
									<asp:Button ID="OpenIncludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
									<asp:HiddenField runat="server" ID="IncludeProductTarget"/>
									<asp:Button ID="OpenIncludeCategorySearchPopupButton" Text="<%$Resources:Product, Button_Category%>" runat="server" />
									<asp:HiddenField runat="server" ID="IncludeCategoryTarget"/>
									<asp:Button ID="OpenIncludeBrandSearchPopupButton" Text="<%$Resources:Product, Literal_Brand%>" runat="server" />
									<asp:HiddenField runat="server" ID="IncludeBrandTarget"/>
								</div>

								<!-- Category Search Popup Start -->
									<!-- Include Start -->
								<div id="CategoryIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
									<div class="campaign-popup-header">
										<div class="campaign-popup-header-left">
										</div>
										<div class="campaign-popup-header-center">
											<span><%= Resources.General.Literal_Search%></span>
											<input type="button" id="CategoryIncludePopupCloseButton" runat="server" value="x"/>
										</div>
										<div class="campaign-popup-header-right">
										</div>
									</div>
									<div class="campaign-popup-content-scrollable">
										<asp:UpdatePanel ID="CategoryIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
											<ContentTemplate>
												<uc:CategoryTree runat="server" ID="CategoryIncludePopupCategoryTree" />
											</ContentTemplate>
										</asp:UpdatePanel>
										<br />
										<div class="campaign-popup-buttons wide-padding">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryIncludePopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</div>
								<ajaxToolkit:ModalPopupExtender 
									ID="CategoryIncludeSearchPopup" 
									runat="server" 
									TargetControlID="IncludeCategoryTarget"
									PopupControlID="CategoryIncludePopupDiv" 
									CancelControlID="CategoryIncludePopupCloseButton" 
									BackgroundCssClass="PopupBackground" />
									<!-- Include End -->
								<!-- Category Search Popup End -->

								<!-- Product Search Popup Start -->
									<!-- Include Start -->
								<div id="ProductIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
									<div class="campaign-popup-header">
										<div class="campaign-popup-header-left">
										</div>
										<div class="campaign-popup-header-center">
											<span><%= Resources.General.Literal_Search%></span>
											<input type="button" id="ProductIncludePopupCloseButton" runat="server" value="x"/>
										</div>
										<div class="campaign-popup-header-right">
										</div>
									</div>
									<div class="campaign-popup-content-scrollable">
										<asp:UpdatePanel ID="ProductIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
											<ContentTemplate>
												<div class="campaign-product-search">
													<div class="content-box">
														<asp:Panel ID="SearchPanel" DefaultButton="ProductIncludePopupSearchButton" runat="server">
														<uc:ProductSearchForm ID="ProductIncludeSearchFormControl" runat="server" />
														<br style="clear:both;" />
														<br />
														<div class="campaign-popup-buttons no-padding">
															<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
														</div>
														</asp:Panel>
														<br class="clear"/>
														<br />
														<uc:ProductSearchResult ID="ProductIncludeSearchResultControl" runat="server" />
													</div>
												</div>
											</ContentTemplate>
										</asp:UpdatePanel>
										<br />
										<div class="campaign-popup-buttons wide-padding">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</div>
								<ajaxToolkit:ModalPopupExtender 
									ID="ProductIncludeSearchPopup" 
									runat="server" 
									TargetControlID="IncludeProductTarget"
									PopupControlID="ProductIncludePopupDiv" 
									CancelControlID="ProductIncludePopupCloseButton" 
									BackgroundCssClass="PopupBackground" />
									<!-- Include End -->
								<!-- Product Search Popup End -->

								<!-- Brand Search Popup Start -->
									<!-- Include Start -->
								<div id="BrandIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
									<div class="campaign-popup-header">
										<div class="campaign-popup-header-left">
										</div>
										<div class="campaign-popup-header-center">
											<span><%= Resources.General.Literal_Search%></span>
											<input type="button" id="BrandIncludePopupCloseButton" runat="server" value="x"/>
										</div>
										<div class="campaign-popup-header-right">
										</div>
									</div>
									<div class="campaign-popup-content-scrollable" style="padding: 10px">
										<asp:UpdatePanel ID="BrandIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
											<ContentTemplate>
												<asp:GridView ID="BrandIncludeSearchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
													<Columns>
														<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
															<HeaderTemplate>
																<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
															</HeaderTemplate>
															<ItemTemplate>
																<asp:CheckBox ID="SelectCheckBox" runat="server" />
																<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
															</ItemTemplate>
														</asp:TemplateField>

														<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
															<ItemTemplate>
																<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
															</ItemTemplate>
														</asp:TemplateField>
													</Columns>
												</asp:GridView>
											</ContentTemplate>
										</asp:UpdatePanel>
										<br />
										<div class="campaign-popup-buttons wide-padding">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
										</div>
									</div>
								</div>
								<ajaxToolkit:ModalPopupExtender 
									ID="BrandIncludeSearchPopup" 
									runat="server" 
									TargetControlID="IncludeBrandTarget"
									PopupControlID="BrandIncludePopupDiv" 
									CancelControlID="BrandIncludePopupCloseButton" 
									BackgroundCssClass="PopupBackground" />
									<!-- Include End -->
								<!-- Brand Search Popup End -->
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
					<div class="image-block right">
						<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
						<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
					</div>
				</div>
			</div>
			<br />
			<div id="IncludePanelDiv" runat="server" class="campaign-include-content">
				<asp:Panel ID="IncludePanel" runat="server">
					<div style="padding-top:15px;clear:both;"></div>
					<asp:Label ID="LabelProducts" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
					<br />
					<asp:UpdatePanel ID="ProductIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
							<sc:GridViewWithCustomPager
								ID="ProductIncludeGrid"
								SkinID="grid"
								runat="server"
								AllowPaging="true"
								AutoGenerateColumns="false"
								Width="100%"
								DataSourceID="IncludeProductDataSource">
								<Columns>
									<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<HeaderTemplate>
											<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
										</HeaderTemplate>
										<ItemTemplate>
											<asp:CheckBox ID="SelectCheckBox" runat="server" />
											<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="10%">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="27%">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Category %>" ItemStyle-Width="13%">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="3%">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="3%">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server"  Text='<%# GetProductType(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Channels" ItemStyle-Width="30%">
										<ItemTemplate>
											<asp:Repeater runat="server" ID="ChannelsRepeater">
												<ItemTemplate>
													<asp:HiddenField ID="hfChannelId" Value='<%#Eval("Id") %>' runat="server" />
													<asp:HiddenField ID="hfItemId" Value='<%#Eval("ItemId") %>' runat="server" />
													<asp:CheckBox ID="cbChannel" runat="server" Checked='<%# IsProductChannelInclude(DataBinder.GetDataItem(Container))%>' />
													<asp:Literal ID="channelName" runat="server" Text='<%# Eval("Country.Iso") %>' />
													&nbsp;&nbsp;&nbsp;
												</ItemTemplate>
											</asp:Repeater>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Reason" ItemStyle-Width="15%">
										<ItemTemplate>
											<asp:TextBox ID="reasonTextBox" runat="server" Text='<%# GetProductReasonIncludeGrid(DataBinder.GetDataItem(Container))%>' Width="100%" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<ItemTemplate>
											<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
												ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
												OnClientClick="return ConfirmProductRemove();" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</sc:GridViewWithCustomPager>
							<asp:ObjectDataSource ID="IncludeProductDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethodWithSortingByErpId" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />

							<div runat="server" id="ProductIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
								<div style="float:left;padding-top:6px;">
									<div class="apply-to-all">
										<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
									</div>
								</div>
								<div style="float:left">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected products');" runat="server" SkinID="DefaultButton"/>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>

					<div style="padding-top:25px;clear:both;"></div>
					<asp:Label ID="Label1" Text="<%$Resources:Product, Label_Categories%>" runat="server" CssClass="text-bold"></asp:Label>
					<br />
					<asp:UpdatePanel ID="CategoryIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
							<asp:GridView ID="CategoryIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
								<Columns>
									<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<HeaderTemplate>
											<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
										</HeaderTemplate>
										<ItemTemplate>
											<asp:CheckBox ID="SelectCheckBox" runat="server" />
											<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Path %>">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server" ID="PathLiteral" Text='<%# GetCategoryPathIncludeGrid(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Channels" ItemStyle-Width="30%">
										<ItemTemplate>
											<asp:Repeater runat="server" ID="ChannelsRepeater">
												<ItemTemplate>
													<asp:HiddenField ID="hfChannelId" Value='<%#Eval("Id") %>' runat="server" />
													<asp:HiddenField ID="hfItemId" Value='<%#Eval("ItemId") %>' runat="server" />
													<asp:CheckBox ID="cbChannel" runat="server" Checked='<%# IsCategoryChannelInclude(DataBinder.GetDataItem(Container))%>' />
													<asp:Literal ID="channelName" runat="server" Text='<%# Eval("Country.Iso") %>' />
													&nbsp;&nbsp;&nbsp;
												</ItemTemplate>
											</asp:Repeater>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Reason" ItemStyle-Width="25%">
										<ItemTemplate>
											<asp:TextBox ID="reasonTextBox" runat="server" Text='<%# GetCategoryReasonIncludeGrid(DataBinder.GetDataItem(Container))%>' Width="100%" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<ItemTemplate>
											<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCategory" CommandArgument='<%# Eval("Id") %>'
												ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
												OnClientClick="return ConfirmCategoryRemove();" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
							<div runat="server" id="CategoryIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
								<div style="float:left;padding-top:6px;">
									<div class="apply-to-all">
										<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
									</div>
								</div>
								<div style="float:left">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCategoryIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected categories');" runat="server" SkinID="DefaultButton"/>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>

					<div style="padding-top:25px;clear:both;"></div>
					<asp:Label ID="Label2" Text="<%$Resources:Product, Literal_Brands%>" runat="server" CssClass="text-bold" />
					<br />
					<asp:UpdatePanel ID="BrandIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
						<ContentTemplate>
							<asp:GridView ID="BrandIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
								<Columns>
									<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<HeaderTemplate>
											<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
										</HeaderTemplate>
										<ItemTemplate>
											<asp:CheckBox ID="SelectCheckBox" runat="server" />
											<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
										<ItemTemplate>
											<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Channels" ItemStyle-Width="30%">
										<ItemTemplate>
											<asp:Repeater runat="server" ID="ChannelsRepeater">
												<ItemTemplate>
													<asp:HiddenField ID="hfChannelId" Value='<%#Eval("Id") %>' runat="server" />
													<asp:HiddenField ID="hfItemId" Value='<%#Eval("ItemId") %>' runat="server" />
													<asp:CheckBox ID="cbChannel" runat="server" Checked='<%# IsBrandChannelInclude(DataBinder.GetDataItem(Container))%>' />
													<asp:Literal ID="channelName" runat="server" Text='<%# Eval("Country.Iso") %>' />
													&nbsp;&nbsp;&nbsp;
												</ItemTemplate>
											</asp:Repeater>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Reason" ItemStyle-Width="25%">
										<ItemTemplate>
											<asp:TextBox ID="reasonTextBox" runat="server" Text='<%# GetBrandReasonIncludeGrid(DataBinder.GetDataItem(Container))%>' Width="100%" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<ItemTemplate>
											<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveBrand" CommandArgument='<%# Eval("Id") %>'
												ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
												OnClientClick="return ConfirmBrandRemove();" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
							<div runat="server" id="BrandIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
								<div style="float:left; padding-top:6px;">
									<div class="apply-to-all">
										<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
									</div>
								</div>
								<div style="float:left">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromBrandIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected brands');" runat="server" SkinID="DefaultButton"/>
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</asp:Panel>
			</div>
		</div>

	<!-- Esclude Toolbar End -->

	</div>

	<br class="clear" />
	<br />
	<div style="float: right">
		<uc:ImageLinkButton UseSubmitBehaviour="true" ID="btnSave" Text="<%$Resources:General, Button_Save %>" runat="server" SkinID="DefaultButton" />
	</div>
</asp:Content>