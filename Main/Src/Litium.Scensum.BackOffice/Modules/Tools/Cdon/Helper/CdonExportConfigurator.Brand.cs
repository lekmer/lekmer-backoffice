using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.Modules.Tools.Cdon
{
	public class CdonExportConfiguratorBrand
	{
		private CdonExportConfiguratorHelper _cdonExportConfiguratorHelper;
		private CdonExportConfiguratorHelper CdonExportConfiguratorHelper
		{
			get { return _cdonExportConfiguratorHelper ?? (_cdonExportConfiguratorHelper = new CdonExportConfiguratorHelper()); }
		}

		private BrandCollection _brandCollection;
		public BrandCollection BrandCollection
		{
			get { return _brandCollection ?? (_brandCollection = IoC.Resolve<IBrandSecureService>().GetAll()); }
		}

		public GridView Grid { get; set; }
		public GridView SearchGrid { get; set; }
		public ImageLinkButton OkButton { get; set; }
		public ImageLinkButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public UpdatePanel PopupUpdatePanel { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public Collection<ICdonExportRestrictionItem> Brands { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;

			SearchGrid.RowDataBound += SearchGridRowDataBound;

			OkButton.Click += OkButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(Collection<ICdonExportRestrictionItem> brands)
		{
			DataBindSearchGrid();
			DataBindGrid(CdonExportConfiguratorHelper.ResolveBrands(ConvertToBrandDictionary(brands)));
		}

		public virtual void DataBindGrid(Collection<IBrand> brands)
		{
			Grid.DataSource = brands;
			Grid.DataBind();
			GridUpdatePanel.Update();
		}

		protected virtual void DataBindSearchGrid()
		{
			SearchGrid.DataSource = BrandCollection;
			SearchGrid.DataBind();
			PopupUpdatePanel.Update();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CdonExportConfiguratorHelper.SetSelectionFunction((GridView)sender, e.Row, ApplyToAllDiv, false);
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var brand = (IBrand)e.Row.DataItem;

				CdonExportConfiguratorHelper.PopulateChannels(e.Row, brand.Id);
			}
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveBrand"))
			{
				Update();
				RemoveBrand(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void SearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CdonExportConfiguratorHelper.SetSelectionFunction((GridView)sender, e.Row, null, false);
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			Update();

			var brandIds = CdonExportConfiguratorHelper.GetSelectedIdsFromGrid(SearchGrid, "IdHiddenField");

			foreach (var brandId in brandIds)
			{
				if (Brands.FirstOrDefault(b => b.ItemId == brandId) == null)
				{
					var brand = IoC.Resolve<ICdonExportRestrictionItem>();
					brand.ItemId = brandId;

					Brands.Add(brand);
				}
			}

			DataBindGrid(CdonExportConfiguratorHelper.ResolveBrands(ConvertToBrandDictionary(Brands)));

			if (IncludeAllCheckbox != null && Brands.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}

			CdonExportConfiguratorHelper.ClearSelectedIdsFromGrid(SearchGrid);
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			Update();

			foreach (var brandId in GetSelectedFromGrid())
			{
				var cdonExportRestrictionBrand = Brands.FirstOrDefault(b => b.ItemId == brandId);
				if (cdonExportRestrictionBrand != null)
				{
					Brands.Remove(cdonExportRestrictionBrand);
				}
			}

			DataBindGrid(CdonExportConfiguratorHelper.ResolveBrands(ConvertToBrandDictionary(Brands)));
		}

		protected virtual void RemoveBrand(int brandId)
		{
			var cdonExportRestrictionBrand = Brands.FirstOrDefault(b => b.ItemId == brandId);
			if (cdonExportRestrictionBrand != null)
			{
				Brands.Remove(cdonExportRestrictionBrand);
			}

			DataBindGrid(CdonExportConfiguratorHelper.ResolveBrands(ConvertToBrandDictionary(Brands)));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CdonExportConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}

		private Dictionary<int, int> ConvertToBrandDictionary(Collection<ICdonExportRestrictionItem> brands)
		{
			return brands == null || brands.Count == 0 ? new Dictionary<int, int>() : brands.ToDictionary(b => b.ItemId, b => b.ItemId);
		}

		public bool Update()
		{
			foreach (GridViewRow row in Grid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				// Reason.
				var id = int.Parse(((HiddenField) row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var reason = ((TextBox) row.FindControl("reasonTextBox")).Text;

				var brand = Brands.FirstOrDefault(b => b.ItemId == id);
				if (brand != null)
				{
					brand.Reason = reason;

					// Channels.
					var channelsRepeater = (Repeater)row.FindControl("ChannelsRepeater");
					if (channelsRepeater != null)
					{
						if (brand.Channels == null)
						{
							brand.Channels = new Dictionary<int, int>();
						}
						else
						{
							brand.Channels.Clear();
						}

						foreach (RepeaterItem item in channelsRepeater.Items)
						{
							var hfChannelId = (HiddenField)item.FindControl("hfChannelId");
							var cbChannel = (CheckBox)item.FindControl("cbChannel");
							int channelId;
							if (hfChannelId != null && cbChannel != null && int.TryParse(hfChannelId.Value, out channelId))
							{
								if (cbChannel.Checked && !brand.Channels.ContainsKey(channelId))
								{
									brand.Channels.Add(channelId, channelId);
								}
							}
						}
					}
				}
			}

			return true;
		}
	}
}