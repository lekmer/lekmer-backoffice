﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Tools.Cdon
{
	public class CdonExportConfiguratorHelper
	{
		public readonly string DOWN_IMG = "~/Media/Images/Common/down.gif";
		public readonly string UP_IMG = "~/Media/Images/Common/up.gif";

		private Collection<IChannel> _channels;
		public Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = IoC.Resolve<IChannelSecureService>().GetAll()); }
		}

		public Collection<int> GetSelectedIdsFromGrid(GridView gridView, string idHiddenFieldName)
		{
			var ids = new Collection<int>();

			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					int id = Convert.ToInt32(((HiddenField)row.FindControl(idHiddenFieldName)).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}

			return ids;
		}

		public void ClearSelectedIdsFromGrid(GridView gridView)
		{
			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox != null)
				{
					selectCheckBox.Checked = false;
				}
			}

			GridViewRow headerRow = gridView.HeaderRow;
			if (headerRow != null)
			{
				var selectAllCheckBox = (CheckBox)headerRow.FindControl("SelectAllCheckBox");
				if (selectAllCheckBox != null)
				{
					selectAllCheckBox.Checked = false;
				}
			}
		}

		public bool HasAnySelectedItem(GridView grid)
		{
			foreach (GridViewRow row in grid.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					return true;
				}
			}

			return false;
		}

		public void SetSelectionFunction(GridView grid, GridViewRow row, HtmlGenericControl applyAllDiv, bool isVoucherControl)
		{
			string scriptShowBlockOption = applyAllDiv != null ? @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');" : string.Empty;

			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", @"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');" + scriptShowBlockOption);
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", @"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');" + scriptShowBlockOption);

				if (isVoucherControl)
				{
					var batchLiteral = (Label)row.FindControl("BatchIdLiteral");
					batchLiteral.Text = ((int)row.DataItem).ToString(CultureInfo.CurrentCulture);

					var batchIdHiddenField = (HiddenField)row.FindControl("BatchIdHiddenField");
					batchIdHiddenField.Value = ((int)row.DataItem).ToString(CultureInfo.CurrentCulture);
				}
			}
		}

		public Collection<ICategory> ResolveCategories(CategoryIdDictionary categoryIds)
		{
			var categories = new Collection<ICategory>();
			foreach (var id in categoryIds)
			{
				var category = IoC.Resolve<ICategorySecureService>().GetById(id.Value);
				if (category == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Category with ID {0} could not be found.",
						id));
				}
				categories.Add(category);
			}
			return categories;
		}

		public Collection<ICategory> ResolveCategories(Dictionary<int, int> categoryIds)
		{
			return ResolveCategories(new CategoryIdDictionary(categoryIds.Keys));
		}

		public Collection<IBrand> ResolveBrands(Dictionary<int, int> brandIds)
		{
			var brands = new Collection<IBrand>();

			foreach (var id in brandIds)
			{
				var brand = IoC.Resolve<IBrandSecureService>().GetById(id.Value);
				if (brand == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Brand with ID {0} could not be found.",
						id.Value));
				}
				brands.Add(brand);
			}

			return brands;
		}

		public void PopulateChannels(GridViewRow row, int itemId)
		{
			var channelsRepeater = (Repeater)row.FindControl("ChannelsRepeater");
			if (channelsRepeater != null)
			{
				var channels = new Collection<CustomChannel>();
				foreach (var channel in Channels)
				{
					var customChannel = new CustomChannel(channel);
					customChannel.ItemId = itemId;

					channels.Add(customChannel);
				}

				channelsRepeater.DataSource = channels;
				channelsRepeater.DataBind();
			}
		}
	}

	public class CustomChannel : Channel
	{
		public CustomChannel(IChannel channel)
		{
			Id = channel.Id;
			Title = channel.Title;
			CommonName = channel.CommonName;
			ApplicationName = channel.ApplicationName;
			AlternateLayoutRatio = channel.AlternateLayoutRatio;
			Country = channel.Country;
			Currency = channel.Currency;
			Language = channel.Language;
			Culture = channel.Culture;
		}

		public int ItemId { get; set; }
	}
}