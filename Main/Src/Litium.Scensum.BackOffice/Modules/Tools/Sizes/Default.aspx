﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Tools.Sizes.Default" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="SizesTab" ContentPlaceHolderID="body" runat="server">
	<Scensum:ToolBoxPanel ID="ImportSizeToolBoxPanel" runat="server" Text="Import new size" />
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="ImportSizeButton" runat="server" />
	<uc:ScensumValidationSummary ID="ImportSizeValidationSummary" CssClass="advance-validation-summary" ValidationGroup="ImportSize" runat="server" />

	<div class="stores-place-holder">
		<div class="column">
			<div class="input-box">
				<span class="text-bold"><%= Resources.General.Literal_ErpId%></span>
				<asp:RequiredFieldValidator
					runat="server"
					ID="ErpIdValidator"
					ControlToValidate="ErpIdTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,ErpIdEmpty %>"
					Display ="None"
					ValidationGroup="ImportSize" />
				<br />
				<asp:TextBox ID="ErpIdTextBox" Width="250px" runat="server" />
			</div>
			<div class="input-box">
				<span class="text-bold"><%= Resources.General.Literal_Value %></span>
				<asp:RequiredFieldValidator
					runat="server"
					ID="ValueValidator"
					ControlToValidate="ValueTextBox"
					ErrorMessage="<%$ Resources:ProductMessage,ValueEmpty %>"
					Display ="None"
					ValidationGroup="ImportSize" />
				<br />
				<asp:TextBox ID="ValueTextBox" Width="250px" runat="server" />
			</div>
		</div>
	</div>

	<div class="right">
		<div class="input-box">
			<uc:ImageLinkButton ID="ImportSizeButton" Text="<%$ Resources:General,Button_Add %>" ValidationGroup="ImportSize" SkinID="DefaultButton" UseSubmitBehaviour="true" runat="server" />
		</div>
	</div>
</asp:Content>