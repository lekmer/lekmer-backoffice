﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Payment.Qliro;
using Litium.Lekmer.Payment.Qliro.Cache;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Scensum.BackOffice.Modules.Tools.Qliro
{
	public partial class Default : LekmerPageController
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IQliroPaymentTypeSecureService _qliroPaymentTypeSecureService;
		private IQliroPaymentTypeSecureService QliroPaymentTypeSecureService
		{
			get { return _qliroPaymentTypeSecureService ?? (_qliroPaymentTypeSecureService = IoC.Resolve<IQliroPaymentTypeSecureService>()); }
		}

		protected override void SetEventHandlers()
		{
			RefreshCurrentQliroPaymentsButton.Click += RefreshCurrentQliroPaymentsButtonClick;
			GetNewQliroPaymentsButton.Click += GetNewQliroPaymentsButtonClick;
			UpdateQliroPaymentsButton.Click += UpdateQliroPaymentsButtonClick;
		}

		protected override void PopulateForm()
		{
			CurrentChannelLabel.Text = string.Format(CurrentChannelLabel.Text, ChannelHelper.CurrentChannel.CommonName);

			PopulateCurrentQliroPayments();

			NewQliroPaymentsGrid.DataSource = null;
			NewQliroPaymentsGrid.DataBind();
		}


		protected void RefreshCurrentQliroPaymentsButtonClick(object sender, EventArgs e)
		{
			PopulateCurrentQliroPayments();
		}

		protected void GetNewQliroPaymentsButtonClick(object sender, EventArgs e)
		{
			try
			{
				IQliroClient qliroClient = QliroFactory.CreateQliroClient(ChannelHelper.CurrentChannel.CommonName);
				IQliroGetPaymentTypesResult qliroGetPaymentTypesResult = qliroClient.GetPaymentTypes(ChannelHelper.CurrentChannel);

				NewQliroPaymentsGrid.DataSource = qliroGetPaymentTypesResult.PaymentTypes;
				NewQliroPaymentsGrid.DataBind();
			}
			catch (Exception ex)
			{
				_log.Error("Get new Qliro payments failed!", ex);
				Messager.Add("Get new Qliro payments failed! " + ex.Message, InfoType.Failure);

				NewQliroPaymentsGrid.DataSource = null;
				NewQliroPaymentsGrid.DataBind();
			}
		}

		protected void UpdateQliroPaymentsButtonClick(object sender, EventArgs e)
		{
			try
			{
				IChannel channel = ChannelHelper.CurrentChannel;

				Collection<IQliroPaymentType> currentQliroPaymentTypes = QliroPaymentTypeSecureService.GetAllByChannel(channel);

				IQliroClient qliroClient = QliroFactory.CreateQliroClient(channel.CommonName);
				IQliroGetPaymentTypesResult qliroGetPaymentTypesResult = qliroClient.GetPaymentTypes(channel);

				// Remove payment types that no longer exist
				foreach (IQliroPaymentType qliroPaymentType in currentQliroPaymentTypes)
				{
					if (qliroGetPaymentTypesResult.PaymentTypes.Any(pt => pt.Code == qliroPaymentType.Code) == false)
					{
						QliroPaymentTypeSecureService.Delete(SignInHelper.SignedInSystemUser, qliroPaymentType.Id);
					}
				}

				// Update existed and insert new payment types
				foreach (IQliroPaymentType qliroPaymentType in qliroGetPaymentTypesResult.PaymentTypes)
				{
					qliroPaymentType.ChannelId = channel.Id;
					qliroPaymentType.Id = QliroPaymentTypeSecureService.Save(SignInHelper.SignedInSystemUser, qliroPaymentType);
				}

				// Cache clean up
				QliroPaymentTypesCache.Instance.Flush();

				PopulateCurrentQliroPayments();

				NewQliroPaymentsGrid.DataSource = qliroGetPaymentTypesResult.PaymentTypes;
				NewQliroPaymentsGrid.DataBind();

				Messager.Add("Qliro payments succesfully updated!", InfoType.Success);
			}
			catch (Exception ex)
			{
				_log.Error("Update Qliro payments failed!", ex);
				Messager.Add("Update Qliro payments failed! " + ex.Message, InfoType.Failure);
			}
		}


		protected void PopulateCurrentQliroPayments()
		{
			Collection<IQliroPaymentType> qliroPaymentTypes = QliroPaymentTypeSecureService.GetAllByChannel(ChannelHelper.CurrentChannel);

			QliroPaymentsGrid.DataSource = qliroPaymentTypes;
			QliroPaymentsGrid.DataBind();
		}
	}
}