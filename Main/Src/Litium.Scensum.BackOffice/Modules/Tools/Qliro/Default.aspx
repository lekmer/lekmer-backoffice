﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Tools.Qliro.Default" %>

<%@ Import Namespace="Litium.Lekmer.Payment.Qliro" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="QliroTab" ContentPlaceHolderID="body" runat="server">
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="UpdateQliroPaymentsButton" runat="server" />

	<Scensum:ToolBoxPanel ID="QliroToolBoxPanel" runat="server" Text="Qliro payments importer" />

	<div class="column">
		<div class="input-box main-caption" style="color: darkgreen;">
			<asp:Label ID="CurrentChannelLabel" runat="server" Text="<%$ Resources:Tools, Literal_CurrentChannel %>" />
		</div>
	</div>

	<br class="clear" />
	<hr />

	<div class="right">
		<div class="input-box">
			<uc:ImageLinkButton ID="RefreshCurrentQliroPaymentsButton" Text="Refresh" SkinID="DefaultButton" UseSubmitBehaviour="true" runat="server" />
		</div>
	</div>

	<br class="clear" />

	<div>
		<span class="main-caption">Payment types in use</span>

		<asp:GridView ID="QliroPaymentsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
			<Columns>
				<asp:TemplateField HeaderText="Code">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).Code %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Description">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).Description %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Registration Fee">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).RegistrationFee %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Settlement Fee">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).SettlementFee %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Interest Rate">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).InterestRate %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Interest Type">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).InterestType %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Interest Calculation">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).InterestCalculation %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="NoOfMonths">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).NoOfMonths %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="MinPurchaseAmount">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).MinPurchaseAmount %>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				No payment types were found!
			</EmptyDataTemplate>
		</asp:GridView>

	</div>

	<br class="clear" />
	<hr />

	<div class="right">
		<div class="input-box">
			<uc:ImageLinkButton ID="GetNewQliroPaymentsButton" Text="Get new from Qliro" SkinID="DefaultButton" UseSubmitBehaviour="true" runat="server" />
		</div>
	</div>

	<br class="clear" />

	<div>
		<span class="main-caption">Payment types from Qliro</span>

		<asp:GridView ID="NewQliroPaymentsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
			<Columns>
				<asp:TemplateField HeaderText="Code">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).Code %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Description">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).Description %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Registration Fee">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).RegistrationFee %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Settlement Fee">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).SettlementFee %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Interest Rate">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).InterestRate %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Interest Type">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).InterestType %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Interest Calculation">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).InterestCalculation %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="NoOfMonths">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).NoOfMonths %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="MinPurchaseAmount">
					<ItemTemplate>
						<%# ((IQliroPaymentType)Container.DataItem).MinPurchaseAmount %>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				No payment types were found!
			</EmptyDataTemplate>
		</asp:GridView>
	</div>

	<br class="clear" />
	<hr />

	<div class="right">
		<div class="input-box">
			<uc:ImageLinkButton ID="UpdateQliroPaymentsButton" Text="Update Qliro payments" SkinID="DefaultButton" UseSubmitBehaviour="true" runat="server" />
		</div>
	</div>
</asp:Content>