﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Payment.Collector;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Scensum.BackOffice.Modules.Tools.Collector
{
	public partial class Default : LekmerPageController
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private ICollectorPaymentTypeSecureService _collectorPaymentTypeSecureService;
		private ICollectorPaymentTypeSecureService CollectorPaymentTypeSecureService
		{
			get { return _collectorPaymentTypeSecureService ?? (_collectorPaymentTypeSecureService = IoC.Resolve<ICollectorPaymentTypeSecureService>()); }
		}

		protected override void SetEventHandlers()
		{
			RefreshCurrentCollectorPaymentsButton.Click += RefreshCurrentCollectorPaymentsButtonClick;
		}

		protected override void PopulateForm()
		{
			CurrentChannelLabel.Text = string.Format(CurrentChannelLabel.Text, ChannelHelper.CurrentChannel.CommonName);

			PopulateCurrentCollectorPayments();
		}


		protected void RefreshCurrentCollectorPaymentsButtonClick(object sender, EventArgs e)
		{
			PopulateCurrentCollectorPayments();
		}

		protected void PopulateCurrentCollectorPayments()
		{
			Collection<ICollectorPaymentType> collectorPaymentTypes = CollectorPaymentTypeSecureService.GetAllByChannel(ChannelHelper.CurrentChannel);

			CollectorPaymentsGrid.DataSource = collectorPaymentTypes;
			CollectorPaymentsGrid.DataBind();
		}
	}
}