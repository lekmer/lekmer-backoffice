﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="OrderEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.Orders.OrderEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="uc" TagName="OrderAddressEdit" Src="../../../UserControls/Customer/Order/OrderAddressEdit.ascx" %>
<%@ Register TagPrefix="uc" TagName="OrderPaymentsControl" Src="../../../UserControls/Customer/Order/OrderPaymentsControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="OrderCommentControl" Src="../../../UserControls/Customer/Order/OrderCommentControl.ascx" %>
<%@ Register Tagprefix="uc" TagName="OrderAuditListControl" Src="../../../UserControls/Customer/Order/OrderAuditListControl.ascx" %>

<asp:Content ID="OrderEditContent" ContentPlaceHolderID="body" runat="server">
	<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="SaveButton">
		<Scensum:ToolBoxPanel ID="VariationPanelToolBoxPanel" runat="server" Text="<%$ Resources:Customer, Literal_OrderToolBoxPanel %>" ShowSeparator="false" />
		<div style="width: 980px;float:left;margin-bottom:0px">
			<asp:UpdatePanel ID="MessagesContainerUpdatePanel" runat="server">
				<ContentTemplate>
					<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<br class="clear" />

		<div runat="server" ID="SplitNotPerformedDiv" class="order-header-row">
			<asp:Label runat="server" ID="OrderLabel" CssClass="order-header left" />
			<asp:Label runat="server" ID="OrderDateLabel" CssClass="order-header right" />
		</div>
		<div runat="server" ID="SplitPerformedDiv" class="order-main-row">
			<ul class="split-order-menu">
				<li>
					<div runat="server" ID="OriginalOrderDiv">
						<div class="left-corner"></div>
						<div class="menu-item">
							<asp:LinkButton runat="server" ID="OriginalOrderButton" />
						</div>
						<div class="right-corner"></div>
					</div>
				</li>
				<li>
					<div ID="SplitOrderDiv" runat="server">
						<div class="left-corner"></div>
						<div class="menu-item">
							<asp:LinkButton runat="server" ID="SplitOrderButton" />
						</div>
						<div class="right-corner"></div>
					</div>
				</li>
			</ul>
			<br clear="all" />
		</div>

		<div id="order-edit" class="order-edit-container">
			<div id="order-edit-container-left">
				<div class="order-address">
					<label class="assortment-header"><%= Resources.Customer.Literal_Customer_Header%></label>
					<br />
					<div class="input-box">
						<div class="column">
							<div><%= Resources.Customer.Literal_Customer_Header%></div>
							<div class="info"><asp:LinkButton runat="server" id="CustomerLinkButton" CssClass="blue" /></div>
							<div><asp:Literal runat="server" ID="CustomerCivicNumberLabel" /></div>
							<div class="info"><asp:Literal runat="server" Mode="Encode" id="CustomerCivicNumberLiteral" /></div>
						</div>
						<div class="column">
							<div><%= Resources.Customer.Literal_ContactInfo%></div>
							<div class="info"><asp:Literal runat="server" id="CustomerInfoLiteral" /></div>
						</div>
						<div class="column">
							<div><%= Resources.Lekmer.Customer_CustomerType%></div>
							<div class="info"><asp:Literal runat="server" Id="CustomerTypeLiteral" /></div>
						</div>
					</div>
				</div>

				<br clear="all" />
				<hr />

				<div class="order-address">
					<label class="assortment-header"><%= Resources.Customer.Literal_DeliveryAddress%></label>
					<br />
					<uc:OrderAddressEdit ID="DeliveryAddressEdit" runat="server" />
				</div>

				<div class="order-address">
					<label class="assortment-header"><%= Resources.Customer.Literal_BillingAddress%></label>
					<br />
					<uc:OrderAddressEdit ID="BillingAddressEdit" runat="server" />
				</div>

				<div id="AlternateAddressPanel" runat="server" class="order-address order-alternate-address">
					<label class="assortment-header"><%= Resources.Lekmer.Customer_AlternateAddress%></label>
					<br />
					<uc:OrderAddressEdit ID="AlternateAddressEdit" runat="server" />
				</div>

				<br clear="all" />
				<hr />

				<div id="order-items">
					<asp:GridView ID="OrderItemGrid" SkinID="grid" runat="server" Width="740" AutoGenerateColumns="false">
						<Columns>
							<asp:TemplateField HeaderStyle-CssClass="grid-header-left" ItemStyle-CssClass="grid-border-left"
								HeaderStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
								ItemStyle-Width="2%">
								<HeaderTemplate>
									<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
								</HeaderTemplate>
								<ItemTemplate>
									<asp:CheckBox ID="SelectCheckBox" runat="server" />
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_ArticleNr %>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
								<ItemTemplate>
									<asp:Label ID="ErpIdLabel" runat="server" ToolTip="HYID" ></asp:Label>
									<asp:Label runat="server" ToolTip="Ean code" Text='<%# Eval("EanCode") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Title %>" ItemStyle-Width="27%">
								<ItemTemplate>
									<div style="float: left; max-width: 96%;"><%# Eval("Title") %></div>
									<div style="float: left">
										<uc:ContextMenu ID="OrderItemContextMenu" runat="server" CallerImageSrc="~/Media/Images/Common/context-menu.png"
											CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png" MenuContainerCssClass="context-menu-container"
											MenuShadowCssClass="context-menu-shadow" MenuCallerCssClass="context-menu-caller">
											<div class="context-menu-header">
												<%= Resources.General.Literal_Manage %>
											</div>
											<div class="menu-row-separator">
											</div>
											<div runat="server" id="SplitMenuItem" class="context-menu-row" Visible="<%# !State.SplitPerformed && State.CurrentOrder.GetOrderItems().Count > 1 %>">
												<img src="<%=ResolveUrl("~/Media/Images/Order/split.png") %>" />
												<asp:LinkButton ID="MenuSplitButton" runat="server"
													CommandArgument='<%# Eval("Id") %>' CommandName="SplitOrderItem" Text="<%$ Resources:General, Button_Split%>" />
											</div>
											<div runat="server" id="DeleteMenuItem" class="context-menu-row">
												<img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
												<asp:LinkButton ID="MenuDeleteButton" runat="server" OnClientClick='return confirmDelete();'
													CommandArgument='<%# Eval("Id") %>' CommandName="DeleteOrderItem" Text="<%$ Resources:General, Button_Delete%>" />
											</div>
										</uc:ContextMenu>
									</div>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_IsDropShip %>" ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Center">
								<ItemTemplate>
									<asp:CheckBox ID="IsDropShipCheckBox" runat="server" Enabled="False" Checked='<%# Eval("IsDropShip")%>'/>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_OriginalPrice %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="13%">
								<ItemTemplate>
									<asp:Label ID="OriginalPriceLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Price %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%">
								<ItemTemplate>
									<asp:Label ID="PriceLabel" runat="server"></asp:Label>
									<asp:Label ID="DiscountLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Vat %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%">
								<ItemTemplate>
									<asp:Label ID="VatLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%">
								<HeaderTemplate>
									<div class="inline">
										<%= Resources.Customer.ColumnHeader_Quantity %>
										<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrderItems"
											CommandArgument='<%#Eval("ContentAreaId") %>' ImageUrl="~/Media/Images/Common/refresh.png"
											ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
									</div>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:TextBox runat="server" ID="QuantityTextBox" Text='<%# Eval("Quantity") %>' Width="30" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Sum %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%">
								<ItemTemplate>
									<asp:Label ID="SumLabel" runat="server"></asp:Label>
									<asp:Label ID="SumDiscountLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
								<ItemTemplate>
									<asp:ImageButton runat="server" OnClientClick='<%# "return DeleteConfirmation(\""+ Resources.Customer.Literal_OrderItemConfirmDelete+"\");"%>' ID="DeleteOrderItemButton" CommandName="DeleteOrderItem" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" />
								
									<asp:PlaceHolder runat="server" ID="OrderItemCampaignPlaceHolder">
										</td></tr>
										<tr>
											<td align="center">╚</td>
											<td><%= Resources.Customer.Literal_Campaigns%>:</td>
											<td colspan="8">
												<asp:Repeater runat="server" ID="OrderItemCampaignRepeater">
													<ItemTemplate>
														<asp:Label runat="server"><%# Eval("Title") %></asp:Label>
														<br>
													</ItemTemplate>
												</asp:Repeater>
											</td>
										</tr>
									</asp:PlaceHolder>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
					<div runat="server" id="ApplyToAllSelectedDiv" style="display: none;" class="apply-to-all-selected">
						<div style="float: left; padding-top: 6px;">
							<div class="apply-to-all">
								<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
							</div>
						</div>
						<div style="float: left">
							<uc:ImageLinkButton runat="server" ID="SplitSelectedButton" UseSubmitBehaviour="true"
								Text="<%$ Resources:General, Button_Split%>" SkinID="DefaultButton" CommandArgument='<%# Eval("Id") %>' />
							<uc:ImageLinkButton runat="server" ID="DeleteSelectedButton" UseSubmitBehaviour="true"
								Text="<%$ Resources:General, Button_Delete%>" SkinID="DefaultButton" CommandArgument='<%# Eval("Id") %>'
								OnClientClick='<%#"return DeleteConfirmation(\"" + Resources.Customer.Literal_SelectedOrderItemsConfirmDelete + "\");"%>' />
						</div>
					</div>
				</div>
				<div id="order-footer">
					<div style="float:left">
						<asp:HiddenField runat="server" ID="FakeAddProductButton" />
						<asp:LinkButton runat="server" id="AddProductButton" Text="<%$ Resources:Customer, Button_AddProduct %>" CssClass="blue" />
						<ajaxToolkit:ModalPopupExtender 
							ID="AddProductPopup" 
							runat="server" 
							TargetControlID="FakeAddProductButton"
							PopupControlID="ProductPopupDiv" 
							CancelControlID="ProductPopupCloseButton" 
							BackgroundCssClass="PopupBackground" />
						<div id="ProductPopupDiv" class="customer-comment-container" runat="server" style="z-index: 10010; display: none;">
							<div class="campaign-popup-header">
								<div class="campaign-popup-header-left" style="margin: auto;">
								</div>
								<div class="campaign-popup-header-center" style="margin: auto;">
									<span><%= Resources.General.Literal_Search%></span>
									<input type="button" id="ProductPopupCloseButton" runat="server" value="x"/>
								</div>
								<div class="campaign-popup-header-right" style="margin: auto;">
								</div>
							</div>
							<div class="campaign-popup-content-scrollable">
								<asp:UpdatePanel ID="ProductPopupUpdatePanel" UpdateMode="Conditional" runat="server">
									<ContentTemplate>
										<div class="campaign-product-search">
											<div class="content-box" id="order-item-search">
												<asp:Panel ID="SearchPanel" DefaultButton="ProductPopupSearchButton" runat="server">
												<uc:ProductSearchForm ID="ProductSearchFormControl" runat="server" />
												<br style="clear:both;" />
												<br />
												<div class="campaign-popup-buttons no-padding">
													<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductPopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
												</div>
												</asp:Panel>
												<br class="clear"/>
												<br />
												<uc:ProductSearchResult ID="ProductSearchResultControl" runat="server" />
											</div>
										</div>
									</ContentTemplate>
								</asp:UpdatePanel>
								<br />
								<div class="campaign-popup-buttons wide-padding">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductPopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductPopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
								</div>
							</div>
						</div>
					</div>
					<div id="order-items-summary" class="right" style="width: auto">
						<table>
							<tr id="OriginalSubTotalRow" runat="server">
								<td class="order-edit-summary-title">
									<span class="right"><%= Resources.Lekmer.Literal_SubTotalOriginal %></span>
								</td>
								<td class="order-edit-summary-vat">
								</td>
								<td class="order-edit-summary-value">
									<div id="OriginalSubTotalDiv" runat="server"></div>
								</td>
							</tr>
							<tr id="TotalDiscountRow" runat="server">
								<td class="order-edit-summary-title">
									<span class="right"><%= Resources.Lekmer.Literal_SubTotalDiscount %></span>
								</td>
								<td class="order-edit-summary-vat">
								</td>
								<td class="order-edit-summary-value">
									<div id="TotalDiscountDiv" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="order-edit-summary-title">
									<span class="right"><%= Resources.Customer.Literal_SubTotal %></span>
								</td>
								<td class="order-edit-summary-vat">
									<div id="SubTotalVatDiv" runat="server"></div>
								</td>
								<td class="order-edit-summary-value">
									<div id="SubTotalDiv" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="order-edit-summary-title">
									<span class="right">
										<asp:Label runat="server" ID="FreightLiteral"><%= Resources.Customer.Literal_Freight %></asp:Label>
										<asp:LinkButton runat="server" ID="FreightButton" CssClass="blue"><%= Resources.Customer.Literal_Freight %></asp:LinkButton>
									</span>
								</td>
								<td class="order-edit-summary-vat">
									<div id="FreightCostVatDiv" runat="server"></div>
								</td>
								<td class="order-edit-summary-value">
									<div id="FreightCostDiv" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="order-edit-summary-title">
									<span class="right">
										<asp:Label runat="server" ID="OptionalFreightLiteral"><%= Resources.Lekmer.Order_OptionalFreightCost %></asp:Label>
										<asp:LinkButton runat="server" ID="OptionalFreightButton" CssClass="blue"><%= Resources.Lekmer.Order_OptionalFreightCost %></asp:LinkButton>
									</span>
								</td>
								<td class="order-edit-summary-vat">
									<div id="OptionalFreightCostVatDiv" runat="server"></div>
								</td>
								<td class="order-edit-summary-value">
									<div id="OptionalFreightCostDiv" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="order-edit-summary-title">
									<span class="right">
										<asp:Label runat="server" ID="DiapersFreightLiteral"><%= Resources.Lekmer.Order_DiapersFreightCost %></asp:Label>
										<asp:LinkButton runat="server" ID="DiapersFreightButton" CssClass="blue"><%= Resources.Lekmer.Order_DiapersFreightCost %></asp:LinkButton>
									</span>
								</td>
								<td class="order-edit-summary-vat">
									<div id="DiapersFreightCostVatDiv" runat="server"></div>
								</td>
								<td class="order-edit-summary-value">
									<div id="DiapersFreightCostDiv" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="order-edit-summary-title">
									<span class="right">
										<asp:Label runat="server" ID="PaymentCostLabel"><%= Resources.Lekmer.Order_PaymentCost%></asp:Label>
										<asp:LinkButton runat="server" ID="PaymentCostPopupButton" CssClass="blue"><%= Resources.Lekmer.Order_PaymentCost%></asp:LinkButton>
									</span>
								</td>
								<td class="order-edit-summary-vat">
									<div id="PaymentCostVatDiv" runat="server"></div>
								</td>
								<td class="order-edit-summary-value">
									<div id="PaymentCostDiv" runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="order-edit-summary-title">
									<span class="right"><%= Resources.Customer.Literal_Total %></span>
								</td>
								<td class="order-edit-summary-vat">
									<div id="TotalVatDiv" runat="server"></div>
								</td>
								<td class="order-edit-summary-value">
									<div id="TotalDiv" runat="server"></div>
								</td>
							</tr>
						</table>
						<div class="summary-row clear left">
							<div id="FreightPopupDiv" class="customer-freight-mainDiv" style="z-index: 1001;display: none;" runat="server" >
								<div class="customer-freight-header">
									<div class="customer-freight-header-left">
									</div>
									<div class="customer-freight-header-center">
										<span><%= Resources.Customer.Literal_Freight%></span>
										<input type="button" id="FreightPopupCloseButton" runat="server" value="x"/>
									</div>
									<div class="customer-freight-header-right">
									</div>
								</div>
								<div class="customer-freight-subMainDiv">
								<asp:Panel runat="server" DefaultButton="FreightPopupOkButton">
									<uc:MessageContainer 
										ID="FreightPopupValidationMessage"
										MessageType="Failure"
										HideMessagesControlId="FreightPopupOkButton"
										runat="server" />
										<div class="column1">
									<div class="input-box">
									<asp:TextBox runat="server" id="FreightAmountTextBox"></asp:TextBox>&nbsp;<asp:Literal runat="server" ID="CurrencyLiteral"></asp:Literal>
								</div>
									<br clear="all" />
								</div>
									<br />
									<div class="customer-edit-action-buttons">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="FreightPopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton" ValidationGroup="Freight" />
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="FreightPopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton" CausesValidation="false" />
									</div>
									</asp:Panel>
								</div>
							</div>
							<div id="OptionalFreightPopupDiv" class="customer-freight-mainDiv" style="z-index: 1001;display: none;" runat="server" >
								<div class="customer-freight-header">
									<div class="customer-freight-header-left">
									</div>
									<div class="customer-freight-header-center">
										<span><%= Resources.Lekmer.Order_OptionalFreightCost%></span>
										<input type="button" id="OptionalFreightPopupCloseButton" runat="server" value="x"/>
									</div>
									<div class="customer-freight-header-right">
									</div>
								</div>
								<div class="customer-freight-subMainDiv">
								<asp:Panel runat="server" DefaultButton="OptionalFreightPopupOkButton">
									<uc:MessageContainer 
										ID="OptionalFreightPopupValidationMessage"
										MessageType="Failure"
										HideMessagesControlId="OptionalFreightPopupOkButton"
										runat="server" />
										<div class="column1">
									<div class="input-box">
									<asp:TextBox runat="server" id="OptionalFreightAmountTextBox"></asp:TextBox>&nbsp;<asp:Literal runat="server" ID="OptionalCurrencyLiteral"></asp:Literal>
								</div>
									<br clear="all" />
								</div>
									<br />
									<div class="customer-edit-action-buttons">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="OptionalFreightPopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton" ValidationGroup="OptionalFreight" />
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="OptionalFreightPopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton" CausesValidation="false" />
									</div>
									</asp:Panel>
								</div>
							</div>
							<div id="DiapersFreightPopupDiv" class="customer-freight-mainDiv" style="z-index: 1001;display: none;" runat="server" >
								<div class="customer-freight-header">
									<div class="customer-freight-header-left">
									</div>
									<div class="customer-freight-header-center">
										<span><%= Resources.Lekmer.Order_DiapersFreightCost%></span>
										<input type="button" id="DiapersFreightPopupCloseButton" runat="server" value="x"/>
									</div>
									<div class="customer-freight-header-right">
									</div>
								</div>
								<div class="customer-freight-subMainDiv">
								<asp:Panel runat="server" DefaultButton="DiapersFreightPopupOkButton">
									<uc:MessageContainer 
										ID="DiapersFreightPopupValidationMessage"
										MessageType="Failure"
										HideMessagesControlId="DiapersFreightPopupOkButton"
										runat="server" />
										<div class="column1">
									<div class="input-box">
									<asp:TextBox runat="server" id="DiapersFreightAmountTextBox"></asp:TextBox>&nbsp;<asp:Literal runat="server" ID="DiapersCurrencyLiteral"></asp:Literal>
								</div>
									<br clear="all" />
								</div>
									<br />
									<div class="customer-edit-action-buttons">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DiapersFreightPopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton" ValidationGroup="DiapersFreight" />
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DiapersFreightPopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton" CausesValidation="false" />
									</div>
									</asp:Panel>
								</div>
							</div>
							<ajaxToolkit:ModalPopupExtender 
								ID="FreightModalPopupExtender" 
								runat="server" 
								TargetControlID="FreightButton"
								PopupControlID="FreightPopupDiv" 
								CancelControlID="FreightPopupCloseButton" 
								BackgroundCssClass="PopupBackground" />
							<ajaxToolkit:ModalPopupExtender 
								ID="OptionalFreightModalPopupExtender" 
								runat="server" 
								TargetControlID="OptionalFreightButton"
								PopupControlID="OptionalFreightPopupDiv" 
								CancelControlID="OptionalFreightPopupCloseButton" 
								BackgroundCssClass="PopupBackground" />
							<ajaxToolkit:ModalPopupExtender 
								ID="DiapersFreightModalPopupExtender" 
								runat="server" 
								TargetControlID="DiapersFreightButton"
								PopupControlID="DiapersFreightPopupDiv" 
								CancelControlID="DiapersFreightPopupCloseButton" 
								BackgroundCssClass="PopupBackground" />
						</div>
						<div class="summary-row clear left">
							<div id="PaymentCostPopupDiv" class="customer-freight-mainDiv" style="z-index: 1001;display: none;" runat="server" >
								<div class="customer-freight-header">
									<div class="customer-freight-header-left">
									</div>
									<div class="customer-freight-header-center">
										<span><%= Resources.Lekmer.Order_PaymentCost%></span>
										<input type="button" id="PaymentCostPopupCloseButton" runat="server" value="x"/>
									</div>
									<div class="customer-freight-header-right">
									</div>
								</div>
								<div class="customer-freight-subMainDiv">
								<asp:Panel ID="PanelPaymentCost" runat="server" DefaultButton="PaymentCostPopupOkButton">
									<uc:MessageContainer 
										ID="PaymentCostPopupValidationMessage"
										MessageType="Failure"
										HideMessagesControlId="PaymentCostPopupOkButton"
										runat="server" />
										<div class="column1">
									<div class="input-box">
									<asp:TextBox runat="server" id="PaymentCostTextBox"></asp:TextBox>&nbsp;<asp:Literal runat="server" ID="PaymentCostCurrencyLiteral"></asp:Literal>
								</div>
									<br clear="all" />
								</div>
									<br />
									<div class="customer-edit-action-buttons">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="PaymentCostPopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton" ValidationGroup="Freight" />
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="PaymentCostPopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton" CausesValidation="false" />
									</div>
									</asp:Panel>
								</div>
							</div>
							<ajaxToolkit:ModalPopupExtender 
								ID="PaymentCostPopup" 
								runat="server" 
								TargetControlID="PaymentCostPopupButton"
								PopupControlID="PaymentCostPopupDiv" 
								CancelControlID="PaymentCostPopupCloseButton" 
								BackgroundCssClass="PopupBackground" />		
						</div>
					</div>
				</div>

				<br clear="all" />
				<hr />

				<label class="assortment-header"><%= Resources.Customer.Literal_Campaigns%></label>

				<asp:GridView ID="CampaignGrid" SkinID="grid" runat="server" Width="725" AutoGenerateColumns="false">
					<Columns>
						<asp:BoundField HeaderText="<%$ Resources:General, Literal_Title %>" DataField="Title" ItemStyle-Width="40%" />
					</Columns>
				</asp:GridView>

				<asp:Panel runat="server" ID="VoucherInfoPanel" >
					<br clear="all" />

					<label class="assortment-header"><%= Resources.Lekmer.Literal_VoucherInfo%></label>
				
					<asp:GridView ID="VoucherInfoGrid" SkinID="grid" runat="server" Width="725" AutoGenerateColumns="false">
						<Columns>
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Literal_VoucherCode %>" DataField="VoucherCode" />
							<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_DiscountType %>">
								<ItemTemplate>
									<asp:Label ID="DiscountTypeLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_DiscountValue %>">
								<ItemTemplate>
									<asp:Label ID="DiscountValueLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_AmountUsed %>">
								<ItemTemplate>
									<asp:Label ID="AmountUsedLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_VoucherStatus %>">
								<ItemTemplate>
									<asp:Label ID="VoucherStatusLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			
				<asp:Panel runat="server" ID="GiftCardViaMailInfoPanel" >
					<br clear="all" />

					<label class="assortment-header"><%= Resources.Lekmer.Literal_GiftCardViaMailInfo%></label>
				
					<asp:GridView ID="GiftCardViaMailInfoGrid" SkinID="grid" runat="server" Width="725" AutoGenerateColumns="false">
						<Columns>
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Literal_DiscountValue %>" DataField="DiscountValue" />
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Literal_DateToSend %>" DataField="DateToSend" />
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>">
								<ItemTemplate>
									<asp:Label ID="GiftCardViaMailInfoStatusLabel" runat="server"></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Literal_BatchId %>" DataField="VoucherInfoId" />
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Literal_VoucherCode %>" DataField="VoucherCode" />
						</Columns>
					</asp:GridView>
				</asp:Panel>

			</div>
			<div id="order-edit-container-right">
				<div><%= Resources.Customer.Literal_IPAddress %>: <asp:Literal runat="server" id="CustomerIPAddressLiteral"></asp:Literal></div>
				<div><%= Resources.Lekmer.Literal_UserAgent %>: <asp:Literal runat="server" id="UserAgentLiteral"></asp:Literal></div>

				<div class="control-container">
					<uc:OrderPaymentsControl ID="OrderPaymentsControl" runat="server" />
				</div>
				<div class="control-container">
					<uc:OrderAuditListControl ID="OrderAuditListControl1" runat="server" />
				</div>
				<div class="control-container">
					<div>
						<di><%= Resources.General.Literal_Status %></div>
						<asp:DropDownList runat="server" id="StatusDropDownList" DataTextField="Title" DataValueField="Id" AutoPostBack="true" Width="190" />
					</div>
				<div class="control-container">
					<div id="tracking-id">
						<div><%= Resources.Customer.Literal_Tracking%></div>
						<asp:TextBox runat="server" ID="TrackingIdTextBox" Width="125" CssClass="left" AutoPostBack="true" />
						<uc:ImageLinkButton runat="server" ID="TrackingFindButton" UseSubmitBehaviour="false" Text="<%$ Resources:General, Button_Find %>" SkinID="DefaultButton" />
						<br class="clear" />
					</div>
				</div>

				<div class="control-container">
					<div>
						<div><%= Resources.Customer.Literal_Delivery_Type%></div>
						<asp:DropDownList runat="server" id="DeliveryMethodList" DataTextField="Title" AutoPostBack="true" DataValueField="Id" Width="190" />
					</div>
				</div>

				<div class="control-container" id="IsOrderDropShip" runat="server">
					<div><%= Resources.LekmerMessage.OrderEdit_DropShipArticlesExists%></div>
					<uc:HyperLinkEncoded ID="DropShipInfoLink" runat="server" Text="Dropship info" />
				</div>

				<uc:OrderCommentControl ID="OrderCommentControl" runat="server" />
			</div>
		</div>

		<br clear="all"/>
		<div style="float:left;margin-top:7px;">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ResendOrderConfirmationButton" runat="server" Text="<%$ Resources:Customer, Button_ResendOrderConfirmation %>" SkinID="DefaultButton" />
		</div>
		<div style="float:right;margin-top:7px;">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgOrderEdit" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="GoBackButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" /> 
		</div>
	</asp:Panel>
</asp:Content>
