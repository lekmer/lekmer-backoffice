using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order;
using Litium.Lekmer.Voucher;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.BackOffice.UserControls.Customer.Order;
using Litium.Scensum.Web.Controls;
using Litium.Scensum.Web.Controls.Common;

using FoundationEncoder = Litium.Scensum.Foundation.Utilities.Encoder;

namespace Litium.Scensum.BackOffice.Modules.Customers.Orders
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
	public partial class OrderEdit : LekmerStatePageController<OrderState>, IEditor
	{
		private ILekmerFormatter _formatter;
		protected ILekmerFormatter Formatter
		{
			get
			{
				return _formatter ?? (_formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>());
			}
		}

		private IChannel _orderChannel;
		protected IChannel OrderChannel
		{
			get
			{
				return _orderChannel 
					?? (_orderChannel = IoC.Resolve<IChannelSecureService>().GetById(State.CurrentOrder.ChannelId));
			}
		}

		protected override void SetEventHandlers()
		{
			OrderCommentControl.OnError += OrderCommentControlOnError;
			SaveButton.Click += OnSave;
			GoBackButton.Click += OnCancel;
			CustomerLinkButton.Click += CustomerLinkButtonOnClick;
			OrderItemGrid.RowDataBound += OrderItemGridOnRowDataBound;
			OrderItemGrid.RowCommand += OrderItemGridOnRowCommand;
			SplitSelectedButton.Click += SplitSelectedClick;
			DeleteSelectedButton.Click += DeleteSelectedClick;
			OriginalOrderButton.Click += OriginalOrderButtonOnClick;
			SplitOrderButton.Click += SplitOrderButtonOnClick;
			ProductPopupSearchButton.Click += OnProductSearch;
			ProductPopupOkButton.Click += OnProductsSelected;
			FreightPopupOkButton.Click += OnFreightOkClick;
			OptionalFreightPopupOkButton.Click += OnOptionalFreightOkClick;
			DiapersFreightPopupOkButton.Click += OnDiapersFreightOkClick;
			DeliveryAddressEdit.EditCompleted += OnDeliveryAddressEditCompleted;
			BillingAddressEdit.EditCompleted += OnBillingAddressEditCompleted;
			AlternateAddressEdit.EditCompleted += OnAlternateAddressEditCompleted;
			StatusDropDownList.SelectedIndexChanged += StatusDropDownListOnSelectedIndexChanged;
			DeliveryMethodList.SelectedIndexChanged += DeliveryMethodList_SelectedIndexChanged;
			AddProductButton.Click += OnAddProductClick;
			OrderPaymentsControl.PaymentsChanged += OnPaymentsChanged;
			ResendOrderConfirmationButton.Click += OnResendOrderConfirmation;
			OrderPaymentsControl.CannotDo += OrderPaymentsControl_CannotDo;
			TrackingIdTextBox.TextChanged += TrackingIdTextBoxOnTextChanged;
			PaymentCostPopupOkButton.Click += OnPaymentCostPopupOkClick;
			VoucherInfoGrid.RowDataBound += VoucherInfoGridOnRowDataBound;
			GiftCardViaMailInfoGrid.RowDataBound += GiftCardViaMailInfoGridOnRowDataBound;
		}

		protected void OrderPaymentsControl_CannotDo(object sender, CannotDoEventArgs e)
		{
			switch (e.Cannot)
			{
				case Cannot.Delete:
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotDeletePayment, InfoType.Warning);
					break;
				case Cannot.Add:
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotCreatePayment, InfoType.Warning);
					break;
			}
		}

		protected void OnPaymentsChanged(object sender, EventArgs e)
		{
			if (State.CurrentOriginalNotSplit)
			{
				State.OrderPayments = OrderPaymentsControl.OrderPayments;
			}
			else
			{
				State.SplitOrderPayments = OrderPaymentsControl.OrderPayments;
			}
		}

		protected void OnDeliveryAddressEditCompleted(object sender, EventArgs e)
		{
			if (State.CurrentOriginalNotSplit)
			{
				State.DeliveryAddress = DeliveryAddressEdit.OrderAddress;
			}
			else
			{
				State.SplitOrderDeliveryAddress = DeliveryAddressEdit.OrderAddress;
			}
		}

		protected void OnBillingAddressEditCompleted(object sender, EventArgs e)
		{
			if (State.CurrentOriginalNotSplit)
			{
				State.BillingAddress = BillingAddressEdit.OrderAddress;
			}
			else
			{
				State.SplitOrderBillingAddress = BillingAddressEdit.OrderAddress;
			}
		}

		protected void OnAlternateAddressEditCompleted(object sender, EventArgs e)
		{
			if (State.CurrentOriginalNotSplit)
			{
				State.AlternateAddress = AlternateAddressEdit.OrderAddress;
			}
			else
			{
				State.SplitOrderAlternateAddress = AlternateAddressEdit.OrderAddress;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			EnableFeatures();

			const string productResizePopupScript = "$(function() { ResizePopup('ProductPopupDiv', 0.8, 0.7, 0.08); });";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "productResizePopupScript", productResizePopupScript, true);
		}

		public void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Order.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Order.GetDefaultUrl());
			}
		}

		public void OnSave(object sender, EventArgs e)
		{
			IOrderFull order = IoC.Resolve<IOrderSecureService>().GetFullById(GetId());
			if (order == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}

			if (!Refresh())
			{
				return;
			}
			CollectOrderData();
			Audit(order);
			Save();
			PopulateForm();
			ReBindAudit();
			ReBindPayments();
		}

		private void ReBindPayments()
		{
			PopulatePayments();
			OrderPaymentsControl.ReBind();
		}

		private void CollectOrderData()
		{
			State.CurrentOrder.DeliveryTrackingId = TrackingIdTextBox.Text;

			if (State.CurrentOriginalNotSplit)
			{
				State.CurrentOrderComments = State.OrderComments = OrderCommentControl.OrderComments;
			}
			else
			{
				State.CurrentOrderComments = State.SplitOrderComments = OrderCommentControl.OrderComments;
			}
		}

		private void Audit(IOrderFull orderFull)
		{
			AuditChangeStatus(orderFull);
			AuditSplit();
			AuditChangeDeliveryAddress(orderFull);
			AuditChangeBillingAddress(orderFull);
			AuditChangeAlternateAddress(orderFull);
			AuditOrderItems(orderFull);
			AuditChangeFreight(orderFull);
			AuditChangeOptionalFreight(orderFull);
			AuditChangeDiapersFreight(orderFull);
			AuditChangePaymentCost(orderFull);
			AuditPayments(orderFull);
			AuditChangeDeliveryMethod(orderFull);
			AuditChangeTrackingId(orderFull);
		}
		private void AuditChangeStatus(IOrder order)
		{
			AuditChangeStatus(order.OrderStatus, State.Order.OrderStatus, State.Order.Id);
			if (State.SplitPerformed)
			{
				AuditChangeStatus(order.OrderStatus, State.SplitOrder.OrderStatus, State.SplitOrder.Id);
			}
		}
		private void AuditChangeStatus(IOrderStatus originalOrderStatus, IOrderStatus stateOrderStatus, int orderId)
		{
			if (originalOrderStatus.Id != stateOrderStatus.Id)
			{
				AddAudit(
					"StatusChanged",
					string.Format(CultureInfo.CurrentCulture, "Status changed from '{0}' to '{1}'.", originalOrderStatus.Title, stateOrderStatus.Title),
					stateOrderStatus.CommonName,
					orderId
				);
			}
		}
		private void AuditSplit()
		{
			if (State.SplitPerformed)
			{
				string idList = string.Join(", ", State.SplitOrder.GetOrderItems().Select(item => item.ErpId).ToArray());
				AddAudit(
					"SplitOrder",
					string.Format(CultureInfo.CurrentCulture, "Split order {0} into {1}. Articles moved are {2}.", State.Order.Number, State.SplitOrder.Number, idList),
					null,
					State.Order.Id
				);
			}
		}
		private void AuditChangeDeliveryAddress(IOrderFull orderFull)
		{
			AuditAddress(orderFull.DeliveryAddress, State.DeliveryAddress, State.Order.Id, "Delivery address changed");
			if (State.SplitPerformed)
			{
				AuditAddress(orderFull.DeliveryAddress, State.SplitOrderDeliveryAddress, State.SplitOrder.Id, "Delivery address changed");
			}
		}
		private void AuditChangeBillingAddress(IOrderFull orderFull)
		{
			AuditAddress(orderFull.BillingAddress, State.BillingAddress, State.Order.Id, "Billing address changed");
			if (State.SplitPerformed)
			{
				AuditAddress(orderFull.BillingAddress, State.SplitOrderBillingAddress, State.SplitOrder.Id, "Billing address changed");
			}
		}
		private void AuditChangeAlternateAddress(IOrderFull orderFull)
		{
			var alternateAddress = ((ILekmerOrderFull) orderFull).AlternateAddress;
			if (alternateAddress == null) return;

			AuditAddress(alternateAddress, State.AlternateAddress, State.Order.Id, "Alternate address changed");
			if (State.SplitPerformed)
			{
				AuditAddress(alternateAddress, State.SplitOrderAlternateAddress, State.SplitOrder.Id, "Alternate address changed");
			}
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.String.Format(System.String,System.Object[])")]
		private void AuditAddress(IOrderAddress originalOrderAddress, IOrderAddress stateOrderAddress, int orderId, string information)
		{
			if (originalOrderAddress.Addressee != stateOrderAddress.Addressee
				|| originalOrderAddress.City != stateOrderAddress.City
				|| originalOrderAddress.CountryId != stateOrderAddress.CountryId
				|| originalOrderAddress.PhoneNumber != stateOrderAddress.PhoneNumber
				|| originalOrderAddress.PostalCode != stateOrderAddress.PostalCode
				|| originalOrderAddress.StreetAddress != stateOrderAddress.StreetAddress
				|| originalOrderAddress.StreetAddress2 != stateOrderAddress.StreetAddress2)
			{
				AddAudit(
					"AddressChanged",
					string.Format(CultureInfo.CurrentCulture,
						"Changed address from {0}, {1}, {2}, {3}, {4}, {5}, {6} to {7}, {8}, {9}, {10}, {11}, {12}, {13}",
						IoC.Resolve<ICountrySecureService>().GetById(originalOrderAddress.CountryId).Title,
						originalOrderAddress.City,
						originalOrderAddress.StreetAddress,
						originalOrderAddress.StreetAddress2,
						originalOrderAddress.PostalCode,
						originalOrderAddress.PhoneNumber,
						originalOrderAddress.Addressee,
						IoC.Resolve<ICountrySecureService>().GetById(stateOrderAddress.CountryId).Title,
						stateOrderAddress.City,
						stateOrderAddress.StreetAddress,
						stateOrderAddress.StreetAddress2,
						stateOrderAddress.PostalCode,
						stateOrderAddress.PhoneNumber,
						stateOrderAddress.Addressee),
					information,
					orderId
				);
			}
		}
		private void AuditOrderItems(IOrderFull orderFull)
		{
			var originalOrderItems = orderFull.GetOrderItems();

			var stateOrderItems = State.Order.GetOrderItems();
			var stateSplitOrderItems = new Collection<IOrderItem>();
			if (State.SplitPerformed)
			{
				stateSplitOrderItems = State.SplitOrder.GetOrderItems();
			}

			var addedOrderItems = stateOrderItems.Union(stateSplitOrderItems).Where(item => item.Id < 0);
			AuditAddedOrderItems(addedOrderItems, State.Order.Id);

			var stateAllNotNewOrderItemIds = stateOrderItems
				.Union(stateSplitOrderItems)
				.Where(item => item.Id > 0)
				.Select(item => item.Id);
			AuditRemovedOrderItems(originalOrderItems, stateAllNotNewOrderItemIds, State.Order.Id);

			var stateNotNewOrderItems = stateOrderItems
				.Where(item => item.Id > 0);
			AuditChangedOrderItems(originalOrderItems, stateNotNewOrderItems, State.Order.Id);

			if (State.SplitPerformed)
			{
				var stateNotNewSplitOrderItems = stateSplitOrderItems
					.Where(item => item.Id > 0);
				AuditChangedOrderItems(originalOrderItems, stateNotNewSplitOrderItems, State.SplitOrder.Id);
			}
		}
		private void AuditAddedOrderItems(IEnumerable<IOrderItem> addedOrderItems, int orderId)
		{
			if (addedOrderItems.Count() == 0)
			{
				return;
			}
			var addedOrderItemIds = addedOrderItems.Select(item => item.ErpId + " (" + item.Quantity + " pcs)");
			AddAudit(
				"ProductAdded",
				string.Format(CultureInfo.CurrentCulture, "Added product(s) {0}.", string.Join(", ", addedOrderItemIds.ToArray())),
				null,
				orderId
			);
		}
		private void AuditRemovedOrderItems(IEnumerable<IOrderItem> originalOrderItems, IEnumerable<int> stateOrderItems, int orderId)
		{
			var removedOrderItemIds = new Collection<string>();
			foreach (IOrderItem orderItem in originalOrderItems)
			{
				if (!stateOrderItems.Contains(orderItem.Id))
				{
					removedOrderItemIds.Add(orderItem.ErpId + " (" + orderItem.Quantity + " pcs)");
				}
			}
			if (removedOrderItemIds.Count > 0)
			{
				AddAudit(
					"ProductRemoved",
					string.Format(CultureInfo.CurrentCulture, "Removed product(s) {0}.", string.Join(", ", removedOrderItemIds.ToArray())),
					null,
					orderId
				);
			}
		}
		private void AuditChangedOrderItems(IEnumerable<IOrderItem> originalOrderItems, IEnumerable<IOrderItem> stateOrderItems, int orderId)
		{
			foreach (IOrderItem orderItem in stateOrderItems)
			{
				IOrderItem orderItemLocal = orderItem;
				var originalOrderItem = originalOrderItems.SingleOrDefault(item => item.Id == orderItemLocal.Id);
				if (originalOrderItem != default(IOrderItem))
				{
					if (originalOrderItem.Quantity != orderItem.Quantity)
					{
						AddAudit(
							"ProductQuantityChanged",
							string.Format(CultureInfo.CurrentCulture, "Changed product '{0}' quantity from {1} to {2}.", orderItem.ErpId, originalOrderItem.Quantity, orderItem.Quantity),
							null,
							orderId
						);
					}
				}
			}
		}
		private void AuditChangeFreight(IOrder order)
		{
			if (order.FreightCost != State.Order.FreightCost)
			{
				AddAudit(
					"FreightChanged",
					string.Format(CultureInfo.CurrentCulture, "Changed freight from {0} to {1}.", order.FreightCost, State.Order.FreightCost),
					null,
					State.Order.Id
				);
			}
		}
		private void AuditChangeOptionalFreight(IOrder order)
		{
			var lekmerOrder = (ILekmerOrder)order;

			if (lekmerOrder.OptionalFreightCost != State.Order.OptionalFreightCost)
			{
				AddAudit(
					"FreightChanged",
					string.Format(CultureInfo.CurrentCulture, "Changed optional freight from {0} to {1}.", lekmerOrder.OptionalFreightCost, State.Order.OptionalFreightCost),
					null,
					State.Order.Id
				);
			}
		}
		private void AuditChangeDiapersFreight(IOrder order)
		{
			var lekmerOrder = (ILekmerOrder)order;

			if (lekmerOrder.DiapersFreightCost != State.Order.DiapersFreightCost)
			{
				AddAudit(
					"FreightChanged",
					string.Format(CultureInfo.CurrentCulture, "Changed diapers freight from {0} to {1}.", lekmerOrder.DiapersFreightCost, State.Order.DiapersFreightCost),
					null,
					State.Order.Id
				);
			}
		}
		private void AuditChangePaymentCost(IOrder order)
		{
			var originalPaymentCost = ((ILekmerOrderFull) order).PaymentCost;
			var newPaymentCost = State.Order.PaymentCost;
			if (originalPaymentCost != newPaymentCost)
			{
				AddAudit(
					"PaymentCostChanged",
					string.Format(CultureInfo.CurrentCulture, "Changed payment cost from {0} to {1}.", originalPaymentCost, newPaymentCost),
					null,
					State.Order.Id
				);
			}
		}
		private void AuditPayments(IOrderFull orderFull)
		{
			var orderPayments = orderFull.Payments;
			var orderPaymentIds = orderPayments.Select(p => p.Id);

			var stateOrderPayments = State.OrderPayments.Where(p => !p.IsDeleted);
			var stateAllNotNewOrderPayments = stateOrderPayments;
			AuditAddedPayments(orderPaymentIds, stateOrderPayments, State.Order.Id);
			AuditChangedPayments(orderPayments, stateOrderPayments, State.Order.Id);

			if (State.SplitPerformed)
			{
				var stateSplitOrderPayments = State.SplitOrderPayments.Where(p => !p.IsDeleted);
				stateAllNotNewOrderPayments = stateAllNotNewOrderPayments.Union(stateSplitOrderPayments);
				AuditAddedPayments(orderPaymentIds, stateSplitOrderPayments, State.SplitOrder.Id);
				AuditChangedPayments(orderPayments, stateSplitOrderPayments, State.SplitOrder.Id);
			}

			var stateAllNotNewOrderPaymentIds = stateAllNotNewOrderPayments.Where(p => p.Id > 0).Select(p => p.Id);
			AuditRemovedPayments(orderPayments, stateAllNotNewOrderPaymentIds, State.Order.Id);
		}
		private void AuditAddedPayments(IEnumerable<int> originalOrderPaymentIds, IEnumerable<IOrderPayment> stateOrderPayments, int orderId)
		{
			foreach (IOrderPayment orderPayment in stateOrderPayments)
			{
				if (!originalOrderPaymentIds.Contains(orderPayment.Id))
				{
					AddAudit(
						"PaymentAdded",
						string.Format(CultureInfo.CurrentCulture,
							"Added payment of type '{0}' for value {1}, VAT {2}, reference Id '{3}'.",
							IoC.Resolve<IPaymentTypeSecureService>().GetById(orderPayment.PaymentTypeId).Title,
							orderPayment.Price,
							orderPayment.Vat,
							orderPayment.ReferenceId),
						null,
						orderId
					);
				}
			}
		}
		private void AuditRemovedPayments(IEnumerable<IOrderPayment> originalOrderPaymentIds, IEnumerable<int> stateOrderPayments, int orderId)
		{
			foreach (IOrderPayment originalOrderPayment in originalOrderPaymentIds)
			{
				if (!stateOrderPayments.Contains(originalOrderPayment.Id))
				{
					AddAudit(
						"PaymentRemoved",
						string.Format(CultureInfo.CurrentCulture,
							"Removed payment of type '{0}' for value {1}, VAT {2}, reference Id '{3}'.",
							IoC.Resolve<IPaymentTypeSecureService>().GetById(originalOrderPayment.PaymentTypeId).Title,
							originalOrderPayment.Price,
							originalOrderPayment.Vat,
							originalOrderPayment.ReferenceId),
						null,
						orderId
					);
				}
			}
		}
		private void AuditChangedPayments(IEnumerable<IOrderPayment> originalOrderPayments, IEnumerable<IOrderPayment> stateOrderPayments, int orderId)
		{
			foreach (IOrderPayment orderPayment in stateOrderPayments)
			{
				IOrderPayment orderPaymentLocal = orderPayment;
				var originalOrderPayment = originalOrderPayments.SingleOrDefault(p => p.Id == orderPaymentLocal.Id);
				if (originalOrderPayment != default(IOrderItem))
				{
					if (originalOrderPayment.PaymentTypeId != orderPayment.PaymentTypeId
						|| originalOrderPayment.Price != orderPayment.Price
						|| originalOrderPayment.Vat != orderPayment.Vat
						|| originalOrderPayment.ReferenceId != orderPayment.ReferenceId)
					{
						AddAudit(
							"PaymentChanged",
							string.Format(CultureInfo.CurrentCulture, "Changed payment from type '{0}', value {1}, VAT {2}, reference Id '{3}' to type '{4}', value {5}, VAT {6}, reference Id '{7}'.",
								IoC.Resolve<IPaymentTypeSecureService>().GetById(originalOrderPayment.PaymentTypeId).Title,
								originalOrderPayment.Price,
								originalOrderPayment.Vat,
								originalOrderPayment.ReferenceId,
								IoC.Resolve<IPaymentTypeSecureService>().GetById(orderPayment.PaymentTypeId).Title,
								orderPayment.Price,
								orderPayment.Vat,
								orderPayment.ReferenceId),
							null,
							orderId
						);
					}
				}
			}
		}
		private void AuditChangeDeliveryMethod(IOrder order)
		{
			AuditChangeDeliveryMethod(order.DeliveryMethodId, State.Order.DeliveryMethodId, State.Order.Id);
			if (State.SplitPerformed)
			{
				AuditChangeDeliveryMethod(order.DeliveryMethodId, State.SplitOrder.DeliveryMethodId, State.SplitOrder.Id);
			}
		}
		private void AuditChangeDeliveryMethod(int originalDeliveryMethodId, int stateDeliveryMethodId, int orderId)
		{
			if (originalDeliveryMethodId != stateDeliveryMethodId)
			{
				var deliveryMethodSecureService = IoC.Resolve<IDeliveryMethodSecureService>();
				var originalDeliveryMethod = deliveryMethodSecureService.GetById(State.Order.ChannelId, originalDeliveryMethodId);
				var stateDeliveryMethod = deliveryMethodSecureService.GetById(State.Order.ChannelId, stateDeliveryMethodId);
				AddAudit(
					"DeliveryMethodChanged",
					string.Format(CultureInfo.CurrentCulture, "Changed delivery method from {0} to {1}.", originalDeliveryMethod.Title, stateDeliveryMethod.Title),
					stateDeliveryMethod.CommonName,
					orderId
				);
			}
		}
		private void AuditChangeTrackingId(IOrder order)
		{
			AuditChangeTrackingId(order.DeliveryTrackingId, State.Order.DeliveryTrackingId, State.Order.Id);
			if (State.SplitPerformed)
			{
				AuditChangeTrackingId(order.DeliveryTrackingId, State.SplitOrder.DeliveryTrackingId, State.SplitOrder.Id);
			}
		}
		private void AuditChangeTrackingId(string originalDeliveryTrackingId, string stateDeliveryTrackingId, int orderId)
		{
			if (originalDeliveryTrackingId.NullWhenEmpty() != stateDeliveryTrackingId.NullWhenTrimmedEmpty())
			{
				AddAudit(
					"DeliveryTrackingIdChanged",
					string.Format(CultureInfo.CurrentCulture, "Changed delivery tracking value from '{0}' to '{1}'.", originalDeliveryTrackingId, stateDeliveryTrackingId),
					null,
					orderId
				);
			}
		}
		private void AddAudit(string auditTypeCommonName, string message, string information, int orderId)
		{
			IOrderAudit audit = CreateAudit(auditTypeCommonName, message, information, orderId);
			if (State.Order.Id == orderId)
			{
				State.OrderAudit.Add(audit);
			}
			else if (State.SplitPerformed && State.SplitOrder.Id == orderId)
			{
				State.SplitOrderAudit.Add(audit);
			}
			else
			{
				throw new InvalidOperationException("Order with Id " + orderId + " not found.");
			}
		}
		private static IOrderAudit CreateAudit(string auditTypeCommonName, string message, string information, int orderId)
		{
			var audit = IoC.Resolve<IOrderAuditSecureService>().Create();
			audit.AuditType = IoC.Resolve<IOrderAuditTypeSecureService>().GetByCommonName(auditTypeCommonName);
			audit.OrderId = orderId;
			audit.SystemUserId = SignInHelper.SignedInSystemUser.Id;
			audit.Information = information;
			audit.Message = message;
			return audit;
		}

		private void Save()
		{
			var orderSecureService = IoC.Resolve<ILekmerOrderSecureService>();
			if (State.SplitPerformed)
			{
				orderSecureService.SaveSplit(SignInHelper.SignedInSystemUser, State.Order, State.OrderComments, State.BillingAddress, State.DeliveryAddress, State.AlternateAddress, State.OrderAudit, State.OrderPayments, State.SplitOrder, State.SplitOrderComments, State.SplitOrderBillingAddress, State.SplitOrderDeliveryAddress, State.SplitOrderAlternateAddress, State.SplitOrderAudit, State.SplitOrderPayments);
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessOrders, InfoType.Success);
			}
			else
			{
				orderSecureService.Save(SignInHelper.SignedInSystemUser, State.Order, State.OrderComments, State.BillingAddress, State.DeliveryAddress, State.AlternateAddress, State.OrderAudit, State.OrderPayments);
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessOrder, InfoType.Success);
			}
		}

		private void OrderItemGridOnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = row.FindControl("SelectAllCheckBox") as CheckBox;
				if (selectAllCheckBox != null)
				{
					var applyToAllSelectedDiv = (HtmlGenericControl)(OrderItemGrid.Parent).FindControl("ApplyToAllSelectedDiv");
					selectAllCheckBox.Attributes.Add("onclick",
						"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + OrderItemGrid.ClientID + @"');
						ShowBlockOption(this,'" + applyToAllSelectedDiv.ClientID + @"');"
					);
					selectAllCheckBox.Checked = false;
				}
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var orderItem = row.DataItem as IOrderItem;
				var lekmerOorderItem = row.DataItem as ILekmerOrderItem;
				if (orderItem == null || lekmerOorderItem == null)
				{
					return;
				}

				string erpId = orderItem.ErpId;
				if (lekmerOorderItem.HasSize)
				{
					erpId = lekmerOorderItem.OrderItemSize.ErpId;
				}
				DataBoundLabel(row, "ErpIdLabel", erpId);

				DataBoundLabel(row, "OriginalPriceLabel", orderItem.OriginalPrice.IncludingVat);
				DataBoundLabel(row, "PriceLabel", orderItem.ActualPrice.IncludingVat);

				decimal discount = lekmerOorderItem.RowDiscount(true);
				if (discount > 0m)
				{
					DataBoundLabel(row, "DiscountLabel", discount, "(-{0})");
				}

				DataBoundLabel(row, "VatLabel", orderItem.GetActualVatAmount());
				DataBoundLabel(row, "SumLabel", orderItem.ActualPrice.IncludingVat * orderItem.Quantity);
				if (discount > 0m && orderItem.Quantity > 1)
				{
					DataBoundLabel(row, "SumDiscountLabel", discount * orderItem.Quantity, "(-{0})");
				}

				var selectAllCheckBox = OrderItemGrid.HeaderRow.FindControl("SelectAllCheckBox") as CheckBox;
				if (selectAllCheckBox != null)
				{
					var applyToAllSelectedDiv = (HtmlGenericControl)(OrderItemGrid.Parent).FindControl("ApplyToAllSelectedDiv");
					var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
					selectCheckBox.Attributes.Add("onclick",
						"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');
						ShowBlockOption(this,'" + applyToAllSelectedDiv.ClientID + @"');"
					);
				}

				PopulateOrderItemCampaigns(row, orderItem);
			}
			EnableFeatures(row);
		}

		private void OrderItemGridOnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteOrderItem")
			{
				int id = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				Refresh();
				DeleteItem(id);
			}
			else if (e.CommandName == "SplitOrderItem")
			{
				int id = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				Refresh();
				Split(id);
			}
			else if (e.CommandName == "RefreshOrderItems")
			{
				Refresh();
			}

			PopulateOrderItems();
			PopulateSummary();
		}

		private void CustomerLinkButtonOnClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Customer.GetEditUrlForOrderEdit(State.CurrentOrder.CustomerId, GetId()));
		}

		private void OrderCommentControlOnError(object sender, EventArgs e)
		{
			SystemMessageContainer.Add(Resources.OrderMessage.CannotAddComment, InfoType.Warning);
		}

		private void DeleteSelectedClick(object sender, EventArgs e)
		{
			Refresh();

			IEnumerable<int> ids = OrderItemGrid.GetSelectedIds();
			DeleteItems(ids);

			PopulateOrderItems();
			PopulateSummary();
		}

		private void SplitSelectedClick(object sender, EventArgs e)
		{
			Refresh();

			IEnumerable<int> ids = OrderItemGrid.GetSelectedIds();
			Split(ids);
		}

		private void OriginalOrderButtonOnClick(object sender, EventArgs e)
		{
			Refresh();

			SwitchToOriginalOrder();

			PopulateOrder();
			OrderCommentControl.REBind();
			OrderAuditListControl1.REBind();
			DeliveryAddressEdit.REBind();
			BillingAddressEdit.REBind();
			AlternateAddressEdit.REBind();
			OrderPaymentsControl.ReBind();
		}

		private void SplitOrderButtonOnClick(object sender, EventArgs e)
		{
			Refresh();

			SwitchToSplitOrder();

			PopulateOrder();
			OrderCommentControl.REBind();
			OrderAuditListControl1.REBind();
			DeliveryAddressEdit.REBind();
			BillingAddressEdit.REBind();
			AlternateAddressEdit.REBind();
			OrderPaymentsControl.ReBind();
		}

		protected virtual void OnProductSearch(object sender, EventArgs e)
		{
			if (ProductSearchFormControl.Validate())
			{
				var criteria = ProductSearchFormControl.CreateSearchCriteria();
				ProductSearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void OnProductsSelected(object sender, EventArgs e)
		{
			var id = 0;
			var orderItems = State.CurrentOrder.GetOrderItems();
			if (orderItems != null && orderItems.Count > 0)
			{
				var minId = orderItems.Min(orderItem => orderItem.Id);
				id = Math.Min(minId, id);
			}
			var deletedOrderItems = State.CurrentOrder.GetDeletedOrderItems();
			if (deletedOrderItems != null && deletedOrderItems.Count > 0)
			{
				var minDeletedId = deletedOrderItems.Min(orderItem => orderItem.Id);
				id = Math.Min(minDeletedId, id);
			}

			foreach (var product in ProductSearchResultControl.GetSelectedProducts())
			{
				id--;
				var orderItem = IoC.Resolve<IOrderItem>();
				orderItem.Id = id;
				orderItem.OrderId = State.CurrentOrder.Id;
				orderItem.MergeWith(product);
				orderItem.Quantity = 1;
				orderItem.OrderItemStatus = IoC.Resolve<IOrderItemStatusSecureService>().GetByCommonName(OrderStatusName.PaymentConfirmed);
				State.CurrentOrder.AddOrderItem(orderItem);
			}
			PopulateOrderItems();
			PopulateSummary();
		}

		protected virtual void OnFreightOkClick(object sender, EventArgs e)
		{
			Refresh();

			var freightValueText = FreightAmountTextBox.Text;

			if (freightValueText.Trim().Length == 0)
			{
				FreightPopupValidationMessage.Add(Resources.OrderMessage.FreightEmpty);
				FreightModalPopupExtender.Show();
				return;
			}


			decimal freightValue;
			if (!freightValueText.TryParseLocalDecimalAsNumber(out freightValue))
			{
				FreightPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyFormat, OrderChannel.Currency.Iso));
				FreightModalPopupExtender.Show();
				return;
			}

			if (!IoC.Resolve<IOrderSecureService>().CanChangeFreightCost(State.CurrentOrder, State.CurrentOrder.FreightCost))
			{
				FreightPopupValidationMessage.Add(Resources.CustomerMessage.CannotChangeOrderFreight);
				FreightModalPopupExtender.Show();
				return;
			}
			if (freightValue < 0)
			{
				FreightPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueNegative, OrderChannel.Currency.Iso));
				FreightModalPopupExtender.Show();
				return;
			}

			State.CurrentOrder.FreightCost = freightValue;
			PopulateSummary();
		}

		protected virtual void OnOptionalFreightOkClick(object sender, EventArgs e)
		{
			Refresh();

			var lekmerOrderFull = (ILekmerOrderFull)State.CurrentOrder;

			var optionalFreightValueText = OptionalFreightAmountTextBox.Text;

			if (optionalFreightValueText.Trim().Length == 0)
			{
				lekmerOrderFull.OptionalFreightCost = null;
				PopulateSummary();
				return;
			}


			decimal optionalFreightValue;
			if (!optionalFreightValueText.TryParseLocalDecimalAsNumber(out optionalFreightValue))
			{
				OptionalFreightPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyFormat, OrderChannel.Currency.Iso));
				OptionalFreightModalPopupExtender.Show();
				return;
			}

			if (!IoC.Resolve<IOrderSecureService>().CanChangeFreightCost(State.CurrentOrder, State.CurrentOrder.FreightCost))
			{
				OptionalFreightPopupValidationMessage.Add(Resources.CustomerMessage.CannotChangeOrderFreight);
				OptionalFreightModalPopupExtender.Show();
				return;
			}
			if (optionalFreightValue < 0)
			{
				OptionalFreightPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueNegative, OrderChannel.Currency.Iso));
				OptionalFreightModalPopupExtender.Show();
				return;
			}

			lekmerOrderFull.OptionalFreightCost = optionalFreightValue;
			PopulateSummary();
		}

		protected virtual void OnDiapersFreightOkClick(object sender, EventArgs e)
		{
			Refresh();

			var lekmerOrderFull = (ILekmerOrderFull)State.CurrentOrder;

			var diapersFreightValueText = DiapersFreightAmountTextBox.Text;

			if (diapersFreightValueText.Trim().Length == 0)
			{
				lekmerOrderFull.DiapersFreightCost = null;
				PopulateSummary();
				return;
			}


			decimal diapersFreightValue;
			if (!diapersFreightValueText.TryParseLocalDecimalAsNumber(out diapersFreightValue))
			{
				DiapersFreightPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyFormat, OrderChannel.Currency.Iso));
				DiapersFreightModalPopupExtender.Show();
				return;
			}

			if (!IoC.Resolve<IOrderSecureService>().CanChangeFreightCost(State.CurrentOrder, State.CurrentOrder.FreightCost))
			{
				DiapersFreightPopupValidationMessage.Add(Resources.CustomerMessage.CannotChangeOrderFreight);
				DiapersFreightModalPopupExtender.Show();
				return;
			}
			if (diapersFreightValue < 0)
			{
				DiapersFreightPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueNegative, OrderChannel.Currency.Iso));
				DiapersFreightModalPopupExtender.Show();
				return;
			}

			lekmerOrderFull.DiapersFreightCost = diapersFreightValue;
			PopulateSummary();
		}

		protected virtual void OnPaymentCostPopupOkClick(object sender, EventArgs e)
		{
			Refresh();

			var paymentCostValueText = PaymentCostTextBox.Text;

			if (paymentCostValueText.Trim().Length == 0)
			{
				PaymentCostPopupValidationMessage.Add(Resources.LekmerMessage.OrderEdit_PaymentCostEmpty);
				PaymentCostPopup.Show();
				return;
			}

			decimal paymentCostValue;
			if (!paymentCostValueText.TryParseLocalDecimalAsNumber(out paymentCostValue))
			{
				var channelSecureService = IoC.Resolve<IChannelSecureService>();
				var channel = channelSecureService.GetById(State.CurrentOrder.ChannelId);
				PaymentCostPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyFormat, channel.Currency.Iso));
				PaymentCostPopup.Show();
				return;
			}

			if (paymentCostValue < 0)
			{
				var channelSecureService = IoC.Resolve<IChannelSecureService>();
				var channel = channelSecureService.GetById(State.CurrentOrder.ChannelId);
				PaymentCostPopupValidationMessage.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueNegative, channel.Currency.Iso));
				PaymentCostPopup.Show();
				return;
			}

			if (!IoC.Resolve<ILekmerOrderSecureService>().CanChangePaymentCost(State.CurrentOrder, ((ILekmerOrderFull)State.CurrentOrder).PaymentCost))
			{
				PaymentCostPopupValidationMessage.Add(Resources.LekmerMessage.OrderEdit_CannotChangePaymentCost);
				PaymentCostPopup.Show();
				return;
			}
			
			((ILekmerOrderFull)State.CurrentOrder).PaymentCost = paymentCostValue;
			PopulateSummary();
		}


		private void DeliveryMethodList_SelectedIndexChanged(object sender, EventArgs e)
		{
			Refresh();

			if (!IoC.Resolve<IDeliveryMethodSecureService>().CanChangeDeliveryMethod(State.CurrentOrder, State.CurrentOrder.DeliveryMethodId))
			{
				SystemMessageContainer.Add(Resources.OrderMessage.CannotChangeDeliveryMethod, InfoType.Warning);
				DeliveryMethodList.SelectedValue = State.CurrentOrder.DeliveryMethodId.ToString(CultureInfo.CurrentCulture);
				return;
			}
			State.CurrentOrder.DeliveryMethodId = int.Parse(DeliveryMethodList.SelectedValue, CultureInfo.CurrentCulture);
		}

		private void StatusDropDownListOnSelectedIndexChanged(object sender, EventArgs e)
		{
			Refresh();

			int previousOrderStatusId = State.CurrentOrder.OrderStatus.Id;
			if (!IoC.Resolve<IOrderSecureService>().CanChangeStatus(State.CurrentOrder, previousOrderStatusId))
			{
				SystemMessageContainer.Add(Resources.OrderMessage.CannotCancel, InfoType.Warning);
				StatusDropDownList.SelectedValue = State.CurrentOrder.OrderStatus.Id.ToString(CultureInfo.CurrentCulture);
				return;
			}

			int orderStatusId = int.Parse(StatusDropDownList.SelectedValue, CultureInfo.CurrentCulture);
			var orderStatus = IoC.Resolve<IOrderStatusSecureService>().GetAll().Single(s => s.Id == orderStatusId);
			State.CurrentOrder.OrderStatus = orderStatus;
		}

		private void TrackingIdTextBoxOnTextChanged(object sender, EventArgs e)
		{
			Refresh();

			State.CurrentOrder.DeliveryTrackingId = TrackingIdTextBox.Text;
		}

		private void OnAddProductClick(object sender, EventArgs e)
		{
			Refresh();

			if (!IoC.Resolve<IOrderItemSecureService>().CanAdd(State.CurrentOrder))
			{
				SystemMessageContainer.Add(Resources.OrderMessage.CannotAddItem, InfoType.Warning);
				return;
			}

			AddProductPopup.Show();
		}

		private void OnResendOrderConfirmation(object sender, EventArgs e)
		{
			Refresh();

			IoC.Resolve<IOrderSecureService>().SendConfirm(State.CurrentOrder);
			SystemMessageContainer.Add(Resources.OrderMessage.OrderConfirmationResentSuccessfully, InfoType.Success);
		}

		private void VoucherInfoGridOnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType == DataControlRowType.DataRow)
			{
				var voucherInfo = row.DataItem as IOrderVoucherInfo;
				if (voucherInfo == null)
				{
					return;
				}

				if (voucherInfo.DiscountType.HasValue)
				{
					string discountTypeName;

					switch (voucherInfo.DiscountType.Value)
					{
						case (int)VoucherValueType.Price:
							discountTypeName = Resources.Lekmer.Literal_DiscountType_Price;
							break;
						case (int)VoucherValueType.Percentage:
							discountTypeName = Resources.Lekmer.Literal_DiscountType_Percentage;
							break;
						case (int)VoucherValueType.GiftCard:
							discountTypeName = Resources.Lekmer.Literal_DiscountType_GiftCard;
							break;
						case (int)VoucherValueType.Code:
							discountTypeName = Resources.Lekmer.Literal_DiscountType_Code;
							break;
						default:
							discountTypeName = Resources.Lekmer.Literal_DiscountType_Unknown;
							break;
					}

					DataBoundLabel(row, "DiscountTypeLabel", discountTypeName);
				}

				if (voucherInfo.DiscountValue.HasValue)
				{
					DataBoundLabel(row, "DiscountValueLabel", voucherInfo.DiscountValue.Value.ToString(CultureInfo.CurrentCulture));
				}

				string voucherStatusName;
				switch (voucherInfo.VoucherStatus)
				{
					case (int)OrderVoucherStatus.Consumed:
						voucherStatusName = Resources.Lekmer.Literal_OrderVoucherStatus_Consumed;
						break;
					case (int)OrderVoucherStatus.Reserved:
						voucherStatusName = Resources.Lekmer.Literal_OrderVoucherStatus_Reserved;
						break;
					case (int)OrderVoucherStatus.Released:
						voucherStatusName = Resources.Lekmer.Literal_OrderVoucherStatus_Released;
						break;
					default:
						voucherStatusName = Resources.Lekmer.Literal_OrderVoucherStatus_Unknown;
						break;
				}

				DataBoundLabel(row, "VoucherStatusLabel", voucherStatusName);

				DataBoundLabel(row, "AmountUsedLabel", voucherInfo.AmountUsed);
			}
		}

		private void GiftCardViaMailInfoGridOnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType == DataControlRowType.DataRow)
			{
				var giftCardViaMailInfo = row.DataItem as IGiftCardViaMailInfo;
				if (giftCardViaMailInfo == null)
				{
					return;
				}

				string giftCardViaMailStatusName = ((GiftCardViaMailInfoStatus)giftCardViaMailInfo.StatusId).ToString();
				DataBoundLabel(row, "GiftCardViaMailInfoStatusLabel", giftCardViaMailStatusName);
			}
		}

		protected override void PopulateForm()
		{
			var order = IoC.Resolve<IOrderSecureService>().GetFullById(GetId());
			var orderComments = IoC.Resolve<IOrderCommentSecureService>().GetAllByOrder(GetId());
			var orderAudit = IoC.Resolve<IOrderAuditSecureService>().GetAllByOrder(GetId());

			State = new OrderState(order, orderComments, orderAudit, order.BillingAddress, order.DeliveryAddress, ((ILekmerOrderFull)order).AlternateAddress, order.Payments);

			SplitNotPerformedDiv.Visible = true;
			SplitPerformedDiv.Visible = false;

			PopulateOrder();
		}
		protected void PopulateOrder()
		{
			PopulateStatus();
			PopulateOrderData();
			PopulateOrderItems();
			PopulateCustomer();
			PopulateSummary();
			PopulateOrderComments();
			PopulateCampaigns();
			PopulateAudit();
			PopulateAddresses();
			PopulatePayments();
			PopulateVoucherInfo();
			PopulateGiftCardViaMailInfo();
		}
		private void PopulatePayments()
		{
			OrderPaymentsControl.OrderPayments = State.CurrentOrderPayments;
			OrderPaymentsControl.OrderFull = State.CurrentOrder;
		}
		private void PopulateAddresses()
		{
			DeliveryAddressEdit.OrderAddress = State.CurrentOrderDeliveryAddress;
			BillingAddressEdit.OrderAddress = State.CurrentOrderBillingAddress;
			DeliveryAddressEdit.OrderFull = State.CurrentOrder;
			BillingAddressEdit.OrderFull = State.CurrentOrder;
			AlternateAddressEdit.OrderAddress = State.CurrentOrderAlternateAddress;
			AlternateAddressEdit.OrderFull = State.CurrentOrder;
			AlternateAddressPanel.Visible = State.CurrentOrderAlternateAddress != null;
		}
		private void PopulateAudit()
		{
			OrderAuditListControl1.OrderAudit = State.CurrentOrderAudit;
		}
		private void ReBindAudit()
		{
			PopulateAudit();
			OrderAuditListControl1.REBind();
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.String.Format(System.String,System.Object,System.Object)")]
		protected void PopulateOrderData()
		{
			var lekmerOrderFull = (ILekmerOrderFull)State.CurrentOrder;

			OrderLabel.Text = string.Format("{0}: {1}", Resources.Order.Literal_OrderId, State.CurrentOrder.Number);
			OrderDateLabel.Text = string.Format("{0} {1}", Resources.Customer.Label_Date, lekmerOrderFull.CreatedDate);
			CustomerIPAddressLiteral.Text = State.CurrentOrder.IP;
			UserAgentLiteral.Text = lekmerOrderFull.UserAgent;
			var isDropShipItemExist = lekmerOrderFull.IsDropShipItemExist(lekmerOrderFull.GetOrderItems());
			IsOrderDropShip.Visible = isDropShipItemExist;
			DropShipInfoLink.NavigateUrl = LekmerPathHelper.DropShip.GetEditUrlForOrderEdit(System.Convert.ToInt32(lekmerOrderFull.Id, CultureInfo.CurrentCulture));

			CurrencyLiteral.Text = OrderChannel.Currency.Iso;
			OptionalCurrencyLiteral.Text = OrderChannel.Currency.Iso;
			DiapersCurrencyLiteral.Text = OrderChannel.Currency.Iso;

			FreightAmountTextBox.Text = State.CurrentOrder.FreightCost.ToLocalString();
			if (lekmerOrderFull.OptionalFreightCost.HasValue)
			{
				OptionalFreightAmountTextBox.Text = lekmerOrderFull.OptionalFreightCost.Value.ToLocalString();
			}
			if (lekmerOrderFull.DiapersFreightCost.HasValue)
			{
				DiapersFreightAmountTextBox.Text = lekmerOrderFull.DiapersFreightCost.Value.ToLocalString();
			}

			var channelSecureService = IoC.Resolve<IChannelSecureService>();
			var channel = channelSecureService.GetById(State.CurrentOrder.ChannelId);
			PaymentCostTextBox.Text = lekmerOrderFull.PaymentCost.ToLocalStringAsNumber();
			PaymentCostCurrencyLiteral.Text = channel.Currency.Iso;

			DeliveryMethodList.DataSource = IoC.Resolve<IDeliveryMethodSecureService>().GetAll(State.CurrentOrder.ChannelId);
			DeliveryMethodList.DataBind();
			DeliveryMethodList.SelectedValue = State.CurrentOrder.DeliveryMethodId.ToString(CultureInfo.CurrentCulture);

			TrackingIdTextBox.Text = State.CurrentOrder.DeliveryTrackingId;
			if (!string.IsNullOrEmpty(State.CurrentOrder.DeliveryTrackingId))
			{
				var deliveryMethod = IoC.Resolve<IDeliveryMethodSecureService>().GetById(State.CurrentOrder.ChannelId, State.CurrentOrder.DeliveryMethodId);
				TrackingFindButton.OnClientClick = string.Format(CultureInfo.CurrentCulture,
					"window.open(String.format('{0}',document.getElementById('{1}').value))",
					deliveryMethod.TrackingUrl,
					TrackingIdTextBox.ClientID);
			}
		}
		private void PopulateStatus()
		{
			StatusDropDownList.DataSource = IoC.Resolve<IOrderStatusSecureService>().GetAll();
			StatusDropDownList.DataBind();
			StatusDropDownList.SelectedValue = State.CurrentOrder.OrderStatus.Id.ToString(CultureInfo.CurrentCulture);
		}
		protected void PopulateCustomer()
		{
			ICustomer customer = IoC.Resolve<ICustomerSecureService>().GetById(State.CurrentOrder.CustomerId);
			CustomerLinkButton.Text = EncodeHelper.HtmlEncode(customer.CustomerInformation.FirstName + " " + customer.CustomerInformation.LastName);
			CustomerCivicNumberLabel.Text = ((ILekmerCustomerInformation) customer.CustomerInformation).IsCompany
				? Resources.Lekmer.Customer_OrganizationNumber
				: Resources.Customer.Literal_CivicNumber;
			CustomerCivicNumberLiteral.Text = customer.CustomerInformation.CivicNumber;

			var customerInfo = new StringBuilder();
			if (!string.IsNullOrEmpty(customer.CustomerInformation.PhoneNumber))
			{
				customerInfo.Append(FoundationEncoder.EncodeHtml(customer.CustomerInformation.PhoneNumber));
				customerInfo.Append("<br />");
			}
			if (!string.IsNullOrEmpty(customer.CustomerInformation.CellPhoneNumber))
			{
				customerInfo.Append(FoundationEncoder.EncodeHtml(customer.CustomerInformation.CellPhoneNumber));
				customerInfo.Append("<br />");
			}
			if (!string.IsNullOrEmpty(customer.CustomerInformation.Email))
			{
				customerInfo.Append(FoundationEncoder.EncodeHtml(customer.CustomerInformation.Email));
				customerInfo.Append("<br />");
			}
			CustomerInfoLiteral.Text = customerInfo.ToString();
			CustomerTypeLiteral.Text = ((ILekmerCustomerInformation) customer.CustomerInformation).IsCompany
				? Resources.Lekmer.Customer_Company
				: Resources.Lekmer.Customer_Private;
		}
		protected void PopulateOrderItems()
		{
			OrderItemGrid.DataSource = State.CurrentOrder.GetOrderItems();
			OrderItemGrid.DataBind();

			ShowOrderItemOptions();
		}
		protected void ShowOrderItemOptions()
		{
			bool selected = false;
			foreach (GridViewRow row in OrderItemGrid.Rows)
			{
				CheckBox selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					selected = true;
					break;
				}
			}
			if (selected)
			{
				ApplyToAllSelectedDiv.Style.Add("display", "block");
			}
			else
			{
				ApplyToAllSelectedDiv.Style.Add("display", "none");
			}
		}
		protected void PopulateSummary()
		{
			var lekmerOrder = (ILekmerOrderFull)State.CurrentOrder;

			Price originalPriceSummary = lekmerOrder.GetOriginalPriceSummary();
			Price discountPriceSummary = lekmerOrder.GetDiscountPriceSummary();

			if (discountPriceSummary.IncludingVat > 0m)
			{
				OriginalSubTotalDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, originalPriceSummary.IncludingVat);
				TotalDiscountDiv.InnerText = "-" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, discountPriceSummary.IncludingVat);
			}
			else
			{
				OriginalSubTotalRow.Visible = false;
				TotalDiscountRow.Visible = false;
			}

			SubTotalDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, State.CurrentOrder.GetActualPriceSummary().IncludingVat);
			SubTotalVatDiv.InnerText = "(" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, State.CurrentOrder.GetActualVatSummary()) + ")";

			FreightCostDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, State.CurrentOrder.FreightCost);
			FreightCostVatDiv.InnerText = "(" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, State.CurrentOrder.GetFreightCostVat()) + ")";

			if (lekmerOrder.OptionalFreightCost.HasValue)
			{
				OptionalFreightCostDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, lekmerOrder.OptionalFreightCost.Value);
				OptionalFreightCostVatDiv.InnerText = "(" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, lekmerOrder.GetOptionalFreightCostVat()) + ")";
			}
			else
			{
				OptionalFreightCostDiv.InnerText = " -- ";
				OptionalFreightCostVatDiv.InnerText = "( -- )";
			}

			if (lekmerOrder.DiapersFreightCost.HasValue)
			{
				DiapersFreightCostDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, lekmerOrder.DiapersFreightCost.Value);
				DiapersFreightCostVatDiv.InnerText = "(" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, lekmerOrder.GetDiapersFreightCostVat()) + ")";
			}
			else
			{
				DiapersFreightCostDiv.InnerText = " -- ";
				DiapersFreightCostVatDiv.InnerText = "( -- )";
			}

			PaymentCostDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, lekmerOrder.PaymentCost);
			PaymentCostVatDiv.InnerText = "(" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, lekmerOrder.GetPaymentCostVat()) + ")";

			TotalDiv.InnerText = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, State.CurrentOrder.GetPriceTotal().IncludingVat);
			TotalVatDiv.InnerText = "(" + Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, State.CurrentOrder.GetVatTotal()) + ")";
		}
		protected void PopulateOrderComments()
		{
			OrderCommentControl.OrderComments = State.CurrentOrderComments;
			OrderCommentControl.Order = State.CurrentOrder;
		}
		protected void PopulateCampaigns()
		{
			var dataSource = new Collection<IOrderCampaign>();

			var orderCampaignInfo = State.CurrentOrder.CampaignInfo;
			if (orderCampaignInfo != null)
			{
				var cartCampaigns = orderCampaignInfo.Campaigns;
				foreach (IOrderCampaign orderCampaign in cartCampaigns)
				{
					dataSource.Add(orderCampaign);
				}
			}

			CampaignGrid.DataSource = dataSource;
			CampaignGrid.DataBind();
		}
		protected void PopulateVoucherInfo()
		{
			var lekmerOrder = (ILekmerOrderFull) State.CurrentOrder;
			if (lekmerOrder.VoucherInfo == null)
			{
				VoucherInfoPanel.Visible = false;
				return;
			}

			VoucherInfoGrid.DataSource = new Collection<IOrderVoucherInfo> { lekmerOrder.VoucherInfo };
			VoucherInfoGrid.DataBind();
		}
		protected void PopulateGiftCardViaMailInfo()
		{
			int orderId = State.CurrentOrder.Id;

			var giftCardViaMailInfoSecureService = IoC.Resolve<IGiftCardViaMailInfoSecureService>();

			Collection<IGiftCardViaMailInfo> giftCardViaMailInfoList = giftCardViaMailInfoSecureService.GetAllByOrder(orderId);

			if (giftCardViaMailInfoList == null || giftCardViaMailInfoList.Count == 0)
			{
				GiftCardViaMailInfoPanel.Visible = false;
				return;
			}

			GiftCardViaMailInfoGrid.DataSource = giftCardViaMailInfoList;
			GiftCardViaMailInfoGrid.DataBind();
		}
		protected void PopulateOrderItemCampaigns(GridViewRow row, IOrderItem orderItem)
		{
			var orderItemCampaignInfo = orderItem.CampaignInfo;
			if (orderItemCampaignInfo != null && orderItemCampaignInfo.Campaigns.Count > 0)
			{
				var campaignsRepeater = row.FindControl("OrderItemCampaignRepeater") as Repeater;
				if (campaignsRepeater != null)
				{
					campaignsRepeater.DataSource = orderItemCampaignInfo.Campaigns;
					campaignsRepeater.DataBind();
				}
			}
			else
			{
				var campaignsPlaceHolder = row.FindControl("OrderItemCampaignPlaceHolder") as PlaceHolder;
				if (campaignsPlaceHolder != null)
				{
					campaignsPlaceHolder.Visible = false;
				}
			}
		}

		private void EnableFeatures()
		{
			DeliveryAddressEdit.EnableEditing = OrderFeatureSetting.Instance.EnableAddressEdit;
			BillingAddressEdit.EnableEditing = OrderFeatureSetting.Instance.EnableAddressEdit;
			AlternateAddressEdit.EnableEditing = OrderFeatureSetting.Instance.EnableAddressEdit;

			AddProductButton.Visible = OrderFeatureSetting.Instance.EnableOrderItemAdd;
			OrderItemGrid.Columns[9].Visible = OrderFeatureSetting.Instance.EnableOrderItemDelete;
			OrderItemGrid.Columns[0].Visible = EnableSplitAndDeleteFeature();
			DeleteSelectedButton.Visible = OrderFeatureSetting.Instance.EnableOrderItemDelete;
			SplitSelectedButton.Visible = EnableSplitFeature();

			FreightLiteral.Visible = !OrderFeatureSetting.Instance.EnableFreightEdit;
			FreightButton.Visible = OrderFeatureSetting.Instance.EnableFreightEdit;

			OptionalFreightLiteral.Visible = !OrderFeatureSetting.Instance.EnableFreightEdit;
			OptionalFreightButton.Visible = OrderFeatureSetting.Instance.EnableFreightEdit;

			DiapersFreightLiteral.Visible = !OrderFeatureSetting.Instance.EnableFreightEdit;
			DiapersFreightButton.Visible = OrderFeatureSetting.Instance.EnableFreightEdit;

			OrderPaymentsControl.EnableAdding = OrderFeatureSetting.Instance.EnablePaymentAdd;
			OrderPaymentsControl.EnableEditing = OrderFeatureSetting.Instance.EnablePaymentEdit;
			OrderPaymentsControl.EnableDeleting = OrderFeatureSetting.Instance.EnablePaymentDelete;

			PaymentCostLabel.Visible = !OrderFeatureSetting.Instance.EnablePaymentCostEdit;
			PaymentCostPopupButton.Visible = OrderFeatureSetting.Instance.EnablePaymentCostEdit;

			StatusDropDownList.Enabled = OrderFeatureSetting.Instance.EnableStatusChange;

			DeliveryMethodList.Enabled = OrderFeatureSetting.Instance.EnableDeliveryTypeChange;

			TrackingIdTextBox.ReadOnly = !OrderFeatureSetting.Instance.EnableDeliveryTrackingIdEdit;

			OrderCommentControl.EnableAdding = OrderFeatureSetting.Instance.EnableCommentAdd;

			ResendOrderConfirmationButton.Visible = OrderFeatureSetting.Instance.EnableResendOrderConfirmation;
			SaveButton.Visible = OrderFeatureSetting.Instance.EnableAddressEdit
				|| OrderFeatureSetting.Instance.EnableOrderItemAdd
				|| OrderFeatureSetting.Instance.EnableOrderItemDelete
				|| OrderFeatureSetting.Instance.EnableOrderItemChangeQuantity
				|| OrderFeatureSetting.Instance.EnableOrderSplit
				|| OrderFeatureSetting.Instance.EnableFreightEdit
				|| OrderFeatureSetting.Instance.EnablePaymentAdd
				|| OrderFeatureSetting.Instance.EnablePaymentEdit
				|| OrderFeatureSetting.Instance.EnablePaymentDelete
				|| OrderFeatureSetting.Instance.EnableStatusChange
				|| OrderFeatureSetting.Instance.EnableDeliveryTypeChange
				|| OrderFeatureSetting.Instance.EnableDeliveryTrackingIdEdit
				|| OrderFeatureSetting.Instance.EnableCommentAdd;
		}
		private bool EnableSplitAndDeleteFeature()
		{
			return OrderFeatureSetting.Instance.EnableOrderItemDelete
				|| EnableSplitFeature();
		}
		private bool ShowSplitFeature()
		{
			return !State.SplitPerformed
				&& State.CurrentOrder.GetOrderItems().Count > 1;
		}
		private bool EnableSplitFeature()
		{
			return OrderFeatureSetting.Instance.EnableOrderSplit
				&& ShowSplitFeature();
		}
		private void EnableFeatures(GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var refreshOrderButton = (Image)row.FindControl("RefreshOrderButton");
				refreshOrderButton.Visible = OrderFeatureSetting.Instance.EnableOrderItemChangeQuantity;
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var quantityTextBox = (TextBox)row.FindControl("QuantityTextBox");
				quantityTextBox.ReadOnly = !OrderFeatureSetting.Instance.EnableOrderItemChangeQuantity;

				var orderItemContextMenu = (ContextMenu)row.FindControl("OrderItemContextMenu");
				if (EnableSplitAndDeleteFeature())
				{
					orderItemContextMenu.Visible = true;

					var splitMenuItem = (HtmlGenericControl)orderItemContextMenu.FindControl("SplitMenuItem");
					splitMenuItem.Visible = EnableSplitFeature();

					var deleteMenuItem = (HtmlGenericControl)orderItemContextMenu.FindControl("DeleteMenuItem");
					deleteMenuItem.Visible = OrderFeatureSetting.Instance.EnableOrderItemDelete;
				}
				else
				{
					orderItemContextMenu.Visible = false;
				}
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.Parse(System.String)")]
		private bool Refresh()
		{
			foreach (GridViewRow row in OrderItemGrid.Rows)
			{
				var idHiddenField = (HiddenField)row.FindControl("IdHiddenField");
				var id = int.Parse(idHiddenField.Value, CultureInfo.CurrentCulture);

				var quantityTextBox = (TextBox)row.FindControl("QuantityTextBox");
				int newQuantity;
				if (!ValidateQuantityValue(quantityTextBox.Text, out newQuantity))
				{
					return false;
				}

				var orderItem = State.CurrentOrder.GetOrderItems().Single(item => item.Id == id);
				var previousQuantity = orderItem.Quantity;
				if (previousQuantity != newQuantity)
				{
					if (!IoC.Resolve<IOrderItemSecureService>().CanChangeQuantity(State.CurrentOrder, orderItem, previousQuantity))
					{
						SystemMessageContainer.Add(Resources.OrderMessage.CannotChangeQuantity, InfoType.Warning);
						return false;
					}

					State.CurrentOrder.SetOrderItemQuantity(id, newQuantity);
				}
			}
			return true;
		}

		private bool ValidateQuantityValue(string quantityValue, out int quantity)
		{
			if (quantityValue.Trim().Length == 0)
			{
				SystemMessageContainer.Add(Resources.OrderMessage.QuantityEmpty, InfoType.Warning);
				quantity = default(int);
				return false;
			}
			if (!int.TryParse(quantityValue, out quantity))
			{
				SystemMessageContainer.Add(Resources.OrderMessage.QuantityFormat, InfoType.Warning);
				return false;
			}
			if (quantity <= 0)
			{
				SystemMessageContainer.Add(Resources.OrderMessage.QuantityNotPositive, InfoType.Warning);
				return false;
			}
			return true;
		}

		private void DataBoundLabel(Control row, string labelId, decimal value)
		{
			var label = (Label)row.FindControl(labelId);
			label.Text = Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, value);
		}
		private void DataBoundLabel(Control row, string labelId, decimal value, string format)
		{
			var label = (Label)row.FindControl(labelId);
			label.Text = string.Format(format, Formatter.FormatPriceTwoOrLessDecimals(OrderChannel, value));
		}
		private void DataBoundLabel(Control row, string labelId, decimal? value)
		{
			if (value.HasValue)
			{
				DataBoundLabel(row, labelId, value.Value);
			}
		}
		private void DataBoundLabel(Control row, string labelId, string value)
		{
			var label = (Label)row.FindControl(labelId);
			label.Text = value;
		}

		private void Split(int id)
		{
			IEnumerable<int> ids = new Collection<int> { id };
			Split(ids);
		}
		private void Split(IEnumerable<int> ids)
		{
			var splitOrderItems = new Collection<IOrderItem>(State.CurrentOrder.GetOrderItems().Where(orderItem => ids.Contains(orderItem.Id)).ToList());
			if (!IoC.Resolve<IOrderSecureService>().CanSplit(State.CurrentOrder, splitOrderItems))
			{
				SystemMessageContainer.Add(Resources.OrderMessage.CannotSplit, InfoType.Warning);
				return;
			}

			var orderSecureService = IoC.Resolve<IOrderSecureService>();
			IOrderFull splitOrder = orderSecureService.CreateSplit(State.Order, ids);

			State.CopyAddressFields(State.BillingAddress, State.SplitOrderBillingAddress);
			State.CopyAddressFields(State.DeliveryAddress, State.SplitOrderDeliveryAddress);
			if (State.AlternateAddress != null && State.SplitOrderAlternateAddress != null)
			{
				State.CopyAddressFields(State.AlternateAddress, State.SplitOrderAlternateAddress);
			}

			State.SplitOrder = splitOrder as ILekmerOrderFull;
			State.SplitPerformed = true;

			SplitNotPerformedDiv.Visible = false;
			SplitPerformedDiv.Visible = true;
			OriginalOrderButton.Text = State.Order.Number;
			SplitOrderButton.Text = State.SplitOrder.Number;

			SwitchToSplitOrder();

			PopulateOrder();
			OrderCommentControl.REBind();
			OrderAuditListControl1.REBind();
			DeliveryAddressEdit.REBind();
			BillingAddressEdit.REBind();
			AlternateAddressEdit.REBind();
			OrderPaymentsControl.ReBind();
		}
		private void SwitchToOriginalOrder()
		{
			State.CurrentOrder = State.Order;
			State.SplitOrderComments = OrderCommentControl.OrderComments;
			State.CurrentOrderComments = State.OrderComments;

			State.SplitOrderAudit = OrderAuditListControl1.OrderAudit;
			State.CurrentOrderAudit = State.OrderAudit;

			State.SplitOrderDeliveryAddress = DeliveryAddressEdit.OrderAddress;
			State.CurrentOrderDeliveryAddress = State.DeliveryAddress;
			
			State.SplitOrderBillingAddress = BillingAddressEdit.OrderAddress;
			State.CurrentOrderBillingAddress = State.BillingAddress;

			State.SplitOrderAlternateAddress = AlternateAddressEdit.OrderAddress;
			State.CurrentOrderAlternateAddress = State.AlternateAddress;

			State.SplitOrderPayments = OrderPaymentsControl.OrderPayments;
			State.CurrentOrderPayments = State.OrderPayments;

			OriginalOrderDiv.Attributes["class"] = "list-item-container list-item-container-active";
			SplitOrderDiv.Attributes["class"] = "list-item-container";
		}
		private void SwitchToSplitOrder()
		{
			State.CurrentOrder = State.SplitOrder;
			State.OrderComments = OrderCommentControl.OrderComments;
			State.CurrentOrderComments = State.SplitOrderComments;

			State.OrderAudit = OrderAuditListControl1.OrderAudit;
			State.CurrentOrderAudit = State.SplitOrderAudit;

			State.DeliveryAddress = DeliveryAddressEdit.OrderAddress;
			State.CurrentOrderDeliveryAddress = State.SplitOrderDeliveryAddress;

			State.BillingAddress = BillingAddressEdit.OrderAddress;
			State.CurrentOrderBillingAddress = State.SplitOrderBillingAddress;

			State.AlternateAddress = AlternateAddressEdit.OrderAddress;
			State.CurrentOrderAlternateAddress = State.SplitOrderAlternateAddress;

			State.OrderPayments = OrderPaymentsControl.OrderPayments;
			State.CurrentOrderPayments = State.SplitOrderPayments;

			OriginalOrderDiv.Attributes["class"] = "list-item-container";
			SplitOrderDiv.Attributes["class"] = "list-item-container list-item-container-active";
		}

		private void DeleteItem(int id)
		{
			var orderItem = State.CurrentOrder.GetOrderItems().First(item => item.Id == id);
			if (id > 0)
			{
				if (!IoC.Resolve<IOrderItemSecureService>().CanDelete(State.CurrentOrder, orderItem))
				{
					SystemMessageContainer.Add(Resources.OrderMessage.CannotDeleteItem, InfoType.Warning);
					return;
				}
			}
			State.CurrentOrder.DeleteOrderItem(id);
		}
		private void DeleteItems(IEnumerable<int> ids)
		{
			foreach (int id in ids)
			{
				DeleteItem(id);
			}
		}
	}

	[Serializable]
	public class OrderState
	{
		public ILekmerOrderFull Order { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderComment> OrderComments { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderAudit> OrderAudit { get; set; }
		public IOrderAddress DeliveryAddress { get; set; }
		public IOrderAddress BillingAddress { get; set; }
		public IOrderAddress AlternateAddress { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderPayment> OrderPayments { get; set; }

		public ILekmerOrderFull SplitOrder { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderComment> SplitOrderComments { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderAudit> SplitOrderAudit { get; set; }
		public IOrderAddress SplitOrderDeliveryAddress { get; set; }
		public IOrderAddress SplitOrderBillingAddress { get; set; }
		public IOrderAddress SplitOrderAlternateAddress { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderPayment> SplitOrderPayments { get; set; }

		public IOrderFull CurrentOrder { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderComment> CurrentOrderComments { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderAudit> CurrentOrderAudit { get; set; }
		public IOrderAddress CurrentOrderDeliveryAddress { get; set; }
		public IOrderAddress CurrentOrderBillingAddress { get; set; }
		public IOrderAddress CurrentOrderAlternateAddress { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderPayment> CurrentOrderPayments { get; set; }

		public bool CurrentOriginalNotSplit
		{
			get
			{
				return CurrentOrder == Order;
			}
		}
		public bool SplitPerformed { get; set; }

		public OrderState(IOrderFull order, Collection<IOrderComment> orderComments, Collection<IOrderAudit> orderAudit, IOrderAddress billingAddress, IOrderAddress deliveryAddress, IOrderAddress alternateAddress, Collection<IOrderPayment> orderPayments)
		{
			Order = order as ILekmerOrderFull;
			CurrentOrder = Order;

			OrderComments = orderComments;
			CurrentOrderComments = OrderComments;
			SplitOrderComments = new Collection<IOrderComment>();

			OrderAudit = orderAudit;
			CurrentOrderAudit = OrderAudit;
			SplitOrderAudit = new Collection<IOrderAudit>();

			OrderPayments = orderPayments;
			CurrentOrderPayments = OrderPayments;
			SplitOrderPayments = new Collection<IOrderPayment>();

			BillingAddress = billingAddress;
			CurrentOrderBillingAddress = BillingAddress;
			SplitOrderBillingAddress = IoC.Resolve<IOrderAddressSecureService>().Create();
			CopyAddressFields(CurrentOrderBillingAddress, SplitOrderBillingAddress);

			DeliveryAddress = deliveryAddress;
			CurrentOrderDeliveryAddress = DeliveryAddress;
			SplitOrderDeliveryAddress = IoC.Resolve<IOrderAddressSecureService>().Create();
			CopyAddressFields(CurrentOrderDeliveryAddress, SplitOrderDeliveryAddress);

			AlternateAddress = alternateAddress;
			CurrentOrderAlternateAddress = AlternateAddress;
			SplitOrderAlternateAddress = IoC.Resolve<IOrderAddressSecureService>().Create();
			if (CurrentOrderAlternateAddress != null)
			{
				CopyAddressFields(CurrentOrderAlternateAddress, SplitOrderAlternateAddress);
			}

			SplitPerformed = false;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		internal void CopyAddressFields(IOrderAddress sourceAddress, IOrderAddress targetAddress)
		{
			targetAddress.Addressee = sourceAddress.Addressee;
			targetAddress.City = sourceAddress.City;
			targetAddress.CountryId = sourceAddress.CountryId;
			targetAddress.PhoneNumber = sourceAddress.PhoneNumber;
			targetAddress.PostalCode = sourceAddress.PostalCode;
			targetAddress.StreetAddress = sourceAddress.StreetAddress;
			targetAddress.StreetAddress2 = sourceAddress.StreetAddress2;

			var lekmerSourceAddress = sourceAddress as ILekmerOrderAddress;
			var lekmerTargetAddress = targetAddress as ILekmerOrderAddress;

			if (lekmerSourceAddress != null && lekmerTargetAddress != null)
			{
				lekmerTargetAddress.HouseNumber = lekmerSourceAddress.HouseNumber;
				lekmerTargetAddress.HouseExtension = lekmerSourceAddress.HouseExtension;
				lekmerTargetAddress.Reference = lekmerSourceAddress.Reference;
			}
		}
	}
}
