using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups
{
	public partial class CustomerGroupEdit : LekmerStatePageController<CustomerGroupState>, IEditor
	{
		private const string _savedSucceed = "SaveSuccess";
		private const string _referrer = "Referrer";

		private Collection<ICustomerStatus> _customerStatuses;
		private Collection<IPriceListStatus> _pricelistStatuses;
		private Collection<ICurrency> _currencies;

		protected virtual CustomerGroupsMaster MasterPage
		{
			get{ return (CustomerGroupsMaster) Master; }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			
			RemoveCustomersButton.Click += OnCustomersRemove;
			CustomersSearch.SelectEvent += OnCustomersAdd;
			CustomersGrid.PageIndexChanging += OnCustomersPageIndexChanging;
			CustomersGrid.RowDataBound += OnCustomersRowDataBound;
			CustomersGrid.RowCommand += OnCustomersRowCommand;

			PricelistsGrid.PageIndexChanging += OnPricelistsPageIndexChanging;

			NodeSelector.NodeCommand += NodeSelectorCommand;

            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            IoC.Resolve<ICustomerGroupSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
            Response.Redirect(PathHelper.Customer.CustomerGroup.GetDefaultUrl());
        }

		protected virtual void NodeSelectorCommand(object sender, CommandEventArgs e)
		{
			NodeSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			NodeSelector.DataBind();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var service = IoC.Resolve<ICustomerGroupSecureService>();
			var customerGroup = GetIdOrNull().HasValue ? service.GetById(GetId()) : service.Create();
			customerGroup.Title = TitleTextBox.Text;
			customerGroup.ErpId = ErpTextBox.Text.Trim();
			customerGroup.StatusId = int.Parse(StatusList.SelectedValue, CultureInfo.CurrentCulture);
			customerGroup.FolderId = NodeSelector.SelectedNodeId;

			customerGroup.Id = service.Save(SignInHelper.SignedInSystemUser, customerGroup, State.Customers);
			
			if (customerGroup.Id <= 0)
			{
				SystemMessageContainer.Add(Resources.CustomerMessage.CustomerGroupTitleExists, InfoType.Failure);
				return;
			}

			MasterPage.SelectedFolderId = customerGroup.FolderId ?? MasterPage.RootNodeId;
			MasterPage.PopulateTree(customerGroup.FolderId);
			if (GetIdOrNull().HasValue)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCustomerGroup, InfoType.Success);
				return;
			}
			
			RedirectEdit(customerGroup.Id);
		}

		protected virtual void OnCustomersAdd(object sender, UserControls.Customer.Events.CustomerSelectEventArgs e)
		{
			foreach (var customer in e.Customers)
			{
				var clone = customer;
				if (State.Customers.FirstOrDefault(c => c.Id == clone.Id) == null)
				{
					State.Customers.Add(customer);
				}
			}
			PopulateCustomersGrid();
		}

		protected virtual void OnCustomersRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = CustomersGrid.GetSelectedIds();
			foreach (var id in ids)
			{
				var cloneId = id;
				var customer = State.Customers.FirstOrDefault(c => c.Id == cloneId);
				if (customer != null)
				{
					State.Customers.Remove(customer);
				}
			}
			PopulateCustomersGrid();
		}

		protected virtual void OnCustomersRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnCustomersPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CustomersGrid.PageIndex = e.NewPageIndex;
			PopulateCustomersGrid();
		}

		protected virtual void OnCustomersRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteCustomer")
			{
				var customer = State.Customers.FirstOrDefault(c => c.Id == System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				if (customer != null)
				{
					State.Customers.Remove(customer);
					PopulateCustomersGrid();
				}
			}
		}

		protected virtual void OnPricelistsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			PricelistsGrid.PageIndex = e.NewPageIndex;
			PopulatePricelistsGrid();
		}

		protected override void PopulateForm()
		{
			if (Request.QueryString.GetBooleanOrNull(_savedSucceed) ?? false)
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCustomerGroup, InfoType.Success);
			
			PopulateCustomerGroup();
			PopulatePath();

			MasterPage.BreadcrumbAppend.Add(GetIdOrNull().HasValue ? Resources.Customer.Literal_EditGroup : Resources.Customer.Literal_CreateGroup);

		    DeleteButton.Visible = GetIdOrNull().HasValue;
		}

		protected virtual void PopulateCustomerGroup()
		{
			var service = IoC.Resolve<ICustomerGroupSecureService>();
			var customerGroup = GetIdOrNull().HasValue ? service.GetById(GetId()) : service.Create();
			TitleTextBox.Text = customerGroup.Title;
			ErpTextBox.Text = customerGroup.ErpId;
			
			PopulateCustomerGroupStatuses(customerGroup.StatusId);
			
			State = new CustomerGroupState(PopulateCustomers());
			PopulateCustomersGrid();
			PopulatePricelistsGrid();
			
			UpdateMasterTree(customerGroup.FolderId ?? MasterPage.RootNodeId);
		}

		protected virtual void PopulateCustomerGroupStatuses(int statusId)
		{
			StatusList.DataSource = IoC.Resolve<ICustomerGroupStatusSecureService>().GetAll();
			StatusList.DataBind();
			var item = StatusList.Items.FindByValue(statusId.ToString(CultureInfo.CurrentCulture));
			if (item != null) item.Selected = true;
		}

		protected virtual void PopulatePath()
		{
			int? parentId = GetParentIdOrNull()
			               ?? IoC.Resolve<ICustomerGroupSecureService>().GetById(GetId()).FolderId;
			if (parentId == MasterPage.RootNodeId) parentId = null;
			
			NodeSelector.SelectedNodeId = parentId;
			NodeSelector.DataSource = GetPathSource(parentId);
			NodeSelector.DataBind();
			NodeSelector.PopulatePath(parentId);

			MasterPage.SelectedFolderId = parentId;
			MasterPage.PopulateTree(parentId);
		}

		protected virtual Collection<ICustomer> PopulateCustomers()
		{
			return GetIdOrNull().HasValue
								? IoC.Resolve<ICustomerSecureService>().GetAllByCustomerGroup(GetId())
								: new Collection<ICustomer>();
		}

		protected virtual void PopulateCustomersGrid()
		{
			CustomersGrid.DataSource = State.Customers;
			CustomersGrid.DataBind();
			CustomersUpdatePanel.Update();
		}

		protected virtual Collection<IPriceList> PopulatePricelists()
		{
			return GetIdOrNull().HasValue
								? IoC.Resolve<IPriceListSecureService>().GetAllByCustomerGroup(GetId())
								: new Collection<IPriceList>();
		}

		protected virtual void PopulatePricelistsGrid()
		{
			PricelistsGrid.DataSource = PopulatePricelists();
			PricelistsGrid.DataBind();
			PricelistsUpdatePanel.Update();
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			return IoC.Resolve<ICustomerGroupFolderSecureService>().GetTree(folderId);
		}

		protected virtual string GetCustomerStatus(object statusId)
		{
			if (_customerStatuses == null)
				_customerStatuses = IoC.Resolve<ICustomerStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _customerStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual string GetPricelistStatus(object statusId)
		{
			if (_pricelistStatuses == null)
				_pricelistStatuses = IoC.Resolve<IPriceListStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _pricelistStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual string GetCurrency(object currencyId)
		{
			if (_currencies == null)
				_currencies = IoC.Resolve<ICurrencySecureService>().GetAll();

			int id = System.Convert.ToInt32(currencyId, CultureInfo.CurrentCulture);
			var currency = _currencies.FirstOrDefault(c => c.Id == id);
			return currency != null ? currency.Iso : string.Empty;
		}

		protected virtual int? GetParentIdOrNull()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString[_referrer];
			Response.Redirect(referrer != null 
				? PathHelper.Customer.CustomerGroup.GetReferrerUrl(referrer)
				: PathHelper.Customer.CustomerGroup.GetDefaultUrl());
		}

		protected void RedirectEdit(int id)
		{
			string referrer = Page.Request.QueryString[_referrer];
			string url = referrer != null
				? PathHelper.Customer.CustomerGroup.GetEditUrlWithReferrer(id, referrer)
				: PathHelper.Customer.CustomerGroup.GetEditUrl(id);
			Response.Redirect(string.Format(CultureInfo.CurrentCulture, "{0}&{1}={2}", url, _savedSucceed, bool.TrueString));
		}

		protected string GetPricelistEditUrl(object priceListId)
		{
			if (GetIdOrNull().HasValue)
				return PathHelper.Assortment.Pricelist.GetEditUrlForCustomerGroupEdit(System.Convert.ToInt32(priceListId, CultureInfo.CurrentCulture), GetId());
			
			if (GetParentIdOrNull().HasValue)
				return PathHelper.Assortment.Pricelist.GetEditUrlForCustomerGroupCreate(System.Convert.ToInt32(priceListId, CultureInfo.CurrentCulture), GetParentIdOrNull().Value);
			
			return string.Empty;
		}

		protected string GetCustomerEditUrl(object customerId)
		{
			if (GetIdOrNull().HasValue)
				return PathHelper.Customer.GetEditUrlForCustomerGroupEdit(System.Convert.ToInt32(customerId, CultureInfo.CurrentCulture), GetId());

			if (GetParentIdOrNull().HasValue)
				return PathHelper.Customer.GetEditUrlForCustomerGroupCreate(System.Convert.ToInt32(customerId, CultureInfo.CurrentCulture), GetParentIdOrNull().Value);

			return string.Empty;
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); 
					ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); 
					ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		//Use for cases when come from customer edit page or price list edit page.
		protected virtual void UpdateMasterTree(int folderId)
		{
		    var master = Master as CustomerGroupsMaster;
			if (master == null || folderId <= 0) return;

		    master.SelectedFolderId = folderId;
		}
	}

	[Serializable]
	public class CustomerGroupState
	{
		private readonly Collection<ICustomer> _customers;
		public Collection<ICustomer> Customers
		{
			get
			{
				return _customers;
			}
		}

		public CustomerGroupState(Collection<ICustomer> customers)
		{
			_customers = customers;
		}
	}
}