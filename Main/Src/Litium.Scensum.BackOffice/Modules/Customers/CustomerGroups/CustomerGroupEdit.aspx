﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Customers/CustomerGroups/CustomerGroups.Master" CodeBehind="CustomerGroupEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.CustomerGroupEdit" %>
<%@ Import Namespace="System.Globalization"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register Src="~/UserControls/Customer/CustomerSearch.ascx" TagName="CustomerSearch" TagPrefix="uc" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
			<ContentTemplate>
			 <uc:MessageContainer ID="SystemMessageContainer" MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
			 <uc:ScensumValidationSummary ID="ValidationSummary" ForeColor= "Black" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="vg" />
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="EditContent" ContentPlaceHolderID="CustomerGroupsPlaceHolder" runat="server">
	<script type="text/javascript">
        function confirmCustomerDelete() {
        	return DeleteConfirmation("<%= Resources.Customer.Literal_customer %>");
        }
	</script>
	
	<div id="customer-group-edit">
		<div class="input-box">
			<span><%=Resources.General.Literal_Title%>&nbsp;*</span>
			&nbsp;
			<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vg" />
			<br />
			<asp:TextBox ID="TitleTextBox" runat="server" CssClass="box" />
		</div>
		<div class="input-box">
			<span><%= Resources.General.Literal_ErpId %></span>
			<br />
			<asp:TextBox ID="ErpTextBox" runat="server" CssClass="box" />
		</div>
		<div class="input-box">
			<span><%= Resources.General.Literal_Status%>&nbsp;*</span>
			<br />
			<asp:DropDownList ID="StatusList" runat="server" DataTextField="Title" DataValueField="Id" CssClass="box"></asp:DropDownList>
		</div>
		<div class="input-box">
			<span ><%= Resources.General.Literal_PlaceInFolder %>&nbsp;*</span>
			<uc:NodeSelect ID="NodeSelector" runat="server" UseRootNode="false" AllowClearSelection="true" />
		</div>
		<br />
	</div>
	<br class="clear" />
	
	
	<div id="customer-group-customer" style="padding-top:40px;">
		<asp:UpdatePanel ID="CustomersUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<span><%= Resources.Customer.Literal_Members %></span>
				<br />
				<Scensum:Grid runat="server" ID="CustomersScensumGrid" FixedColumns="First name">
					<sc:GridViewWithCustomPager 
						ID="CustomersGrid" 
						SkinID="grid" 
						runat="server" 
						Width="100%"				
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>">
						<Columns>
							<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<HeaderTemplate>
									<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:CheckBox ID="SelectCheckBox" runat="server" />
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_ErpId %>" DataField="ErpId" ItemStyle-Width="13%" />
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_FirstName %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<uc:HyperLinkEncoded runat="server" ID="EditLink" NavigateUrl='<%# GetCustomerEditUrl(Eval("Id")) %>' Text='<%# Eval("CustomerInformation.FirstName")%>' />
								</ItemTemplate >
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_LastName %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="LastNameLiteral" runat="server" Text='<%# Eval("CustomerInformation.LastName")%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Status %>" ItemStyle-Width="10%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="StatusLiteral" runat="server" Text='<%# GetCustomerStatus(Eval("CustomerStatusId"))%>' />
								</ItemTemplate>
								</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_CivicNumber %>" ItemStyle-Width="9%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="CivicNumberLiteral" runat="server" Text='<%# Eval("CustomerInformation.CivicNumber")%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" ItemStyle-Width="11%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="CreatedDateLiteral" runat="server" Text='<%# Convert.ToDateTime(Eval("CustomerInformation.CreatedDate")).ToShortDateString() %>' /> 
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="EmailLiteral" runat="server" Text='<%# Eval("CustomerInformation.Email")%>'/>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteCustomer"
										CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
										OnClientClick="return confirmCustomerDelete();" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>
				</Scensum:Grid>
				<div id="AllSelectedDiv" runat="server" style="display: none;" class="apply-to-all-selected-2">
					<uc:ImageLinkButton ID="RemoveCustomersButton" runat="server" Text="<%$ Resources:Customer, Button_RemoveSelected%>" SkinID="DefaultButton" OnClientClick="return confirmCustomerDelete();" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="right">
			<uc:CustomerSearch ID="CustomersSearch" runat="server" />
		</div>
	</div>
	<br />
	
	
	
	<div id="customer-group-pricelist" style="padding-top:50px;">
		<asp:UpdatePanel ID="PricelistsUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<span><%= Resources.Customer.Literal_Pricelists %></span>
				<br />
				<Scensum:Grid runat="server" ID="PricelistsScensumGrid" FixedColumns="Title">
					<sc:GridViewWithCustomPager 
						ID="PricelistsGrid" 
						SkinID="grid" 
						runat="server" 
						Width="100%"				
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>">
						<Columns>
							<asp:BoundField HeaderText="<%$ Resources:General, Literal_ErpId %>" DataField="ErpId" ItemStyle-Width="13%" />
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="31%">
								<ItemTemplate>
									<uc:HyperLinkEncoded ID="TitleLink" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# GetPricelistEditUrl(Eval("Id")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Currency %>" ItemStyle-Width="7%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="CurrencyLiteral" runat="server" Text='<%# GetCurrency(Eval("CurrencyId"))%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="7%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="StatusLiteral" runat="server" Text='<%# GetPricelistStatus(Eval("PriceListStatusId"))%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_StartDate %>" ItemStyle-Width="18%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="StartDateLiteral" runat="server" Text='<%# Eval("StartDateTime")%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_EndDate %>" ItemStyle-Width="18%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="EndDateLiteral" runat="server" Text='<%# Eval("EndDateTime") %>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>
				</Scensum:Grid>
				<br />
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	
	
	
	<br />
	<asp:UpdatePanel id="FooterUpdatePanel" runat="server">
		<ContentTemplate>
		<div class="buttons left">
                    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('customer group');" />
                </div>
			<div class="buttons right">
				<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save%>" ValidationGroup="vg" SkinID="DefaultButton" />
				<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel%>" CausesValidation="false" SkinID="DefaultButton" />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
