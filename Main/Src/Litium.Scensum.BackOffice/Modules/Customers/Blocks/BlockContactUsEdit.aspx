﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="BlockContactUsEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.Blocks.BlockContactUsEdit" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer runat="server" ID="errors" MessageType="Failure" HideMessagesControlId="SaveButton" />
	<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="vgBlock"/>
</asp:Content>

<asp:Content ID="BlockEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="BlockEditPanel" runat="server" DefaultButton="SaveButton">
		<br clear="all" />
		<div class="content-box">
			<span><%= Resources.General.Literal_Title %></span>&nbsp
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp
			<asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox"
				ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgBlock" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br /><br />
			<span><%= Resources.Lekmer.ContactUs_Receiver %></span>&nbsp
			<asp:RequiredFieldValidator runat="server" ID="ReceiverRequiredValidator" ControlToValidate="ReceiverTextBox"
				ErrorMessage="<%$ Resources:LekmerMessage, ContactUs_ReceiverEmpty %>" Display="None" ValidationGroup="vgBlock" />
			<asp:RegularExpressionValidator runat="server" ID="ReceiverExpressionValidator" ControlToValidate="ReceiverTextBox"
				ErrorMessage="<%$ Resources:LekmerMessage, ContactUs_ReceiverIncorrect %>" Display="None" 
				ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgBlock" />
			<br />
			<asp:TextBox runat="server" ID="ReceiverTextBox" Width="300px" MaxLength="320"></asp:TextBox>
			<br />
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings %></span>
				<br />
				<div class="block-setting-content">
					<span><%= Resources.General.Literal_ChooseTemplate %></span><br />
					<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="194px" />
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" runat="server" ID="SaveButton" ValidationGroup="vgBlock" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" runat="server" ID="CancelButton" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
		</div>
	</asp:Panel>
</asp:Content>
