<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="BlockSignInEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.Blocks.BlockSignInEdit" %>
<%@ Register assembly="Litium.Scensum.Web.Controls" namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree.SiteStructure" tagprefix="TemplatedTreeView" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Modules.Customers.Blocks" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"	ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgBlockSignIn" />
	</asp:Content>
<asp:Content ID="BlockSignInEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
	<br clear="all" /><br clear="all" />
	<div class="content-box">
		<span><%= Resources.General.Literal_Title %></span>&nbsp;
		<uc:GenericTranslator ID="Translator" runat="server" />
		<br />
		<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="BlockTitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display ="None" ValidationGroup="vgBlockSignIn" />
		<br />
		<div class="block-setting-container">
			<span class="bold"><%= Resources.General.Label_Settings %></span>
			<br />
			<div class="block-setting-content">
				<span><%= Resources.General.Literal_ChooseTemplate %></span><br />
				<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="194px" />
			</div>
		</div>
		<br />
		<div class="float-left">
			<div id="product-parent-content-node">
				<span><%= Resources.Customer.Literal_RedirectContentNode %></span>
				<asp:UpdatePanel ID="RedirectUpdatePanel" runat="server">
					<ContentTemplate>
					<TemplatedTreeView:SiteStructureTreeView ID="SiteStructureTree" runat="server" DisplayTextControl="lbName"
							NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="ss-node-type"
							MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
							NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
							NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
							NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
							MenuContainerElementId="node-menu" FolderImageUrlOnline="~/Media/Images/SiteStructure/folder.png"
							FolderImageUrlOffline="~/Media/Images/SiteStructure/folder-off.png" FolderImageUrlHidden="~/Media/Images/SiteStructure/folder-hid.png"
							MasterPageImageUrl="~/Media/Images/SiteStructure/master.png" SubPageImageUrlOnline="~/Media/Images/SiteStructure/sub-page.png"
							SubPageImageUrlOffline="~/Media/Images/SiteStructure/sub-page-off.png" SubPageImageUrlHidden="~/Media/Images/SiteStructure/sub-page-hid.png"
							PageImageUrlOnline="~/Media/Images/SiteStructure/page.png" PageImageUrlOffline="~/Media/Images/SiteStructure/page-off.png"
							PageImageUrlHidden="~/Media/Images/SiteStructure/page-hid.png" ShortcutImageUrlOnline="~/Media/Images/SiteStructure/shortcut.png"
							ShortcutImageUrlOffline="~/Media/Images/SiteStructure/shortcut-off.png" ShortcutImageUrlHidden="~/Media/Images/SiteStructure/shortcut-hid.png"
							UrlLinkImageUrlOnline="~/Media/Images/SiteStructure/link.png" UrlLinkImageUrlOffline="~/Media/Images/SiteStructure/link-off.png"
							UrlLinkImageUrlHidden="~/Media/Images/SiteStructure/link-hid.png" TypeImageContainerId="img"
							TypeImageContainerCssClass="ss-node-type" MenuToOnlineContainerId="to-online"
							MenuToOfflineContainerId="to-offline" MenuToHiddenContainerId="to-hidden" MenuContainerElementCssClass="menu-container"
							MenuContainerShadowElementCssClass="menu-shadow" MenuContainerElementTypeOfPageCssClass="menu-container-typeof-page"
							MenuContainerShadowElementTypeOfPageCssClass="menu-shadow-typeof-page" MenuContainerShadowElementId="menu-shadow"
							MenuPageSpecificContainerId="menu-pages-container" 
							MenuItemsContainerElementId="menu-container" NodeNavigationAnchorId="aName" NodeSelectedCssClass="treeview-node-selected">
							<NodeTemplate>
								<div class="tree-item-cell-expand">
									<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
									<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
								</div>
								<div class="tree-item-cell-main">
									<img runat="server" id="img" class="ss-node-type tree-node-img" />
									<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
								</div>
								<br />
							</NodeTemplate>
						</TemplatedTreeView:SiteStructureTreeView>		
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" runat="server" ID="SaveButton"  Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgBlockSignIn"/>
			<uc:ImageLinkButton  UseSubmitBehaviour="true" runat="server" ID="CancelButton" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton"/>
		</div>
	</asp:Panel>
</asp:Content>