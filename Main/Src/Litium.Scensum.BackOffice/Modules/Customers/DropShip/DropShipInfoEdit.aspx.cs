﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.DropShip;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Customers.DropShip
{
	public partial class DropShipInfoEdit : LekmerPageController
	{
		private CultureInfo _seCultureInfo = new CultureInfo("sv-SE");

		private int _orderId;
		protected int OrderId
		{
			get { return _orderId > 0 ? _orderId : (_orderId = Request.QueryString.GetInt32("OrderId")); }
		}

		private string _supplierNo;
		protected string SupplierNo
		{
			get
			{
				if (string.IsNullOrEmpty(_supplierNo) && (DropShipCollection != null && DropShipCollection.Count >= 0))
				{
					_supplierNo = DropShipCollection[0].SupplierNo;
				}
				return _supplierNo;
			}
		}

		private bool _isFileSent;
		protected bool IsFileSent
		{
			get
			{
				if (DropShipCollection != null && DropShipCollection.Count >= 0)
				{
					_isFileSent = DropShipCollection[0].SendDate != null;
				}
				return _isFileSent;
			}
		}

		private IDropShipSetting _setting;
		protected IDropShipSetting Setting
		{
			get { return _setting ?? (_setting = IoC.Resolve<IDropShipSetting>()); }
		}

		private ISupplierSecureService _supplierService;
		protected ISupplierSecureService SupplierService
		{
			get { return _supplierService ?? (_supplierService = IoC.Resolve<ISupplierSecureService>()); }
		}

		private IDropShipSecureService _dropShipService;
		protected IDropShipSecureService DropShipService
		{
			get { return _dropShipService ?? (_dropShipService = IoC.Resolve<IDropShipSecureService>()); }
		}

		private IDropShipSharedService _dropShipSharedService;
		protected IDropShipSharedService DropShipSharedService
		{
			get { return _dropShipSharedService ?? (_dropShipSharedService = IoC.Resolve<IDropShipSharedService>()); }
		}

		private Collection<IDropShipInfo> _dropShipCollection;
		protected Collection<IDropShipInfo> DropShipCollection
		{
			get { return _dropShipCollection ?? (_dropShipCollection = DropShipService.GetAllByOrder(OrderId)); }
		}

		protected override void SetEventHandlers()
		{
			SendFilesButton.Click += OnSendFiles;
			ViewPdfButton.Click += OnViewFile;
			CancelButton.Click += OnCancel;
			DropShipOrderInfoGrid.RowDataBound += DropShipOrderInfoGridRowDataBound;
			DropShipOrderInfoSummaryGrid.RowDataBound += DropShipOrderInfoSummaryGridRowDataBound;
		}

		protected override void PopulateForm()
		{
			if (DropShipCollection == null || DropShipCollection.Count < 0)
			{
				SendFilesButton.Visible = false;
				ViewPdfButton.Visible = false;
				return;
			}
			else
			{
				SendFilesButton.Visible = true;
				ViewPdfButton.Visible = true;
			}

			IDropShipInfo generalInfo = DropShipCollection[0];
			IDropShipInfoFormatter generalInfoFormatter = new DropShipInfoFormatter {DropShipInfoSource = generalInfo};

			var orderId = generalInfoFormatter.OrderId;

			// General Info.
			OrdernummerLink.Text = orderId;
			OrdernummerLink.NavigateUrl = string.Format(CultureInfo.CurrentCulture, "{0}?Id={1}&Referrer={2}", PathHelper.Order.GetEditUrl(), orderId, string.Format(CultureInfo.CurrentCulture, "DropShip/DropShipInfoEdit.aspx?OrderId={0}", orderId));
			SupplierNameLiteral.Text = GetSupplier(generalInfoFormatter.SupplierNo);
			RegisteradLiteral.Text = generalInfoFormatter.OrderDate;
			KundnummerLiteral.Text = generalInfoFormatter.CustomerNumber;
			AviseringsnummerLiteral.Text = generalInfoFormatter.NotificationNumber;
			BetalningsmetodLiteral.Text = generalInfoFormatter.PaymentMethod;

			// Billing Address.
			BillingFirstNameLiteral.Text = generalInfoFormatter.BillingFirstName;
			BillingLastNameLiteral.Text = generalInfoFormatter.BillingLastName;
			BillingCompanyLiteral.Text = generalInfoFormatter.BillingCompany;
			BillingCoLiteral.Text = generalInfoFormatter.BillingCo;
			BillingStreetLiteral.Text = generalInfoFormatter.BillingStreet;
			BillingStreetNrLiteral.Text = generalInfoFormatter.BillingStreetNr;
			BillingEntranceLiteral.Text = generalInfoFormatter.BillingEntrance;
			BillingFloorLiteral.Text = generalInfoFormatter.BillingFloor;
			BillingApartmentNoLiteral.Text = generalInfoFormatter.BillingApartmentNumber;
			BillingZipCodeLiteral.Text = generalInfoFormatter.BillingZip;
			BillingCityLiteral.Text = generalInfoFormatter.BillingCity;
			BillingCountryLiteral.Text = generalInfoFormatter.BillingCountry;
			BillingCellphoneLiteral.Text = generalInfoFormatter.Phone;
			BillingEmailLiteral.Text = generalInfoFormatter.Email;

			generalInfo.PopulateDeliveryInfo();
			IDropShipInfoFormatter deliveryAddressFormatter = new DropShipInfoFormatter {DropShipInfoSource = generalInfo};
			// Delivery Address.
			DeliveryFirstNameLiteral.Text = deliveryAddressFormatter.DeliveryFirstName;
			DeliveryLastNameLiteral.Text = deliveryAddressFormatter.DeliveryLastName;
			DeliveryCompanyLiteral.Text = deliveryAddressFormatter.DeliveryCompany;
			DeliveryCoLiteral.Text = deliveryAddressFormatter.DeliveryCo;
			DeliveryStreetLiteral.Text = deliveryAddressFormatter.DeliveryStreet;
			DeliveryStreetNrLiteral.Text = deliveryAddressFormatter.DeliveryStreetNr;
			DeliveryEntranceLiteral.Text = deliveryAddressFormatter.DeliveryEntrance;
			DeliveryFloorLiteral.Text = deliveryAddressFormatter.DeliveryFloor;
			DeliveryApartmentNoLiteral.Text = deliveryAddressFormatter.DeliveryApartmentNumber;
			DeliveryZipCodeLiteral.Text = deliveryAddressFormatter.DeliveryZip;
			DeliveryCityLiteral.Text = deliveryAddressFormatter.DeliveryCity;
			DeliveryCountryLiteral.Text = deliveryAddressFormatter.DeliveryCountry;
			DeliveryCellphoneLiteral.Text = generalInfoFormatter.Phone;
			DeliveryEmailLiteral.Text = generalInfoFormatter.Email;

			// Order items.
			DropShipOrderInfoGrid.DataSource = DropShipCollection;
			DropShipOrderInfoGrid.DataBind();

			// Summary.
			decimal sum = DropShipCollection.Sum(dropShipInfo => Convert.ToDecimal(dropShipInfo.ProductSoldSum, _seCultureInfo));
			var summary = new DropShipInfoSummary
			{
				TotalDiscount = generalInfoFormatter.Discount,
				TotalSum = sum.ToString(_seCultureInfo)
			};
			DropShipOrderInfoSummaryGrid.DataSource = new Collection<DropShipInfoSummary> { summary };
			DropShipOrderInfoSummaryGrid.DataBind();
		}

		public virtual void OnSendFiles(object sender, EventArgs e)
		{
			var generalInfo = DropShipCollection[0];

			var item = new KeyValuePair<string, Collection<IDropShipInfo>>(SupplierNo, DropShipCollection);

			string csvFilePath = !string.IsNullOrEmpty(generalInfo.CsvFileName)
				? Path.Combine(Setting.DestinationDirectory, generalInfo.CsvFileName)
				: DropShipSharedService.SaveCsvFile(item);

			string pdfFilePath = !string.IsNullOrEmpty(generalInfo.PdfFileName)
				? Path.Combine(Setting.DestinationDirectory, generalInfo.PdfFileName)
				: DropShipSharedService.SavePdfFile(item);

			try
			{
				DropShipSharedService.SendMessage(item.Key, csvFilePath, pdfFilePath, IsFileSent);
				Messager.Add(Resources.LekmerMessage.DropShipEdit_ResendInfoSuccess, InfoType.Success);
			}
			catch (Exception ex)
			{
				Messager.Add(string.Format("ERROR: {0}", ex.Message), InfoType.Warning);
			}
		}

		public virtual void OnViewFile(object sender, EventArgs e)
		{
			IDropShipInfo generalInfo = DropShipCollection[0];
			string pdfFilePath = !string.IsNullOrEmpty(generalInfo.PdfFileName)
				? Path.Combine(Setting.DestinationDirectory, generalInfo.PdfFileName)
				: DropShipSharedService.SavePdfFile(new KeyValuePair<string, Collection<IDropShipInfo>>(SupplierNo, DropShipCollection));

			var sourceFile = new FileStream(pdfFilePath, FileMode.Open);
			var getContent = new byte[sourceFile.Length];
			sourceFile.Position = 0;
			sourceFile.Read(getContent, 0, (int)sourceFile.Length);
			sourceFile.Close();
			Response.ClearContent();
			Response.ClearHeaders();
			Response.Buffer = true;
			Response.ContentType = "application/pdf";
			Response.AddHeader("Content-Length", getContent.Length.ToString(CultureInfo.InvariantCulture));
			Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(pdfFilePath));
			Response.BinaryWrite(getContent);
			Response.Flush();
			Response.End();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer == null)
			{
				Response.Redirect(LekmerPathHelper.DropShip.GetDefaultUrl());
			}
			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		protected void DropShipOrderInfoGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						row.Cells[0].Text = DropShipInfoCaptions.SupplierArtNo;
						row.Cells[1].Text = DropShipInfoCaptions.LekmerArtNo;
						row.Cells[2].Text = DropShipInfoCaptions.Produkt;
						row.Cells[3].Text = DropShipInfoCaptions.Antal;
						row.Cells[4].Text = DropShipInfoCaptions.PrisSt;
						row.Cells[5].Text = DropShipInfoCaptions.Rabbat;
						row.Cells[6].Text = DropShipInfoCaptions.Summa;
					}
					break;
			}
		}
		protected void DropShipOrderInfoSummaryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						row.Cells[0].Text = DropShipInfoCaptions.TotalRabatt;
						row.Cells[1].Text = DropShipInfoCaptions.TotalSumma;
					}
					break;
			}
		}

		protected string GetSupplier(string supplierNo)
		{
			var supplier = SupplierService.GetByNo(supplierNo);
			return supplier != null ? supplier.Name : string.Empty;
		}
	}

	public class DropShipInfoSummary
	{
		public string TotalDiscount { get; set; }
		public string TotalSum { get; set; }
	}
}