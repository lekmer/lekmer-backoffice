<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.Newsletter.Default" %>

<%@ Import Namespace="Litium.Scensum.BackOffice.Base" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="NewsletterTabContent" ContentPlaceHolderID="body" runat="server">
	<Scensum:ToolBoxPanel ID="SubscriberPanelToolBoxPanel" runat="server" Text="<%$ Resources:Customer, Literal_SubscriberToolBoxPanel %>" ShowSeparator="false">
	</Scensum:ToolBoxPanel>

	<div style="width: 980px;float:left;margin-bottom:0px">
		<asp:UpdatePanel ID="MessagesContainerUpdatePanel" runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	<br class="clear" />

	<div style="float: left; width:980px; margin-bottom:-20px">
		<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" DisplayMode="BulletList" HeaderText="<%$ Resources:CustomerMessage, SubscriberSearchErrors %>" ValidationGroup="vgSearchSubscriber" />
	</div>

	<div id="search-block" style="float: left ">
		<span class="order-header"><%= Resources.General.Literal_Search %></span>
	</div>

	<asp:Panel ID="SubscriberSearchPanel" runat="server" DefaultButton="SearchButtonSubscriber">
		<asp:UpdatePanel ID="SearchUpdatePanelSubscriber" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div class="customer-content-box customer-caption">
					<div class="left">
						<div class="column">
							<div class="input-box">
									<span><%= Resources.Customer.Literal_Email %></span>
									<asp:RequiredFieldValidator runat="server" ID="EmailRequiredValidator" ControlToValidate="EmailTextBox" ErrorMessage="<%$ Resources:GeneralMessage, EmailEmpty %>" Display ="None" ValidationGroup="vgSearchSubscriber" />
									<br />
									<asp:TextBox ID="EmailTextBox" runat="server" />
								</div>
						</div>
					</div>
					<div class="right">
						<br />
						<div id="SearchButtonDiv" class="buttons right" style="margin-right: 7px; margin-top: 5px;">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SearchButtonSubscriber" Text="<%$ Resources:General, Button_Search %>" runat="server" ValidationGroup="vgSearchSubscriber" SkinID="DefaultButton" />
						</div>
					</div>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="SearchButtonSubscriber" EventName="Click" />
			</Triggers>
		</asp:UpdatePanel>
	</asp:Panel>
	
	<br class="clear" />
	<div class="order-horizontal-separator"></div>
	<br class="clear" />

	<label class="order-header" style="padding: 0px 20px 10px 20px; float: left; width: 96%;">
		<%= Resources.General.Literal_SearchResults %>
	</label>
	
	<div id="order-search-result">
		<div id="ResultsDiv" runat="server" visible="false">
			<span class="bold">List of subscribed customers</span>
			<asp:UpdatePanel ID="SearchSubscribedResultUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
						<Scensum:Grid runat="server" ID="SubscribedCustomerGrid" FixedColumns="Email">
							<sc:GridViewWithCustomPager ID="SearchSubscribedResultsGrid" SkinID="grid" runat="server" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>"
								AutoGenerateColumns="false" DataSourceID="NewsletterSubscriberDataSource" Width="100%">
								<Columns>
									<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" DataField="Email" ItemStyle-Width="60%" />
									<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" DataField="CreatedDate" ItemStyle-Width="15%" />
									<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_UpdatedDate %>" DataField="UpdatedDate" ItemStyle-Width="15%" />
									<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
									<ItemTemplate>
										<asp:LinkButton ID="UnsubscribeButton" runat="server" Text="Unsubscribe" CommandName="Unsubscribe" CommandArgument='<%# Eval("Id") %>' />
									</ItemTemplate>
								</asp:TemplateField>
								</Columns>
							</sc:GridViewWithCustomPager>
						</Scensum:Grid>
						<asp:ObjectDataSource ID="NewsletterSubscriberDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Customers.Newsletter.NewsletterSubscriberDataSource" />
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="SearchButtonSubscriber" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>

			<br />

			<span class="bold">List of unsubscribed customers</span>
			<asp:UpdatePanel ID="SearchUnsubscribedResultUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<Scensum:Grid runat="server" ID="UnsubscribedCustomerGrid" FixedColumns="Email">
						<sc:GridViewWithCustomPager ID="SearchUnsubscribedResultsGrid" SkinID="grid" runat="server" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>"
							AutoGenerateColumns="false" DataSourceID="NewsletterUnsubscriberDataSource" Width="100%">
							<Columns>
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" DataField="Email" ItemStyle-Width="40%" />
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_UnregisteredFrom %>" ItemStyle-Width="20%" >
									<ItemTemplate>
										<asp:Label ID="UnregisteredFromLabel" Text ="" runat="server"/>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" DataField="CreatedDate" ItemStyle-Width="15%" />
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_UpdatedDate %>" DataField="UpdatedDate" ItemStyle-Width="15%" />
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
									<ItemTemplate>
										<asp:LinkButton ID="SubscribeButton" runat="server" Text="Subscribe" CommandName="Subscribe" CommandArgument='<%# Eval("Id") %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
					</Scensum:Grid>
					<asp:ObjectDataSource ID="NewsletterUnsubscriberDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Customers.Newsletter.NewsletterUnsubscriberDataSource" />
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="SearchButtonSubscriber" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
		</div>
	</div>
</asp:Content>