﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Campaigns/Flags/FlagsMaster.master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Campaigns.Flags.Default" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessagePlaceHolder" runat="server">		
	<uc:MessageContainer ID="ErrorMessager"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:MessageContainer ID="SuccessMessager"  MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
</asp:Content>
<asp:Content ID="FlagsContent" ContentPlaceHolderID="FlagsPlaceHolder" runat="server">
	<script type="text/javascript">
        function confirmDelete() {
        	return DeleteConfirmation("<%= Resources.Campaign.Literal_FlagSmall %>");
        }
	</script>
	<asp:Panel ID="FlagsPanel" runat="server" DefaultButton="SaveButton">
		<div>
			<div>
				<asp:GridView ID="FlagsGrid" runat="server" SkinID="grid" AutoGenerateColumns="false" Width="100%">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server" />
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>	
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="43%">
							<ItemTemplate>
								<div style="width:190px; overflow:hidden">
									<uc:LinkButtonTrimmed ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' CommandName="NavigateFlag" CommandArgument='<%#Eval("Id")%>' MaxLength="50"></uc:LinkButtonTrimmed>
								</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Campaign, Literal_Class %>" ItemStyle-Width="43%" >
							<ItemTemplate>
								<uc:LiteralEncoded ID="ClassLiteral" runat="server" Text='<%# Eval("Class") %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>			
						<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
							<HeaderTemplate>
								<div class="inline">
									<%= Resources.General.Literal_SortOrder %>
									<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" CommandArgument="0" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
								</div>
							</HeaderTemplate>
							<ItemTemplate>
								<div class="inline">
									<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
									<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
									<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%#Eval("Ordinal") %>' />
									<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
									<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
								</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<ItemTemplate>
								<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteFlag"
									CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
									OnClientClick="return confirmDelete();" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
			<div id="AllSelectedDiv" class="flag-apply-to-selected" runat="server">
				<div style="float:left">
					<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="DeleteFlagButton" runat="server" Text="<%$ Resources:General,Button_Delete %>" OnClientClick="return confirmDelete();" />
				</div> 
			</div>
		</div>
		<br style="clear: both;" />
		<br/>
		<div runat="server" ID="SavePanel" class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgOrdinals" SkinID="DefaultButton"/>
		</div>
	</asp:Panel>
</asp:Content>
