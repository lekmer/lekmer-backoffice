﻿using System;
using System.Web.UI;

namespace Litium.Scensum.BackOffice.Modules.Campaigns.Flags
{
	public partial class FlagsMaster : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CreateButton.Click += OnFlagCreate;
		}

		protected void OnFlagCreate(object sender, EventArgs e)
		{
			Response.Redirect("FlagEdit.aspx");
		}
	}
}