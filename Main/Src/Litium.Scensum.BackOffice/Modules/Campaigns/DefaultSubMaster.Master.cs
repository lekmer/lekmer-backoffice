﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Common;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public partial class DefaultSubMaster : MasterPage, IEditor
	{
		private const string ChannelsSeperator = ",";

		private Collection<ICampaignFolder> _allFolders;
		private Collection<ICampaignStatus> _statuses;
		private Collection<ContextMenuDataSourceItem> _contextMenuStatuses;

		private ICampaignRegistryCampaignSecureService _campaignRegistryCampaignSecureService;
		public ICampaignRegistryCampaignSecureService CampaignRegistryCampaignSecureService
		{
			get { return _campaignRegistryCampaignSecureService ?? (_campaignRegistryCampaignSecureService = IoC.Resolve<ICampaignRegistryCampaignSecureService>()); }
		}

		private IContentPageSecureService _contentPageSecureService;
		public IContentPageSecureService ContentPageSecureService
		{
			get { return _contentPageSecureService ?? (_contentPageSecureService = IoC.Resolve<IContentPageSecureService>()); }
		}

		private ICampaignSecureService _campaignSecureService;
		public ICampaignSecureService CampaignSecureService
		{
			get { return _campaignSecureService ?? (_campaignSecureService = IoC.Resolve<ICampaignSecureService>()); }
		}

		private IProductCampaignSecureService _productCampaignSecureService;
		public IProductCampaignSecureService ProductCampaignSecureService
		{
			get { return _productCampaignSecureService ?? (_productCampaignSecureService = IoC.Resolve<IProductCampaignSecureService>()); }
		}

		private ICartCampaignSecureService _cartCampaignSecureService;
		public ICartCampaignSecureService CartCampaignSecureService
		{
			get { return _cartCampaignSecureService ?? (_cartCampaignSecureService = IoC.Resolve<ICartCampaignSecureService>()); }
		}

		private ICampaignLandingPageSecureService _campaignLandingPageSecureService;
		public ICampaignLandingPageSecureService CampaignLandingPageSecureService
		{
			get { return _campaignLandingPageSecureService ?? (_campaignLandingPageSecureService = IoC.Resolve<ICampaignLandingPageSecureService>()); }
		}

		public virtual CampaignsMaster MasterPage
		{
			get
			{
				return (CampaignsMaster)Master;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CampaignsGrid.RowCommand += OnRowCommand;
			CampaignsGrid.RowDataBound += OnRowDataBound;
			CampaignsGrid.PageIndexChanging += OnPageIndexChanging;
			SetButton.Click += OnSave;
			DeleteCampaignButton.Click += OnDelete;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack)
			{
				PopulateCampaigns();
			}
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
		}

		public virtual void OnDelete(object sender, EventArgs e)
		{
			var ids = CampaignsGrid.GetSelectedIds().ToList();
			if (ids.Count <= 0)
			{
				Messager.Add(Resources.CampaignMessage.NoCampaignSelected, InfoType.Warning);
				return;
			}
			DeleteCampaigns(ids);

			Messager.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);

			PopulateCampaigns();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(StatusList.SelectedValue) && string.IsNullOrEmpty(TypeList.SelectedValue))
			{
				Messager.Add(Resources.CampaignMessage.NoSetValue);
				return;
			}

			var campaignIds = CampaignsGrid.GetSelectedIds().ToList();
			if (campaignIds.Count <= 0)
			{
				Messager.Add(Resources.CampaignMessage.NoCampaignSelected, InfoType.Warning);
				return;
			}

			int statusId;
			if (int.TryParse(StatusList.SelectedValue, out statusId))
			{
				CampaignSecureService.SetStatus(SignInHelper.SignedInSystemUser, campaignIds, statusId);

				//Set auto generated content page online if one exists
				if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
				{
					foreach (var campaignId in campaignIds)
					{
						ChangeContentNodeStatus(campaignId, statusId);
					}
				}
			}

			if (!string.IsNullOrEmpty(TypeList.SelectedValue))
			{
				CampaignSecureService.SetType(SignInHelper.SignedInSystemUser, campaignIds, TypeList.SelectedValue == Resources.Campaign.Literal_TypeSelective);
			}

			Messager.Add(Resources.GeneralMessage.SaveSuccessCampaigns, InfoType.Success);
			PopulateCampaigns();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id)) return;

			switch (e.CommandName)
			{
				case "MoveCampaign":
					MasterPage.RedirectWithType(PathHelper.Campaign.GetCampaignMoveUrl(id));
					break;
				case "DeleteCampaign":
					DeleteCampaign(id);
					Messager.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
					break;
				case "SetAlways":
					CampaignSecureService.SetType(SignInHelper.SignedInSystemUser, id, false);
					Messager.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
					break;
				case "SetSelective":
					CampaignSecureService.SetType(SignInHelper.SignedInSystemUser, id, true);
					Messager.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
					break;
			}

			if (e.CommandName != "Page") // When page index is changed, row command is also raised.
			{
				PopulateCampaigns();
			}
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);

			var row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			if (IsSearchResult)
			{
				var campaignHyId = row.DataItem as ICampaignHyId;
				if (campaignHyId == null) return;


				if (campaignHyId.Id == 0)
				{
					e.Row.Cells[0].Text = campaignHyId.ErpId ?? "";
					e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
					e.Row.Cells[0].ColumnSpan = 5;
					var count = e.Row.Cells.Count;
					for (var i = 1; i < count; i++)
						e.Row.Cells[i].Visible = false;
				}
				else
				{
					PrepareRow(row, campaignHyId.Id, campaignHyId.CampaignStatus.Id.ToString(CultureInfo.CurrentCulture));
				}
			}
			else
			{
				var campaign = row.DataItem as ICampaign;
				if (campaign == null) return;

				PrepareRow(row, campaign.Id, campaign.CampaignStatus.Id.ToString(CultureInfo.CurrentCulture));
			}
						
		}

		private void PrepareRow(GridViewRow _row, int _id, string _statusId)
		{

			var statusList = (ContextMenu3)_row.FindControl("StatusList");
			statusList.DataSource = GetContextMenuStatuses();
			statusList.DataBind();
			statusList.SelectedValue = _statusId;

			var registries = CampaignRegistryCampaignSecureService.GetRegistriesByCampaignId(_id);
			if (registries.Count > 0)
			{
				string channels = string.Empty;
				foreach (var registry in registries)
				{
					channels += registry.CountryIso + ChannelsSeperator;
				}

				var lastSeperator = channels.LastIndexOf(ChannelsSeperator, StringComparison.Ordinal);
				if (lastSeperator > 0)
				{
					channels = channels.Substring(0, lastSeperator);
				}

				var channelsLiteral = (LiteralEncoded)_row.FindControl("ChannelsLiteral");
				channelsLiteral.Text = channels;
			}


		}

		protected void OnCampaignStatusChanged(object sender, ContextMenu3EventArgs e)
		{
			int statusId;
			if (!int.TryParse(e.SelectedValue, out statusId)) return;

			var idField = (HiddenField)((Control)sender).Parent.Parent.FindControl("IdHiddenField");
			int campaignId;
			if (!int.TryParse(idField.Value, out campaignId)) return;

			CampaignSecureService.SetStatus(SignInHelper.SignedInSystemUser, campaignId, statusId);

			//Set auto generated content page online if one exists
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				ChangeContentNodeStatus(campaignId, statusId);
			}

			Messager.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
		}

		protected virtual void ChangeContentNodeStatus(int campaignId, int statusId)
		{
			var landingPage = CampaignLandingPageSecureService.GetByCampaignIdSecure(campaignId);
			if (landingPage == null || landingPage.RegistryDataList == null) return;

			foreach (var page in landingPage.RegistryDataList)
			{
				var contentPage = ContentPageSecureService.GetById(page.ContentNodeId);
				contentPage.ContentNodeStatusId = statusId == 0 ? 0 : 1;
				ContentPageSecureService.Save(SignInHelper.SignedInSystemUser, contentPage);
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CampaignsGrid.PageIndex = e.NewPageIndex;
			PopulateCampaigns();
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected virtual void PopulateCampaigns()
		{
			if (IsSearchResult)
			{
				CampaignsGrid.DataSource = SearchCampaigns();
				CampaignsGrid.Columns[2].Visible = false;
				CampaignsGrid.Columns[3].Visible = false;
				CampaignsGrid.Columns[4].Visible = false;
				CampaignsGrid.Columns[5].Visible = false;
				CampaignsGrid.Columns[6].Visible = true;
				CampaignsGrid.Columns[7].Visible = true;

			}
			else
			{
				CampaignsGrid.DataSource = RetrieveCampaigns();
				CampaignsGrid.Columns[2].Visible = true;
				CampaignsGrid.Columns[3].Visible = true;
				CampaignsGrid.Columns[4].Visible = true;
				CampaignsGrid.Columns[5].Visible = true;
				CampaignsGrid.Columns[6].Visible = true;
				CampaignsGrid.Columns[7].Visible = false;

			}
			CampaignsGrid.DataBind();
			CampaignsGrid.Visible = CampaignsGrid.DataSource != null;

			bool resultExists = AllSelectedDiv.Visible = CampaignsGrid.Rows.Count > 0;
			if (resultExists)
			{
				PopulateStatuses();
				PopulateTypes();
			}
		}

		protected virtual Collection<ICampaign> RetrieveCampaigns()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return CampaignSecureService.GetAllByFolder(folderId.Value);
			}
			return null;
		}

		protected virtual Collection<ICampaignHyId> SearchCampaigns()
		{
			var searchUrl = MasterPage.GetSearchPageUrl();
			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);
			if (string.IsNullOrEmpty(searchCriteria)) return new Collection<ICampaignHyId>();
			var result = new Collection<ICampaignHyId>();
			var list = MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
				? ((LekmerCampaignSecureService) CampaignSecureService).SearchProductCampaignsByHyId(searchCriteria)
				: ((LekmerCampaignSecureService) CampaignSecureService).SearchCartCampaignsByHyId(searchCriteria);

			if (list.Any())
			{
				var currentErp = list[0].ErpId;
				if (!string.IsNullOrEmpty(currentErp))
				{
					result.Add(new CampaignHyId() {ErpId = currentErp});
				}
				foreach (var item in list)
				{
					if (item.ErpId != currentErp)
					{
						result.Add(new CampaignHyId() {ErpId = item.ErpId});
						currentErp = item.ErpId;

					}
					result.Add(item);
				}
			}
			return result;
		}

		protected virtual void PopulateStatuses()
		{
			StatusList.DataSource = RetrieveStatuses();
			StatusList.DataBind();
			StatusList.Items.Insert(0, new ListItem(Resources.Campaign.Literal_StatusEmpty, string.Empty) { Selected = true });
		}

		protected virtual void PopulateTypes()
		{
			TypeList.Items.Clear();
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeEmpty, string.Empty) { Selected = true });
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeAlways, Resources.Campaign.Literal_TypeAlways));
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeSelective, Resources.Campaign.Literal_TypeSelective));
		}

		protected virtual string GetEditUrl(object campaignId)
		{
			string url = PathHelper.Campaign.GetEditUrl(System.Convert.ToInt32(campaignId, CultureInfo.CurrentCulture));
			return MasterPage.GetUrlWithType(url);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<ICampaignFolderSecureService>();
				_allFolders = MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
								? service.GetAllProductCampaignFolders()
								: service.GetAllCartCampaignFolders();
			}

			var folders = _allFolders;
			var folder = folders.FirstOrDefault(f => f.Id == System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
			if (folder == null) return string.Empty;

			var stack = new Stack<string>();
			stack.Push(folder.Title);
			while (folder.ParentFolderId.HasValue)
			{
				var child = folder;
				folder = folders.FirstOrDefault(f => f.Id == child.ParentFolderId.Value);
				stack.Push(folder.Title);
			}

			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString().TrimEnd(new[] { '\\' });
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual void DeleteCampaign(int campaignId)
		{
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				DeleteAutoCampaign(campaignId);
				ProductCampaignSecureService.Delete(SignInHelper.SignedInSystemUser, campaignId);
			}
			else
			{
				CartCampaignSecureService.Delete(SignInHelper.SignedInSystemUser, campaignId);
			}
		}

		protected virtual void DeleteCampaigns(List<int> campaignIds)
		{
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				foreach (var campaignId in campaignIds)
				{
					DeleteAutoCampaign(campaignId);
				}

				ProductCampaignSecureService.Delete(SignInHelper.SignedInSystemUser, campaignIds);
			}
			else
			{
				CartCampaignSecureService.Delete(SignInHelper.SignedInSystemUser, campaignIds);
			}
		}

		protected virtual void DeleteAutoCampaign(int campaignId)
		{
			var landingPage = CampaignLandingPageSecureService.GetByCampaignIdSecure(campaignId);
			if (landingPage == null || landingPage.RegistryDataList == null) return;

			foreach (var page in landingPage.RegistryDataList)
			{
				//Delete content page for campaign
				ContentPageSecureService.Delete(SignInHelper.SignedInSystemUser, page.ContentNodeId);

				//Delete tags for campaign
				var tagSecureService = IoC.Resolve<ITagSecureService>();
				var tags = tagSecureService.GetAllByTagGroup(AutoCampaignPageSetting.Instance.CampaignTagGroupId);
				foreach (var tag in tags)
				{
					if (tag.CommonName == campaignId.ToString(CultureInfo.InvariantCulture))
					{
						tagSecureService.Delete(SignInHelper.SignedInSystemUser, tag.Id);
					}
				}
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ICampaignStatus> RetrieveStatuses()
		{
			return _statuses
				?? (_statuses = IoC.Resolve<ICampaignStatusSecureService>().GetAll());

		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ContextMenuDataSourceItem> GetContextMenuStatuses()
		{
			return _contextMenuStatuses
				?? (_contextMenuStatuses = new Collection<ContextMenuDataSourceItem>(RetrieveStatuses().Select(
					item => new ContextMenuDataSourceItem
					{
						Text = item.Title,
						Value = item.Id.ToString(CultureInfo.CurrentCulture),
						ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture,
						"~/Media/Images/Common/{0}.gif", item.CommonName))
					}).ToList()));
		}

	
	}
}
