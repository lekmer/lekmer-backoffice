﻿<%@ Page
	Title=""
	Language="C#"
	MasterPageFile="~/Modules/Campaigns/CampaignsMaster.Master"
	AutoEventWireup="true"
	CodeBehind="CampaignEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Campaigns.CampaignEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="CampaignGeneral" Src="~/UserControls/Campaign/CampaignGeneral.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConditionList" Src="~/UserControls/Campaign/ConditionList.ascx" %>
<%@ Register TagPrefix="uc" TagName="ActionList" Src="~/UserControls/Campaign/ActionList.ascx" %>
<%@ Register TagPrefix="uc" TagName="ToolTip" Src="~/UserControls/ToolTip/ToolTip.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="MediaItemSelect" Src="~/UserControls/Media/MediaItemSelect.ascx"%>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="CampaignValidationMessage" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton "/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ProductCampaignsPlaceHolder" runat="server">

	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>

	<!-- Javascript for Actions/Conditions helptext -->
	<script type="text/javascript">
		$(document).ready(function () {
			$("#<%=ConditionTypeList.ClientID %>").change(function () {
				displayConditionHelpText();
			});
			$("#<%=ActionTypeList.ClientID %>").change(function () {
				displayActionHelpText();
			});

			displayConditionHelpText();
			displayActionHelpText();
		});

		function displayConditionHelpText() {
			var el = document.getElementById("<%=ConditionTypeList.ClientID %>");

			var helpText = conditionsHelpText[el.options[el.selectedIndex].value];

			if (helpText) {
				$('#HelpTextForConditions').attr('style', 'display:block;white-space:normal;');
				$('#HelpTextForConditions').html(helpText);
			}
			else {
				$('#HelpTextForConditions').hide();
			}
		}

		function displayActionHelpText() {
			var el = document.getElementById("<%=ActionTypeList.ClientID %>");

			var helpText = actionsHelpText[el.options[el.selectedIndex].value];

			if (helpText) {
				$('#HelpTextForActions').attr('style', 'display:block;white-space:normal;');
				$('#HelpTextForActions').html(helpText);
			}
			else {
				$('#HelpTextForActions').hide();
			}
		}
		var conditionsHelpText = new Array();
		<%=ConditionsHelpTexts %>

		var actionsHelpText = new Array();
		<%=ActionsHelpTexts %>

		function ResetDefaultMediaMessages() {
			$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
		}
		
		$(function () {
			media_url = '<%=ResolveUrl("~/Media") %>';
			$('div.light-box a.light-box').lightBox();
		});

		function confirmDeleteMediaItem() {
			return DeleteConfirmation("<%= Resources.Media.Literal_mediaItem%>");
		}
	</script>

	<uc:CampaignGeneral ID="CampaignGeneralControl" runat="server" />

	<br class="clear" />
	
	<uc:GenericWysiwygTranslator ID="CampaignDescriptionTranslator" runat="server" />

	<div class="campaign-general">
		<div class="campaign-general-row">
			<asp:Panel runat="server" ID="ChannelsPanel" GroupingText="<%$ Resources:Lekmer, Campaign_ApplyToChannels %>" CssClass="input-box">
				<asp:Repeater runat="server" ID="RegistriesRepeater">
					<ItemTemplate>
						<div>
							<div class="left" style="width: 120px;">
								<asp:HiddenField runat="server" ID="ChannelIdHidden" Value='<%# Eval("Id") %>' />
								<asp:CheckBox runat="server" ID="ChannelCheckbox" Checked='<%# Eval("IsSelected") %>' Text='<%# Eval("Title") %>' />
							</div>
						</div>
						<br class="clear" />
					</ItemTemplate>
				</asp:Repeater>
			</asp:Panel>
			<asp:Panel runat="server" ID="FlagsPanel" CssClass="input-box label-row">
				<asp:Label runat="server" AssociatedControlID="FlagDropDownList"><%= Resources.Campaign.Literal_CampaignFlag %> * <uc:ToolTip runat="server" AliasName="Backoffice.Tooltip.CampaignFlag" /></asp:Label>
				<asp:DropDownList ID="FlagDropDownList" runat="server" />
			</asp:Panel>
			<asp:Panel runat="server" ID="CampaignLevelPanel" CssClass="input-box label-row">
				<asp:Label runat="server" AssociatedControlID="CampaignLevel"><%= Resources.Campaign.Literal_CampaignLevel %> * <uc:ToolTip runat="server" AliasName="Backoffice.Tooltip.CampaignLevel" /></asp:Label>
				<asp:DropDownList ID="CampaignLevel" runat="server" />
			</asp:Panel>
			<asp:Panel runat="server" ID="PriceTypePanel" CssClass="input-box label-row">
				<asp:Label runat="server" AssociatedControlID="PriceType"><%= Resources.Campaign.Literal_CampaignPriceType %> * <uc:ToolTip runat="server" AliasName="Backoffice.Tooltip.CampaignPriceType" /></asp:Label>
				<asp:DropDownList ID="PriceType" runat="server" Width="125" />
			</asp:Panel>
			<asp:Panel runat="server" ID="TagPanel" CssClass="input-box label-row">
				<asp:Label runat="server" AssociatedControlID="CampaignTag"><%= Resources.Lekmer.Literal_Tag %> * <uc:ToolTip runat="server" AliasName="Backoffice.Tooltip.CampaignTag" /></asp:Label>
				<asp:DropDownList ID="CampaignTag" runat="server" Width="180" />
			</asp:Panel>
		</div>
	</div>

	<br class="clear" />
	<br />

	<div id="conditionToolbar">
		<div class="collapsible-items left">
			<div id="ConditionCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<span><%=Resources.Campaign.Literal_Conditions%></span>
				</div>
				<div id="ConditionCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Literal_Type%></span> &nbsp;
						<asp:DropDownList ID="ConditionTypeList" runat="server"></asp:DropDownList>
					</div>
					<div class="link-button">
						<uc:ImageLinkButton ID="OpenConditionPopupButton" runat="server" Text="<%$ Resources:General,Button_Add %>" SkinID="DefaultButton" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ConditionPanelStateHidden" runat="server" Value="true" />
					<asp:Image ID="ConditionIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
	</div>

	<br />

	<div id="ConditionPanelDiv" runat="server" class="left">
		<asp:Panel ID="ConditionPanel" runat="server">
			<uc:ConditionList ID="ConditionListControl" runat="server" />
		</asp:Panel>
	</div>

	<br class="clear" />
	<br />
	<span id="HelpTextForConditions" class="classic" style="display:none;"></span>
	<br />

	<div id="errorDiv" runat="server" visible="False" style="color: red;">
		<span><%= Resources.CampaignMessage.IncorrectCountOfGiftCardActions %></span>
	</div>

	<br />

	<div id="actionToolbar">
		<div class="collapsible-items left">
			<div id="ActionCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<span><%=Resources.Campaign.Literal_Actions%></span>
				</div>
				<div id="ActionCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Literal_Type%></span> &nbsp;
						<asp:DropDownList ID="ActionTypeList" runat="server"></asp:DropDownList>
					</div>
					<div class="link-button">
						<uc:ImageLinkButton ID="OpenActionPopupButton" runat="server" Text="<%$ Resources:General,Button_Add %>" SkinID="DefaultButton" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ActionPanelStateHidden" runat="server" Value="true" />
					<asp:Image ID="ActionIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
	</div>

	<br />

	<div id="ActionPanelDiv" runat="server" class="left">
		<asp:Panel ID="ActionPanel" runat="server">
			<uc:ActionList ID="ActionListControl" runat="server" />
		</asp:Panel>
	</div>

	<br class="clear" />
	<br />
	<span id="HelpTextForActions" class="classic" style="display:none;"></span>
	<br class="clear" />

	<div>
		<div class="collapsible-items left">
			<div id="LandingPageCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<span><%=Resources.Lekmer.Campaign_AutoCampaigns%></span>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="LandingPagePanelStateHidden" runat="server" Value="true" />
					<asp:Image ID="LandingPageIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
	</div>
	<br />
	<div id="LandingPageDiv" runat="server">
		<asp:Panel runat="server" ID="AutoCampaignPanel" GroupingText="<%$ Resources:Lekmer, Campaign_AutoCampaigns %>" Visible="true" CssClass="left" Style="padding-right: 20px;width:100%;padding-bottom:20px;">
			<asp:CheckBox runat="server" Text="<%$ Resources:Lekmer, Campaign_UseAutoCampaigns %>" ID="CheckboxUseCampaignPage" OnCheckedChanged="ToggleAutoCampaignContent" AutoPostBack="true" />
			<div runat="server" id="AutoCampaignContent">
				<div class="input-box">
					<asp:Label ID="Label4" runat="server" Text="<%$ Resources:Campaign,Label_WebTitle %>" />
					<uc:GenericTranslator ID="CampaignWebTitleTranslator" runat="server" />

					<div>
						<asp:TextBox runat="server" ID="CampaignWebTitle" />
					</div>
				</div>

				<div class="input-box">
					<asp:Label ID="Label5" runat="server" Text="<%$ Resources:Campaign,Label_WebDescription %>" />
					<asp:ImageButton runat="server" ID="CampaignDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />

					<div>
						<uc:LekmerTinyMceEditor ID="CampaignDescriptionEditor" runat="server"  SkinID="tinyMCE"/>
					</div>
				</div>

				<div class="media-catalog-container">
					<asp:UpdatePanel ID="ImageMediaUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<uc:GridViewWithCustomPager 
								ID="ImageMediaItemsGrid"
								SkinID="grid"
								runat="server"
								AutoGenerateColumns="false"
								AllowPaging="true"
								PageSize="<%$AppSettings:DefaultGridPageSize%>"
								Width="100%">
								<Columns>
									<asp:TemplateField ItemStyle-Height="50px" ItemStyle-Width="6%" >
										<ItemTemplate>
											<asp:Image id="imgArchiveImage" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
											<asp:HiddenField ID="imgIdHiddenField" Value='<%# Eval("Id") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title%>" ItemStyle-Width="25%">
										<ItemTemplate>
											<asp:Label ID="imgTitle" runat="server" Text='<%# Eval("MediaItem.Title") %>' />
										</ItemTemplate>
									</asp:TemplateField>
									
									<asp:TemplateField HeaderText="Channels" ItemStyle-Width="45%">
										<ItemTemplate>
											<asp:Repeater runat="server" ID="ImageChannels">
												<ItemTemplate>
													<asp:HiddenField ID="hfImgChannelId" Value='<%#Eval("Id") %>' runat="server" />
													<asp:CheckBox ID="cbImgChannel" runat="server" Checked='<%#Eval("IsSelected") %>' />
													<asp:Literal ID="imgChannelName" runat="server" Text='<%# Eval("ShortName") %>' />
													&nbsp;&nbsp;&nbsp;
												</ItemTemplate>
											</asp:Repeater>
										</ItemTemplate>
									</asp:TemplateField>

									<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<ItemTemplate>
											<asp:ImageButton ID="imgDeleteButton" runat="server" CommandName="DeleteMediaItem" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDeleteMediaItem();" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</uc:GridViewWithCustomPager>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<uc:ImageLinkButton runat="server" ID="ImageMediaItemBrowseButton" Text="Add image" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();"/>

				<br class="clear" />
				<hr />

				<div class="media-catalog-container">
					<asp:UpdatePanel ID="IconMediaUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<uc:GridViewWithCustomPager 
								ID="IconMediaItemsGrid"
								SkinID="grid"
								runat="server"
								AutoGenerateColumns="false"
								AllowPaging="true"
								PageSize="<%$AppSettings:DefaultGridPageSize%>"
								Width="100%">
								<Columns>
									<asp:TemplateField ItemStyle-Height="50px" ItemStyle-Width="6%" >
										<ItemTemplate>
											<asp:Image id="imgArchiveIcon" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
											<asp:HiddenField ID="iconIdHiddenField" Value='<%# Eval("Id") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title%>" ItemStyle-Width="25%">
										<ItemTemplate>
											<asp:Label ID="iconTitle" runat="server" Text='<%# Eval("MediaItem.Title") %>' />
										</ItemTemplate>
									</asp:TemplateField>
									
									<asp:TemplateField HeaderText="Channels" ItemStyle-Width="45%">
										<ItemTemplate>
											<asp:Repeater runat="server" ID="IconChannels">
												<ItemTemplate>
													<asp:HiddenField ID="hfIconChannelId" Value='<%#Eval("Id") %>' runat="server" />
													<asp:CheckBox ID="cbIconChannel" runat="server" Checked='<%#Eval("IsSelected") %>' />
													<asp:Literal ID="iconChannelName" runat="server" Text='<%# Eval("ShortName") %>' />
													&nbsp;&nbsp;&nbsp;
												</ItemTemplate>
											</asp:Repeater>
										</ItemTemplate>
									</asp:TemplateField>

									<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										<ItemTemplate>
											<asp:ImageButton ID="iconDeleteButton" runat="server" CommandName="DeleteMediaItem" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDeleteMediaItem();" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</uc:GridViewWithCustomPager>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<uc:ImageLinkButton runat="server" ID="IconMediaItemBrowseButton" Text="Add icon" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();"/>
			</div>
		</asp:Panel>
	</div>

	<br class="clear-all" />
	<br clear="all" />

	<div class="buttons left">
		<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteCampaignButton" runat="server" Text="<%$ Resources:General,Button_Delete %>" SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('campaign');" />
	</div>

	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" />
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
	</div>

	<!-- Condition Popup Start -->
	<div id="ConditionPopupDiv" runat="server" class="campaign-popup-container small-campaign-popup-container"
		style="z-index: 10010; display: none;">
		<asp:Panel ID="ConditionPopupPanel" runat="server" DefaultButton="ConditionPopupOkButton">
			<div class="campaign-popup-header">
				<div class="campaign-popup-header-left">
				</div>
				<div class="campaign-popup-header-center small-campaign-popup-header-center">
					<span><%= Resources.Campaign.Literal_ConfigureCondition%></span>
					<input type="button" id="ConditionPopupCloseButton" runat="server" value="x" />
				</div>
				<div class="campaign-popup-header-right">
				</div>
			</div>
			<div class="campaign-popup-content small-campaign-popup-content">
				<asp:UpdatePanel ID="ConditionPopupUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<uc:MessageContainer ID="ConditionPopupValidationMessage" MessageType="Failure" HideMessagesControlId="ConditionPopupOkButton" runat="server" />
						<asp:PlaceHolder ID="ConditionControlPlaceHolder" runat="server"></asp:PlaceHolder>
					</ContentTemplate>
				</asp:UpdatePanel>
				<div class="campaign-popup-buttons">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ConditionPopupOkButton" runat="server" Text="<%$Resources:General, Button_Ok %>" SkinID="DefaultButton" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ConditionPopupCancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
				</div>
			</div>
		</asp:Panel>
	</div>

	<ajaxToolkit:ModalPopupExtender
		ID="ConditionPopup"
		runat="server"
		TargetControlID="MockedConditionPopupTargetButton"
		PopupControlID="ConditionPopupDiv"
		CancelControlID="ConditionPopupCloseButton"
		BackgroundCssClass="PopupBackground" />

	<div style="display: none;">
		<asp:Button ID="MockedConditionPopupTargetButton" Visible="true" runat="server" />
	</div>

	<asp:HiddenField ID="ConditionControlToInitializeHiddenField" runat="server" />
	<!-- Condition Popup End -->

	<!-- Action Popup Start -->
	<div id="ActionPopupDiv" runat="server" class="campaign-popup-container" style="z-index: 10010;
		display: none;">
		<div id="campaign-popup-header" class="campaign-popup-header">
			<div class="campaign-popup-header-left">
			</div>
			<div class="campaign-popup-header-center">
				<span>
					<%= Resources.Campaign.Literal_ConfigureAction%></span>
				<input type="button" id="ActionPopupCloseButton" runat="server" value="x" />
			</div>
			<div class="campaign-popup-header-right">
			</div>
		</div>
		<div class="campaign-popup-content-scrollable">
			<asp:UpdatePanel ID="ActionPopupUpdatePanel" UpdateMode="Conditional" runat="server">
				<ContentTemplate>
					<asp:PlaceHolder ID="ActionControlPlaceHolder" runat="server"></asp:PlaceHolder>
					<br class="clear" />
                    <div style="width:98%">
					<uc:MessageContainer ID="ActionPopupValidationMessage" MessageType="Warning" HideMessagesControlId="ActionPopupOkButton"
						runat="server" />
                    <div style="height:200px;overflow-y:scroll" class="message-container warning" id="ActionPopupAdditionalValidationMessageDiv" visible="false" runat="server">
                        <asp:Literal runat="server" ID="ActionPopupAdditionalValidationMessage"></asp:Literal>
                    </div>
                    </div>
				</ContentTemplate>
			</asp:UpdatePanel>
			<br />
			<div class="left" id="UploadExcelDiv" runat="server">
				<div class="left">
					<span><%= Resources.Review.Literal_UploadExcelFile %></span>
					<asp:FileUpload ID="FileUploadControl" runat="server" CssClass="file-upload" ContentEditable="false" />
					&nbsp;
				</div>
				<asp:RequiredFieldValidator ID="FileNameValidator" runat="server" ControlToValidate="FileUploadControl" ErrorMessage="" Text="* Excel file should be selected." ValidationGroup="FileUpload" />
				<uc:ImageLinkButton ID="ActionPopupUploadExcelButton" runat="server" Text="<%$ Resources:Interface,Button_Upload %>" SkinID="DefaultButton" ValidationGroup="FileUpload" />
			</div>

			<div class="campaign-popup-buttons">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ActionPopupOkButton" runat="server"
					Text="<%$Resources:General, Button_Ok %>" SkinID="DefaultButton" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ActionPopupCancelButton" runat="server"
					Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
			</div>
		</div>
	</div>
	
	<ajaxToolkit:ModalPopupExtender
		ID="ActionPopup" runat="server"
		TargetControlID="MockedActionPopupTargetButton"
		PopupControlID="ActionPopupDiv"
		CancelControlID="ActionPopupCloseButton"
		BackgroundCssClass="PopupBackground" />

	<div style="display: none;">
		<asp:Button ID="MockedActionPopupTargetButton" Visible="true" runat="server" />
	</div>

	<asp:HiddenField ID="ActionControlToInitializeHiddenField" runat="server" />
	<!-- Action Popup End -->

	<ajaxToolkit:ModalPopupExtender
		ID="ImageMediaItemsPopup"
		runat="server"
		TargetControlID="ImageMediaItemBrowseButton"
		PopupControlID="ImageMediaItemsDiv" 
		BackgroundCssClass="popup-background"
		CancelControlID="_inpClose"
		OnCancelScript="ResetDefaultMediaMessages();">
	</ajaxToolkit:ModalPopupExtender>

	<div id="ImageMediaItemsDiv" runat="server" class="product-popup-images-container" style="z-index: 10010; display: none;">
		<div id="product-popup-images-header">
			<div id="product-popup-images-header-left">
			</div>

			<div id="product-popup-images-header-center">
				<span>Add media item</span>
				<input type="button" id="_inpClose" class="_inpClose" value="x" />
			</div>

			<div id="product-popup-images-header-right">
			</div>
		</div>
		<br clear="all" />

		<div class="product-popup-images-body">
			<uc:MediaItemSelect id="ImageMediaItemSelectControl" runat="server"/>
		</div>
	</div>
	
	<ajaxToolkit:ModalPopupExtender
		ID="IconMediaItemsPopup"
		runat="server"
		TargetControlID="IconMediaItemBrowseButton"
		PopupControlID="IconMediaItemsDiv" 
		BackgroundCssClass="popup-background"
		CancelControlID="_inpClose"
		OnCancelScript="ResetDefaultMediaMessages();">
	</ajaxToolkit:ModalPopupExtender>

	<div id="IconMediaItemsDiv" runat="server" class="product-popup-images-container" style="z-index: 10010; display: none;">
		<div id="product-popup-images-header2">
			<div id="product-popup-images-header-left2">
			</div>

			<div id="product-popup-images-header-center2">
				<span>Add media item</span>
				<input type="button" id="_inpClose" class="_inpClose" value="x" />
			</div>

			<div id="product-popup-images-header-right2">
			</div>
		</div>
		<br clear="all" />

		<div class="product-popup-images-body">
			<uc:MediaItemSelect id="IconMediaItemSelectControl" runat="server"/>
		</div>
	</div>

</asp:Content>