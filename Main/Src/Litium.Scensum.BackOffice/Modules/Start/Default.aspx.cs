using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.UI.DataVisualization.Charting;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Statistics;
using RssToolkit.Rss;
using RssToolkit.Web.WebControls;

namespace Litium.Scensum.BackOffice.Modules.Start
{
	public partial class Default : LekmerPageController
	{
		private const int _bestsellersDisplayQuantity = 3;
		private const int _topSearchPhrasesDisplayQuantity = 5;
		private const int _zeroHitsPhrasesDisplayQuantity = 3;
		private const string _bestsellersViewModeKey = "BestsellersViewMode";
		private const string _searchViewModeKey = "TopSearchViewMode";
		private const string _performanceViewModeKey = "PerformanceViewMode";

		protected bool RssHasTitle { get; set; }
		protected bool RssHasLink { get; set; }
		protected bool RssHasPubDate { get; set; }
		protected bool RssHasDescription { get; set; }

		protected override void SetEventHandlers()
		{
			RssDataSource.Load += OnRssDataSourceLoad;
			BestsellersViewModeList.SelectedValueChanged += OnBestsellersViewModeChanged;
			SearchViewModeList.SelectedValueChanged += OnSearchViewModeChanged;
			PerformanceViewModeList.SelectedValueChanged += OnPerformanceViewModeChanged;
			OrderStatisticsChart.PreRender += OnChartPreRender;
			SaleStatisticsChart.PreRender += OnChartPreRender;
			VisitorStatisticsChart.PreRender += OnChartPreRender;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			InitializePanels();
		}

		protected override void PopulateForm()
		{
			var statusCommonNames = GetAvailableStatuses();
			InitializeStatisticsViewModes();
			PopulateBestsellers(statusCommonNames);
			PopulateTopSearchPhrases();
			PopulateSitePerformance(statusCommonNames);
		}

		protected void OnBestsellersViewModeChanged(object sender, ContextMenu3EventArgs e)
		{
			SetViewModeAndReload(_bestsellersViewModeKey, e.SelectedValue);
		}

		protected void OnSearchViewModeChanged(object sender, ContextMenu3EventArgs e)
		{
			SetViewModeAndReload(_searchViewModeKey, e.SelectedValue);
		}

		protected void OnPerformanceViewModeChanged(object sender, ContextMenu3EventArgs e)
		{
			SetViewModeAndReload(_performanceViewModeKey, e.SelectedValue);
		}

		protected virtual void OnChartPreRender(object sender, EventArgs e)
		{
			ConfigureChart(sender as Chart);
		}

		protected void OnRssDataSourceLoad(object sender, EventArgs e)
		{
			CheckRssFeed((RssDataSource)sender);
		}

		protected virtual void InitializeStatisticsViewModes()
		{
			var modes = Enum.GetNames(typeof(ViewMode));
			var fullViewModeSource = new Collection<ContextMenuDataSourceItem>(modes.Select(
						item => new ContextMenuDataSourceItem { Text = item, Value = item }).ToList());
			var cuttedViewModeSource = new Collection<ContextMenuDataSourceItem>(modes.Where(item => item != ViewMode.Hour.ToString()).Select(
						item => new ContextMenuDataSourceItem { Text = item, Value = item }).ToList());

			BestsellersViewModeList.DataSource = fullViewModeSource;
			BestsellersViewModeList.DataBind();
			BestsellersViewModeList.SelectedValue = GetViewMode(_bestsellersViewModeKey).ToString();

			SearchViewModeList.DataSource = cuttedViewModeSource;
			SearchViewModeList.DataBind();
			SearchViewModeList.SelectedValue = GetViewMode(_searchViewModeKey).ToString();

			PerformanceViewModeList.DataSource = fullViewModeSource;
			PerformanceViewModeList.DataBind();
			PerformanceViewModeList.SelectedValue = GetViewMode(_performanceViewModeKey).ToString();
		}

		protected virtual void PopulateBestsellers(string[] statusCommonNames)
		{
			var viewMode = GetViewMode(_bestsellersViewModeKey);
			var dateTimeTo = DateTime.Now;
			var dateTimeFrom = GetStatisticsStartDateTime(dateTimeTo, viewMode);
			var service = IoC.Resolve<ITopSellerSecureService>();
			var data = service.GetAll(ChannelHelper.CurrentChannel.Id, dateTimeFrom, dateTimeTo, _bestsellersDisplayQuantity, statusCommonNames);
			
			BestsellersGrid.DataSource = data;
			BestsellersGrid.DataBind();
		}

		protected virtual void PopulateTopSearchPhrases()
		{
			var viewMode = GetViewMode(_searchViewModeKey);
			var dateTimeTo = DateTime.Now;
			var dateTimeFrom = GetStatisticsStartDateTime(dateTimeTo, viewMode);
			var service = IoC.Resolve<IDailySearchSecureService>();
			var data = service.GetAll(ChannelHelper.CurrentChannel.Id,
				dateTimeFrom, dateTimeTo, _topSearchPhrasesDisplayQuantity,
				_zeroHitsPhrasesDisplayQuantity);
			
			if (data.Count == 0)
			{
				SearchPhrasesGrid.DataSource = null;
				SearchPhrasesGrid.DataBind();
				return;
			}
			var searchedPhrases = data.Where(item => item.HitCount > 0);
			if (searchedPhrases.Count() > 0)
			{
				SearchPhrasesGrid.DataSource = searchedPhrases;
				SearchPhrasesGrid.DataBind();
			}
			var zeroHitsPhrases = data.Where(item => item.HitCount == 0);
			if (zeroHitsPhrases.Count() > 0)
			{
				ZeroHitsPhrasesGrid.DataSource = zeroHitsPhrases;
				ZeroHitsPhrasesGrid.DataBind();
			}
		}

		protected virtual void PopulateSitePerformance(string[] statusCommonNames)
		{
			PopulateSitePerformanceGrid(statusCommonNames);
			PopulateSitePerformanceCharts(statusCommonNames);
		}

		protected virtual void PopulateSitePerformanceGrid(string[] statusCommonNames)
		{
			var viewMode = GetViewMode(_performanceViewModeKey);
			var dateTimeTo = DateTime.Now;
			var dateTimeFrom = GetStatisticsStartDateTime(dateTimeTo, viewMode);
			var channelId = ChannelHelper.CurrentChannel.Id;
			var service = IoC.Resolve<IGroupedDataSecureService>();
			var visitors = service.GetVisitorSummary(channelId, dateTimeFrom, dateTimeTo, false);
			var visitorsAlternate = service.GetVisitorSummary(channelId, dateTimeFrom, dateTimeTo, true);
			var orders = service.GetOrderSummary(channelId, dateTimeFrom, dateTimeTo, false, statusCommonNames);
			var ordersAlternate = service.GetOrderSummary(channelId, dateTimeFrom, dateTimeTo, true, statusCommonNames);
			var sales = service.GetSaleSummary(channelId, dateTimeFrom, dateTimeTo, statusCommonNames);

			PerformanceGrid.DataSource = new Dictionary<string, string>
			{
				{string.Format(CultureInfo.CurrentCulture, Resources.Start.Literal_TotalOrderValue, ChannelHelper.CurrentChannel.Currency.Iso), sales.ToString("F", CultureInfo.CurrentCulture)},
				{Resources.Start.Literal_TotalNumberOfOrders, (orders + ordersAlternate).ToString(CultureInfo.CurrentCulture)},
				{string.Format(CultureInfo.CurrentCulture, Resources.Start.Literal_AverageOrderValue, ChannelHelper.CurrentChannel.Currency.Iso), ((orders + ordersAlternate) > 0 ? sales/(orders + ordersAlternate) : 0).ToString("F", CultureInfo.CurrentCulture)},
				{Resources.Start.Literal_AccumulatedVisitors, (visitors + visitorsAlternate).ToString(CultureInfo.CurrentCulture)},
				{Resources.Start.Literal_ConversionRate, string.Format(CultureInfo.CurrentCulture, "{0} / {1}", 
					(visitors > 0 ? (double)orders/visitors : 0).ToString("P", CultureInfo.CurrentCulture), 
					(visitorsAlternate > 0 ? (double)ordersAlternate/visitorsAlternate : 0).ToString("P", CultureInfo.CurrentCulture))}
			};
			PerformanceGrid.DataBind();
		}

		protected virtual void PopulateSitePerformanceCharts(string[] statusCommonNames)
		{
			var viewMode = GetViewMode(_performanceViewModeKey);
			var chartSpecificViewMode = viewMode == ViewMode.Hour ? ViewMode.Day : viewMode;
			var dateTimeTo = DateTime.Now;
			var dateTimeFrom = GetStatisticsStartDateTime(dateTimeTo, chartSpecificViewMode);
			var channelId = ChannelHelper.CurrentChannel.Id;
			var service = IoC.Resolve<IGroupedDataSecureService>();

			OrderStatisticsChart.DataSource = service.GetOrderStatistics(channelId, dateTimeFrom, dateTimeTo, statusCommonNames);
			OrderStatisticsChart.DataBind();
			SaleStatisticsChart.DataSource = service.GetSaleStatistics(channelId, dateTimeFrom, dateTimeTo, statusCommonNames);
			SaleStatisticsChart.DataBind();
			VisitorStatisticsChart.DataSource = service.GetVisitorStatistics(channelId, dateTimeFrom, dateTimeTo);
			VisitorStatisticsChart.DataBind();
		}
		private static string[] GetAvailableStatuses()
		{
			var availableStatuses = OrderSetting.Instance.OrderStatusesIncludedInStatistics;
			var statuses = availableStatuses.Split(',');
			return statuses;
		}

		protected virtual DateTime GetStatisticsStartDateTime(DateTime endDateTime, ViewMode viewMode)
		{
			switch (viewMode)
			{
				case ViewMode.Hour:
					return endDateTime.AddHours(-1);
				case ViewMode.Day:
					return endDateTime.AddDays(-1);
				case ViewMode.Week:
					return endDateTime.AddDays(-7);
				case ViewMode.Month:
					return endDateTime.AddMonths(-1);
				case ViewMode.Year:
					return endDateTime.AddYears(-1);
				default:
					return endDateTime.AddDays(-1);
			}
		}

		protected virtual void ConfigureChart(Chart chart)
		{
			var series = chart.Series[0];
			series.XValueType = ChartValueType.DateTime;
			var axisX = chart.ChartAreas[0].AxisX;
			var axisY = chart.ChartAreas[0].AxisY;
			
			var yMax = 0;
			var decData = chart.DataSource as Collection<IGroupedData<decimal>>;
			if (decData != null)
			{
				yMax = (int)Math.Ceiling(decData.Max(item => item.Value));
			}
			else
			{
				var intData = chart.DataSource as Collection<IGroupedData<int>>;
				if (intData != null)
				{
					yMax = intData.Max(item => item.Value);
				}
			}
			axisY.Interval = axisY.MajorGrid.Interval = (int)Math.Truncate(yMax / 4.0);
			if (yMax < 1000)
			{
				axisY.LabelStyle.Format = "";
			}
			else if (yMax < 1000000)
			{
				axisY.LabelStyle.Format = "0,.0K";
			}
			else if (yMax < 1000000000)
			{
				axisY.LabelStyle.Format = @"0,,.0M";
			}
			else
			{
				axisY.LabelStyle.Format = @"0,,,.0G";
			}

			var position = chart.ChartAreas[0].InnerPlotPosition;
			if (yMax < 10)
			{
				position.X = 2;
				position.Width = 93;
			}
			else if (yMax < 100)
			{
				position.X = 3;
				position.Width = 93;
			}
			else if (yMax < 1000)
			{
				position.X = 4;
				position.Width = 92;
			}
			else
			{
				position.X = 7;
				position.Width = 90;
			}
			
			var viewMode = GetViewMode(_performanceViewModeKey);
			switch (viewMode)
			{
				case ViewMode.Hour:
				case ViewMode.Day:
					axisX.IntervalType = axisX.LabelStyle.IntervalType = DateTimeIntervalType.Hours;
					axisX.Interval = axisX.MajorGrid.Interval = axisX.LabelStyle.Interval = 2;
					axisX.LabelStyle.Format = "HH:mm";
					break;
				case ViewMode.Week:
					axisX.IntervalType = axisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;
					axisX.Interval = axisX.MajorGrid.Interval = axisX.LabelStyle.Interval = 1;
					axisX.LabelStyle.Format = "ddd";
					break;
				case ViewMode.Month:
					axisX.IntervalType = axisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;
					axisX.Interval = axisX.MajorGrid.Interval = axisX.LabelStyle.Interval = 3;
					axisX.LabelStyle.Format = "dd" + CultureInfo.CurrentCulture.DateTimeFormat.DateSeparator + "MM";
					break;
				case ViewMode.Year:
					axisX.IntervalType = axisX.LabelStyle.IntervalType = DateTimeIntervalType.Months;
					axisX.Interval = axisX.MajorGrid.Interval = axisX.LabelStyle.Interval = 1;
					axisX.LabelStyle.Format = "MMM";
					break;
				default:
					axisX.IntervalType = axisX.LabelStyle.IntervalType = DateTimeIntervalType.Hours;
					axisX.Interval = axisX.MajorGrid.Interval = axisX.LabelStyle.Interval = 2;
					axisX.LabelStyle.Format = "HH:mm";
					break;
			}
		}

		protected virtual void InitializePanels()
		{
			BestsellersPanel.Text = Resources.Start.Literal_Bestsellers;
			SearchPanel.Text = Resources.Start.Literal_TopSearchPhrases;
			PerformancePanel.Text = Resources.Start.Literal_SitePerformance;
			InformationPanel.Text = Resources.Start.Literal_Information;
		}

		protected virtual string GetImageUrl(object productImage)
		{
			var image = productImage as IImage;
			return image == null
				? string.Empty
				: ResolveUrl(PathHelper.Media.GetImageThumbnailLoaderUrl(image.Id, image.FormatExtension));
		}

		protected virtual string GetEditUrl(object productId)
		{
			var id = System.Convert.ToInt32(productId, CultureInfo.CurrentCulture);
			return PathHelper.Assortment.Product.GetEditUrl(id);
		}

		protected virtual string GetPrice(object productPrice)
		{
			var price = System.Convert.ToDecimal(productPrice, CultureInfo.CurrentCulture);
			var formatter = IoC.Resolve<IFormatter>();
			return formatter.FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, price);
		}

		protected virtual string GetErp(object productErp)
		{
			return string.Format(CultureInfo.CurrentCulture, "{0}: {1}", Resources.General.Literal_ErpId, productErp);
		}

		protected virtual string GetRssDate(object pubDate)
		{
			DateTime date;
			return DateTime.TryParse(pubDate.ToString(), out date) ? date.ToString("g", CultureInfo.CurrentCulture) : string.Empty;
		}

		//Used to prevent data binding errors in cases when RSS feed that doesn't meet standard requirements
		protected virtual void CheckRssFeed(RssDataSource rssDataSource)
		{
			RssDocument rss;
			if (rssDataSource == null || !TryLoadRss(rssDataSource, out rss) || rss.Channel == null || rss.Channel.Items == null)
			{
				NoRssFeedMessage.Visible = true;
				RssList.DataSourceID = string.Empty;
				return;
			}

			var rssItems = rss.Channel.Items;
			if (rssItems.Count == 0) return;

			var rssItem = rssItems[0];
			RssHasTitle = rssItem.Title != null;
			RssHasLink = rssItem.Link != null;
			RssHasPubDate = rssItem.PubDate != null;
			RssHasDescription = rssItem.Description != null;
		}

		protected virtual bool TryLoadRss(RssDataSource rssDataSource, out RssDocument rss)
		{
			try
			{
				rss = rssDataSource.Rss;
				return rss != null ? true : false;
			}
			catch (WebException)
			{
				rss = null;
				return false;
			}
		}

		protected virtual ViewMode GetViewMode(string key)
		{
			var mode = Session[key];
			return mode == null ? ViewMode.Day : (ViewMode)mode;
		}

		protected virtual void SetViewModeAndReload(string key, string mode)
		{
			Session[key] = (ViewMode)Enum.Parse(typeof(ViewMode), mode);
			Response.Redirect(PathHelper.GetStartUrl());
		}
	}

	public enum ViewMode
	{
		Hour = 0,
		Day = 1,
		Week = 2,
		Month = 3,
		Year = 4
	}
}
