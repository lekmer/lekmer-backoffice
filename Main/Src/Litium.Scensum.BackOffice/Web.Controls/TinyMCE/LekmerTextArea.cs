﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Moxiecode.TinyMCE;

namespace Litium.Scensum.BackOffice.WebControls
{
	public class LekmerTextArea : WebControl, IPostBackDataHandler
	{
		private static readonly object _textChangedEvent = new object();
		private int _rows = 10;
		private int _cols = 70;
		private readonly NameValueCollection _settings;
		private bool _isRendered;
		private readonly bool _gzipEnabled;
		private bool _merged;
		private string _installPath;
		private readonly string _mode;

		public string Value
		{
			get
			{
				return (string)ViewState["value"];
			}
			set
			{
				ViewState["value"] = value;
			}
		}

		public int Rows
		{
			get
			{
				return _rows;
			}
			set
			{
				_rows = value;
			}
		}

		public int Cols
		{
			get
			{
				return _cols;
			}
			set
			{
				_cols = value;
			}
		}

		public string InstallPath
		{
			get
			{
				if (_installPath == null)
				{
					_installPath = Settings["InstallPath"];
				}
				return _installPath;
			}
			set
			{
				_installPath = value;
			}
		}

		public bool IsRendered
		{
			get
			{
				return _isRendered;
			}
		}

		public NameValueCollection Settings
		{
			get
			{
				if (!_merged)
				{
					foreach (string index in Attributes.Keys)
					{
						_settings[index] = Attributes[index];
					}

					if (_settings["plugins"] != null && _settings["spellchecker_rpc_url"] == null)
					{
						if (Array.IndexOf(_settings["plugins"].Split(new[] { ',' }), "spellchecker") != -1)
						{
							_settings["spellchecker_rpc_url"] = "TinyMCE.ashx?module=SpellChecker";
						}
					}

					_settings["elements"] = ClientID;
					_settings["mode"] = "exact";
					_merged = true;
				}
				return _settings;
			}
		}

		private string ScriptUri
		{
			get
			{
				string str1 = "";
				if (InstallPath == null)
				{
					throw new Exception("Required installPath setting is missing, add it to your web.config. You can also add it directly to your tinymce:TextArea element using InstallPath but the web.config method is recommended since it allows you to switch over to gzip compression.");
				}

				if (_mode != null)
				{
					str1 = "_" + _mode;
				}

				string path = InstallPath + "/tiny_mce" + str1 + ".js";

				if (!File.Exists(Context.Server.MapPath(path)))
				{
					throw new Exception("Could not locate TinyMCE by URI:" + path + ", Physical path:" + Context.Server.MapPath(path) + ". Make sure that you configured the installPath to a valid location in your web.config. This path should be an relative or site absolute URI to where TinyMCE is located.");
				}

				if (_gzipEnabled)
				{
					var arrayList1 = new ArrayList();
					var arrayList2 = new ArrayList();
					var arrayList3 = new ArrayList();

					foreach (LekmerTextArea textArea in FindAllTinyMceAreas(Page, new ArrayList()))
					{
						if (textArea.Settings["theme"] == null)
						{
							textArea.Settings["theme"] = "simple";
						}

						string str2 = textArea.Settings["theme"];
						var chArray1 = new[] { ',' };

						foreach (string str3 in str2.Split(chArray1))
						{
							string str4 = str3.Trim();
							if (arrayList1.IndexOf(str4) == -1)
							{
								arrayList1.Add(str4);
							}
						}

						if (textArea.Settings["plugins"] != null)
						{
							string str3 = textArea.Settings["plugins"];
							var chArray2 = new[] { ',' };

							foreach (string str4 in str3.Split(chArray2))
							{
								string str5 = str4.Trim();
								if (arrayList2.IndexOf(str5) == -1)
								{
									arrayList2.Add(str5);
								}
							}
						}

						if (textArea.Settings["language"] == null)
						{
							textArea.Settings["language"] = "en";
						}

						if (arrayList3.IndexOf(textArea.Settings["language"]) == -1)
						{
							arrayList3.Add(textArea.Settings["language"]);
						}

						textArea.Settings["theme"] = "-" + textArea.Settings["theme"];

						if (textArea.Settings["plugins"] != null)
						{
							textArea.Settings["plugins"] = "-" + string.Join(",-", textArea.Settings["plugins"].Split(new[] { ',' }));
						}
					}

					path = "TinyMCE.ashx?module=GzipModule";

					if (arrayList1.Count > 0)
					{
						path = path + "&themes=" + string.Join(",", (string[])arrayList1.ToArray(typeof(string)));
					}

					if (arrayList2.Count > 0)
					{
						path = path + "&plugins=" + string.Join(",", (string[])arrayList2.ToArray(typeof(string)));
					}

					if (arrayList3.Count > 0)
					{
						path = path + "&languages=" + string.Join(",", (string[])arrayList3.ToArray(typeof(string)));
					}
				}
				return path;
			}
		}

		public event EventHandler TextChanged
		{
			add
			{
				Events.AddHandler(_textChangedEvent, value);
			}
			remove
			{
				Events.RemoveHandler(_textChangedEvent, value);
			}
		}

		static LekmerTextArea()
		{
		}

		public LekmerTextArea()
		{
			var configSection = (ConfigSection)HttpContext.Current.GetSection("TinyMCE");

			_settings = new NameValueCollection();

			if (configSection == null)
			{
				return;
			}

			_installPath = configSection.InstallPath;
			_mode = configSection.Mode;
			_gzipEnabled = configSection.GzipEnabled;

			foreach (string index in configSection.GlobalSettings.Keys)
			{
				_settings[index] = configSection.GlobalSettings[index];
			}
		}

		protected override void OnLoad(EventArgs args)
		{
			_settings["elements"] = ClientID;
		}

		public bool HasRenderedTextArea(Control control)
		{
			if (control is LekmerTextArea && ((LekmerTextArea)control).IsRendered)
			{
				return true;
			}
			foreach (Control control1 in control.Controls)
			{
				if (HasRenderedTextArea(control1))
				{
					return true;
				}
			}
			return false;
		}

		protected override void Render(HtmlTextWriter outWriter)
		{
			bool flag = true;
			if (!HasRenderedTextArea(Page))
			{
				outWriter.WriteBeginTag("script");
				outWriter.WriteAttribute("type", "text/javascript");
				outWriter.WriteAttribute("src", ScriptUri);
				outWriter.Write('>');
				outWriter.WriteEndTag("script");
				_isRendered = true;
			}

			outWriter.WriteBeginTag("script");
			outWriter.WriteAttribute("type", "text/javascript");
			outWriter.Write('>');
			outWriter.Write("tinyMCE.init({\n");

			foreach (string index in Settings.Keys)
			{
				string str = Settings[index];

				if (!flag)
				{
					outWriter.Write(",\n");
				}
				else
				{
					flag = false;
				}

				if (str == "true" || str == "false")
				{
					outWriter.Write(index + ":" + Settings[index]);
				}
				else
				{
					outWriter.Write(index + ":'" + Settings[index] + "'");
				}
			}

			outWriter.Write("\n});\n");
			outWriter.WriteEndTag("script");
			outWriter.AddAttribute("id", ClientID);
			outWriter.AddAttribute("name", UniqueID);

			if (Rows > 0)
			{
				outWriter.AddAttribute("rows", string.Concat(Rows));
			}

			if (Cols > 0)
			{
				outWriter.AddAttribute("cols", string.Concat(Cols));
			}

			if (CssClass.Length > 0)
			{
				outWriter.AddAttribute("class", CssClass);
			}

			if (Width.Value > 0.0)
			{
				outWriter.AddStyleAttribute("width", Width.ToString());
			}

			if (Height.Value > 0.0)
			{
				outWriter.AddStyleAttribute("height", Height.ToString());
			}

			outWriter.RenderBeginTag("textarea");
			outWriter.Write(Context.Server.HtmlEncode(Value));
			// Fix tag closing
			//outWriter.WriteEndTag("textarea");
			outWriter.RenderEndTag();
		}

		protected virtual void OnTextChanged(EventArgs e)
		{
			var eventHandler = (EventHandler)Events[_textChangedEvent];

			if (eventHandler == null)
			{
				return;
			}

			eventHandler(this, e);
		}

		public ArrayList FindAllTinyMceAreas(Control control, ArrayList items)
		{
			if (control is LekmerTextArea)
			{
				items.Add(control);
			}

			foreach (Control control1 in control.Controls)
			{
				FindAllTinyMceAreas(control1, items);
			}

			return items;
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			string str = postCollection[postDataKey];
			if (str == Value)
			{
				return false;
			}
			Value = str;
			return true;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			OnTextChanged(EventArgs.Empty);
		}
	}
}
