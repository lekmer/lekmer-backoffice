﻿using System;

namespace Litium.Lekmer.Core
{
	/// <summary>
	/// Handles formatting of different values.
	/// </summary>
	public static class PriceCalc
	{
		public static decimal PriceExcludingVat1(decimal priceIncludingVat, decimal vatPercentage)
		{
			decimal priceExcludingVat = priceIncludingVat / (1 + vatPercentage);

			return RoundPrice(priceExcludingVat);
		}

		public static decimal PriceExcludingVat2(decimal priceIncludingVat, decimal vatAmount)
		{
			decimal priceExcludingVat = priceIncludingVat - vatAmount;

			return RoundPrice(priceExcludingVat);
		}

		public static decimal VatPercentage1(decimal priceIncludingVat, decimal vatAmount)
		{
			decimal priceExcludingVat = priceIncludingVat - vatAmount;

			if (priceExcludingVat == 0)
			{
				return 0.25m; // default vat %
			}

			decimal vatPercentage = vatAmount / priceExcludingVat;

			return RoundPrice(vatPercentage);
		}

		public static decimal VatPercentage2(decimal priceIncludingVat, decimal priceExcludingVat)
		{
			if (priceExcludingVat == 0)
			{
				return 0.25m; // default vat %
			}

			decimal vatPercentage = (priceIncludingVat - priceExcludingVat) / priceExcludingVat;

			return RoundPrice(vatPercentage);
		}

		public static decimal VatPercentage100(decimal priceIncludingVat, decimal priceExcludingVat)
		{
			if (priceExcludingVat == 0)
			{
				return 25m; // default vat %
			}

			decimal vatPercentage = (priceIncludingVat - priceExcludingVat) / priceExcludingVat * 100m;

			return RoundPrice(vatPercentage);
		}

		public static decimal RoundPrice(decimal price)
		{
			return Math.Round(price, 2, MidpointRounding.AwayFromZero);
		}
	}
}
