﻿using System.Web;

namespace Litium.Lekmer.Core
{
	public static class LekmerEncoder
	{
		/// <summary>
		/// Encodes parentheses.
		/// </summary>
		/// <param name="input">The string to encode.</param>
		/// <returns>The encoded string.</returns>
		public static string UrlEncodeWithParentheses(string input)
		{
			return HttpUtility.UrlEncode(input ?? string.Empty)
				.Replace("(", @"%28")
				.Replace(")", @"%29");
		}
	}
}