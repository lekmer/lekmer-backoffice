﻿using System;
using System.Globalization;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	/// <summary>
	/// Handles formatting of different values.
	/// </summary>
	public class LekmerFormatter : Formatter, ILekmerFormatter
	{
		/// <summary>
		/// Special format on price for checkout.
		/// </summary>
		/// <param name="channel">The channel to format the price for.</param>
		/// <param name="price">The price to format.</param>
		/// <returns>The formatted price. Shows two decimals if price contains decimals otherwise no decimals</returns>
		public virtual string FormatPriceTwoOrLessDecimals(IChannel channel, decimal price)
		{
			if (channel == null)
			{
				throw new ArgumentNullException("channel");
			}

			CultureInfo cultureInfo = channel.CultureInfo;
			var numberFormatInfo = (NumberFormatInfo)cultureInfo.NumberFormat.Clone();

			decimal check = Math.Round(price * 100m) % 100m;
			numberFormatInfo.NumberDecimalDigits = check != 0 ? 2 : 0;

			string result = price.ToString("N", numberFormatInfo);
			return string.Format(cultureInfo, channel.Currency.PriceFormat, result);
		}

		/// <summary>
		/// Special format on price with minimum decimals.
		/// </summary>
		/// <param name="channel">The channel to format the price for.</param>
		/// <param name="price">The price to format.</param>
		/// <returns>The formatted price. Shows channel specified decimals if price contains decimals otherwise no decimals</returns>
		public virtual string FormatPriceChannelOrLessDecimals(IChannel channel, decimal price)
		{
			if (channel == null)
			{
				throw new ArgumentNullException("channel");
			}

			CultureInfo cultureInfo = channel.CultureInfo;
			var numberFormatInfo = (NumberFormatInfo)cultureInfo.NumberFormat.Clone();

			decimal check = Math.Round(price * 100m) % 100m;
			numberFormatInfo.NumberDecimalDigits = check != 0 ? channel.Currency.NumberOfDecimals : 0;

			string result = price.ToString("N", numberFormatInfo);
			return string.Format(cultureInfo, channel.Currency.PriceFormat, result);
		}
	}
}
