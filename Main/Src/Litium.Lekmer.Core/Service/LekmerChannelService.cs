using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Repository;

namespace Litium.Lekmer.Core
{
	public class LekmerChannelService : ChannelService, ILekmerChannelService
	{
		public LekmerChannelService(ChannelRepository channelRepository) : base(channelRepository)
		{
		}

		public virtual Collection<IChannel> GetAll()
		{
			return Repository.GetAll();
		}
	}
}