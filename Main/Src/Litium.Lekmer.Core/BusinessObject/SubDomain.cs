﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core
{
	[Serializable]
	public class SubDomain : BusinessObjectBase, ISubDomain
	{
		private int _id;
		private string _commonName;
		private string _domainUrl;
		private int _urlType;
		private int _contentType;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string DomainUrl
		{
			get { return _domainUrl; }
			set
			{
				CheckChanged(_domainUrl, value);
				_domainUrl = value;
			}
		}

		public int UrlType
		{
			get { return _urlType; }
			set
			{
				CheckChanged(_urlType, value);
				_urlType = value;
			}
		}

		public int ContentType
		{
			get { return _contentType; }
			set
			{
				CheckChanged(_contentType, value);
				_contentType = value;
			}
		}
	}
}
