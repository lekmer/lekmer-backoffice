﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core
{
	[Serializable]
	public class ChannelFormatInfo : BusinessObjectBase, IChannelFormatInfo
	{
		private int _channelId;
		private string _timeFormat;
		private string _weekDayFormat;
		private string _dayFormat;
		private string _dateTimeFormat;
		private int _timeZoneDiff;

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public string TimeFormat
		{
			get { return _timeFormat; }
			set
			{
				CheckChanged(_timeFormat, value);
				_timeFormat = value;
			}
		}

		public string WeekDayFormat
		{
			get { return _weekDayFormat; }
			set
			{
				CheckChanged(_weekDayFormat, value);
				_weekDayFormat = value;
			}
		}

		public string DayFormat
		{
			get { return _dayFormat; }
			set
			{
				CheckChanged(_dayFormat, value);
				_dayFormat = value;
			}
		}

		public string DateTimeFormat
		{
			get { return _dateTimeFormat; }
			set
			{
				CheckChanged(_dateTimeFormat, value);
				_dateTimeFormat = value;
			}
		}

		public int TimeZoneDiff
		{
			get { return _timeZoneDiff; }
			set
			{
				CheckChanged(_timeZoneDiff, value);
				_timeZoneDiff = value;
			}
		}
	}
}
