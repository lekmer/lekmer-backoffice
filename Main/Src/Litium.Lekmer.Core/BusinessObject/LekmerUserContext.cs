using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public class LekmerUserContext : UserContext, ILekmerUserContext
	{
		public IVoucherCheckResult Voucher { get; set; }
	}
}