﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Cache;
using Litium.Scensum.Core.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core
{
	public class LekmerChannelSecureService : ChannelSecureService, ILekmerChannelSecureService
	{
		public LekmerChannelSecureService(IAccessValidator accessValidator, ChannelRepository repository)
			: base(accessValidator, repository)
		{
		}

		/// <summary>
		/// Create new object
		/// </summary>
		public override IChannel Create()
		{
			IChannel channel = base.Create();

			var lekmerChannel = channel as ILekmerChannel;
			if (lekmerChannel != null)
			{
				lekmerChannel.FormatInfo = IoC.Resolve<IChannelFormatInfo>();
				lekmerChannel.FormatInfo.SetNew();
			}

			return channel;
		}

		public virtual Collection<IChannel> GetAll(string channelCommonNamesToIgnore)
		{
			Collection<IChannel> channels = Repository.GetAll();

			if (channelCommonNamesToIgnore.IsEmpty())
			{
				return channels;
			}

			List<string> commonNamesToIgnore = channelCommonNamesToIgnore.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

			var channelsFiltered = new Collection<IChannel>();

			foreach (IChannel channel in channels)
			{
				if (commonNamesToIgnore.Contains(channel.CommonName))
				{
					continue;
				}

				channelsFiltered.Add(channel);
			}

			return channelsFiltered;
		}

		/// <summary>
		/// Saves a Channel to Db, if user has Channel_Write privilege.
		/// </summary>
		/// <returns>int channelId</returns>
		public override int Save(ISystemUserFull adminUserFull, IChannel channel)
		{
			int channelId = base.Save(adminUserFull, channel);

			if (channelId > 0)
			{
				var lekmerChannel = channel as ILekmerChannel;
				if (lekmerChannel != null)
				{
					IChannelFormatInfo formatInfo = lekmerChannel.FormatInfo;
					if (formatInfo != null)
					{
						if (formatInfo.IsEdited || formatInfo.IsNew)
						{
							formatInfo.ChannelId = channelId;

							var lekmerChannelRepository = Repository as LekmerChannelRepository;
							if (lekmerChannelRepository != null)
							{
								lekmerChannelRepository.SaveFormatInfo(lekmerChannel.FormatInfo);

								// Cache is removed also in base method, but we need to remove it again !!!
								ChannelCache.Instance.Remove(new ChannelKey(channel.ApplicationName));
							}
						}
					}
				}
			}

			return channelId;
		}
	}
}
