﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core.Mapper
{
	public class SubDomainDataMapper : DataMapperBase<ISubDomain>
	{
		private int _subDomainIdOrdinal;
		private int _commonNameOrdinal;
		private int _domainUrlOrdinal;
		private int _urlTypeOrdinal;
		private int _contentTypeOrdinal;
		private int _channelIdOrdinal;

		public SubDomainDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_subDomainIdOrdinal = OrdinalOf("SubDomain.SubDomainId");
			_commonNameOrdinal = OrdinalOf("SubDomain.CommonName");
			_domainUrlOrdinal = OrdinalOf("SubDomain.DomainUrl");
			_urlTypeOrdinal = OrdinalOf("SubDomain.UrlTypeId");
			_contentTypeOrdinal = OrdinalOf("SubDomain.ContentTypeId");
		}

		protected override ISubDomain Create()
		{
			var subDomain = IoC.Resolve<ISubDomain>();

			subDomain.Id = MapValue<int>(_subDomainIdOrdinal);
			subDomain.CommonName = MapValue<string>(_commonNameOrdinal);
			subDomain.DomainUrl = MapValue<string>(_domainUrlOrdinal);
			subDomain.UrlType = MapValue<int>(_urlTypeOrdinal);
			subDomain.ContentType = MapValue<int>(_contentTypeOrdinal);

			subDomain.Status = BusinessObjectStatus.Untouched;
			return subDomain;
		}
	}
}
