using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class TagGroupService : ITagGroupService
	{
		protected TagGroupRepository Repository { get; private set; }
		protected ITagService TagService { get; private set; }

		public TagGroupService(TagGroupRepository repository, ITagService tagService)
		{
			Repository = repository;
			TagService = tagService;
		}


		public virtual Collection<ITagGroup> GetAll(IUserContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			return TagGroupCollectionCache.Instance.TryGetItem(
				new TagGroupKey(context.Channel.Id),
				() => GetAllCore(context));
		}

		public virtual Dictionary<string, string> InfoPointTagGroupGetAll()
		{
			return Repository.InfoPointTagGroupGetAll();
		}

		public virtual Collection<ITagGroup> GetAllWithoutTags(IUserContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			return TagGroupCollectionCache.Instance.TryGetItem(
				new TagGroupWithoutTagsKey(context.Channel.Id),
				() => Repository.GetAll());
		}

		public virtual Collection<ITagGroup> GetAllByProduct(IUserContext context, int productId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return ProductTagGroupCollectionCache.Instance.TryGetItem(
				new ProductTagGroupCollectionKey(context.Channel.Id, productId),
				() => GetAllByProductCore(context, productId));
		}


		public virtual ITagGroup Clone(ITagGroup sourceTagGroup)
		{
			var tagGroup = IoC.Resolve<ITagGroup>();

			tagGroup.Id = sourceTagGroup.Id;
			tagGroup.Title = sourceTagGroup.Title;
			tagGroup.CommonName = sourceTagGroup.CommonName;

			return tagGroup;
		}

		public virtual Collection<ITagGroup> Clone(Collection<ITagGroup> sourceTagGroups)
		{
			var tagGroups = new Collection<ITagGroup>();

			foreach (ITagGroup sourceTagGroup in sourceTagGroups)
			{
				tagGroups.Add(Clone(sourceTagGroup));
			}

			return tagGroups;
		}


		protected virtual Collection<ITagGroup> GetAllCore(IUserContext context)
		{
			Collection<ITagGroup> tagGroups = GetAllWithoutTags(context);
			tagGroups = Clone(tagGroups); //!!!

			Collection<ITag> tags = TagService.GetAll(context);
			Dictionary<int, Collection<ITag>> tagsDictionary = tags.GroupBy(t => t.TagGroupId).ToDictionary(t => t.Key, t => new Collection<ITag>(t.ToArray()));

			foreach (ITagGroup tagGroup in tagGroups)
			{
				tagGroup.Tags = tagsDictionary.ContainsKey(tagGroup.Id) ? tagsDictionary[tagGroup.Id] : new Collection<ITag>();
			}

			return tagGroups;
		}

		protected virtual Collection<ITagGroup> GetAllByProductCore(IUserContext context, int productId)
		{
			Collection<ITagGroup> tagGroups = GetAllWithoutTags(context);
			tagGroups = Clone(tagGroups); //!!!

			Collection<ITag> tags = TagService.GetAllByProduct(context, productId);
			Dictionary<int, Collection<ITag>> tagsDictionary = tags.GroupBy(t => t.TagGroupId).ToDictionary(t => t.Key, t => new Collection<ITag>(t.ToArray()));

			foreach (ITagGroup tagGroup in tagGroups)
			{
				tagGroup.Tags = tagsDictionary.ContainsKey(tagGroup.Id) ? tagsDictionary[tagGroup.Id] : new Collection<ITag>();
			}

			return tagGroups;
		}
	}
}