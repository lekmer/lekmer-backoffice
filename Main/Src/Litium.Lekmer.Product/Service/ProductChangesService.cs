using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class ProductChangesService : IProductChangesService
	{
		protected ProductChangesRepository Repository { get; private set; }

		public ProductChangesService(ProductChangesRepository repository)
		{
			Repository = repository;
		}

		public virtual IProductChangeEvent Create(int productId, string reference)
		{
			var productChangeEvent = IoC.Resolve<IProductChangeEvent>();

			productChangeEvent.ProductId = productId;
			productChangeEvent.EventStatusId = (int)ProductChangeEventStatus.InQueue;
			productChangeEvent.CdonExportEventStatusId = (int)ProductChangeEventStatus.InQueue;
			productChangeEvent.CreatedDate = DateTime.Now;
			productChangeEvent.Reference = reference;

			return productChangeEvent;
		}

		public virtual int Insert(IProductChangeEvent productChangeEvent)
		{
			productChangeEvent.Id = Repository.Insert(productChangeEvent);

			productChangeEvent.SetUntouched();

			return productChangeEvent.Id;
		}

		public virtual Collection<IProductChangeEvent> GetAll(ProductChangeEventStatus status)
		{
			return Repository.GetAll(int.MaxValue, status);
		}

		public virtual Collection<IProductChangeEvent> GetAllInQueue(int pageSize)
		{
			return Repository.GetAll(pageSize, ProductChangeEventStatus.InQueue);
		}

		public virtual Collection<IProductChangeEvent> GetCdonExportAll(ProductChangeEventStatus status)
		{
			return Repository.GetCdonExportAll(int.MaxValue, status);
		}

		public virtual Collection<IProductChangeEvent> GetCdonExportAllInQueue(int pageSize)
		{
			return Repository.GetCdonExportAll(pageSize, ProductChangeEventStatus.InQueue);
		}

		public virtual void SetStatus(Collection<IProductChangeEvent> productChangeEvents, ProductChangeEventStatus status)
		{
			string idList = Convert.ToStringIdentifierList(productChangeEvents.Select(p => p.Id));

			var actionAppliedDate = new DateTime();
			if (status == ProductChangeEventStatus.ActionApplied)
			{
				actionAppliedDate = DateTime.Now;
			}

			Repository.SetStatusByIdList(idList, Convert.DefaultListSeparator, status, actionAppliedDate);
		}

		public virtual void SetCdonExportStatus(Collection<IProductChangeEvent> productChangeEvents, ProductChangeEventStatus status)
		{
			string idList = Convert.ToStringIdentifierList(productChangeEvents.Select(p => p.Id));

			var actionAppliedDate = new DateTime();
			if (status == ProductChangeEventStatus.ActionApplied)
			{
				actionAppliedDate = DateTime.Now;
			}

			Repository.SetCdonExportStatusByIdList(idList, Convert.DefaultListSeparator, status, actionAppliedDate);
		}

		public virtual void DeleteExpiredItems()
		{
			Repository.DeleteExpiredItems();
		}
	}
}