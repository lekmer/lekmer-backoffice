﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerCategoryService : CategoryService, ILekmerCategoryService
	{
		protected LekmerCategoryRepository LekmerRepository { get; private set; }

		public LekmerCategoryService(
			CategoryRepository repository,
			IBlockCategoryProductListCategoryService blockCategoryProductListCategoryService)
			: base(repository,
			blockCategoryProductListCategoryService)
		{
			LekmerRepository = (LekmerCategoryRepository)repository;
		}

		protected override ICategoryTree GetAllAsTreeCore(IUserContext context)
		{
			Collection<ICategoryView> categories = GetViewAll(context);
			ILekmerCategoryTree tree = new LekmerCategoryTree();
			tree.FillTree(categories);
			return tree;
		}

		public virtual Collection<ICategoryView> GetViewByIdAll(IUserContext context, int categoryId)
		{
			return LekmerRepository.GetViewByIdAll(context.Channel, categoryId);
		}

		public virtual Collection<ICategoryView> GetViewAllByBlock(IUserContext context, IBlockCategoryProductList block)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			BlockCategoryProductListCategoryService.EnsureNotNull();

			ICategoryTree categoryTree = GetAllAsTree(context);
			Collection<IBlockCategoryProductListCategory> blockCategories = BlockCategoryProductListCategoryService.GetAllByBlock(context, block.Id);

			var resolvedCategories = new Collection<ICategoryView>();

			foreach (IBlockCategoryProductListCategory blockCategory in blockCategories)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, categoryTree, blockCategory);
			}

			return resolvedCategories;
		}
	}
}