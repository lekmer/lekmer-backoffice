﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class ProductStoreStockService : IProductStoreStockService
	{
		protected ProductStoreStockRepository Repository { get; private set; }

		public ProductStoreStockService(ProductStoreStockRepository repository)
		{
			Repository = repository;
		}

		public Collection<IProductStoreStock> GetAllByProduct(int productId)
		{
			return ProductStoreStockCollectionCache.Instance.TryGetItem(
				new ProductStoreStockCollectionKey(productId),
				() => Repository.GetAllByProduct(productId));
		}
	}
}
