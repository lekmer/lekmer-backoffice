﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Messenger;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.Product
{
	public class MonitorProductService : IMonitorProductService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected MonitorProductRepository Repository { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected IChannelService ChannelService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }

		public MonitorProductService(MonitorProductRepository repository, IProductService productService, IChannelService channelService, IProductSizeService productSizeService, ICategoryService categoryService)
		{
			Repository = repository;
			ProductService = productService;
			ChannelService = channelService;
			ProductSizeService = productSizeService;
			CategoryService = categoryService;
		}

		public virtual IMonitorProduct Create(IUserContext context, int productId, string email, int? sizeId)
		{
			var monitorProduct = IoC.Resolve<IMonitorProduct>();
			monitorProduct.ProductId = productId;
			monitorProduct.ChannelId = context.Channel.Id;
			monitorProduct.Email = email;
			monitorProduct.CreatedDate = DateTime.Now;
			monitorProduct.SizeId = sizeId;
			monitorProduct.Status = BusinessObjectStatus.New;
			return monitorProduct;
		}

		public virtual int Save(IUserContext context, IMonitorProduct monitorProduct)
		{
			if (monitorProduct == null) throw new ArgumentNullException("monitorProduct");
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				monitorProduct.Id = Repository.Save(monitorProduct);
				transactedOperation.Complete();
			}
			return monitorProduct.Id;
		}

		/// <summary>
		/// Monitors the products and sends notifications to customers.
		/// </summary>
		/// <param name="portionSize">Number of products in portion to be monitored between the breaks.</param>
		/// <param name="breakDuration">Number of seconds to wait before the next portion will be monitored.</param>
		/// <param name="minNumberInStockConfig">Number in stock required for mail sending.</param>
		public virtual void Monitor(int portionSize, int breakDuration, int minNumberInStockConfig)
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			var channels = new Collection<IChannel>();
			var maxId = 0;
			while (true)
			{
				var monitors = GetNextPortion(maxId, portionSize);
				if (monitors.Count == 0) break;

				maxId = monitors.Max(i => i.Id);
				foreach (var monitor in monitors)
				{
					var channel = GetChannel(channels, monitor.ChannelId);
					var product = GetProduct(channel, monitor.ProductId);

					if (product == null) continue;

					bool isBookable = product.IsBookable;
					bool isBuyable = product.IsBuyable;
					int numberInStock = product.NumberInStock;

					if (monitor.SizeId.HasValue)
					{
						var productSize = ProductSizeService.GetById(product.Id, monitor.SizeId.Value);
						if (productSize == null) continue;

						isBuyable = productSize.IsBuyable;
						numberInStock = productSize.NumberInStock;
					}
					else if (product.HasSizes)
					{
						var productSizes = ProductSizeService.GetAllByProduct(product.Id);
						if (productSizes != null)
						{
							isBuyable = productSizes.FirstOrDefault(ps => ps.IsBuyable) != null;
							numberInStock = 0;
							foreach (var size in productSizes)
							{
								numberInStock = numberInStock + size.NumberInStock;
							}
						}
					}

					var minNumberInStock = GetMinNumberInStock(product, channel, minNumberInStockConfig);
					if ((isBuyable || isBookable) && numberInStock >= minNumberInStock)
					{
						SendMail(product, channel, monitor);
					}
				}

				Thread.Sleep(breakDuration * 1000); //convert seconds to miliseconds
			}

			stopwatch.Stop();
			_log.InfoFormat("MonitorProductService.Monitor - elapsed time - {0}", stopwatch.Elapsed);
		}

		public virtual void Delete(IUserContext context, string email)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(context.Channel.Id, email);
				transactedOperation.Complete();
			}
		}


		private Collection<IMonitorProduct> GetNextPortion(int previousMaxId, int quantity)
		{
			return Repository.GetNextPortion(previousMaxId, quantity);
		}

		private void SendMail(ILekmerProduct product, IChannel channel, IMonitorProduct monitor)
		{
			var messageArgs = new ProductMessageArgsSecure(channel, monitor, product, ((IProductView) product).Description);
			var messenger = new MonitorProductMessenger();
			messenger.Send(messageArgs);
			Delete(monitor.Id);
		}

		private void Delete(int id)
		{
			Repository.Delete(id);
		}

		private ILekmerProduct GetProduct(IChannel channel, int productId)
		{
			var context = IoC.Resolve<IUserContext>();
			context.Channel = channel;
			var product = ProductService.GetViewById(context, productId) as ILekmerProduct;

			return product;
		}

		private IChannel GetChannel(ICollection<IChannel> storage, int channelId)
		{
			var channel = storage.FirstOrDefault(c => c.Id == channelId);
			if (channel == null)
			{
				channel = ChannelService.GetById(channelId);
				storage.Add(channel);
			}
			return channel;
		}

		private int GetMinNumberInStock(ILekmerProduct product, IChannel channel, int minNumberInStockConfig)
		{
			if (product.MonitorThreshold.HasValue)
			{
				return product.MonitorThreshold.Value;
			}

			if (product.Brand != null && product.Brand.MonitorThreshold.HasValue)
			{
				return product.Brand.MonitorThreshold.Value;
			}

			var context = IoC.Resolve<IUserContext>();
			context.Channel = channel;
			var categoryTree = CategoryService.GetAllAsTree(context);
			var category = categoryTree.FindItemById(product.CategoryId);
			var categoryMonitorThreshold = GetCategoryMonitorThreshold(category);
			if (categoryMonitorThreshold.HasValue)
			{
				return categoryMonitorThreshold.Value;
			}

			return minNumberInStockConfig;
		}

		private int? GetCategoryMonitorThreshold(ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null)
			{
				return null;
			}

			var category = categoryTreeItem.Category as ILekmerCategory;
			if (category != null && category.MonitorThreshold.HasValue)
			{
				return category.MonitorThreshold.Value;
			}

			return GetCategoryMonitorThreshold(categoryTreeItem.Parent);
		}
	}
}
