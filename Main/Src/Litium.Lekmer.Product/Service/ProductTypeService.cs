using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductTypeService : IProductTypeService
	{
		protected ProductTypeRepository Repository { get; private set; }

		public ProductTypeService(ProductTypeRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IProductType> GetAll(IUserContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return ProductTypeCollectionCache.Instance.TryGetItem(
				new ProductTypeCollectionKey(context.Channel.Id),
				() => Repository.GetAll());
		}

		public virtual IProductType GetById(IUserContext context, int productTypeId)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return ProductTypeCache.Instance.TryGetItem(
				new ProductTypeKey(context.Channel.Id, productTypeId),
				() => GetByIdCore(context, productTypeId));
		}

		public virtual Collection<int> GetIdAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetIdAllByBlock(blockId);
		}


		protected virtual IProductType GetByIdCore(IUserContext context, int productTypeId)
		{
			return GetAll(context).FirstOrDefault(pt => pt.Id == productTypeId);
		}
	}
}