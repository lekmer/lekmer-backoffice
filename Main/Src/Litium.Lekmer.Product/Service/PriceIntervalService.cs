using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class PriceIntervalService : IPriceIntervalService
	{
		protected PriceIntervalRepository Repository { get; private set; }

		public PriceIntervalService(PriceIntervalRepository repository)
		{
			Repository = repository;
		}

		public virtual IPriceIntervalBand GetPriceInterval(decimal? priceFrom, decimal? priceTo)
		{
			var priceInterval = IoC.Resolve<IPriceIntervalBand>();

			priceInterval.From = priceFrom;
			priceInterval.To = priceTo;

			return priceInterval;
		}

		public virtual IPriceIntervalBand GetPriceInterval(IUserContext context, int priceIntervalId)
		{
			IPriceInterval priceInterval = GetAll(context).FirstOrDefault(p => p.Id == priceIntervalId);

			var priceIntervalBand = IoC.Resolve<IPriceIntervalBand>();

			if (priceInterval != null)
			{
				priceIntervalBand.From = priceInterval.From;
				priceIntervalBand.To = priceInterval.To;
			}

			return priceIntervalBand;
		}

		public virtual IPriceIntervalBand GetPriceIntervalOuter(Collection<IPriceInterval> priceIntervals)
		{
			var priceInterval = IoC.Resolve<IPriceIntervalBand>();

			priceInterval.From = priceIntervals.Min(p => p.From);
			priceInterval.To = priceIntervals.Max(p => p.To);

			return priceInterval;
		}

		public virtual Collection<IPriceInterval> GetAll(IUserContext context)
		{
			return PriceIntervalListCache.Instance.TryGetItem(
				new PriceIntervalListKey(context.Channel.Id),
				delegate { return Repository.GetAll(context.Channel.Currency.Id, context.Channel.Language.Id); });
		}

		public virtual IPriceInterval GetMatching(IUserContext context, decimal price)
		{
			return GetAll(context)
				.FirstOrDefault(interval => interval.From <= price && interval.To >= price);
		}
	}
}