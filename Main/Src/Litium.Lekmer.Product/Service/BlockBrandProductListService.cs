﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockBrandProductListService : IBlockBrandProductListService
	{
		protected BlockBrandProductListRepository Repository { get; private set; }

		public BlockBrandProductListService(BlockBrandProductListRepository repository)
		{
			Repository = repository;
		}

		public IBlockBrandProductList GetById(IUserContext context, int blockId)
		{
			return BlockBrandProductListCache.Instance.TryGetItem(
				new BlockBrandProductListKey(context.Channel.Id, blockId),
				() => Repository.GetById(context.Channel, blockId));
		}
	}
}