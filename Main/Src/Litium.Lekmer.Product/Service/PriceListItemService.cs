using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class PriceListItemService : IPriceListItemService
	{
		public IPriceListItem Create()
		{
			return IoC.Resolve<IPriceListItem>();
		}
	}
}