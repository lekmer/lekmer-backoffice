﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class IconService : IIconService
	{
		protected IconRepository Repository { get; private set; }

		public IconService(IconRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IIcon> GetAll(IUserContext context, int productId, int categoryId, int? brandId)
		{
			return Repository.GetAll(productId, categoryId, brandId, context.Channel.Language.Id);
		}
	}
}