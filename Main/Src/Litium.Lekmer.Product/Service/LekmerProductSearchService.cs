using System;
using System.Globalization;
using System.Linq;
using Litium.Framework.Search;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch;

namespace Litium.Lekmer.Product
{
	public class LekmerProductSearchService : ProductSearchService
	{
		public LekmerProductSearchService(ISearchService searchService, IProductService productService)
			: base(searchService, productService)
		{
		}

		public ProductCollection WildcardSearch(IUserContext context, string text, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (text == null) throw new ArgumentNullException("text");

			var request = new SearchRequest();
			request.Text = text;
			request.Paging = new Paging(pageNumber, pageSize);
			request.QueryName = "WildcardProductQuery";
			request.LanguageId = context.Channel.CommonName;

			SearchResponse response = SearchService.Search(request);

			var productIds = new ProductIdCollection(
				response.Hits.Select(o => int.Parse(o.Id, CultureInfo.InvariantCulture)));
			productIds.TotalCount = response.TotalHitCount;

			return ProductService.PopulateProducts(context, productIds);
		}
	}
}