using System;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class PackageService : IPackageService
	{
		protected PackageRepository Repository { get; private set; }

		public PackageService(PackageRepository repository)
		{
			Repository = repository;
		}

		public virtual int GetNumberInStockForPackageWithSizes(string itemsWithSizesXml, string itemsWithoutSizes, char delimiter)
		{
			return Repository.GetNumberInStockForPackageWithSizes(itemsWithSizesXml, itemsWithoutSizes, delimiter);
		}

		public virtual IPackage GetByMasterProduct(IUserContext context, int masterProductId)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return PackageCache.Instance.TryGetItem(
				new PackageKey(context.Channel.Id, masterProductId),
				delegate { return Repository.GetByMasterProduct(context.Channel.Id, masterProductId); });
		}

		public ProductIdCollection GetMasterProductIdsByPackageProduct(IUserContext context, int productId)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return PackageProductRelationCache.Instance.TryGetItem(
				new PackageProductRelationKey(productId),
				() => Repository.GetMasterProductIdsByPackageProduct(context.Channel.Id, productId));
		}
	}
}