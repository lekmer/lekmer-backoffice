using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ColorService : IColorService
	{
		protected ColorRepository Repository { get; private set; }

		public ColorService(ColorRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IColor> GetAll(IUserContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return Repository.GetAll(context.Channel.Language.Id);
		}
	}
}