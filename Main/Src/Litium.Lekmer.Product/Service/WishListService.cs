﻿using System;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Lekmer.Product.Messenger;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class WishListService : IWishListService
	{
		protected WishListRepository Repository { get; private set; }
		protected IWishListItemService WishListItemService { get; private set; }

		public WishListService(WishListRepository repository, IWishListItemService wishListItemService)
		{
			Repository = repository;
			WishListItemService = wishListItemService;
		}

		public virtual IWishList Create()
		{
			var wishList = IoC.Resolve<IWishList>();
			wishList.CreatedDate = DateTime.Now;
			wishList.Status = BusinessObjectStatus.New;
			return wishList;
		}

		public virtual IWishList GetByKey(IUserContext context, Guid key)
		{
			if (context == null) throw new ArgumentNullException("context");

			return WishListCache.Instance.TryGetItem(
				new WishListCacheKey(key),
				() =>
					{
						var wishList = Repository.GetByKey(key);
						if (wishList != null)
						{
							((WishList)wishList).Items = WishListItemService.GetAllByWishList(context, wishList.Id);
						}
						return wishList;
					});
		}

		public virtual void Save(IUserContext context, IWishList wishList)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (wishList == null) throw new ArgumentNullException("wishList");

			using (var transactedOperation = new TransactedOperation())
			{
				wishList.UpdatedDate = DateTime.Now;
				wishList.Id = Repository.Save(wishList);
				SaveItems(context, wishList);
				transactedOperation.Complete();
			}

			UpdateItemsCollection(wishList);
			WishListCache.Instance.Remove(wishList.Key);
		}

		public virtual void Delete(IUserContext context, IWishList wishList)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (wishList == null) throw new ArgumentNullException("wishList");

			using (var transactedOperation = new TransactedOperation())
			{
				DeleteItems(context, wishList);
				Repository.Delete(wishList);
				transactedOperation.Complete();
			}

			WishListCache.Instance.Remove(wishList.Key);
		}

		public virtual void SendWishList(IUserContext context, WishListMessageArgs wishListMessageArgs)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (wishListMessageArgs == null) throw new ArgumentNullException("wishListMessageArgs");

			new WishListMessenger().Send(wishListMessageArgs);
		}

		private void SaveItems(IUserContext context, IWishList wishList)
		{
			foreach (var item in ((WishList)wishList).Items)
			{
				if (item.IsDeleted)
				{
					WishListItemService.Delete(context, item);
				}
				else if (item.IsNew || item.IsEdited)
				{
					item.WishListId = wishList.Id;
					WishListItemService.Save(context, item);
				}
			}
		}

		private void DeleteItems(IUserContext context, IWishList wishList)
		{
			foreach (var item in ((WishList)wishList).Items)
			{
				if (item.IsDeleted)
				{
					WishListItemService.Delete(context, item);
				}
			}
		}

		private void UpdateItemsCollection(IWishList wishList)
		{
			var allItems = ((WishList)wishList).Items;

			var itemsIdsToRemove = allItems.Where(x => x.IsDeleted).Select(x => x.Id).ToArray();
			foreach (var id in itemsIdsToRemove)
			{
				allItems.Remove(allItems.First(x => x.Id == id));
			}

			foreach (var item in allItems.Where(x => x.IsNew || x.IsEdited))
			{
				item.SetUntouched();
			}
		}
	}
}