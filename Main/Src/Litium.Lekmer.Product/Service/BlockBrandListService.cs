﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockBrandListService : IBlockBrandListService
	{
		protected BlockBrandListRepository Repository { get; private set; }

		public BlockBrandListService(BlockBrandListRepository repository)
		{
			Repository = repository;
		}

		public IBlockBrandList GetById(IUserContext context, int id)
		{
			return BlockBrandListCache.Instance.TryGetItem(
				new BlockBrandListKey(context.Channel.Id, id),
				delegate { return Repository.GetById(context.Channel, id); });
		}
	}
}