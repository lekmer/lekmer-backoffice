using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class SizeService : ISizeService
	{
		protected SizeRepository Repository { get; private set; }

		public SizeService(SizeRepository repository)
		{
			Repository = repository;
		}

		public Collection<ISize> GetAll(IUserContext userContext)
		{
			if (userContext == null)
			{
				throw new ArgumentNullException("userContext");
			}

			return SizeCollectionCache.Instance.TryGetItem(
				new SizeKey(userContext.Channel.Id),
				delegate { return Repository.GetAll(); });
		}
	}
}