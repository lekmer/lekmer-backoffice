﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Cache;
using Litium.Scensum.Product.Repository;
using Litium.Scensum.SiteStructure;
using BlockCategoryProductCollectionCache = Litium.Lekmer.Product.Cache.BlockCategoryProductCollectionCache;
using BlockCategoryProductCollectionKey = Litium.Lekmer.Product.Cache.BlockCategoryProductCollectionKey;
using Convert=Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class LekmerProductService : ProductService, ILekmerProductService
	{
		protected LekmerProductRepository LekmerRepository { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected IIconService IconService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected IContentNodeService ContentNodeService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected IEsalesProductTicketService EsalesProductTicketService { get; private set; }

		public LekmerProductService(
			ProductRepository repository, ICategoryService categoryService,
			IProductCampaignProcessor campaignProcessor, IBrandService brandService, IIconService iconService,
			IProductSizeService productSizeService, IContentNodeService contentNodeService, ITagGroupService tagGroupService,
			IEsalesProductTicketService esalesProductTicketService)
			: base(repository, categoryService, campaignProcessor)
		{
			LekmerRepository = (LekmerProductRepository)repository;
			BrandService = brandService;
			IconService = iconService;
			ProductSizeService = productSizeService;
			ContentNodeService = contentNodeService;
			TagGroupService = tagGroupService;
			EsalesProductTicketService = esalesProductTicketService;
		}

		public virtual ILekmerProduct Create()
		{
			return (ILekmerProduct)IoC.Resolve<IProduct>();
		}


		// Online products

		public override IProduct GetById(IUserContext context, int productId)
		{
			// Method overridden to be able to use cache for signed in customers.
			return GetById(context, productId, false);
		}

		public virtual IProduct GetById(IUserContext context, int productId, bool allowSellOnlyInPackage)
		{
			if (context == null) throw new ArgumentNullException("context");
			ProductCampaignProcessor.EnsureNotNull();

			IProduct product;
			if (IsCacheDenied(context))
			{
				product = GetByIdCore(context, productId, allowSellOnlyInPackage);
			}
			else
			{
				product = ProductCache.Instance.TryGetItem(
					new ProductKey(context.Channel.Id, productId),
					() => GetByIdCore(context, productId, allowSellOnlyInPackage));
			}

			if (product != null)
			{
				if (!allowSellOnlyInPackage && ((ILekmerProduct)product).SellOnlyInPackage) return null;

				ProductCampaignProcessor.Process(context, product);
			}

			return product;
		}

		public override IProductView GetViewById(IUserContext context, int productId)
		{
			// Method overridden to be able to use cache for signed in customers.
			return GetViewById(context, productId, false, true);
		}

		public virtual IProductView GetViewById(IUserContext context, int productId, bool allowSellOnlyInPackage, bool isNeedEsalesTicket)
		{
			if (context == null) throw new ArgumentNullException("context");
			ProductCampaignProcessor.EnsureNotNull();

			IProductView productView;
			if (IsCacheDenied(context))
			{
				productView = GetViewByIdCore(context, productId, allowSellOnlyInPackage, isNeedEsalesTicket);
			}
			else
			{
				productView = ProductViewCache.Instance.TryGetItem(
					new ProductKey(context.Channel.Id, productId),
					() => GetViewByIdCore(context, productId, allowSellOnlyInPackage, isNeedEsalesTicket));
			}

			if (productView != null)
			{
				if (!allowSellOnlyInPackage && ((ILekmerProductView)productView).SellOnlyInPackage) return null;

				ProductCampaignProcessor.Process(context, productView);
			}

			return productView;
		}


		public override ProductCollection PopulateProducts(IUserContext context, ProductIdCollection productIds)
		{
			return PopulateProducts(context, productIds, false);
		}

		public virtual ProductCollection PopulateProductsWithOnlyInPackage(IUserContext context, ProductIdCollection productIds)
		{
			return PopulateProducts(context, productIds, true);
		}

		protected virtual ProductCollection PopulateProducts(IUserContext context, ProductIdCollection productIds, bool allowSellOnlyInPackage)
		{
			// Method overridden to be able to use cache for signed in customers.

			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");

			ProductCampaignProcessor.EnsureNotNull();

			var productsPopulated = new Dictionary<int, IProduct>();
			var productsNotInCache = new List<int>();

			if (IsCacheDenied(context))
			{
				productsNotInCache = new List<int>(productIds.Distinct());
			}
			else
			{
				// Tries to get products from cache, otherwise adds them to fetching queue.
				foreach (int productId in productIds)
				{
					IProduct product = ProductCache.Instance.GetData(new ProductKey(context.Channel.Id, productId));

					if (product != null && !productsPopulated.ContainsKey(productId))
					{
						productsPopulated.Add(productId, product);
					}
					else if (!productsNotInCache.Contains(productId))
					{
						productsNotInCache.Add(productId);
					}
				}
			}

			// Fetch the products that wasn't in the cache.
			if (productsNotInCache.Count > 0)
			{
				Collection<IProduct> productsFetched = allowSellOnlyInPackage
					? GetAllWithOnlyInPackageByIds(context, productsNotInCache)
					: GetAllByIds(context, productsNotInCache);

				foreach (IProduct product in productsFetched)
				{
					if (!productsPopulated.ContainsKey(product.Id))
					{
						productsPopulated.Add(product.Id, product);

						if (IsCacheAllowed(context))
						{
							ProductCache.Instance.Add(new ProductKey(context.Channel.Id, product.Id), product);
						}
					}
				}
			}

			// Return populated products in the same order as they were passed to the method.
			var products = new ProductCollection();
			foreach (int productId in productIds)
			{
				IProduct product;
				if (productsPopulated.TryGetValue(productId, out product))
				{
					if (allowSellOnlyInPackage || !((ILekmerProduct)product).SellOnlyInPackage)
					{
						ProductCampaignProcessor.Process(context, product);
						products.Add(product);
					}
				}
			}

			products.TotalCount = productIds.TotalCount;
			return products;
		}


		public virtual ProductCollection GetAll(IUserContext context, int page, int pageSize)
		{
			var productIds = GetIdAll(context, page, pageSize);

			return PopulateProducts(context, productIds);
		}

		public virtual ProductCollection GetAllByBlock(IUserContext context, IBlockCategoryProductList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			ProductIdCollection productIds = BlockCategoryProductCollectionCache.Instance.TryGetItem(
				new BlockCategoryProductCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize),
				delegate { return GetIdAllByBlock(context, block, pageNumber, pageSize); });

			return PopulateProducts(context, productIds);
		}

		public virtual ProductCollection GetAllByBlock(IUserContext context, IBlockBrandProductList block, int pageNumber, int pageSize, int productSortOrderId)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			ProductIdCollection productIds = BlockBrandProductCollectionCache.Instance.TryGetItem(
				new BlockBrandProductCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize, productSortOrderId),
				() => GetIdAllByBlock(context, block, pageNumber, pageSize));

			return PopulateProducts(context, productIds);
		}

		public virtual ProductCollection GetAllByBlock(IUserContext context, IBlockProductList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			ProductIdCollection productIds = BlockListProductCollectionCache.Instance.TryGetItem(
				new BlockListProductCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize),
				delegate { return GetIdAllByBlock(context, block, pageNumber, pageSize); });

			return PopulateProducts(context, productIds);
		}

		public virtual ProductCollection GetAllByBlockAndProduct(IUserContext context, IBlockProductRelationList block, int productId, bool showVariants, int pageNumber, int pageSize)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			ProductIdCollection productIds = BlockRelatedProductCollectionCache.Instance.TryGetItem(
				new BlockRelatedProductCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize, productId),
				() => GetIdAllByBlockAndProduct(context, block, productId, showVariants, pageNumber, pageSize));
			
			return PopulateProducts(context, productIds);
		}

		public virtual ProductCollection GetAllByProductAndRelationType(IUserContext context, int productId, string relationType)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			ProductIdCollection productIds = ProductRelationCache.Instance.TryGetItem(
				new ProductRelationKey(productId, relationType),
				() => LekmerRepository.GetIdAllByProductAndRelationType(context.Channel.Id, context.Customer, productId, relationType));

			return PopulateProducts(context, productIds);
		}

		public virtual ProductCollection GetViewAllForExport(IUserContext context, int page, int pageSize)
		{
			var productIds = GetIdAll(context, page, pageSize);

			return PopulateViewProductsForExport(context, productIds);
		}

		public virtual ProductCollection PopulateViewProducts(IUserContext context, ProductIdCollection productIds)
		{
			return PopulateViewProducts(context, productIds, false);
		}

		public virtual ProductCollection PopulateViewWithOnlyInPackageProducts(IUserContext context, ProductIdCollection productIds)
		{
			return PopulateViewProducts(context, productIds, true);
		}

		protected virtual ProductCollection PopulateViewProducts(IUserContext context, ProductIdCollection productIds, bool allowSellOnlyInPackage)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");
			if (ProductCampaignProcessor == null) throw new InvalidOperationException("ProductCampaignProcessor cannot be null.");

			var productsPopulated = new Dictionary<int, IProductView>();
			var productsNotInCache = new List<int>();

			if (IsCacheDenied(context))
			{
				productsNotInCache = new List<int>(productIds.Distinct());
			}
			else
			{
				// Tries to get products from cache, otherwise adds them to fetching queue.
				foreach (int productId in productIds)
				{
					IProductView product = ProductViewCache.Instance.GetData(new ProductKey(context.Channel.Id, productId));

					if (product != null && !productsPopulated.ContainsKey(productId))
					{
						productsPopulated.Add(productId, product);
					}
					else if (!productsNotInCache.Contains(productId))
					{
						productsNotInCache.Add(productId);
					}
				}
			}

			// Fetch the products that wasn't in the cache.
			if (productsNotInCache.Count > 0)
			{
				Collection<IProductView> productsFetched = allowSellOnlyInPackage
					? GetAllViewWithOnlyInPackageByIds(context, productsNotInCache)
					: GetAllViewByIds(context, productsNotInCache);

				foreach (IProductView product in productsFetched)
				{
					if (!productsPopulated.ContainsKey(product.Id))
					{
						productsPopulated.Add(product.Id, product);
						if (IsCacheAllowed(context))
						{
							ProductViewCache.Instance.Add(new ProductKey(context.Channel.Id, product.Id), product);
						}
					}
				}
			}

			// Return populated products in the same order as they were passed to the method.
			var products = new ProductCollection();
			foreach (int productId in productIds)
			{
				IProductView product;
				if (productsPopulated.TryGetValue(productId, out product))
				{
					if (allowSellOnlyInPackage || !((ILekmerProductView)product).SellOnlyInPackage)
					{
						ProductCampaignProcessor.Process(context, product);
						products.Add(product);
					}
				}
			}

			products.TotalCount = productIds.TotalCount;
			return products;
		}

		public virtual ProductCollection PopulateViewProductsForExport(IUserContext context, ProductIdCollection productIds)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");
			if (ProductCampaignProcessor == null) throw new InvalidOperationException("ProductCampaignProcessor cannot be null.");

			LekmerProductViewCollection productCollection = LekmerRepository.GetAllViewByIdList(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProductWithLekmerUrl(context, productCollection);

			Dictionary<int, IProductView> productsPopulated = productCollection.ToDictionary(p => p.Id);

			// Return populated products in the same order as they were passed to the method.
			var products = new ProductCollection();
			foreach (int productId in productIds)
			{
				IProductView product;
				if (productsPopulated.TryGetValue(productId, out product))
				{
					ProductCampaignProcessor.Process(context, product);
					products.Add(product);
				}
			}

			products.TotalCount = productIds.TotalCount;
			return products;
		}

		public virtual ProductCollection SupplementProductsWithSizes(IEnumerable<ILekmerProductView> products)
		{
			var collection = new ProductCollection();

			foreach (LekmerProductView product in products.Where(product => product.ProductSizes.Any(productSize => productSize.NumberInStock > 0)))
			{
				collection.Add(product);
			}

			return collection;
		}

		public virtual ProductIdCollection GetVariantIdAllByProduct(int productId)
		{
			ProductIdCollection productIds = ProductVariantCache.Instance.TryGetItem(
				new ProductVariantKey(productId),
				() => LekmerRepository.GetVariantIdAllByProduct(productId, (int)RelationListType.Variant));

			var variantIds = new ProductIdCollection();
			foreach (var id in productIds)
			{
				variantIds.Add(id);
			}

			return variantIds;
		}


		protected virtual IProduct GetByIdCore(IUserContext context, int productId, bool allowSellOnlyInPackage)
		{
			var product = allowSellOnlyInPackage
				? LekmerRepository.GetProductIncludeOnlyInPackageById(context.Channel.Id, context.Customer, productId)
				: base.GetByIdCore(context, productId);
			if (product != null)
			{
				SupplementLekmerProduct(context, (ILekmerProduct)product, true);
			}
			return product;
		}

		protected virtual IProductView GetViewByIdCore(IUserContext context, int productId, bool allowSellOnlyInPackage, bool isNeedEsalesTicket)
		{
			var product = allowSellOnlyInPackage
				? LekmerRepository.GetProductViewIncludeOnlyInPackageById(context.Channel.Id, context.Customer, productId)
				: base.GetViewByIdCore(context, productId);
			if (product != null)
			{
				SupplementLekmerProduct(context, (ILekmerProduct) product, isNeedEsalesTicket);
				SupplementLekmerProductView(context, (ILekmerProductView) product);
			}
			return product;
		}

		protected override ProductCollection GetAllByIds(IUserContext context, IEnumerable<int> productIds)
		{
			var productCollection = LekmerRepository.GetAllByIdList(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProduct(context, productCollection);

			return productCollection;
		}

		protected virtual ProductCollection GetAllWithOnlyInPackageByIds(IUserContext context, IEnumerable<int> productIds)
		{
			var productCollection = LekmerRepository.GetAllWithOnlyInPackageByIdList(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProduct(context, productCollection);

			return productCollection;
		}


		protected virtual ProductIdCollection GetIdAll(IUserContext context, int page, int pageSize)
		{
			return LekmerRepository.GetIdAll(context.Channel, context.Customer, page, pageSize);
		}

		protected virtual ProductIdCollection GetIdAllByBlock(IUserContext context, IBlockCategoryProductList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			return GetIdAllByCategories(
				context,
				((ILekmerCategoryService) CategoryService).GetViewAllByBlock(context, block),
				pageNumber,
				pageSize,
				block.ProductSortOrder);
		}

		protected virtual ProductIdCollection GetIdAllByBlock(IUserContext context, IBlockBrandProductList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			return GetIdAllByBrands(
				context,
				BrandService.GetAllByBlockBrandProductList(context, block),
				pageNumber,
				pageSize,
				block.ProductSortOrder);
		}

		protected virtual ProductIdCollection GetIdAllByBlock(IUserContext context, IBlockProductList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			return LekmerRepository.GetIdAllByBlock(context.Channel, context.Customer, block, pageNumber, pageSize);
		}

		protected virtual ProductIdCollection GetIdAllByBlockAndProduct(IUserContext context, IBlockProductRelationList block, int productId, bool showVariants, int pageNumber, int pageSize)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			return LekmerRepository.GetIdAllByBlockAndProduct(context.Channel.Id, context.Customer, block, productId, showVariants, pageNumber, pageSize);
		}

		protected virtual ProductIdCollection GetIdAllByBrands(IUserContext context, Collection<IBrand> brands, int pageNumber, int pageSize, IProductSortOrder sortOrder)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (brands == null) throw new ArgumentNullException("brands");
			if (sortOrder == null) throw new ArgumentNullException("sortOrder");

			const char delimiter = ',';

			return LekmerRepository.GetIdAllByBrandIdList(
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(brands.Select(c => c.Id), delimiter),
				delimiter,
				pageNumber,
				pageSize,
				sortOrder);
		}

		protected virtual LekmerProductViewCollection GetAllViewByIds(IUserContext context, IEnumerable<int> productIds)
		{
			LekmerProductViewCollection productCollection = LekmerRepository.GetAllViewByIdList(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProduct(context, productCollection);
			SupplementLekmerProductView(context, productCollection);

			return productCollection;
		}

		protected virtual LekmerProductViewCollection GetAllViewWithOnlyInPackageByIds(IUserContext context, IEnumerable<int> productIds)
		{
			LekmerProductViewCollection productCollection = LekmerRepository.GetAllViewWithOnlyInPackageByIdList(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProduct(context, productCollection);
			SupplementLekmerProductView(context, productCollection);

			return productCollection;
		}


		// Products without status filter, no caching

		public virtual IProduct GetByIdWithoutStatusFilter(IUserContext context, int productId)
		{
			return GetByIdWithoutStatusFilter(context, productId, false);
		}

		public virtual IProduct GetByIdWithoutStatusFilter(IUserContext context, int productId, bool supplementProduct)
		{
			return GetByIdWithoutStatusFilter(context, productId, supplementProduct, false);
		}

		public virtual IProduct GetByIdWithoutStatusFilter(IUserContext context, int productId, bool supplementProduct, bool allowSellOnlyInPackage)
		{
			if (context == null) throw new ArgumentNullException("context");
			ProductCampaignProcessor.EnsureNotNull();

			var product = allowSellOnlyInPackage
				? LekmerRepository.GetProductIncludeOnlyInPackageByIdWithoutStatusFilter(context.Channel.Id, context.Customer, productId)
				: LekmerRepository.GetProductByIdWithoutStatusFilter(context.Channel.Id, context.Customer, productId);
			if (product != null)
			{
				if (supplementProduct)
				{
					SupplementLekmerProduct(context, (ILekmerProduct)product, true);
				}

				ProductCampaignProcessor.Process(context, product);
			}

			return product;
		}


		public virtual ProductCollection GetAllWithoutStatusFilter(IUserContext context, int page, int pageSize)
		{
			var productIds = GetIdAllWithoutStatusFilter(context, page, pageSize);

			return PopulateProductsWithoutStatusFilter(context, productIds);
		}

		public virtual ProductCollection PopulateProductsWithoutStatusFilter(IUserContext context, ProductIdCollection productIds)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");

			ProductCampaignProcessor.EnsureNotNull();

			ProductCollection products = GetAllByIdsWithoutStatusFilter(context, productIds);

			foreach (IProduct product in products)
			{
				ProductCampaignProcessor.Process(context, product);
			}

			products.TotalCount = productIds.TotalCount;

			return products;
		}

		public virtual ProductCollection PopulateViewProductsWithoutStatusFilter(IUserContext context, ProductIdCollection productIds)
		{
			return PopulateViewProductsWithoutStatusFilter(context, productIds, true);
		}

		public virtual ProductCollection PopulateViewProductsWithoutStatusFilter(IUserContext context, ProductIdCollection productIds, bool supplementProductView)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");

			ProductCampaignProcessor.EnsureNotNull();

			Collection<IProductView> productViews = GetAllViewByIdsWithoutStatusFilter(context, productIds, supplementProductView);

			var products = new ProductCollection();
			foreach (IProductView productView in productViews)
			{
				ProductCampaignProcessor.Process(context, productView);
				products.Add(productView);
			}

			products.TotalCount = productIds.TotalCount;

			return products;
		}


		protected virtual ProductIdCollection GetIdAllWithoutStatusFilter(IUserContext context, int page, int pageSize)
		{
			return LekmerRepository.GetIdAllWithoutStatusFilter(context.Channel, context.Customer, page, pageSize);
		}

		protected virtual ProductCollection GetAllByIdsWithoutStatusFilter(IUserContext context, IEnumerable<int> productIds)
		{
			var productCollection = LekmerRepository.GetAllByIdListWithoutStatusFilter(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProduct(context, productCollection);

			return productCollection;
		}

		protected virtual LekmerProductViewCollection GetAllViewByIdsWithoutStatusFilter(IUserContext context, IEnumerable<int> productIds, bool supplementProductView)
		{
			LekmerProductViewCollection productCollection = LekmerRepository.GetAllViewByIdListWithoutStatusFilter(// Does not support paging !!!
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			if (supplementProductView)
			{
				SupplementLekmerProduct(context, productCollection);
				SupplementLekmerProductView(context, productCollection);
			}

			return productCollection;
		}


		// Products without any filter

		public virtual ProductCollection GetAllWithoutAnyFilter(IUserContext context, int page, int pageSize, bool? isSellOnlyInPackage)
		{
			var productIds = GetIdAllWithoutAnyFilter(page, pageSize, isSellOnlyInPackage);
			return PopulateProductsWithoutAnyFilter(context, productIds);
		}

		public virtual ProductIdCollection GetIdAllWithoutAnyFilter(int page, int pageSize, bool? isSellOnlyInPackage)
		{
			return LekmerRepository.GetIdAllWithoutAnyFilter(page, pageSize, isSellOnlyInPackage);
		}

		public virtual ProductCollection PopulateProductsWithoutAnyFilter(IUserContext context, ProductIdCollection productIds)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");
			ProductCampaignProcessor.EnsureNotNull();

			ProductCollection products = GetAllByIdsWithoutAnyFilter(context, productIds);
			foreach (IProduct product in products)
			{
				ProductCampaignProcessor.Process(context, product);
			}
			products.TotalCount = productIds.TotalCount;
			return products;
		}

		protected virtual ProductCollection GetAllByIdsWithoutAnyFilter(IUserContext context, IEnumerable<int> productIds)
		{
			var productCollection = LekmerRepository.GetAllByIdListWithoutAnyFilter(
				context.Channel.Id,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			SupplementLekmerProduct(context, productCollection);

			return productCollection;
		}

		// Cdon export

		public virtual ProductIdCollection GetIdAllWithoutAnyFilterForCdonExport(int page, int pageSize, int monthPurchasedAgo)
		{
			return LekmerRepository.GetIdAllWithoutAnyFilterForCdonExport(page, pageSize,  monthPurchasedAgo);
		}

		public virtual ProductCollection PopulateViewProductsWithoutAnyFilter(IUserContext context, ProductIdCollection productIds, bool supplementProductView)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");

			ProductCampaignProcessor.EnsureNotNull();

			Collection<IProductView> productViews = GetAllViewByIdsWithoutAnyFilter(context, productIds, supplementProductView);

			var products = new ProductCollection();
			foreach (IProductView productView in productViews)
			{
				ProductCampaignProcessor.Process(context, productView);
				products.Add(productView);
			}

			products.TotalCount = productIds.TotalCount;

			return products;
		}

		protected virtual LekmerProductViewCollection GetAllViewByIdsWithoutAnyFilter(IUserContext context, IEnumerable<int> productIds, bool supplementProductView)
		{
			LekmerProductViewCollection productCollection = LekmerRepository.GetAllViewByIdListWithoutAnyFilter(// Does not support paging !!!
				context.Channel.Id,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator);

			if (supplementProductView)
			{
				SupplementLekmerProduct(context, productCollection);
				SupplementLekmerProductView(context, productCollection);
			}

			return productCollection;
		}


		// Additional product info

		protected virtual void SupplementLekmerProduct(IUserContext context, ILekmerProduct product, bool isNeedEsalesTicket)
		{
			if (product == null)
			{
				return;
			}

			var categoryTree = CategoryService.GetAllAsTree(context);
			var contentNodeTree = ContentNodeService.GetAllAsTree(context);

			SupplementLekmerProductCore(context, product, categoryTree, contentNodeTree);

			if (isNeedEsalesTicket)
			{
				SupplementProductWithEsalesTicket(context, product);
			}
		}

		protected virtual void SupplementLekmerProductCore(IUserContext context, ILekmerProduct product, ICategoryTree categoryTree, IContentNodeTree contentNodeTree)
		{
			if (product == null)
			{
				return;
			}

			if (product.BrandId.HasValue)
			{
				product.Brand = BrandService.GetById(context, product.BrandId.Value);
			}

			AppendLekmerUrl(product, categoryTree, contentNodeTree);
		}

		protected virtual void SupplementLekmerProduct(IUserContext context, ProductCollection productCollection)
		{
			if (productCollection == null)
			{
				return;
			}

			var categoryTree = CategoryService.GetAllAsTree(context);
			var contentNodeTree = ContentNodeService.GetAllAsTree(context);

			foreach (var product in productCollection)
			{
				var lekmerProduct = (ILekmerProduct)product;

				SupplementLekmerProductCore(context, lekmerProduct, categoryTree, contentNodeTree);
			}

			SupplementProductWithEsalesTicket(context, productCollection);
		}

		protected virtual void SupplementLekmerProduct(IUserContext context, LekmerProductViewCollection productViewCollection)
		{
			if (productViewCollection == null)
			{
				return;
			}

			var categoryTree = CategoryService.GetAllAsTree(context);
			var contentNodeTree = ContentNodeService.GetAllAsTree(context);

			foreach (var productView in productViewCollection)
			{
				var lekmerProduct = (ILekmerProduct)productView;

				SupplementLekmerProductCore(context, lekmerProduct, categoryTree, contentNodeTree);
			}

			SupplementProductWithEsalesTicket(context, productViewCollection);
		}

		protected virtual void SupplementLekmerProductWithLekmerUrl(IUserContext context, LekmerProductViewCollection productViewCollection)
		{
			if (productViewCollection == null)
			{
				return;
			}

			var categoryTree = CategoryService.GetAllAsTree(context);
			var contentNodeTree = ContentNodeService.GetAllAsTree(context);

			foreach (var productView in productViewCollection)
			{
				var lekmerProduct = (ILekmerProduct)productView;

				AppendLekmerUrl(lekmerProduct, categoryTree, contentNodeTree);
			}
		}



		protected virtual void SupplementLekmerProductView(IUserContext context, ILekmerProductView productView)
		{
			SupplementLekmerProductViewCore(context, productView);
		}

		protected virtual void SupplementLekmerProductViewCore(IUserContext context, ILekmerProductView productView)
		{
			if (productView == null)
			{
				return;
			}

			productView.Icons = IconService.GetAll(context, productView.Id, productView.CategoryId, productView.BrandId);
			productView.ProductSizes = ProductSizeService.GetAllByProduct(productView.Id);
			productView.TagGroups = TagGroupService.GetAllByProduct(context, productView.Id);
		}

		protected virtual void SupplementLekmerProductView(IUserContext context, LekmerProductViewCollection productViewCollection)
		{
			if (productViewCollection == null)
			{
				return;
			}

			foreach (var productView in productViewCollection)
			{
				var lekmerProductView = (ILekmerProductView)productView;

				SupplementLekmerProductViewCore(context, lekmerProductView);
			}
		}

		protected virtual void SupplementLekmerProductViewWithTagGroups(IUserContext context, LekmerProductViewCollection productViewCollection)
		{
			if (productViewCollection == null)
			{
				return;
			}

			foreach (var productView in productViewCollection)
			{
				var lekmerProductView = (ILekmerProductView)productView;
				lekmerProductView.TagGroups = TagGroupService.GetAllByProduct(context, productView.Id);
			}
		}


		protected virtual void SupplementProductWithEsalesTicket(IUserContext context, ILekmerProduct product)
		{
			EsalesProductTicketService.EnsureNotNull();

			product.EsalesTicket = EsalesProductTicketService.FindTicket(context, product.Id);
		}

		protected virtual void SupplementProductWithEsalesTicket(IUserContext context, ProductCollection productCollection)
		{
			EsalesProductTicketService.EnsureNotNull();

			if (productCollection == null)
			{
				return;
			}

			Dictionary<int, string> tickets = EsalesProductTicketService.FindTickets(context, new ProductIdCollection(productCollection.Select(p => p.Id)));

			foreach (IProduct product in productCollection)
			{
				if (tickets.ContainsKey(product.Id))
				{
					var lekmerProduct = (ILekmerProduct)product;
					lekmerProduct.EsalesTicket = tickets[product.Id];
				}
			}
		}

		protected virtual void SupplementProductWithEsalesTicket(IUserContext context, LekmerProductViewCollection productViewCollection)
		{
			EsalesProductTicketService.EnsureNotNull();

			if (productViewCollection == null)
			{
				return;
			}

			Dictionary<int, string> tickets = EsalesProductTicketService.FindTickets(context, new ProductIdCollection(productViewCollection.Select(p => p.Id)));

			foreach (IProductView productView in productViewCollection)
			{
				if (tickets.ContainsKey(productView.Id))
				{
					var lekmerProductView = (ILekmerProductView)productView;
					lekmerProductView.EsalesTicket = tickets[productView.Id];
				}
			}
		}


		protected virtual void AppendLekmerUrl(ILekmerProduct product, ICategoryTree categoryTree, IContentNodeTree contentNodeTree)
		{
			product.ParentPageUrl = GetParentPageUrl(product, categoryTree, contentNodeTree);
		}


		protected virtual bool IsCacheDenied(IUserContext context)
		{
			// No caching when signed in.
			return LekmerProductCacheSetting.Instance.NoCachingWhenSignedIn && context.Customer != null;
		}

		protected virtual bool IsCacheAllowed(IUserContext context)
		{
			// No caching when signed in.
			return !IsCacheDenied(context);
		}


		private static string GetParentPageUrl(ILekmerProduct product, ICategoryTree categoryTree, IContentNodeTree contentNodeTree)
		{
			int? parentContentNodeId = ResolveParentContentNodeId(product, categoryTree);

			if (parentContentNodeId.HasValue)
			{
				var contentNodeTreeItem = contentNodeTree.FindItemById(parentContentNodeId.Value);
				if (contentNodeTreeItem != null)
				{
					return contentNodeTreeItem.Url;
				}
			}

			return null;
		}

		private static int? ResolveParentContentNodeId(ILekmerProduct product, ICategoryTree categoryTree)
		{
			if (product.ParentContentNodeId.HasValue)
			{
				return product.ParentContentNodeId;
			}

			var categoryTreeItem = categoryTree.FindItemById(product.CategoryId);
			if (categoryTreeItem != null && categoryTreeItem.Category != null)
			{
				if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
				{
					return categoryTreeItem.Category.ProductParentContentNodeId.Value;
				}

				foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
				{
					if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
					{
						return parentItem.Category.ProductParentContentNodeId.Value;
					}
				}
			}

			return null;
		}

		// Defines if multiple sizes purchase is allowed for product according to its category settings
		public bool AllowMultipleSizesPurchase(IUserContext context, IProduct product)
		{
			var categoryTree = CategoryService.GetAllAsTree(context);
			var categoryTreeItem = categoryTree.FindItemById(product.CategoryId);
			return DefineIfMultipleSizesPurchaseIsAllowedRecursively(categoryTreeItem);
		}

		private static bool DefineIfMultipleSizesPurchaseIsAllowedRecursively(ICategoryTreeItem categoryTreeItem)
		{
			const bool _default = false;
			if (categoryTreeItem == null || categoryTreeItem.Category == null) return _default;
			var category = (ILekmerCategory)categoryTreeItem.Category;
			return category.AllowMultipleSizesPurchase.HasValue
				? category.AllowMultipleSizesPurchase.Value
				: DefineIfMultipleSizesPurchaseIsAllowedRecursively(categoryTreeItem.Parent);
		}

		// Package
		public virtual ProductIdCollection GetIdAllByPackageMasterProduct(IUserContext context, int productId)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return PackageProductRelationCache.Instance.TryGetItem(
				new PackageProductRelationKey(productId),
				() => LekmerRepository.GetIdAllByPackageMasterProduct(context.Channel.Id, context.Customer, productId));
		}
	}
}