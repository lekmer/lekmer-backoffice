﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class WishListItemService : IWishListItemService
	{
		protected WishListItemRepository Repository { get; private set; }
		protected IWishListPackageItemService WishListPackageItemService { get; private set; }

		public WishListItemService(WishListItemRepository repository, IWishListPackageItemService wishListPackageItemService)
		{
			Repository = repository;
			WishListPackageItemService = wishListPackageItemService;
		}

		public IWishListItem Create()
		{
			var item = IoC.Resolve<IWishListItem>();
			item.Status = BusinessObjectStatus.New;
			return item;
		}

		public Collection<IWishListItem> GetAllByWishList(IUserContext context, int wishListId)
		{
			Collection<IWishListItem> items;
			using (var transactedOperation = new TransactedOperation())
			{
				items = Repository.GetByWishList(wishListId);
				foreach (var item in items)
				{
					item.PackageItems = WishListPackageItemService.GetAllByWishListItem(item.Id);
				}
				transactedOperation.Complete();
			}
			return items;
		}

		public int Save(IUserContext context, IWishListItem item)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (item == null) throw new ArgumentNullException("item");

			item.Id = Repository.Save(item);
			return item.Id;
		}

		public void Delete(IUserContext context, IWishListItem item)
		{
			if (item == null) throw new ArgumentNullException("item");

			Repository.Delete(item.Id);
		}
	}
}