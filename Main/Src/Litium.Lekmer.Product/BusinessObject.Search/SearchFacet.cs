using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class SearchFacet : ISearchFacet
	{
		public SearchFacet()
		{
			AllBrandsWithoutFilter = new BrandCollection();
			BrandsWithFilter = new Dictionary<int, int>();
			AllMainCategoriesWithoutFilter = new CategoryCollection();
			MainCategoriesWithFilter = new Dictionary<int, int>();
			AllParentCategoriesWithoutFilter = new CategoryCollection();
			ParentCategoriesWithFilter = new Dictionary<int, int>();
			AllCategoriesWithoutFilter = new CategoryCollection();
			CategoriesWithFilter = new Dictionary<int, int>();
			AgeInterval = new AgeIntervalBand();
			PriceInterval = new PriceIntervalBand();
			AllSizesWithoutFilter = new Collection<ISize>();
			SizesWithFilter = new Dictionary<int, int>();
			TagsWithFilter = new Dictionary<int, int>();
		}

		public BrandCollection AllBrandsWithoutFilter { get; set; }
		public Dictionary<int, int> BrandsWithFilter { get; set; }
		public CategoryCollection AllMainCategoriesWithoutFilter { get; set; }
		public Dictionary<int, int> MainCategoriesWithFilter { get; set; }
		public CategoryCollection AllParentCategoriesWithoutFilter { get; set; }
		public Dictionary<int, int> ParentCategoriesWithFilter { get; set; }
		public CategoryCollection AllCategoriesWithoutFilter { get; set; }
		public Dictionary<int, int> CategoriesWithFilter { get; set; }
		public IAgeIntervalBand AgeInterval { get; set; }
		public IPriceIntervalBand PriceInterval { get; set; }
		public Collection<ISize> AllSizesWithoutFilter { get; set; }
		public Dictionary<int, int> SizesWithFilter { get; set; }
		public Dictionary<int, int> TagsWithFilter { get; set; }
	}
}