using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BrandSuggestionInfo : IdSuggestionInfo, IBrandSuggestionInfo
	{
		public IBrand Brand { get; set; }
	}
}