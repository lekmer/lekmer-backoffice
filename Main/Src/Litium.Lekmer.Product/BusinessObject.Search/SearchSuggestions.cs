using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class SearchSuggestions : ISearchSuggestions
	{
		public ProductCollection Products { get; set; }

		public Collection<ICategoryTagSearchSuggestion> CategoryTags { get; set; }

		public Collection<IBrandSuggestionInfo> Brands { get; set; }

		public Collection<ISearchSuggestionInfo> Suggestions { get; set; }

		public Collection<ISearchSuggestionInfo> Corrections { get; set; }

		public Collection<ISearchSuggestionInfo> TopSearches { get; set; }

		public ISearchFacet SearchFacetWithFilter { get; set; }
		public ISearchFacet SearchFacetWithoutFilter { get; set; }
	}
}