using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SearchSuggestionInfo : ISearchSuggestionInfo
	{
		public string Text { get; set; }
		public string Ticket { get; set; }
	}
}