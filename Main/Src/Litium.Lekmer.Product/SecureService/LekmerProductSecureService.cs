﻿using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Cache;
using Litium.Scensum.Product;
using System.Collections.Generic;
using Litium.Lekmer.Product.Repository;
using System;
using Litium.Framework.Transaction;
using Litium.Scensum.Product.Repository;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class LekmerProductSecureService : ProductSecureService, ILekmerProductSecureService
	{
		protected ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> ProductWebShopTitleTranslationSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductShortDescriptionTranslationRepository> ProductShortDescriptionTranslationSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> ProductDescriptionTranslationSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductSeoSettingTitleTranslationRepository> ProductSeoSettingTitleTranslationSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductSeoSettingDescriptionTranslationRepository> ProductSeoSettingDescriptionTranslationSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductSeoSettingKeywordsTranslationRepository> ProductSeoSettingKeywordsTranslationSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductMeasurementTranslationRepository> ProductMeasurementTranslationSecureService { get; private set; }
		protected IIconSecureService IconSecureService { get; private set; }
		protected ITagSecureService TagSecureService { get; private set; }
		protected IProductSizeSecureService ProductSizeSecureService { get; private set; }
		protected IImageSecureService ImageSecureService { get; private set; }
		protected IProductUrlSecureService ProductUrlSecureService { get; private set; }
		protected IProductRegistryRestrictionProductSecureService ProductRegistryRestrictionProductSecureService { get; private set; }
		protected IProductChangesSecureService ProductChangesSecureService { get; private set; }
		protected IRecommendedPriceSecureService RecommendedPriceSecureService { get; private set; }
		protected LekmerProductRepository LekmerRepository;

		public LekmerProductSecureService(
			IAccessValidator accessValidator,
			ProductRepository repository,
			IProductSeoSettingSecureService productSeoSettingSecureService,
			IProductSiteStructureRegistrySecureService productSiteStructureRegistrySecureService,
			IProductImageSecureService productImageSecureService,
			IStoreProductSecureService storeProductSecureService,
			ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> productWebShopTitleTranslationSecureService,
			ProductTranslationGenericSecureService<ProductShortDescriptionTranslationRepository> productShortDescriptionTranslationSecureService,
			ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> productDescriptionTranslationSecureService,
			ProductTranslationGenericSecureService<ProductSeoSettingTitleTranslationRepository> productSeoSettingTitleTranslationSecureService,
			ProductTranslationGenericSecureService<ProductSeoSettingDescriptionTranslationRepository> productSeoSettingDescriptionTranslationSecureService,
			ProductTranslationGenericSecureService<ProductSeoSettingKeywordsTranslationRepository> productSeoSettingKeywordsTranslationSecureService,
			ProductTranslationGenericSecureService<ProductMeasurementTranslationRepository> productMeasurementTranslationSecureService,
			IIconSecureService iconSecureService,
			ITagSecureService tagSecureService,
			IProductSizeSecureService productSizeSecureService,
			IProductUrlSecureService productUrlSecureService,
			IImageCacheCleaner imageCacheCleaner,
			IImageSecureService imageSecureService,
			IProductRegistryRestrictionProductSecureService productRegistryRestrictionProductSecureService,
			IProductChangesSecureService productChangesSecureService,
			IRecommendedPriceSecureService recommendedPriceSecureService)
			: base(accessValidator, repository, productSeoSettingSecureService, productSiteStructureRegistrySecureService,
				productImageSecureService, storeProductSecureService, imageCacheCleaner)
		{
			ProductWebShopTitleTranslationSecureService = productWebShopTitleTranslationSecureService;
			ProductShortDescriptionTranslationSecureService = productShortDescriptionTranslationSecureService;
			ProductDescriptionTranslationSecureService = productDescriptionTranslationSecureService;
			ProductSeoSettingTitleTranslationSecureService = productSeoSettingTitleTranslationSecureService;
			ProductSeoSettingDescriptionTranslationSecureService = productSeoSettingDescriptionTranslationSecureService;
			ProductSeoSettingKeywordsTranslationSecureService = productSeoSettingKeywordsTranslationSecureService;
			ProductMeasurementTranslationSecureService = productMeasurementTranslationSecureService;
			IconSecureService = iconSecureService;
			TagSecureService = tagSecureService;
			ProductSizeSecureService = productSizeSecureService;
			ProductUrlSecureService = productUrlSecureService;
			ImageSecureService = imageSecureService;
			ProductRegistryRestrictionProductSecureService = productRegistryRestrictionProductSecureService;
			ProductChangesSecureService = productChangesSecureService;
			RecommendedPriceSecureService = recommendedPriceSecureService;
			LekmerRepository = (LekmerProductRepository) Repository;
		}

		public override IProductRecord GetRecordById(int channelId, int productId)
		{
			var product = base.GetRecordById(channelId, productId) as ILekmerProductRecord;
			if (product != null)
			{
				product.Icons = IconSecureService.GetAllByProduct(productId, product.CategoryId, product.BrandId);
			}
			return product;
		}

		public void Save(ISystemUserFull systemUserFull,
			IProductRecord product,
			Collection<IRelationList> relationLists,
			IProductSeoSetting seoSetting,
			Collection<IProductImageGroupFull> imageGroups,
			IEnumerable<IStoreProduct> storeProductItems,
			Collection<ITranslationGeneric> webShopTitleTranslations,
			Collection<ITranslationGeneric> shortDescriptionTranslations,
			Collection<ITranslationGeneric> descriptionTranslations,
			Collection<ITranslationGeneric> seoTitleTranslations,
			Collection<ITranslationGeneric> seoDescriptionTranslations,
			Collection<ITranslationGeneric> seoKeywordsTranslations,
			Collection<ITranslationGeneric> measurementTranslations,
			Collection<int> icons,
			Collection<int> tagsToRemove,
			Collection<int> tagsToAdd,
			IEnumerable<IProductSize> productSizes,
			IEnumerable<IProductUrl> productUrls,
			Collection<IProductRegistryRestrictionItem> registryRestrictions,
			Collection<IRecommendedPrice> recommendedPrices)
		{
			if (product == null) throw new ArgumentNullException("product");
			if (relationLists == null) throw new ArgumentNullException("relationLists");
			if (seoSetting == null) throw new ArgumentNullException("seoSetting");
			if (icons == null) throw new ArgumentNullException("icons");
			if (tagsToRemove == null) throw new ArgumentNullException("tagsToRemove");
			if (tagsToAdd == null) throw new ArgumentNullException("tagsToAdd");
			if (productSizes == null) throw new ArgumentNullException("productSizes");
			if (productUrls == null) throw new ArgumentNullException("productUrls");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				base.Save(systemUserFull, product, relationLists, seoSetting, imageGroups, storeProductItems);

				ProductWebShopTitleTranslationSecureService.Save(systemUserFull, webShopTitleTranslations);
				ProductShortDescriptionTranslationSecureService.Save(systemUserFull, shortDescriptionTranslations);
				ProductDescriptionTranslationSecureService.Save(systemUserFull, descriptionTranslations);

				ProductSeoSettingTitleTranslationSecureService.Save(systemUserFull, seoTitleTranslations);
				ProductSeoSettingDescriptionTranslationSecureService.Save(systemUserFull, seoDescriptionTranslations);
				ProductSeoSettingKeywordsTranslationSecureService.Save(systemUserFull, seoKeywordsTranslations);

				ProductMeasurementTranslationSecureService.Save(systemUserFull, measurementTranslations);
				IconSecureService.SaveProductIcon(systemUserFull, product.Id, icons);
				TagSecureService.UpdateProductTags(product.Id, tagsToAdd, tagsToRemove, -1);
				ProductSizeSecureService.Save(systemUserFull, productSizes);
				ProductUrlSecureService.Save(systemUserFull, product.Id, productUrls);
				ProductRegistryRestrictionProductSecureService.Save(systemUserFull, product.Id, registryRestrictions);

				RecommendedPriceSecureService.Save(systemUserFull, product.Id, recommendedPrices);

				transactedOperation.Complete();
			}

			TrackProductChanges(systemUserFull, product);

			RemoveFromCache(product.Id);
			FilterProductCollectionCache.Instance.Flush();
		}

		public void UpdatePopularity(Collection<IChannel> channels, DateTime dateTimeFrom, DateTime dateTimeTo)
		{
			foreach (IChannel channel in channels)
			{
				LekmerRepository.UpdatePopularity(channel.Id, dateTimeFrom, dateTimeTo);
			}
			FilterProductCollectionCache.Instance.Flush();
		}

		/// <summary>
		/// Gets the list of variant product id's for specified product.
		/// </summary>
		/// <param name="productId">The product id.</param>
		/// <returns>Returns list of variant product id's, excluding specified product id. If product hasn't variants, empty list is returned</returns>
		public virtual ProductIdCollection GetVariantIdAllByProduct(int productId)
		{
			ProductIdCollection productIds = ProductVariantCache.Instance.TryGetItem(
				new ProductVariantKey(productId),
				() => LekmerRepository.GetVariantIdAllByProduct(productId, (int)RelationListType.Variant));

			var variantIds = new ProductIdCollection();
			foreach (var id in productIds)
			{
				variantIds.Add(id);
			}

			return variantIds;
		}

		[SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "full")]
		protected override void SaveImageGroups(int productId, Collection<IProductImageGroupFull> imageGroups)
		{
			ProductImageSecureService.DeleteAllByProduct(productId);
			foreach (var group in imageGroups)
			{
				foreach (IProductImage image in group.ProductImages)
				{
					if (((ILekmerImage)image.Image).IsLink)
					{
						((ILekmerImageSecureService)ImageSecureService).SaveMovie((ILekmerImage)image.Image);
						image.MediaId = image.Image.Id;
					}
					ProductImageSecureService.Save(image);
				}
			}
		}

		protected virtual void TrackProductChanges(ISystemUserFull systemUserFull, IProduct product)
		{
			IProductChangeEvent productChangeEvent = ProductChangesSecureService.Create(systemUserFull, product.Id);
			ProductChangesSecureService.Insert(productChangeEvent);
		}

		public virtual ProductCollection GetAllByPackage(int channelId, int packageId)
		{
			return LekmerRepository.GetAllByPackageSecure(channelId, packageId);
		}

		public virtual ProductIdCollection GetIdAllByCampaign(int campaignId)
		{
			return LekmerRepository.GetIdAllByCampaign(campaignId);
		}

		public virtual Collection<IProductRecord> GetProductRecordAllByIdList(IEnumerable<int> productIds)
		{
			return LekmerRepository.GetProductRecordAllByIdList(Convert.ToStringIdentifierList(productIds), Convert.DefaultListSeparator);
		}
		public virtual Dictionary<string, int> GetProductIdsByHyIdList(IEnumerable<string> productHyIds)
		{
			return LekmerRepository.GetProductIdsByHyIdList(Convert.ToStringIdentifierList(productHyIds), Convert.DefaultListSeparator);
		}

		public virtual Dictionary<string, decimal> GetPriceByChannelAndCampaign(int currencyId, decimal priceLimit, IFixedPriceAction action)
		{
			return LekmerRepository.GetPriceByChannelAndCampaign(currencyId, priceLimit, action);
		}

		public virtual bool IsProductDeleted(int productId)
		{
			LekmerRepository.EnsureNotNull();
			return LekmerRepository.IsProductDeleted(productId);
		}

		#region Import Product Info

		public string UpdateProductInfo(IImportProductInfo importProductInfo, int channelId, int languageId, string userName)
		{
			LekmerRepository.EnsureNotNull();
			return LekmerRepository.UpdateProductInfo(importProductInfo, channelId, languageId, userName);
		}
		public void RemoveOldProductInfoImportLogs(int days)
		{
			LekmerRepository.EnsureNotNull();
			LekmerRepository.RemoveOldProductInfoImportLogs(days);
		}

		#endregion
	}
}