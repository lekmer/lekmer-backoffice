﻿using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class MonitorProductSecureService : IMonitorProductSecureService
	{
		protected MonitorProductRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public MonitorProductSecureService(MonitorProductRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual void Delete(ISystemUserFull user, int channelId, string email)
		{
			AccessValidator.ForceAccess(user, PrivilegeConstant.CustomerCustomer);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(channelId, email);
				transactedOperation.Complete();
			}
		}
	}
}