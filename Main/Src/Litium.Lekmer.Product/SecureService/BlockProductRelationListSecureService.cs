﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockProductRelationListSecureService : IBlockProductRelationListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductRelationListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockProductRelationListItemSecureService BlockProductRelationListItemSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockProductRelationListSecureService(
			IAccessValidator accessValidator,
			BlockProductRelationListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockProductRelationListItemSecureService blockProductRelationListItemSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockProductRelationListItemSecureService = blockProductRelationListItemSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockProductRelationList Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var block = IoC.Resolve<IBlockProductRelationList>();
			block.AccessId = AccessSecureService.GetByCommonName("All").Id;
			block.ProductSortOrder = IoC.Resolve<IProductSortOrder>();
			block.Setting = BlockSettingSecureService.Create();
			block.Status = BusinessObjectStatus.New;
			return block;
		}

		public virtual IBlockProductRelationList GetById(int blockId)
		{
			return Repository.GetByIdSecure(blockId);
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductRelationList block, Collection<IBlockProductRelationListItem> items)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (items == null) throw new ArgumentNullException("items");

			BlockSecureService.EnsureNotNull();
			BlockProductRelationListItemSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				// Save IBlock.
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				// Save IBlockProductRelationList.
				Repository.Save(block);

				// Save list of IBlockProductRelationListItem.
				SaveItems(block.Id, items);

				// Save Setting.
				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			BlockProductRelationListCache.Instance.Remove(block.Id);
			return block.Id;
		}

		protected virtual void SaveItems(int blockId, IEnumerable<IBlockProductRelationListItem> items)
		{
			BlockProductRelationListItemSecureService.Delete(blockId);
			foreach (var item in items)
			{
				BlockProductRelationListItemSecureService.Save(item);
			}
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			IBlockProductRelationList block = Create();
			block.ContentNodeId = contentNodeId;
			block.ContentAreaId = contentAreaId;
			block.BlockTypeId = blockTypeId;
			block.BlockStatusId = (int)BlockStatusInfo.Offline;
			block.Title = title;
			block.TemplateId = null;
			block.Id = Save(systemUserFull, block, new Collection<IBlockProductRelationListItem>());
			return block;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				BlockProductRelationListItemSecureService.Delete(blockId);
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockProductRelationListCache.Instance.Remove(blockId);
		}
	}
}
