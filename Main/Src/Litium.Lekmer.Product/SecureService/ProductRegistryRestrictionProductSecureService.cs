﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductRegistryRestrictionProductSecureService : IProductRegistryRestrictionProductSecureService
	{
		protected ProductRegistryRestrictionProductRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductRegistryRestrictionProductSecureService(ProductRegistryRestrictionProductRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual IProductRegistryRestrictionItem Create()
		{
			var productRegistryRestrictionProduct = IoC.Resolve<IProductRegistryRestrictionItem>();
			productRegistryRestrictionProduct.CreatedDate = DateTime.Now;
			productRegistryRestrictionProduct.Status = BusinessObjectStatus.New;
			return productRegistryRestrictionProduct;
		}

		public virtual void Save(ISystemUserFull systemUserFull, int productId, Collection<IProductRegistryRestrictionItem> productRegistryRestrictionProductList)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			if (productRegistryRestrictionProductList == null)
			{
				throw new NullReferenceException("productRegistryRestrictionProductList");
			}

			using (var transaction = new TransactedOperation())
			{
				Delete(systemUserFull, productId);

				foreach (var productRegistryRestrictionProduct in productRegistryRestrictionProductList)
				{
					Repository.Save(productRegistryRestrictionProduct);
				}

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int productId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);
			Repository.Delete(productId);
		}

		public Collection<IProductRegistryRestrictionItem> GetAllByProduct(int productId)
		{
			return Repository.GetAllByProduct(productId);
		}

		public Collection<IProductRegistryRestrictionItem> GetAllWithChannel()
		{
			return Repository.GetAllWithChannel();
		}
	}
}