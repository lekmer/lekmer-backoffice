﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class DeliveryTimeSecureService : IDeliveryTimeSecureService
	{
		protected DeliveryTimeRepository Repository { get; private set; }

		public DeliveryTimeSecureService(DeliveryTimeRepository repository)
		{
			Repository = repository;
		}


		public virtual int Insert()
		{
			return Repository.Insert();
		}

		public virtual void Delete(int id)
		{
			Repository.DeleteProductRegistryDeliveryTime(id);
			Repository.Delete(id);
		}


		public virtual void SaveProductRegistryDeliveryTime(int id, int? fromDays, int? toDays, string format, int? productRegistryId)
		{
			Repository.SaveProductRegistryDeliveryTime(id, fromDays, toDays, format, productRegistryId);
		}

		public virtual Collection<IDeliveryTime> GetAllProductRegistryDeliveryTimeById(int id)
		{
			return Repository.GetAllProductRegistryDeliveryTimeById(id);
		}
	}
}