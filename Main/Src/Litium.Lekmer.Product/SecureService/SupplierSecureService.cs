﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class SupplierSecureService : ISupplierSecureService
	{
		protected SupplierRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public SupplierSecureService(SupplierRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual Collection<ISupplier> Search(string searchCriteria)
		{
			Repository.EnsureNotNull();
			return Repository.Search(searchCriteria);
		}

		public virtual Collection<ISupplier> GetAll()
		{
			Repository.EnsureNotNull();
			return Repository.GetAll();
		}

		public virtual ISupplier GetById(int id)
		{
			return Repository.GetById(id);
		}

		public virtual ISupplier GetByNo(string supplierNo)
		{
			return Repository.GetByNo(supplierNo);
		}

		public virtual void Save(ISystemUserFull systemUserFull, ISupplier supplier)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			Repository.Save(supplier);
		}
	}
}