﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Xml;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Cache;

namespace Litium.Lekmer.Product
{
	public class PackageSecureService : IPackageSecureService
	{
		protected PackageRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> ProductDescriptionTranslationSecureService { get; private set; }
		protected IProductRegistryRestrictionProductSecureService ProductRegistryRestrictionProductSecureService { get; private set; }
		protected ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> ProductWebShopTitleTranslationSecureService { get; private set; }


		public PackageSecureService(PackageRepository repository,
			ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> productDescriptionTranslationSecureService,
			IProductRegistryRestrictionProductSecureService productRegistryRestrictionProductSecureService,
			ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> productWebShopTitleTranslationSecureService,
			IAccessValidator accessValidator)
		{
			Repository = repository;
			ProductDescriptionTranslationSecureService = productDescriptionTranslationSecureService;
			ProductRegistryRestrictionProductSecureService = productRegistryRestrictionProductSecureService;
			ProductWebShopTitleTranslationSecureService = productWebShopTitleTranslationSecureService;
			AccessValidator = accessValidator;
		}


		public virtual IPackage Create()
		{
			var package = IoC.Resolve<IPackage>();
			package.MasterProduct = IoC.Resolve<IProductRecord>();
			package.Status = BusinessObjectStatus.New;
			return package;
		}

		public virtual void SavePackageProducts(ISystemUserFull systemUserFull, int packageId, IEnumerable<int> productIds)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);
			Repository.SavePackageProducts(packageId, Convert.ToStringIdentifierList(productIds), Convert.DefaultListSeparator);
		}

		public virtual void DeletePackageProducts(ISystemUserFull systemUserFull, int packageId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);
			Repository.DeletePackageProducts(packageId);
		}

		public virtual IPackage Save(ISystemUserFull systemUserFull,
			IPackage package,
			Collection<IPriceListItem> priceListItems,
			Collection<ITranslationGeneric> webShopTitleTranslations,
			Collection<ITranslationGeneric> descriptionTranslations,
			IEnumerable<ITranslationGeneric> generalInfoTranslations,
			Collection<IProductRegistryRestrictionItem> registryRestrictions)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);

			int masterProductId;

			using (var transactedOperation = new TransactedOperation())
			{
				package = Repository.Save(package, CreatePricesXml(priceListItems));
				masterProductId = package.MasterProductId;

				if (masterProductId > 0)
				{
					foreach (var descriptionTranslation in descriptionTranslations)
					{
						descriptionTranslation.Id = masterProductId;
					}
					foreach (var webShopTitleTranslation in webShopTitleTranslations)
					{
						webShopTitleTranslation.Id = masterProductId;
					}
					foreach (var registryRestriction in registryRestrictions)
					{
						registryRestriction.ItemId = masterProductId;
					}
				}
				ProductDescriptionTranslationSecureService.Save(systemUserFull, descriptionTranslations);
				ProductRegistryRestrictionProductSecureService.Save(systemUserFull, masterProductId, registryRestrictions);
				ProductWebShopTitleTranslationSecureService.Save(systemUserFull, webShopTitleTranslations);
				SaveTranslations(package.PackageId, generalInfoTranslations);

				transactedOperation.Complete();
			}

			if (masterProductId > 0)
			{
				ProductCache.Instance.Remove(masterProductId);
				ProductViewCache.Instance.Remove(masterProductId);
				PackageCache.Instance.Remove(masterProductId);
			}

			return package;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, IPackage package)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);
			if (package != null)
			{
				Repository.Delete(package.PackageId);

				ProductCache.Instance.Remove(package.MasterProductId);
				ProductViewCache.Instance.Remove(package.MasterProductId);
				PackageCache.Instance.Remove(package.MasterProductId);
			}
		}

		public virtual void SetOfflineByIncludeProduct(ISystemUserFull systemUserFull, int productId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);

			var updatedProductIds = Repository.SetOfflineByIncludeProduct(productId);

			foreach (var id in updatedProductIds)
			{
				ProductCache.Instance.Remove(id);
				ProductViewCache.Instance.Remove(id);
				PackageCache.Instance.Remove(id);
			}
		}

		public virtual PackageCollection Search(int channelId, IProductSearchCriteria searchCriteria, int page, int pageSize)
		{
			return Repository.Search(channelId, searchCriteria, page, pageSize);
		}

		public virtual IPackage GetByIdSecure(int channelId, int packageId)
		{
			return Repository.GetByIdSecure(channelId, packageId);
		}

		public virtual IPackage GetByProductIdSecure(int channelId, int productId)
		{
			return Repository.GetByMasterProductSecure(channelId, productId);
		}

		public Collection<ITranslationGeneric> GetAllGeneralInfoTranslationsByPackage(int id)
		{
			return Repository.GetAllGeneralInfoTranslationsByPackage(id);
		}


		protected virtual string CreatePricesXml(Collection<IPriceListItem> priceListItems)
		{
			var doc = new XmlDocument();
			XmlNode pricesNode = AddNode(doc, null, "prices");

			foreach (var priceListItem in priceListItems)
			{
				XmlNode priceNode = AddNode(doc, pricesNode, "price");
				AddAttribute(doc, priceNode, "priceListId", priceListItem.PriceListId.ToString(CultureInfo.InvariantCulture));
				AddAttribute(doc, priceNode, "priceIncludingVat", priceListItem.PriceIncludingVat.ToString(CultureInfo.InvariantCulture));
				AddAttribute(doc, priceNode, "vat", priceListItem.VatPercentage.ToString(CultureInfo.InvariantCulture));
			}

			return doc.OuterXml;
		}

		private XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName)
		{
			XmlNode node = xmlDocument.CreateElement(elementName);

			if (parentNode != null)
			{
				parentNode.AppendChild(node);
			}
			else
			{
				xmlDocument.AppendChild(node);
			}

			return node;
		}

		private void AddAttribute(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlAttribute attribute = xmlDocument.CreateAttribute(elementName);
			attribute.Value = elementValue;
			if (parentNode.Attributes != null)
			{
				parentNode.Attributes.Append(attribute);
			}
		}

		private void SaveTranslations(int packageId, IEnumerable<ITranslationGeneric> generalInfoTranslations)
		{
			foreach (var translation in generalInfoTranslations)
			{
				Repository.SaveTranslation(packageId, translation.LanguageId, translation.Value);
			}
		}
	}
}