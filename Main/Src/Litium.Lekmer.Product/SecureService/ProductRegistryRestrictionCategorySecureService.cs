﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductRegistryRestrictionCategorySecureService : IProductRegistryRestrictionCategorySecureService
	{
		protected ProductRegistryRestrictionCategoryRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductRegistryRestrictionCategorySecureService(ProductRegistryRestrictionCategoryRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual IProductRegistryRestrictionItem Create()
		{
			var productRegistryRestrictionCategory = IoC.Resolve<IProductRegistryRestrictionItem>();
			productRegistryRestrictionCategory.CreatedDate = DateTime.Now;
			productRegistryRestrictionCategory.Status = BusinessObjectStatus.New;
			return productRegistryRestrictionCategory;
		}

		public virtual void Save(ISystemUserFull systemUserFull, int categoryId, Collection<IProductRegistryRestrictionItem> productRegistryRestrictionCategoryList)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			if (productRegistryRestrictionCategoryList == null)
			{
				throw new NullReferenceException("productRegistryRestrictionCategoryList");
			}

			using (var transaction = new TransactedOperation())
			{
				Delete(systemUserFull, categoryId);

				foreach (var productRegistryRestrictionCategory in productRegistryRestrictionCategoryList)
				{
					Repository.Save(productRegistryRestrictionCategory);
				}

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int categoryId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);
			Repository.Delete(categoryId);
		}

		public Collection<IProductRegistryRestrictionItem> GetAllByCategory(int categoryId)
		{
			return Repository.GetAllByCategory(categoryId);
		}

		public Collection<IProductRegistryRestrictionItem> GetAllWithChannel()
		{
			return Repository.GetAllWithChannel();
		}
	}
}