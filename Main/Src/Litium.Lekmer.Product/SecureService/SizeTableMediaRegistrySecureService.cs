﻿using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class SizeTableMediaRegistrySecureService : ISizeTableMediaRegistrySecureService
	{
		protected SizeTableMediaRegistryRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public SizeTableMediaRegistrySecureService(
			SizeTableMediaRegistryRepository sizeTableMediaRegistryRepository,
			IAccessValidator accessValidator
			)
		{
			Repository = sizeTableMediaRegistryRepository;
			AccessValidator = accessValidator;
		}

		public virtual ISizeTableMediaRegistry Create()
		{
			var sizeTableMediaRegistry = IoC.Resolve<ISizeTableMediaRegistry>();

			sizeTableMediaRegistry.SetNew();

			return sizeTableMediaRegistry;
		}

		public virtual Collection<ISizeTableMediaRegistry> GetAllBySizeTable(int sizeTableId)
		{
			return Repository.GetAllBySizeTable(sizeTableId);
		}

		public virtual void Save(ISystemUserFull systemUserFull, Collection<ISizeTableMediaRegistry> sizeTableMediaRegistries)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				foreach (var sizeTableMediaRegistry in sizeTableMediaRegistries)
				{
					if (sizeTableMediaRegistry.IsDeleted)
					{
						Delete(systemUserFull, sizeTableMediaRegistry.Id);
					}
				}

				foreach (var sizeTableMediaRegistry in sizeTableMediaRegistries)
				{
					if (sizeTableMediaRegistry.IsDeleted == false)
					{
						Save(systemUserFull, sizeTableMediaRegistry);
					}
				}

				transaction.Complete();
			}
		}

		public virtual void Save(ISystemUserFull systemUserFull, ISizeTableMediaRegistry sizeTableMediaRegistry)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			int sizeTableMediaRegistryId = Repository.Save(sizeTableMediaRegistry);

			sizeTableMediaRegistry.Id = sizeTableMediaRegistryId;
			sizeTableMediaRegistry.SetUntouched();
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int sizeTableMediaRegistryId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			Repository.Delete(sizeTableMediaRegistryId);
		}
	}
}