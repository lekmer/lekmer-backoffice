﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductSizeSecureService : IProductSizeSecureService
	{
		protected ProductSizeRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductSizeSecureService(ProductSizeRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public Collection<IProductSize> GetAllByProduct(int productId)
		{
			return Repository.GetAllByProduct(productId);
		}

		public void Save(ISystemUserFull systemUserFull, IEnumerable<IProductSize> productSizes)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				foreach (var productSize in productSizes)
				{
					Repository.Save(productSize);
				}

				transactedOperation.Complete();
			}

			RemoveFromCache(productSizes);
		}

		protected virtual void RemoveFromCache(IEnumerable<IProductSize> productSizes)
		{
			foreach (var productSize in productSizes)
			{
				ProductSizeCache.Instance.Remove(new ProductSizeKey(productSize.ProductId, productSize.SizeInfo.Id));
			}

			var productIds = productSizes.Select(ps => ps.ProductId).Distinct();
			foreach (int productId in productIds)
			{
				ProductSizeCollectionCache.Instance.Remove(new ProductSizeCollectionKey(productId));
			}
		}
	}
}
