﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockProductImageListSecureService : IBlockProductImageListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductImageListRepository Repository { get; private set; }

		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockProductImageListSecureService(
			IAccessValidator accessValidator,
			BlockProductImageListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockProductImageList Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var block = IoC.Resolve<IBlockProductImageList>();
			block.AccessId = AccessSecureService.GetByCommonName("All").Id;
			block.Setting = BlockSettingSecureService.Create();
			block.Status = BusinessObjectStatus.New;
			return block;
		}

		public virtual IBlockProductImageList GetById(int blockId)
		{
			return Repository.GetByIdSecure(blockId);
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductImageList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				// Save IBlock.
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				// Save IBlockProductImageList.
				Repository.Save(block);

				// Save Setting.
				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			BlockProductImageListCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			IBlockProductImageList block = Create();
			block.ContentNodeId = contentNodeId;
			block.ContentAreaId = contentAreaId;
			block.BlockTypeId = blockTypeId;
			block.BlockStatusId = (int)BlockStatusInfo.Offline;
			block.Title = title;
			block.TemplateId = null;
			block.Id = Save(systemUserFull, block);
			return block;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockProductImageListCache.Instance.Remove(blockId);
		}
	}
}
