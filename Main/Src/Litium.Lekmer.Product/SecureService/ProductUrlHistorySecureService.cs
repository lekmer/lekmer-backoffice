﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductUrlHistorySecureService : IProductUrlHistorySecureService
	{
		protected ProductUrlHistoryRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductUrlHistorySecureService(ProductUrlHistoryRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public Collection<IProductUrlHistory> GetAllByProduct(int productId)
		{
			return Repository.GetAllByProduct(productId);
		}

		public IProductUrlHistory Create(int productId, int languageId, string urlTitle, int typeId)
		{
			var productUrlHistory = IoC.Resolve<IProductUrlHistory>();

			productUrlHistory.ProductId = productId;
			productUrlHistory.LanguageId = languageId;
			productUrlHistory.TypeId = typeId;
			productUrlHistory.UrlTitleOld = urlTitle;
			productUrlHistory.CreationDate = productUrlHistory.LastUsageDate = DateTime.Now;

			productUrlHistory.SetNew();

			return productUrlHistory;
		}

		public List<int> Save(ISystemUserFull systemUserFull, Collection<IProductUrlHistory> productUrlHistoryCollection)
		{
			var productUrlHistoryIds = new List<int>();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			if (productUrlHistoryCollection.Count == 0)
			{
				return productUrlHistoryIds;
			}

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (IProductUrlHistory productUrlHistory in productUrlHistoryCollection)
				{
					productUrlHistoryIds.Add(Repository.Save(productUrlHistory));

					ProductUrlHistoryCache.Instance.Remove(new ProductUrlHistoryKey(productUrlHistory.LanguageId, productUrlHistory.UrlTitleOld));
				}

				transactedOperation.Complete();
			}

			return productUrlHistoryIds;
		}

		public void DeleteById(ISystemUserFull systemUserFull, int productUrlHistoryId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			Repository.DeleteById(productUrlHistoryId);

			ProductUrlHistoryCache.Instance.Flush();
		}

		public void DeleteAllByProduct(ISystemUserFull systemUserFull, int productId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			Repository.DeleteAllByProduct(productId);

			ProductUrlHistoryCache.Instance.Flush();
		}

		public void DeleteAllByProductAndUrlTitle(ISystemUserFull systemUserFull, int productId, int languageId, string urlTitle)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			Repository.DeleteAllByProductAndUrlTitle(productId, languageId, urlTitle);

			ProductUrlHistoryCache.Instance.Remove(new ProductUrlHistoryKey(languageId, urlTitle));
		}
	}
}