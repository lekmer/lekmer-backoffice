﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class RecommendedPriceSecureService : IRecommendedPriceSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected RecommendedPriceRepository Repository { get; private set; }

		public RecommendedPriceSecureService(RecommendedPriceRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public Collection<IRecommendedPrice> GetAllByProduct(int productId)
		{
			return Repository.GetAllByProduct(productId);
		}

		public void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<IRecommendedPrice> recommendedPrices)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByProduct(productId);
				foreach (var recommendedPrice in recommendedPrices)
				{
					Repository.Save(recommendedPrice);
				}
				transactedOperation.Complete();
			}
		}
	}
}