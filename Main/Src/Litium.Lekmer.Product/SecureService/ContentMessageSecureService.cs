﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class ContentMessageSecureService : IContentMessageSecureService
	{
		protected ContentMessageRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public ContentMessageSecureService(ContentMessageRepository contentMessageRepository, IAccessValidator accessValidator, IChannelSecureService channelSecureService)
		{
			Repository = contentMessageRepository;
			AccessValidator = accessValidator;
			ChannelSecureService = channelSecureService;
		}

		public virtual IContentMessage Save(ISystemUserFull systemUserFull, IContentMessage contentMessage, Collection<IContentMessageTranslation> translations)
		{
			if (contentMessage == null)
			{
				throw new ArgumentNullException("contentMessage");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				int contentMessageId = Repository.Save(contentMessage);
				contentMessage.Id = contentMessageId;

				if (contentMessageId > 0)
				{
					// Save translations.
					SaveTranslations(contentMessageId, translations);

					// Save Includes.
					SaveIncludes(contentMessageId, contentMessage.IncludeProducts.Select(p => p.Key), contentMessage.IncludeCategories.Select(p => p.Key), contentMessage.IncludeBrands.Select(p => p.Key), contentMessage.IncludeSuppliers.Select(p => p.Key));
				}
				transaction.Complete();
			}

			ProductContentMessageCache.Instance.Flush();

			return contentMessage;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(id);
				transaction.Complete();
			}

			ProductContentMessageCache.Instance.Flush();
		}

		public virtual IContentMessage GetById(int id)
		{
			Repository.EnsureNotNull();
			return Repository.GetByIdSecure(id);
		}

		public virtual Collection<IContentMessage> GetAllByFolder(int folderId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllByFolder(folderId);
		}

		public virtual Collection<IContentMessage> Search(string searchCriteria)
		{
			Repository.EnsureNotNull();
			return Repository.Search(searchCriteria);
		}

		protected virtual string ConvertToString(IEnumerable<int> values)
		{
			return Convert.ToStringIdentifierList(values);
		}

		// Includes.

		public virtual IContentMessage GetAllIncludes(IContentMessage contentMessage)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllIncludes(contentMessage);
		}

		public virtual void SaveIncludes(int contentMessageId, IEnumerable<int> productIds, IEnumerable<int> categoryIds, IEnumerable<int> brandIds, IEnumerable<int> supplierIds)
		{
			Repository.SaveIncludes(contentMessageId, ConvertToString(productIds), ConvertToString(categoryIds), ConvertToString(brandIds), ConvertToString(supplierIds), Convert.DefaultListSeparator);
			ProductContentMessageCache.Instance.Flush();
		}

		public virtual void ClearIncludes(List<int> productIds, List<int> categoryIds, List<int> brandIds, List<int> supplierIds)
		{
			Repository.ClearIncludes(ConvertToString(productIds), ConvertToString(categoryIds), ConvertToString(brandIds), ConvertToString(supplierIds), Convert.DefaultListSeparator);
			ProductContentMessageCache.Instance.Flush();
		}

		// Translations.

		public virtual Collection<IContentMessageTranslation> GetAllTranslations(int contentMessageId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllTranslations(contentMessageId);
		}

		protected virtual void SaveTranslations(int contentMessageId, Collection<IContentMessageTranslation> translations)
		{
			foreach (var translation in translations)
			{
				translation.ContentMessageId = contentMessageId;
				Repository.SaveTranslations(translation);
			}
		}
	}
}