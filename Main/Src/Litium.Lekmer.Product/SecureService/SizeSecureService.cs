﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class SizeSecureService : ISizeSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected SizeRepository Repository { get; private set; }

		public SizeSecureService(IAccessValidator accessValidator, SizeRepository sizeRepository)
		{
			AccessValidator = accessValidator;
			Repository = sizeRepository;
		}

		public virtual ISize Create()
		{
			var size = IoC.Resolve<ISize>();
			size.Status = BusinessObjectStatus.New;
			return size;
		}
		
		public virtual Collection<ISize> GetAll()
		{
			return Repository.GetAll();
		}

		public virtual ISize GetById(int sizeId)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(sizeId);
		}

		public virtual void SetOrdinals(ISystemUserFull user, IEnumerable<ISize> sizes)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");
			if (sizes == null) throw new ArgumentNullException("sizes");

			AccessValidator.ForceAccess(user, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.SetOrdinals(sizes);
				transactedOperation.Complete();
			}

			RemoveFromCache();
		}

		public virtual int Save(ISystemUserFull user, ISize size)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");
			if (size == null) throw new ArgumentNullException("size");

			int result;
	
			AccessValidator.ForceAccess(user, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				result = Repository.Save(size);
				transactedOperation.Complete();
			}

			RemoveFromCache();

			return result;
		}

		public virtual int Delete(ISystemUserFull user, int sizeId)
		{
			int result;
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");
			AccessValidator.ForceAccess(user, PrivilegeConstant.AssortmentProduct);


			using (var transactedOperation = new TransactedOperation())
			{
				result = Repository.Delete(sizeId);
				transactedOperation.Complete();
			}

			return result;
		}

		protected virtual void RemoveFromCache()
		{
			SizeCollectionCache.Instance.Flush();
		}
	}
}
