﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockSharedWishListSecureService : IBlockSharedWishListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockSharedWishListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockSharedWishListSecureService(
			IAccessValidator accessValidator,
			BlockSharedWishListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockSharedWishList Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockSharedWishList = IoC.Resolve<IBlockSharedWishList>();
			blockSharedWishList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockSharedWishList.Setting = BlockSettingSecureService.Create();
			blockSharedWishList.Status = BusinessObjectStatus.New;
			return blockSharedWishList;
		}

		public virtual IBlockSharedWishList GetById(int id)
		{
			return Repository.GetByIdSecure((id));
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			IBlockSharedWishList blockSharedWishList = Create();
			blockSharedWishList.ContentNodeId = contentNodeId;
			blockSharedWishList.ContentAreaId = contentAreaId;
			blockSharedWishList.BlockTypeId = blockTypeId;
			blockSharedWishList.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockSharedWishList.Title = title;
			blockSharedWishList.TemplateId = null;
			blockSharedWishList.Id = Save(systemUserFull, blockSharedWishList);
			return blockSharedWishList;
		}
		public virtual int Save(ISystemUserFull systemUserFull, IBlockSharedWishList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			BlockSharedWishListCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				transactedOperation.Complete();
			}
		}
	}
}
