﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockBrandListSecureService : IBlockBrandListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockBrandListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IBlockBrandListBrandSecureService BlockBrandListBrandSecureService { get; private set; }

		public BlockBrandListSecureService(
			IAccessValidator accessValidator,
			BlockBrandListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IBlockBrandListBrandSecureService blockBrandListBrandSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			BlockBrandListBrandSecureService = blockBrandListBrandSecureService;
		}

		public IBlockBrandList Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockBrandList = IoC.Resolve<IBlockBrandList>();
			blockBrandList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockBrandList.Setting = BlockSettingSecureService.Create();
			blockBrandList.Status = BusinessObjectStatus.New;
			return blockBrandList;
		}

		public IBlockBrandList GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockBrandList = Create();
			blockBrandList.ContentNodeId = contentNodeId;
			blockBrandList.ContentAreaId = contentAreaId;
			blockBrandList.BlockTypeId = blockTypeId;
			blockBrandList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockBrandList.IncludeAllBrands = true;
			blockBrandList.Title = title;
			blockBrandList.TemplateId = null;
			blockBrandList.Id = Save(systemUserFull, blockBrandList);
			return blockBrandList;
		}

		private int Save(ISystemUserFull systemUserFull, IBlockBrandList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			return block.Id;
		}

		public int Save(ISystemUserFull systemUserFull, IBlockBrandList block, Collection<IBlockBrandListBrand> blockBrands)
		{
			if (BlockBrandListBrandSecureService == null)
				throw new InvalidOperationException("BlockBrandListBrandSecureService must be set before calling Save.");

			using (var transactedOperation = new TransactedOperation())
			{
				Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				BlockBrandListBrandSecureService.Save(block.Id, blockBrands);
				transactedOperation.Complete();
			}
			BlockBrandListBrandCollectionCache.Instance.Flush();
			BlockBrandListCache.Instance.Remove(block.Id);
			return block.Id;
		}


		public void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockBrandListBrandCollectionCache.Instance.Flush();
			BlockBrandListCache.Instance.Remove(blockId);
		}
	}
}