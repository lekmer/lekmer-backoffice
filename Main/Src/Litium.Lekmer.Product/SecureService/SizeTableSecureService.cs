﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class SizeTableSecureService : ISizeTableSecureService
	{
		protected SizeTableRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }
		protected ISizeTableMediaSecureService SizeTableMediaSecureService { get; private set; }

		public SizeTableSecureService(
			SizeTableRepository sizeTableRepository,
			IAccessValidator accessValidator,
			IChannelSecureService channelSecureService,
			ISizeTableMediaSecureService sizeTableMediaSecureService)
		{
			Repository = sizeTableRepository;
			AccessValidator = accessValidator;
			ChannelSecureService = channelSecureService;
			SizeTableMediaSecureService = sizeTableMediaSecureService;
		}

		public virtual ISizeTable Create()
		{
			var sizeTable = IoC.Resolve<ISizeTable>();

			sizeTable.SetNew();

			return sizeTable;
		}

		public virtual ISizeTable Save(ISystemUserFull systemUserFull, ISizeTable sizeTable, Collection<ISizeTableTranslation> translations, Collection<ISizeTableMediaRecord> mediaItems)
		{
			if (sizeTable == null)
			{
				throw new ArgumentNullException("sizeTable");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				int sizeTableId = Repository.Save(sizeTable);
				sizeTable.Id = sizeTableId;

				if (sizeTableId > 0)
				{
					// Save translations.
					SaveTranslations(sizeTableId, translations);

					// Save Includes.
					SaveIncludes(sizeTableId, sizeTable.IncludeProducts.Select(p => p.Key), sizeTable.IncludeCategoryBrandPairs);

					// Save Media Items.
					SaveMediaItems(systemUserFull, sizeTableId, mediaItems);
				}
				transaction.Complete();
			}

			SizeTableCache.Instance.Remove(sizeTable.Id, ChannelSecureService.GetAll());

			return sizeTable;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int sizeTableId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(sizeTableId);
				transaction.Complete();
			}

			SizeTableCache.Instance.Remove(sizeTableId, ChannelSecureService.GetAll());
		}

		public virtual ISizeTable GetById(int id)
		{
			Repository.EnsureNotNull();
			return Repository.GetByIdSecure(id);
		}

		public virtual Collection<ISizeTable> GetAllByFolder(int folderId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllByFolder(folderId);
		}

		public virtual Collection<ISizeTable> GetAll()
		{
			Repository.EnsureNotNull();
			return Repository.GetAll();
		}

		public virtual Collection<ISizeTable> Search(string searchCriteria)
		{
			Repository.EnsureNotNull();
			return Repository.Search(searchCriteria);
		}

		// Includes.

		public virtual ISizeTable GetAllIncludes(ISizeTable sizeTable)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllIncludes(sizeTable);
		}

		public virtual void SaveIncludes(int sizeTableId, IEnumerable<int> productIds, Collection<ISizeTableCategoryBrandPair> categoryBrandPairs)
		{
			var categoryIds = new Collection<int>();
			var brandIds = new Collection<int?>();
			foreach (var categoryBrandPair in categoryBrandPairs)
			{
				categoryIds.Add(categoryBrandPair.CategoryId);
				brandIds.Add(categoryBrandPair.BrandId);
			}

			Repository.SaveIncludes(sizeTableId, Convert.ToStringIdentifierList(productIds), Convert.ToStringIdentifierList(categoryIds), Convert.ToStringIdentifierList(brandIds), Convert.DefaultListSeparator);
			ProductSizeTableIdCache.Instance.Flush();
		}

		// Translations.

		public virtual Collection<ISizeTableTranslation> GetAllTranslations(int sizeTableId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllTranslations(sizeTableId);
		}

		protected virtual void SaveTranslations(int sizeTableId, Collection<ISizeTableTranslation> translations)
		{
			foreach (var translation in translations)
			{
				translation.SizeTableId = sizeTableId;
				Repository.SaveTranslations(translation);
			}
		}

		// Media.

		protected virtual void SaveMediaItems(ISystemUserFull systemUserFull, int sizeTableId, Collection<ISizeTableMediaRecord> mediaItems)
		{
			foreach (var mediaItem in mediaItems)
			{
				mediaItem.SizeTableId = sizeTableId;
			}
			
			SizeTableMediaSecureService.Save(systemUserFull, mediaItems);
		}
	}
}