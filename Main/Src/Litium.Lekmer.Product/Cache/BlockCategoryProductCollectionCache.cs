﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockCategoryProductCollectionCache : ScensumCacheBase<BlockCategoryProductCollectionKey, ProductIdCollection>
	{
		private static readonly BlockCategoryProductCollectionCache _instance = new BlockCategoryProductCollectionCache();

		private BlockCategoryProductCollectionCache()
		{
		}

		public static BlockCategoryProductCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class BlockCategoryProductCollectionKey : ICacheKey
	{
		public BlockCategoryProductCollectionKey(int channelId, int blockId, int pageNumber, int pageSize)
		{
			ChannelId = channelId;
			BlockId = blockId;
			PageNumber = pageNumber;
			PageSize = pageSize;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public int PageNumber { get; set; }

		public int PageSize { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture) + "-" +
					PageNumber.ToString(CultureInfo.InvariantCulture) + "-" +
					PageSize.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
