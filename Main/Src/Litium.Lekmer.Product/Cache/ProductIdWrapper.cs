namespace Litium.Lekmer.Product.Cache
{
	public class ProductIdWrapper
	{
		public ProductIdWrapper(int? productId)
		{
			ProductId = productId;
		}

		public int? ProductId { get; set; }
	}
}