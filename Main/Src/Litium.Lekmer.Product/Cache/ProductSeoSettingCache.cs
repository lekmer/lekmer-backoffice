﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductSeoSettingCache : ScensumCacheBase<ProductSeoSettingKey, IProductSeoSetting>
	{
		private static readonly ProductSeoSettingCache _instance = new ProductSeoSettingCache();

		private ProductSeoSettingCache()
		{
		}

		public static ProductSeoSettingCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int productId)
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			foreach (var channel in channels)
			{
				Remove(new ProductSeoSettingKey(channel.Id, productId));
			}
		}
	}

	public class ProductSeoSettingKey : ICacheKey
	{
		public ProductSeoSettingKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public int ChannelId { get; set; }
		public int ProductId { get; set; }

		public string Key
		{
			get
			{
				return string.Format("{0}-{1}", ChannelId.ToString(CultureInfo.InvariantCulture), ProductId);
			}
		}
	}
}