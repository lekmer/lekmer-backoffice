﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductSizeCache : ScensumCacheBase<ProductSizeKey, IProductSize>
	{
		// Singleton

		private static readonly ProductSizeCache _instance = new ProductSizeCache();

		private ProductSizeCache()
		{
		}

		public static ProductSizeCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton
	}

	public class ProductSizeKey : ICacheKey
	{
		public ProductSizeKey(int productId, int sizeId)
		{
			ProductId = productId;
			SizeId = sizeId;
		}

		public int ProductId { get; set; }

		public int SizeId { get; set; }

		public string Key
		{
			get { return string.Format("ProductSize_{0}_{1}", ProductId, SizeId); }
		}
	}
}
