﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockSharedWishListCache : ScensumCacheBase<BlockSharedWishListKey, IBlockSharedWishList>
	{
		private static readonly BlockSharedWishListCache _instance = new BlockSharedWishListCache();

		private BlockSharedWishListCache()
		{
		}

		public static BlockSharedWishListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockSharedWishListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockSharedWishListKey : ICacheKey
	{
		public BlockSharedWishListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
