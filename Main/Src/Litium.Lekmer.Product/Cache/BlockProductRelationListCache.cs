﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IBlockProductRelationList"/>.
	/// </summary>
	public sealed class BlockProductRelationListCache : ScensumCacheBase<BlockProductRelationListKey, IBlockProductRelationList>
	{
		private static readonly BlockProductRelationListCache _instance = new BlockProductRelationListCache();

		private BlockProductRelationListCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static BlockProductRelationListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockProductRelationListKey(channel.Id, blockId));
			}
		}
	}

	/// <summary>
	/// Key used for caching <see cref="IBlockProductRelationList"/>.
	/// </summary>
	public class BlockProductRelationListKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="channelId">Channel id of the <see cref="IBlockProductRelationList"/>.</param>
		/// <param name="blockId">Id of the <see cref="IBlockProductRelationList"/>.</param>
		public BlockProductRelationListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		/// <summary>
		/// Channel id of the <see cref="IBlockProductRelationList"/>.
		/// </summary>
		public int ChannelId { get; set; }

		/// <summary>
		/// Id of the <see cref="IBlockProductRelationList"/>.
		/// </summary>
		public int BlockId { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
