﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductUrlIdCache : ScensumCacheBase<ProductUrlIdKey, ProductIdWrapper>
	{
		#region Singleton

		private static readonly ProductUrlIdCache _instance = new ProductUrlIdCache();

		private ProductUrlIdCache()
		{
		}

		public static ProductUrlIdCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class ProductUrlIdKey : ICacheKey
	{
		public ProductUrlIdKey(int channelId, string urlTitle)
		{
			ChannelId = channelId;
			UrlTitle = urlTitle;
		}

		public int ChannelId { get; set; }
		public string UrlTitle { get; set; }

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + UrlTitle; }
		}
	}
}