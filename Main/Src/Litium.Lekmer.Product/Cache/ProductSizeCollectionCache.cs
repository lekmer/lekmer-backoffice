﻿using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{

	public sealed class ProductSizeCollectionCache : ScensumCacheBase<ProductSizeCollectionKey, Collection<IProductSize>>
	{
		// Singleton

		private static readonly ProductSizeCollectionCache _instance = new ProductSizeCollectionCache();

		private ProductSizeCollectionCache()
		{
		}

		public static ProductSizeCollectionCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton
	}

	public class ProductSizeCollectionKey : ICacheKey
	{
		public ProductSizeCollectionKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return string.Format("ProductSizeCollection_{0}", ProductId); }
		}
	}
}
