using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public class AgeIntervalListCache : ScensumCacheBase<AgeIntervalListKey, Collection<IAgeInterval>>
	{
		private static readonly AgeIntervalListCache _ageIntervalListCache = new AgeIntervalListCache();

		private AgeIntervalListCache()
		{
		}

		public static AgeIntervalListCache Instance
		{
			get { return _ageIntervalListCache; }
		}
	}

	public class AgeIntervalListKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public AgeIntervalListKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}