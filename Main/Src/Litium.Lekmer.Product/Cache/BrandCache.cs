﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BrandCache : ScensumCacheBase<BrandKey, IBrand>
	{
		// Singleton

		private static readonly BrandCache _instance = new BrandCache();

		private BrandCache()
		{
		}

		public static BrandCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton

		public void Remove(int brandId, IEnumerable<IChannel> channels)
		{
			foreach (IChannel channel in channels)
			{
				Remove(new BrandKey(channel.Id, brandId));
			}
		}
	}

	public class BrandKey : ICacheKey
	{
		public BrandKey(int channelId, int brandId)
		{
			ChannelId = channelId;
			BrandId = brandId;
		}

		public int ChannelId { get; set; }

		public int BrandId { get; set; }

		public string Key
		{
			get { return string.Format("c_{0}-id_{1}", ChannelId, BrandId); }
		}
	}
}