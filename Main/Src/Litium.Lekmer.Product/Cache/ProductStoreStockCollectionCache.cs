﻿using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{

	public sealed class ProductStoreStockCollectionCache : ScensumCacheBase<ProductStoreStockCollectionKey, Collection<IProductStoreStock>>
	{
		// Singleton

		private static readonly ProductStoreStockCollectionCache _instance = new ProductStoreStockCollectionCache();

		private ProductStoreStockCollectionCache()
		{
		}

		public static ProductStoreStockCollectionCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton
	}

	public class ProductStoreStockCollectionKey : ICacheKey
	{
		public ProductStoreStockCollectionKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return string.Format("ProductStoreStockCollection_{0}", ProductId); }
		}
	}
}
