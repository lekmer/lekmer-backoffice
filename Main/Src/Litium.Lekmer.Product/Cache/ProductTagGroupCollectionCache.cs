﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductTagGroupCollectionCache : ScensumCacheBase<ProductTagGroupCollectionKey, Collection<ITagGroup>>
	{
		#region Singleton

		private static readonly ProductTagGroupCollectionCache _instance = new ProductTagGroupCollectionCache();

		private ProductTagGroupCollectionCache()
		{
		}

		public static ProductTagGroupCollectionCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int productId)
		{
			foreach (var channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new ProductTagGroupCollectionKey(channel.Id, productId));
			}
		}

		#endregion
	}

	public class ProductTagGroupCollectionKey : ICacheKey
	{
		public int ChannelId { get; set; }
		public int ProductId { get; set; }

		public ProductTagGroupCollectionKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}