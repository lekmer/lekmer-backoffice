﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductTicketCache : ScensumCacheBase<ProductTicketKey, string>
	{
		// Singleton

		private static readonly ProductTicketCache _instance = new ProductTicketCache();

		private ProductTicketCache()
		{
		}

		public static ProductTicketCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton
	}

	public class ProductTicketKey : ICacheKey
	{
		public ProductTicketKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}
