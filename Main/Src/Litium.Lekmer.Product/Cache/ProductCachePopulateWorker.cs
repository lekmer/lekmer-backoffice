using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Timers;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;
using Timer = System.Timers.Timer;

namespace Litium.Lekmer.Product.Cache
{
	public static class ProductCachePopulateWorker
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly BackgroundWorker _worker = CreateBackgroundWorker();
		private static readonly Timer _timer = CreateTimer();
		private static TimeSpan _refreshInterval;
		private static IUserContext _context;
		private static int _populateCount;
		private static bool _started;
		private static readonly object _startedLockObject = new object();

		private static void OnRefresh(object sender, DoWorkEventArgs e)
		{
			if (_context == null) return;

			try
			{
				if (LekmerProductCacheSetting.Instance.ProductCachePopulateWorkerClosed)
				{
					return;
				}

				var stopwatch = Stopwatch.StartNew();

				PopulateCache(_context);

				stopwatch.Stop();
				_log.InfoFormat("Cache population took {0} seconds.", stopwatch.Elapsed.TotalSeconds);

				TimeSpan waitTime = LekmerProductCacheSetting.Instance.ExpirationInMinutes;
				_timer.Interval = waitTime.TotalMilliseconds;
				_timer.Start();
			}
			catch (Exception ex)
			{
				_log.Error("Cache population failed.", ex);
			}
		}

		private static void PopulateCache(IUserContext context)
		{
			TimeSpan readPageBreakDuration = LekmerProductCacheSetting.Instance.ReadPagePauseDuration;

			var productService = (ILekmerProductService) IoC.Resolve<IProductService>();

			int previouslyFetched = 0;
			int page = 0;
			const int pageSize = 100;

			for (int i = 0; i < Math.Ceiling((decimal) _populateCount/pageSize); i++)
			{
				int fetch = Math.Min(pageSize, _populateCount - previouslyFetched);

				page++;
				ProductCollection products = productService.GetAll(context, page, fetch);

				if (products.Count == 0) break;

				previouslyFetched += products.Count;

				if (previouslyFetched >= _populateCount) break;

				//we have one productCachePapulateWorker proces per channel and there is a risk that they start to query the database at the same time
				//because of this we make the thread to sleep a certain amount of time to give the DB server more time 
				Thread.Sleep((int) readPageBreakDuration.TotalMilliseconds);
			}
		}

		#region Background worker

		public static void StartIfNotStarted(IUserContext context, int populateCount, TimeSpan refreshInterval, TimeSpan additionalTimeToWait)
		{
			if (!_started)
			{
				lock (_startedLockObject)
				{
					if (!_started)
					{
						Start(context, populateCount, refreshInterval, additionalTimeToWait);
						_started = true;
					}
				}
			}
		}

		public static void Start(IUserContext context, int populateCount, TimeSpan refreshInterval, TimeSpan additionalTimeToWait)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (populateCount <= 0) throw new ArgumentOutOfRangeException("populateCount", populateCount, "populateCount must be larger then 0.");
			if (refreshInterval.TotalMilliseconds <= 0) throw new ArgumentOutOfRangeException("refreshInterval", refreshInterval, "refreshInterval must be larger then 0.");

			additionalTimeToWait = new TimeSpan(0,0,0,0);
			_refreshInterval = new TimeSpan(0,0,1,0);

			_context = context;
			_populateCount = populateCount;
			_refreshInterval = refreshInterval;

			_timer.Interval = _refreshInterval.TotalMilliseconds;
			_timer.Start();

			// Run it on startup, but delay it by the randomized additional time.
			new Thread(() =>
			{
				Thread.Sleep((int)additionalTimeToWait.TotalMilliseconds);
				RunWorker();
			}).Start();
		}

		private static void RunWorker()
		{
			if (!_worker.IsBusy)
			{
				_worker.RunWorkerAsync();
			}
		}

		private static BackgroundWorker CreateBackgroundWorker()
		{
			var worker = new BackgroundWorker();
			worker.DoWork += OnRefresh;

			return worker;
		}

		private static Timer CreateTimer()
		{
			var timer = new Timer();
			timer.Stop();
			timer.Elapsed += OnTimerElapsed;
			return timer;
		}

		private static void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			RunWorker();
		}

		#endregion
	}
}