﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Framework.Setting;

namespace Litium.Lekmer.Product.Setting
{
	public abstract class LekmerProductCacheSettingBase:SettingBase
	{
		/// <summary>
		/// Gets a setting value.
		/// </summary>
		/// <param name="group">Group where the setting is.</param>
		/// <param name="name">Name of the setting.</param>
		/// <param name="throwExceptionWhenNotFound">
		/// If set to true an exception will be thrown if the setting can't be found.
		/// If set to false, null will be returned.
		/// </param>
		/// <exception cref="ConfigurationErrorsException">Thrown when group or setting not found and throwExceptionWhenNotFound set to true.</exception>
		/// <returns>Value of the setting.</returns>
		protected override string GetValue(string group, string name, bool throwExceptionWhenNotFound)
		{
			if (group == null) throw new ArgumentNullException("group");
			if (name == null) throw new ArgumentNullException("name");

			if (name=="ExpirationInMinutes")
			{
				var repository = base.CreateRepositoryInstance();
				return repository.GetValue(StorageName, group, name, throwExceptionWhenNotFound);
			}
			return base.GetValue(group, name, throwExceptionWhenNotFound);
		}
	}
}
