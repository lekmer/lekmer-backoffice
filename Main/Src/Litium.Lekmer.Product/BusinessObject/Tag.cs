using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Tag : ITag
	{
		public int Id { get; set; }
		public int TagGroupId { get; set; }
		public string Value { get; set; }
		public string CommonName { get; set; }
		public int? FlagId { get; set; }
	}
}