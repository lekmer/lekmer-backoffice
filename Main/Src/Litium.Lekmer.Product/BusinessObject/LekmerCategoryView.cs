﻿using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerCategoryView : LekmerCategory, ILekmerCategoryView
	{
		private int? _productParentContentNodeId;
		private int? _productTemplateContentNodeId;

		public int? ProductParentContentNodeId
		{
			get { return _productParentContentNodeId; }
			set
			{
				CheckChanged(_productParentContentNodeId, value);
				_productParentContentNodeId = value;
			}
		}

		public int? ProductTemplateContentNodeId
		{
			get { return _productTemplateContentNodeId; }
			set
			{
				CheckChanged(_productTemplateContentNodeId, value);
				_productTemplateContentNodeId = value;
			}
		}
	}
}