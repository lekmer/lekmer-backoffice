﻿using System;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerProductIndexItem : ProductIndexItem, ILekmerProductIndexItem
	{
		public string LekmerErpId
		{
			get;
			set;
		}
	}
}
