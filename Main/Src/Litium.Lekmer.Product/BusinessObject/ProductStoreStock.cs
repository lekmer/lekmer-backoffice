﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductStoreStock : BusinessObjectBase, IProductStoreStock
	{
		private int _productId;
		private int _numberInStock;
		private ISize _sizeInfo;

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int NumberInStock
		{
			get { return _numberInStock; }
			set
			{
				CheckChanged(_numberInStock, value);
				_numberInStock = value;
			}
		}

		public ISize SizeInfo
		{
			get { return _sizeInfo; }
			set
			{
				CheckChanged(_sizeInfo, value);
				_sizeInfo = value;
			}
		}
	}
}
