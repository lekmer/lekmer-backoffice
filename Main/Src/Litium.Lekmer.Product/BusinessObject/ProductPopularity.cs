using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductPopularity : BusinessObjectBase, IProductPopularity
	{
		private int _channelId;
		private int _productId;
		private int _popularity;
		private int _salesAmount;

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int Popularity
		{
			get { return _popularity; }
			set
			{
				CheckChanged(_popularity, value);
				_popularity = value;
			}
		}

		public int SalesAmount
		{
			get { return _salesAmount; }
			set
			{
				CheckChanged(_salesAmount, value);
				_salesAmount = value;
			}
		}
	}
}