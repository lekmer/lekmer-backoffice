using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductTagHash : IProductTagHash
	{
		private Dictionary<int, Collection<IProductTag>> _tagsByProduct;

		private readonly Dictionary<int, Dictionary<int, ITag>> _tagsByChannel = new Dictionary<int, Dictionary<int, ITag>>();
		private readonly Dictionary<int, Dictionary<int, ITagGroup>> _tagGroupsByChannel = new Dictionary<int, Dictionary<int, ITagGroup>>();


		public virtual void Add(Collection<IProductTag> productTags)
		{
			_tagsByProduct = productTags.GroupBy(pt => pt.ProductId).ToDictionary(g => g.Key, g => new Collection<IProductTag>(g.ToArray()));
		}

		public virtual void Add(int channelId, Collection<ITagGroup> tagGroups)
		{
			_tagGroupsByChannel[channelId] = tagGroups.ToDictionary(tg => tg.Id);
			_tagsByChannel[channelId] = tagGroups.SelectMany(tagGroup => tagGroup.Tags).ToDictionary(t => t.Id);
		}

		public virtual Collection<ITagGroup> GetProductTags(int channelId, int productId)
		{
			var productTagGroupsByTagGroup = new Dictionary<int, ITagGroup>();

			Dictionary<int, ITag> tags = _tagsByChannel[channelId];
			Dictionary<int, ITagGroup> tagGroups = _tagGroupsByChannel[channelId];

			if (_tagsByProduct.ContainsKey(productId))
			{
				Collection<IProductTag> productTags = _tagsByProduct[productId];

				foreach (IProductTag productTag in productTags)
				{
					ITag tag = tags[productTag.TagId];
					ITagGroup tagGroup = tagGroups[tag.TagGroupId];

					ITagGroup productTaGroup;

					if (productTagGroupsByTagGroup.ContainsKey(tagGroup.Id))
					{
						productTaGroup = productTagGroupsByTagGroup[tagGroup.Id];
					}
					else
					{
						productTaGroup = CloneTagGroup(tagGroup); // Origin tag group has list of all tags. We need only product specific tags.
						productTagGroupsByTagGroup[tagGroup.Id] = productTaGroup;
					}

					productTaGroup.Tags.Add(tag);
				}
			}

			return new Collection<ITagGroup>(productTagGroupsByTagGroup.Values.ToArray());
		}

		public virtual Dictionary<int, Collection<ITag>> GetChannelTags()
		{
			return _tagsByChannel.ToDictionary(t => t.Key, t => new Collection<ITag>(t.Value.Values.ToArray()));
		}

		public virtual bool ContainProductTags()
		{
			return _tagsByProduct != null && _tagsByProduct.Count > 0;
		}

		public virtual bool ContainChannel(int channelId)
		{
			return _tagsByChannel.ContainsKey(channelId);
		}

		/// <summary>
		/// Origin tag group has list of all tags. We need only product specific tags.
		/// So we clone tag group, without it's own tags
		/// </summary>
		/// <param name="tagGroup"></param>
		/// <returns></returns>
		protected virtual ITagGroup CloneTagGroup(ITagGroup tagGroup)
		{
			var newTagGroup = IoC.Resolve<ITagGroup>();

			newTagGroup.Id = tagGroup.Id;
			newTagGroup.CommonName = tagGroup.CommonName;
			newTagGroup.Title = tagGroup.Title;
			newTagGroup.Tags = new Collection<ITag>();

			return newTagGroup;
		}
	}
}