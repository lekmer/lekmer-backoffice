﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockBrandProductList : LekmerBlockBase, IBlockBrandProductList
	{
		private int _productSorterOrderId;
		private IProductSortOrder _productSortOrder;
		private IBlockSetting _setting;

		public int ProductSortOrderId
		{
			get { return _productSorterOrderId; }
			set
			{
				CheckChanged(_productSorterOrderId, value);
				_productSorterOrderId = value;
			}
		}
		public IProductSortOrder ProductSortOrder
		{
			get { return _productSortOrder; }
			set
			{
				CheckChanged(_productSortOrder, value);
				_productSortOrder = value;
			}
		}
		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}