﻿using System;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockImageRotator : LekmerBlockBase, IBlockImageRotator
	{
		private int? _secondaryTemplateId;
		public int? SecondaryTemplateId
		{
			get { return _secondaryTemplateId; }
			set
			{
				CheckChanged(_secondaryTemplateId, value);
				_secondaryTemplateId = value;
			}
		}
	}
}