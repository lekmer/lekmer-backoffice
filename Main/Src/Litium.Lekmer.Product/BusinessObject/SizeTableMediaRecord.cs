﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableMediaRecord : BusinessObjectBase, ISizeTableMediaRecord
	{
		private int _id;
		private int _sizeTableId;
		private int _mediaId;
		private IMediaItem _mediaItem;
		private Collection<ISizeTableMediaRegistry> _mediaRegistries;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int SizeTableId
		{
			get { return _sizeTableId; }
			set
			{
				CheckChanged(_sizeTableId, value);
				_sizeTableId = value;
			}
		}

		public int MediaId
		{
			get { return _mediaId; }
			set
			{
				CheckChanged(_mediaId, value);
				_mediaId = value;
			}
		}

		public IMediaItem MediaItem
		{
			get { return _mediaItem; }
			set
			{
				CheckChanged(_mediaItem, value);
				_mediaItem = value;
			}
		}

		public Collection<ISizeTableMediaRegistry> MediaRegistries
		{
			get { return _mediaRegistries; }
			set
			{
				CheckChanged(_mediaRegistries, value);
				_mediaRegistries = value;
			}
		}
	}
}