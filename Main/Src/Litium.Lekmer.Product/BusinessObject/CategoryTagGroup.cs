using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class CategoryTagGroup : ICategoryTagGroup
	{
		public int CategoryId { get; set; }
		public int TagGroupId { get; set; }
	}
}