﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockBrandList : BlockBase, IBlockBrandList
	{
		private bool _includeAllBrands;
		private int? _linkContentNodeId;
		private IBlockSetting _setting;

		public bool IncludeAllBrands
		{
			get { return _includeAllBrands; }
			set
			{
				CheckChanged(_includeAllBrands, value);
				_includeAllBrands = value;
			}
		}

		public int? LinkContentNodeId
		{
			get { return _linkContentNodeId; }
			set
			{
				CheckChanged(_linkContentNodeId, value);
				_linkContentNodeId = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}