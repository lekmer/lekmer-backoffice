using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Color : IColor
	{
		public int Id { get; set; }
		public string HyErpId { get; set; }
		public string Value { get; set; }
		public string CommonName { get; set; }
	}
}