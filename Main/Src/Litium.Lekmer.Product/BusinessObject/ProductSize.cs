﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductSize : BusinessObjectBase, IProductSize
	{
		private int _productId;
		private string _erpId;
		private int _numberInStock;
		private int? _millimeterDeviation;
		private decimal? _overrideEu;
		private int? _overrideMillimeter;
		private decimal? _weight;
		private int _stockStatusId;
		private ISize _sizeInfo;

		public int ProductId
		{
			get { return _productId; }
			set
			{ 
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public string ErpId
		{
			get { return _erpId; }
			set
			{
				CheckChanged(_erpId, value);
				_erpId = value;
			}
		}

		public int NumberInStock
		{
			get { return _numberInStock; }
			set
			{
				CheckChanged(_numberInStock, value);
				_numberInStock = value;
			}
		}

		public int? MillimeterDeviation
		{
			get { return _millimeterDeviation; }
			set
			{
				CheckChanged(_millimeterDeviation, value);
				_millimeterDeviation = value;
			}
		}

		public decimal? OverrideEu
		{
			get { return _overrideEu; }
			set
			{
				CheckChanged(_overrideEu, value);
				_overrideEu = value;
			}
		}

		public int? OverrideMillimeter
		{
			get { return _overrideMillimeter; }
			set
			{
				CheckChanged(_overrideMillimeter, value);
				_overrideMillimeter = value;
			}
		}

		public bool IsBuyable
		{
			get { return NumberInStock > 0; }
		}

		public decimal? Weight
		{
			get { return _weight; }
			set
			{
				CheckChanged(_weight, value);
				_weight = value;
			}
		}

		public int StockStatusId
		{
			get { return _stockStatusId; }
			set
			{
				CheckChanged(_stockStatusId, value);
				_stockStatusId = value;
			}
		}

		public ISize SizeInfo
		{
			get { return _sizeInfo; }
			set
			{
				CheckChanged(_sizeInfo, value);
				_sizeInfo = value;
			}
		}
	}
}
