﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductMedia : BusinessObjectBase, IProductMedia
	{
		private int _productId;
		private int _mediaId;
		private int _groupId;
		private int _formatId;
		private int _ordinal;
		private string _title;
		private string _fileName;
		private string _extension;

		public int ProductId
		{
			get
			{
				return _productId;
			}
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int MediaId
		{
			get
			{
				return _mediaId;
			}
			set
			{
				CheckChanged(_mediaId, value);
				_mediaId = value;
			}
		}

		public int GroupId
		{
			get
			{
				return _groupId;
			}
			set
			{
				CheckChanged(_groupId, value);
				_groupId = value;
			}
		}

		public int Ordinal
		{
			get
			{
				return _ordinal;
			}
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}

		public int FormatId
		{
			get { return _formatId; }
			set
			{
				CheckChanged(_formatId, value);
				_formatId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string FileName
		{
			get { return _fileName; }
			set
			{
				CheckChanged(_fileName, value);
				_fileName = value;
			}
		}
		public string Extension
		{
			get { return _extension; }
			set
			{
				CheckChanged(_fileName, value);
				_extension = value;
			}
		}
	}
}
