using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class TagGroup : ITagGroup
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string CommonName { get; set; }

		public Collection<ITag> Tags { get; set; }
	}
}