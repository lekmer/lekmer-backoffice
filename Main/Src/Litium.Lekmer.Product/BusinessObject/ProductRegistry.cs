using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductRegistry : BusinessObjectBase, IProductRegistry
	{
		private int _productRegistryId;
		private string _title;

		public int ProductRegistryId
		{
			get { return _productRegistryId; }
			set
			{
				CheckChanged(_productRegistryId, value);
				_productRegistryId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
	}
}