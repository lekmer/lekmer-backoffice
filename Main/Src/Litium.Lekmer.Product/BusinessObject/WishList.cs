﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class WishList : BusinessObjectBase, IWishList
	{
		private int _id;
		private Guid _key;
		private string _title;
		private DateTime? _createdDate;
		private DateTime? _updatedDate;
		private string _ipAddress;
		private int? _numberOfRemovedItems;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public Guid Key
		{
			get { return _key; }
			set
			{
				CheckChanged(_key, value);
				_key = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public DateTime? CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public DateTime? UpdatedDate
		{
			get { return _updatedDate; }
			set
			{
				CheckChanged(_updatedDate, value);
				_updatedDate = value;
			}
		}

		public string IpAddress
		{
			get { return _ipAddress; }
			set
			{
				CheckChanged(_ipAddress, value);
				_ipAddress = value;
			}
		}

		public int? NumberOfRemovedItems
		{
			get { return _numberOfRemovedItems; }
			set
			{
				CheckChanged(_numberOfRemovedItems, value);
				_numberOfRemovedItems = value;
			}
		}

		private Collection<IWishListItem> _items = new Collection<IWishListItem>();
		internal Collection<IWishListItem> Items
		{
			get { return _items; }
			set { _items = value; }
		}

		//Methods

		public IEnumerable<IWishListItem> GetItems()
		{
			return _items.Where(x => !x.IsDeleted).OrderBy(x => x.Ordinal);
		}

		public bool AddItem(IWishListItem item)
		{
			if (_items.Any(x => x.ProductId == item.ProductId && x.SizeId == item.SizeId && x.GetPackageItemsHashCode() == item.GetPackageItemsHashCode()))
				return false;

			if (GetItems().Count() >= WishListSetting.Instance.MaxNumberOfItems)
				return false;

			item.Ordinal = GetItems().Any() ? GetItems().Max(x => x.Ordinal) + 1 : 1; // Ordinal starts from 1
			_items.Add(item);
			return true;
		}

		public void DeleteItem(int itemId)
		{
			var item = _items.FirstOrDefault(x => x.Id == itemId);
			DeleteItem(item);
		}

		public void DeleteItem(int productId, int? sizeId, int? packageItemsHashCode)
		{
			var item = _items.FirstOrDefault(x => x.ProductId == productId && x.SizeId == sizeId && x.GetPackageItemsHashCode() == packageItemsHashCode);
			DeleteItem(item);
		}

		public void SetItemOrdinal(int itemId, int ordinal)
		{
			var item = _items.FirstOrDefault(x => x.Id == itemId);
			SetItemOrdinal(item, ordinal);
		}

		public void SetItemOrdinal(int productId, int? sizeId, int? packageItemsHashCode, int ordinal)
		{
			var item = _items.FirstOrDefault(x => x.ProductId == productId && x.SizeId == sizeId && x.GetPackageItemsHashCode() == packageItemsHashCode);
			SetItemOrdinal(item, ordinal);
		}

		public bool UpdateProductSize(int itemId, int productId, int sizeId)
		{
			var sameItem = _items.FirstOrDefault(x => x.ProductId == productId && x.SizeId == sizeId);
			if (sameItem != null) return false;

			var item = _items.FirstOrDefault(x => x.Id == itemId);
			if (item != null)
			{
				item.SizeId = sizeId;
				return true;
			}

			return false;
		}

		public bool UpdatePackageSize(int itemId, List<string> productIds, List<string> sizeIds, out IWishListItem wishListItem)
		{
			wishListItem = null;

			var item = _items.FirstOrDefault(x => x.Id == itemId);
			if (item == null) return false;

			var clone = ObjectCopier.Clone(item);

			var updatedPackageItems = new Dictionary<int, int>();
			for (int i = 0; i < productIds.Count; i++)
			{
				int id;
				if (!int.TryParse(productIds[i], out id)) continue;

				int sizeId;
				if (!int.TryParse(sizeIds[i], out sizeId)) continue;

				// the same package item has been updated
				if (updatedPackageItems.ContainsKey(id))
				{
					var packageItems = clone.PackageItems.Where(pi => pi.ProductId == id).ToList();
					var packageItem = packageItems[updatedPackageItems[id]];
					if (packageItem != null)
					{
						packageItem.SizeId = sizeId;
						updatedPackageItems[id]++;
					}
				}
				else // update package item
				{
					var packageItem = clone.PackageItems.FirstOrDefault(pi => pi.ProductId == id);
					if (packageItem != null)
					{
						packageItem.SizeId = sizeId;
						updatedPackageItems.Add(id, 1);
					}
				}
			}

			if (_items.Any(x => x.ProductId == clone.ProductId && x.SizeId == clone.SizeId && x.GetPackageItemsHashCode() == clone.GetPackageItemsHashCode()))
			{
				return false;
			}

			item.PackageItems = clone.PackageItems;
			wishListItem = item;
			return true;
		}

		private void DeleteItem(IWishListItem item)
		{
			if (item == null)
				return;

			item.SetDeleted();
			SortItems();

			NumberOfRemovedItems = NumberOfRemovedItems.HasValue ? NumberOfRemovedItems + 1 : 1;
		}

		private void SetItemOrdinal(IWishListItem item, int ordinal)
		{
			if (item == null)
			{
				return;
			}

			if (item.Ordinal == ordinal)
			{
				return;
			}

			ChangeOrder(item.Id, ordinal);

			SortItems();
		}

		private void ChangeOrder(int itemId, int ordinal)
		{
			foreach (IWishListItem item in GetItems())
			{
				if (item.Id == itemId)
				{
					item.Ordinal = ordinal;
				}
				else if (item.Ordinal >= ordinal)
				{
					item.Ordinal += 1;
				}
			}
		}

		private void SortItems()
		{
			int i = 1; // Ordianl starts from 1
			foreach (IWishListItem item in GetItems())
			{
				item.Ordinal = i;
				i++;
			}
		}
	}
}