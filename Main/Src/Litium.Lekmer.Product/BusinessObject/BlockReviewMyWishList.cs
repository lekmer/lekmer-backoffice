﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockReviewMyWishList : BlockBase, IBlockReviewMyWishList
	{
		private IBlockSetting _setting;
		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}