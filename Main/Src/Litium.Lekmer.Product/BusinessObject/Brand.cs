﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Brand : BusinessObjectBase, IBrand
	{
		private int _id;
		private string _title;
		private string _description;
		private string _url;
		private int? _mediaId;
		private IImage _image;
		private string _erpId;
		private int? _contentNodeId;
		private string _packageInfo;
		private int? _monitorThreshold;
		private int? _maxQuantityPerOrder;
		private int? _deliveryTimeId;
		private IDeliveryTime _deliveryTime;
		private bool _isOffline;

		public int? MediaId
		{
			get { return _mediaId; }
			set
			{
				CheckChanged(_mediaId, value);
				_mediaId = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public string ExternalUrl
		{
			get { return _url; }
			set
			{
				CheckChanged(_url, value);
				_url = value;
			}
		}

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public IImage Image
		{
			get { return _image; }
			set
			{
				CheckChanged(_image, value);
				_image = value;
			}
		}

		public string ErpId
		{
			get { return _erpId; }
			set
			{
				CheckChanged(_erpId, value);
				_erpId = value;
			}
		}

		public int? ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}

		public string PackageInfo
		{
			get { return _packageInfo; }
			set
			{
				CheckChanged(_packageInfo, value);
				_packageInfo = value;
			}
		}

		/// <summary>
		/// The minimum number in stock value for sending monitor product mails
		/// </summary>
		public int? MonitorThreshold
		{
			get { return _monitorThreshold; }
			set
			{
				CheckChanged(_monitorThreshold, value);
				_monitorThreshold = value;
			}
		}

		/// <summary>
		/// The maximum quantity of product allowed in a single order
		/// </summary>
		public int? MaxQuantityPerOrder
		{
			get { return _maxQuantityPerOrder; }
			set
			{
				CheckChanged(_maxQuantityPerOrder, value);
				_maxQuantityPerOrder = value;
			}
		}

		public int? DeliveryTimeId
		{
			get { return _deliveryTimeId; }
			set
			{
				CheckChanged(_deliveryTimeId, value);
				_deliveryTimeId = value;
			}
		}

		public IDeliveryTime DeliveryTime
		{
			get { return _deliveryTime; }
			set
			{
				CheckChanged(_deliveryTime, value);
				_deliveryTime = value;
			}
		}

		public bool IsOffline
		{
			get { return _isOffline; }
			set
			{
				CheckChanged(_isOffline, value);
				_isOffline = value;
			}
		}
	}
}