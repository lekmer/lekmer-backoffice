﻿using System;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ContentMessage : BusinessObjectBase, IContentMessage
	{
		private int _id;
		private int _contentMessageFolderId;
		private string _commonName;
		private string _message;
		private bool _isDropShip;
		private DateTime? _startDate;
		private DateTime? _endDate;
		private TimeSpan? _startDailyInterval;
		private TimeSpan? _endDailyInterval;
		private ProductIdDictionary _includeProducts;
		private CampaignCategoryDictionary _includeCategories;
		private BrandIdDictionary _includeBrands;
		private IdDictionary _includeSuppliers;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ContentMessageFolderId
		{
			get { return _contentMessageFolderId; }
			set
			{
				CheckChanged(_contentMessageFolderId, value);
				_contentMessageFolderId = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Message
		{
			get { return _message; }
			set
			{
				CheckChanged(_message, value);
				_message = value;
			}
		}

		public bool IsDropShip
		{
			get { return _isDropShip; }
			set
			{
				CheckChanged(_isDropShip, value);
				_isDropShip = value;
			}
		}

		public virtual DateTime? StartDate
		{
			get { return _startDate; }
			set
			{
				CheckChanged(_startDate, value);
				_startDate = value;
			}
		}

		public virtual DateTime? EndDate
		{
			get { return _endDate; }
			set
			{
				CheckChanged(_endDate, value);
				_endDate = value;
			}
		}

		public virtual TimeSpan? StartDailyInterval
		{
			get { return _startDailyInterval; }
			set
			{
				CheckChanged(_startDailyInterval, value);
				_startDailyInterval = value;
			}
		}

		public virtual TimeSpan? EndDailyInterval
		{
			get { return _endDailyInterval; }
			set
			{
				CheckChanged(_endDailyInterval, value);
				_endDailyInterval = value;
			}
		}

		public virtual bool IsInTimeLimit
		{
			get
			{
				var now = DateTime.Now;

				if (StartDate.HasValue && now < StartDate) return false;
				if (EndDate.HasValue && now > EndDate) return false;

				//Both daily interval boundaries are required to set limitation
				if (!StartDailyInterval.HasValue || !EndDailyInterval.HasValue) return true;

				if (now.TimeOfDay < StartDailyInterval) return false;
				if (now.TimeOfDay > EndDailyInterval) return false;

				return true;
			}
		}

		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts ?? (_includeProducts = new ProductIdDictionary()); }
			set { _includeProducts = value; }
		}

		public CampaignCategoryDictionary IncludeCategories
		{
			get { return _includeCategories ?? (_includeCategories = new CampaignCategoryDictionary()); }
			set { _includeCategories = value; }
		}

		public BrandIdDictionary IncludeBrands
		{
			get { return _includeBrands ?? (_includeBrands = new BrandIdDictionary()); }
			set { _includeBrands = value; }
		}

		public IdDictionary IncludeSuppliers
		{
			get { return _includeSuppliers ?? (_includeSuppliers = new IdDictionary()); }
			set { _includeSuppliers = value; }
		}
	}
}