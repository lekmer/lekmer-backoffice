﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableRow : BusinessObjectBase, ISizeTableRow
	{
		private int _id;
		private int _sizeTableId;
		private int _sizeId;
		private string _column1Value;
		private string _column2Value;
		private int _ordinal;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int SizeTableId
		{
			get { return _sizeTableId; }
			set
			{
				CheckChanged(_sizeTableId, value);
				_sizeTableId = value;
			}
		}

		public int SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}

		public string Column1Value
		{
			get { return _column1Value; }
			set
			{
				CheckChanged(_column1Value, value);
				_column1Value = value;
			}
		}

		public string Column2Value
		{
			get { return _column2Value; }
			set
			{
				CheckChanged(_column2Value, value);
				_column2Value = value;
			}
		}

		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
	}
}