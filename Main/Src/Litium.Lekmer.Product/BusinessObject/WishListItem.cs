﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class WishListItem:  BusinessObjectBase, IWishListItem
	{
		private int _id;
		private int _wishListId;
		private int _productId;
		private int? _sizeId;
		private int _ordinal;
		private Collection<IWishListPackageItem> _packageItems;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int WishListId
		{
			get { return _wishListId; }
			set
			{
				CheckChanged(_wishListId, value);
				_wishListId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int? SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}

		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}

		public Collection<IWishListPackageItem> PackageItems
		{
			get { return _packageItems ?? (_packageItems = new Collection<IWishListPackageItem>()); }
			set { _packageItems = value; }
		}

		public int? GetPackageItemsHashCode()
		{
			if (PackageItems == null || PackageItems.Count == 0) return null;

			var elements = new List<IWishListPackageItem>(PackageItems);
			elements.Sort(delegate(IWishListPackageItem element1, IWishListPackageItem element2)
			{
				if (element1.ProductId < element2.ProductId)
					return -1;
				if (element1.ProductId > element2.ProductId)
					return 1;
				return 0;
			});

			string elementsInfo = string.Empty;
			foreach (var element in elements)
			{
				elementsInfo += element.ProductId + element.SizeId;
			}

			var hashCode = elementsInfo.GetHashCode();
			if (hashCode < 0)
			{
				hashCode = hashCode * (-1);
			}

			return hashCode;
		}
	}
}