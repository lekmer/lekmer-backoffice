﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableRowTranslation : BusinessObjectBase, ISizeTableRowTranslation
	{
		private int _sizeTableRowId;
		private int _languageId;
		private string _column1Value;
		private string _column2Value;

		public int SizeTableRowId
		{
			get { return _sizeTableRowId; }
			set
			{
				CheckChanged(_sizeTableRowId, value);
				_sizeTableRowId = value;
			}
		}

		public int LanguageId
		{
			get { return _languageId; }
			set
			{
				CheckChanged(_languageId, value);
				_languageId = value;
			}
		}

		public string Column1Value
		{
			get { return _column1Value; }
			set
			{
				CheckChanged(_column1Value, value);
				_column1Value = value;
			}
		}

		public string Column2Value
		{
			get { return _column2Value; }
			set
			{
				CheckChanged(_column2Value, value);
				_column2Value = value;
			}
		}
	}
}