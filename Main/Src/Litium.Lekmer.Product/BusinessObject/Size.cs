using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Size : BusinessObjectBase, ISize
	{
		private int _id;
		private string _erpId;
		private string _erpTitle;
		private decimal _eu;
		private string _euTitle;
		private decimal _usMale;
		private decimal _usFemale;
		private decimal _ukMale;
		private decimal _ukFemale;
		private int _millimeter;
		private int _ordinal;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string ErpId
		{
			get { return _erpId; }
			set
			{
				CheckChanged(_erpId, value);
				_erpId = value;
			}
		}

		public string ErpTitle
		{
			get { return _erpTitle; }
			set
			{
				CheckChanged(_erpTitle, value);
				_erpTitle = value;
			}
		}

		public decimal Eu
		{
			get { return _eu; }
			set
			{
				CheckChanged(_eu, value);
				_eu = value;
			}
		}

		public string EuTitle
		{
			get { return _euTitle; }
			set
			{
				CheckChanged(_euTitle, value);
				_euTitle = value;
			}
		}

		public decimal UsMale
		{
			get { return _usMale; }
			set
			{
				CheckChanged(_usMale, value);
				_usMale = value;
			}
		}

		public decimal UsFemale
		{
			get { return _usFemale; }
			set
			{
				CheckChanged(_usFemale, value);
				_usFemale = value;
			}
		}

		public decimal UkMale
		{
			get { return _ukMale; }
			set
			{
				CheckChanged(_ukMale, value);
				_ukMale = value;
			}
		}

		public decimal UkFemale
		{
			get { return _ukFemale; }
			set
			{
				CheckChanged(_ukFemale, value);
				_ukFemale = value;
			}
		}

		public int Millimeter
		{
			get { return _millimeter; }
			set
			{
				CheckChanged(_millimeter, value);
				_millimeter = value;
			}
		}

		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
	}
}