﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ContentMessageFolder : BusinessObjectBase, IContentMessageFolder
	{
		private int _id;
		private int? _parentFolderId;
		private string _title;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int? ParentFolderId
		{
			get { return _parentFolderId; }
			set
			{
				CheckChanged(_parentFolderId, value);
				_parentFolderId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
	}
}