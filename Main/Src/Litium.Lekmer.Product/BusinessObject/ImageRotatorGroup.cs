﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ImageRotatorGroup : BusinessObjectBase, IImageRotatorGroup
	{
		private int _imageGroupId;
		private int _blockId;
		private int? _mainImageId;
		private int? _thumbnailImageId;

		private int? _internalLinkContentNodeId;
		private string _externalLink;
		private bool _isMovie;
		private string _htmalMarkup;
		private string _title;
		private string _externalLink2;
		private bool _isMovie2;
		private int? _internalLinkContentNodeId2;
		private string _externalLink3;
		private bool _isMovie3;
		private int? _internalLinkContentNodeId3;

		private DateTime? _startDateTime;
		private DateTime? _endDateTime;
		private int _ordinal;

		public int ImageGroupId
		{
			get { return _imageGroupId; }
			set
			{
				CheckChanged(_imageGroupId, value);
				_imageGroupId = value;
			}
		}
		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}
		public int? MainImageId
		{
			get { return _mainImageId; }
			set
			{
				CheckChanged(_mainImageId, value);
				_mainImageId = value;
			}
		}

		public int? ThumbnailImageId
		{
			get { return _thumbnailImageId; }
			set
			{
				CheckChanged(_thumbnailImageId, value);
				_thumbnailImageId = value;
			}
		}

		public int? InternalLinkContentNodeId
		{
			get { return _internalLinkContentNodeId; }
			set
			{
				CheckChanged(_internalLinkContentNodeId, value);
				_internalLinkContentNodeId = value;
			}
		}

		public string ExternalLink
		{
			get { return _externalLink; }
			set
			{
				CheckChanged(_externalLink, value);
				_externalLink = value;
			}
		}

		public bool IsMovie
		{

			get { return _isMovie; }
			set
			{
				CheckChanged(_isMovie, value);
				_isMovie = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string HtmalMarkup
		{
			get { return _htmalMarkup; }
			set
			{
				CheckChanged(_htmalMarkup, value);
				_htmalMarkup = value;
			}
		}

		public string ExternalLink2
		{
			get { return _externalLink2; }
			set
			{
				CheckChanged(_externalLink2, value);
				_externalLink2 = value;
			}
		}

		public bool IsMovie2
		{

			get { return _isMovie2; }
			set
			{
				CheckChanged(_isMovie2, value);
				_isMovie2 = value;
			}
		}

		public int? InternalLinkContentNodeId2
		{
			get { return _internalLinkContentNodeId2; }
			set
			{
				CheckChanged(_internalLinkContentNodeId2, value);
				_internalLinkContentNodeId2 = value;
			}
		}

		public int? InternalLinkContentNodeId3
		{
			get { return _internalLinkContentNodeId3; }
			set
			{
				CheckChanged(_internalLinkContentNodeId3, value);
				_internalLinkContentNodeId3 = value;
			}
		}

		public string ExternalLink3
		{
			get { return _externalLink3; }
			set
			{
				CheckChanged(_externalLink3, value);
				_externalLink3 = value;
			}
		}

		public bool IsMovie3
		{

			get { return _isMovie3; }
			set
			{
				CheckChanged(_isMovie3, value);
				_isMovie3 = value;
			}
		}

		public DateTime? StartDateTime
		{
			get { return _startDateTime; }
			set
			{
				CheckChanged(_startDateTime, value);
				_startDateTime = value;
			}
		}
		public DateTime? EndDateTime
		{
			get { return _endDateTime; }
			set
			{
				CheckChanged(_endDateTime, value);
				_endDateTime = value;
			}
		}
		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
	}
}