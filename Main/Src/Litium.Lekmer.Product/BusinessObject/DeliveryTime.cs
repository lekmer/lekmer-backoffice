using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class DeliveryTime : BusinessObjectBase, IDeliveryTime
	{
		private int _id;
		private int? _fromDays;
		private int? _toDays;
		private string _format;
		private int? _productRegistryid;
		private int? _channelId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		/// <summary>
		/// Min delivery time in days
		/// </summary>
		public int? FromDays
		{
			get { return _fromDays; }
			set
			{
				CheckChanged(_fromDays, value);
				_fromDays = value;
			}
		}

		/// <summary>
		/// Max delivery time in days
		/// </summary>
		public int? ToDays
		{
			get { return _toDays; }
			set
			{
				CheckChanged(_toDays, value);
				_toDays = value;
			}
		}

		/// <summary>
		/// Delivery time format
		/// </summary>
		public string Format
		{
			get { return _format; }
			set
			{
				CheckChanged(_format, value);
				_format = value;
			}
		}

		public int? ProductRegistryid
		{
			get { return _productRegistryid; }
			set
			{
				CheckChanged(_productRegistryid, value);
				_productRegistryid = value;
			}
		}

		public int? ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}
	}
}