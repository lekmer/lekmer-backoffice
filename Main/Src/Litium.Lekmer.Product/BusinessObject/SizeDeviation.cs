﻿using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeDeviation : ISizeDeviation
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string CommonName { get; set; }
	}
}
