﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class WishListPackageItem : BusinessObjectBase, IWishListPackageItem
	{
		private int _id;
		private int _wishListItemId;
		private int _productId;
		private int? _sizeId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int WishListItemId
		{
			get { return _wishListItemId; }
			set
			{
				CheckChanged(_wishListItemId, value);
				_wishListItemId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int? SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}
	}
}