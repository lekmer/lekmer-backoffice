﻿using System;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerProductSearchCriteria : ProductSearchCriteria, ILekmerProductSearchCriteria
	{
		private string _brandId;
		public string BrandId
		{
			get { return string.IsNullOrEmpty(_brandId) ? null : _brandId; }
			set { _brandId = value; }
		}

		private string _productTypeId;
		public string ProductTypeId
		{
			get { return string.IsNullOrEmpty(_productTypeId) ? null : _productTypeId; }
			set { _productTypeId = value; }
		}

		private string _supplierArticleNumber;
		public string SupplierArticleNumber
		{
			get { return string.IsNullOrEmpty(_supplierArticleNumber) ? null : _supplierArticleNumber; }
			set { _supplierArticleNumber = value; }
		}
	}
}