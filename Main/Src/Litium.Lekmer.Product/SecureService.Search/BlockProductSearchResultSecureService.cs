﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.ProductSearch.Cache;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockProductSearchResultSecureService : IBlockProductSearchResultSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductSearchResultRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockProductSearchResultSecureService(
			IAccessValidator accessValidator,
			BlockProductSearchResultRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockProductSearchResult Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockProductSearchResult = IoC.Resolve<IBlockProductSearchResult>();
			blockProductSearchResult.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductSearchResult.Setting = BlockSettingSecureService.Create();
			blockProductSearchResult.Status = BusinessObjectStatus.New;
			return blockProductSearchResult;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			IBlockProductSearchResult blockProductSearchResult = Create();
			blockProductSearchResult.ContentNodeId = contentNodeId;
			blockProductSearchResult.ContentAreaId = contentAreaId;
			blockProductSearchResult.BlockTypeId = blockTypeId;
			blockProductSearchResult.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockProductSearchResult.Title = title;
			blockProductSearchResult.TemplateId = null;
			blockProductSearchResult.Id = Save(systemUserFull, blockProductSearchResult);
			return blockProductSearchResult;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductSearchResult block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			BlockProductSearchResultCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public virtual IBlockProductSearchResult GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockProductSearchResultCache.Instance.Remove(blockId);
		}
	}
}
