﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IBlockProductSearchResult"/>.
	/// </summary>
	public sealed class BlockProductSearchResultCache : ScensumCacheBase<BlockProductSearchResultKey, IBlockProductSearchResult>
	{
		private static readonly BlockProductSearchResultCache _instance = new BlockProductSearchResultCache();

		private BlockProductSearchResultCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static BlockProductSearchResultCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockProductSearchResultKey(channel.Id, blockId));
			}
		}
	}

	/// <summary>
	/// Key used for caching <see cref="IBlockProductSearchResult"/>.
	/// </summary>
	public class BlockProductSearchResultKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="channelId">Channel id of the <see cref="IBlockProductSearchResult"/>.</param>
		/// <param name="blockId">Id of the <see cref="IBlockProductSearchResult"/>.</param>
		public BlockProductSearchResultKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		/// <summary>
		/// Channel id of the <see cref="IBlockProductSearchResult"/>.
		/// </summary>
		public int ChannelId { get; set; }

		/// <summary>
		/// Id of the <see cref="IBlockProductSearchResult"/>.
		/// </summary>
		public int BlockId { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get
			{
				return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
