using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerProductImageRepository : ProductImageRepository
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual Collection<IProductImage> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerProductImageRepository.GetAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductImageGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}



		public virtual Collection<IProductMedia> GetAllProductMediaByProduct(int productId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerProductImageRepository.GetAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductMediaGetAllByProduct]", parameters, dbSettings))
			{
				var dataMapper = DataMapperResolver.Resolve<IProductMedia>(dataReader);
				return dataMapper.ReadMultipleRows();
			}
		}


		public virtual void Delete(int productId, int mediaId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("MediaId", mediaId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("ProductImageRepository.Delete");
			new DataHandler().ExecuteCommand("[product].[pProductImageDelete]", parameters, dbSettings);


		}

		public virtual void Save(int mediaId, int productId, int groupId, int ordinal)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("MediaId", mediaId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductImageGroupId", groupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Ordinal", ordinal, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("ProductImageRepository.Save");
			new DataHandler().ExecuteCommand("[product].[pProductMediaSave]", parameters, dbSettings);

		
		}

	}
}