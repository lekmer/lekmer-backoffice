﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeTableRowRepository
	{
		protected virtual DataMapperBase<ISizeTableRow> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableRow>(dataReader);
		}

		protected virtual DataMapperBase<ISizeTableRowTranslation> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableRowTranslation>(dataReader);
		}

		public virtual Collection<ISizeTableRow> GetAllBySizeTable(int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRowRepository.GetAllBySizeTable");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableRowGetAllBySizeTable]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(ISizeTableRow sizeTableRow)
		{
			if (sizeTableRow == null)
			{
				throw new ArgumentNullException("sizeTableRow");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableRow.SizeTableId, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeId", sizeTableRow.SizeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Column1Value", sizeTableRow.Column1Value, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Column2Value", sizeTableRow.Column2Value, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Ordinal", sizeTableRow.Ordinal, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRowRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeTableRowSave]", parameters, dbSettings);
		}

		public virtual void Delete(int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRowRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableRowDelete]", parameters, dbSettings);
		}

		// Translations.

		public virtual Collection<ISizeTableRowTranslation> GetAllTranslations(int sizeTableRowId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableRowId", sizeTableRowId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRowRepository.GetAllTranslations");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableRowTranslationGetAll]", parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void SaveTranslations(ISizeTableRowTranslation translation)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableRowId", translation.SizeTableRowId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", translation.LanguageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Column1Value", translation.Column1Value, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Column2Value", translation.Column2Value, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("SizeTableRowRepository.SaveTranslations");
			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableRowTranslationSave]", parameters, dbSettings);
		}
	}
}