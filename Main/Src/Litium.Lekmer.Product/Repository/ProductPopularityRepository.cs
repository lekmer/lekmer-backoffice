using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductPopularityRepository
	{
		protected virtual DataMapperBase<IProductPopularity> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductPopularity>(dataReader);
		}

		public Collection<IProductPopularity> GetAll(int channelId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("ProductPopularityRepository.GetAll");
			
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductPopularityGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}