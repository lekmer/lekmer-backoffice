using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BrandSiteStructureProductRegistryWrapperRepository
	{
		protected virtual DataMapperBase<IBrandSiteStructureProductRegistryWrapper> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBrandSiteStructureProductRegistryWrapper>(dataReader);
		}

		public virtual Collection<IBrandSiteStructureProductRegistryWrapper> GetAllByBrand(int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BrandSiteStructureProductRegistryWrapperRepository.GetAllByBrand");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandSiteStructureProductRegistryWrapperGetAllByBrand]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}