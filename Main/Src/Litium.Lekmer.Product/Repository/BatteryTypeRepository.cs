﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BatteryTypeRepository
	{
		protected virtual DataMapperBase<IBatteryType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBatteryType>(dataReader);
		}

		public Collection<IBatteryType> GetAll()
		{
			var dbSettings = new DatabaseSetting("BatteryTypeRepository.GetAll");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBatteryTypeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
