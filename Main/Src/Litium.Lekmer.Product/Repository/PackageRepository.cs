using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Repository
{
	public class PackageRepository
	{
		protected virtual DataMapperBase<IPackage> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IPackage>(dataReader);
		}

		protected virtual DataMapperBase<IProductRecord> CreateProductRecordDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductRecord>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}


		public virtual IPackage Save(IPackage package, string pricesXml)
		{
			if (package == null)
			{
				throw new ArgumentNullException("package");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", package.PackageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("MasterProductId", package.MasterProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ErpId", package.MasterProduct.ErpId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("StatusId", package.MasterProduct.ProductStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StockStatusId", ((ILekmerProduct) package.MasterProduct).StockStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", package.MasterProduct.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("WebShopTitle", package.MasterProduct.WebShopTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("NumberInStock", package.MasterProduct.NumberInStock, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", package.MasterProduct.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Description", ((IProductRecord) package.MasterProduct).Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("GeneralInfo", package.GeneralInfo, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("PriceXml", pricesXml, SqlDbType.Xml)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.Save");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageSave]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					package.PackageId = dataReader.GetInt32(0);
					package.MasterProductId = dataReader.GetInt32(1);
				}
			}

			return package;
		}

		public virtual void Delete(int packageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", packageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.Delete");
			new DataHandler().ExecuteCommand("[productlek].[pPackageDelete]", parameters, dbSettings);
		}

		public virtual Collection<int> SetOfflineByIncludeProduct(int productId)
		{
			var productIds = new Collection<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.SetOfflineByIncludeProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageSetOfflineByIncludeProduct]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual PackageCollection Search(int channelId, IProductSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null)
			{
				throw new ArgumentNullException("searchCriteria");
			}

			int itemsCount = 0;
			var packages = new PackageCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CategoryId", searchCriteria.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title", searchCriteria.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@BrandId", ((ILekmerProductSearchCriteria)searchCriteria).BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@StatusId", searchCriteria.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PriceFrom", searchCriteria.PriceFrom, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@PriceTo", searchCriteria.PriceTo, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@ErpId", searchCriteria.ErpId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageSearch]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						var package = CreateDataMapper(dataReader).MapRow();
						package.MasterProduct = CreateProductRecordDataMapper(dataReader).MapRow();
						packages.Add(package);
					}
				}
			}

			packages.TotalCount = itemsCount;

			return packages;
		}

		public virtual IPackage GetByIdSecure(int channelId, int packageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", packageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageGetByIdSecure]", parameters, dbSettings))
			{
				var package = CreateDataMapper(dataReader).ReadRow();
				package.MasterProduct = CreateProductRecordDataMapper(dataReader).MapRow();
				return package;
			}
		}

		public virtual IPackage GetByMasterProduct(int channelId, int masterProductId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("MasterProductId", masterProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.GetByMasterProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageGetByMasterProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IPackage GetByMasterProductSecure(int channelId, int masterProductId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("MasterProductId", masterProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.GetByMasterProductSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageGetByMasterProductSecure]", parameters, dbSettings))
			{
				var package = CreateDataMapper(dataReader).ReadRow();
				package.MasterProduct = CreateProductRecordDataMapper(dataReader).MapRow();
				return package;
			}
		}

		public virtual ProductIdCollection GetMasterProductIdsByPackageProduct(int channelId, int productId)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.GetMasterProductIdsByPackageProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageMasterProductIdsGetByPackageProduct]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual int GetNumberInStockForPackageWithSizes(string itemsWithSizesXml, string itemsWithoutSizes, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ItemsWithoutSizes", itemsWithoutSizes, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ItemsWithSizes", itemsWithSizesXml, SqlDbType.Xml),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("PackageRepository.GetNumberInStockForPackageWithSizes");

			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pPackageWithSizesGetNumberInStock]", parameters, dbSettings);
		}


		public virtual void SavePackageProducts(int packageId, string productIdList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", packageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductIds", productIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.SavePackageProducts");
			new DataHandler().ExecuteCommand("[productlek].[pPackageProductSave]", parameters, dbSettings);
		}

		public virtual void DeletePackageProducts(int packageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", packageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageRepository.DeletePackageProducts");
			new DataHandler().ExecuteCommand("[productlek].[pPackageProductDelete]", parameters, dbSettings);
		}


		public virtual Collection<ITranslationGeneric> GetAllGeneralInfoTranslationsByPackage(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("PackageRepository.GetAllGeneralInfoTranslationsByPackage");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pPackageGeneralInfoTranslationGetAllByPackage]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void SaveTranslation(int packageId, int languageId, string generalInfo)
		{
			var dbSettings = new DatabaseSetting("PackageRepository.SaveTranslation");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", packageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("GeneralInfo", generalInfo, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[productlek].[pPackageTranslationSave]", parameters, dbSettings);
		}
	}
}