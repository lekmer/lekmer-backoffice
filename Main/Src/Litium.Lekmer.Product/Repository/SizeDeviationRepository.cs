﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeDeviationRepository
	{
		protected DataMapperBase<ISizeDeviation> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeDeviation>(dataReader);
		}

		public Collection<ISizeDeviation> GetAll()
		{
			var dbSettings = new DatabaseSetting("SizeDeviationRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeDeviationGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
