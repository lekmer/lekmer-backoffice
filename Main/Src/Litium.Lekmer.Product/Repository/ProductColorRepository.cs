using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductColorRepository
	{
		protected virtual DataMapperBase<IProductColor> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductColor>(dataReader);
		}

		public virtual Collection<IProductColor> GetAll()
		{
			var dbSettings = new DatabaseSetting("ProductColorRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductColorGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductColor> GetAllByProductErpId(string erpId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductErpId", erpId, SqlDbType.VarChar)
			};
			var dbSettings = new DatabaseSetting("ProductColorRepository.GetAllByProductErpId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pColorGetAllByProductErpId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}