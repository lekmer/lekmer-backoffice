﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductRegistryRestrictionProductRepository
	{
		protected virtual DataMapperBase<IProductRegistryRestrictionItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductRegistryRestrictionItem>(dataReader);
		}

		public virtual void Save(IProductRegistryRestrictionItem productRegistryRestrictionProduct)
		{
			if (productRegistryRestrictionProduct == null)
			{
				throw new ArgumentNullException("productRegistryRestrictionProduct");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductRegistryId", productRegistryRestrictionProduct.ProductRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productRegistryRestrictionProduct.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RestrictionReason", productRegistryRestrictionProduct.RestrictionReason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", productRegistryRestrictionProduct.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", productRegistryRestrictionProduct.CreatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionProductRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryRestrictionProductSave]", parameters, dbSettings);
		}

		public virtual void Delete(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionProductRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryRestrictionProductDelete]", parameters, dbSettings);
		}

		public virtual Collection<IProductRegistryRestrictionItem> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionProductRepository.GetAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryRestrictionProductGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductRegistryRestrictionItem> GetAllWithChannel()
		{
			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionProductRepository.GetAllWithChannel");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryRestrictionProductGetAllWithChannel]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}