﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockSharedWishListRepository
	{
		protected virtual DataMapperBase<IBlockSharedWishList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockSharedWishList>(dataReader);
		}

		public virtual IBlockSharedWishList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
				 ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockSharedWishListRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockSharedWishListGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public IBlockSharedWishList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockSharedWishListRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[lekmer].[pBlockSharedWishListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}