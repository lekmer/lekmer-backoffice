﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class WishListItemRepository
	{
		protected DataMapperBase<IWishListItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IWishListItem>(dataReader);
		}

		public Collection<IWishListItem> GetByWishList(int wishListId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@WishListId", wishListId, SqlDbType.Int)
			};

			DatabaseSetting dbSettings = new DatabaseSetting("WishListItemRepository.GetByWishList");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pWishListItemGetAllByWishList]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public int Save(IWishListItem wishListItem)
		{
			if (wishListItem == null)
			{
				throw new ArgumentNullException("wishListItem");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Id", wishListItem.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@WishListId", wishListItem.WishListId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductId", wishListItem.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@SizeId", wishListItem.SizeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Ordinal", wishListItem.Ordinal, SqlDbType.Int)
			};

			DatabaseSetting dbSettings = new DatabaseSetting("WishListItemRepository.Save");

			wishListItem.Id = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pWishListItemSave]", parameters, dbSettings);
			return wishListItem.Id;
		}

		public void Delete(int wishListItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@Id", wishListItemId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("WishListItemRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pWishListItemDelete]", parameters, dbSettings);
		}
	}
}
