﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductTypeRepository
	{
		protected virtual DataMapperBase<IProductType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductType>(dataReader);
		}

		public Collection<IProductType> GetAll()
		{
			var dbSettings = new DatabaseSetting("ProductTypeRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductTypeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<int> GetIdAllByBlock(int blockId)
		{
			var dbSettings = new DatabaseSetting("ProductTypeRepository.GetAllByBlock");

			IDataParameter[] parameters = 
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var ids = new Collection<int>();

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductTypeGetIdAllByBlock]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					ids.Add(dataReader.GetInt32(0));
				}
			}

			return ids;
		}

		public virtual void SaveByBlock(int blockId, string productTypeIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductTypeIds", productTypeIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductTypeRepository.SaveByBlock");

			new DataHandler().ExecuteCommand("[productlek].[pProductTypeSaveByBlock]", parameters, dbSettings);
		}

		public virtual void DeleteByBlock(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductTypeRepository.DeleteByBlock");

			new DataHandler().ExecuteCommand("[productlek].[pProductTypeDeleteByBlock]", parameters, dbSettings);
		}
	}
}