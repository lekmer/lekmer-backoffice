﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeTableFolderRepository
	{
		protected virtual DataMapperBase<ISizeTableFolder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableFolder>(dataReader);
		}

		protected virtual DataMapperBase<INode> CreateDataMapperNode(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INode>(dataReader);
		}

		public virtual int Save(ISizeTableFolder sizeTableFolder)
		{
			if (sizeTableFolder == null)
			{
				throw new ArgumentNullException("sizeTableFolder");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableFolderId", sizeTableFolder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ParentSizeTableFolderId", sizeTableFolder.ParentFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", sizeTableFolder.Title, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("SizeTableFolderRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeTableFolderSave]", parameters, dbSettings);
		}

		public virtual void Delete(int sizeTableFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableFolderId", sizeTableFolderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableFolderRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableFolderDelete]", parameters, dbSettings);
		}

		public virtual ISizeTableFolder GetById(int sizeTableFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableFolderId", sizeTableFolderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableFolderRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableFolderGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<ISizeTableFolder> GetAll()
		{
			var dbSettings = new DatabaseSetting("SizeTableFolderRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableFolderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SelectedId", selectedId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableFolderRepository.GetTree");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableFolderGetTree]", parameters, dbSettings))
			{
				return CreateDataMapperNode(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ISizeTableFolder> GetAllByParent(int parentId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ParentId", parentId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableFolderRepository.GetAllByParent");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableFolderGetAllByParent]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}