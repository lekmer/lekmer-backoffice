﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class WishListPackageItemRepository
	{
		protected DataMapperBase<IWishListPackageItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IWishListPackageItem>(dataReader);
		}

		public Collection<IWishListPackageItem> GetAllByWishListItem(int wishListItemId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("WishListItemId", wishListItemId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("WishListPackageItemRepository.GetAllByWishListItem");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pWishListPackageItemGetAllByWishListItem]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public int Save(IWishListPackageItem wishListPackageItem)
		{
			if (wishListPackageItem == null)
			{
				throw new ArgumentNullException("wishListPackageItem");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@WishListItemId", wishListPackageItem.WishListItemId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductId", wishListPackageItem.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@SizeId", wishListPackageItem.SizeId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("WishListPackageItemRepository.Save");
			wishListPackageItem.Id = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pWishListPackageItemSave]", parameters, dbSettings);
			return wishListPackageItem.Id;
		}

		public void Update(IWishListPackageItem wishListPackageItem)
		{
			if (wishListPackageItem == null)
			{
				throw new ArgumentNullException("wishListPackageItem");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("Id", wishListPackageItem.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("SizeId", wishListPackageItem.SizeId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("WishListPackageItemRepository.Update");
			new DataHandler().ExecuteCommand("[lekmer].[pWishListPackageItemUpdate]", parameters, dbSettings);
		}

		public void Delete(int wishListItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("WishListItemId", wishListItemId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("WishListPackageItemRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pWishListPackageItemDelete]", parameters, dbSettings);
		}
	}
}