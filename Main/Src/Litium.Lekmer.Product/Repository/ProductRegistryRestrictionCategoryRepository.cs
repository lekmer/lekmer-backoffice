﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductRegistryRestrictionCategoryRepository
	{
		protected virtual DataMapperBase<IProductRegistryRestrictionItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductRegistryRestrictionItem>(dataReader);
		}

		public virtual void Save(IProductRegistryRestrictionItem productRegistryRestrictionCategory)
		{
			if (productRegistryRestrictionCategory == null)
			{
				throw new ArgumentNullException("productRegistryRestrictionCategory");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductRegistryId", productRegistryRestrictionCategory.ProductRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", productRegistryRestrictionCategory.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RestrictionReason", productRegistryRestrictionCategory.RestrictionReason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", productRegistryRestrictionCategory.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", productRegistryRestrictionCategory.CreatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionCategoryRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryRestrictionCategorySave]", parameters, dbSettings);
		}

		public virtual void Delete(int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionCategoryRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryRestrictionCategoryDelete]", parameters, dbSettings);
		}

		public virtual Collection<IProductRegistryRestrictionItem> GetAllByCategory(int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionCategoryRepository.GetAllByCategory");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductRegistryRestrictionItem> GetAllWithChannel()
		{
			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionCategoryRepository.GetAllWithChannel");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryRestrictionCategoryGetAllWithChannel]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}