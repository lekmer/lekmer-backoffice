﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockProductSearchEsalesResultDataMapper : DataMapperBase<IBlockProductSearchEsalesResult>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private int _secondaryTemplateId;

		public BlockProductSearchEsalesResultDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_secondaryTemplateId = OrdinalOf("BlockProductSearchEsalesResult.SecondaryTemplateId");
		}

		protected override IBlockProductSearchEsalesResult Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductSearchEsalesResult = IoC.Resolve<IBlockProductSearchEsalesResult>();

			block.ConvertTo(blockProductSearchEsalesResult);

			blockProductSearchEsalesResult.SecondaryTemplateId = MapNullableValue<int?>(_secondaryTemplateId);

			blockProductSearchEsalesResult.Status = BusinessObjectStatus.Untouched;

			return blockProductSearchEsalesResult;
		}
	}
}