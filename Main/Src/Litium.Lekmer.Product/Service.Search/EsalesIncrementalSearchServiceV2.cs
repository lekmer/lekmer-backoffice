using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class EsalesIncrementalSearchServiceV2 : IEsalesIncrementalSearchServiceV2
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ITagService TagService { get; private set; }
		protected IBrandService BrandService { get; private set; }

		public EsalesIncrementalSearchServiceV2(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService,
			IProductService productService, ICategoryService categoryService,
			ITagService tagService, IBrandService brandService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = productService;
			CategoryService = categoryService;
			TagService = tagService;
			BrandService = brandService;
		}

		public virtual ISearchSuggestions Search(IUserContext context, string text, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchIncrementalPanelSettingV2 panelSetting = new SearchIncrementalPanelSettingV2(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();
			searchSuggestions.Suggestions = FindStringSuggestions(panelSetting, esalesResponse);
			searchSuggestions.Corrections = FindStringCorrections(panelSetting, esalesResponse);
			searchSuggestions.Products = FindProductSuggestions(context, panelSetting, esalesResponse);
			searchSuggestions.Brands = FindBrandSuggestions(context, panelSetting, esalesResponse);
			searchSuggestions.CategoryTags = FindCategorySuggestions(context, panelSetting, esalesResponse);

			return searchSuggestions;
		}

		protected virtual Collection<ISearchSuggestionInfo> FindStringSuggestions(ISearchIncrementalPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			return GetCompletionHits(panelSetting.AutocompletePanelName, esalesResponse);
		}
		protected virtual Collection<ISearchSuggestionInfo> FindStringCorrections(ISearchIncrementalPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.DidYouMeanPanelName);
			if (panelContent != null)
			{
				var correctionsResult = panelContent.FindResult(EsalesResultType.Corrections) as ICorrectionHits;
				if (correctionsResult != null)
				{
					return new Collection<ISearchSuggestionInfo>(correctionsResult.CorrectionsInfo.Select(c => new SearchSuggestionInfo {Text = c.Text, Ticket = c.Ticket} as ISearchSuggestionInfo).ToList());
				}
			}
			return new Collection<ISearchSuggestionInfo>();
		}
		protected virtual ProductCollection FindProductSuggestions(IUserContext context, ISearchIncrementalPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductSuggestionsPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;
				if (productsResult != null)
				{
					Dictionary<int, string> tickets = productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

					var productIds = new ProductIdCollection(productsResult.ProductsInfo.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
					ProductCollection products = ProductService.PopulateProducts(context, productIds);
					foreach (ILekmerProduct product in products)
					{
						product.EsalesTicket = tickets[product.Id];
					}

					return products;
				}
			}
			return new ProductCollection();
		}
		protected virtual Collection<IBrandSuggestionInfo> FindBrandSuggestions(IUserContext context, ISearchIncrementalPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			var brandAuocompleteResults = GetCompletionHits(panelSetting.BrandsPanelName, esalesResponse);
			if (brandAuocompleteResults != null && brandAuocompleteResults.Count > 0)
			{
				var brandSuggestions = new Collection<IBrandSuggestionInfo>();

				Dictionary<string, string> results = brandAuocompleteResults.ToDictionary(s => s.Text, s => s.Ticket);

				BrandCollection allBrands = BrandService.GetAll(context);
				foreach (var text in results.Keys)
				{
					var brand = allBrands.FirstOrDefault(b => b.Title.Equals(text, StringComparison.OrdinalIgnoreCase));
					if (brand == null) continue;

					IBrandSuggestionInfo brandSuggestion = new BrandSuggestionInfo
						{
							Id = brand.Id,
							Brand = brand,
							Ticket = results[text] ?? string.Empty
						};

					brandSuggestions.Add(brandSuggestion);
				}
				return brandSuggestions;
			}
			return new Collection<IBrandSuggestionInfo>();
		}
		protected virtual Collection<ICategoryTagSearchSuggestion> FindCategorySuggestions(IUserContext context, ISearchIncrementalPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			var categoryAuocompleteResults = GetCompletionHits(panelSetting.CategoriesPanelName, esalesResponse);
			if (categoryAuocompleteResults != null && categoryAuocompleteResults.Count > 0)
			{
				var categorySuggestions = new Collection<ICategoryTagSearchSuggestion>();

				Dictionary<string, string> results = categoryAuocompleteResults.ToDictionary(s => s.Text, s => s.Ticket);

				var categoryTree = (ILekmerCategoryTree) CategoryService.GetAllAsTree(context);
				foreach (var text in results.Keys)
				{
					ICategoryTreeItem treeItem = categoryTree.FindItemByTitle(text);
					if (treeItem == null) continue;

					ICategoryTagSearchSuggestion categorySuggestion = new CategoryTagSearchSuggestion
						{
							Category = treeItem.Category,
							CategoryTicket = results[text] ?? string.Empty,
							ParentCategories = new CategoryCollection(),
						};

					// Parent categories
					while (treeItem.Parent != null)
					{
						treeItem = treeItem.Parent;
						if (treeItem.Category != null)
						{
							categorySuggestion.ParentCategories.Insert(0, treeItem.Category);
						}
					}

					categorySuggestions.Add(categorySuggestion);
				}
				return categorySuggestions;
			}
			return new Collection<ICategoryTagSearchSuggestion>();
		}

		protected virtual Collection<ISearchSuggestionInfo> GetCompletionHits(string panelName, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var completionResult = panelContent.FindResult(EsalesResultType.Completions) as ICompletionHits;
				if (completionResult != null)
				{
					return new Collection<ISearchSuggestionInfo>(completionResult.CompletionsInfo.Select(c => new SearchSuggestionInfo { Text = c.Text, Ticket = c.Ticket } as ISearchSuggestionInfo).ToList());
				}
			}
			return new Collection<ISearchSuggestionInfo>();
		}
		protected virtual IdSuggestionCollection FindIdSuggestions(IUserContext context, IEsalesResponse esalesResponse, string panelName)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					var ids = new IdSuggestionCollection(result.ValuesInfo.Select(c => new IdSuggestionInfo {Id = int.Parse(c.Text), Ticket = c.Ticket} as IIdSuggestionInfo));
					ids.TotalCount = result.ValuesInfo.Count;
					return ids;
				}
			}
			return null;
		}
	}
}