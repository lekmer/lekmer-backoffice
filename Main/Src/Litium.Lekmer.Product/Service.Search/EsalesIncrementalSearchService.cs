using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class EsalesIncrementalSearchService : IEsalesIncrementalSearchService
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ITagService TagService { get; private set; }
		protected IBrandService BrandService { get; private set; }

		public EsalesIncrementalSearchService(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService,
			IProductService productService, ICategoryService categoryService,
			ITagService tagService, IBrandService brandService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = productService;
			CategoryService = categoryService;
			TagService = tagService;
			BrandService = brandService;
		}


		public virtual ISearchSuggestions SearchSuggestions(IUserContext context, string text, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchIncrementalAutocompletePanelSetting panelSetting = new SearchIncrementalAutocompletePanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();

			searchSuggestions.Suggestions = FindStringSuggestions(panelSetting, esalesResponse);
			searchSuggestions.Corrections = FindStringCorrections(panelSetting, esalesResponse);

			return searchSuggestions;
		}

		public virtual ISearchSuggestions SearchProductsWithFilter(IUserContext context, string text, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchIncrementalProductFilterPanelSetting panelSetting = new SearchIncrementalProductFilterPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.FilterPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();

			searchSuggestions.Products = FindProductSuggestions(context, panelSetting, esalesResponse);
			searchSuggestions.CategoryTags = FindCategoryTagSearchSuggestion(context, panelSetting, esalesResponse);
			searchSuggestions.Brands = FindBrandSuggestions(context, panelSetting, esalesResponse);

			return searchSuggestions;
		}

		public virtual ISearchSuggestions SearchProducts(IUserContext context, string text, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchIncrementalProductListPanelSetting panelSetting = new SearchIncrementalProductListPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();

			searchSuggestions.Products = FindProductSuggestions(context, panelSetting, esalesResponse);
			searchSuggestions.CategoryTags = FindCategoryTagSearchSuggestion(context, panelSetting, esalesResponse);
			searchSuggestions.Brands = FindBrandSuggestions(context, panelSetting, esalesResponse);

			return searchSuggestions;
		}


		protected virtual ProductCollection FindProductSuggestions(IUserContext context, ISearchProductPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;
				var productsCountResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;

				if (productsResult != null)
				{
					var productIds = new ProductIdCollection(productsResult.ProductsInfo.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
					productIds.TotalCount = productsResult.ProductsInfo.Count;
					if (productsCountResult != null)
					{
						productIds.TotalCount = productsCountResult.TotalHitCount;
					}

					ProductCollection products = ProductService.PopulateProducts(context, productIds);

					Dictionary<int, string> tickets = productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

					foreach (ILekmerProduct product in products)
					{
						product.EsalesTicket = tickets[product.Id];
					}

					return products;
				}
			}

			return new ProductCollection();
		}

		protected virtual Collection<ICategoryTagSearchSuggestion> FindCategoryTagSearchSuggestion(IUserContext context, ISearchIncrementalProductFilterPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			var suggestions = new Collection<ICategoryTagSearchSuggestion>();
			
			IdSuggestionCollection categoryIds = FindIdSuggestions(context, esalesResponse, panelSetting.CategoryPanelName);
			IdSuggestionCollection tagIds = FindIdSuggestions(context, esalesResponse, panelSetting.TagPanelName);

			FindCategorySuggestions(context, suggestions, categoryIds);
			FindTagSuggestions(context, suggestions, tagIds);
			
			return suggestions;
		}

		protected virtual Collection<IBrandSuggestionInfo> FindBrandSuggestions(IUserContext context, ISearchIncrementalProductFilterPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			IdSuggestionCollection ids = 
				FindIdSuggestions(context, esalesResponse, panelSetting.BrandPanelName) 
				??
				FindIdSuggestions(context, esalesResponse, panelSetting.BrandAllPanelName);

			if (ids != null)
			{
				var brandIds = new BrandIdCollection(ids.Select(s => s.Id));

				BrandCollection brands = BrandService.PopulateBrands(context, brandIds);
				Dictionary<int, string> brandTickets = ids.ToDictionary(s => s.Id, s => s.Ticket);

				var brandSuggestions = new Collection<IBrandSuggestionInfo>();
				foreach (IBrand brand in brands)
				{
					IBrandSuggestionInfo brandSuggestion = new BrandSuggestionInfo();

					brandSuggestion.Id = brand.Id;
					brandSuggestion.Brand = brand;
					brandSuggestion.Ticket = brandTickets[brand.Id];

					brandSuggestions.Add(brandSuggestion);
				}

				return brandSuggestions;
			}

			return new Collection<IBrandSuggestionInfo>();
		}

		protected virtual Collection<ISearchSuggestionInfo> FindStringSuggestions(ISearchProductPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.AutoCompletePanelName);
			if (panelContent != null)
			{
				var completionResult = panelContent.FindResult(EsalesResultType.Completions) as ICompletionHits;

				if (completionResult != null)
				{
					return new Collection<ISearchSuggestionInfo>(completionResult.CompletionsInfo.Select(c => new SearchSuggestionInfo { Text = c.Text, Ticket = c.Ticket } as ISearchSuggestionInfo).ToList());
				}
			}

			return new Collection<ISearchSuggestionInfo>();
		}

		protected virtual Collection<ISearchSuggestionInfo> FindStringCorrections(ISearchProductPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.DidYouMeanPanelName);
			if (panelContent != null)
			{
				var correctionsResult = panelContent.FindResult(EsalesResultType.Corrections) as ICorrectionHits;

				if (correctionsResult != null)
				{
					return new Collection<ISearchSuggestionInfo>(correctionsResult.CorrectionsInfo.Select(c => new SearchSuggestionInfo { Text = c.Text, Ticket = c.Ticket } as ISearchSuggestionInfo).ToList());
				}
			}

			return new Collection<ISearchSuggestionInfo>();
		}


		protected virtual IdSuggestionCollection FindIdSuggestions(IUserContext context, IEsalesResponse esalesResponse, string panelName)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				var countResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;

				if (result != null)
				{
					var ids = new IdSuggestionCollection(result.ValuesInfo.Select(c => new IdSuggestionInfo { Id = int.Parse(c.Text), Ticket = c.Ticket } as IIdSuggestionInfo));
					ids.TotalCount = result.ValuesInfo.Count;
					if (countResult != null)
					{
						ids.TotalCount = countResult.TotalHitCount;
					}

					return ids;
				}
			}

			return null;
		}

		protected virtual void FindCategorySuggestions(IUserContext context, Collection<ICategoryTagSearchSuggestion> suggestions, IdSuggestionCollection categoryIdSuggestions)
		{
			if (categoryIdSuggestions == null || categoryIdSuggestions.Count == 0)
			{
				return;
			}

			ICategoryTree categoryTree = CategoryService.GetAllAsTree(context);

			foreach (IIdSuggestionInfo categoryIdSuggestion in categoryIdSuggestions)
			{
				ICategoryTreeItem treeItem = categoryTree.FindItemById(categoryIdSuggestion.Id);
				if (treeItem != null)
				{
					ICategoryTagSearchSuggestion suggestion = new CategoryTagSearchSuggestion();
					suggestion.Category = treeItem.Category;
					suggestion.CategoryTicket = categoryIdSuggestion.Ticket;
					suggestion.ParentCategories = new CategoryCollection();

					// Parent categories
					while (treeItem.Parent != null)
					{
						treeItem = treeItem.Parent;
						if (treeItem.Category != null)
						{
							suggestion.ParentCategories.Insert(0, treeItem.Category);
						}
					}

					suggestions.Add(suggestion);
				}
			}
		}

		protected virtual void FindTagSuggestions(IUserContext context, Collection<ICategoryTagSearchSuggestion> suggestions, IdSuggestionCollection tagIdSuggestions)
		{
			if (tagIdSuggestions == null || tagIdSuggestions.Count == 0)
			{
				return;
			}

			Dictionary<int, int> tagIdDictionary = tagIdSuggestions.ToDictionary(t => t.Id, t => t.Id);
			Dictionary<int, ITag> tagDictionary = TagService.GetAll(context).ToDictionary(t => t.Id);

			foreach (ICategoryTagSearchSuggestion suggestion in suggestions)
			{
				suggestion.Tags = new Collection<ITag>();

				IdCollection categoryTagIds = TagService.GetAllIdsByCategory(suggestion.Category.Id);

				foreach (int tagId in categoryTagIds)
				{
					if (tagIdDictionary.ContainsKey(tagId))
					{
						if (tagDictionary.ContainsKey(tagId))
						{
							ITag tag = tagDictionary[tagId];

							suggestion.Tags.Add(tag);
						}
					}
				}
			}
		}
	}
}