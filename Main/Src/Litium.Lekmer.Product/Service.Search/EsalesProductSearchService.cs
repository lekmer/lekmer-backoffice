using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class EsalesProductSearchService : IEsalesProductSearchService
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected IProductService ProductService { get; private set; }

		public EsalesProductSearchService(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService, IProductService productService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = productService;
		}


		public virtual ISearchSuggestions SearchSuggestions(IUserContext context, string text, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchProductAutocompletePanelSetting panelSetting = new SearchProductAutocompletePanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();

			searchSuggestions.Suggestions = FindStringSuggestions(panelSetting, esalesResponse);

			return searchSuggestions;
		}

		public virtual ISearchSuggestions SearchProductsWithFilter(IUserContext context, string text, IFilterQuery query, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchProductFilterPanelSetting panelSetting = new SearchProductFilterPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchFilterRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.FilterPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);
			searchRequest.FilterQuery = query;
			searchRequest.Channel = context.Channel;

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();
			searchSuggestions.Products = FindProductSuggestions(context, panelSetting, esalesResponse);

			var ageFromMonth = FindIdSuggestions(context, esalesResponse, panelSetting.AgeFromMonthFilterPanelName);
			var ageToMonth = FindIdSuggestions(context, esalesResponse, panelSetting.AgeToMonthFilterPanelName);
			var prices = FindPriceSuggestions(context, esalesResponse, panelSetting.PriceFilterPanelName);

			int? minAge;
			int? maxAge;
			decimal? minPrice;
			decimal? maxPrice;
			FindAgeAndPriceIntervals(ageFromMonth, ageToMonth, prices, out minAge, out maxAge, out minPrice, out maxPrice);

			searchSuggestions.SearchFacetWithFilter = new SearchFacet
				{
					BrandsWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.BrandFilterPanelName),
					MainCategoriesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.MainCategoryFilterPanelName),
					ParentCategoriesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.ParentCategoryFilterPanelName),
					CategoriesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.CategoryFilterPanelName),
					AgeInterval = { FromMonth = minAge, ToMonth = maxAge },
					PriceInterval = { From = minPrice, To = maxPrice },
					SizesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.SizeFilterPanelName),
					TagsWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.TagFilterPanelName)
				};

			return searchSuggestions;
		}

		public virtual ISearchSuggestions SearchProducts(IUserContext context, string text, IFilterQuery query, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchProductPanelSetting panelSetting = new SearchProductPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchFilterRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);
			searchRequest.FilterQuery = query;
			searchRequest.Channel = context.Channel;

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();

			searchSuggestions.Products = FindProductSuggestions(context, panelSetting, esalesResponse);

			var ageFromMonth = FindIdSuggestions(context, esalesResponse, panelSetting.AgeFromMonthFilterPanelName);
			var ageToMonth = FindIdSuggestions(context, esalesResponse, panelSetting.AgeToMonthFilterPanelName);
			var prices = FindPriceSuggestions(context, esalesResponse, panelSetting.PriceFilterPanelName);

			int? minAge;
			int? maxAge;
			decimal? minPrice;
			decimal? maxPrice;
			FindAgeAndPriceIntervals(ageFromMonth, ageToMonth, prices, out minAge, out maxAge, out minPrice, out maxPrice);

			searchSuggestions.SearchFacetWithFilter = new SearchFacet
				{
					BrandsWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.BrandFilterPanelName),
					MainCategoriesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.MainCategoryFilterPanelName),
					ParentCategoriesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.ParentCategoryFilterPanelName),
					CategoriesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.CategoryFilterPanelName),
					AgeInterval = { FromMonth = minAge, ToMonth = maxAge },
					PriceInterval = { From = minPrice, To = maxPrice },
					SizesWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.SizeFilterPanelName),
					TagsWithFilter = FindIdSuggestions(context, esalesResponse, panelSetting.TagFilterPanelName)
				};

			return searchSuggestions;
		}


		protected virtual ProductCollection FindProductSuggestions(IUserContext context, ISearchProductPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;
				var productsCountResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;

				if (productsResult != null)
				{
					var productIds = new ProductIdCollection(productsResult.ProductsInfo.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
					productIds.TotalCount = productsResult.ProductsInfo.Count;
					if (productsCountResult != null)
					{
						productIds.TotalCount = productsCountResult.TotalHitCount;
					}

					ProductCollection products = ((ILekmerProductService)ProductService).PopulateViewProducts(context, productIds);
					
					Dictionary<int, string> tickets = productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);
					foreach (ILekmerProduct product in products)
					{
						product.EsalesTicket = tickets[product.Id];
					}

					return products;
				}
			}

			return new ProductCollection();
		}

		protected virtual Collection<ISearchSuggestionInfo> FindStringSuggestions(ISearchProductPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.AutoCompletePanelName);
			if (panelContent != null)
			{
				var completionResult = panelContent.FindResult(EsalesResultType.Completions) as ICompletionHits;
				if (completionResult != null)
				{
					var suggestions = new Collection<ISearchSuggestionInfo>();
					foreach (ICompletionInfo completionInfo in completionResult.CompletionsInfo)
					{
						suggestions.Add(new SearchSuggestionInfo { Text = completionInfo.Text, Ticket = completionInfo.Ticket });
					}
					return suggestions;
				}
			}

			return new Collection<ISearchSuggestionInfo>();
		}

		protected virtual Dictionary<int, int> FindIdSuggestions(IUserContext context, IEsalesResponse esalesResponse, string panelName)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					return result.ValuesInfo.ToDictionary(c => int.Parse(c.Text), c => c.Count);
				}
			}

			return new Dictionary<int, int>();
		}

		protected virtual Collection<decimal> FindPriceSuggestions(IUserContext context, IEsalesResponse esalesResponse, string panelName)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					return new Collection<decimal>(result.ValuesInfo.Select(c => decimal.Parse(c.Text.Replace('.', ','), CultureInfo.CurrentCulture)).ToList());
				}
			}

			return new Collection<decimal>();
		}

		protected virtual void FindAgeAndPriceIntervals(Dictionary<int, int> ageFromMonths, Dictionary<int, int> ageToMonths, Collection<decimal> prices, out int? minAge, out int? maxAge, out decimal? minPrice, out decimal? maxPrice)
		{
			minAge = null;
			maxAge = null;
			minPrice = null;
			maxPrice = null;

			if (ageFromMonths != null && ageFromMonths.Keys.Count > 0)
			{
				minAge = ageFromMonths.Keys.Min();
			}

			if (ageToMonths != null && ageToMonths.Keys.Count > 0)
			{
				maxAge = ageToMonths.Keys.Max();
			}

			if (prices != null && prices.Count > 0)
			{
				minPrice = prices.Min();
				maxPrice = prices.Max();
			}
		}
	}
}