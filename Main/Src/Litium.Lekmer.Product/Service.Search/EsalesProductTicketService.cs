using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.Product.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class EsalesProductTicketService : IEsalesProductTicketService
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected ISessionStateService SessionStateService { get; set; }

		public EsalesProductTicketService(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService,
			ISessionStateService sessionStateService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			SessionStateService = sessionStateService;
		}


		public virtual string FindTicket(IUserContext context, int productId)
		{
			EsalesService.EnsureNotNull();
			SessionStateService.EnsureNotNull();

			return ProductTicketCache.Instance.TryGetItem(
				new ProductTicketKey(productId), 
				() => FindTicketCore(context, productId));
		}

		public virtual Dictionary<int, string> FindTickets(IUserContext context, ProductIdCollection productIds)
		{
			EsalesService.EnsureNotNull();
			SessionStateService.EnsureNotNull();

			var ticketsPopulated = new Dictionary<int, string>();
			var ticketsNotInCache = new ProductIdCollection();

			// Tries to get tickets from cache, otherwise adds them to fetching queue.
			foreach (int productId in productIds)
			{
				string ticket = ProductTicketCache.Instance.GetData(new ProductTicketKey(productId));

				// Ticket could be NULL, empty and could has value
				if (ticket != null)
				{
					ticketsPopulated.Add(productId, ticket);
				}
				else
				{
					ticketsNotInCache.Add(productId);
				}
			}

			// Fetch the tickets that wasn't in the cache.
			if (ticketsNotInCache.Count > 0)
			{
				Dictionary<int, string> ticketsFetched = FindTicketsCore(context, ticketsNotInCache);

				foreach (int productId in ticketsNotInCache)
				{
					string ticket = string.Empty;
					if (ticketsFetched.ContainsKey(productId))
					{
						ticket = ticketsFetched[productId];
					}

					ticketsPopulated.Add(productId, ticket);
					ProductTicketCache.Instance.Add(new ProductTicketKey(productId), ticket);
				}
			}

			return ticketsPopulated;
		}


		protected virtual string FindTicketCore(IUserContext context, int productId)
		{
			if (!SessionStateService.Available) //Product tickets are not necessary when session is not available, like product exports, ets
			{
				return string.Empty;
			}

			IProductTicketPanelSetting panelSetting = new ProductTicketPanelSetting(context.Channel.CommonName);

			var esalesRequest = IoC.Resolve<IProductTicketEsalesRequest>();
			esalesRequest.PanelPath = panelSetting.PanelPath;
			esalesRequest.Products = productId.ToString(CultureInfo.InvariantCulture);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductTicketPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;

				if (productsResult != null)
				{
					return productsResult.ProductsInfo.Where(p => EsalesProductKeyService.ParseProductKey(p.Key) == productId).Select(p => p.Ticket).FirstOrDefault();
				}
			}

			return string.Empty;
		}

		protected virtual Dictionary<int, string> FindTicketsCore(IUserContext context, ProductIdCollection productIds)
		{
			if (!SessionStateService.Available) //Product tickets are not necessary when session is not available, like product exports, ets
			{
				return new Dictionary<int, string>();
			}

			IProductTicketPanelSetting panelSetting = new ProductTicketPanelSetting(context.Channel.CommonName);

			var esalesRequest = IoC.Resolve<IProductTicketEsalesRequest>();
			esalesRequest.PanelPath = panelSetting.PanelPath;
			esalesRequest.Products = Convert.ToStringIdentifierList(productIds).TrimEnd(',');

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductTicketPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;

				if (productsResult != null)
				{
					return productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);
				}
			}

			return new Dictionary<int, string>();
		}
	}
}