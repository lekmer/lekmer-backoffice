using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockProductSearchEsalesResultService : IBlockProductSearchEsalesResultService
	{
		protected BlockProductSearchEsalesResultRepository Repository { get; private set; }

		public BlockProductSearchEsalesResultService(BlockProductSearchEsalesResultRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductSearchEsalesResult GetById(IUserContext context, int blockId)
		{
			return BlockProductSearchEsalesResultCache.Instance.TryGetItem(
				new BlockProductSearchEsalesResultKey(context.Channel.Id, blockId),
				() => GetByIdCore(context.Channel, blockId));
		}

		protected virtual IBlockProductSearchEsalesResult GetByIdCore(IChannel channel, int blockId)
		{
			return Repository.GetById(channel, blockId);
		}
	}
}