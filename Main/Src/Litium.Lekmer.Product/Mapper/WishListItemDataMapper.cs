﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class WishListItemDataMapper : DataMapperBase<IWishListItem>
	{
		public WishListItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IWishListItem Create()
		{
			var wishListItem = IoC.Resolve<IWishListItem>();
			wishListItem.Id = MapValue<int>("Id");
			wishListItem.WishListId = MapValue<int>("WishListId");
			wishListItem.ProductId = MapValue<int>("ProductId");
			wishListItem.SizeId = MapNullableValue<int?>("SizeId");
			wishListItem.Ordinal = MapValue<int>("Ordinal");
			wishListItem.Status = BusinessObjectStatus.Untouched;

			return wishListItem;
		}
	}
}