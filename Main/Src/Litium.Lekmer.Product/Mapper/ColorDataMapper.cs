using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ColorDataMapper : DataMapperBase<IColor>
	{
		public ColorDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IColor Create()
		{
			var color = IoC.Resolve<IColor>();

			color.Id = MapValue<int>("Color.ColorId");
			color.HyErpId = MapValue<string>("Color.HyErpId");
			color.Value = MapValue<string>("Color.Value");
			color.CommonName = MapValue<string>("Color.CommonName");

			return color;
		}
	}
}