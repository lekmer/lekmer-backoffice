using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class CategoryTagGroupDataMapper : DataMapperBase<ICategoryTagGroup>
	{
		public CategoryTagGroupDataMapper(IDataReader dataReader)
			: base(dataReader) {}

		protected override ICategoryTagGroup Create()
		{
			var categoryTagGroup = IoC.Resolve<ICategoryTagGroup>();

			categoryTagGroup.CategoryId = MapValue<int>("CategoryTagGroup.CategoryId");
			categoryTagGroup.TagGroupId = MapValue<int>("CategoryTagGroup.TagGroupId");

			return categoryTagGroup;
		}
	}
}