﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockProductListDataMapper : DataMapperBase<IBlockProductList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockProductListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockProductList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductList = IoC.Resolve<IBlockProductList>();
			block.ConvertTo(blockProductList);
			blockProductList.Setting = _blockSettingDataMapper.MapRow();
			blockProductList.Status = BusinessObjectStatus.Untouched;
			return blockProductList;
		}
	}
}
