﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductRegistryRestrictionItemDataMapper : DataMapperBase<IProductRegistryRestrictionItem>
	{
		public ProductRegistryRestrictionItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductRegistryRestrictionItem Create()
		{
			var productRegistryRestrictionBrand = IoC.Resolve<IProductRegistryRestrictionItem>();

			productRegistryRestrictionBrand.ProductRegistryId = MapValue<int>("ProductRegistryId");
			productRegistryRestrictionBrand.ItemId = MapValue<int>("ItemId");
			productRegistryRestrictionBrand.RestrictionReason = MapNullableValue<string>("RestrictionReason");
			productRegistryRestrictionBrand.UserId = MapNullableValue<int?>("UserId");
			productRegistryRestrictionBrand.CreatedDate = MapValue<DateTime>("CreatedDate");
			productRegistryRestrictionBrand.ChannelId = MapValue<int>("ChannelId");

			return productRegistryRestrictionBrand;
		}
	}
}