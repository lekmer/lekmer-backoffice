using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductColorDataMapper : DataMapperBase<IProductColor>
	{
		public ProductColorDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductColor Create()
		{
			var productColor = IoC.Resolve<IProductColor>();

			productColor.ColorId = MapValue<int>("ProductColor.ColorId");
			productColor.ProductId = MapValue<int>("ProductColor.ProductId");

			return productColor;
		}
	}
}