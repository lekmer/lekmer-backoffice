﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductStoreStockDataMapper : DataMapperBase<IProductStoreStock>
	{
		protected DataMapperBase<ISize> SizeDataMapper { get; private set; }

		public ProductStoreStockDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			SizeDataMapper = DataMapperResolver.Resolve<ISize>(DataReader);
		}

		protected override IProductStoreStock Create()
		{
			var productStoreStock = IoC.Resolve<IProductStoreStock>();
			productStoreStock.ProductId = MapValue<int>("ProductId");
			productStoreStock.NumberInStock = MapValue<int>("Quantity");
			productStoreStock.SizeInfo = SizeDataMapper.MapRow();
			productStoreStock.SetUntouched();
			return productStoreStock;
		}
	}
}
