﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductUrlDataMapper : DataMapperBase<IProductUrl>
	{
		public ProductUrlDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IProductUrl Create()
		{
			var productUrl = IoC.Resolve<IProductUrl>();
			productUrl.ProductId = MapValue<int>("ProductUrl.ProductId");
			productUrl.LanguageId = MapValue<int>("ProductUrl.LanguageId");
			productUrl.UrlTitle = MapValue<string>("ProductUrl.UrlTitle");
			productUrl.SetUntouched();
			return productUrl;
		}
	}
}
