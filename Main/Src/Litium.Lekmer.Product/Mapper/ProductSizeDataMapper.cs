﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductSizeDataMapper : DataMapperBase<IProductSize>
	{
		protected DataMapperBase<ISize> SizeDataMapper { get; private set; }

		public ProductSizeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			SizeDataMapper = DataMapperResolver.Resolve<ISize>(DataReader);
		}

		protected override IProductSize Create()
		{
			var productSize = IoC.Resolve<IProductSize>();
			productSize.ProductId = MapValue<int>("ProductSize.ProductId");
			productSize.ErpId = MapValue<string>("ProductSize.ErpId");
			productSize.NumberInStock = MapValue<int>("ProductSize.NumberInStock");
			productSize.MillimeterDeviation = MapNullableValue<int?>("ProductSize.MillimeterDeviation");
			productSize.OverrideEu = MapNullableValue<decimal?>("ProductSize.OverrideEU");
			productSize.OverrideMillimeter = MapNullableValue<int?>("ProductSize.OverrideMillimeter");
			productSize.Weight = MapNullableValue<decimal?>("ProductSize.Weight");
			productSize.StockStatusId = MapValue<int>("ProductSize.StockStatusId");

			productSize.SizeInfo = SizeDataMapper.MapRow();

			productSize.SetUntouched();
			return productSize;
		}
	}
}
