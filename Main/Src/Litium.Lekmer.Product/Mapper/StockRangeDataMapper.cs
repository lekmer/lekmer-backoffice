using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class StockRangeDataMapper : DataMapperBase<IStockRange>
	{
		public StockRangeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IStockRange Create()
		{
			var stockRange = IoC.Resolve<IStockRange>();
			stockRange.StockRangeId = MapValue<int>("StockRange.StockRangeId");
			stockRange.CommonName = MapValue<string>("StockRange.CommonName");
			stockRange.StartValue = MapNullableValue<int?>("StockRange.StartValue");
			stockRange.EndValue = MapNullableValue<int?>("StockRange.EndValue");
			return stockRange;
		}
	}
}