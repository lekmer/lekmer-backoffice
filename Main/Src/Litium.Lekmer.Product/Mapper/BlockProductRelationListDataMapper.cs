﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockProductRelationListDataMapper : DataMapperBase<IBlockProductRelationList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IProductSortOrder> _productSortOrderDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockProductRelationListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_productSortOrderDataMapper = DataMapperResolver.Resolve<IProductSortOrder>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockProductRelationList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductRelationList = IoC.Resolve<IBlockProductRelationList>();
			block.ConvertTo(blockProductRelationList);
			blockProductRelationList.ProductSortOrder = _productSortOrderDataMapper.MapRow();
			blockProductRelationList.Setting = _blockSettingDataMapper.MapRow();
			blockProductRelationList.Status = BusinessObjectStatus.Untouched;
			return blockProductRelationList;
		}
	}
}
