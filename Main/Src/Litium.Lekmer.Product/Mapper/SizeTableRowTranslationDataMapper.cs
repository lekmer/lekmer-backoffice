﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableRowTranslationDataMapper : DataMapperBase<ISizeTableRowTranslation>
	{
		public SizeTableRowTranslationDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableRowTranslation Create()
		{
			var sizeTableRowTranslation = IoC.Resolve<ISizeTableRowTranslation>();
			sizeTableRowTranslation.SizeTableRowId = MapValue<int>("SizeTableRowTranslation.SizeTableRowId");
			sizeTableRowTranslation.LanguageId = MapValue<int>("SizeTableRowTranslation.LanguageId");
			sizeTableRowTranslation.Column1Value = MapNullableValue<string>("SizeTableRowTranslation.Column1Value");
			sizeTableRowTranslation.Column2Value = MapNullableValue<string>("SizeTableRowTranslation.Column2Value");
			sizeTableRowTranslation.SetUntouched();
			return sizeTableRowTranslation;
		}
	}
}