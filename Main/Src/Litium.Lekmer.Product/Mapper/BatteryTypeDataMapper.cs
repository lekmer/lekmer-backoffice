﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class BatteryTypeDataMapper : DataMapperBase<IBatteryType>
	{
		public BatteryTypeDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IBatteryType Create()
		{
			var batteryType = IoC.Resolve<IBatteryType>();
			batteryType.Id = MapValue<int>("BatteryType.Id");
			batteryType.Title = MapValue<string>("BatteryType.Title");
			batteryType.SetUntouched();
			return batteryType;
		}
	}
}
