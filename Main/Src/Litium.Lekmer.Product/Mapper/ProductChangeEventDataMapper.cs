using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductChangeEventDataMapper : DataMapperBase<IProductChangeEvent>
	{
		public ProductChangeEventDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductChangeEvent Create()
		{
			var productChangeEvent = IoC.Resolve<IProductChangeEvent>();

			productChangeEvent.Id = MapValue<int>("ProductChangeEvent.ProductChangeEventId");
			productChangeEvent.ProductId = MapValue<int>("ProductChangeEvent.ProductId");
			productChangeEvent.EventStatusId = MapValue<int>("ProductChangeEvent.EventStatusId");
			productChangeEvent.CdonExportEventStatusId = MapValue<int>("ProductChangeEvent.CdonExportEventStatusId");
			productChangeEvent.CreatedDate = MapValue<DateTime>("ProductChangeEvent.CreatedDate");
			productChangeEvent.ActionAppliedDate = MapNullableValue<DateTime>("ProductChangeEvent.ActionAppliedDate");
			productChangeEvent.Reference = MapNullableValue<string>("ProductChangeEvent.Reference");

			productChangeEvent.SetUntouched();

			return productChangeEvent;
		}
	}
}