﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableRowDataMapper : DataMapperBase<ISizeTableRow>
	{
		public SizeTableRowDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableRow Create()
		{
			var sizeTableRow = IoC.Resolve<ISizeTableRow>();
			sizeTableRow.Id = MapValue<int>("SizeTableRow.SizeTableRowId");
			sizeTableRow.SizeTableId = MapValue<int>("SizeTableRow.SizeTableId");
			sizeTableRow.SizeId = MapValue<int>("SizeTableRow.SizeId");
			sizeTableRow.Column1Value = MapValue<string>("SizeTableRow.Column1Value");
			sizeTableRow.Column2Value = MapValue<string>("SizeTableRow.Column2Value");
			sizeTableRow.Ordinal = MapValue<int>("SizeTableRow.Ordinal");
			sizeTableRow.SetUntouched();
			return sizeTableRow;
		}
	}
}