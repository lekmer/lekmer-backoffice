﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Media;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Messenger
{
	public class ProductEntityMapperSecure : EntityMapper<ProductMessageArgsSecure>
	{
		private const string _productUrlMatchPattern = @"\[Product.Url\(""(.*?)""\)\]";
		private const string _imageUrlMatchPattern = @"\[Product.ImageUrl\(""(.*?)""\)\]";
		private const string _imageWidthMatchPattern = @"\[Product.ImageWidth\(""(.*?)""\)\]";
		private const string _imageHeightMatchPattern = @"\[Product.ImageHeight\(""(.*?)""\)\]";
		private const string _brandImageUrlMatchPattern = @"\[Brand.ImageUrl\(""(.*?)""\)\]";
		private const string _brandImageWidthMatchPattern = @"\[Brand.ImageWidth\(""(.*?)""\)\]";
		private const string _brandImageHeightMatchPattern = @"\[Brand.ImageHeight\(""(.*?)""\)\]";
		private const int _productUrlMatchCommonNamePosition = 1;
		private const int _imageUrlMatchCommonNamePosition = 1;
		private const int _brandImageUrlMatchCommonNamePosition = 1;

		public override void AddEntityVariables(Fragment fragment, ProductMessageArgsSecure args)
		{
			var product = args.Product;
			var channel = args.Channel;
			var description = args.ProductDescription;

			fragment.AddVariable("Product.Id", product.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Product.Title", product.DisplayTitle);
			fragment.AddVariable("Product.ShortDescription", product.ShortDescription, VariableEncoding.None);
			fragment.AddVariable("Product.Description", description, VariableEncoding.None);

			var formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>();
			fragment.AddVariable("Product.NumberInStock", formatter.FormatNumber(channel, product.NumberInStock));
			fragment.AddVariable("Product.ItemsInPackage", formatter.FormatNumber(channel, product.ItemsInPackage));
			fragment.AddVariable("Product.Price", formatter.FormatPrice(channel, product.Price.PriceIncludingVat));
			fragment.AddVariable("Product.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, product.Price.PriceIncludingVat));
			fragment.AddVariable("Product.Price_Formatted.Json", product.Price.PriceIncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);
			
			bool campaignApplied = IsPriceAffectedByCampaign(product);
			fragment.AddCondition("Product.IsPriceAffectedByCampaign", campaignApplied);
			if (campaignApplied)
			{
				fragment.AddVariable("Product.CampaignInfo.Price", formatter.FormatPrice(channel, product.CampaignInfo.Price.IncludingVat));
				fragment.AddVariable("Product.CampaignInfo.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, product.CampaignInfo.Price.IncludingVat));
				fragment.AddVariable("Product.CampaignInfo.Price_Formatted.Json", product.CampaignInfo.Price.IncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);
			}

			fragment.AddVariable("Product.Url", ResolveUrl(product.LekmerUrl, channel));
			fragment.AddRegexVariable(
				_productUrlMatchPattern,
				delegate(Match match)
				{
					string commonName = match.Groups[_productUrlMatchCommonNamePosition].Value;
					string productUrl = string.Format(CultureInfo.InvariantCulture, "~/{0}?page-common-name={1}", product.LekmerUrl, commonName);
					return ResolveUrl(productUrl, channel);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			AddImageVariables(fragment, product.Image, product.DisplayTitle, channel);

			IBrand brand = product.Brand;
			fragment.AddCondition("Product.HasBrand", brand != null);

			if (brand != null)
			{
				fragment.AddVariable("Brand.Id", brand.Id.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Brand.Title", brand.Title);
				fragment.AddVariable("Brand.ExternalUrl", brand.ExternalUrl);

				string brandUrl = GetBrandUrl(brand, channel);
				fragment.AddCondition("Brand.HasUrl", !brandUrl.IsNullOrEmpty());
				fragment.AddVariable("Brand.Url", brandUrl);
				fragment.AddVariable("Brand.Description", brand.Description, VariableEncoding.None);

				AddBrandImageVariables(fragment, brand.Image, brand.Title, channel);
			}

			if (IsPriceAffectedByCampaign(product))
			{
				fragment.AddVariable("Product.DiscountInPercent", CalculateDiscountPercent(product.Price.PriceIncludingVat, product.CampaignInfo.Price.IncludingVat));
				fragment.AddVariable("Product.DiscountInAmount", CalculateDiscountAmount(product.Price.PriceIncludingVat, product.CampaignInfo.Price.IncludingVat, channel));
			}

			fragment.AddCondition("Product.IsBuyable", product.IsBuyable);
			fragment.AddCondition("Product.IsBookable", product.IsBookable);
			fragment.AddCondition("Product.IsMonitorable", product.IsMonitorable);
			fragment.AddCondition("Product.IsNew", product.IsNewProduct);
			fragment.AddCondition("Product.HasSizes", product.HasSizes);
			fragment.AddCondition("Product.ShowVariantRelations", product.ShowVariantRelations);
			fragment.AddVariable("Product.ErpId", product.ErpId);
			fragment.AddCondition("Product.HasLekmerErpId", product.LekmerErpId.HasValue());
			fragment.AddVariable("Product.LekmerErpId", product.LekmerErpId);
			fragment.AddVariable("Product.EsalesTicket", product.EsalesTicket);

			AddCampaignAppliedConditions(fragment, product);
			AddRecommendedPriceData(fragment, product, channel);
		}

		private static bool IsPriceAffectedByCampaign(IProduct product)
		{
			return product.CampaignInfo != null && product.CampaignInfo.Price.IncludingVat != product.Price.PriceIncludingVat;
		}

		private static string CalculateDiscountPercent(decimal originalPrice, decimal campaignPrice)
		{
			var discountInPercent = (originalPrice - campaignPrice)*100/originalPrice;
			discountInPercent = Math.Round(discountInPercent, 0, MidpointRounding.AwayFromZero);
			return discountInPercent + "%";
		}

		private static string CalculateDiscountAmount(decimal originalPrice, decimal campaignPrice, IChannel channel)
		{
			var discount = originalPrice - campaignPrice;
			var formatter = IoC.Resolve<IFormatter>();
			string formatDiscount = formatter.FormatPrice(channel, discount);
			return formatDiscount;
		}

		private static void AddImageVariables(Fragment fragment, IImage image, string productTitle, IChannel channel)
		{
			string mediaUrl = string.Empty;

			string originalSizeImageUrl = null;
			string alternativeText = null;
			if (image != null)
			{
				mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(channel, image);
				alternativeText = image.AlternativeText;
				originalSizeImageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image, productTitle);
			}

			fragment.AddCondition("Product.HasImage", image != null);
			fragment.AddVariable("Product.ImageUrl", ResolveImageUrl(originalSizeImageUrl));
			fragment.AddCondition("Product.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
			fragment.AddVariable("Product.ImageAlternativeText", alternativeText);
			fragment.AddVariable("Product.ImageWidth", image != null ? image.Width.ToString(CultureInfo.CurrentCulture) : null);
			fragment.AddVariable("Product.ImageHeight", image != null ? image.Height.ToString(CultureInfo.CurrentCulture) : null);

			fragment.AddRegexVariable(
				_imageUrlMatchPattern,
				delegate(Match match)
					{
						if (image == null) return null;

						string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
						var url = MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, image, productTitle, commonName);
						return ResolveImageUrl(url);
					},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageWidthMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageHeightMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}

		private static void AddCampaignAppliedConditions(Fragment fragment, IProduct product)
		{
			fragment.AddCondition("Product.HasProductDiscountAction", HasCampaignActionApplied(product, "ProductDiscount"));
			fragment.AddCondition("Product.HasPercentagePriceDiscountAction", HasCampaignActionApplied(product, "PercentagePriceDiscount"));
		}

		private static bool HasCampaignActionApplied(IProduct product, string actionCommonName)
		{
			var campaignInfo = (ILekmerProductCampaignInfo) product.CampaignInfo;
			return campaignInfo.CampaignActionsApplied.Any(action => action.ActionCommonName.Equals(actionCommonName));
		}

		private static void AddRecommendedPriceData(Fragment fragment, ILekmerProduct product, IChannel channel)
		{
			var formatter = IoC.Resolve<IFormatter>();
			fragment.AddCondition("Product.HasRecommendedPrice", product.RecommendedPrice.HasValue);
			fragment.AddVariable("Product.RecommendedPrice", product.RecommendedPrice.HasValue
				? formatter.FormatPrice(channel, product.RecommendedPrice.Value)
				: string.Empty);

			var absoluteSave = product.AbsoluteSave;
			fragment.AddCondition("Product.HasAbsoluteSave", absoluteSave.HasValue);
			fragment.AddVariable("Product.AbsoluteSave", absoluteSave.HasValue
				? formatter.FormatPrice(channel, absoluteSave.Value)
				: string.Empty);

			var percentageSave = product.PercentageSave;
			fragment.AddCondition("Product.HasPercentageSave", percentageSave.HasValue);
			fragment.AddVariable("Product.PercentageSave", percentageSave.HasValue
				? Math.Round(percentageSave.Value, 0, MidpointRounding.AwayFromZero) + "%"
				: string.Empty);
		}

		private static string GetBrandUrl(IBrand brand, IChannel channel)
		{
			if (brand == null) throw new ArgumentNullException("brand");
			if (!brand.ContentNodeId.HasValue)
			{
				return null;
			}

			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = channel;
			var contentNodeService = IoC.Resolve<IContentNodeService>();
			var contentNodeTreeItem = contentNodeService.GetTreeItemById(userContext, brand.ContentNodeId.Value);
			if (contentNodeTreeItem == null || contentNodeTreeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return ResolveUrl(contentNodeTreeItem.Url, channel);
		}

		private static void AddBrandImageVariables(Fragment fragment, IImage image, string brandTitle, IChannel channel)
		{
			string originalSizeImageUrl = null;
			string alternativeText = null;
			string mediaUrl = null;

			if (image != null)
			{
				alternativeText = image.AlternativeText;
				mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(channel, image);
				originalSizeImageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image, brandTitle);
			}

			fragment.AddCondition("Brand.HasImage", image != null);
			fragment.AddVariable("Brand.ImageUrl", ResolveImageUrl(originalSizeImageUrl));
			fragment.AddCondition("Brand.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
			fragment.AddVariable("Brand.ImageAlternativeText", alternativeText);
			fragment.AddVariable("Brand.ImageWidth", image != null ? image.Width.ToString(CultureInfo.CurrentCulture) : null);
			fragment.AddVariable("Brand.ImageHeight", image != null ? image.Height.ToString(CultureInfo.CurrentCulture) : null);

			fragment.AddRegexVariable(
				_brandImageUrlMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					string commonName = match.Groups[_brandImageUrlMatchCommonNamePosition].Value;
					var url = MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, image, brandTitle, commonName);
					return ResolveImageUrl(url);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_brandImageWidthMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_brandImageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_brandImageHeightMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_brandImageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}

		private static string ResolveUrl(string url, IChannel channel)
		{
			if (string.IsNullOrEmpty(url))
				return url;

			return string.Format(CultureInfo.CurrentCulture, "http://{0}/{1}", channel.ApplicationName, url);
		}

		private static string ResolveImageUrl(string url)
		{
			if (string.IsNullOrEmpty(url))
				return url;

			if (url.StartsWith("//"))
				return url.Insert(0, "http:");

			return url;
		}
	}
}