﻿using System;
using System.Web;
using Litium.Framework.Messaging;

namespace Litium.Lekmer.Product.Messenger
{
	public class MonitorProductMessenger : MessengerBase<ProductMessageArgsSecure>
	{
		protected MonitorProductEmailTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "MonitorProduct"; }
		}

		protected override Message CreateMessage(ProductMessageArgsSecure messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");

			Template = new MonitorProductEmailTemplate(messageArgs.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = RenderSubject(messageArgs),
				ProviderName = "email",
				Type = MessageType.Single
			};
			message.Recipients.Add(new EmailRecipient
			{
				Name = messageArgs.MonitorProduct.Email,
				Address = messageArgs.MonitorProduct.Email
			});
			return message;
		}

		private string RenderSubject(ProductMessageArgsSecure messageArgs)
		{
			var fragment = Template.GetFragment("Subject");

			ILekmerProduct product = messageArgs.Product;

			fragment.AddVariable("Product.Title", product.Title);

			return HttpUtility.HtmlDecode(fragment.Render());
		}

		private string RenderMessage(ProductMessageArgsSecure messageArgs)
		{
			var htmlFragment = Template.GetFragment("Message");
			htmlFragment.AddEntity(messageArgs);
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}