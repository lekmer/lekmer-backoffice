﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Messenger
{
	public class ProductMessageArgsSecure
	{
		public IChannel Channel { get; set; }
		public IMonitorProduct MonitorProduct { get; set; }
		public ILekmerProduct Product { get; set; }
		public string ProductDescription { get; set; }

		public ProductMessageArgsSecure(IChannel channel, IMonitorProduct monitorProduct, ILekmerProduct product, string productDescription)
		{
			Channel = channel;
			MonitorProduct = monitorProduct;
			Product = product;
			ProductDescription = productDescription;
		}
	}
}