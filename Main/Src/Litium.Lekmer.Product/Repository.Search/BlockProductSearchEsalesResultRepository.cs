﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockProductSearchEsalesResultRepository
	{
		protected virtual DataMapperBase<IBlockProductSearchEsalesResult> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductSearchEsalesResult>(dataReader);
		}


		public virtual IBlockProductSearchEsalesResult GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("BlockProductSearchEsalesResultRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductSearchEsalesResultGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductSearchEsalesResult GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("BlockProductSearchEsalesResultRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductSearchEsalesResultGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		public virtual void Save(IBlockProductSearchEsalesResult block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("SecondaryTemplateId", block.SecondaryTemplateId, SqlDbType.Int),
			};

			var dbSettings = new DatabaseSetting("BlockProductSearchEsalesResultRepository.Save");

			new DataHandler().ExecuteCommand("[productlek].[pBlockProductSearchEsalesResultSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("BlockProductSearchEsalesResultRepository.Delete");

			new DataHandler().ExecuteCommand("[productlek].[pBlockProductSearchEsalesResultDelete]", parameters, dbSettings);
		}
	}
}