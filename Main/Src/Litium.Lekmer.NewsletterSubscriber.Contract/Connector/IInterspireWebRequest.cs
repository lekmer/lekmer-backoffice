﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireWebRequest
	{
		IInterspireWebResponse PostData(string postUrl, string postData);
	}
}