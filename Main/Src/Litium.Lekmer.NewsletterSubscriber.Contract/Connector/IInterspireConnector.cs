﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireConnector
	{
		IInterspireResponseAddSubscriber AddSubscriberToList(string emailAddress, string mailingListId);
		IInterspireResponseDeleteSubscriber DeleteSubscriber(string emailAddress, string mailingListId);
		IInterspireResponseUnsubscribeSubscriber UnsubscribeSubscriber(string emailAddress, string mailingListId);
		IInterspireResponseGetLists GetLists();
		IInterspireResponseIsContactOnList IsContactOnList(string emailAddress, string mailingListId);
		IInterspireResponse AddBannedSubscriber(string emailAddress);
		IInterspireResponseFetchBannedSubscriber FetchBannedSubscriber(string emailAddress);
		IInterspireResponse RemoveBannedSubscriber(int banId);
	}
}