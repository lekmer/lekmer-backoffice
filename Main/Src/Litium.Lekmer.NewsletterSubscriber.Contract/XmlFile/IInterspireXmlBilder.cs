﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireXmlBilder
	{
		string GetAddSubscriberXml(string userName, string userToken, string emailAddress, string mailingListId);
		string GetDeleteSubscriberXml(string userName, string userToken, string emailAddress, string mailingListId);
		string GetUnsubscribeSubscriberXml(string userName, string userToken, string emailAddress, string mailingListId);
		string GetAddBannedSubscriberXml(string userName, string userToken, string emailAddress);
		string GetFetchBannedSubscriberXml(string userName, string userToken);
		string RemoveBannedSubscriber(string userName, string userToken, int banId);
		string GetGetListsXml(string userName, string userToken);
		string GetIsContactOnListXml(string userName, string userToken, string emailAddress, string mailingListId);
	}
}