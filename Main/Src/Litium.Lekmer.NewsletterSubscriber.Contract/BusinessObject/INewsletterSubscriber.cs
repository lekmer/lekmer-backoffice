﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterSubscriber : IBusinessObjectBase
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		string Email { get; set; }
		int SubscriberTypeId { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime UpdatedDate { get; set; }
	}
}