﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireBanList
	{
		int BanId { get; set; }
		string Email { get; set; }
		string List { get; set; }
	}
}