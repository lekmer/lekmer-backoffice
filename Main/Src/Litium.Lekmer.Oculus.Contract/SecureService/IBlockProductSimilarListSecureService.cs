using Litium.Scensum.Core;

namespace Litium.Lekmer.Oculus
{
	public interface IBlockProductSimilarListSecureService
	{
		IBlockProductSimilarList Create();

		IBlockProductSimilarList GetById(int id);

		int Save(ISystemUserFull systemUserFull, IBlockProductSimilarList block);
	}
}