﻿namespace Litium.Lekmer.Oculus
{
	public interface IProductSimilarService
	{
		IProductSimilar GetAllByProductId(int productId);
	}
}