using System.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core.Web
{
	public class LekmerUserContextFactory : UserContextFactory
	{
		public override IUserContext Create()
		{
			ILekmerUserContext context;
			if (HttpContext.Current.Session != null)
			{
				context = (ILekmerUserContext)base.Create();
			}
			else
			{
				context = (ILekmerUserContext)base.CreateCore();
			}

			context.Voucher = IoC.Resolve<IVoucherSession>().Voucher;
			return context;
		}
	}
}