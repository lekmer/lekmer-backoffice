using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Core.Web
{
	/// <summary>
	/// Control for rendering paging.
	/// </summary>
	public class LekmerPagingControl : PagingControl, ILekmerPagingControl
	{
		private readonly ITemplateFactory _templateFactory;
		protected IPagingCalculator PagingCalculator { get; set; }

		public LekmerPagingControl(
			ITemplateFactory templateFactory,
			IPagingCalculator pagingCalculator)
			: base(templateFactory)
		{
			_templateFactory = templateFactory;
			PagingCalculator = pagingCalculator;
		}

		public virtual int PageCount { get; private set; }
		public virtual bool ViewAllModeAllowed { get; set; }

		/// <summary>
		/// Initialize the paging control.
		/// Will set SelectedPage and Template.
		/// </summary>
		public override void Initialize()
		{
			if (PageQueryStringParameterName.IsEmpty())
			{
				throw new InvalidOperationException("PageQueryStringParameterName must be set.");
			}

			SelectedPage = GetSelectedPage();

			Template = _templateFactory.Create("Paging", TemplateId);
		}

		/// <summary>
		/// Renders the paging.
		/// </summary>
		/// <returns>The rendered paging.</returns>
		public override PagingContent Render()
		{
			if (PageBaseUrl.IsEmpty()) throw new InvalidOperationException("PageBaseUrl must be set.");
			if (PageSize <= 0) throw new InvalidOperationException("PageSize must be more then 0.");
			if (TotalCount < 0) throw new InvalidOperationException("TotalCount must be more or equal to 0.");
			if (SelectedPage <= 0) throw new InvalidOperationException("Initialize must be called before calling Render.");

			PageCount = GetPageCount();

			if (PageCount <= 1)
			{
				return new PagingContent();
			}

			string pageLinkSeparatorContent = Template.GetFragment("PageLinkSeparator").Render();
			string pagesBreakContent = Template.GetFragment("PagesBreak").Render();

			SortedDictionary<int, int> visiblePageLinks = GetVisiblePageLinks(PageCount);

			var linkBuilder = new StringBuilder();

			// Backward link.
			if (SelectedPage >= 1 && SelectedPage <= PageCount)
			{
				linkBuilder.AppendLine(RenderBackLink());
			}

			// Link to each page.
			int lastPage = 1;
			foreach (KeyValuePair<int, int> visiblePage in visiblePageLinks)
			{
				if (visiblePage.Key - lastPage > 0)
				{
					linkBuilder.AppendLine(pageLinkSeparatorContent);
				}

				if (visiblePage.Key - lastPage > 1)
				{
					linkBuilder.AppendLine(pagesBreakContent);
					linkBuilder.AppendLine(pageLinkSeparatorContent);
				}

				linkBuilder.AppendLine(RenderPageLink(visiblePage.Key));
				lastPage = visiblePage.Key;
			}

			// Forward link.
			if (SelectedPage >= 1 && SelectedPage <= PageCount)
			{
				linkBuilder.AppendLine(RenderForwardLink(PageCount));
			}

			// App links to "Paging" fragment.
			Fragment fragmentContent = Template.GetFragment("Content");

			fragmentContent.AddVariable("Iterate:Link", linkBuilder.ToString(), VariableEncoding.None);
			fragmentContent.AddVariable("Page.CurrentNumber", SelectedPage.ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddVariable("Page.TotalCount", PageCount.ToString(CultureInfo.InvariantCulture));

			return new PagingContent(
				RenderHead(),
				fragmentContent.Render());
		}

		protected virtual string RenderHead()
		{
			Fragment fragmentHead = Template.GetFragment("Head");

			fragmentHead.AddVariable("Page.CurrentNumber", SelectedPage.ToString(CultureInfo.InvariantCulture));
			fragmentHead.AddVariable("Page.TotalCount", PageCount.ToString(CultureInfo.InvariantCulture));

			fragmentHead.AddCondition("ViewAllMode.Allowed", ViewAllModeAllowed);

			if (ViewAllModeAllowed)
			{
				fragmentHead.AddCondition("Page.IsFirst", SelectedPage == 1);
				fragmentHead.AddCondition("Page.IsLast", SelectedPage == PageCount);

				if (SelectedPage > 1)
				{
					string pagePrevLink = GetPageUrl(SelectedPage - 1);
					fragmentHead.AddVariable("Page.Prev.Link", pagePrevLink);
				}

				if (SelectedPage < PageCount)
				{
					string pageNextLink = GetPageUrl(SelectedPage + 1);
					fragmentHead.AddVariable("Page.Next.Link", pageNextLink);
				}
			}

			return fragmentHead.Render();
		}

		protected virtual int GetPageCount()
		{
			return PagingCalculator.CalculatePageCount(TotalCount, PageSize);
		}

		protected virtual SortedDictionary<int, int> GetVisiblePageLinks(int pageCount)
		{
			int adjacentPages;
			if (!int.TryParse(Template.GetSettingOrNull("AdjacentPages"), out adjacentPages))
			{
				adjacentPages = 3;
			}

			int sidePages;
			if (!int.TryParse(Template.GetSettingOrNull("SidePages"), out sidePages))
			{
				sidePages = 2;
			}

			return PagingCalculator.CalculateVisiblePageLinks(pageCount, SelectedPage, adjacentPages, sidePages);
		}
	}
}