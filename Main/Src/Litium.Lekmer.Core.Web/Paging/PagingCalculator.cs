using System.Collections.Generic;

namespace Litium.Lekmer.Core.Web
{
	public class PagingCalculator : IPagingCalculator
	{
		public virtual SortedDictionary<int, int> CalculateVisiblePageLinks(int pageCount, int selectedPage, int adjacentPages, int sidePages)
		{
			var pages = new SortedDictionary<int, int>();

			// Selected page
			pages[selectedPage] = selectedPage;

			// Side pages 
			// x x ... ... x x
			for (int i = 1; i < sidePages + 1; i++)
			{
				int leftPage = i;
				if (leftPage <= pageCount)
				{
					pages[leftPage] = leftPage;
				}

				int rightPage = pageCount - i + 1;
				if (rightPage >= 1)
				{
					pages[rightPage] = rightPage;
				}
			}

			// Adjacent pages
			// ... x x x [x] x x x ...
			for (int i = 1; i < adjacentPages + 1; i++)
			{
				int leftPage = selectedPage - i;
				if (leftPage >= 1)
				{
					pages[leftPage] = leftPage;
				}

				int rightPage = selectedPage + i;
				if (rightPage <= pageCount)
				{
					pages[rightPage] = rightPage;
				}
			}

			// More side pages
			// [x] x x x ... x x x
			// x x x ... x x x [x]
			if (selectedPage == 1)
			{
				for (int i = 1; i < adjacentPages + 1; i++)
				{
					int rightPage = pageCount - i + 1;
					if (rightPage >= 1)
					{
						pages[rightPage] = rightPage;
					}
				}
			}
			else if (selectedPage == pageCount)
			{
				for (int i = 1; i < adjacentPages + 1; i++)
				{
					int leftPage = i;
					if (leftPage <= pageCount)
					{
						pages[leftPage] = leftPage;
					}
				}
			}

			return pages;
		}

		public virtual int CalculatePageCount(int totalCount, int pageSize)
		{
			return (totalCount + pageSize - 1) / pageSize;
		}
	}
}