﻿using System.ServiceProcess;

namespace Litium.Lekmer.RatingReviewMessaging.Service
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] ServicesToRun;

			ServicesToRun = new ServiceBase[] 
			{ 
				new RatingReviewMessagingServiceHost() 
			};

			ServiceBase.Run(ServicesToRun);
		}
	}
}
