﻿using System.Globalization;
using System.Text;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.BrandTopList.Web
{
	public class BlockBrandTopListControl : BlockControlBase<IBlockBrandTopList>
	{
		private readonly IBlockBrandTopListService _blockBrandTopListService;
		private readonly ITopListBrandService _topListBrandService;
		private readonly IContentNodeService _contentNodeService;
		private readonly IBlockBrandTopListCategoryService _blockBrandTopListCategoryService;
		private IPagingControl _pagingControl;
		private BrandCollection _brands;

		public BlockBrandTopListControl(ITemplateFactory templateFactory, IBlockBrandTopListService blockBrandTopListService, ITopListBrandService topListBrandService, IContentNodeService contentNodeService, IBlockBrandTopListCategoryService blockBrandTopListCategoryService)
			: base(templateFactory)
		{
			_blockBrandTopListService = blockBrandTopListService;
			_topListBrandService = topListBrandService;
			_contentNodeService = contentNodeService;
			_blockBrandTopListCategoryService = blockBrandTopListCategoryService;
		}

		protected override IBlockBrandTopList GetBlockById(int blockId)
		{
			return _blockBrandTopListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			_pagingControl.TotalCount = _brands.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("BrandList", RenderBrandList(), VariableEncoding.None);
			fragmentContent.AddVariable("Block.Title", Block.Title, VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			var brandIds = RenderBrandIdsArrayParam();
			string head = RenderFragment("Head", brandIds, pagingContent);
			string footer = RenderFragment("Footer", brandIds, pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private void Initialize()
		{
			_pagingControl = CreatePagingControl();
			_brands = _topListBrandService.GetAllByBlock(UserContext.Current, Block, _pagingControl.SelectedPage, _pagingControl.PageSize);
		}

		private string RenderFragment(string fragmentName, string brandIds, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.BrandsIds", brandIds);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderBrandList()
		{
			var grid = new BrandGridControl
			{
				Block = Block,
				ContentNodeService = _contentNodeService,
				BlockBrandTopListCategoryService = _blockBrandTopListCategoryService,
				Items = _brands,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "BrandList",
				RowFragmentName = "BrandRow",
				ItemFragmentName = "Brand",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		private IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();

			pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;

			pagingControl.Initialize();

			return pagingControl;
		}

		private string RenderBrandIdsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			if (_brands.Count > 0)//first or the only item in array
			{
				itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _brands[0].Id));
			}

			// from second element, if exist
			for (int i = 1; i < _brands.Count; i++)
			{
				itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", _brands[i].Id));
			}

			return itemBuilder.ToString();
		}
	}
}