﻿using System;
using System.Reflection;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Product.Monitor
{
	internal class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			Console.WriteLine("Monitor Product Console");
			Console.WriteLine("-----------------------");
			Console.WriteLine("Press any key to start the products monitoring process.");
			Console.ReadLine();
			Console.WriteLine("Monitor is working, please wait...");

			try
			{
				var monitorProductSetting = IoC.Resolve<IMonitorProductSetting>();
				var monitorProductService = IoC.Resolve<IMonitorProductService>();

				monitorProductService.Monitor(monitorProductSetting.PortionSize, monitorProductSetting.BreakDuration, monitorProductSetting.MinNumberInStockDefault);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_log.Error(ex);
			}

			Console.WriteLine("Done!");
			Console.ReadLine();
		}
	}
}