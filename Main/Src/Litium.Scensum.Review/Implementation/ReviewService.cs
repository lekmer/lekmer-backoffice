using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Review.Repository;

namespace Litium.Scensum.Review
{
	public class ReviewService : IReviewService
	{
		protected ReviewRepository Repository { get; private set; }
		protected IReviewStatusService ReviewStatusService { get; private set; }

		public ReviewService(ReviewRepository repository, IReviewStatusService reviewStatusService)
		{
			Repository = repository;
			ReviewStatusService = reviewStatusService;
		}

		public virtual IReview Create(IUserContext context)
		{
			if (ReviewStatusService == null)
			{
				throw new InvalidOperationException("ReviewStatusService must be set before calling Create.");
			}

			var reviewStatus = ReviewStatusService.GetByCommonName(context, "Pending");
			if (reviewStatus == null)
			{
				throw new BusinessObjectNotExistsException("Could not find a IReviewStatus with common name 'Pending'.");
			}

			var review = IoC.Resolve<IReview>();
			review.ReviewStatusId = reviewStatus.Id;
			review.CustomerId = context.Customer != null ? context.Customer.Id : (int?) null;
			review.CreatedDate = DateTime.Now;
			review.ChannelId = context.Channel.Id;
			return review;
		}

		public virtual ReviewCollection GetAllByProduct(IUserContext context, int productId, int pageNumber, int pageSize)
		{
			return GetAllByProduct(context, productId, pageNumber, pageSize, true);
		}

		public virtual ReviewCollection GetAllByProduct(IUserContext context, int productId, int pageNumber, int pageSize, bool cacheEnabled)
		{
			if (!cacheEnabled || pageNumber != 1)
			{
				return GetAllByProductCore(context, productId, pageNumber, pageSize);
			}
			return ReviewCollectionCache.Instance.TryGetItem(
				new ReviewCollectionKey(context.Channel.Id, productId),
				delegate { return GetAllByProductCore(context, productId, pageNumber, pageSize); });
		}

		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		protected virtual ReviewCollection GetAllByProductCore(IUserContext context, int productId, int pageNumber,
		                                                       int pageSize)
		{
			return Repository.GetAllByProduct(context.Channel.Id, productId, pageNumber, pageSize);
		}

		public virtual int Save(IUserContext context, IReview review)
		{
			if (review == null)
			{
				throw new ArgumentNullException("review");
			}

			ClearCache(context.Channel.Id, review.ProductId);
			return Repository.Save(review);
		}

		protected virtual void ClearCache(int channelId, int productId)
		{
			ReviewCollectionCache.Instance.Remove(new ReviewCollectionKey(channelId, productId));
			ReviewSummaryCache.Instance.Remove(new ReviewSummaryKey(channelId, productId));
		}
	}
}