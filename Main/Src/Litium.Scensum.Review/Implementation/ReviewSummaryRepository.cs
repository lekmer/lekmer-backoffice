﻿using System.Data;
using System.Diagnostics.CodeAnalysis;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Repository
{
	public class ReviewSummaryRepository
	{
		protected virtual DataMapperBase<IReviewSummary> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IReviewSummary>(dataReader);
		}

		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual IReviewSummary GetByProduct(int channelId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ReviewSummaryRepository.GetByProduct");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pReviewSummaryGetByProduct]",
				                                                         parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}