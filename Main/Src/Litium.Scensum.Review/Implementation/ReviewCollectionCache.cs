﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Scensum.Review
{
	public sealed class ReviewCollectionCache : ScensumCacheBase<ReviewCollectionKey, ReviewCollection>
	{
		#region Singleton

		private static readonly ReviewCollectionCache _instance = new ReviewCollectionCache();

		private ReviewCollectionCache()
		{
		}

		public static ReviewCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class ReviewCollectionKey : ICacheKey
	{
		public ReviewCollectionKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public int ChannelId { get; set; }

		public int ProductId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + ProductId; }
		}
	}
}