using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Mapper
{
	public class ReviewStatusDataMapper : DataMapperBase<IReviewStatus>
	{
		public ReviewStatusDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IReviewStatus Create()
		{
			var reviewStatus = IoC.Resolve<IReviewStatus>();
			reviewStatus.Id = MapValue<int>("ReviewStatus.Id");
			reviewStatus.Title = MapValue<string>("ReviewStatus.Title");
			reviewStatus.CommonName = MapValue<string>("ReviewStatus.CommonName");
			return reviewStatus;
		}
	}
}