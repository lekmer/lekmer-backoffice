using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Mapper
{
	public class ReviewSummaryDataMapper : DataMapperBase<IReviewSummary>
	{
		public ReviewSummaryDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IReviewSummary Create()
		{
			var summary = IoC.Resolve<IReviewSummary>();
			summary.AverageRating = MapNullableValue<decimal?>("AverageRating");
			summary.ReviewCount = MapValue<int>("ReviewCount");
			return summary;
		}
	}
}