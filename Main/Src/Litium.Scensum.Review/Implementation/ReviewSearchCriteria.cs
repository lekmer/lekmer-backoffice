using System;

namespace Litium.Scensum.Review
{
	[Serializable]
	public class ReviewSearchCriteria : IReviewSearchCriteria
	{
		private string _productTitle;

		public string ProductTitle
		{
			get { return string.IsNullOrEmpty(_productTitle) ? null : _productTitle; }
			set { _productTitle = value; }
		}


		private string _productId;

		public string ProductId
		{
			get { return string.IsNullOrEmpty(_productId) ? null : _productId; }
			set { _productId = value; }
		}

		private string _message;

		public string Message
		{
			get { return string.IsNullOrEmpty(_message) ? null : _message; }
			set { _message = value; }
		}

		private string _author;

		public string Author
		{
			get { return string.IsNullOrEmpty(_author) ? null : _author; }
			set { _author = value; }
		}

		private string _statusId;

		public string StatusId
		{
			get { return string.IsNullOrEmpty(_statusId) ? null : _statusId; }
			set { _statusId = value; }
		}

		private string _createdFrom;

		public string CreatedFrom
		{
			get { return string.IsNullOrEmpty(_createdFrom) ? null : _createdFrom; }
			set { _createdFrom = value; }
		}

		private string _createdTo;

		public string CreatedTo
		{
			get { return string.IsNullOrEmpty(_createdTo) ? null : _createdTo; }
			set { _createdTo = value; }
		}

		private string _sortBy;

		public string SortBy
		{
			get { return string.IsNullOrEmpty(_sortBy) ? null : _sortBy; }
			set { _sortBy = value; }
		}

		private string _sortByDescending;

		public string SortByDescending
		{
			get { return string.IsNullOrEmpty(_sortByDescending) ? null : _sortByDescending; }
			set { _sortByDescending = value; }
		}
	}
}