using System;
using Litium.Framework.Transaction;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Review.Repository;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Review
{
	public class BlockProductReviewSecureService : IBlockProductReviewSecureService, IBlockCreateSecureService,
	                                               IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductReviewRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }

		public BlockProductReviewSecureService(
			IAccessValidator accessValidator,
			BlockProductReviewRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
		}

		public virtual IBlockProductReview Create()
		{
			if (AccessSecureService == null)
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockProductReview = IoC.Resolve<IBlockProductReview>();
			blockProductReview.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductReview.Status = BusinessObjectStatus.New;
			return blockProductReview;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId,
		                              string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockProductReview = Create();
			blockProductReview.ContentNodeId = contentNodeId;
			blockProductReview.ContentAreaId = contentAreaId;
			blockProductReview.BlockTypeId = blockTypeId;
			blockProductReview.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockProductReview.RowCount = 1;
			blockProductReview.Title = title;
			blockProductReview.TemplateId = null;
			blockProductReview.Id = Save(systemUserFull, blockProductReview);
			return blockProductReview;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductReview block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null)
				throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				Repository.Save(block);

				transactedOperation.Complete();
			}
			BlockProductReviewCache.Instance.Remove(block.Id);
			ReviewCollectionCache.Instance.Flush();
			return block.Id;
		}

		public virtual IBlockProductReview GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, Core.PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockProductReviewCache.Instance.Remove(blockId);
		}
	}
}