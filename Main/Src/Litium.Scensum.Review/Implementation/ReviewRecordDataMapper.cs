using System.Data;

namespace Litium.Scensum.Review.Mapper
{
	public class ReviewRecordDataMapper : ReviewDataMapper<IReviewRecord>
	{
		public ReviewRecordDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IReviewRecord Create()
		{
			IReviewRecord reviewRecord = base.Create();
			reviewRecord.ProductTitle = MapValue<string>("Product.Title");
			reviewRecord.UserName = MapNullableValue<string>("Customer.UserName");
			return reviewRecord;
		}
	}
}