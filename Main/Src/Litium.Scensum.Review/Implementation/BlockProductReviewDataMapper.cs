using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Review.Mapper
{
	public class BlockProductReviewDataMapper : DataMapperBase<IBlockProductReview>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockProductReviewDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockProductReview Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductReview = IoC.Resolve<IBlockProductReview>();
			block.ConvertTo(blockProductReview);
			blockProductReview.RowCount = MapValue<int>("BlockProductReview.RowCount");
			return blockProductReview;
		}
	}
}