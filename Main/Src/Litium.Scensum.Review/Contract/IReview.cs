using System;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review
{
	public interface IReview : IBusinessObjectBase
	{
		int Id { get; set; }

		int ProductId { get; set; }

		int ReviewStatusId { get; set; }

		int? CustomerId { get; set; }

		byte Rating { get; set; }

		string Author { get; set; }

		string Message { get; set; }

		DateTime CreatedDate { get; set; }

		int ChannelId { get; set; }
	}
}