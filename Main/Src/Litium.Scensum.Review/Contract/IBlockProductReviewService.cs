﻿using Litium.Scensum.Core;

namespace Litium.Scensum.Review
{
	public interface IBlockProductReviewService
	{
		IBlockProductReview GetById(IUserContext context, int id);
	}
}