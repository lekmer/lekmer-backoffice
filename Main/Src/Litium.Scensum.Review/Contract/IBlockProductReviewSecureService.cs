using Litium.Scensum.Core;

namespace Litium.Scensum.Review
{
	public interface IBlockProductReviewSecureService
	{
		IBlockProductReview Create();

		int Save(ISystemUserFull systemUserFull, IBlockProductReview block);

		IBlockProductReview GetById(int id);
	}
}