using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Scensum.Review
{
	public interface IReviewSecureService
	{
		ReviewRecordCollection Search(IReviewSearchCriteria searchCriteria, int page, int pageSize);

		void SetStatus(ISystemUserFull user, int reviewId, int statusId);

		void SetStatus(ISystemUserFull user, IEnumerable<int> reviewIds, int statusId);

		void Delete(ISystemUserFull user, int reviewId);

		void Delete(ISystemUserFull user, IEnumerable<int> reviewIds);
	}
}