using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Review
{
	public interface IBlockProductReview : IBlock
	{
		int RowCount { get; set; }
	}
}