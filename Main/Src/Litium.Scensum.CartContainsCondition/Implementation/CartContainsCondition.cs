﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Scensum.CartContainsCondition
{
	[Serializable]
	public class CartContainsCondition : LekmerCondition, ICartContainsCondition
	{
		private int _minQuantity;
		private bool _allowDuplicates;
		private bool _includeAllProducts;

		private ProductIdDictionary _includeProducts;
		private ProductIdDictionary _excludeProducts;

		private CampaignCategoryDictionary _includeCategories;
		private CampaignCategoryDictionary _excludeCategories;

		private BrandIdDictionary _includeBrands;
		private BrandIdDictionary _excludeBrands;


		public int MinQuantity
		{
			get { return _minQuantity; }
			set
			{
				CheckChanged(_minQuantity, value);
				_minQuantity = value;
			}
		}

		public bool AllowDuplicates
		{
			get { return _allowDuplicates; }
			set
			{
				CheckChanged(_allowDuplicates, value);
				_allowDuplicates = value;
			}
		}

		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}

		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}

		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}

		public CampaignCategoryDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}

		public CampaignCategoryDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}

		public BrandIdDictionary IncludeBrands
		{
			get { return _includeBrands; }
			set
			{
				CheckChanged(_includeBrands, value);
				_includeBrands = value;
			}
		}

		public BrandIdDictionary ExcludeBrands
		{
			get { return _excludeBrands; }
			set
			{
				CheckChanged(_excludeBrands, value);
				_excludeBrands = value;
			}
		}

		public override bool Fulfilled(IUserContext context)
		{
			var cart = context.Cart;
			if (cart == null)
			{
				return false;
			}

			var items = new Collection<ICartItem>();
			foreach (var item in cart.GetCartItems())
			{
				if (!IsTargetType(item.Product)) continue; // skip

				if (!IsRangeMember(item.Product)) continue; // skip

				items.Add(item);
			}

			var quantity = AllowDuplicates
				? items.Sum(i => i.Quantity)
				: items.GroupBy(i => i.Product.Id).Count();

			return quantity >= MinQuantity;
		}

		protected virtual bool IsRangeMember(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");
			if (ExcludeProducts == null) throw new InvalidOperationException("The ExcludeProducts property must be set.");
			if (IncludeCategories == null) throw new InvalidOperationException("The IncludeCategories property must be set.");
			if (ExcludeCategories == null) throw new InvalidOperationException("The ExcludeCategories property must be set.");
			if (IncludeBrands == null) throw new InvalidOperationException("The IncludeBrands property must be set.");
			if (ExcludeBrands == null) throw new InvalidOperationException("The ExcludeBrands property must be set.");

			var lekmerProduct = (ILekmerProduct) product;

			if (ExcludeCategories.ContainsKey(product.CategoryId))
			{
				return false;
			}

			if (lekmerProduct.BrandId.HasValue
				&& ExcludeBrands.ContainsKey(lekmerProduct.BrandId.Value))
			{
				return false;
			}

			if (ExcludeProducts.ContainsKey(product.Id))
			{
				return false;
			}

			if (IncludeAllProducts)
			{
				return true;
			}

			if (IncludeCategories.ContainsKey(product.CategoryId))
			{
				return true;
			}

			if (lekmerProduct.BrandId.HasValue
				&& IncludeBrands.ContainsKey(lekmerProduct.BrandId.Value))
			{
				return true;
			}

			if (IncludeProducts.ContainsKey(product.Id))
			{
				return true;
			}

			return false;
		}

		protected override bool IsTargetType(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");

			// When product resides in "IncludeProducts" list explicitly, we ignore target options and return true !!!
			if (IncludeAllProducts == false)
			{
				if (IncludeProducts.ContainsKey(product.Id))
				{
					return true;
				}
			}

			return base.IsTargetType(product);
		}

		public override object[] GetInfoArguments()
		{
			return new object[] { MinQuantity, AllowDuplicates ? string.Empty : " unique" };
		}
	}
}
