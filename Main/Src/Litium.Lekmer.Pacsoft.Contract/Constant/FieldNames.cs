﻿using System.ComponentModel;

namespace Litium.Lekmer.Pacsoft.Contract
{
	public enum FieldNames
	{
		[Description("quickid")]
		quickid = 1,
		[Description("name")]
		name = 2,
		[Description("address1")]
		address1 = 3,
		[Description("address2")]
		address2 = 4,
		[Description("zipcode")]
		zipcode = 5,
		[Description("city")]
		city = 6,
		[Description("country")]
		country = 7,
		[Description("email")]
		email = 8,
		[Description("phone")]
		phone = 9,
		[Description("sms")]
		sms = 10
	}
}