using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class NodeTreeReplicationIndex
	{
		private Dictionary<int, int> _sourceAndReplicatedItems = new Dictionary<int, int>();
		private Dictionary<int, int> _replicatedAndSourceItems = new Dictionary<int, int>();
		private Dictionary<int, IContentNode> _replicatedItems = new Dictionary<int, IContentNode>();

		public void AddReplication(IContentNode sourceNode, IContentNode replicatedNode)
		{
			if (replicatedNode.Id > 0)
			{
				_sourceAndReplicatedItems[sourceNode.Id] = replicatedNode.Id;
				_replicatedAndSourceItems[replicatedNode.Id] = sourceNode.Id;
				_replicatedItems[replicatedNode.Id] = replicatedNode;
			}
		}

		public int FindReplicatedId(IContentNode sourceNode)
		{
			return _sourceAndReplicatedItems[sourceNode.Id];
		}

		public string GetReplicatedCommonName(IContentNode sourceNode)
		{
			return sourceNode.CommonName + "##" + sourceNode.Id;
		}

		public string GetReplicatedUrlTitle(IContentPage sourcePage)
		{
			return sourcePage.UrlTitle + "##" + sourcePage.Id;
		}

		public string GetNormalCommonName(string replicatedCommonName)
		{
			int index = replicatedCommonName.IndexOf("##", System.StringComparison.Ordinal);
			if (index >= 0)
			{
				string normalCommonName = replicatedCommonName.Substring(0, index);
				if (string.IsNullOrEmpty(normalCommonName))
				{
					return null;
				}
				return normalCommonName;
			}

			return replicatedCommonName;
		}

		public string GetNormalUrlTitle(string replicatedUrlTitle)
		{
			int index = replicatedUrlTitle.IndexOf("##", System.StringComparison.Ordinal);
			if (index >= 0)
			{
				string normalUrlTitle = replicatedUrlTitle.Substring(0, index);
				return normalUrlTitle;
			}

			return replicatedUrlTitle;
		}

		public void Initialize(Collection<IContentNode> replicatedNodes)
		{
			foreach (IContentNode replicatedNode in replicatedNodes)
			{
				int sourceNodeId = GetSourceIdFromReplicatedCommonName(replicatedNode.CommonName);

				if (sourceNodeId > 0)
				{
					_sourceAndReplicatedItems[sourceNodeId] = replicatedNode.Id;
					_replicatedAndSourceItems[replicatedNode.Id] = sourceNodeId;
					_replicatedItems[replicatedNode.Id] = replicatedNode;
				}
			}
		}

		public int GetSourceIdFromReplicatedCommonName(string commonName)
		{
			if (string.IsNullOrEmpty(commonName))
			{
				return -1;
			}

			int index = commonName.IndexOf("##", System.StringComparison.Ordinal);
			if (index >= 0)
			{
				int id;
				string idValue = commonName.Substring(index + 2);
				if (int.TryParse(idValue, out id))
				{
					return id;
				}
			}

			return -1;
		}

		public IContentNode GetReplicatedNode(int sourceNodeId)
		{
			if (_sourceAndReplicatedItems.ContainsKey(sourceNodeId))
			{
				int replicatedNodeId = _sourceAndReplicatedItems[sourceNodeId];
				if (_replicatedItems.ContainsKey(replicatedNodeId))
				{
					return _replicatedItems[replicatedNodeId];
				}
			}

			return null;
		}

		public int GetReplicatedNodeId(int sourceNodeId)
		{
			if (_sourceAndReplicatedItems.ContainsKey(sourceNodeId))
			{
				return _sourceAndReplicatedItems[sourceNodeId];
			}

			return -1;
		}
	}
}