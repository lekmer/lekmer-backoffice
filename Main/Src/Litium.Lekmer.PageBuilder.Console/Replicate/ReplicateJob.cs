using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class ReplicateJob : JobBase
	{
		private readonly IChannelSecureService _channelSecureService = IoC.Resolve<IChannelSecureService>();
		private readonly ISiteStructureRegistrySecureService _siteStructureRegistrySecureService = IoC.Resolve<ISiteStructureRegistrySecureService>();
		private readonly ISystemUserSecureService _systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();

		private int _siteStructureRegistryId;
		private int _siteStructureRegistryIdSource;
		private int _languageId;
		private ISystemUserFull _systemUser;

		public ReplicateJob(Configuration configuration)
			: base(configuration)
		{
		}

		protected override void ExecuteCore()
		{
			var nodeTreeReplicateJob = new NodeTreeReplicateJob(Configuration, _systemUser, _siteStructureRegistryId, _siteStructureRegistryIdSource);
			nodeTreeReplicateJob.Execute();

			var brandReplicateJob = new BrandReplicateJob(Configuration, _systemUser, _siteStructureRegistryId, _siteStructureRegistryIdSource);
			brandReplicateJob.Execute();

			var categoryReplicateJob = new CategoryReplicateJob(Configuration, _systemUser, _languageId, _siteStructureRegistryId, _siteStructureRegistryIdSource);
			categoryReplicateJob.Execute();

			var blockReplicateJob = new BlockReplicateJob(Configuration, _systemUser, _siteStructureRegistryId, _siteStructureRegistryIdSource);
			blockReplicateJob.Execute();

			var nodeTreeAfterReplicateJob = new NodeTreeAfterReplicateJob(Configuration, _systemUser, _siteStructureRegistryId, _siteStructureRegistryIdSource);
			nodeTreeAfterReplicateJob.Execute();
		}

		internal override void Initialize()
		{
			IChannel channel = _channelSecureService.GetById(Configuration.ChannelId);
			_languageId = channel.Language.Id;

			ISiteStructureRegistry siteStructureRegistry = _siteStructureRegistrySecureService.GetByChannel(Configuration.ChannelId);
			_siteStructureRegistryId = siteStructureRegistry.Id;

			ISiteStructureRegistry siteStructureRegistrySource = _siteStructureRegistrySecureService.GetByChannel(Configuration.ReplicateConfiguration.ChannleIdSource);
			_siteStructureRegistryIdSource = siteStructureRegistrySource.Id;

			_systemUser = _systemUserSecureService.GetFullByUserName(Configuration.SystemUserName);
		}
	}
}