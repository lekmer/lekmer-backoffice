using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class NodeTreeReplicateJob : JobBase
	{
		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
		private readonly IContentNodeShortcutLinkSecureService _contentNodeShortcutLinkSecureService = IoC.Resolve<IContentNodeShortcutLinkSecureService>();
		private readonly IContentNodeUrlLinkSecureService _contentNodeUrlLinkSecureService = IoC.Resolve<IContentNodeUrlLinkSecureService>();
		private readonly IContentPageSecureService _contentPageSecureService = IoC.Resolve<IContentPageSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _siteStructureRegistryId;
		private readonly int _siteStructureRegistryIdSource;
		private NodeTreeReplicationIndex _replicationIndex;

		public NodeTreeReplicateJob(Configuration configuration, ISystemUserFull systemUser, int siteStructureRegistryId, int siteStructureRegistryIdSource)
			: base(configuration)
		{
			_systemUser = systemUser;
			_siteStructureRegistryId = siteStructureRegistryId;
			_siteStructureRegistryIdSource = siteStructureRegistryIdSource;
		}

		internal override void Initialize()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			_replicationIndex = new NodeTreeReplicationIndex();

			Collection<IContentNode> replicatedNodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryId);
			_replicationIndex.Initialize(replicatedNodes);
		}

		protected override void ExecuteCore()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			Console.WriteLine("-replication starts");

			Collection<IContentNode> nodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryIdSource);

			foreach (IContentNode node in nodes)
			{
				ReplicateNode(node);
			}

			foreach (IContentNode node in nodes)
			{
				FixReplicatedNodeCrossReferences(node);
			}

			foreach (IContentNode node in nodes)
			{
				if (node.ContentNodeTypeId == (int)ContentNodeTypeInfo.Page)
				{
					FixReplicatedPage(node);
				}
			}

			foreach (IContentNode node in nodes)
			{
				if (node.ContentNodeTypeId == (int)ContentNodeTypeInfo.ShortcutLink)
				{
					FixReplicatedShortcutLink(node);
				}
			}

			Console.WriteLine("-replication done");
		}


		private void ReplicateNode(IContentNode node)
		{
			IContentNode replicatedNode = null;

			switch (node.ContentNodeTypeId)
			{
				case (int)ContentNodeTypeInfo.ShortcutLink:
					replicatedNode = ReplicateNodeShortcutLink(node);
					break;
				case (int)ContentNodeTypeInfo.UrlLink:
					replicatedNode = ReplicateNodeUrlLink(node);
					break;
				case (int)ContentNodeTypeInfo.Page:
					replicatedNode = ReplicateNodePage(node);
					break;
				case (int)ContentNodeTypeInfo.Folder:
					replicatedNode = ReplicateNodeFolder(node);
					break;
			}

			if (replicatedNode != null && replicatedNode.Id > 0)
			{
				_replicationIndex.AddReplication(node, replicatedNode);
			}
			else
			{
				Console.WriteLine("--Warning, some node is not replicated !!!");
			}
		}

		private IContentNode ReplicateNodeShortcutLink(IContentNode node)
		{
			IContentNodeShortcutLink contentNodeShortcutLink = _contentNodeShortcutLinkSecureService.GetById(node.Id);

			IContentNodeShortcutLink replicatedNodeShortcutLink = _contentNodeShortcutLinkSecureService.Create();

			replicatedNodeShortcutLink.LinkContentNodeId = null; //TODO

			ReplicateNodeProperties(contentNodeShortcutLink, replicatedNodeShortcutLink);

			replicatedNodeShortcutLink.Id = _contentNodeShortcutLinkSecureService.Save(_systemUser, replicatedNodeShortcutLink);
			return replicatedNodeShortcutLink;
		}

		private IContentNode ReplicateNodeUrlLink(IContentNode node)
		{
			IContentNodeUrlLink contentNodeUrlLink = _contentNodeUrlLinkSecureService.GetById(node.Id);

			IContentNodeUrlLink replicatedUrlLink = _contentNodeUrlLinkSecureService.Create();

			replicatedUrlLink.LinkUrl = contentNodeUrlLink.LinkUrl;

			ReplicateNodeProperties(contentNodeUrlLink, replicatedUrlLink);

			replicatedUrlLink.Id = _contentNodeUrlLinkSecureService.Save(_systemUser, replicatedUrlLink);
			return replicatedUrlLink;
		}

		private IContentNode ReplicateNodePage(IContentNode node)
		{
			IContentPage contentPage = _contentPageSecureService.GetById(node.Id);

			IContentPage replicatedPage = _contentPageSecureService.Create();

			replicatedPage.TemplateId = contentPage.TemplateId;
			replicatedPage.MasterTemplateId = contentPage.MasterTemplateId;
			replicatedPage.UrlTitle = _replicationIndex.GetReplicatedUrlTitle(contentPage);
			replicatedPage.ContentPageTypeId = contentPage.ContentPageTypeId;
			replicatedPage.MasterPageId = null;
			replicatedPage.IsMaster = contentPage.IsMaster;

			ReplicateNodeProperties(contentPage, replicatedPage);

			replicatedPage.Id = _contentPageSecureService.Save(_systemUser, replicatedPage);
			return replicatedPage;
		}

		private IContentNode ReplicateNodeFolder(IContentNode node)
		{
			IContentNode replicatedFolder = _contentNodeSecureService.Create();

			ReplicateNodeProperties(node, replicatedFolder);

			replicatedFolder.Id = _contentNodeSecureService.Save(_systemUser, replicatedFolder);
			return replicatedFolder;
		}

		private void ReplicateNodeProperties(IContentNode sourceNode, IContentNode replicatedNode)
		{
			replicatedNode.ContentNodeStatusId = sourceNode.ContentNodeStatusId;
			replicatedNode.ContentNodeTypeId = sourceNode.ContentNodeTypeId;
			replicatedNode.Ordinal = sourceNode.Ordinal;
			replicatedNode.Title = sourceNode.Title;
			replicatedNode.CommonName = _replicationIndex.GetReplicatedCommonName(sourceNode);
			replicatedNode.AccessId = sourceNode.AccessId;

			replicatedNode.SiteStructureRegistryId = _siteStructureRegistryId;
			replicatedNode.ParentContentNodeId = null;
		}


		private void FixReplicatedNodeCrossReferences(IContentNode sourceNode)
		{
			if (sourceNode.ParentContentNodeId.HasValue)
			{
				IContentNode replicatedNode = _replicationIndex.GetReplicatedNode(sourceNode.Id);

				replicatedNode.ParentContentNodeId = _replicationIndex.GetReplicatedNodeId(sourceNode.ParentContentNodeId.Value);

				_contentNodeSecureService.UpdateParent(replicatedNode.Id, replicatedNode.ParentContentNodeId);
			}
		}

		private void FixReplicatedPage(IContentNode sourceNode)
		{
			IContentPage sourcePage = _contentPageSecureService.GetById(sourceNode.Id);
			IContentPage replicatedPage;

			IContentNode replicatedNode = _replicationIndex.GetReplicatedNode(sourceNode.Id);
			if (!(replicatedNode is IContentPage))
			{
				replicatedPage = _contentPageSecureService.GetById(replicatedNode.Id);
			}
			else
			{
				replicatedPage = (IContentPage) replicatedNode;
			}

			replicatedPage.UrlTitle = _replicationIndex.GetNormalUrlTitle(replicatedPage.UrlTitle);

			if (sourcePage.MasterPageId.HasValue)
			{
				replicatedPage.MasterPageId = _replicationIndex.GetReplicatedNodeId(sourcePage.MasterPageId.Value);
			}

			_contentPageSecureService.Save(_systemUser, replicatedPage);
		}

		private void FixReplicatedShortcutLink(IContentNode sourceNode)
		{
			IContentNodeShortcutLink sourceNodeShortcutLink = _contentNodeShortcutLinkSecureService.GetById(sourceNode.Id);

			if (sourceNodeShortcutLink.LinkContentNodeId.HasValue)
			{
				IContentNode replicatedNode = _replicationIndex.GetReplicatedNode(sourceNode.Id);
				IContentNodeShortcutLink replicatedNodeShortcutLink;

				if (!(replicatedNode is IContentNodeShortcutLink))
				{
					replicatedNodeShortcutLink = _contentNodeShortcutLinkSecureService.Create();
					replicatedNodeShortcutLink.Id = replicatedNode.Id;
					ReplicateNodeProperties(sourceNode, replicatedNodeShortcutLink);
				}
				else
				{
					replicatedNodeShortcutLink = (IContentNodeShortcutLink)replicatedNode;
				}

				replicatedNodeShortcutLink.LinkContentNodeId = _replicationIndex.GetReplicatedNodeId(sourceNodeShortcutLink.LinkContentNodeId.Value);

				_contentNodeShortcutLinkSecureService.Save(_systemUser, replicatedNodeShortcutLink);
			}
		}
	}
}