﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.TopList;

namespace Litium.Lekmer.PageBuilder
{
	internal static class ConfigurationFactory
	{
		internal static IEnumerable<Configuration> GetConfigurations()
		{
			//return GetHeppoConfigurations();
			//return GetLekmerConfigurations();
			//return GetHeppoReplicationConfigurations();

			return GetLekmerReplicationConfigurations();

			//return GetLekmerNewCategoryStructureConfigurations();

			//return new Configuration[0];
		}


		private static IEnumerable<Configuration> GetHeppoConfigurations()
		{
			const string systemUserName = "admin@litium.se";

			var parentCategoryIdList = new List<int?> { 1000064 }; //1000059;
			int? categoryMasterPageId = null;
			const int categoryContentNodeTemplateId = 1000024;
			const int categoryFilterBlockAreaId = 1000010;
			const int categoryFilterBlockOrdinal = 1;
			const int categoryFilterBlockTypeId = 1000001;
			const int categoryFilterBlockTemplateId = 1000343; //1000058;
			const int categoryFilterBlockSecondaryTemplateId = 1000343; //1000025;
			const string categoryFilterBlockTitle = "Lista";

			var brandListBlockTemplate = new BlockBrandList
			{
				ContentAreaId = 1000018,
				Ordinal = 10,
				BlockTypeId = 18,
				Title = "Brand profile",
				TemplateId = 1000049,
				Setting = new BlockSetting
					{
						ColumnCount = 1,
						RowCount = 1,
						TotalItemCount = 10000
					},
				IncludeAllBrands = false
			};
			var brandList2BlockTemplate = new BlockBrandList
			{
				ContentAreaId = 1000018,
				Ordinal = 30,
				BlockTypeId = 18,
				Title = "Brand info",
				TemplateId = 1000095,
				Setting = new BlockSetting
					{
						ColumnCount = 1,
						RowCount = 1,
						TotalItemCount = 10000
					},
				IncludeAllBrands = false
			};
			var richTextBlockTemplate = new LekmerBlockRichText
			{
				ContentAreaId = 1000018,
				Ordinal = 20,
				BlockTypeId = 1,
				Title = "Image",
				TemplateId = 1000096,
				Content = string.Empty
			};
			var femaleProductFilterBlockTemplate = new BlockProductFilter
			{
				ContentAreaId = 1000010,
				Ordinal = 1,
				BlockTypeId = 1000001,
				Title = "Brand/category filter",
				TemplateId = null,
				SecondaryTemplateId = null,
				DefaultCategoryId = 1000059,
				DefaultTagIds = new Collection<int> { 1000010 }
			};
			var maleProductFilterBlockTemplate = new BlockProductFilter
			{
				ContentAreaId = 1000010,
				Ordinal = 1,
				BlockTypeId = 1000001,
				Title = "Brand/category filter",
				TemplateId = null,
				SecondaryTemplateId = null,
				DefaultCategoryId = 1000059,
				DefaultTagIds = new Collection<int> { 1000009 }
			};
			var childProductFilterBlockTemplate = new BlockProductFilter
			{
				ContentAreaId = 1000010,
				Ordinal = 1,
				BlockTypeId = 1000001,
				Title = "Brand/category filter",
				TemplateId = 1000313,
				SecondaryTemplateId = null,
				DefaultCategoryId = 1000059,
				DefaultTagIds = new Collection<int>()
			};

			BlockProductFilter womenCategoryProductFilterBlockTemplate = new BlockProductFilter
			{
				ContentAreaId = 1000010,
				Ordinal = 1,
				BlockTypeId = 1000001,
				Title = "category filter",
				TemplateId = null, // 1000313 fel
				SecondaryTemplateId = null, // fel дndras sen
				DefaultCategoryId = 1000064,
				DefaultTagIds = new Collection<int> { 1000010 },
				DefaultSort = "IsNewFrom"
			};


			var femaleSubPageConfiguration = new BrandSubPageConfiguration
			{
				Ordinal = 10,
				Title = "-",
				TemplateId = 1000024,
				MasterTemplateId = 1000050,
				SubPageFilterBlockTemplate = ObjectCopier.Clone(femaleProductFilterBlockTemplate)
			};
			var maleSubPageConfiguration = new BrandSubPageConfiguration
			{
				Ordinal = 10,
				Title = "-",
				TemplateId = 1000024,
				MasterTemplateId = 1000050,
				SubPageFilterBlockTemplate = ObjectCopier.Clone(maleProductFilterBlockTemplate)
			};
			var childSubPageConfiguration = new BrandSubPageConfiguration
			{
				Ordinal = 10,
				Title = "-",
				TemplateId = 1000024,
				MasterTemplateId = 1000050,
				SubPageFilterBlockTemplate = ObjectCopier.Clone(childProductFilterBlockTemplate)
			};

			var frenchConfiguration =
				new Configuration
				{
					ChannelId = 1000015,
					SystemUserName = systemUserName,
					CategoryConfiguration = null,
					/*CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryId = parentCategoryId,
						ParentContentNodeId = 1012031, //1002684,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					}, */
					//BrandConfiguration = null
					BrandConfiguration =
						new BrandConfiguration
						{
							ParentContentNodeId = 1008027,
							MasterPageId = 1008029, //1000057,
							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
							SubPageConfigurations =
								new[]
											{
												ObjectCopier.Clone(femaleSubPageConfiguration)
													.Set(block => block.Title = "{0} pour femmes"),
												ObjectCopier.Clone(maleSubPageConfiguration)
													.Set(block => block.Title = "{0} pour hommes"),
												ObjectCopier.Clone(childSubPageConfiguration)
													.Set(block => block.Title = "{0} pour enfants"),
											}
						}
				};


			var swedishConfiguration =
				new Configuration
				{
					ChannelId = 1,
					SystemUserName = systemUserName,
					CategoryConfiguration = null,
					BrandConfiguration =
						new BrandConfiguration
						{
							ParentContentNodeId = 1000080, //1001004,
							MasterPageId = 1000057,
							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
							SubPageConfigurations =
								new[]
											{
												ObjectCopier.Clone(femaleSubPageConfiguration)
													.Set(block => block.Title = "{0} fцr kvinnor"),
												ObjectCopier.Clone(maleSubPageConfiguration)
													.Set(block => block.Title = "{0} fцr mдn")
											}
						}
				};


			var norweiganConfiguration =
				new Configuration
				{
					ChannelId = 4,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryIdList = parentCategoryIdList,
						ParentContentNodeId = 1002684,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					},
					BrandConfiguration =
						new BrandConfiguration
						{
							ParentContentNodeId = 1001004,
							MasterPageId = 1000057,
							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
							SubPageConfigurations =
								new[]
											{
												ObjectCopier.Clone(femaleSubPageConfiguration)
													.Set(block => block.Title = "{0} fцr kvinnor"),
												ObjectCopier.Clone(maleSubPageConfiguration)
													.Set(block => block.Title = "{0} fцr mдn"),
											}
						}
				};

			var danishConfiguration =
				new Configuration
				{
					ChannelId = 657,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryIdList = parentCategoryIdList,
						ParentContentNodeId = 1002685,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					},
					BrandConfiguration = null
					//						new BrandConfiguration
					//						{
					//							ParentContentNodeId = 1001637,
					//							MasterPageId = 1001322,
					//							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
					//							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
					//							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
					//							SubPageConfigurations =
					//								new[]
					//											{
					//												ObjectCopier.Clone(femaleSubPageConfiguration)
					//													.Set(block => block.Title = "{0} til kvinder"),
					//												ObjectCopier.Clone(maleSubPageConfiguration)
					//													.Set(block => block.Title = "{0} for mжnd"),
					//											}
					//						}
				};

			var finnishConfiguration =
				new Configuration
				{
					ChannelId = 1000003,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryIdList = parentCategoryIdList,
						ParentContentNodeId = 1002686,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					},
					BrandConfiguration = null
					//						new BrandConfiguration
					//						{
					//							ParentContentNodeId = 1001638,
					//							MasterPageId = 1001321,
					//							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
					//							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
					//							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
					//							SubPageConfigurations =
					//								new[]
					//											{
					//												ObjectCopier.Clone(femaleSubPageConfiguration)
					//													.Set(block => block.Title = "{0} naisten kengдt"),
					//												ObjectCopier.Clone(maleSubPageConfiguration)
					//													.Set(block => block.Title = "{0} miesten kengдt"),
					//											}
					//						}
				};

			return
				new[]
					{
						//swedishConfiguration,
						//norweiganConfiguration
						//danishConfiguration,
						//finnishConfiguration
						frenchConfiguration
					};
		}

		private static IEnumerable<Configuration> GetLekmerConfigurations()
		{
			var parentCategoryIdList = new List<int?> { null };
			const string systemUserName = "admin@litium.se";

			const int categoryFilterBlockTemplateId = 1000022;
			const int categoryFilterBlockSecondaryTemplateId = 1000025;
			const string categoryFilterBlockTitle = "Category filter";

			const int brandFilterBlockTemplateId = 1000022;
			const int brandFilterBlockSecondaryTemplateId = 1000025;
			const string brandFilterBlockTitle = "Brand filter";

			const int brandListBlockTemplateId = 1000047;
			const string brandListBlockTitle = "Brand info";
			const int brandListColumnCount = 1;
			const int brandListRowCount = 1;
			const bool brandListIncludeAllBrands = false;

			const int menuBlockTemplateId = 1000048;
			const string menuBlockTitle = "Brand menu";

			const int topListBlockTemplateId = 1000039;
			const string topListBlockTitle = "Brand top list";
			const int topListColumnCount = 1;
			const int topListRowCount = 5;
			const bool topListIncludeAllCategories = false;
			const int topListOrderStatisticsDayCount = 100;
			const int topListTotalProductCount = 30;

			return
				new[]
					{
						new Configuration
							{
								ChannelId = 1,
								SystemUserName = systemUserName,
								CategoryConfiguration =
									new CategoryConfiguration
										{
											ParentCategoryIdList = parentCategoryIdList,
											ParentContentNodeId = 1000091,
											MasterPageId = 2252,
											FilterBlockAreaId = 1000001,
											FilterBlockOrdinal = 1,
											FilterBlockTypeId = 1000001,
											FilterBlockTitle = categoryFilterBlockTitle,
											FilterBlockTemplateId = categoryFilterBlockTemplateId,
											FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
										},
								BrandConfiguration =
									new BrandConfiguration
										{
											ParentContentNodeId = 1000540,
											MasterPageId = 1000541,
											FilterBlockTemplate = new BlockProductFilter
																	{
																		ContentAreaId = 1000011,
																		Ordinal = 1,
																		BlockTypeId = 1000001,
																		Title = brandFilterBlockTitle,
																		TemplateId = brandFilterBlockTemplateId,
																		SecondaryTemplateId = brandFilterBlockSecondaryTemplateId
																	},
											BrandListBlockTemplate = new BlockBrandList
																		{
																			ContentAreaId = 1000013,
																			Ordinal = 1,
																			BlockTypeId = 18,
																			Title = brandListBlockTitle,
																			TemplateId = brandListBlockTemplateId,
																			Setting = new BlockSetting
																				{
																					ColumnCount = 1,
																					RowCount = 1,
																					TotalItemCount = 10000
																				},
																			IncludeAllBrands = brandListIncludeAllBrands
																		},
											MenuBlockTemplate = new Block
																	{
																		ContentAreaId = 1000013,
																		Ordinal = 2,
																		BlockTypeId = 10,
																		Title = menuBlockTitle,
																		TemplateId = menuBlockTemplateId
																	},
											TopListBlockTemplate = new BlockTopList
																	{
																		ContentAreaId = 1000013,
																		Ordinal = 1,
																		BlockTypeId = 1002,
																		Title = topListBlockTitle,
																		TemplateId = topListBlockTemplateId,
																		IncludeAllCategories = topListIncludeAllCategories,
																		OrderStatisticsDayCount = topListOrderStatisticsDayCount,
																		Setting = new BlockSetting
																			{
																				ColumnCount = topListColumnCount,
																				RowCount = topListRowCount,
																				TotalItemCount = topListTotalProductCount
																			}
																	}
										}
							}
					};
		}

		private static IEnumerable<Configuration> GetHeppoReplicationConfigurations()
		{
			const string systemUserName = "admin@litium.se";

			return
				new[]
					{
						new Configuration
							{
								ChannelId = -1, // Put here real id //TODO
								SystemUserName = systemUserName,
								CategoryConfiguration = null,
								BrandConfiguration = null,
								ReplicateConfiguration = 
								new ReplicateConfiguration
									{
										ChannleIdSource = 1 // se
									}
							}
					};
		}

		private static IEnumerable<Configuration> GetLekmerReplicationConfigurations()
		{
			const string systemUserName = "admin@litium.se";

			return
				new[]
					{
						new Configuration
							{
								ChannelId = 1000005, // Lekmer NL
								SystemUserName = systemUserName,
								CategoryConfiguration = null,
								BrandConfiguration = null,
								ReplicateConfiguration = 
								new ReplicateConfiguration
									{
										ChannleIdSource = 1 // Lekmer SE
									}
							}
					};
		}

		private static IEnumerable<Configuration> GetLekmerNewCategoryStructureConfigurations()
		{
			const string systemUserName = "admin@litium.se";

			var configurations = new[] {
				//Lekmer.se
				new Configuration {
					ChannelId = 1,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration {
						ParentContentNodeId = 1006996, //"Categories SE"
						MasterPageId = 2252, //Content Master Page - SE
					}
				},

				//Lekmer.no
				new Configuration {
					ChannelId = 2,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration {
						ParentContentNodeId = 1003531, //"Categories NO"
						MasterPageId = 1003536, //Content Master Page - NO
					}
				},

				//Lekmer.dk
				new Configuration {
					ChannelId = 3,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration {
						ParentContentNodeId = 1005873, //"Categories DK"
						MasterPageId = 1005841, //Content Master Page - DK
					}
				},

				//Lekmer.fi
				new Configuration {
					ChannelId = 4,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration {
						ParentContentNodeId = 1004968, //"Categories FI"
						MasterPageId = 1004976, //Content Master Page - FI
					}
				}
			};

			var categoryParentCategoryIdList = new List<int?>
				{
					1000445, // Barn & Baby C_10
					1001310, // Barnkläder C_50
					1000494, // Inredning C_20
					1000533  // Leksaker C_30
				}; // Level 1

			foreach (Configuration configuration in configurations)
			{
				configuration.CategoryConfiguration.ParentCategoryIdList = categoryParentCategoryIdList;
				configuration.CategoryConfiguration.NewCategoryIdFrom = 1002281;

				configuration.CategoryConfiguration.ContentNodeForParentCategories = false;

				configuration.CategoryConfiguration.ContentNodeForCategoryLevel1 = false;
				configuration.CategoryConfiguration.ContentNodeForCategoryLevel2 = true;
				configuration.CategoryConfiguration.ContentNodeForCategoryLevel3 = true;

				configuration.CategoryConfiguration.ContentNodeTemplateId = 1000002; // Content page template - "Lekmer - Startpage";

				configuration.CategoryConfiguration.FilterBlockAreaId = 1000001; // "1. Main"
				configuration.CategoryConfiguration.FilterBlockOrdinal = 20;
				configuration.CategoryConfiguration.FilterBlockTypeId = 1000001; // "Product filter"

				configuration.CategoryConfiguration.FilterBlockTemplateId_Leksaker = 1000181; // "2011 - Filter - Default"
				configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_Leksaker = 1000180; // "2011 - Filter - JS"

				configuration.CategoryConfiguration.FilterBlockTemplateId_BarnBaby = 1000181; // "2011 - Filter - Default"
				configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_BarnBaby = 1000180; // "2011 - Filter - JS"

				configuration.CategoryConfiguration.FilterBlockTemplateId_Barnrum = 1000320; // "2011 - Filter - Interior"
				configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_Barnrum = 1000180; // "2011 - Filter - JS"

				configuration.CategoryConfiguration.FilterBlockTemplateId_Barnkläder = 1000182; // "2011 - Filter - Barnklader"
				configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_Barnkläder = 1000180; // "2011 - Filter - JS"

				configuration.CategoryConfiguration.FilterBlockTitle = "Category filter";


				configuration.CategoryConfiguration.RichtextBlockAreaId = 1000001; // "1. Main"
				configuration.CategoryConfiguration.RichtextBlockOrdinal = 30;
				configuration.CategoryConfiguration.RichtextBlockTypeId = 1; // "Rich text"

				configuration.CategoryConfiguration.RichtextBlockTemplateId = 24; // "Box with block title"
			}

			return configurations;
		}
	}
}