﻿using System;
using System.IO;
using System.Linq;

namespace Litium.Lekmer.DropShip
{
	public class CsvDropShipFile
	{
		private bool _isInitialized;
		private string _delimiter;
		private IOrderedEnumerable<IDropShipInfo> _dropShipInfoCollection;

		public void Initialize(IDropShipSetting setting, IOrderedEnumerable<IDropShipInfo> dropShipInfoCollection)
		{
			_delimiter = setting.Delimiter;
			_dropShipInfoCollection = dropShipInfoCollection;

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap);

			foreach (var dropShipInfo in _dropShipInfoCollection)
			{
				writer.WriteLine(AddRow(dropShipInfo));
			}

			writer.Flush();
			writer.Close();
		}

		protected string AddRow(IDropShipInfo dropShipInfo)
		{
			string row = string.Empty;

			AddValue(ref row, dropShipInfo.ExternalOrderId);
			AddValue(ref row, dropShipInfo.OfferId);
			AddValue(ref row, dropShipInfo.Email);
			AddValue(ref row, dropShipInfo.Phone);
			AddValue(ref row, dropShipInfo.SocialSecurityNumber);
			AddValue(ref row, dropShipInfo.BillingFirstName);
			AddValue(ref row, dropShipInfo.BillingLastName);
			AddValue(ref row, dropShipInfo.BillingCompany);
			AddValue(ref row, dropShipInfo.BillingCo);
			AddValue(ref row, dropShipInfo.BillingStreet);
			AddValue(ref row, dropShipInfo.BillingStreetNr);
			AddValue(ref row, dropShipInfo.BillingEntrance);
			AddValue(ref row, dropShipInfo.BillingFloor);
			AddValue(ref row, dropShipInfo.BillingApartmentNumber);
			AddValue(ref row, dropShipInfo.BillingZip);
			AddValue(ref row, dropShipInfo.BillingCity);
			AddValue(ref row, dropShipInfo.BillingCountry);
			AddValue(ref row, dropShipInfo.DeliveryFirstName);
			AddValue(ref row, dropShipInfo.DeliveryLastName);
			AddValue(ref row, dropShipInfo.DeliveryCompany);
			AddValue(ref row, dropShipInfo.DeliveryCo);
			AddValue(ref row, dropShipInfo.DeliveryStreet);
			AddValue(ref row, dropShipInfo.DeliveryStreetNr);
			AddValue(ref row, dropShipInfo.DeliveryFloor);
			AddValue(ref row, dropShipInfo.DeliveryEntrance);
			AddValue(ref row, dropShipInfo.DeliveryZip);
			AddValue(ref row, dropShipInfo.DeliveryCity);
			AddValue(ref row, dropShipInfo.DeliveryCountry);
			AddValue(ref row, dropShipInfo.Discount);
			AddValue(ref row, dropShipInfo.Comment);
			AddValue(ref row, dropShipInfo.SupplierNo);
			AddValue(ref row, dropShipInfo.OrderId);
			AddValue(ref row, dropShipInfo.OrderDate);
			AddValue(ref row, dropShipInfo.CustomerNumber);
			AddValue(ref row, dropShipInfo.NotificationNumber);
			AddValue(ref row, dropShipInfo.PaymentMethod);
			AddValue(ref row, dropShipInfo.ProductArticleNumber);
			AddValue(ref row, dropShipInfo.ProductTitle);
			AddValue(ref row, dropShipInfo.Quantity);
			AddValue(ref row, dropShipInfo.Price);
			AddValue(ref row, dropShipInfo.ProductDiscount);
			AddValue(ref row, dropShipInfo.ProductSoldSum);
			AddValue(ref row, dropShipInfo.ProductStatus);
			AddValue(ref row, dropShipInfo.InvoiceDate);
			AddValue(ref row, dropShipInfo.VatProc);

			int lastDelimiterPosition = row.LastIndexOf(_delimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				row = row.Substring(0, lastDelimiterPosition);
			}

			return row;
		}

		protected void AddValue(ref string row, string value)
		{
			row += value + _delimiter;
		}
	}
}