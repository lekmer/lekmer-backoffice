using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DropShip
{
	public class DropShipRepository
	{
		protected virtual DataMapperBase<IDropShipInfo> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IDropShipInfo>(dataReader);
		}

		public virtual void ImportDropShipData(string filePath)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("FilePath", filePath, SqlDbType.NVarChar)
			};
			var dbSettings = new DatabaseSetting("DropShipRepository.ImportDropShipData");
			new DataHandler().ExecuteCommand("[integration].[pImportDropShip]", parameters, dbSettings);
		}

		public virtual Collection<IDropShipInfo> GetAllNotSent()
		{
			var dbSettings = new DatabaseSetting("DropShipRepository.GetAllNotSent");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDropShipGetAllNotSent]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IDropShipInfo> GetAllGroupByOrder()
		{
			var dbSettings = new DatabaseSetting("DropShipRepository.GetAllGroupByOrder");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDropShipGetAllGroupByOrder]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IDropShipInfo> GetAllByOrder(int orderId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("Orderid", orderId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("DropShipRepository.GetAllByOrder");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDropShipGetAllByOrder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IDropShipInfo> GetAllByOrderAndSupplier(int orderId, string supplierNo)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("Orderid", orderId, SqlDbType.Int),
				ParameterHelper.CreateParameter("SupplierNo", supplierNo, SqlDbType.NVarChar)
			};
			var dbSettings = new DatabaseSetting("DropShipRepository.GetAllByOrderAndSupplier");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDropShipGetAllByOrderAndSupplier]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Update(int id, string csvFileName, string pdfFileName)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("DropShipId", id, SqlDbType.Int),
				ParameterHelper.CreateParameter("CsvFileName", csvFileName, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("PdfFileName", pdfFileName, SqlDbType.NVarChar)
			};
			var dbSettings = new DatabaseSetting("DropShipRepository.Update");
			new DataHandler().ExecuteCommand("[orderlek].[pDropShipUpdate]", parameters, dbSettings);
		}
	}
}