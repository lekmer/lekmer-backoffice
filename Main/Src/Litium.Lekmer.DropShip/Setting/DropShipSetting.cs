﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.DropShip
{
	public class DropShipSetting : SettingBase, IDropShipSetting
	{
		protected override string StorageName
		{
			get { return "DropShipService"; }
		}
		protected virtual string GroupName
		{
			get { return "DropShipSettings"; }
		}

		public string IncomingDirectory
		{
			get { return GetString(GroupName, "IncomingDirectory"); }
		}
		public string ProcessingDirectory
		{
			get { return GetString(GroupName, "ProcessingDirectory"); }
		}
		public string DoneDirectory
		{
			get { return GetString(GroupName, "DoneDirectory"); }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}
		public string LogoPath
		{
			get { return GetString(GroupName, "LogoPath"); }
		}
		public string CsvFileName
		{
			get { return GetString(GroupName, "CsvFileName"); }
		}
		public string PdfFileName
		{
			get { return GetString(GroupName, "PdfFileName"); }
		}
		public string Delimiter
		{
			get { return GetString(GroupName, "Delimiter"); }
		}

		public string FromName
		{
			get { return GetString(GroupName, "FromName"); }
		}
		public string FromEmail
		{
			get { return GetString(GroupName, "FromEmail"); }
		}
		public string BccEmail
		{
			get { return GetString(GroupName, "BccEmail"); }
		}
		public string MailSubject
		{
			get { return GetString(GroupName, "MailSubject"); }
		}
		public string MailSubjectSecond
		{
			get { return GetString(GroupName, "MailSubjectSecond"); }
		}
		public string MailBody
		{
			get { return GetString(GroupName, "MailBody"); }
		}
		public string MailBodySecond
		{
			get { return GetString(GroupName, "MailBodySecond"); }
		}

		public bool EnableSsl
		{
			get { return GetBoolean(GroupName, "EnableSSL", false); }
		}
	}
}