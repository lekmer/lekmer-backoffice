using System;

namespace Litium.Lekmer.DropShip
{
	[Serializable]
	public class DropShipInfo : IDropShipInfo
	{
		public int Id { get; set; }
		public string ExternalOrderId { get; set; }
		public string OfferId { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string SocialSecurityNumber { get; set; }
		public string BillingFirstName { get; set; }
		public string BillingLastName { get; set; }
		public string BillingCompany { get; set; }
		public string BillingCo { get; set; }
		public string BillingStreet { get; set; }
		public string BillingStreetNr { get; set; }
		public string BillingEntrance { get; set; }
		public string BillingFloor { get; set; }
		public string BillingApartmentNumber { get; set; }
		public string BillingZip { get; set; }
		public string BillingCity { get; set; }
		public string BillingCountry { get; set; }
		public string DeliveryFirstName { get; set; }
		public string DeliveryLastName { get; set; }
		public string DeliveryCompany { get; set; }
		public string DeliveryCo { get; set; }
		public string DeliveryStreet { get; set; }
		public string DeliveryStreetNr { get; set; }
		public string DeliveryApartmentNumber { get; set; }
		public string DeliveryFloor { get; set; }
		public string DeliveryEntrance { get; set; }
		public string DeliveryZip { get; set; }
		public string DeliveryCity { get; set; }
		public string DeliveryCountry { get; set; }
		public string Discount { get; set; }
		public string Comment { get; set; }
		public string SupplierNo { get; set; }
		public string OrderId { get; set; }
		public string OrderDate { get; set; }
		public string CustomerNumber { get; set; }
		public string NotificationNumber { get; set; }
		public string PaymentMethod { get; set; }
		public string ProductArticleNumber { get; set; }
		public string ProductTitle { get; set; }
		public string Quantity { get; set; }
		public string Price { get; set; }
		public string ProductDiscount { get; set; }
		public string ProductSoldSum { get; set; }
		public string ProductStatus { get; set; }
		public string InvoiceDate { get; set; }
		public string VatProc { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime? SendDate { get; set; }
		public string CsvFileName { get; set; }
		public string PdfFileName { get; set; }

		public void PopulateDeliveryInfo()
		{
			DeliveryApartmentNumber = string.Empty;

			if (DeliveryFirstName == null && DeliveryLastName == null && DeliveryCompany == null && DeliveryCo == null
				&& DeliveryStreet == null && DeliveryStreetNr == null && DeliveryEntrance == null && DeliveryFloor == null
				&& DeliveryZip == null && DeliveryCity == null && DeliveryCountry == null)
			{
				DeliveryFirstName = BillingFirstName;
				DeliveryLastName = BillingLastName;
				DeliveryCompany = BillingCompany;
				DeliveryCo = BillingCo;
				DeliveryStreet = BillingStreet;
				DeliveryStreetNr = BillingStreetNr;
				DeliveryEntrance = BillingEntrance;
				DeliveryFloor = BillingFloor;
				DeliveryApartmentNumber = BillingApartmentNumber;
				DeliveryZip = BillingZip;
				DeliveryCity = BillingCity;
				DeliveryCountry = BillingCountry;
			}
		}
	}
}