﻿using System;
using System.Diagnostics;
using Litium.Framework.Search.Configuration;
using Litium.Framework.Search.Indexing;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.IndexBuilder
{
	internal class Program
	{
		private static void Main()
		{
			Console.WriteLine("IndexBuilder Console");
			Console.WriteLine("----------------------------------------------------");
			Console.WriteLine("IndexBuilder will immediately process all index jobs ");
			Console.WriteLine("defined in the search configuration file, ");
			Console.WriteLine("regardless of their scheduling information.");
			Console.WriteLine("----------------------------------------------------");
			Console.WriteLine("Press any key to start the build process.");
			Console.ReadLine();
			Console.WriteLine("Building, please wait...");

			try
			{
				var indexingService = IoC.Resolve<IIndexingService>();
				var configurationService = IoC.Resolve<IConfigurationService>();
				var jobLocator = IoC.Resolve<IIndexJobLocator>();
				//var indexWorker = new LuceneIndexWorker(indexingService, configurationService, jobLocator, (int) TimeSpan.FromSeconds(10).TotalMilliseconds);
				var indexWorker = new UntimelyIndexWorker(
					indexingService, configurationService, jobLocator, 0);

				var stopWatch = Stopwatch.StartNew();
				indexWorker.Start();
				stopWatch.Stop();

				Console.WriteLine("Done! Index build completed in {0} ms. Press any key to quit.",
				                  stopWatch.ElapsedMilliseconds);
			}
			catch (Exception ex)
			{
				Console.WriteLine("An error occured:");
				Console.WriteLine(ex);
			}

			Console.ReadLine();
		}
	}
}