﻿using System;
using System.Diagnostics;
using System.Reflection;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Twosell.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.InfoFormat("Twosell.Console started.");

				var exporter = IoC.Resolve<IExporter>("OrderInfoExporter");
				exporter.Execute();
				_log.InfoFormat("Twosell.Console completed.");

				stopwatch.Stop();
				_log.InfoFormat("Twosell.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch (Exception ex)
			{
				_log.Error("Twosell.Console failed.", ex);
			}
		}
	}
}