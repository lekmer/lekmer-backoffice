﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign.Extensions
{
	public static class CartItemExtensions
	{
		public static Collection<ICartItem> SplitIntoSingleItems(this Collection<ICartItem> singleCartItems, Collection<ICartItem> cartItems)
		{
			foreach (CartItem cartItem in cartItems)
			{
				for (int i = 0; i < cartItem.Quantity; ++i)
				{
					var cartItemClone = ObjectCopier.Clone(cartItem);
					cartItemClone.Quantity = 1;
					singleCartItems.Add(cartItemClone);
				}
			}

			return singleCartItems;
		}

		public static Collection<ILekmerCartItem> SplitIntoSingleLekmerItems(this Collection<ILekmerCartItem> singleCartItems, Collection<ICartItem> cartItems)
		{
			foreach (CartItem cartItem in cartItems)
			{
				for (int i = 0; i < cartItem.Quantity; ++i)
				{
					var cartItemClone = ObjectCopier.Clone(cartItem);
					cartItemClone.Quantity = 1;
					singleCartItems.Add((ILekmerCartItem) cartItemClone);
				}
			}

			return singleCartItems;
		}
	}
}