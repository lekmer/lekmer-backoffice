﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Campaign.Setting
{
	public sealed class PreferredFoldersCookieSetting : SettingBase
	{
		private static readonly PreferredFoldersCookieSetting _instance = new PreferredFoldersCookieSetting();

		private PreferredFoldersCookieSetting()
		{
		}

		public static PreferredFoldersCookieSetting Instance
		{
			get { return _instance; }
		}

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "LekmerCampaignPreferredFolders"; }
		}

		public bool StoreInCookies
		{
			get { return GetBoolean(_groupName, "StoreInCookies", true); }
		}

		public string CookiesKey
		{
			get { return GetString(_groupName, "CookiesKey", "LekmerCampaignPreferredFolders"); }
		}

		public int CookiesExpiresInMinutes
		{
			get { return GetInt32(_groupName, "CookiesExpiresInMinutes", 0); }
		}
	}
}
