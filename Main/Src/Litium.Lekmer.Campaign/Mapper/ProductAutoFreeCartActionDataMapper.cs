﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class ProductAutoFreeCartActionDataMapper : CartActionDataMapper<IProductAutoFreeCartAction>
	{
		public ProductAutoFreeCartActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductAutoFreeCartAction Create()
		{
			var action = base.Create();

			action.ProductId = MapValue<int>("ProductAutoFreeCartAction.ProductId");
			action.Quantity = MapValue<int>("ProductAutoFreeCartAction.Quantity");

			return action;
		}
	}
}