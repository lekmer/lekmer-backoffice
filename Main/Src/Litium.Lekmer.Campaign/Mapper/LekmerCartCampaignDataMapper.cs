﻿using System.Data;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class LekmerCartCampaignDataMapper : LekmerCampaignDataMapper<ICartCampaign>
	{
		public LekmerCartCampaignDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
	}
}