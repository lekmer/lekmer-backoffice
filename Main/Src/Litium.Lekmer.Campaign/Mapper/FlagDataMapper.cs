﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class FlagDataMapper : DataMapperBase<IFlag>
	{
		public FlagDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IFlag Create()
		{
			var flag = IoC.Resolve<IFlag>();
			flag.Id = MapValue<int>("Flag.Id");
			flag.Title = MapValue<string>("Flag.Title");
			flag.Class = MapValue<string>("Flag.Class");
			flag.Ordinal = MapValue<int>("Flag.Ordinal");
			return flag;
		}
	}
}