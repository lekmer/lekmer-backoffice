﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartItemPercentageDiscountActionDataMapper : CartActionDataMapper<ICartItemPercentageDiscountAction>
	{
		public CartItemPercentageDiscountActionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICartItemPercentageDiscountAction Create()
		{
			var action = base.Create();
			action.DiscountAmount = MapValue<decimal>("CartItemPercentageDiscountAction.DiscountAmount");
			action.IncludeAllProducts = MapValue<bool>("CartItemPercentageDiscountAction.IncludeAllProducts");
			return action;
		}
	}
}