﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class VoucherConditionDataMapper : ConditionDataMapper<IVoucherCondition>
	{
		public VoucherConditionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IVoucherCondition Create()
		{
			var condition = base.Create();

			condition.IncludeAllBatchIds = MapValue<bool>("VoucherCondition.IncludeAllBatchIds");

			return condition;
		}
	}
}
