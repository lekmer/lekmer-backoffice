﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class VoucherDiscountActionDataMapper : CartActionDataMapper<IVoucherDiscountAction>
	{
		public VoucherDiscountActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
		protected override IVoucherDiscountAction Create()
		{
			var action = base.Create();
			action.FixedDiscount = MapNullableValue<bool>("Fixed");
			action.PercentageDiscount = MapNullableValue<bool>("Percentage");
			action.DiscountValue = MapNullableValue<decimal?>("DiscountValue");
			action.AllowSpecialOffer = MapNullableValue<bool>("AllowSpecialOffer");
			return action;
		}
	}
}