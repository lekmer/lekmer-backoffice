﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class ConditionTargetProductTypeDataMapper : DataMapperBase<IConditionTargetProductType>
	{
		public ConditionTargetProductTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IConditionTargetProductType Create()
		{
			var targetProductType = IoC.Resolve<IConditionTargetProductType>();

			targetProductType.ConditionId = MapValue<int>("ConditionTargetProductType.ConditionId");
			targetProductType.ProductTypeId = MapValue<int>("ConditionTargetProductType.ProductTypeId");

			targetProductType.SetUntouched();

			return targetProductType;
		}
	}
}