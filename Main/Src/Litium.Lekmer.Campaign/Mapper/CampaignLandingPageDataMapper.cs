﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignLandingPageDataMapper : DataMapperBase<ICampaignLandingPage>
	{
		public CampaignLandingPageDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignLandingPage Create()
		{
			var campaignLandingPage = IoC.Resolve<ICampaignLandingPage>();

			campaignLandingPage.CampaignLandingPageId = MapValue<int>("CampaignLandingPage.CampaignLandingPageId");
			campaignLandingPage.CampaignId = MapValue<int>("CampaignLandingPage.CampaignId");
			campaignLandingPage.WebTitle = MapNullableValue<string>("CampaignLandingPage.WebTitle");
			campaignLandingPage.Description = MapNullableValue<string>("CampaignLandingPage.Description");

			return campaignLandingPage;
		}
	}
}