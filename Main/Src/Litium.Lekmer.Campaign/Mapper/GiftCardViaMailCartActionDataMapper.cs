﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class GiftCardViaMailCartActionDataMapper : CartActionDataMapper<IGiftCardViaMailCartAction>
	{
		public GiftCardViaMailCartActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IGiftCardViaMailCartAction Create()
		{
			var giftCardViaMailCartAction = base.Create();

			giftCardViaMailCartAction.SendingInterval = MapValue<int>("GiftCardViaMailCartAction.SendingInterval");
			giftCardViaMailCartAction.TemplateId = MapNullableValue<int?>("GiftCardViaMailCartAction.TemplateId");

			return giftCardViaMailCartAction;
		}
	}
}