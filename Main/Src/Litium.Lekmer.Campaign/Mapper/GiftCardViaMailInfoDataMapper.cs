﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class GiftCardViaMailInfoDataMapper : DataMapperBase<IGiftCardViaMailInfo>
	{
		public GiftCardViaMailInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IGiftCardViaMailInfo Create()
		{
			var giftCardViaMailInfo = IoC.Resolve<IGiftCardViaMailInfo>();

			giftCardViaMailInfo.Id = MapValue<int>("GiftCardViaMailInfo.GiftCardViaMailInfoId");
			giftCardViaMailInfo.OrderId = MapValue<int>("GiftCardViaMailInfo.OrderId");
			giftCardViaMailInfo.CampaignId = MapValue<int>("GiftCardViaMailInfo.CampaignId");
			giftCardViaMailInfo.ActionId = MapValue<int>("GiftCardViaMailInfo.ActionId");
			giftCardViaMailInfo.DiscountValue = MapValue<decimal>("GiftCardViaMailInfo.DiscountValue");
			giftCardViaMailInfo.DateToSend = MapValue<DateTime>("GiftCardViaMailInfo.DateToSend");
			giftCardViaMailInfo.StatusId = MapValue<int>("GiftCardViaMailInfo.StatusId");
			giftCardViaMailInfo.ChannelId = MapValue<int>("GiftCardViaMailInfo.ChannelId");
			giftCardViaMailInfo.VoucherInfoId = MapNullableValue<int?>("GiftCardViaMailInfo.VoucherInfoId");
			giftCardViaMailInfo.VoucherCode = MapNullableValue<string>("GiftCardViaMailInfo.VoucherCode");

			giftCardViaMailInfo.SetUntouched();

			return giftCardViaMailInfo;
		}
	}
}