﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignConfigDataMapper : DataMapperBase<ICampaignConfig>
	{
		public CampaignConfigDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignConfig Create()
		{
			var campaignConfig = IoC.Resolve<ICampaignConfig>();

			campaignConfig.Id = MapValue<int>("CampaignActionConfigurator.CampaignActionConfiguratorId");
			campaignConfig.IncludeAllProducts = MapValue<bool>("CampaignActionConfigurator.IncludeAllProducts");

			campaignConfig.SetUntouched();

			return campaignConfig;
		}
	}
}