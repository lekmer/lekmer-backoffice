﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class FixedPriceActionDataMapper : ProductActionDataMapper<IFixedPriceAction>
	{
        public FixedPriceActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IFixedPriceAction Create()
		{
            var fixedPriceAction = base.Create();

            fixedPriceAction.IncludeAllProducts = MapValue<bool>("FixedPriceAction.IncludeAllProducts");

            return fixedPriceAction;
		}
	}
}