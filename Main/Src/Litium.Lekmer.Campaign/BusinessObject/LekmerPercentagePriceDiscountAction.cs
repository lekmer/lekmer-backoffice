﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerPercentagePriceDiscountAction : LekmerProductAction, ILekmerPercentagePriceDiscountAction
	{
		private int _campaignConfigId;
		private ICampaignConfig _campaignConfig;
		private decimal _discountAmount;

		public int CampaignConfigId
		{
			get { return _campaignConfigId; }
			set
			{
				CheckChanged(_campaignConfigId, value);
				_campaignConfigId = value;
			}
		}
		public ICampaignConfig CampaignConfig
		{
			get { return _campaignConfig; }
			set
			{
				CheckChanged(_campaignConfig, value);
				_campaignConfig = value;
			}
		}
		public decimal DiscountAmount
		{
			get { return _discountAmount; }
			set
			{
				if (value < 0 || value > 100)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				CheckChanged(_discountAmount, value);
				_discountAmount = value;
			}
		}


		#region IPercentagePriceDiscountAction Members
		public bool IncludeAllProducts
		{
			get { throw new InvalidOperationException("IPercentagePriceDiscountAction.IncludeAllProducts is not supported"); }
			set { throw new InvalidOperationException("IPercentagePriceDiscountAction.IncludeAllProducts is not supported"); }
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public ProductIdDictionary IncludeProducts
		{
			get { throw new InvalidOperationException("IPercentagePriceDiscountAction.IncludeProducts is not supported"); }
			set { throw new InvalidOperationException("IPercentagePriceDiscountAction.IncludeProducts is not supported"); }
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public ProductIdDictionary ExcludeProducts
		{
			get { throw new InvalidOperationException("IPercentagePriceDiscountAction.ExcludeProducts is not supported"); }
			set { throw new InvalidOperationException("IPercentagePriceDiscountAction.ExcludeProducts is not supported"); }
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		CategoryIdDictionary IPercentagePriceDiscountAction.IncludeCategories
		{
			get { throw new InvalidOperationException("IPercentagePriceDiscountAction.IncludeCategories is not supported"); }
			set { throw new InvalidOperationException("IPercentagePriceDiscountAction.IncludeCategories is not supported"); }
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		CategoryIdDictionary IPercentagePriceDiscountAction.ExcludeCategories
		{
			get { throw new InvalidOperationException("IPercentagePriceDiscountAction.ExcludeCategories is not supported"); }
			set { throw new InvalidOperationException("IPercentagePriceDiscountAction.ExcludeCategories is not supported"); }
		}
		#endregion


		public override bool Apply(IUserContext context, IProduct product)
		{
			if (!Verify(product))
			{
				return false;
			}

			if (DiscountAmount == 0m)
			{
				// Possible it is FLAG campaign (just a workaround)
				return true;
			}

			decimal priceIncludingVat = (product.Price.PriceIncludingVat*(1 - DiscountAmount/100)).Round();
			if (priceIncludingVat >= product.CampaignInfo.Price.IncludingVat)
			{
				return false;
			}

			decimal priceExcludingVat = (priceIncludingVat/(1 + product.Price.VatPercentage/100)).Round();
			product.CampaignInfo.Price = new Price(priceIncludingVat, priceExcludingVat);
			((ILekmerProductCampaignInfo) product.CampaignInfo).UpdateAppliedActions(this);

			return true;
		}

		public override bool Verify(IProduct product)
		{
			if (CampaignConfig == null)
			{
				throw new InvalidOperationException("The CampaignConfig property must be set.");
			}
			CampaignConfig.Validate();

			return IsTargetType(product) && IsRangeMember((ILekmerProduct) product);
		}

		protected virtual bool IsRangeMember(ILekmerProduct product)
		{
			return CampaignConfig.IsRangeMember(product.Id, product.CategoryId, product.BrandId);
		}

		protected override bool IsTargetType(IProduct product)
		{
			return CampaignConfig.IsTargetType(product.Id) || base.IsTargetType(product);
		}

		public override object[] GetInfoArguments()
		{
			return new object[] { DiscountAmount };
		}
	}
}