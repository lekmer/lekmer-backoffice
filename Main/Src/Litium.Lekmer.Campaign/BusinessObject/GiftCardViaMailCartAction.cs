﻿using System;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class GiftCardViaMailCartAction : LekmerCartAction, IGiftCardViaMailCartAction
	{
		private int _sendingInterval;
		private int? _templateId;
		private LekmerCurrencyValueDictionary _amounts;

		public int SendingInterval
		{
			get { return _sendingInterval; }
			set
			{
				CheckChanged(_sendingInterval, value);
				_sendingInterval = value;
			}
		}

		public int? TemplateId
		{
			get { return _templateId; }
			set
			{
				CheckChanged(_templateId, value);
				_templateId = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, since the Amounts list is null or empty.");
			}

			return new object[]
				{
					Amounts.ElementAt(0).Value,
					Amounts.ElementAt(0).Key
				};
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			decimal discountValue;
			if (!Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			return true;
		}
	}
}