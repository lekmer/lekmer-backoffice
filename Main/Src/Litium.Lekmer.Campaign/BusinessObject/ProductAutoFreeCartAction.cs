using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign.Extensions;
using Litium.Lekmer.Order;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class ProductAutoFreeCartAction : LekmerCartAction, IProductAutoFreeCartAction
	{
		private int _productId;
		private int _quantity;

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int Quantity
		{
			get { return _quantity; }
			set
			{
				CheckChanged(_quantity, value);
				_quantity = value;
			}
		}


		public override object[] GetInfoArguments()
		{
			return new object[]
				{
					ProductId
				};
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			if (ProductId <= 0)
			{
				return false;
			}

			var singleCartItems = new Collection<ICartItem>();
			singleCartItems.SplitIntoSingleItems(cart.GetCartItems());

			IEnumerable<ICartItem> cartItems = singleCartItems.Where(cartItem => cartItem.Product.Id == ProductId);
			foreach (var cartItem in cartItems)
			{
				var lekmerCartItem = (ILekmerCartItem)cartItem;
				if (lekmerCartItem != null && lekmerCartItem.IsAffectedByCampaign && lekmerCartItem.Status != BusinessObjectStatus.Deleted)
				{
					return false;
				}
			}

			var campaign = IoC.Resolve<ICartCampaignService>().GetById(context, CartCampaignId);
			if (campaign.ConditionsFulfilled(context))
			{
				AddFreeProduct(context, cart);

				return true;
			}

			return false;
		}

		protected virtual void AddFreeProduct(IUserContext context, ICartFull cart)
		{
			var cartItemOptionService = IoC.Resolve<ICartItemOptionService>();
			var cartItems = cartItemOptionService.GetAllByCart(context, cart.Id);
			var firstOrDefault = cartItems.FirstOrDefault(ci => ci.CartId == cart.Id && ci.Product.Id == ProductId);

			int quantityToAdd = firstOrDefault == null || firstOrDefault.Quantity < 0 ? Quantity : Quantity - firstOrDefault.Quantity;
			if (quantityToAdd <= 0)
			{
				return;
			}

			var productService = IoC.Resolve<IProductService>();
			var freeProduct = productService.GetById(context, ProductId);
			if (freeProduct != null)
			{
				freeProduct.CampaignInfo.Price = new Price(0, 0);

				var cartItemService = IoC.Resolve<ICartItemService>();
				var freeCartItem = cartItemService.Create();
				freeCartItem.CartId = cart.Id;

				freeCartItem.Quantity = quantityToAdd;
				((ILekmerCartItem) freeCartItem).IsAffectedByCampaign = true;
				freeCartItem.Product = freeProduct;

				freeCartItem.Id = GetCartItemId(cart);

				cart.AddItem(freeCartItem);
			}
		}

		protected virtual int GetCartItemId(ICartFull cart)
		{
			int cartItemId = 0;
			foreach (var cartItem in cart.GetCartItems())
			{
				var lekmerCartItem = (ILekmerCartItem)cartItem;
				if (lekmerCartItem != null && lekmerCartItem.IsAffectedByCampaign)
				{
					cartItemId = cartItemId - 1;
				}
			}

			return cartItemId;
		}
	}
}