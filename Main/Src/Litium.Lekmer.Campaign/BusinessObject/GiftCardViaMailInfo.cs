﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class GiftCardViaMailInfo : BusinessObjectBase, IGiftCardViaMailInfo
	{
		private int _id;
		private int _orderId;
		private int _campaignId;
		private int _actionId;
		private decimal _discountValue;
		private DateTime _dateToSend;
		private int _statusId;
		private int _channelId;
		private int? _voucherInfoId;
		private string _voucherCode;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int OrderId
		{
			get { return _orderId; }
			set
			{
				CheckChanged(_orderId, value);
				_orderId = value;
			}
		}

		public int CampaignId
		{
			get { return _campaignId; }
			set
			{
				CheckChanged(_campaignId, value);
				_campaignId = value;
			}
		}

		public int ActionId
		{
			get { return _actionId; }
			set
			{
				CheckChanged(_actionId, value);
				_actionId = value;
			}
		}

		public decimal DiscountValue
		{
			get { return _discountValue; }
			set
			{
				CheckChanged(_discountValue, value);
				_discountValue = value;
			}
		}

		public DateTime DateToSend
		{
			get { return _dateToSend; }
			set
			{
				CheckChanged(_dateToSend, value);
				_dateToSend = value;
			}
		}

		public int StatusId
		{
			get { return _statusId; }
			set
			{
				CheckChanged(_statusId, value);
				_statusId = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public int? VoucherInfoId
		{
			get { return _voucherInfoId; }
			set
			{
				CheckChanged(_voucherInfoId, value);
				_voucherInfoId = value;
			}
		}

		public string VoucherCode
		{
			get { return _voucherCode; }
			set
			{
				CheckChanged(_voucherCode, value);
				_voucherCode = value;
			}
		}
	}
}