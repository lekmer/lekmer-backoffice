using System;
using System.Collections.Generic;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerCartCampaignInfo : CartCampaignInfo, ILekmerCartCampaignInfo
	{
		private Dictionary<int, decimal> _campaignFreightCostDictionary;
		public Dictionary<int, decimal> CampaignFreightCostDictionary
		{
			get { return _campaignFreightCostDictionary ?? (_campaignFreightCostDictionary = new Dictionary<int, decimal>()); }
		}

		/// <summary>
		/// Configured total fixed discount on group of cart items given by voucher.
		/// </summary>
		public decimal VoucherTotalFixedDiscountOnGroupOfCartItems { get; set; }

		/// <summary>
		/// Actually applied fixed discount on group of cart items given by voucher.
		/// Could be equal or less than configured total discount if bought cart items total cost is less than it
		/// </summary>
		public decimal VoucherActualFixedDiscountOnGroupOfCartItems { get; set; }

		/// <summary>
		/// Applied discount on cart items described in 'Cart item percentage discount' action given by voucher.
		/// </summary>
		public decimal VoucherDiscountOnCartItemPercentageDiscountActionItems { get; set; }

		/// <summary>
		/// Applied discount on cart items described in 'Cart item fixed discount' action given by voucher.
		/// </summary>
		public decimal VoucherDiscountOnCartItemFixedDiscountActionItems { get; set; }
	}
}