﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class ProductActionTargetProductType : BusinessObjectBase, IProductActionTargetProductType
	{
		private int _productActionId;
		private int _productTypeId;

		public int ProductActionId
		{
			get { return _productActionId; }
			set
			{
				CheckChanged(_productActionId, value);
				_productActionId = value;
			}
		}

		public int ProductTypeId
		{
			get { return _productTypeId; }
			set
			{
				CheckChanged(_productTypeId, value);
				_productTypeId = value;
			}
		}
	}
}