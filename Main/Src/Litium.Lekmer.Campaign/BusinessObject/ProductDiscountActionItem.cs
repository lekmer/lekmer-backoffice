﻿namespace Litium.Lekmer.Campaign
{
	public class ProductDiscountActionItem
	{
		public int ProductId { get; set; }
		public int CurrencyId { get; set; }
		public decimal MonetaryValue { get; set; }
	}
}