﻿using System;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerCartAction : CartAction, ILekmerCartAction
	{
		private IdDictionary _targetProductTypes;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public IdDictionary TargetProductTypes
		{
			get { return _targetProductTypes; }
			set
			{
				CheckChanged(_targetProductTypes, value);
				_targetProductTypes = value;
			}
		}

		public virtual Price ApplyDiscount(Price priceSummary, ICurrency currency)
		{
			throw new NotImplementedException();
		}


		protected virtual bool IsTargetType(IProduct product)
		{
			if (TargetProductTypes.Count == 0)
			{
				// When action does not contain any product type filter, assume all types are valid.
				return true;
			}

			return TargetProductTypes.ContainsKey(((ILekmerProduct)product).ProductTypeId);
		}
	}
}
