﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Customer;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CustomerGroupCondition : Condition, ICustomerGroupCondition
	{
		private CustomerGroupIdDictionary _includeCustomerGroups;
		private CustomerGroupIdDictionary _excludeCustomerGroups;


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CustomerGroupIdDictionary IncludeCustomerGroups
		{
			get { return _includeCustomerGroups; }
			set
			{
				CheckChanged(_includeCustomerGroups, value);
				_includeCustomerGroups = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CustomerGroupIdDictionary ExcludeCustomerGroups
		{
			get { return _excludeCustomerGroups; }
			set
			{
				CheckChanged(_excludeCustomerGroups, value);
				_excludeCustomerGroups = value;
			}
		}


		public override bool Fulfilled(IUserContext context)
		{
			if (IncludeCustomerGroups == null)
			{
				throw new InvalidOperationException("The IncludeCustomerGroups property must be set.");
			}

			if (ExcludeCustomerGroups == null)
			{
				throw new InvalidOperationException("The ExcludeCustomerGroups property must be set.");
			}

			var customer = context.Customer;
			if (customer == null)
			{
				return false;
			}

			var lekmerCustomerGroupService = IoC.Resolve<ILekmerCustomerGroupService>();
			Collection<ICustomerGroup> customerGroups = lekmerCustomerGroupService.GetAllByCustomer(customer.Id);

			if (customerGroups.Any(customerGroup => ExcludeCustomerGroups.ContainsKey(customerGroup.Id)))
			{
				return false;
			}

			if (customerGroups.Any(customerGroup => IncludeCustomerGroups.ContainsKey(customerGroup.Id)))
			{
				return true;
			}

			return false;
		}

		public override object[] GetInfoArguments()
		{
			return new object[0];
		}
	}
}