﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public class GiftCardViaEmailMessenger : MessengerBase<GiftCardViaEmailMessageArgs>
	{
		private string _customerName;

		protected GiftCardViaEmailTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "GiftCardViaEmail"; }
		}

		protected override Message CreateMessage(GiftCardViaEmailMessageArgs messageArgs)
		{
			if (messageArgs == null
				|| messageArgs.Order.Customer == null 
				|| messageArgs.Order.Customer.CustomerInformation == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			string firstName = messageArgs.Order.Customer.CustomerInformation.FirstName ?? string.Empty;
			string lastName = messageArgs.Order.Customer.CustomerInformation.LastName ?? string.Empty;
			_customerName = firstName + " " + lastName;

			Template = messageArgs.TemplateId.HasValue 
				? new GiftCardViaEmailTemplate(messageArgs.TemplateId.Value, messageArgs.Channel)
				: new GiftCardViaEmailTemplate(messageArgs.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name = _customerName,
				Address = messageArgs.Order.Email
			});

			return message;
		}

		private string RenderMessage(GiftCardViaEmailMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("Message");

			var formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>();

			htmlFragment.AddVariable("Customer.Name", _customerName);
			htmlFragment.AddVariable("Order.Number", messageArgs.Order.Number);
			htmlFragment.AddVariable("Order.CreatedDate", formatter.FormatDateTime(messageArgs.Channel, messageArgs.Order.CreatedDate));
			htmlFragment.AddVariable("DiscountValue", formatter.FormatPriceChannelOrLessDecimals(messageArgs.Channel, messageArgs.DiscountValue));
			htmlFragment.AddVariable("VoucherCode", messageArgs.VoucherCode.ToString(CultureInfo.InvariantCulture));
			htmlFragment.AddVariable("CurrencyIso", messageArgs.Channel.Currency.Iso);

			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}