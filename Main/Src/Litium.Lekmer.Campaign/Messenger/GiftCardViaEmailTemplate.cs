﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview
{
	public class GiftCardViaEmailTemplate : Template
	{
		public GiftCardViaEmailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public GiftCardViaEmailTemplate(IChannel channel)
			: base("GiftCardViaEmail", channel, false)
		{
		}
	}
}