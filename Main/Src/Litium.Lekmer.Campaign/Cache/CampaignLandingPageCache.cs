﻿using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Campaign.Cache
{
	public sealed class CampaignLandingPageCache : ScensumCacheBase<CampaignLandingPageCacheKey, ICampaignLandingPage>
	{
		private static readonly CampaignLandingPageCache _instance = new CampaignLandingPageCache();

		private CampaignLandingPageCache()
		{
		}

		public static CampaignLandingPageCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int id)
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			foreach (var channel in channels)
			{
				Remove(new CampaignLandingPageCacheKey(channel.Id, id));
			}
		}
	}

	public class CampaignLandingPageCacheKey : ICacheKey
	{
		public CampaignLandingPageCacheKey(int channelId, int campaignId)
		{
			ChannelId = channelId;
			CampaignId = campaignId;
		}

		public int ChannelId { get; set; }
		public int CampaignId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + CampaignId; }
		}
	}
}