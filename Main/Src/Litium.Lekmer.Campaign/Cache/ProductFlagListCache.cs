﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Campaign.Cache
{
	public sealed class ProductFlagListCache : ScensumCacheBase<ProductFlagListKey, Collection<IFlag>>
	{
		private static readonly ProductFlagListCache _flagListCache = new ProductFlagListCache();

		private ProductFlagListCache()
		{
		}

		public static ProductFlagListCache Instance
		{
			get { return _flagListCache; }
		}

		public void Remove(int id)
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			foreach (var channel in channels)
			{
				Remove(new ProductFlagListKey(channel.Language.Id, id));
			}
		}
	}

	public class ProductFlagListKey : ICacheKey
	{
		public int LanguageId { get; set; }
		public int ProductId { get; set; }

		public ProductFlagListKey(int languageId, int productId)
		{
			LanguageId = languageId;
			ProductId = productId;
		}

		public string Key
		{
			get { return string.Format(CultureInfo.InvariantCulture, "langId_{0}-prId_{1}", LanguageId, ProductId); }
		}
	}
}