// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project. 
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc. 
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File". 
// You do not need to add suppressions to this file manually. 

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Litium.Lekmer.Campaign.IProductDiscountAction.#ProductDiscountPrices")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Litium.Lekmer.Campaign.ProductDiscountAction.#ProductDiscountPrices")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#Delete(System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#Save(Litium.Lekmer.Campaign.IProductDiscountAction)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#InsertItem(System.Int32,System.Int32,System.Decimal)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#GetItemsSecure(System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#GetItems(System.Int32,System.Nullable`1<System.Int32>,System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#DeleteItems(System.Int32)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Lekmer.Campaign.Repository.ProductDiscountActionRepository.#CreateDataMapper(System.Data.IDataReader)")]
