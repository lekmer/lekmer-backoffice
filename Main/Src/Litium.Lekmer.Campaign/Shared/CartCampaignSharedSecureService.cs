﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public class CartCampaignSharedSecureService : ICartCampaignSharedService
	{
		protected ICartCampaignSecureService CartCampaignSecureService { get; private set; }

		public CartCampaignSharedSecureService(ICartCampaignSecureService cartCampaignSecureService)
		{
			CartCampaignSecureService = cartCampaignSecureService;
		}
		
		public virtual ICartCampaign GetById(int campaignId)
		{
			return CartCampaignSecureService.GetById(campaignId);
		}
	}
}