﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerProductCampaignService : ProductCampaignService, ILekmerProductCampaignService
	{
		protected LekmerCampaignRepository LekmerCampaignRepository { get; private set; }

		public LekmerProductCampaignService(
			ProductCampaignRepository productCampaignRepository,
			IConditionService conditionService,
			IProductActionService productActionService,
			CampaignRepository repository) : base(productCampaignRepository, conditionService, productActionService)
		{
			LekmerCampaignRepository = (LekmerCampaignRepository) repository;
		}

		public ICampaignToTag CampaignToTagGet()
		{
			return LekmerCampaignRepository.CampaignToTagGet();
		}
		public void CampaignToTagDelete(int id)
		{
			LekmerCampaignRepository.CampaignToTagDelete(id);
		}

		public override Collection<IProductCampaign> GetDistinctCampaigns(IUserContext context, Collection<IProductAction> productActions)
		{
			throw new InvalidOperationException("IProductCampaignService.GetDistinctCampaigns is not supported");
		}
		public virtual Collection<IProductCampaign> GetDistinctCampaigns(IUserContext context, Collection<IProductActionAppliedItem> productActionItems)
		{
			var distinctCampaigns = new Collection<IProductCampaign>();
			var distinctCampaignIds = new Collection<int>();
			foreach (var productActionItem in productActionItems)
			{
				int productCampaignId = productActionItem.ProductCampaignId;
				if (distinctCampaignIds.Contains(productCampaignId))
				{
					continue;
				}

				IProductCampaign campaign = GetById(context, productCampaignId);
				if (campaign != null)
				{
					distinctCampaigns.Add(campaign);
				}
				distinctCampaignIds.Add(productCampaignId);
			}
			return distinctCampaigns;
		}
	}
}