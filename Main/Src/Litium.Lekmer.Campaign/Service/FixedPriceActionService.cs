﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class FixedPriceActionService : IProductActionPluginService, IFixedPriceActionService
	{
		protected FixedPriceActionRepository Repository { get; private set; }

		public FixedPriceActionService(FixedPriceActionRepository fixedPriceActionRepository)
		{
			Repository = fixedPriceActionRepository;
		}


		public IProductAction GetById(IUserContext context, int productActionId)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(productActionId);
			if (action == null)
			{
				return null;
			}

			action.Amounts = GetCurrencyValues(productActionId);

			action.IncludeProducts = GetIncludeProducts(productActionId);
			action.ExcludeProducts = GetExcludeProducts(productActionId);
			action.IncludeCategories = GetIncludeCategories(productActionId);
			action.ExcludeCategories = GetExcludeCategories(productActionId);
			action.IncludeBrands = GetIncludeBrands(productActionId);
			action.ExcludeBrands = GetExcludeBrands(productActionId);

			return action;
		}

		public virtual Collection<int> GetProductIds(int productActionId)
		{
			Repository.EnsureNotNull();
			return Repository.GetProductIds(productActionId);
		}


		protected virtual LekmerCurrencyValueDictionary GetCurrencyValues(int productActionId)
		{
			return Repository.GetCurrencyValuesByAction(productActionId);
		}


		protected virtual ProductIdDictionary GetIncludeProducts(int productActionId)
		{
			return Repository.GetIncludeProducts(productActionId);
		}

		protected virtual ProductIdDictionary GetExcludeProducts(int productActionId)
		{
			return Repository.GetExcludeProducts(productActionId);
		}


		protected virtual CategoryIdDictionary GetIncludeCategories(int productActionId)
		{
			return Repository.GetIncludeCategories(productActionId);
		}

		protected virtual CategoryIdDictionary GetExcludeCategories(int productActionId)
		{
			return Repository.GetExcludeCategories(productActionId);
		}


		protected virtual BrandIdDictionary GetIncludeBrands(int productActionId)
		{
			return Repository.GetIncludeBrands(productActionId);
		}

		protected virtual BrandIdDictionary GetExcludeBrands(int productActionId)
		{
			return Repository.GetExcludeBrands(productActionId);
		}
	}
}