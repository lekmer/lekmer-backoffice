﻿using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class CampaignLandingPageService : ICampaignLandingPageService
	{
		protected CampaignLandingPageRepository Repository { get; private set; }

		public CampaignLandingPageService(CampaignLandingPageRepository repository)
		{
			Repository = repository;
		}

		public ICampaignLandingPage GetByCampaignId(IUserContext userContext, int campaignId)
		{
			Repository.EnsureNotNull();

			return CampaignLandingPageCache.Instance.TryGetItem(
				new CampaignLandingPageCacheKey(userContext.Channel.Id, campaignId),
				() => Repository.GetByCampaignId(userContext.Channel.Id, campaignId));
		}
	}
}