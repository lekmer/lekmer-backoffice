﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerPercentagePriceDiscountActionService : PercentagePriceDiscountActionService, ILekmerPercentagePriceDiscountActionService
	{
		protected ICampaignConfigService CampaignConfigService { get; private set; }

		public LekmerPercentagePriceDiscountActionService(PercentagePriceDiscountActionRepository repository, ICampaignConfigService campaignConfigService)
			: base(repository)
		{
			CampaignConfigService = campaignConfigService;
		}

		public override IProductAction GetById(IUserContext context, int productActionId)
		{
			Repository.EnsureNotNull();

			var action = ((LekmerPercentagePriceDiscountActionRepository) Repository).GetById(productActionId);
			if (action == null)
			{
				return null;
			}

			var lekmerAction = action as ILekmerPercentagePriceDiscountAction;
			if (lekmerAction != null)
			{
				lekmerAction.CampaignConfig = CampaignConfigService.GetById(lekmerAction.CampaignConfigId);
			}
			return lekmerAction;
		}

		public Collection<int> GetProductIds(int productActionId)
		{
			Repository.EnsureNotNull();
			return ((LekmerPercentagePriceDiscountActionRepository) Repository).GetProductIds(productActionId);
		}
	}
}