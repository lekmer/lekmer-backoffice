﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class LekmerFreightValueActionService : FreightValueActionService
	{
		protected LekmerFreightValueActionRepository LekmerRepository { get; private set; }

		public LekmerFreightValueActionService(LekmerFreightValueActionRepository freightValueActionRepository)
			: base(freightValueActionRepository)
		{
			LekmerRepository = freightValueActionRepository;
		}

		/// <summary>
		/// Gets action by Id
		/// </summary>
		/// <param name="cartActionId">Id</param>
		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int cartActionId).")]
		public override ICartAction GetById(int cartActionId)
		{
			if (LekmerRepository == null)
			{
				throw new InvalidOperationException("LekmerRepository cannot be null.");
			}

			var action = Repository.GetById(cartActionId);

			if (action != null)
			{
				var lekmerAction = (ILekmerFreightValueAction) action;
				lekmerAction.Amounts = LekmerRepository.GetCartValuesByAction(cartActionId);
				lekmerAction.DeliveryMethodIds = LekmerRepository.GetDeliveryMethods(cartActionId);
				lekmerAction.Status = BusinessObjectStatus.Untouched;
			}

			return action;
		}

		/// <summary>
		/// Gets action by Id
		/// </summary>
		/// <param name="userContext">User context</param>
		/// <param name="cartActionId">Id</param>
		public override ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			if (LekmerRepository == null)
			{
				throw new InvalidOperationException("LekmerRepository cannot be null.");
			}

			var action = Repository.GetById(cartActionId);

			if (action != null)
			{
				var lekmerAction = (ILekmerFreightValueAction)action;
				lekmerAction.Amounts = LekmerRepository.GetCartValuesByAction(cartActionId);
				lekmerAction.DeliveryMethodIds = LekmerRepository.GetDeliveryMethods(cartActionId);
				lekmerAction.Status = BusinessObjectStatus.Untouched;
			}

			return action;
		}
	}
}