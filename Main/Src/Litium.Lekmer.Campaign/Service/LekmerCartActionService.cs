﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCartActionService : CartActionService
	{
		protected ICartActionTargetProductTypeService CartActionTargetProductTypeService { get; private set; }

		public LekmerCartActionService(
			CartActionRepository repository,
			CartActionServiceLocator serviceLocator,
			ICartActionTargetProductTypeService cartActionTargetProductTypeService)
			: base(repository, serviceLocator)
		{
			CartActionTargetProductTypeService = cartActionTargetProductTypeService;
		}

		public override Collection<ICartAction> GetAllByCampaign(IUserContext userContext, int campaignId)
		{
			Collection<ICartAction> cartActions = base.GetAllByCampaign(userContext, campaignId);

			var actionTargetProductTypes = CartActionTargetProductTypeService.GetAllByCampaign(campaignId);

			foreach (ILekmerCartAction cartAction in cartActions)
			{
				int cartActionId = cartAction.Id;
				IEnumerable<int> productTypes = actionTargetProductTypes.Where(t => t.CartActionId == cartActionId).Select(t => t.ProductTypeId);

				cartAction.TargetProductTypes = new IdDictionary(productTypes);
			}

			return cartActions;
		}
	}
}