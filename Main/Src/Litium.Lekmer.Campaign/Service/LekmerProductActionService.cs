﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class LekmerProductActionService : ProductActionService, ILekmerProductActionService
	{
		protected IProductActionTargetProductTypeService ProductActionTargetProductTypeService { get; private set; }

		public LekmerProductActionService(
			ProductActionRepository repository,
			ProductActionServiceLocator serviceLocator,
			IProductActionTargetProductTypeService productActionTargetProductTypeService)
			: base(repository, serviceLocator)
		{
			ProductActionTargetProductTypeService = productActionTargetProductTypeService;
		}

		public virtual IProductActionAppliedItem CreateProductActionAppliedItem()
		{
			var productActionAppliedItem = IoC.Resolve<IProductActionAppliedItem>();
			productActionAppliedItem.Status = BusinessObjectStatus.New;
			return productActionAppliedItem;
		}

		public override Collection<IProductAction> GetAllByCampaign(IUserContext context, int campaignId)
		{
			Collection<IProductAction> productActions = base.GetAllByCampaign(context, campaignId);

			var actionTargetProductTypes = ProductActionTargetProductTypeService.GetAllByCampaign(campaignId);

			foreach (ILekmerProductAction productAction in productActions)
			{
				int productActionId = productAction.Id;
				IEnumerable<int> productTypes = actionTargetProductTypes.Where(t => t.ProductActionId == productActionId).Select(t => t.ProductTypeId);

				productAction.TargetProductTypes = new IdDictionary(productTypes);
			}

			return productActions;
		}
	}
}