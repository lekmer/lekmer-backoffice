﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	public class CampaignRegistryCampaignService : ICampaignRegistryCampaignService
	{
		protected CampaignRegistryCampaignRepository Repository { get; private set; }

		public CampaignRegistryCampaignService(CampaignRegistryCampaignRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<ICampaignRegistryCampaign> GetRegistriesByCampaignId(int campaignId)
		{
			Repository.EnsureNotNull();

			return CampaignRegistryCampaignCache.Instance.TryGetItem(
				new CampaignRegistryCampaignKey(campaignId),
				delegate { return Repository.GetRegistriesByCampaignId(campaignId); });
		}
	}
}