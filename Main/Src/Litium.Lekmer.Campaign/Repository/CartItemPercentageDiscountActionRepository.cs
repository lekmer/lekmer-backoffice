﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartItemPercentageDiscountActionRepository
	{
		protected DataMapperBase<ICartItemPercentageDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemPercentageDiscountAction>(dataReader);
		}

		public virtual void Save(ICartItemPercentageDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("DiscountAmount", action.DiscountAmount, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("IncludeAllProducts", action.IncludeAllProducts, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionDelete]", parameters, dbSettings);
		}

		public virtual ICartItemPercentageDiscountAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		// Products

		public virtual void InsertIncludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemPercentageDiscountActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemPercentageDiscountActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetIncludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetExcludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Categories

		public virtual void InsertIncludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemPercentageDiscountActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemPercentageDiscountActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CampaignCategoryDictionary GetIncludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetIncludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetIncludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetIncludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetExcludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetExcludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}


		// Brands

		public virtual void InsertIncludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.InsertIncludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemPercentageDiscountActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.InsertExcludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemPercentageDiscountActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.DeleteIncludeBrands");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.DeleteExcludeBrands");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemPercentageDiscountActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetIncludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPercentageDiscountActionRepository.GetExcludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemPercentageDiscountActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}
