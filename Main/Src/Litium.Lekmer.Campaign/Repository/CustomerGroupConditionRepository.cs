﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CustomerGroupConditionRepository
	{
		protected virtual DataMapperBase<ICustomerGroupCondition> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICustomerGroupCondition>(dataReader);
		}


		public virtual void Save(ICustomerGroupCondition condition)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", condition.Id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pCustomerGroupConditionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pCustomerGroupConditionDelete]", parameters, dbSettings);
		}

		public virtual ICustomerGroupCondition GetById(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCustomerGroupConditionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		public virtual void InsertIncludeCustomerGroup(int conditionId, int customerGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerGroupId", customerGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.InsertIncludeCustomerGroup");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCustomerGroupConditionIncludeInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCustomerGroups(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.DeleteIncludeCustomerGroups");

			new DataHandler().ExecuteCommand("[campaignlek].[pCustomerGroupConditionIncludeDeleteAll]", parameters, dbSettings);
		}

		public virtual CustomerGroupIdDictionary GetIncludeCustomerGroups(int conditionId)
		{
			var customerGroupIds = new CustomerGroupIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.GetIncludeCustomerGroups");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCustomerGroupConditionIncludeGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					customerGroupIds.Add(dataReader.GetInt32(0));
				}
			}

			return customerGroupIds;
		}


		public virtual void InsertExcludeCustomerGroup(int conditionId, int customerGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerGroupId", customerGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.InsertExcludeCustomerGroup");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCustomerGroupConditionExcludeInsert]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCustomerGroups(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.DeleteExcludeCustomerGroups");

			new DataHandler().ExecuteCommand("[campaignlek].[pCustomerGroupConditionExcludeDeleteAll]", parameters, dbSettings);
		}

		public virtual CustomerGroupIdDictionary GetExcludeCustomerGroups(int conditionId)
		{
			var customerGroupIds = new CustomerGroupIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("CustomerGroupConditionRepository.DeleteExcludeCustomerGroups");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCustomerGroupConditionExcludeGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					customerGroupIds.Add(dataReader.GetInt32(0));
				}
			}

			return customerGroupIds;
		}
	}
}