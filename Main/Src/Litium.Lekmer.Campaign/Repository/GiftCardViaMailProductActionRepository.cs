﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class GiftCardViaMailProductActionRepository
	{
		protected virtual DataMapperBase<IGiftCardViaMailProductAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IGiftCardViaMailProductAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(IGiftCardViaMailProductAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("SendingInterval", action.SendingInterval, SqlDbType.Int),
					ParameterHelper.CreateParameter("TemplateId", action.TemplateId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ConfigId", action.CampaignConfigId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.Save");
			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailProductActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.Delete");
			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailProductActionDelete]", parameters, dbSettings);
		}

		public virtual IGiftCardViaMailProductAction GetById(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailProductActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<int> GetProductIds(int productActionId)
		{
			var productIds = new Collection<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ActionId ", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.GetProductIds");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailProductActionGetProductIds]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int productActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", currencyValue.Value, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.SaveCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailProductActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.DeleteAllCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int productActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailProductActionRepository.GetCurrencyValuesByAction");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailProductActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}
	}
}