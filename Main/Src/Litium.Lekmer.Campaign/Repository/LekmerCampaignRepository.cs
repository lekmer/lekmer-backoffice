﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class LekmerCampaignRepository : CampaignRepository
	{
		protected virtual DataMapperBase<ICampaignToTag> CreateCampaignToTagDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignToTag>(dataReader);
		}

		protected virtual DataMapperBase<ICampaignHyId> CreateCampaignHyIdDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignHyId>(dataReader);
		}

		public override int Save(ICampaign campaign)
		{
			if (campaign == null)
			{
				throw new ArgumentNullException("campaign");
			}

			var lekmerCampaign = (ILekmerCampaign)campaign;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaign.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", campaign.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CampaignStatusId", campaign.CampaignStatus.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("FolderId", campaign.FolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StartDate", campaign.StartDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("EndDate", campaign.EndDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Exclusive", campaign.Exclusive, SqlDbType.Bit),
					ParameterHelper.CreateParameter("Priority", campaign.Priority, SqlDbType.Int),
					ParameterHelper.CreateParameter("LevelId", lekmerCampaign.LevelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("UseLandingPage", lekmerCampaign.UseLandingPage, SqlDbType.Bit),
					ParameterHelper.CreateParameter("PriceTypeId", lekmerCampaign.PriceTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagId", lekmerCampaign.TagId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerCampaignRepository.Save");

			campaign.Id = new DataHandler().ExecuteCommandWithReturnValue("[campaign].[pCampaignSave]", parameters, dbSettings);
			return campaign.Id;
		}

		// Campaign to tag.
		public virtual void CampaignToTagSave(int campaignId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerCampaignRepository.CampaignToTagSave");
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignToTagSave]", parameters, dbSettings);
		}

		public virtual ICampaignToTag CampaignToTagGet()
		{
			var dbSettings = new DatabaseSetting("LekmerCampaignRepository.CampaignToTagGet");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignToTagGet]", dbSettings))
			{
				return CreateCampaignToTagDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void CampaignToTagDelete(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerCampaignRepository.CampaignToTagDelete");
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignToTagDeleteById]", parameters, dbSettings);
		}


		/// <summary>
		/// Search for the product campaigns.
		/// </summary>
		public virtual Collection<ICampaignHyId> SearchProductCampaignsByHyId(string searchPhrase)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchString", searchPhrase, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("CampaignRepository.SearchProductCampaignsByHyId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignSearchProductCampaigns]", parameters, dbSettings))
			{
				return CreateCampaignHyIdDataMapper(dataReader).ReadMultipleRows();
			}
		}

		/// <summary>
		/// Search for the cart campaigns.
		/// </summary>
		public virtual Collection<ICampaignHyId> SearchCartCampaignsByHyId(string searchPhrase)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchString", searchPhrase, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("CampaignRepository.SearchCartCampaignsByHyId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignSearchCartCampaigns]", parameters, dbSettings))
			{
				return CreateCampaignHyIdDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}