﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartValueRangeConditionRepository
	{
		protected virtual DataMapperBase<ICartValueRangeCondition> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartValueRangeCondition>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValueRange> CreateCurrencyValueRangeDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValueRange>(dataReader);
		}

		public virtual void Save(ICartValueRangeCondition condition)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", condition.Id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartValueRangeConditionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartValueRangeConditionSave]", parameters, dbSettings);
		}

		public virtual void SaveCartValueRange(int conditionId, CurrencyValueRange currencyValueRange)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValueRange.CurrencyId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ValueFrom", currencyValueRange.MonetaryValueFrom, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("ValueTo", currencyValueRange.MonetaryValueTo, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("CartValueRangeConditionRepository.SaveCartValueRange");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartValueRangeConditionCurrencySave]", parameters, dbSettings);
		}

		public virtual void Delete(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartValueRangeConditionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartValueRangeConditionDelete]", parameters, dbSettings);
		}

		public virtual void DeleteAllCartValueRange(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartValueRangeConditionRepository.DeleteAllCartValueRange");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartValueRangeConditionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual ICartValueRangeCondition GetById(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartValueRangeConditionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartValueRangeConditionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<CurrencyValueRange> GetCartValueRangeByCondition(int conditionId)
		{
			Collection<CurrencyValueRange> currencyValueRanges;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartValueRangeConditionRepository.GetCartValueRangeByCondition");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartValueRangeConditionCurrencyGetByCondition]", parameters, dbSettings))
			{
				currencyValueRanges = CreateCurrencyValueRangeDataMapper(dataReader).ReadMultipleRows();
			}

			return currencyValueRanges;
		}
	}
}