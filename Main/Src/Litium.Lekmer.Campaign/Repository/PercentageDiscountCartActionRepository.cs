using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class PercentageDiscountCartActionRepository
	{
		protected virtual DataMapperBase<IPercentageDiscountCartAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IPercentageDiscountCartAction>(dataReader);
		}

		public virtual void Save(IPercentageDiscountCartAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("DiscountAmount", action.DiscountAmount, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("PercentageDiscountCartActionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pPercentageDiscountCartActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("PercentageDiscountCartActionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pPercentageDiscountCartActionDelete]", parameters, dbSettings);
		}

		public virtual IPercentageDiscountCartAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("PercentageDiscountCartActionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pPercentageDiscountCartActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}