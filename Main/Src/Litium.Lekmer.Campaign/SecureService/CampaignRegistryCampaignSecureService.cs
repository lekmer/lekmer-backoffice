﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CampaignRegistryCampaignSecureService : ICampaignRegistryCampaignSecureService
	{
		protected CampaignRegistryCampaignRepository Repository { get; private set; }

		public CampaignRegistryCampaignSecureService(CampaignRegistryCampaignRepository repository)
		{
			Repository = repository;
		}

		public Collection<ICampaignRegistryCampaign> GetRegistriesByCampaignId(int campaignId)
		{
			Repository.EnsureNotNull();
			return Repository.GetRegistriesByCampaignId(campaignId);
		}

		public void Save(int campaignId, Collection<int> campaignRegistries)
		{
			Repository.EnsureNotNull();
			Repository.Save(campaignId, Convert.ToStringIdentifierList(campaignRegistries), Convert.DefaultListSeparator);
		}

		public void Delete(int campaignId)
		{
			Repository.EnsureNotNull();
			Repository.Delete(campaignId);
		}
	}
}