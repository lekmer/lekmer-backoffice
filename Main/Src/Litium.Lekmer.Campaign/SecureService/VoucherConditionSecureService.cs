﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
  public  class VoucherConditionSecureService: IConditionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected VoucherConditionRepository Repository { get; private set; }
		protected IConditionTypeSecureService ConditionTypeSecureService { get; private set; }

		public VoucherConditionSecureService(
			IAccessValidator accessValidator,
			VoucherConditionRepository repository,
			IConditionTypeSecureService conditionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			ConditionTypeSecureService = conditionTypeSecureService;
		}

		/// <summary>
		/// Creates new <see cref="IVoucherCondition"/> object.
		/// </summary>
		/// <returns>The <see cref="IVoucherCondition"/> object</returns>
		public virtual ICondition Create()
		{
			if (ConditionTypeSecureService == null)
			{
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");
			}

			var condition = IoC.Resolve<IVoucherCondition>();

			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("Voucher");
			condition.BatchIdsInclude = new Collection<int>();
			condition.BatchIdsExclude = new Collection<int>();

			condition.Status = BusinessObjectStatus.New;

			return condition;
		}

		/// <summary>
		/// Gets <see cref="IVoucherCondition"/> object by id.
		/// </summary>
		/// <param name="id">Condition id</param>
		/// <returns>The <see cref="IVoucherCondition"/> object</returns>
		public virtual ICondition GetById(int id)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var condition = Repository.GetById(id);

			if (condition == null)
			{
				return null;
			}

			condition.BatchIdsInclude = Repository.GetBatchesIncludeByConditionId(id);
			condition.BatchIdsExclude = Repository.GetBatchesExcludeByConditionId(id);

			return condition;
		}

		/// <summary>
		/// Saves <see cref="IVoucherCondition"/> object.
		/// </summary>
		/// <param name="user">Logged in system user</param>
		/// <param name="condition">Condition to save</param>
		public virtual void Save(ISystemUserFull user, ICondition condition)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var voucherCondition = condition as IVoucherCondition;
			if (voucherCondition == null)
			{
				throw new InvalidOperationException("condition is not IVoucherCondition type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(voucherCondition);

				Repository.DeleteAllBatchIds(voucherCondition.Id);

				foreach (var batchId in voucherCondition.BatchIdsInclude)
				{
					Repository.BatchIncludeInsert(voucherCondition.Id, batchId);
				}

				foreach (var batchId in voucherCondition.BatchIdsExclude)
				{
					Repository.BatchExcludeInsert(voucherCondition.Id, batchId);
				}

				transactedOperation.Complete();
			}
		}

		/// <summary>
		/// Deletes <see cref="IVoucherCondition"/> object.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="id">Condition identifier</param>
		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}
			
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}
	}
}