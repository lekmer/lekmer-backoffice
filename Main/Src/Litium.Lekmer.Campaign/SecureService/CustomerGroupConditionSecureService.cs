﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CustomerGroupConditionSecureService : IConditionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CustomerGroupConditionRepository Repository { get; private set; }
		protected IConditionTypeSecureService ConditionTypeSecureService { get; private set; }

		public CustomerGroupConditionSecureService(
			IAccessValidator accessValidator,
			CustomerGroupConditionRepository repository,
			IConditionTypeSecureService conditionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			ConditionTypeSecureService = conditionTypeSecureService;
		}


		public virtual ICondition Create()
		{
			if (ConditionTypeSecureService == null)
			{
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");
			}

			var condition = IoC.Resolve<ICustomerGroupCondition>();

			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("CustomerGroupUser");
			condition.IncludeCustomerGroups = new CustomerGroupIdDictionary();
			condition.ExcludeCustomerGroups = new CustomerGroupIdDictionary();
			condition.Status = BusinessObjectStatus.New;

			return condition;
		}

		public virtual ICondition GetById(int id)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var condition = Repository.GetById(id);
			if (condition == null)
			{
				return null;
			}

			condition.IncludeCustomerGroups = Repository.GetIncludeCustomerGroups(id);
			condition.ExcludeCustomerGroups = Repository.GetExcludeCustomerGroups(id);

			return condition;
		}

		public virtual void Save(ISystemUserFull user, ICondition condition)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var customerGroupCondition = condition as ICustomerGroupCondition;
			if (customerGroupCondition == null)
			{
				throw new InvalidOperationException("condition is not ICustomerGroupCondition type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(customerGroupCondition);

				SaveIncludeCustomerGroups(customerGroupCondition);
				SaveExcludeCustomerGroups(customerGroupCondition);

				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}


		protected virtual void SaveIncludeCustomerGroups(ICustomerGroupCondition condition)
		{
			Repository.DeleteIncludeCustomerGroups(condition.Id);

			CustomerGroupIdDictionary customerGroupIdDictionary = condition.IncludeCustomerGroups;
			if (customerGroupIdDictionary == null || customerGroupIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> customerGroupIdPair in customerGroupIdDictionary)
			{
				Repository.InsertIncludeCustomerGroup(condition.Id, customerGroupIdPair.Key);
			}
		}

		protected virtual void SaveExcludeCustomerGroups(ICustomerGroupCondition condition)
		{
			Repository.DeleteExcludeCustomerGroups(condition.Id);

			CustomerGroupIdDictionary customerGroupIdDictionary = condition.ExcludeCustomerGroups;
			if (customerGroupIdDictionary == null || customerGroupIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> customerGroupIdPair in customerGroupIdDictionary)
			{
				Repository.InsertExcludeCustomerGroup(condition.Id, customerGroupIdPair.Key);
			}
		}
	}
}