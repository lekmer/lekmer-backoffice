﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class LekmerPercentagePriceDiscountActionSecureService : PercentagePriceDiscountActionSecureService
	{
		protected ICampaignConfigSecureService CampaignConfigSecureService { get; private set; }

		public LekmerPercentagePriceDiscountActionSecureService(
			IAccessValidator accessValidator,
			PercentagePriceDiscountActionRepository repository,
			IProductActionTypeSecureService productActionTypeSecureService,
			ICampaignConfigSecureService campaignConfigSecureService)
			: base(accessValidator, repository, productActionTypeSecureService)
		{
			CampaignConfigSecureService = campaignConfigSecureService;
		}

		public override IProductAction Create()
		{
			if (ProductActionTypeSecureService == null)
			{
				throw new InvalidOperationException("ProductActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IPercentagePriceDiscountAction>() as ILekmerPercentagePriceDiscountAction;
			if (action != null)
			{
				action.ActionType = ProductActionTypeSecureService.GetByCommonName("PercentagePriceDiscount");
				action.CampaignConfig = CampaignConfigSecureService.Create();
				action.Status = BusinessObjectStatus.New;
			}
			return action;
		}
		
		public override void Save(ISystemUserFull user, IProductAction action)
		{
			Repository.EnsureNotNull();

			var percentagePriceDiscountAction = action as ILekmerPercentagePriceDiscountAction;
			if (percentagePriceDiscountAction == null)
			{
				throw new InvalidOperationException("action is not ILekmerPercentagePriceDiscountAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				percentagePriceDiscountAction.CampaignConfigId = CampaignConfigSecureService.Save(percentagePriceDiscountAction.CampaignConfig);
				((LekmerPercentagePriceDiscountActionRepository) Repository).Save(percentagePriceDiscountAction);
				transactedOperation.Complete();
			}
		}

		public override IProductAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = ((LekmerPercentagePriceDiscountActionRepository)Repository).GetById(id);
			if (action == null)
			{
				return null;
			}

			var lekmerAction = action as ILekmerPercentagePriceDiscountAction;
			if (lekmerAction != null)
			{
				lekmerAction.CampaignConfig = CampaignConfigSecureService.GetById(lekmerAction.CampaignConfigId);
			}
			return lekmerAction;
		}
	}
}