﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CartItemPercentageDiscountActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartItemPercentageDiscountActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; set; }

		public CartItemPercentageDiscountActionSecureService(
			IAccessValidator accessValidator,
			CartItemPercentageDiscountActionRepository repository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
			{
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<ICartItemPercentageDiscountAction>();
			action.ActionType = CartActionTypeSecureService.GetByCommonName("CartItemPercentageDiscount");
			action.Status = BusinessObjectStatus.New;
			action.IncludeProducts = new ProductIdDictionary();
			action.ExcludeProducts = new ProductIdDictionary();
			action.IncludeCategories = new CampaignCategoryDictionary();
			action.ExcludeCategories = new CampaignCategoryDictionary();
			action.IncludeBrands = new BrandIdDictionary();
			action.ExcludeBrands = new BrandIdDictionary();
			return action;
		}

		public void Save(ISystemUserFull user, ICartAction action)
		{
			Repository.EnsureNotNull();

			var cartItemPercentageDiscountAction = action as ICartItemPercentageDiscountAction;
			if (cartItemPercentageDiscountAction == null)
			{
				throw new InvalidOperationException("action is not ICartItemPercentageDiscountAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(cartItemPercentageDiscountAction);

				SaveIncludeProducts(cartItemPercentageDiscountAction);
				SaveExcludeProducts(cartItemPercentageDiscountAction);
				SaveIncludeCategories(cartItemPercentageDiscountAction);
				SaveExcludeCategories(cartItemPercentageDiscountAction);
				SaveIncludeBrands(cartItemPercentageDiscountAction);
				SaveExcludeBrands(cartItemPercentageDiscountAction);

				transactedOperation.Complete();
			}
		}

		public void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public ICartAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);
			if (action == null)
			{
				return null;
			}

			action.IncludeProducts = Repository.GetIncludeProducts(id);
			action.ExcludeProducts = Repository.GetExcludeProducts(id);
			action.IncludeCategories = Repository.GetIncludeCategories(id);
			action.ExcludeCategories = Repository.GetExcludeCategories(id);
			action.IncludeBrands = Repository.GetIncludeBrands(id);
			action.ExcludeBrands = Repository.GetExcludeBrands(id);

			return action;
		}

		protected void SaveIncludeProducts(ICartItemPercentageDiscountAction action)
		{
			Repository.DeleteIncludeProducts(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}

			ProductIdDictionary productIdDictionary = action.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertIncludeProduct(action.Id, productIdPair.Key);
			}
		}

		protected void SaveExcludeProducts(ICartItemPercentageDiscountAction action)
		{
			Repository.DeleteExcludeProducts(action.Id);

			ProductIdDictionary productIdDictionary = action.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertExcludeProduct(action.Id, productIdPair.Key);
			}
		}

		protected void SaveIncludeCategories(ICartItemPercentageDiscountAction action)
		{
			Repository.DeleteIncludeCategories(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}

			var categories = action.IncludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, bool> category in categories)
			{
				Repository.InsertIncludeCategory(action.Id, category.Key, category.Value);
			}
		}

		protected void SaveExcludeCategories(ICartItemPercentageDiscountAction action)
		{
			Repository.DeleteExcludeCategories(action.Id);

			var categories = action.ExcludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, bool> category in categories)
			{
				Repository.InsertExcludeCategory(action.Id, category.Key, category.Value);
			}
		}

		protected void SaveIncludeBrands(ICartItemPercentageDiscountAction action)
		{
			Repository.DeleteIncludeBrands(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}

			BrandIdDictionary brandIds = action.IncludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertIncludeBrand(action.Id, brandId.Key);
			}
		}

		protected void SaveExcludeBrands(ICartItemPercentageDiscountAction action)
		{
			Repository.DeleteExcludeBrands(action.Id);

			BrandIdDictionary brandIds = action.ExcludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertExcludeBrand(action.Id, brandId.Key);
			}
		}
	}
}