﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class ProductDiscountActionSecureService : IProductActionPluginSecureService
	{
		private const string SORT_BY = "[p].[ErpId]";

		protected IAccessValidator AccessValidator { get; private set; }
		protected ProductDiscountActionRepository Repository { get; private set; }
		protected IProductActionTypeSecureService ProductActionTypeSecureService { get; set; }

		public ProductDiscountActionSecureService(
			IAccessValidator accessValidator,
			ProductDiscountActionRepository repository,
			IProductActionTypeSecureService productActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			ProductActionTypeSecureService = productActionTypeSecureService;
		}

		public IProductAction Create()
		{
			if (ProductActionTypeSecureService == null) throw new InvalidOperationException("ProductActionTypeSecureService cannot be null.");

			var action = IoC.Resolve<IProductDiscountAction>();
			action.ActionType = ProductActionTypeSecureService.GetByCommonName("ProductDiscount");
			action.ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>();
			action.Status = BusinessObjectStatus.New;
			return action;
		}

		public IProductAction GetById(int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var action = Repository.GetById(id);
			if (action == null)
			{
				return null;
			}
			action.ProductDiscountPrices = Repository.GetItems(id, SORT_BY);
			return action;
		}

		public void Save(ISystemUserFull user, IProductAction action)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");
			
			var productDiscountAction = action as IProductDiscountAction;
			if (productDiscountAction == null) throw new InvalidOperationException("action is not IProductDiscountAction type");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(productDiscountAction);
				SaveItems(productDiscountAction);
				transactedOperation.Complete();
			}
		}

		private void SaveItems(IProductDiscountAction productDiscountAction)
		{
			Repository.DeleteItems(productDiscountAction.Id);

			foreach (var productDiscount in productDiscountAction.ProductDiscountPrices)
			{
				foreach (var item in productDiscount.Value)
				{
					Repository.InsertItem(productDiscountAction.Id, productDiscount.Key, item.Key, item.Value);
				}
			}
		}

		public void Delete(ISystemUserFull user, int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteItems(id);
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}
	}
}
