﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class FlagSecureService : IFlagSecureService
	{
		protected FlagRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public FlagSecureService(FlagRepository flagRepository, IAccessValidator accessValidator)
		{
			Repository = flagRepository;
			AccessValidator = accessValidator;
		}

		public IFlag Create()
		{
			var flag = IoC.Resolve<IFlag>();
			flag.Status = BusinessObjectStatus.New;
			return flag;
		}

		public IFlag GetById(int flagId)
		{
			return Repository.GetByIdSecure(flagId);
		}

		public IFlag GetByCampaign(int campaignId)
		{
			return Repository.GetByCampaignSecure(campaignId);
		}

		public IFlag GetByCampaignFolder(int folderId)
		{
			return Repository.GetByCampaignFolderSecure(folderId);
		}

		public Collection<IFlag> GetAll()
		{
			return Repository.GetAllSecure();
		}

		public int Save(ISystemUserFull user, IFlag flag, IEnumerable<ITranslationGeneric> translations)
		{
			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);
			int flagId;
			using (var transaction = new TransactedOperation())
			{
				flagId = Repository.Save(flag);
				if (flagId > 0)
				{
					SaveTranslations(flagId, translations);
				}
				transaction.Complete();
			}
			CampaignFlagCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();
			return flagId;
		}

		public void SetOrder(ISystemUserFull user, IEnumerable<IFlag> flags)
		{
			if (flags == null) throw new ArgumentNullException("flags");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.SetOrder(flags);
				transactedOperation.Complete();
			}
			CampaignFlagCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();
		}

		public void SaveCampaignFlag(ISystemUserFull user, int campaignId, int? flagId)
		{
			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.SaveCampaignFlag(campaignId, flagId);
				transactedOperation.Complete();
			}
			CampaignFlagCache.Instance.Flush();
		}

		public void SaveCampaignFolderFlag(ISystemUserFull user, int campaignFolderId, int? flagId)
		{
			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.SaveCampaignFolderFlag(campaignFolderId, flagId);
				transactedOperation.Complete();
			}
			CampaignFlagCache.Instance.Flush();
		}

		public void Delete(ISystemUserFull systemUserFull, int flagId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);
			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(flagId);
				transaction.Complete();
			}
			CampaignFlagCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();
		}

		public void Delete(ISystemUserFull user, IEnumerable<int> ids)
		{
			if (ids == null) throw new ArgumentNullException("ids");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);
			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var id in ids)
				{
					Delete(user, id);
				}
				transactedOperation.Complete();
			}
			CampaignFlagCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();
		}

		public Collection<ITranslationGeneric> GetAllTranslationsByFlag(int flagId)
		{
			return Repository.GetAllTranslationsByFlag(flagId);
		}

		private void SaveTranslations(int flagId, IEnumerable<ITranslationGeneric> translations)
		{
			foreach (var translation in translations)
			{
				Repository.SaveTranslation(flagId, translation.LanguageId, translation.Value);
			}
		}
	}
}
