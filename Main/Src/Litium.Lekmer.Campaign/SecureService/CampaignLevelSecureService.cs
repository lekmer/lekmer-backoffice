﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;

namespace Litium.Lekmer.Campaign
{
	public class CampaignLevelSecureService : ICampaignLevelSecureService
	{
		protected CampaignLevelRepository Repository { get; private set; }

		public CampaignLevelSecureService(CampaignLevelRepository repository)
		{
			Repository = repository;
		}

		public Collection<ICampaignLevel> GetAll()
		{
			return Repository.GetAll();
		}
	}
}