﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CartItemsGroupValueConditionSecureService : IConditionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartItemsGroupValueConditionRepository Repository { get; private set; }
		protected CampaignActionRepository CampaignActionRepository { get; private set; }
		protected IConditionTypeSecureService ConditionTypeSecureService { get; private set; }
		protected ICampaignConfigSecureService CampaignConfigSecureService { get; private set; }

		public CartItemsGroupValueConditionSecureService(
			IAccessValidator accessValidator,
			CartItemsGroupValueConditionRepository cartItemsGroupValueConditionRepository,
			CampaignActionRepository campaignActionRepository,
			IConditionTypeSecureService conditionTypeSecureService,
			ICampaignConfigSecureService campaignConfigSecureService)
		{
			AccessValidator = accessValidator;
			Repository = cartItemsGroupValueConditionRepository;
			CampaignActionRepository = campaignActionRepository;
			ConditionTypeSecureService = conditionTypeSecureService;
			CampaignConfigSecureService = campaignConfigSecureService;
		}

		public virtual ICondition Create()
		{
			if (ConditionTypeSecureService == null)
			{
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");
			}

			var condition = IoC.Resolve<ICartItemsGroupValueCondition>();
			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("CartItemsGroupValue");
			condition.Amounts = new LekmerCurrencyValueDictionary();
			condition.CampaignConfig = CampaignConfigSecureService.Create();
			condition.Status = BusinessObjectStatus.New;
			return condition;
		}

		public virtual void Save(ISystemUserFull user, ICondition condition)
		{
			Repository.EnsureNotNull();

			var cartItemsGroupValueCondition = condition as ICartItemsGroupValueCondition;
			if (cartItemsGroupValueCondition == null)
			{
				throw new InvalidOperationException("condition is not ICartItemsGroupValueCondition type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				cartItemsGroupValueCondition.CampaignConfigId = CampaignConfigSecureService.Save(cartItemsGroupValueCondition.CampaignConfig);
				Repository.Save(cartItemsGroupValueCondition);
				SaveAmounts(cartItemsGroupValueCondition);
				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public virtual ICondition GetById(int id)
		{
			Repository.EnsureNotNull();

			var condition = Repository.GetById(id);
			if (condition == null)
			{
				return null;
			}

			condition.Amounts = Repository.GetCurrencyValuesByCondition(id);
			condition.CampaignConfig = CampaignConfigSecureService.GetById(condition.CampaignConfigId);
			return condition;
		}


		protected virtual void SaveAmounts(ICartItemsGroupValueCondition cartItemsGroupValueCondition)
		{
			Repository.DeleteAllCurrencyValue(cartItemsGroupValueCondition.Id);
			
			foreach (var amount in cartItemsGroupValueCondition.Amounts)
			{
				Repository.SaveCurrencyValue(cartItemsGroupValueCondition.Id, amount);
			}
		}
	}
}