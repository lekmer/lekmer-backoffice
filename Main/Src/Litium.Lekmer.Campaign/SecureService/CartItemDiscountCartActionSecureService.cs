﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CartItemDiscountCartActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartItemDiscountCartActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; set; }

		public CartItemDiscountCartActionSecureService(
			IAccessValidator accessValidator,
			CartItemDiscountCartActionRepository cartItemDiscountCartActionRepository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = cartItemDiscountCartActionRepository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public virtual ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
			{
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<ICartItemDiscountCartAction>();

			action.ActionType = CartActionTypeSecureService.GetByCommonName("CartItemDiscount");
			action.Amounts = new LekmerCurrencyValueDictionary();
			action.Status = BusinessObjectStatus.New;
			action.IncludeProducts = new ProductIdDictionary();
			action.ExcludeProducts = new ProductIdDictionary();
			action.IncludeCategories = new CampaignCategoryDictionary();
			action.ExcludeCategories = new CampaignCategoryDictionary();
			action.IncludeBrands = new BrandIdDictionary();
			action.ExcludeBrands = new BrandIdDictionary();

			return action;
		}

		public virtual void Save(ISystemUserFull user, ICartAction action)
		{
			Repository.EnsureNotNull();

			var cartItemDiscountCartAction = action as ICartItemDiscountCartAction;
			if (cartItemDiscountCartAction == null)
			{
				throw new InvalidOperationException("action is not ICartItemDiscountCartAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(cartItemDiscountCartAction);

				SaveAmounts(cartItemDiscountCartAction);

				SaveIncludeProducts(cartItemDiscountCartAction);
				SaveExcludeProducts(cartItemDiscountCartAction);
				SaveIncludeCategories(cartItemDiscountCartAction);
				SaveExcludeCategories(cartItemDiscountCartAction);
				SaveIncludeBrands(cartItemDiscountCartAction);
				SaveExcludeBrands(cartItemDiscountCartAction);

				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public virtual ICartAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);

			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(id);
			action.IncludeProducts = Repository.GetIncludeProducts(id);
			action.ExcludeProducts = Repository.GetExcludeProducts(id);
			action.IncludeCategories = Repository.GetIncludeCategories(id);
			action.ExcludeCategories = Repository.GetExcludeCategories(id);
			action.IncludeBrands = Repository.GetIncludeBrands(id);
			action.ExcludeBrands = Repository.GetExcludeBrands(id);

			return action;
		}


		protected virtual void SaveAmounts(ICartItemDiscountCartAction cartItemDiscountCartAction)
		{
			Repository.DeleteAllCurrencyValue(cartItemDiscountCartAction.Id);

			foreach (var amount in cartItemDiscountCartAction.Amounts)
			{
				Repository.SaveCurrencyValue(cartItemDiscountCartAction.Id, amount);
			}
		}

		protected void SaveIncludeProducts(ICartItemDiscountCartAction action)
		{
			Repository.DeleteIncludeProducts(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}

			ProductIdDictionary productIdDictionary = action.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertIncludeProduct(action.Id, productIdPair.Key);
			}
		}

		protected void SaveExcludeProducts(ICartItemDiscountCartAction action)
		{
			Repository.DeleteExcludeProducts(action.Id);

			ProductIdDictionary productIdDictionary = action.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertExcludeProduct(action.Id, productIdPair.Key);
			}
		}

		protected void SaveIncludeCategories(ICartItemDiscountCartAction action)
		{
			Repository.DeleteIncludeCategories(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}

			var categories = action.IncludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, bool> category in categories)
			{
				Repository.InsertIncludeCategory(action.Id, category.Key, category.Value);
			}
		}

		protected void SaveExcludeCategories(ICartItemDiscountCartAction action)
		{
			Repository.DeleteExcludeCategories(action.Id);

			var categories = action.ExcludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, bool> category in categories)
			{
				Repository.InsertExcludeCategory(action.Id, category.Key, category.Value);
			}
		}

		protected void SaveIncludeBrands(ICartItemDiscountCartAction action)
		{
			Repository.DeleteIncludeBrands(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}

			BrandIdDictionary brandIds = action.IncludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertIncludeBrand(action.Id, brandId.Key);
			}
		}

		protected void SaveExcludeBrands(ICartItemDiscountCartAction action)
		{
			Repository.DeleteExcludeBrands(action.Id);

			BrandIdDictionary brandIds = action.ExcludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertExcludeBrand(action.Id, brandId.Key);
			}
		}
	}
}