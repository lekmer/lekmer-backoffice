﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class ConditionTargetProductTypeSecureService : IConditionTargetProductTypeSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected ConditionTargetProductTypeRepository Repository { get; private set; }

		public ConditionTargetProductTypeSecureService(
			IAccessValidator accessValidator,
			ConditionTargetProductTypeRepository conditionTargetProductTypeRepository)
		{
			AccessValidator = accessValidator;
			Repository = conditionTargetProductTypeRepository;
		}

		public virtual Collection<IConditionTargetProductType> GetAllByCampaign(int campaignId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCampaign(campaignId);
		}

		public virtual void Save(ISystemUserFull systemUserFull, ICondition condition)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			var lekmerCondition = (ILekmerCondition)condition;

			string targetProductTypeIds = Convert.ToStringIdentifierList(lekmerCondition.TargetProductTypes.Keys);

			Repository.Save(condition.Id, targetProductTypeIds, Convert.DefaultListSeparator);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int conditionId)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			Repository.Delete(conditionId);
		}
	}
}
