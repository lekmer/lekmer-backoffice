﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Campaign
{
	public class CampaignLandingPageSecureService : ICampaignLandingPageSecureService
	{
		protected CampaignLandingPageRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public CampaignLandingPageSecureService(CampaignLandingPageRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public ICampaignLandingPage Save(ISystemUserFull systemUserFull, ICampaignLandingPage campaignLandingPage, Collection<ITranslationGeneric> webTitleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			if (campaignLandingPage == null)
			{
				throw new ArgumentNullException("campaignLandingPage");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);
			Repository.EnsureNotNull();

			using (var transaction = new TransactedOperation())
			{
				int campaignLandingPageId = Repository.Save(campaignLandingPage);
				if (campaignLandingPageId > 0)
				{
					campaignLandingPage.CampaignLandingPageId = campaignLandingPageId;
					DeleteTranslations(campaignLandingPageId);
					SaveTranslations(campaignLandingPageId, webTitleTranslations, descriptionTranslations);
				}
				transaction.Complete();
			}

			CampaignLandingPageCache.Instance.Remove(campaignLandingPage.CampaignId);

			return campaignLandingPage;
		}
		public void Delete(ISystemUserFull systemUserFull, int campaignId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign); 
			Repository.EnsureNotNull();
			Repository.Delete(campaignId);

			CampaignLandingPageCache.Instance.Remove(campaignId);
		}
		public void DeleteCampaignRegistryCampaignLandingPage(ISystemUserFull systemUserFull, int campaignLandingPageId, IEnumerable<int> campaignRegistryIds)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);
			Repository.EnsureNotNull();
			Repository.DeleteCampaignRegistryCampaignLandingPage(campaignLandingPageId, Convert.ToStringIdentifierList(campaignRegistryIds));

			CampaignLandingPageCache.Instance.Flush();
		}
		public ICampaignLandingPage GetByCampaignIdSecure(int campaignId)
		{
			Repository.EnsureNotNull();
			return Repository.GetByCampaignIdSecure(campaignId);
		}

		// Translations.
		protected virtual void SaveTranslations(int campaignLandingPageId, Collection<ITranslationGeneric> webTitleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			if (webTitleTranslations == null)
			{
				throw new ArgumentNullException("webTitleTranslations");
			}

			if (descriptionTranslations == null)
			{
				throw new ArgumentNullException("descriptionTranslations");
			}

			for (int index = 0; index < webTitleTranslations.Count; index++)
			{
				var webTitleTranslation = webTitleTranslations[index].Value;
				var descriptionTranslation = descriptionTranslations[index].Value;
				if (!string.IsNullOrEmpty(webTitleTranslation) || !string.IsNullOrEmpty(descriptionTranslation))
				{
					SaveTranslation(campaignLandingPageId, webTitleTranslations[index].LanguageId, webTitleTranslation, descriptionTranslation);
				}
			}
		}
		public void SaveTranslation(int campaignLandingPageId, int languageId, string webTitle, string description)
		{
			Repository.EnsureNotNull();
			Repository.SaveTranslation(campaignLandingPageId, languageId, webTitle, description);
		}
		public void DeleteTranslations(int campaignLandingPageId)
		{
			Repository.EnsureNotNull();
			Repository.DeleteTranslations(campaignLandingPageId);
		}
		public Collection<ITranslationGeneric> GetAllWebTitleTranslations(int campaignLandingPageId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllWebTitleTranslations(campaignLandingPageId);
		}
		public Collection<ITranslationGeneric> GetAllDescriptionTranslations(int campaignLandingPageId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllDescriptionTranslations(campaignLandingPageId);
		}
	}
}