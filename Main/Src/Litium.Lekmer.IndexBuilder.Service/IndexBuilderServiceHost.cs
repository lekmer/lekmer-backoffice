﻿using System;
using System.ServiceProcess;
using Litium.Framework.Search.Configuration;
using Litium.Framework.Search.Indexing;
using Litium.Framework.Search.Indexing.Lucene;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.IndexBuilder.Service
{
	public partial class IndexBuilderServiceHost : ServiceBase
	{
		private IndexWorker _indexWorker;

		public IndexBuilderServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			var indexingService = IoC.Resolve<IIndexingService>();
			var configurationService = IoC.Resolve<IConfigurationService>();
			var jobLocator = IoC.Resolve<IIndexJobLocator>();
			//var indexWorker = new LuceneIndexWorker(indexingService, configurationService, jobLocator, (int) TimeSpan.FromSeconds(10).TotalMilliseconds);
			_indexWorker = new LuceneIndexWorker(
				indexingService, configurationService, jobLocator, (int) TimeSpan.FromMinutes(1).TotalMilliseconds);

			_indexWorker.Start();
		}

		protected override void OnStop()
		{
			if (_indexWorker == null) return;

			_indexWorker.Abort();
		}
	}
}