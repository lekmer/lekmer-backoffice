﻿namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockListService
	{
		IBlockList GetById(int blockId);
	}
}
