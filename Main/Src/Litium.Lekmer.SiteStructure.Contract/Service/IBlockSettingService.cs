namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockSettingService
	{
		IBlockSetting GetByBlock(int blockId);
	}
}