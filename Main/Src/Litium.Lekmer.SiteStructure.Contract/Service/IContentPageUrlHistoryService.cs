using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface IContentPageUrlHistoryService
	{
		Collection<IContentPageUrlHistory> GetAllByUrlTitle(IUserContext context, string urlTitle);

		Collection<IContentPageUrlHistory> GetAll(IUserContext context);

		void Update(int contentPageUrlHistoryId);

		void DeleteExpiredItems();

		void CleanUp(int maxCount);
	}
}