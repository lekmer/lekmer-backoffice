﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	public interface ILekmerBlock : IBlock
	{
		DateTime? StartDate { get; set; }
		DateTime? EndDate { get; set; }
		TimeSpan? StartDailyInterval { get; set; }
		TimeSpan? EndDailyInterval { get; set; }
		bool IsInTimeLimit { get; }
		bool? ShowOnDesktop { get; set; }
		bool? ShowOnMobile { get; set; }
		bool? CanBeUsedAsFallbackOption { get; set; }
		bool UseFallback { get; set; }
	}
}