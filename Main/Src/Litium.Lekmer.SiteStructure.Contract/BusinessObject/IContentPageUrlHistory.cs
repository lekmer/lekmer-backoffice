﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	public interface IContentPageUrlHistory : IBusinessObjectBase
	{
		int Id { get; set; }
		int ContentPageId { get; set; }
		string UrlTitleOld { get; set; }
		DateTime CreationDate { get; set; }
		DateTime LastUsageDate { get; set; }
		int UsageAmount { get; set; }
		int TypeId { get; set; }
	}
}