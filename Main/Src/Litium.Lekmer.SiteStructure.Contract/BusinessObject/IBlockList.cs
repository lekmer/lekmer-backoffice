﻿namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockList : ILekmerBlock
	{
		IBlockSetting Setting { get; set; }
	}
}
