﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockListSecureService
	{
		IBlockList Create();

		IBlockList GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockList block);
	}
}
