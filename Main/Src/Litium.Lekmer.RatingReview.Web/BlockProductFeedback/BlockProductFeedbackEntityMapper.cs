using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockProductFeedbackEntityMapper : LekmerBlockEntityMapper<IBlockProductFeedback>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductFeedback item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddCondition("Block.AllowReview", item.AllowReview); //TODO: Do not save review if not allowed
		}
	}
}