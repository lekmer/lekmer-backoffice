using System.Globalization;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class RatingGroupEntityMapper : EntityMapper<IRatingGroup>
	{
		public override void AddEntityVariables(Fragment fragment, IRatingGroup item)
		{
			fragment.AddVariable("RatingGroup.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingGroup.CommonName", item.CommonName);
			fragment.AddVariable("RatingGroup.Title", item.Title);
			fragment.AddVariable("RatingGroup.Description", item.Description);
		}
	}
}