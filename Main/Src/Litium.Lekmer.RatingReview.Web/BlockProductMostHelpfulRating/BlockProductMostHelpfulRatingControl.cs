using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockProductMostHelpfulRatingControl : BlockProductPageControlBase<IBlockProductMostHelpfulRating>
	{
		private readonly IBlockProductMostHelpfulRatingService _blockProductMostHelpfulRatingService;
		private readonly IRatingReviewFeedbackService _ratingReviewFeedbackService;
		private readonly IRatingItemProductVoteService _ratingItemProductVoteService;
		private readonly IRatingReviewStatusService _ratingReviewStatusService;
		private readonly IReviewService _reviewService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingService _ratingService;
		private readonly IRatingReviewFeedbackLikeService _ratingReviewFeedbackLikeService;
		private readonly IRatingReviewUserLocatorService _ratingReviewUserLocatorService;
		private readonly IProductService _productService;

		private LikeFeedbackForm _likeFeedbackForm;
		private InappropriateFeedbackForm _inappropriateFeedbackForm;
		
		private IProduct _feedbackProduct;
		private IRating _rating;
		private Collection<int> _ratingItemIds;
		private IRatingReviewFeedback _ratingReviewFeedback;
		private IReview _review;

		private Collection<IRatingGroup> _ratingGroups;
		private Collection<IRatingItemProductVote> _ratingItemProductVotes;

		private Dictionary<int, Collection<IRating>> _ratingGroupRatings;
		private Dictionary<int, IRating> _ratings;

		public BlockProductMostHelpfulRatingControl(
			ITemplateFactory templateFactory,
			IBlockProductMostHelpfulRatingService blockProductMostHelpfulRatingService,
			IRatingReviewFeedbackService ratingReviewFeedbackService,
			IRatingItemProductVoteService ratingItemProductVoteService,
			IRatingReviewStatusService ratingReviewStatusService,
			IReviewService reviewService,
			IRatingGroupService ratingGroupService,
			IRatingService ratingService,
			IRatingReviewFeedbackLikeService ratingReviewFeedbackLikeService,
			IRatingReviewUserLocatorService ratingReviewUserLocatorService,
			IProductService productService)
			: base(templateFactory)
		{
			_blockProductMostHelpfulRatingService = blockProductMostHelpfulRatingService;
			_ratingReviewFeedbackService = ratingReviewFeedbackService;
			_ratingItemProductVoteService = ratingItemProductVoteService;
			_ratingReviewStatusService = ratingReviewStatusService;
			_reviewService = reviewService;
			_ratingGroupService = ratingGroupService;
			_ratingService = ratingService;
			_ratingReviewFeedbackLikeService = ratingReviewFeedbackLikeService;
			_ratingReviewUserLocatorService = ratingReviewUserLocatorService;
			_productService = productService;
		}

		protected override IBlockProductMostHelpfulRating GetBlockById(int blockId)
		{
			return _blockProductMostHelpfulRatingService.GetById(UserContext.Current, blockId);
		}


		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render product most helpful rating control on this page ]");
			}

			Initialize();

			if (_rating == null || _review == null || _ratingReviewFeedback == null)
			{
				return new BlockContent();
			}

			var content = RenderContent();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, content.Render(), footer);
		}


		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);

			fragment.AddEntity(Block);

			return fragment.Render();
		}

		protected virtual Fragment RenderContent()
		{
			Fragment fragment = Template.GetFragment("Content");

			fragment.AddEntity(Block);
			fragment.AddEntity(_feedbackProduct);

			fragment.AddVariable("LikeForm", ProcessLikeForm(), VariableEncoding.None);
			fragment.AddVariable("InappropriateForm", ProcessInappropriateForm(), VariableEncoding.None);

			fragment.AddEntity(_ratingReviewFeedback);
			fragment.AddEntity(_review);

			fragment.AddVariable("Review", RenderReview().Render(), VariableEncoding.None);

			fragment.AddVariable("Rating", RenderRating().Render(), VariableEncoding.None);
			fragment.AddVariable("RatingGroupList", RenderRatingGroupList().Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual string ProcessLikeForm()
		{
			_likeFeedbackForm.MapFromRequest(Block.Id, _ratingReviewFeedback.Id);

			bool userCanLike = true;

			if (_likeFeedbackForm.IsFormPostBack && _ratingReviewFeedback.Id == _likeFeedbackForm.PostBackFeedbackId)
			{
				IRatingReviewFeedbackLike feedbackLike = _ratingReviewFeedbackLikeService.Create(UserContext.Current);
				feedbackLike.RatingReviewFeedbackId = _ratingReviewFeedback.Id;
				feedbackLike.RatingReviewUserId = _ratingReviewUserLocatorService.GetRatingReviewUser(UserContext.Current).Id;
				feedbackLike.IPAddress = Request.UserHostAddress;

				int feedbackLikeId = _ratingReviewFeedbackLikeService.Insert(UserContext.Current, feedbackLike);
				userCanLike = feedbackLikeId >= 0;

				if (userCanLike)
				{
					_ratingReviewFeedback.LikeHit = _ratingReviewFeedback.LikeHit + 1;
				}
			}

			return RenderLikeForm(userCanLike);
		}

		protected virtual string RenderLikeForm(bool userCanLike)
		{
			Fragment fragment = Template.GetFragment("LikeForm");

			_likeFeedbackForm.MapToForm(Block.Id, _ratingReviewFeedback.Id, userCanLike, fragment);

			return fragment.Render();
		}


		protected virtual string ProcessInappropriateForm()
		{
			_inappropriateFeedbackForm.MapFromRequest(Block.Id, _ratingReviewFeedback.Id);

			if (_inappropriateFeedbackForm.IsFormPostBack && _ratingReviewFeedback.Id == _inappropriateFeedbackForm.PostBackFeedbackId)
			{
				_ratingReviewFeedbackService.InsertInappropriateFlag(_ratingReviewFeedback.Id);
			}

			return RenderInappropriateForm();
		}

		protected virtual string RenderInappropriateForm()
		{
			Fragment fragment = Template.GetFragment("InappropriateForm");

			_inappropriateFeedbackForm.MapToForm(Block.Id, _ratingReviewFeedback.Id, fragment);

			return fragment.Render();
		}


		protected virtual Fragment RenderReview()
		{
			Fragment fragment = Template.GetFragment("Review");

			fragment.AddEntity(Block);
			fragment.AddEntity(_feedbackProduct);
			fragment.AddEntity(_ratingReviewFeedback);
			fragment.AddEntity(_review);

			return fragment;
		}


		protected virtual Fragment RenderRatingGroupList()
		{
			Fragment fragment = Template.GetFragment("RatingGroupList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingGroup ratingGroup in _ratingGroups)
			{
				if (_ratingGroupRatings.ContainsKey(ratingGroup.Id) && _ratingGroupRatings[ratingGroup.Id].Count > 0)
				{
					stringBuilder.AppendLine(RenderRatingGroup(ratingGroup).Render());
				}
			}

			fragment.AddVariable("Iterate:RatingGroup", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroup(IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("RatingGroup");

			fragment.AddEntity(ratingGroup);
			fragment.AddVariable("RatingList", RenderRatingList(ratingGroup).Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual Fragment RenderRatingList(IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("RatingList");

			var stringBuilder = new StringBuilder();
			foreach (IRating rating in _ratingGroupRatings[ratingGroup.Id])
			{
				stringBuilder.AppendLine(RenderRating(rating, ratingGroup).Render());
			}

			fragment.AddVariable("Iterate:Rating", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRating()
		{
			return RenderRating(_rating, null);
		}

		protected virtual Fragment RenderRating(IRating rating, IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("Rating");

			if (ratingGroup != null)
			{
				fragment.AddEntity(ratingGroup);
			}

			fragment.AddEntity(rating);

			int selectedRatingItemId = FindSelectedRatingItemId(rating.Id);

			fragment.AddVariable("Rating.SelectedRatingItemId", selectedRatingItemId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItemList", RenderRatingItemList(rating, selectedRatingItemId).Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual Fragment RenderRatingItemList(IRating rating, int selectedRatingItemId)
		{
			Fragment fragment = Template.GetFragment("RatingItemList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingItem ratingItem in rating.RatingItems)
			{
				stringBuilder.AppendLine(RenderRatingItem(rating, ratingItem, selectedRatingItemId).Render());
			}

			fragment.AddVariable("Iterate:RatingItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItem(IRating rating, IRatingItem ratingItem, int selectedRatingItemId)
		{
			Fragment fragment = Template.GetFragment("RatingItem");

			fragment.AddEntity(rating);
			fragment.AddEntity(ratingItem);
			fragment.AddEntity(Block);

			fragment.AddCondition("RatingItem.Selected", ratingItem.Id == selectedRatingItemId);

			return fragment;
		}


		protected virtual void Initialize()
		{
			// Get rating linked to Block.
			IBlockRating blockRating = Block.BlockRating;

			if (blockRating != null)
			{
				int ratingId = blockRating.RatingId;

				// Get rating groups linked to Product
				var productRatingDictionaty = new Dictionary<int, IRating>();

				_ratingGroups = _ratingGroupService.GetAllByProduct(UserContext.Current, Product.Id, Product.CategoryId);
				foreach (IRatingGroup productRatingGroup in _ratingGroups)
				{
					foreach (IRating productRating in _ratingService.GetAllByGroup(UserContext.Current, productRatingGroup.Id))
					{
						productRatingDictionaty[productRating.Id] = productRating;
					}
				}

				// Check if Block rating exists in Product ratings.
				if (productRatingDictionaty.ContainsKey(ratingId))
				{
					// Get rating.
					PopulateRating(ratingId);

					// Get Block rating items.
					PopulateRatingItemIds();

					// Get most helpful feedback.
					PopulateRatingReviewFeedback();
					if (_ratingReviewFeedback == null)
					{
						return;
					}

					// Get feddback product
					PopulateFeedbackProduct();

					// Get review for most helpful feedback.
					PopulateReview();

					// Get rating item product votes for most helpful feedback.
					PopulateRatingItemProductVotes();

					// Populate ratings from rating groups.
					PopulateRatingGroupRatings();
				}
			}
		}

		protected virtual void PopulateRating(int ratingId)
		{
			_rating = _ratingService.GetById(UserContext.Current, ratingId);
		}

		protected virtual void PopulateRatingItemIds()
		{
			if (_ratingItemIds == null)
			{
				_ratingItemIds = new Collection<int>();
			}

			foreach (var id in Block.BlockRatingItems.Select(ri => ri.RatingItemId))
			{
				_ratingItemIds.Add(id);
			}
		}

		protected virtual void PopulateRatingReviewFeedback()
		{
			var feedbacks = new Dictionary<int, int>();

			var ratingReviewStatus = _ratingReviewStatusService.GetByCommonName(RatingReviewStatusType.Approved.ToString());
			if (ratingReviewStatus == null)
			{
				return;
			}

			// Get most helpful feedback.
			_ratingReviewFeedback = _ratingReviewFeedbackService.GetMostHelpful(UserContext.Current, Product.Id, _rating.Id, _ratingItemIds, ratingReviewStatus.Id, false);

			if (_ratingReviewFeedback != null)
			{
				feedbacks.Add(_ratingReviewFeedback.Id, _ratingReviewFeedback.Id);

				AddFeedbackIdIntoState(_ratingReviewFeedback.Id);
			}

			CreateForms(feedbacks);
		}

		protected virtual void PopulateReview()
		{
			_review = _reviewService.GetByFeedback(_ratingReviewFeedback.Id);
		}

		protected virtual void PopulateRatingItemProductVotes()
		{
			_ratingItemProductVotes = _ratingItemProductVoteService.GetAllByFeedback(_ratingReviewFeedback.Id);
		}

		protected virtual void PopulateRatingGroupRatings()
		{
			_ratingGroupRatings = new Dictionary<int, Collection<IRating>>();
			_ratings = new Dictionary<int, IRating>();
			_ratings[_rating.Id] = _rating;

			foreach (IRatingGroup ratingGroup in _ratingGroups)
			{
				_ratingGroupRatings[ratingGroup.Id] = new Collection<IRating>();

				foreach (IRating rating in _ratingService.GetAllByGroup(UserContext.Current, ratingGroup.Id))
				{
					if (!_ratings.ContainsKey(rating.Id))
					{
						_ratings[rating.Id] = rating;
						_ratingGroupRatings[ratingGroup.Id].Add(rating);
					}
				}
			}
		}

		protected virtual void PopulateFeedbackProduct()
		{
			if (_ratingReviewFeedback != null)
			{
				if (_ratingReviewFeedback.ProductId != Product.Id)
				{
					_feedbackProduct = _productService.GetById(UserContext.Current, _ratingReviewFeedback.ProductId);
				}
				else
				{
					_feedbackProduct = Product;
				}
			}
		}

		protected virtual int FindSelectedRatingItemId(int ratingId)
		{
			int selectedRatingItemId = -1;

			var selectedRatingItem = _ratingItemProductVotes.FirstOrDefault(v => v.RatingId == ratingId);

			if (selectedRatingItem != null)
			{
				selectedRatingItemId = selectedRatingItem.RatingItemId;
			}

			return selectedRatingItemId;
		}


		protected virtual void CreateForms(Dictionary<int, int> feedbacks)
		{
			string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrl());

			_likeFeedbackForm = CreateLikeFeedbackForm(postUrl);
			_likeFeedbackForm.Feedbacks = feedbacks;

			_inappropriateFeedbackForm = CreateInappropriateFeedbackForm(postUrl);
			_inappropriateFeedbackForm.Feedbacks = feedbacks;
		}

		protected virtual LikeFeedbackForm CreateLikeFeedbackForm(string postUrl)
		{
			return new LikeFeedbackForm(postUrl, "most-helpful-rating-like-" + Block.Id);
		}

		protected virtual InappropriateFeedbackForm CreateInappropriateFeedbackForm(string postUrl)
		{
			return new InappropriateFeedbackForm(postUrl, "most-helpful-rating-inappropriate-" + Block.Id);
		}

		/// <summary>
		/// This feedback id is used in the BlockProductLatestFeedbackListControl to skip the duplicate review
		/// </summary>
		protected virtual void AddFeedbackIdIntoState(int id)
		{
			const string mostHelpfulRatingFeedbackIdKey = "MostHelpfulRating.FeedbackId";
			HttpContext.Current.Items.Add(mostHelpfulRatingFeedbackIdKey, id);
		}
	}
}