using System;
using System.Web;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewUserLocatorService : IRatingReviewUserLocatorService
	{
		private const string TokenCookieKey = "CustomerToken";

		protected ICookieService CookieService { get; private set; }
		protected IRatingReviewUserService RatingReviewUserService { get; private set; }

		public RatingReviewUserLocatorService(ICookieService cookieService, IRatingReviewUserService ratingReviewUserService)
		{
			CookieService = cookieService;
			RatingReviewUserService = ratingReviewUserService;
		}


		public virtual IRatingReviewUser GetRatingReviewUser(IUserContext context)
		{
			CookieService.EnsureNotNull();
			RatingReviewUserService.EnsureNotNull();

			IRatingReviewUser userFromCookie = FetchUserFromCookie();
			IRatingReviewUser userFromSignIn = FetchUserFromSignInCustomer(context);

			if (userFromSignIn != null)
			{
				return userFromSignIn;
			}

			if (userFromCookie != null)
			{
				if (userFromCookie.CustomerId == null && context.Customer != null)
				{
					userFromCookie.CustomerId = context.Customer.Id;
					RatingReviewUserService.Save(userFromCookie);
				}
				
				return userFromCookie;
			}

			IRatingReviewUser newUser = RatingReviewUserService.Create(context);
			newUser = RatingReviewUserService.Save(newUser);
			SetTokenIntoCookie(newUser.Token);

			return newUser;
		}

		public virtual IRatingReviewUser GetRatingReviewUser(IUserContext context, int customerId)
		{
			var user = RatingReviewUserService.GetByCustomer(customerId);

			if (user == null)
			{
				return GetRatingReviewUser(context);
			}

			return user;
		}


		protected virtual IRatingReviewUser FetchUserFromCookie()
		{
			HttpCookie cookie = CookieService.GetRequestCookie(TokenCookieKey);
			if (cookie == null)
			{
				return null;
			}

			string token = cookie.Value;
			IRatingReviewUser ratingReviewUser = null;

			if (IsValidCookieToken(token))
			{
				ratingReviewUser = RatingReviewUserService.GetByToken(token);
			}

			if (ratingReviewUser == null)
			{
				RemoveTokenFromCookie();
			}

			return ratingReviewUser;
		}

		protected virtual IRatingReviewUser FetchUserFromSignInCustomer(IUserContext context)
		{
			if (context.Customer != null)
			{
				int customerId = context.Customer.Id;

				return RatingReviewUserService.GetByCustomer(customerId);
			}

			return null;
		}

		protected virtual void SetTokenIntoCookie(string token)
		{
			var cookie = new HttpCookie(TokenCookieKey);

			cookie.Value = token;
			cookie.Expires = DateTime.Now.AddYears(1);

			CookieService.SetResponseCookie(cookie);
		}

		protected virtual void RemoveTokenFromCookie()
		{
			//Remove cookie from client system.
			var cookie = new HttpCookie(TokenCookieKey);

			cookie.Value = Guid.Empty.ToString();
			cookie.Expires = DateTime.Now.AddYears(-1);

			CookieService.SetResponseCookie(cookie);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
		protected virtual bool IsValidCookieToken(string token)
		{
			try
			{
				return new Guid(token) != Guid.Empty;
			}
			catch
			{
				return false;
			}
		}
	}
}