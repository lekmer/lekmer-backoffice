﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockBestRatedProductListControl : BlockControlBase<IBlockBestRatedProductList>
	{
		private readonly IBlockBestRatedProductListService _blockBestRatedProductListService;
		private readonly IRatingItemProductScoreService _ratingItemProductScoreService;
		private readonly IFilterProductService _filterProductService;
		private readonly IProductService _productService;

		private IPagingControl _pagingControl;
		private ProductCollection _products;

		public BlockBestRatedProductListControl(
			ITemplateFactory templateFactory,
			IBlockBestRatedProductListService blockBestRatedProductListService,
			IRatingItemProductScoreService ratingItemProductScoreService,
			IFilterProductService filterProductService,
			IProductService productService)
			: base(templateFactory)
		{
			_blockBestRatedProductListService = blockBestRatedProductListService;
			_ratingItemProductScoreService = ratingItemProductScoreService;
			_filterProductService = filterProductService;
			_productService = productService;
		}

		protected override IBlockBestRatedProductList GetBlockById(int blockId)
		{
			return _blockBestRatedProductListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			if (_products.Count <= 0)
			{
				return new BlockContent();
			}

			_pagingControl.TotalCount = _products.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Block.Title", Block.Title, VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			var productIds = RenderProductIdsArrayParam();
			string head = RenderFragment("Head", productIds, pagingContent);
			string footer = RenderFragment("Footer", productIds, pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private void Initialize()
		{
			_pagingControl = CreatePagingControl();
			_products = new ProductCollection();

			if (!Block.RatingId.HasValue)
			{
				return;
			}

			var blockBrandIdList = new Collection<int>();

			if (Block.BlockBestRatedProductListBrands != null && Block.BlockBestRatedProductListBrands.Count > 0)
			{
				var brandIds = Block.BlockBestRatedProductListBrands.Select(b => b.Brand.Id).ToList();
				foreach (int id in brandIds)
				{
					blockBrandIdList.Add(id);
				}
			}

			ProductIdCollection productIds = _ratingItemProductScoreService.GetBestRatedProductsByRating(UserContext.Current, Block.RatingId.Value, Block.CategoryId, blockBrandIdList, Block.Setting.ActualTotalItemCount);

			// Get all online products.
			Collection<FilterProduct> filterProducts = _filterProductService.GetAllOnline(UserContext.Current, productIds);

			IEnumerable<FilterProduct> products = filterProducts
				.Take(Block.Setting.ActualTotalItemCount)
				.Skip((_pagingControl.SelectedPage - 1) * _pagingControl.PageSize)
				.Take(_pagingControl.PageSize);

			_products = _productService.PopulateProducts(UserContext.Current, new ProductIdCollection(products.Select(p => p.Id).ToArray()));

			_products.TotalCount = Block.Setting.ActualTotalItemCount <= filterProducts.Count
				? Block.Setting.ActualTotalItemCount
				: filterProducts.Count;
		}

		protected virtual string RenderFragment(string fragmentName, string productIds, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", productIds);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		private IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();

			pagingControl.PageBaseUrl = ResolveUrl("~" + System.Web.HttpContext.Current.Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;

			pagingControl.Initialize();

			return pagingControl;
		}

		private string RenderProductIdsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			for (int i = 0; i < _products.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _products[i].Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", _products[i].Id));
				}
			}
			return itemBuilder.ToString();
		}
	}
}