﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Repository
{
	public class NewsletterUnsubscriberRepository
	{
		protected virtual DataMapperBase<INewsletterUnsubscriber> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INewsletterUnsubscriber>(dataReader);
		}

		public virtual int Save(INewsletterUnsubscriber unsubscriber)
		{
			if (unsubscriber == null) throw new ArgumentNullException("unsubscriber");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", unsubscriber.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", unsubscriber.Email, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@CreatedDate", unsubscriber.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@UpdateDate", unsubscriber.UpdatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterUnsubscriber_Save]", parameters, dbSettings);
		}

		public virtual void SetSentStatus(int unsubscriberId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("UnubscriberId", unsubscriberId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.SetSentStatus");

			new DataHandler().ExecuteCommand("[lekmer].[pNewsletterUnsubscriber_SetSentStatus]", parameters, dbSettings);
		}

		public virtual int Delete(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.Delete");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterUnsubscriber_Delete]", parameters, dbSettings);
		}

		public virtual int DeleteByEmail(int channelId, string email)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", email, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.DeleteByEmail");

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterUnsubscriber_DeleteByEmail]", parameters, dbSettings);
		}

		public virtual INewsletterUnsubscriber Get(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.Get");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterUnsubscriber_GetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual INewsletterUnsubscriber GetByEmail(int channelId, string email)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", email, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.GetByEmail");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterUnsubscriber_GetByEmail]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<INewsletterUnsubscriber> SearchByEmail(int channelId, string email, int page, int pageSize, out int items)
		{
			var unsubscribers = new Collection<INewsletterUnsubscriber>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Email", email, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("PageSize", pageSize, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.SearchByEmail");

			items = 0;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterUnsubscriber_SearchByEmail]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					items = dataReader.GetInt32(0);
				}

				if (dataReader.NextResult())
				{
					unsubscribers = CreateDataMapper(dataReader).ReadMultipleRows();
				}

				return unsubscribers;
			}
		}

		public virtual Collection<INewsletterUnsubscriber> GetAllWithNotSentStatus()
		{
			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberRepository.GetAllWithNotSentStatus");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterUnsubscriber_GetAllWithNotSentStatus]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}