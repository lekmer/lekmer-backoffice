﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Repository
{
	public class NewsletterUnsubscriberOptionRepository
	{
		protected virtual DataMapperBase<INewsletterUnsubscriberOption> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INewsletterUnsubscriberOption>(dataReader);
		}

		public virtual Collection<INewsletterUnsubscriberOption> GetByUnsubscriber(int unsubscriberId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@UnsubscriberId", unsubscriberId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberOptionRepository.GetByUnsubscriber");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterUnsubscriberOption_GetByUnsubscriber]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(INewsletterUnsubscriberOption unsubscriberOption)
		{
			if (unsubscriberOption == null) throw new ArgumentNullException("unsubscriberOption");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("UnsubscriberId", unsubscriberOption.UnsubscriberId, SqlDbType.Int),
					ParameterHelper.CreateParameter("NewsletterTypeId", unsubscriberOption.NewsletterTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", unsubscriberOption.CreatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberOptionRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterUnsubscriberOption_Save]", parameters, dbSettings);
		}

		public virtual void Delete(int unsubscriberOptionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("UnsubscriberOprionId", unsubscriberOptionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("NewsletterUnsubscriberOptionRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pNewsletterUnsubscriberOption_Delete]", parameters, dbSettings);
		}
	}
}