﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Mapper
{
	public class NewsletterSubscriberDataMapper : DataMapperBase<INewsletterSubscriber>
	{
		public NewsletterSubscriberDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override INewsletterSubscriber Create()
		{
			var newsletterSubscriber = IoC.Resolve<INewsletterSubscriber>();

			newsletterSubscriber.Id = MapValue<int>("NewsletterSubscriber.SubscriberId");
			newsletterSubscriber.ChannelId = MapValue<int>("NewsletterSubscriber.ChannelId");
			newsletterSubscriber.Email = MapValue<string>("NewsletterSubscriber.Email");
			newsletterSubscriber.SubscriberTypeId = MapValue<int>("NewsletterSubscriber.SubscriberTypeId");
			newsletterSubscriber.CreatedDate = MapValue<DateTime>("NewsletterSubscriber.CreatedDate");
			newsletterSubscriber.UpdatedDate = MapValue<DateTime>("NewsletterSubscriber.UpdatedDate");

			newsletterSubscriber.SetUntouched();

			return newsletterSubscriber;
		}
	}
}