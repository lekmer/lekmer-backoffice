﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Mapper
{
	public class NewsletterUnsubscriberOptionDataMapper : DataMapperBase<INewsletterUnsubscriberOption>
	{
		public NewsletterUnsubscriberOptionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override INewsletterUnsubscriberOption Create()
		{
			var newsletterSubscriberOption = IoC.Resolve<INewsletterUnsubscriberOption>();

			newsletterSubscriberOption.Id = MapValue<int>("NewsletterUnsubscriberOption.UnsubscriberOprionId");
			newsletterSubscriberOption.UnsubscriberId = MapValue<int>("NewsletterUnsubscriberOption.UnsubscriberId");
			newsletterSubscriberOption.NewsletterTypeId= MapValue<int>("NewsletterUnsubscriberOption.NewsletterTypeId");

			newsletterSubscriberOption.SetUntouched();

			return newsletterSubscriberOption;
		}
	}
}