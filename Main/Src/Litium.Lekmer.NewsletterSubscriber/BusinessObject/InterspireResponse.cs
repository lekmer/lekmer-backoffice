﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponse : IInterspireResponse
	{
		public int Status { get; set; }
		public string ErrorMessage { get; set; }
	}
}