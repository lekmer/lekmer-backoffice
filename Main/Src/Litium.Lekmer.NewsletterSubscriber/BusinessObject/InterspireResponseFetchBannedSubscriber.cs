﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponseFetchBannedSubscriber : InterspireResponse, IInterspireResponseFetchBannedSubscriber
	{
		public IInterspireBanList BanList { get; set; }
	}
}