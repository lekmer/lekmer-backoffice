﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class NewsletterUnsubscriber : BusinessObjectBase, INewsletterUnsubscriber
	{
		private int _id;
		private int _channelId;
		private string _email;
		private DateTime _createdDate;
		private DateTime _updatedDate;
		private Collection<INewsletterUnsubscriberOption> _unsubscriberOptions;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get { return _updatedDate; }
			set
			{
				CheckChanged(_updatedDate, value);
				_updatedDate = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<INewsletterUnsubscriberOption> UnsubscriberOptions
		{
			get { return _unsubscriberOptions; }
			set
			{
				CheckChanged(_unsubscriberOptions, value);
				_unsubscriberOptions = value;
			}
		}
	}
}
