﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponseDeleteSubscriber : InterspireResponse, IInterspireResponseDeleteSubscriber
	{
		public int RemovedSubscribers { get; set; }
	}
}