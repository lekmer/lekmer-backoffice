﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireBanList : IInterspireBanList
	{
		public int BanId { get; set; }
		public string Email { get; set; }
		public string List { get; set; }
	}
}