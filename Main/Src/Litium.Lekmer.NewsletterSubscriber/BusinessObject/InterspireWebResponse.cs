﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireWebResponse : IInterspireWebResponse
	{
		public int StatusCode { get; set; }
		public string StatusDescription { get; set; }
		public string Response { get; set; }
	}
}