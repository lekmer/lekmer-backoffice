﻿using System;
using System.Linq;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterUnsubscriberExporter : INewsletterUnsubscriberExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected INewsletterUnsubscriberService NewsletterUnsubscriberService { get; private set; }
		protected IInterspireConnector InterspireConnector { get; private set; }

		public NewsletterUnsubscriberExporter(INewsletterUnsubscriberService newsletterUnsubscriberService, IInterspireConnector interspireConnector)
		{
			NewsletterUnsubscriberService = newsletterUnsubscriberService;
			InterspireConnector = interspireConnector;
		}

		public virtual void Execute()
		{
			_log.InfoFormat("Get unsubscribers.");
			var unsubscribers = NewsletterUnsubscriberService.GetAllWithNotSentStatus();
			_log.InfoFormat("Count of unsubscribers - {0}", unsubscribers.Count);

			int index = 1;
			foreach (var unsubscriber in unsubscribers)
			{
				bool statusSuccess;
				try
				{
					_log.InfoFormat("Processing: {0}, index: {1}", unsubscriber.Email, index);
					statusSuccess = UnregisterFromInterspire(unsubscriber);
				}
				catch (Exception ex)
				{
					statusSuccess = false;
					_log.ErrorFormat("Interspire error: {0}, error message: {1}", unsubscriber.Email, ex);
				}

				index = index + 1;

				// if status = SUCCESS write to DB and change status on unsubscriber
				if (statusSuccess)
				{
					NewsletterUnsubscriberService.SetSentStatus(unsubscriber.Id);
				}
			}
		}

		protected virtual bool UnregisterFromInterspire(INewsletterUnsubscriber unsubscriber)
		{
			var result = true;
			var email = unsubscriber.Email;

			if (unsubscriber.UnsubscriberOptions.Any(o => o.NewsletterTypeId == (int)NewsletterType.InterspireContactList))
			{
				var response = InterspireConnector.AddBannedSubscriber(email);

				LogResponseFromInterspire(email, response);
				if (response.Status == (int)InterspireResponseStatus.BadWebResponse)
				{
					result = false;
				}
			}
			else
			{
				var interspireResponseFetchBannedSubscriber = InterspireConnector.FetchBannedSubscriber(email);
				if (interspireResponseFetchBannedSubscriber.BanList != null && interspireResponseFetchBannedSubscriber.BanList.Email == email)
				{
					var response = InterspireConnector.RemoveBannedSubscriber(interspireResponseFetchBannedSubscriber.BanList.BanId);
					LogResponseFromInterspire(email, response);
				}
			}

			return result;
		}

		protected virtual void LogResponseFromInterspire(string email, IInterspireResponse response)
		{
			if (response.Status != (int)InterspireResponseStatus.Success)
			{
				_log.WarnFormat("Interspire response: {0}, status: {1}, message: {2}", email, response.Status, response.ErrorMessage);
			}
		}
	}
}