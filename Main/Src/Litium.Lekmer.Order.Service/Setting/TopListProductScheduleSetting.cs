﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Order.Service
{
	public class TopListProductScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "OrderService"; }
		}

		protected override string GroupName
		{
			get { return "TopListProductJob"; }
		}

		public int KeepOrderStatisticDays
		{
			get { return GetInt32(GroupName, "KeepOrderStatisticDays", 28); }
		}
	}
}