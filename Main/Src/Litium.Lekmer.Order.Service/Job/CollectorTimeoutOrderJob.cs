﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Service
{
	public class CollectorTimeoutOrderJob : BaseJob
	{
		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "CollectorTimeoutOrderJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<ICollectorTimeoutOrderService>().ManageCollectorTimeoutOrders();
		}
	}
}
