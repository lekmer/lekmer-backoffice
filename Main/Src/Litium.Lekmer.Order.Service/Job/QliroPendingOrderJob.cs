﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Service
{
	public class QliroPendingOrderJob : BaseJob
	{
		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "QliroPendingOrderJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<IPendingOrderService>().CheckQliroOrders();
		}
	}
}
