﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Service
{
	public class KlarnaPendingOrderJob : BaseJob
	{
		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "KlarnaPendingOrderJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<IPendingOrderService>().CheckKlarnaOrders();
		}
	}
}
