﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Product;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Cleaner.Service
{
	public class UrlHistoryCleanerJob : BaseJob
	{
		public override string Group
		{
			get { return "Cleaner"; }
		}

		public override string Name
		{
			get { return "UrlHistoryCleanerJob"; }
		}

		protected override void ExecuteAction()
		{
			var urlHistoryScheduleSetting = new UrlHistoryScheduleSetting();

			var productUrlHistoryService = IoC.Resolve<IProductUrlHistoryService>();

			productUrlHistoryService.DeleteExpiredItems();
			productUrlHistoryService.CleanUp(urlHistoryScheduleSetting.UrlHistoryMaxNumber);

			var contentPageUrlHistoryService = IoC.Resolve<IContentPageUrlHistoryService>();

			contentPageUrlHistoryService.DeleteExpiredItems();
			contentPageUrlHistoryService.CleanUp(urlHistoryScheduleSetting.UrlHistoryMaxNumber);
		}
	}
}
