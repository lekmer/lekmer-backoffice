﻿namespace Litium.Lekmer.Cleaner.Service
{
	public class CartCleanerScheduleSetting : BaseCleanerScheduleSetting
	{
		protected override string GroupName
		{
			get { return "CartCleanerJob"; }
		}
	}
}