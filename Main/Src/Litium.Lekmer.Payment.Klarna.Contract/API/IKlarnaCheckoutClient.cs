﻿using System;
using System.Collections.Generic;
using Klarna.Checkout;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaCheckoutClient
	{
		IKlarnaCheckoutSetting KlarnaSetting { get; set; }

		string GetKlarnaCheckoutContent(IUserContext context, List<ICartItem> items, decimal freightCost, decimal? discountAmount, ref Uri orderLocation);
		string GetKlarnaConfirmationContent(Uri orderLocation);
		Order MakeOrder(Uri resourceUri);
	}
}