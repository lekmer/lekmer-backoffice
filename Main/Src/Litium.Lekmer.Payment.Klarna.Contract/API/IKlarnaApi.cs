﻿using System.Collections.Generic;
using Litium.Lekmer.Payment.Klarna.Setting;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaApi
	{
		IKlarnaSetting KlarnaSetting { get; set; }

		void CreateOrderValueSpecificClient(double orderValue);

		void FetchPClasses();
		List<object> GetPClasses();

		/// <summary>
		/// Retrieve address information from Klarna for specified civic number
		/// </summary>
		IKlarnaGetAddressResponse GetAddress(string civicNumber, int pnoEncoding, string ip);

		/// <summary>
		/// Used to create an item for the goodsList
		/// </summary>
		/// <param name="quantity">Quantity of the articles</param>
		/// <param name="articleNumber">Article number</param>
		/// <param name="title">Article title</param>
		/// <param name="price">The articles price, including VAT</param>
		/// <param name="vat">VAT in percent</param>
		/// <param name="discount">Discount in percent</param>
		void AddArticle(int quantity, string articleNumber, string title, double price, double vat, double discount);

		/// <summary>
		/// Used to create an shipment item for the goodsList
		/// </summary>
		/// <param name="quantity">Quantity of the articles</param>
		/// <param name="articleNumber">Article number</param>
		/// <param name="title">Article title</param>
		/// <param name="price">The articles price, including VAT</param>
		/// <param name="vat">VAT in percent</param>
		void AddShipment(int quantity, string articleNumber, string title, double price, double vat);

		void SetShippingAddress(
			string email,
			string telNo,
			string cellNo,
			string fName,
			string lName,
			string careof,
			string company,
			string street,
			string zip,
			string city,
			string countryIso,
			string houseNumber,
			string houseExtension);

		void SetBillingAddress(
			string email,
			string telNo,
			string cellNo,
			string fName,
			string lName,
			string careof,
			string company,
			string street,
			string zip,
			string city,
			string countryIso,
			string houseNumber,
			string houseExtension);

		/// <summary>
		/// Reserves an amount for a company.
		/// </summary>
		IKlarnaReservationResult ReserveAmountCompany(
			string orgNumber,
			int? gender,
			double amount,
			string reference,
			int orderId,
			string ip,
			int pClass);

		/// <summary>
		/// Reserves a purchased amount to a particular customer. The reservation is valid, by default, in 7 days.
		/// </summary>
		IKlarnaReservationResult ReserveAmount(
			string civicNumber,
			int? gender,
			double amount,
			int orderId,
			string ip,
			int pClass,
			double yearlySalary);

		/// <summary>
		/// Cancelling a reservation
		/// </summary>
		IKlarnaResult CancelReservation(int orderId, string reservationNumber);

		/// <summary>
		/// Checks status on yor order
		/// </summary>
		IKlarnaResult CheckOrderStatus(int orderId, string reservationNumber);

		double CalculateCheapestMonthlyFee();
		double CalculateCheapestMonthlyCost(double sum, int flags);
		double CalculateMonthlyCost(double sum, object pClass, int flags);

		void Initialize(string channelCommonName);
	}
}