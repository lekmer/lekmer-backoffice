﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public interface IKlarnaPendingIntervalSetting
	{
		int OrderStatusCheckInterval { get; }
		int DecisionGuaranteedInterval { get; }
	}
}