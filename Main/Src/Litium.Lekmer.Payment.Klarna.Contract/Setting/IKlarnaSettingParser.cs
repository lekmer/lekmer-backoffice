﻿using System.Collections.Generic;

namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public interface IKlarnaSettingParser
	{
		IEnumerable<ITimeOutData> ParseTimeoutSteps(string rawTimeoutSteps);
	}
}