﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Payment.Klarna
{
	[Serializable]
	// Important: This attribute is NOT inherited from Exception, and MUST be specified 
	// otherwise serialization will fail with a SerializationException stating that
	// "Type X in Assembly Y is not marked as serializable."
	public class KlarnaExceptionCustom : Exception
	{
		public KlarnaExceptionCustom()
		{
		}

		public KlarnaExceptionCustom(string message)
			: base(message)
		{
		}

		public KlarnaExceptionCustom(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		// Without this constructor, deserialization will fail
		protected KlarnaExceptionCustom(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
