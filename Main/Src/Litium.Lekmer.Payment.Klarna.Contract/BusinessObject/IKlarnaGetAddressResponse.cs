﻿using System.Collections.Generic;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaGetAddressResponse : IKlarnaResult
	{
		List<object> Addresses { get; set; }
	}
}
