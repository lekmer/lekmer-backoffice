﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using Litium.Lekmer.Pacsoft.Contract;

namespace Litium.Lekmer.Pacsoft
{
	public class PacsoftInfoFile : BaseXmlFile
	{
		private IPacsoftExportSetting _setting;
		private IPacsoftInfo _pacsoftInfo;

		private bool _isInitialized;

		public void Initialize(IPacsoftExportSetting setting, IPacsoftInfo pacsoftInfo)
		{
			if (setting == null)
			{
				throw new ArgumentNullException("setting");
			}

			_setting = setting;
			_pacsoftInfo = pacsoftInfo;

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) and should be called first.");
			}

			var doc = CreateXmlDocument(_setting.Encoding);

			XmlNode dataNode = doc.CreateElement(ExporterConstants.NodeData);
			doc.AppendChild(dataNode);

			XmlNode shipmentNode = AddNode(doc, dataNode, ExporterConstants.NodeShipment);
			AddAttribute(doc, shipmentNode, ExporterConstants.OrdernoAttribute, _pacsoftInfo.OrderId.ToString(CultureInfo.InvariantCulture));
			AddShipmentChilds(doc, shipmentNode, _pacsoftInfo);

			XmlNode receiverNode = AddNode(doc, dataNode, ExporterConstants.NodeReceiver);
			AddAttribute(doc, receiverNode, ExporterConstants.RcvIdAttribute, _setting.RcvId);
			AddReceiverChilds(doc, receiverNode, _pacsoftInfo);

			
			doc.Save(savedSiteMap);
		}

		protected void AddReceiverChilds(XmlDocument xmlDocument, XmlNode parentNode, IPacsoftInfo pacsoftInfo)
		{
			// name
			if (!string.IsNullOrEmpty(pacsoftInfo.Name))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Name, ExporterConstants.NameAttribute);
			}

			// address1
			if (!string.IsNullOrEmpty(pacsoftInfo.Address1))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Address1, ExporterConstants.Address1Attribute);
			}

			// address2
			if (!string.IsNullOrEmpty(pacsoftInfo.Address2))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Address2, ExporterConstants.Address2Attribute);
			}

			// city
			if (!string.IsNullOrEmpty(pacsoftInfo.City))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.City, ExporterConstants.CityAttribute);
			}

			// country
			if (!string.IsNullOrEmpty(pacsoftInfo.Country))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Country, ExporterConstants.CountryAttribute);
			}

			// zipcode
			if (!string.IsNullOrEmpty(pacsoftInfo.PostalCode))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.PostalCode, ExporterConstants.ZipcodeAttribute);
			}

			// email
			if (!string.IsNullOrEmpty(pacsoftInfo.Email))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Email, ExporterConstants.EmailAttribute);
			}

			// phone
			if (!string.IsNullOrEmpty(pacsoftInfo.Phone))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Phone, ExporterConstants.PhoneAttribute);
			}

			// sms
			if (!string.IsNullOrEmpty(pacsoftInfo.Sms))
			{
				CreateValNode(xmlDocument, parentNode, pacsoftInfo.Sms, ExporterConstants.SmsAttribute);
			}
		}

		protected void AddShipmentChilds(XmlDocument xmlDocument, XmlNode parentNode, IPacsoftInfo pacsoftInfo)
		{
			// from
			CreateValNode(xmlDocument, parentNode, _setting.QuickId, ExporterConstants.FromAttribute);

			// to
			CreateValNode(xmlDocument, parentNode, _setting.RcvId, ExporterConstants.ToAttribute);

			// service
			XmlNode serviceNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeService);
			AddAttribute(xmlDocument, serviceNode, ExporterConstants.SrvIdAttribute, _setting.Srvid);
			// addon notsms
			XmlNode addonNotsms = AddNode(xmlDocument, serviceNode, ExporterConstants.NodeAddon, string.Empty);
			AddAttribute(xmlDocument, addonNotsms, ExporterConstants.AdnIdAttribute, ExporterConstants.AdnidAttributeValue);

			// ufonline
			XmlNode ufonlineNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeUfonline);
			// option
			XmlNode optionNode = AddNode(xmlDocument, ufonlineNode, ExporterConstants.NodeOption);
			AddAttribute(xmlDocument, optionNode, ExporterConstants.OptidAttribute, ExporterConstants.OptidAttributeValue);
			// from
			CreateValNode(xmlDocument, optionNode, _setting.SmsFrom, ExporterConstants.FromAttribute);
			// to
			CreateValNode(xmlDocument, optionNode, pacsoftInfo.Email, ExporterConstants.ToAttribute);

			// container
			XmlNode containerNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeContainer);
			AddAttribute(xmlDocument, containerNode, ExporterConstants.TypeAttribute, ExporterConstants.TypeAttributeValue);
			// copies
			CreateValNode(xmlDocument, containerNode, pacsoftInfo.ItemsCount.ToString(CultureInfo.InvariantCulture), ExporterConstants.CopiesAttribute);
			// contents
			CreateValNode(xmlDocument, containerNode, _setting.Contents, ExporterConstants.ContentsAttribute);
		}

		private void CreateValNode(XmlDocument xmlDocument, XmlNode parentNode, string nodeValue, string attributeValue)
		{
			XmlNode node = AddNode(xmlDocument, parentNode, ExporterConstants.NodeVal, nodeValue);
			AddAttribute(xmlDocument, node, ExporterConstants.NAttribute, attributeValue);
		}
	}
}