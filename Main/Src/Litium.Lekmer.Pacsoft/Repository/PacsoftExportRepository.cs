﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Pacsoft
{
	public class PacsoftExportRepository
	{
		public virtual void PopulatePacsoftOrders(int channelId, string productHyId, string supplierId, int daysAfterPurchase)
		{
			var parameters = new[]
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("HyId", productHyId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("SupplierId", supplierId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("DaysAfterPurchase", daysAfterPurchase, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PacsoftExportRepository.Populatepacsoftorders");
			new DataHandler().ExecuteCommand("[orderlek].[pPacsoftOrderPopulate]", parameters, dbSettings);
		}

		public virtual void Update(int orderId)
		{
			var parameters = new[]
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PacsoftExportRepository.Update");
			new DataHandler().ExecuteCommand("[orderlek].[pPacsoftOrderUpdate]", parameters, dbSettings);
		}
	}
}
