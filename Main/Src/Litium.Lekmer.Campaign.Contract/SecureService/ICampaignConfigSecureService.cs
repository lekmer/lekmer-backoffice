﻿namespace Litium.Lekmer.Campaign
{
	public interface ICampaignConfigSecureService
	{
		ICampaignConfig Create();
		ICampaignConfig GetById(int campaignConfigId);
		int Save(ICampaignConfig campaignConfig);
	}
}