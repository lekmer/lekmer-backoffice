﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface IFlagService
	{
		IFlag GetByCampaign(IUserContext context, int campaignId);
		Collection<IFlag> GetByProduct(IUserContext context, int productId);
	}
}