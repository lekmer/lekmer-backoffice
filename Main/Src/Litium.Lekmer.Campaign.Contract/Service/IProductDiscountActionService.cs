﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface IProductDiscountActionService
	{
		Collection<int> GetProductIds(int productActionId);
	}
}