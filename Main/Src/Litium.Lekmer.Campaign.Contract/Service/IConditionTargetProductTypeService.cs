﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface IConditionTargetProductTypeService
	{
		Collection<IConditionTargetProductType> GetAllByCampaign(int campaignId);
	}
}