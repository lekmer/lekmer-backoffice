﻿namespace Litium.Lekmer.Campaign
{
	public interface ICampaignPriceTypeService
	{
		ICampaignPriceType GetById(int id);
	}
}