﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerPercentagePriceDiscountActionService
	{
		Collection<int> GetProductIds(int productActionId);
	}
}