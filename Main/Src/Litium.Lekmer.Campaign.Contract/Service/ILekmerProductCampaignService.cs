﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerProductCampaignService : IProductCampaignService
	{
		ICampaignToTag CampaignToTagGet();
		void CampaignToTagDelete(int id);

		Collection<IProductCampaign> GetDistinctCampaigns(IUserContext context, Collection<IProductActionAppliedItem> productActionItems);
	}
}