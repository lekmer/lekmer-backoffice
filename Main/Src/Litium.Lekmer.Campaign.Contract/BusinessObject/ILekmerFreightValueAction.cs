﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerFreightValueAction : ILekmerCartAction, IFreightValueAction
	{
		IdDictionary DeliveryMethodIds { get; set; }
		new LekmerCurrencyValueDictionary Amounts { get; set; }
	}
}