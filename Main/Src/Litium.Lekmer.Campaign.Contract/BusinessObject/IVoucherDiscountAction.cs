namespace Litium.Lekmer.Campaign
{
	public interface IVoucherDiscountAction : ILekmerCartAction
	{
		bool FixedDiscount { get; set; }
		bool PercentageDiscount { get; set; }
		decimal? DiscountValue { get; set; }
		LekmerCurrencyValueDictionary Amounts { get; set; }
		bool AllowSpecialOffer { get; set; }
	}
}