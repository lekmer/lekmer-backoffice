﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignHyId 
	{
		string ErpId { get; set; }
		ICampaignStatus CampaignStatus { get; set; }
		int CampaignStatusId { get; set; }
		string CampaignStatusTitle { get; set; }
		string CampaignStatusCommonName { get; set; }
		DateTime? EndDate { get; set; }
		bool Exclusive { get; set; }
		int FolderId { get; set; }
		int Id { get; set; }
		int Priority { get; set; }
		DateTime? StartDate { get; set;}
		string Title { get; set; }
	}


}
