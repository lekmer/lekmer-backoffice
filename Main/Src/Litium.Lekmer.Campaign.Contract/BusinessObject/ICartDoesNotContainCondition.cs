﻿namespace Litium.Lekmer.Campaign
{
	public interface ICartDoesNotContainCondition : ILekmerCondition
	{
		int CampaignConfigId { get; set; }
		ICampaignConfig CampaignConfig { get; set; }
	}
}