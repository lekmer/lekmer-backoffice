﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerPercentagePriceDiscountAction : ILekmerProductAction, IPercentagePriceDiscountAction
	{
		int CampaignConfigId { get; set; }
		ICampaignConfig CampaignConfig { get; set; }
		new decimal DiscountAmount { get; set; }
	}
}