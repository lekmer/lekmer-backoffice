using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ICartItemDiscountCartAction : ILekmerCartAction
	{
		int Quantity { get; set; }
		bool ApplyForCheapest { get; set; }
		bool IsPercentageDiscount { get; set; }
		decimal? PercentageDiscountAmount { get; set; }
		LekmerCurrencyValueDictionary Amounts { get; set; }
		bool IncludeAllProducts { get; set; }
		ProductIdDictionary IncludeProducts { get; set; }
		ProductIdDictionary ExcludeProducts { get; set; }
		CampaignCategoryDictionary IncludeCategories { get; set; }
		CampaignCategoryDictionary ExcludeCategories { get; set; }
		BrandIdDictionary IncludeBrands { get; set; }
		BrandIdDictionary ExcludeBrands { get; set; }
	}
}