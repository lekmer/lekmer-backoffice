﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignRegistryCampaignLandingPage : IBusinessObjectBase
	{
		int CampaignLandingPageId { get; set; }
		int CampaignRegistryId { get; set; }
		int? IconMediaId { get; set; }
		int? ImageMediaId { get; set; }
		int ContentNodeId { get; set; }
	}
}