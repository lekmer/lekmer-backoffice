﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerProductAction : IProductAction
	{
		IdDictionary TargetProductTypes { get; set; }
	}
}