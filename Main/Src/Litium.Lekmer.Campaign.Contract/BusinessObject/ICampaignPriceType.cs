﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignPriceType : IBusinessObjectBase
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
	}
}