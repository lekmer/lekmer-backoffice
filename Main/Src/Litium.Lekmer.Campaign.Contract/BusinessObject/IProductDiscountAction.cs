﻿using System.Collections.Generic;

namespace Litium.Lekmer.Campaign
{
	public interface IProductDiscountAction : ILekmerProductAction
	{
		Dictionary<int, LekmerCurrencyValueDictionary> ProductDiscountPrices { get; set; }
	}
}