﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ICartItemPercentageDiscountAction : ILekmerCartAction
	{
		decimal DiscountAmount { get; set; }
		bool IncludeAllProducts { get; set; }
		ProductIdDictionary IncludeProducts { get; set; }
		ProductIdDictionary ExcludeProducts { get; set; }
		CampaignCategoryDictionary IncludeCategories { get; set; }
		CampaignCategoryDictionary ExcludeCategories { get; set; }
		BrandIdDictionary IncludeBrands { get; set; }
		BrandIdDictionary ExcludeBrands { get; set; }
	}
}