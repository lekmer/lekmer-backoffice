﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignRegistryCampaign : IBusinessObjectBase
	{
		int CampaignRegistryId { get; set; }

		int CampaignId { get; set; }

		int? ChannelId { get; set; }

		string CountryIso { get; set; }
	}
}