namespace Litium.Lekmer.Campaign
{
	public struct CartItemDiscountKey
	{
		private readonly int _productId;
		private readonly decimal _price;
		private readonly int? _sizeId;
		private readonly bool _isAffectedByCampaign;

		public CartItemDiscountKey(int productId, decimal price, int? sizeId, bool isAffectedByCampaign)
		{
			_productId = productId;
			_price = price;
			_sizeId = sizeId;
			_isAffectedByCampaign = isAffectedByCampaign;
		}

		public int ProductId
		{
			get { return _productId; }
		}

		public decimal Price
		{
			get { return _price; }
		}

		public int? SizeId
		{
			get { return _sizeId; }
		}

		public bool IsAffectedByCampaign
		{
			get { return _isAffectedByCampaign; }
		}

		public bool Equals(CartItemDiscountKey other)
		{
			return other._productId == _productId && other._price == _price && other._sizeId == _sizeId && other._isAffectedByCampaign == _isAffectedByCampaign;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (obj.GetType() != typeof(CartItemDiscountKey)) return false;
			return Equals((CartItemDiscountKey)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (_productId * (_sizeId ?? 1) * 397) ^ _price.GetHashCode();
			}
		}

		public static bool operator ==(CartItemDiscountKey left, CartItemDiscountKey right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(CartItemDiscountKey left, CartItemDiscountKey right)
		{
			return !left.Equals(right);
		}
	}
}