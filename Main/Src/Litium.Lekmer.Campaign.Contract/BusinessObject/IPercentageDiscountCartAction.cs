using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface IPercentageDiscountCartAction : ILekmerCartAction
	{
		decimal DiscountAmount { get; set; }
	}
}