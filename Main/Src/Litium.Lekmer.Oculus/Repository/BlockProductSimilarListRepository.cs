﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Oculus.Repository
{
	public class BlockProductSimilarListRepository
	{
		protected virtual DataMapperBase<IBlockProductSimilarList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductSimilarList>(dataReader);
		}

		public virtual IBlockProductSimilarList GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockProductSimilarListRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockProductSimilarListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductSimilarList GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockProductSimilarListRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockProductSimilarListGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockProductSimilarList block)
		{
			if (block == null) throw new ArgumentNullException("block");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("PriceRangeType", block.PriceRangeType, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockProductSimilarListRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductSimilarListSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockProductSimilarListRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductSimilarListDelete]", parameters, dbSettings);
		}
	}
}