﻿using System;

namespace Litium.Lekmer.Oculus
{
	[Serializable]
	public class SimilarProductItem : ISimilarProductItem
	{
		public int ProductId { get; set; }

		public string ProductErpId { get; set; }

		public decimal Score { get; set; }
	}
}