﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionCategoryService
	{
		Collection<ICdonExportRestrictionItem> GetIncludeAll();
		Collection<ICdonExportRestrictionItem> GetRestrictionAll();
	}
}