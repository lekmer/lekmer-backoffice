﻿namespace Litium.Lekmer.CdonExport
{
	public interface ICdonExportSetting
	{
		int ExportInterval { get; }
		int ExecutionHour { get; }
		int ExecutionMinute { get; }
		int IncrementalExportInterval { get; }
		int IncrementalExportExecutionHour { get; }
		int IncrementalExportExecutionMinute { get; }
		string ExcludeChannels { get; }
		string NotRoundPriceInChannels { get; }
		string ApplicationName { get; }
		string DestinationDirectory { get; }
		string FileNameFullExport { get; }
		string FileNameIncrementalExport { get; }
		string ZipFileDirectory { get; }
		string ZipFileExtension { get; }
		string SizeCommonName { get; }
		string ColorTagGroupCommonName { get; }
		string ExcludeTagGroups { get; }
		string SpecificTagGroupIds { get; }
		string NotShoesCategoryIds { get; }
		string TopLevelCategoryIds { get; }
		bool NeedToValidate { get; }
		string XsdFilePath { get; }
		int PageSize { get; }
		int BatchSize { get; }
		int MonthPurchasedAgo { get; }

		// Tag <-> Alias logic
		string ChannelTemplateIds { get; }
		string GetChannelTemplateId(string channelName);
		string FragmentName { get; }
		string AliasBegin { get; }
		string AliasBeginReplace { get; }
		string AliasEnd { get; }
		string AliasEndReplace { get; }
		string MainKey { get; }
	}
}