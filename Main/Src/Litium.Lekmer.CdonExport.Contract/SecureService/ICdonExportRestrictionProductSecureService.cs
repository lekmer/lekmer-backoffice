﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionProductSecureService
	{
		void InsertIncludeProduct(ICdonExportRestrictionItem product, List<int> productChannelIds);

		void InsertRestrictionProduct(ICdonExportRestrictionItem product, List<int> productChannelIds);

		void DeleteIncludeProducts(List<int> productIds);

		void DeleteRestrictionProducts(List<int> productIds);

		Collection<ICdonExportRestrictionItem> GetIncludeAll();

		Collection<ICdonExportRestrictionItem> GetRestrictionAll();
	}
}