﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum UnitTypeLenght
	{
		[Description("mm")]
		Millimeter = 1,
		[Description("cm")]
		Centimeter = 2,
		[Description("m")]
		Meter = 3
	}
}