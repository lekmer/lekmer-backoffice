﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum UnitTypeShoes
	{
		[Description("EU")]
		EU = 1,
		[Description("US MALE")]
		USMALE = 2,
		[Description("US FEMALE")]
		USFEMALE = 3,
		[Description("UK MALE")]
		UKMALE = 4,
		[Description("UK FEMALE")]
		UKFEMALE = 5
	}
}