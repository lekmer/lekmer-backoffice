﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum UnitType
	{
		[Description("Mon")]
		Month = 1,
		[Description("Qty")]
		Quantity = 2
	}
}