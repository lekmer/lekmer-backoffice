﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum AttributeDataType
	{
		[Description("INT")]
		Int,
		[Description("DECIMAL(18,4)")]
		Decimal,
		[Description("FLOAT")]
		Float,
		[Description("DATETIME")]
		DateTime,
		[Description("STRING")]
		String,
		[Description("BIT")]
		Bit
	}
}
