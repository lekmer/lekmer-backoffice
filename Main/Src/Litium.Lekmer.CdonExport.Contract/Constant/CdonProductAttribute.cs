﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum CdonProductAttribute
	{
		EAN_CODE = 1,
		CATEGORY_ID = 2,
		[Description("Short Description:SE,Short Description:NO,Short Description:DK,Short Description:FI,Short Description:FR,Short Description:NL")]
		SHORT_DESCRIPTION = 3,
		[Description("Size EU:SE,Size EU:NO,Size EU:DK,Size EU:FI,Size EU:FR,Size EU:NL")]
		SIZE_EU = 4,
		[Description("COLOR:SE,COLOR:NO,COLOR:DK,COLOR:FI,COLOR:FR,COLOR:NL")]
		COLOR = 5,
		[Description("AGE_FROM_MONTH:SE,AGE_FROM_MONTH:NO,AGE_FROM_MONTH:DK,AGE_FROM_MONTH:FI,AGE_FROM_MONTH:FR,AGE_FROM_MONTH:NL")]
		AGE_FROM_MONTH = 6,
		[Description("AGE_TO_MONTH:SE,AGE_TO_MONTH:NO,AGE_TO_MONTH:DK,AGE_TO_MONTH:FI,AGE_TO_MONTH:FR,AGE_TO_MONTH:NL")]
		AGE_TO_MONTH = 7,
		IS_NEW_FROM = 8,
		IS_NEW_TO = 9,
		MEASUREMENT = 10,
		BATTERY_TYPE_ID = 11,
		NUMBER_OF_BATTARIES = 12,
		IS_BATTARY_INCLUDED = 13,
		HAS_SIZES = 14,
		LEKMER_ERP_ID = 15,
		ITEMS_IN_PACKAGE = 16,
		WEB_SHOP_TITLE = 17,
		ID = 18,
		SIZE_ERP_ID = 19,
		COLOR_ERP_ID = 20,
		BRAND_ID = 21,
		[Description("ICON:SE,ICON:NO,ICON:DK,ICON:FI,ICON:FR,ICON:NL")]
		ICON = 22
	}
}