﻿namespace Litium.Lekmer.CdonExport
{
	public enum CdonProductDefaultAttribute
	{
		CDON_PRODUCT_TITLE,
		CDON_PRODUCT_DESCRIPTION,
		CDON_PRODUCT_FRONT_IMAGE,
		CDON_PRODUCT_EXTRA_IMAGE
	}
}