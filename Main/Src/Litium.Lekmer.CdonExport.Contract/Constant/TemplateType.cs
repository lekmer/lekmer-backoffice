﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum TemplateType
	{
		[Description("SizeOrColor")]
		SizeOrColor = 1,
		[Description("Color")]
		Color = 2,
		[Description("Size")]
		Size = 3
		//[Description("SizeAndColor")]
		//SizeAndColor = 4
	}
}