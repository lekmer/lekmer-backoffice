﻿namespace Litium.Lekmer.CdonExport
{
	public enum CdonProductExposeStatus
	{
		BUYABLE,
		BOOKABLE,
		WATCHABLE
	}
}