﻿using System;
using System.Collections.Generic;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionItem : IBusinessObjectBase
	{
		int ProductRegistryId { get; set; }
		int ItemId { get; set; }
		string Reason { get; set; }
		int? UserId { get; set; }
		DateTime CreatedDate { get; set; }
		int ChannelId { get; set; }
		Dictionary<int,int> Channels { get; set; }
	}
}