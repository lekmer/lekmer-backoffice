namespace Litium.Lekmer.CdonExport
{
	public interface ICdonTag
	{
		int Id { get; set; }
		int CdonTagId { get; set; }
		string Value { get; set; }
	}
}