﻿using System;

namespace Litium.Lekmer.TwosellExport.Contract
{
	public interface IOrderInfo
	{
		DateTime Date { get; set; }
		int OrderId { get; set; }
		int ProductId { get; set; }
		string ErpId { get; set; }
		int Quantity { get; set; }
		decimal Price { get; set; }
		string CustomerIp { get; set; }
		string Email { get; set; }
		string LekmerId { get; set; }
	}
}