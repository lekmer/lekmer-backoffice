﻿using System.Collections.Generic;

namespace Litium.Lekmer.TwosellExport.Contract
{
	public interface ITwosellExportSetting
	{
		string DestinationDirectory { get; }

		string OrderInfoFileName { get; }
		string OrderInfoFileDelimiter { get; }
		int DaysAfterPurchase { get; }

		string ReplaceChannelNames { get; }
		bool GetChannelNameReplacementExists(string channelName);
		string GetChannelNameReplacement(string channelName);
		ICollection<string> ImageGroups { get; }
		ICollection<string> ImageSizes { get; }
		ICollection<string> ColorTagGroup { get; }
		ICollection<string> CategoryLevelUrl { get; }
		string ProductInfoFileName { get; }
		int PageSize { get; }
	}
}