﻿namespace Litium.Lekmer.TwosellExport.Contract
{
	public interface IExporter
	{
		void Execute();
	}
}