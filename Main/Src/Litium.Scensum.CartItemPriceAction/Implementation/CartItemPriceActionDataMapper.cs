using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Scensum.CartItemPriceAction.Mapper
{
	public class CartItemPriceActionDataMapper : CartActionDataMapper<ICartItemPriceAction>
	{
		public CartItemPriceActionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICartItemPriceAction Create()
		{
			ICartItemPriceAction action = base.Create();
			action.MaxQuantity = MapValue<int>("CartItemPriceAction.MaxQuantity");
			action.IncludeAllProducts = MapValue<bool>("CartItemPriceAction.IncludeAllProducts");
			return action;
		}
	}
}