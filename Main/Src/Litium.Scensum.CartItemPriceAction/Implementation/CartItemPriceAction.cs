using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;

namespace Litium.Scensum.CartItemPriceAction
{
	[Serializable]
	public class CartItemPriceAction : CartAction, ICartItemPriceAction
	{
		private int _maxQuantity;
		private bool _includeAllProducts;
		private ProductIdDictionary _includeProducts;
		private CategoryIdDictionary _includeCategories;
		private ProductIdDictionary _excludeProducts;
		private CategoryIdDictionary _excludeCategories;
		private LekmerCurrencyValueDictionary _amounts;

		public int MaxQuantity
		{
			get { return _maxQuantity; }
			set
			{
				CheckChanged(_maxQuantity, value);
				_maxQuantity = value;
			}
		}
		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}
		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}
		public CategoryIdDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}
		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}
		public CategoryIdDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}
		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			decimal campaignPrice;
			if (!Amounts.TryGetValue(context.Channel.Currency.Id, out campaignPrice))
			{
				return false;
			}

			//Need to define cart items that satisfy conditions and trigger the action, and then apply action to the next cart items.
			var campaign = GetCurrentCampaign(context);
			var resetCartContext = ResetUserContextCart(context);
			var singleCartItems = new Collection<ICartItem>();
			singleCartItems.SplitIntoSingleItems(cart.GetCartItems());

			// Check if empty cart (without items) satisfy conditions and trigger the action, and then apply action to the all cart items.
			if (campaign.ConditionsFulfilled(resetCartContext))
			{
				return PerformAction(resetCartContext.Cart, cart, singleCartItems, campaignPrice);
			}

			// Check what cart items satisfy conditions and trigger the action, and then apply action to the next cart items.
			for (var i = 0; i < singleCartItems.Count(); i++)
			{
				resetCartContext.Cart.AddItem(singleCartItems.ElementAt(i));
				if (campaign.ConditionsFulfilled(resetCartContext))
				{
					var itemsToApply = singleCartItems.Skip(i + 1);
					return PerformAction(resetCartContext.Cart, cart, itemsToApply, campaignPrice);
				}
			}

			return false;
		}

		protected virtual bool PerformAction(ICartFull resetCart, ICartFull cart, IEnumerable<ICartItem> itemsToApply, decimal campaignPrice)
		{
			var affectedItemsCount = 0;
			foreach (var item in itemsToApply)
			{
				if (affectedItemsCount < MaxQuantity && Verify(item, campaignPrice))
				{
					SetCampaignPrice(item, campaignPrice);
					affectedItemsCount++;
				}
				resetCart.AddItem(item);
			}

			cart.DeleteAllItems();
			foreach (var item in resetCart.GetCartItems())
			{
				cart.AddItem(item);
			}
			return affectedItemsCount > 0;
		}

		protected virtual ICartCampaign GetCurrentCampaign(IUserContext context)
		{
			return IoC.Resolve<ICartCampaignService>().GetById(context, CartCampaignId);
		}

		protected virtual IUserContext ResetUserContextCart(IUserContext context)
		{
			var resetCartContext = IoC.Resolve<IUserContext>();
			resetCartContext.Channel = ObjectCopier.Clone(context.Channel);
			resetCartContext.Customer = ObjectCopier.Clone(context.Customer);
			resetCartContext.Cart = ObjectCopier.Clone(context.Cart);
			resetCartContext.Cart.DeleteAllItems();
			return resetCartContext;
		}

		protected virtual bool Verify(ICartItem cartItem, decimal campaignPrice)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");
			if (IncludeCategories == null) throw new InvalidOperationException("The IncludeCategories property must be set.");
			if (ExcludeProducts == null) throw new InvalidOperationException("The ExcludeProducts property must be set.");
			if (ExcludeCategories == null) throw new InvalidOperationException("The ExcludeCategories property must be set.");

			return IsIncluded(cartItem) && !IsExcluded(cartItem) && cartItem.GetActualPrice().IncludingVat > campaignPrice;
		}

		protected virtual bool IsIncluded(ICartItem cartItem)
		{
			return
				IncludeAllProducts ||
				IncludeCategories.ContainsKey(cartItem.Product.CategoryId) ||
				IncludeProducts.ContainsKey(cartItem.Product.Id);
		}

		protected virtual bool IsExcluded(ICartItem cartItem)
		{
			return
				ExcludeCategories.ContainsKey(cartItem.Product.CategoryId) ||
				ExcludeProducts.ContainsKey(cartItem.Product.Id);
		}

		protected virtual void SetCampaignPrice(ICartItem item, decimal campaignPrice)
		{
			var priceIncludingVat = campaignPrice;
			var priceExcludingVat = priceIncludingVat / (1 + item.Product.Price.VatPercentage / 100);

			item.Product.CampaignInfo.Price = new Price(priceIncludingVat.Round(), priceExcludingVat.Round());
		}

		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, " +
													"since the Amounts list is null or empty.");
			}
			return new object[] {Amounts.ElementAt(0).Value, Amounts.ElementAt(0).Key};
		}
	}
}