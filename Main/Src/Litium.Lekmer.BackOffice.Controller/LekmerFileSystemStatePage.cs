﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;

namespace Litium.Lekmer.BackOffice.Controller
{
	/// <summary>
	/// Saves ViewState on disk. Performs ViewState file cleanup on application start.
	/// Extends <see cref="System.Web.UI.Page"/>.
	/// </summary>
	public class LekmerFileSystemStatePage : Page
	{
		public const string ViewStateCookiesKey = "ViewStateFileName";

		private static string _viewStateHiddenFieldName = "__ViewStateGuid";

		/// <summary>
		/// Saves the page state to persistence medium.
		/// </summary>
		/// <param name="state">State of the view.</param>
		protected override void SavePageStateToPersistenceMedium(object state)
		{
			string viewStateGuid;

			if (!Page.IsPostBack) // create a unique vs file
			{
				viewStateGuid = Guid.NewGuid().ToString();
			}
			else // get our vs key from the page, re use it
			{
				viewStateGuid = Request.Form[_viewStateHiddenFieldName];
				if (string.IsNullOrEmpty(viewStateGuid))
				{
					throw new ViewStateException();
				}
			}

			string viewStateGuidFilePath = GetViewStateFilePath(viewStateGuid);

			// Serialize ViewState.
			var los = new LosFormatter();
			var stringWriter = new StringWriter(CultureInfo.CurrentCulture);

			los.Serialize(stringWriter, state);

			// Save to disk.
			StreamWriter streamWriter = File.CreateText(viewStateGuidFilePath);
			streamWriter.Write(stringWriter.ToString());
			streamWriter.Close();

			ClientScript.RegisterHiddenField(_viewStateHiddenFieldName, viewStateGuid);
		}

		/// <summary>
		/// Loads any saved ViewState information to the <see cref="T:System.Web.UI.Page"/> object.
		/// </summary>
		/// <returns>The saved view state.</returns>
		protected override object LoadPageStateFromPersistenceMedium()
		{
			if (!Page.IsPostBack)
			{
				// We don't want to load up anything if this is an inital request
				return base.LoadPageStateFromPersistenceMedium();
			}

			string viewStateGuid = Request.Form[_viewStateHiddenFieldName];
			if (string.IsNullOrEmpty(viewStateGuid))
			{
				throw new ViewStateException();
			}

			string viewStateGuidFilePath = GetViewStateFilePath(viewStateGuid);
			if (!File.Exists(viewStateGuidFilePath))
			{
				throw new ViewStateException();
			}

			using (StreamReader streamReader = File.OpenText(viewStateGuidFilePath))
			{
				// Read file content.
				string viewStateString = streamReader.ReadToEnd();
				var los = new LosFormatter();

				// Deserialize and return.
				return los.Deserialize(viewStateString);
			}
		}

		/// <summary>
		/// Gets the viewstate file path.
		/// </summary>
		/// <value>The viewstate file path.</value>
		protected virtual string GetViewStateFilePath(string viewStateGuid)
		{
			string folderPath = ConfigurationManager.AppSettings["PersistedViewStateFolderPath"];

			string fileName = string.Concat(viewStateGuid, ".vs");

			return Path.Combine(folderPath, fileName);
		}

		/// <summary>
		/// Static contructor to perform cleanup of old ViewState files.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
		static LekmerFileSystemStatePage()
		{
			string folderPath = ConfigurationManager.AppSettings["PersistedViewStateFolderPath"];
			var dir = new DirectoryInfo(folderPath);
			FileInfo[] files = dir.GetFiles("*.vs");
			foreach (FileInfo file in files)
			{
				if (file.LastWriteTime < (DateTime.Now.AddMinutes(-60 * 24)))
				{
					file.Delete();
				}
			}
		}
	}
}