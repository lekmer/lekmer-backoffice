﻿using System.Linq;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.CommonItems;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BackOffice.Controller.CommonItems
{
	public static class LekmerPrivilegeHelper
	{
		public static bool CheckSecurity(Tab tab)
		{
			if (
				string.IsNullOrEmpty(tab.SecuredBy)
				&&
				(tab.Children.Count == 0 || tab.Children.Any(CheckSecurity))
				||
				IoC.Resolve<IAccessValidator>().HasAccess(SignInHelper.SignedInSystemUser, tab.SecuredBy)
				||
				IoC.Resolve<IAccessValidator>().HasAccess(SignInHelper.SignedInSystemUser, tab.SecuredBy + ".ReadOnly")
			)
			{
				return true;
			}

			return false;
		}
	}
}