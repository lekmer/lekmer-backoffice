﻿namespace Litium.Lekmer.BackOffice.Controller
{
	/// <summary>
	/// Abstract base page controller class for Scensum.BackOffice page controllers,
	/// with typed access to ViewState. Extends <see cref="LekmerPageController"/>.
	/// </summary>
	public abstract class LekmerStatePageController<T> : LekmerPageController
	{
		protected T State { get; set; }

		protected override object SaveViewState()
		{
			ViewState["PageState"] = State;
			return base.SaveViewState();
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			State = (T)ViewState["PageState"];
		}
	}
}