﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.TwosellExport.Service
{
	public class ProductInfoExportScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "TwosellExport"; }
		}

		protected override string GroupName
		{
			get { return "ProductInfoExportJob"; }
		}
	}
}