﻿using System.ComponentModel;

namespace Litium.Lekmer.TwosellExport.Service
{
	[RunInstaller(true)]
	public partial class TwosellExportServiceHostInstaller : System.Configuration.Install.Installer
	{
		public TwosellExportServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}
