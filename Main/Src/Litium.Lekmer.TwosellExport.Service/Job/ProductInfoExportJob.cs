﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwosellExport.Service
{
	public class ProductInfoExportJob : BaseJob
	{
		public override string Group
		{
			get { return "TwosellExport"; }
		}

		public override string Name
		{
			get { return "ProductInfoExportJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<IExporter>("ProductInfoExporter").Execute();
		}
	}
}
