﻿using System;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public interface IMaksuturvaPendingOrder
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		int OrderId { get; set; }
		int StatusId { get; set; }
		DateTime? FirstAttempt { get; set; }
		DateTime? LastAttempt { get; set; }
		DateTime? NextAttempt { get; set; }
	}
}