﻿using System;

namespace Litium.Lekmer.Payment.Maksuturva.Contract
{
	public interface IMaksuturvaSetting
	{
		string MaksuturvaTemporaryUrl { get; }
		string MaksuturvaPaymentUrl { get; }
		string MaksuturvaStatusQueryUrl { get; }
		string MaksuturvaCompensationUrl { get; }
		string UserId { get; }
		string SecretKey { get; }
		string KeyGeneration { get; }
		string OkReturnUrl { get; }
		string CancelReturnUrl { get; }
		string ErrorReturnUrl { get; }
		string DelayedReturnUrl { get; }
		string Escrow { get; }
		string PendingOrderCheckInterval { get; }
		string RejectedOrderCheckInterval { get; }
		string OnHoldOrderCheckInterval { get; }
		int DecisionGuaranteedInterval { get; }
		string DestinationDirectory { get; }
		string FileName { get; }
		int DaysAfterOrder { get; }
		string FtpUrl { get; }
		string Username { get; }
		string Password { get; }
		void Initialize(string channelCommonName);
		DateTime GetOrderCheckDate(string orderStatus, DateTime? firstCheck, DateTime currentTime);
	}
}