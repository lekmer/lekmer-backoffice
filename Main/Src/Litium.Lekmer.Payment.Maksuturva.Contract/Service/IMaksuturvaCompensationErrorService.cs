﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public interface IMaksuturvaCompensationErrorService
	{
		IMaksuturvaCompensationError Create(string channelCommonName, DateTime fetchDate);
		void Insert(IMaksuturvaCompensationError maksuturvaCompensationError);
		void Update(IMaksuturvaCompensationError maksuturvaCompensationError);
		void Delete(int maksuturvaCompensationErrorId);
		Collection<IMaksuturvaCompensationError> Get();
	}
}