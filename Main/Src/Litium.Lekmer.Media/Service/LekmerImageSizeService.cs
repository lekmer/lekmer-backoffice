﻿using System;
using Litium.Lekmer.Media.Cache;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
	public class LekmerImageSizeService : ImageSizeService, ILekmerImageSizeService
	{
		public LekmerImageSizeService(ImageSizeRepository repository)
			: base(repository)
		{
		}

		/// <summary>
		///  Retrieves an <see cref="IImageSize"/> by its common name.
		/// </summary>
		/// <param name="commonName">The common name of the <see cref="IImageSize"/> to be retrieved.</param>
		/// <param name="mobileVersion">When need mobile version of image, send true.</param>
		/// <returns>The <see cref="IImageSize"/> item that was requested, or null if none was found.</returns>
		/// <exception cref="ArgumentException">The requested common name was null or empty.</exception>
		public virtual IImageSize GetByCommonName(string commonName, bool mobileVersion)
		{
			if (string.IsNullOrEmpty(commonName))
			{
				throw new ArgumentException("The requested common name was null or empty.");
			}

			return LekmerImageSizeCache.Instance.TryGetItem(
				new LekmerImageSizeKey(commonName, mobileVersion),
				() => GetByCommonNameCore(commonName, mobileVersion));
		}

		protected virtual IImageSize GetByCommonNameCore(string commonName, bool mobileVersion)
		{
			var imageSize = (ILekmerImageSize)Repository.GetByCommonName(commonName);

			if (mobileVersion)
			{
				if (imageSize.MobileWidth.HasValue)
				{
					imageSize.Width = imageSize.MobileWidth.Value;
				}

				if (imageSize.MobileHeight.HasValue)
				{
					imageSize.Height = imageSize.MobileHeight.Value;
				}

				if (imageSize.MobileQuality.HasValue)
				{
					imageSize.Quality = imageSize.MobileQuality.Value;
				}
			}

			return imageSize;
		}
	}
}