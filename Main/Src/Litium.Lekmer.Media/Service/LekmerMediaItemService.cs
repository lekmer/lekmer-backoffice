﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
	public class LekmerMediaItemService : MediaItemService, ILekmerMediaItemService
	{
		protected LekmerMediaItemRepository LekmerMediaItemRepository { get; private set; }

		public LekmerMediaItemService(
			MediaItemRepository repository,
			IMediaItemSharedService mediaItemSharedService,
			IMediaFormatService mediaFormatService)
			: base(repository, mediaItemSharedService, mediaFormatService)
		{
			LekmerMediaItemRepository = (LekmerMediaItemRepository) repository;
		}

		public virtual Collection<IMediaItem> GetAllBySizeTable(IUserContext userContext, int sizeTableId)
		{
			if (sizeTableId <= 0)
			{
				throw new ArgumentOutOfRangeException("sizeTableId");
			}

			return LekmerMediaItemRepository.GetAllBySizeTable(userContext.Channel.Id, sizeTableId);
		}
	}
}