﻿using System.IO;
using Litium.Framework.Image;
using Litium.Scensum.Media;
using SysImage = System.Drawing.Image;

namespace Litium.Lekmer.Media
{
	public class LekmerImageSharedService : ImageSharedService
	{
		/// <summary>
		/// Initializes a new <see cref="T:Litium.Lekmer.Media.LekmerImageSharedService"/>. 
		/// </summary>
		/// <param name="mediaItemSharedService">The <see cref="T:Litium.Scensum.Media.IMediaItemSharedService"/>.</param>
		public LekmerImageSharedService(IMediaItemSharedService mediaItemSharedService)
			: base(mediaItemSharedService)
		{
		}

		/// <summary>
		/// Resizes an image and returns it as a <see cref="T:System.IO.Stream"/>.
		/// </summary>
		/// <param name="id">Identifier of the media item.</param><param name="mediaFormat">Format of the media item.</param><param name="width">Specifies width of the image returned.</param><param name="height">Specifies height of the image returned.</param><param name="quality">Specifies quality of the image returned.</param>
		/// <returns>
		/// A resized image as a <see cref="T:System.IO.Stream"/>.
		/// </returns>
		protected override Stream ResizeImage(int id, IMediaFormat mediaFormat, int width, int height, int quality)
		{
			using (Stream stream = MediaItemSharedService.LoadMedia(id, mediaFormat))
			{
				if (stream == null)
				{
					return null;
				}

				SysImage image = ImageUtil.GetImage(stream);

				if (!IsNeedToResize(image, width, height, quality))
				{
					return ImageUtil.CopyImageToMemoryStream(stream);
				}

				ImageFormat format = ImageFormat.Unknown;
				using (ImageManager imageManager = new ImageManager(stream))
				{
					switch (mediaFormat.Extension)
					{
						case "jpg":
						case "jpeg":
							format = ImageFormat.Jpeg;
							break;
						case "gif":
							format = ImageFormat.Gif;
							break;
						case "bmp":
							format = ImageFormat.Bmp;
							break;
						case "png":
							format = ImageFormat.Png;
							break;
						case "tiff":
						case "tif":
							format = ImageFormat.Tiff;
							break;
					}
					return imageManager.ResizeTo(new Size(width, height), quality, format);
				}
			}
		}

		protected virtual bool IsNeedToResize(SysImage image, int width, int height, int quality)
		{
			if (image.Height == height && image.Width == width && quality == 100)
			{
				return false;
			}

			// 958x400 image; resize to 960x400 x 100%; result - no resizing, better to use original image.
			if (quality == 100)
			{
				var originSize = new Size(image.Width, image.Height);
				var targetSize = new Size(width, height);

				Size newSize = originSize.ProportionalResize(targetSize);

				if (image.Height == newSize.Height && image.Width == newSize.Width)
				{
					return false;
				}
			}

			return true;
		}
	}
}