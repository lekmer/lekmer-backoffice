﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Media.Mapper
{
	public class LekmerMovieDataMapper :   DataMapperBase<ILekmerMovie>
	{
		private DataMapperBase<IImage> _lekmerImageDataMapper;
		private DataMapperBase<IProductMedia> _productMediaDataMapper;
		public LekmerMovieDataMapper(IDataReader dataReader) : base(dataReader)
		{
			Initialize();
			_lekmerImageDataMapper = DataMapperResolver.Resolve<IImage>(DataReader);
			_productMediaDataMapper = DataMapperResolver.Resolve<IProductMedia>(DataReader);
		}

		protected override ILekmerMovie Create()
		{
			var movie = IoC.Resolve<ILekmerMovie>();
			movie.LekmerImage.Link = MapNullableValue<string>("LekmerImage.Link");
			movie.LekmerImage.Parameter = MapNullableValue<string>("LekmerImage.Parameter");

			movie.LekmerImage.TumbnailImageUrl = MapNullableValue<string>("LekmerImage.TumbnailImageUrl");
			movie.LekmerImage.HasImage = MapNullableValue<bool>("LekmerImage.HasImage");

			movie.LekmerImage.SetUntouched();
		
			movie.LekmerImage = (ILekmerImage)_lekmerImageDataMapper.MapRow();
			movie.ProductMedia= _productMediaDataMapper.MapRow();
			
			return movie;
		}
	}
}
