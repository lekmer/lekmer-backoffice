﻿using System;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	[Serializable]
	public class LekmerImageSize : ImageSize, ILekmerImageSize
	{
		private int? _mobileWidth;
		private int? _mobileHeight;
		private int? _mobileQuality;

		public int? MobileWidth
		{
			get { return _mobileWidth; }
			set
			{
				CheckChanged(_mobileWidth, value);
				_mobileWidth = value;
			}
		}

		public int? MobileHeight
		{
			get { return _mobileHeight; }
			set
			{
				CheckChanged(_mobileHeight, value);
				_mobileHeight = value;
			}
		}

		public int? MobileQuality
		{
			get { return _mobileQuality; }
			set
			{
				CheckChanged(_mobileQuality, value);
				_mobileQuality = value;
			}
		}
	}
}
