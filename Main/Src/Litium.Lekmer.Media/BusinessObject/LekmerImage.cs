﻿using System;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	[Serializable]
	public class LekmerImage : Image, ILekmerImage
	{
		private string _link;
		private string _parameter;
		private string _tumbnailImageUrl;
		private bool _hasImage;

		public string Link
		{
			get { return _link; }
			set
			{
				CheckChanged(_link, value);
				_link = value;
			}
		}

		public string Parameter
		{
			get { return _parameter; }
			set
			{
				CheckChanged(_parameter, value);
				_parameter = value;
			}
		}

		public string TumbnailImageUrl
		{
			get { return _tumbnailImageUrl; }
			set
			{
				CheckChanged(_tumbnailImageUrl, value);
				_tumbnailImageUrl = value;
			}
		}

		public bool IsLink
		{
			get { return !string.IsNullOrEmpty(Link); }

		}

		public bool HasImage
		{
			get { return _hasImage; }
			set
			{
				CheckChanged(_hasImage, value);
				_hasImage = value;
			}
		}
	}
}
