﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Media
{
	[Serializable]
	public class LekmerMovie :BusinessObjectBase, ILekmerMovie
	{
		private ILekmerImage _lekmerImage;
		private IProductMedia _productMedia;

		public ILekmerImage LekmerImage
		{
			get { return _lekmerImage; }
			set
			{
				CheckChanged(_lekmerImage, value);
				_lekmerImage = value;
			}
		}

		public IProductMedia ProductMedia
		{
			get { return _productMedia; }
			set
			{
				CheckChanged(_productMedia, value);
				_productMedia = value;
			}
		}


	}
}
