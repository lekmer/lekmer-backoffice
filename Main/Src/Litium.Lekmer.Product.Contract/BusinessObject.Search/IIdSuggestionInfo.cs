namespace Litium.Lekmer.Product
{
	public interface IIdSuggestionInfo
	{
		int Id { get; set; }
		string Ticket { get; set; }
	}
}