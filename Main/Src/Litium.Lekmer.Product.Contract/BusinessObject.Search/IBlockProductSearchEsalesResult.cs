﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductSearchEsalesResult : IBlock
	{
		int? SecondaryTemplateId { get; set; }
	}
}