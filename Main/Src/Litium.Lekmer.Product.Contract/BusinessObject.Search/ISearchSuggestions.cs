using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ISearchSuggestions
	{
		ProductCollection Products { get; set; }

		Collection<ICategoryTagSearchSuggestion> CategoryTags { get; set; }

		Collection<IBrandSuggestionInfo> Brands { get; set; }

		Collection<ISearchSuggestionInfo> Suggestions { get; set; }

		Collection<ISearchSuggestionInfo> Corrections { get; set; }

		Collection<ISearchSuggestionInfo> TopSearches { get; set; }

		ISearchFacet SearchFacetWithFilter { get; set; }
		ISearchFacet SearchFacetWithoutFilter { get; set; }
	}
}