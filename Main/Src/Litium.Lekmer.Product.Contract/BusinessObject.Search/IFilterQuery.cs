using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IFilterQuery
	{
		Collection<int> BrandIds { get; }

		Collection<int> Level1CategoryIds { get; }

		Collection<int> Level2CategoryIds { get; }

		Collection<int> Level3CategoryIds { get; }

		Collection<IEnumerable<int>> GroupedTagIds { get; }

		IAgeIntervalBand AgeInterval { get; set; }

		IPriceIntervalBand PriceInterval { get; set; }

		Collection<int> ProductIds { get; }

		SortOption SortOption { get; set; }

		Collection<int> SizeIds { get; }

		Collection<int> ProductTypeIds { get; }
	}
}