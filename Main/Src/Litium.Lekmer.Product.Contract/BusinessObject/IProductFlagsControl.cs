﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign;

namespace Litium.Lekmer.Product
{
	public interface IProductFlagsControl
	{
		/// <summary>
		/// Render the component's body.
		/// </summary>
		string Render(Collection<IFlag> flags, int? templateId = null);

		/// <summary>
		/// Render the component's body.
		/// </summary>
		string RenderFromIncludes(Collection<IFlag> flags);
	}
}
