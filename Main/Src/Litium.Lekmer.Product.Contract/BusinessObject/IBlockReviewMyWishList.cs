﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockReviewMyWishList : IBlock
	{
		IBlockSetting Setting { get; set; }
	}
}