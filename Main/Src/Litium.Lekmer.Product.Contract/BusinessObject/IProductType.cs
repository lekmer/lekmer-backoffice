﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductType : IBusinessObjectBase
	{
		int Id { get; set; }
		string Title { get; set; }
		string CommonName { get; set; }
	}
}