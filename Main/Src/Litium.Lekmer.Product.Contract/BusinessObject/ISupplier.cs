﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISupplier : IBusinessObjectBase
	{
		int Id { get; set; }
		string SupplierNo { get; set; }
		string ActivePassiveCode { get; set; }
		string Name { get; set; }
		string Address { get; set; }
		string ZipArea { get; set; }
		string City { get; set; }
		string CountryIso { get; set; }
		string LanguageIso { get; set; }
		string VatRate { get; set; }
		string Phone1 { get; set; }
		string Phone2 { get; set; }
		string Email { get; set; }
		bool IsDropShip { get; set; }
		string DropShipEmail { get; set; }
	}
}