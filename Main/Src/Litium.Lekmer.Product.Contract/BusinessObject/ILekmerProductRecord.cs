﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProductRecord : ILekmerProduct, IProductRecord
	{
		int AgeFromMonth { get; set; }
		int AgeToMonth { get; set; }
		string Measurement { get; set; }
		int? BatteryTypeId { get; set; }
		int? NumberOfBatteries { get; set; }
		bool IsBatteryIncluded { get; set; }
		Collection<IIcon> Icons { get; set; }
		DateTime? ExpectedBackInStock { get; set; }
		int? SizeDeviationId { get; set; }
		string SupplierId { get; set; }
		decimal? PurchasePrice { get; set; }
		int? PurchaseCurrencyId { get; set; }
		decimal? AveragePrice { get; set; }
	}
}