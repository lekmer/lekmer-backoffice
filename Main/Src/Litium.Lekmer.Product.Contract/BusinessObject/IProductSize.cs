﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductSize : IBusinessObjectBase
	{
		int ProductId { get; set; }
		string ErpId { get; set; }
		int NumberInStock { get; set; }
		int? MillimeterDeviation { get; set; }
		decimal? OverrideEu { get; set; }
		int? OverrideMillimeter { get; set; }
		bool IsBuyable { get; }
		decimal? Weight { get; set; }
		int StockStatusId { get; set; }
		ISize SizeInfo { get; set; }
	}
}
