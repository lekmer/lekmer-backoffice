﻿using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductList : ILekmerBlock
	{
		IBlockSetting Setting { get; set; }
	}
}