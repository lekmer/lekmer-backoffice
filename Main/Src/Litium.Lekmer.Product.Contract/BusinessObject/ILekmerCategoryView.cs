﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerCategoryView : ILekmerCategory, ICategoryView
	{ 
	}
}