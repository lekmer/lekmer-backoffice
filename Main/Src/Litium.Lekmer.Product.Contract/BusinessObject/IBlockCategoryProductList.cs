﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockCategoryProductList : IBlock
	{
		int ProductSortOrderId { get; set; }
		IProductSortOrder ProductSortOrder { get; set; }
		IBlockSetting Setting { get; set; }
	}
}
