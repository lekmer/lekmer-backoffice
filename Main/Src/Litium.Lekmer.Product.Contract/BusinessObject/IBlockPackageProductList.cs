﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockPackageProductList : IBlock
	{
		IBlockSetting Setting { get; set; }
	}
}