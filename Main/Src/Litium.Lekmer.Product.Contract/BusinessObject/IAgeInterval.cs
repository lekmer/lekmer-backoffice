using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IAgeInterval : IBusinessObjectBase
	{
		int Id { get; set; }
		string Title { get; set; }
		int FromMonth { get; set; }
		int ToMonth { get; set; }
		int Ordinal { get; set; }
		int? ContentNodeId { get; set; }
	}
}