using Litium.Scensum.Foundation;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IPriceInterval : IBusinessObjectBase
	{
		int Id { get; set; }
		decimal From { get; set; }
		decimal To { get; set; }
		string Title { get; set; }
		ICurrency Currency { get; set; }
		int? ContentNodeId { get; set; }
	}
}