﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableCategoryBrandPair : IBusinessObjectBase
	{
		string Id { get; set; }
		int CategoryId { get; set; }
		int? BrandId { get; set; }

		ICategory Category { get; set; }
		IBrand Brand { get; set; }
	}
}