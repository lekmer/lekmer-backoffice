﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProductIndexItem : IProductIndexItem
	{
		string LekmerErpId { get; set; }
	}
}
