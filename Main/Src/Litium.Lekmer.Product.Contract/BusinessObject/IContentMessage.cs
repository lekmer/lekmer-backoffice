﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IContentMessage : IBusinessObjectBase, IIncludeItems, IEntityTimeLimit
	{
		int Id { get; set; }
		int ContentMessageFolderId { get; set; }
		string CommonName { get; set; }
		string Message { get; set; }
		bool IsDropShip { get; set; }
	}
}