﻿using Litium.Lekmer.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IContentMessageFolder : IBusinessObjectBase, IFolder
	{
		new int Id { get; set; }
		new int? ParentFolderId { get; set; }
		new string Title { get; set; }
	}
}