﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IWishListItem : IBusinessObjectBase
	{
		int Id { get; set; }
		int WishListId { get; set; }
		int ProductId { get; set; }
		int? SizeId { get; set; }
		int Ordinal { get; set; }
		Collection<IWishListPackageItem> PackageItems { get; set; }
		int? GetPackageItemsHashCode();
	}
}