﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IStockRange : IBusinessObjectBase
	{
		int StockRangeId { get; set; }
		string CommonName { get; set; }
		int? StartValue { get; set; }
		int? EndValue { get; set; }
	}
}