namespace Litium.Lekmer.Product
{
	public interface IBrandSiteStructureProductRegistryWrapper : IBrandSiteStructureRegistry, IProductRegistry
	{
		int ContentNodeStatusId { get; set; }
	}
}