﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    public interface IBatteryType : IBusinessObjectBase
    {
        int Id { get; set; }
        string Title { get; set; }
    }
}
