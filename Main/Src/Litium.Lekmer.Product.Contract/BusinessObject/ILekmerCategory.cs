﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerCategory : ICategory
	{
		bool? AllowMultipleSizesPurchase { get; set; }
		string PackageInfo { get; set; }

		/// <summary>
		/// The minimum number in stock value for sending monitor product mails
		/// </summary>
		int? MonitorThreshold { get; set; }

		/// <summary>
		/// The maximum quantity of product allowed in a single order
		/// </summary>
		int? MaxQuantityPerOrder { get; set; }

		/// <summary>
		/// will only be shown as in stock in the store if there are equval or more in actual stock
		/// </summary>
		int? MinQuantityInStock { get; set; }

		int? DeliveryTimeId { get; set; }
		IDeliveryTime DeliveryTime { get; set; }
	}
}