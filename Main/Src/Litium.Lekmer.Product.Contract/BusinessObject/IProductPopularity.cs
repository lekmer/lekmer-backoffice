using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductPopularity : IBusinessObjectBase
	{
		int ChannelId { get; set; }
		int ProductId { get; set; }
		int Popularity { get; set; }
		int SalesAmount { get; set; }
	}
}