﻿using System;

namespace Litium.Lekmer.Product
{
	public interface IEntityTimeLimit
	{
		DateTime? StartDate { get; set; }
		DateTime? EndDate { get; set; }
		TimeSpan? StartDailyInterval { get; set; }
		TimeSpan? EndDailyInterval { get; set; }
		bool IsInTimeLimit { get; }
	}
}