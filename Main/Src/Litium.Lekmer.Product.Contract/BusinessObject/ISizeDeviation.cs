﻿namespace Litium.Lekmer.Product
{
	public interface ISizeDeviation
	{
		int Id { get; set; }
		string Title { get; set; }
		string CommonName { get; set; }
	}
}
