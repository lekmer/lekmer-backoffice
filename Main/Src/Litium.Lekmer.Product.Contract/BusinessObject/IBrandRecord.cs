using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IBrandRecord : IBrand
	{
		Collection<IBrandSiteStructureRegistry> BrandSiteStructureRegistries { get; set; }
	}
}