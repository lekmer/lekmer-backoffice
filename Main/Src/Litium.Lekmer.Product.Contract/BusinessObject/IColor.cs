namespace Litium.Lekmer.Product
{
	public interface IColor
	{
		int Id { get; set; }
		string HyErpId { get; set; }
		string Value { get; set; }
		string CommonName { get; set; }
	}
}