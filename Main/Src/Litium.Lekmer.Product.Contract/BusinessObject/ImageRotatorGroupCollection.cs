﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ImageRotatorGroupCollection : Collection<IImageRotatorGroup>
	{
		public ImageRotatorGroupCollection()
		{
		}

		public ImageRotatorGroupCollection(IList<IImageRotatorGroup> list)
			: base(list)
		{
		}

		public ImageRotatorGroupCollection(IEnumerable<IImageRotatorGroup> list)
			: base(new List<IImageRotatorGroup>(list))
		{
		}

		public int TotalCount { get; set; }
	}
}