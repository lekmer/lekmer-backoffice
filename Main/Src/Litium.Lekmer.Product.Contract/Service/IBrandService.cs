﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBrandService
	{
		IBrand Create();
		IBrand GetById(IUserContext context, int brandId);
		BrandCollection GetAll(IUserContext context);
		BrandCollection GetAllByBlock(IUserContext context, IBlockBrandList block, int page, int pageSize);
		BrandCollection GetAllByBlockBrandProductList(IUserContext context, IBlockBrandProductList block);
		BrandCollection PopulateBrands(IUserContext context, BrandIdCollection brandIds);
		int? GetIdByTitle(IUserContext context, string title);
		IBrandSiteStructureRegistry GetByNode(int nodeId);
	}
}