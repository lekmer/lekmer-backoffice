﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandProductListService
	{
		IBlockBrandProductList GetById(IUserContext context, int blockId);
	}
}