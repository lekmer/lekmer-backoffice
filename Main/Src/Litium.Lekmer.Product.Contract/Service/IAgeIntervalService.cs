using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IAgeIntervalService
	{
		IAgeIntervalBand GetAgeInterval(int? fromMonth, int? toMonth);
		IAgeIntervalBand GetAgeInterval(IUserContext context, int ageIntervalId);
		IAgeIntervalBand GetAgeIntervalOuter(Collection<IAgeInterval> ageIntervals);
		Collection<IAgeInterval> GetAll(IUserContext context);
		IEnumerable<IAgeInterval> GetAllMatching(IUserContext context, int ageFromMonth, int ageToMonth);
	}
}