﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockImageRotatorService
	{
		IBlockImageRotator GetById(IUserContext context, int id);
	}
}
