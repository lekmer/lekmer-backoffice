﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockLatestProductAddedToBasketService
	{
		IBlockLatestProductAddedToBasket GetById(IUserContext context, int id);
	}
}

