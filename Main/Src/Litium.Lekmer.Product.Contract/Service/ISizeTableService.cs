using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableService
	{
		ISizeTable GetByProduct(IUserContext userContext, int productId);
	}
}