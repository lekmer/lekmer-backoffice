using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ITagGroupService
	{
		Collection<ITagGroup> GetAll(IUserContext context);
		Dictionary<string, string> InfoPointTagGroupGetAll();
		Collection<ITagGroup> GetAllWithoutTags(IUserContext context);
		Collection<ITagGroup> GetAllByProduct(IUserContext context, int productId);

		ITagGroup Clone(ITagGroup sourceTagGroup);
		Collection<ITagGroup> Clone(Collection<ITagGroup> sourceTagGroups);
	}
}