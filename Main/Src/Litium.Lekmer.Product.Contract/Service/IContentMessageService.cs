using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IContentMessageService
	{
		Collection<IContentMessage> GetAllByProduct(IUserContext userContext, int productId);
	}
}