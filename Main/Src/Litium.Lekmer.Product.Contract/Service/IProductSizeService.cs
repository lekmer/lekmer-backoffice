﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductSizeService
	{
		IProductSize GetById(int productId, int sizeId);
		Collection<IProductSize> GetAllByProduct(int productId);
		Collection<IProductSizeRelation> GetAll();
		Collection<IProductSize> GetAllByProductIdList(IEnumerable<int> productIds);
	}
}