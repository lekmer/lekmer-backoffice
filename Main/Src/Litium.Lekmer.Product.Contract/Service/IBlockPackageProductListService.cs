using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockPackageProductListService
	{
		IBlockPackageProductList GetById(IUserContext context, int id);
	}
}