﻿using System;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IWishListService
	{
		IWishList Create();
		IWishList GetByKey(IUserContext context, Guid key);
		void Save(IUserContext context, IWishList wishList);
		void Delete(IUserContext context, IWishList wishList);
		void SendWishList(IUserContext context, WishListMessageArgs wishListMessageArgs);
	}
}