using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeService
	{
		Collection<ISize> GetAll(IUserContext userContext);
	}
}