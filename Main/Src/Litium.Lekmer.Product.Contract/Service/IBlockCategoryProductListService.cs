﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockCategoryProductListService
	{
		IBlockCategoryProductList GetById(IUserContext context, int blockId);
	}
}
