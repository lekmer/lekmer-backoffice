﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandListService
	{
		IBlockBrandList GetById(IUserContext context, int id);
	}
}