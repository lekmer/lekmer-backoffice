﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IMonitorProductService
	{
		IMonitorProduct Create(IUserContext context, int productId, string email, int? sizeId);
		int Save(IUserContext context, IMonitorProduct monitorProduct);
		void Monitor(int numberOfProductsInPortion, int breakDurationInSeconds, int minNumberInStock);
		void Delete(IUserContext context, string email);
	}
}
