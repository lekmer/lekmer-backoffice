using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductColorService
	{
		Collection<IProductColor> GetAll();
		Collection<IProductColor> GetAllByProductErpId(string erpId);
	}
}