﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandProductListBrandService
	{
		Collection<IBlockBrandProductListBrand> GetAllByBlock(IUserContext context, int blockId);
	}
}