﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockSharedWishListService
	{
		IBlockSharedWishList GetById(IUserContext context, int id);
	}
}

