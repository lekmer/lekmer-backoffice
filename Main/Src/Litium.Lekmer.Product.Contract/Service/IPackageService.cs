using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IPackageService
	{
		int GetNumberInStockForPackageWithSizes(string itemsWithSizesXml, string itemsWithoutSizes, char delimiter);
		IPackage GetByMasterProduct(IUserContext context, int masterProductId);
		ProductIdCollection GetMasterProductIdsByPackageProduct(IUserContext context, int productId);
	}
}