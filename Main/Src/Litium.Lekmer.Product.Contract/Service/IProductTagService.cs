using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductTagService
	{
		Collection<IProductTag> GetAll();
	}
}