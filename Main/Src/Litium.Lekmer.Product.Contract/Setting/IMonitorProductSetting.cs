namespace Litium.Lekmer.Product.Setting
{
	public interface IMonitorProductSetting
	{
		int BreakDuration { get; }
		int PortionSize { get; }
		int MinNumberInStockDefault { get; }
	}
}