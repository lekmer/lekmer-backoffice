﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.MessageArgs
{
    public class WishListMessageArgs
    {
        public ICollection<string> Receivers = new Collection<string>();
        public string Senders { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string WishListPageUrl  { get; set; }
        public string WishListKey { get; set; }
        public IChannel Channel { get; set; }
        public bool MailToMysef { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
    }
}
