﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IAgeIntervalSecureService
	{
		Collection<IAgeInterval> GetAll();
	}
}
