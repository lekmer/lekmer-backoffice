﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableRowSecureService
	{
		ISizeTableRow Save(ISystemUserFull systemUserFull, ISizeTableRow sizeTableRow, Collection<ISizeTableRowTranslation> translations);

		void Delete(ISystemUserFull systemUserFull, int sizeTableId);

		Collection<ISizeTableRow> GetAllBySizeTable(int sizeTableId);

		// Translations.

		Collection<ISizeTableRowTranslation> GetAllTranslations(int sizeTableRowId);
	}
}