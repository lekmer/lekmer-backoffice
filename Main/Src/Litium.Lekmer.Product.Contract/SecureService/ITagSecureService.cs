﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ITagSecureService
	{
		ITag GetById(int id);
		Collection<ITag> GetAll();
		Collection<ITag> GetAllByTagGroup(int tagGroupId);
		Collection<ITag> GetAllByProductAndTagGroup(int productId, int tagGroupId);
		Collection<ITranslationGeneric> GetAllTranslationsByTag(int tagId);

		ITag Create(int groupId, string value, string commonName);
		int Save(ISystemUserFull systemUserFull, ITag tag, IEnumerable<ITranslationGeneric> translations);
		void Delete(ISystemUserFull systemUserFull, int tagId);

		/// <summary>
		/// Add new tag to collection, keeping the old tags
		/// </summary>
		void AddAutoCampaignTag(ISystemUserFull systemUserFull, int productId, int tagId);

		void UpdateProductTags(int productId, Collection<int> tagsToInsert, Collection<int> tagsToDelete, int objectId);


		// Tag-Flag
		void TagFlagSave(ISystemUserFull systemUserFull, int tagId, int flagId);
		void TagFlagDeleteByTag(ISystemUserFull systemUserFull, int tagId);
	}
}