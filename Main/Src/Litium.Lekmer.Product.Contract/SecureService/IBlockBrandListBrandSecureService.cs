﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandListBrandSecureService
	{
		Collection<IBlockBrandListBrand> GetAllByBlock(int blockId);
		void Save(int blockId, Collection<IBlockBrandListBrand> blockBrands);
	}
}