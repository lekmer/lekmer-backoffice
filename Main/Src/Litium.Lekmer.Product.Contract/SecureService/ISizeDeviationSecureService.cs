﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface ISizeDeviationSecureService
	{
		Collection<ISizeDeviation> GetAll();
	}
}
