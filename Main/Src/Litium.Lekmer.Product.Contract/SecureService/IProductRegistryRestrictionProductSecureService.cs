﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistryRestrictionProductSecureService
	{
		IProductRegistryRestrictionItem Create();

		void Save(ISystemUserFull systemUserFull, int productId, Collection<IProductRegistryRestrictionItem> productRegistryRestrictionProductList);

		void Delete(ISystemUserFull systemUserFull, int productId);

		Collection<IProductRegistryRestrictionItem> GetAllByProduct(int productId);

		Collection<IProductRegistryRestrictionItem> GetAllWithChannel();
	}
}