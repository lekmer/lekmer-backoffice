﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistryRestrictionCategorySecureService
	{
		IProductRegistryRestrictionItem Create();

		void Save(ISystemUserFull systemUserFull, int categoryId, Collection<IProductRegistryRestrictionItem> productRegistryRestrictionCategoryList);

		void Delete(ISystemUserFull systemUserFull, int categoryId);

		Collection<IProductRegistryRestrictionItem> GetAllByCategory(int categoryId);

		Collection<IProductRegistryRestrictionItem> GetAllWithChannel();
	}
}