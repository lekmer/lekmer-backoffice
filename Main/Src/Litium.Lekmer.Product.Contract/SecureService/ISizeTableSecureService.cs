﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableSecureService
	{
		ISizeTable Create();

		ISizeTable Save(ISystemUserFull systemUserFull, ISizeTable sizeTable, Collection<ISizeTableTranslation> translations, Collection<ISizeTableMediaRecord> mediaItems);

		void Delete(ISystemUserFull systemUserFull, int sizeTableId);

		ISizeTable GetById(int id);

		Collection<ISizeTable> GetAllByFolder(int folderId);

		Collection<ISizeTable> GetAll();

		Collection<ISizeTable> Search(string searchCriteria);

		// Includes.

		ISizeTable GetAllIncludes(ISizeTable sizeTable);

		void SaveIncludes(int sizeTableId, IEnumerable<int> productIds, Collection<ISizeTableCategoryBrandPair> categoryBrandPairs);

		// Translations.

		Collection<ISizeTableTranslation> GetAllTranslations(int sizeTableId);
	}
}