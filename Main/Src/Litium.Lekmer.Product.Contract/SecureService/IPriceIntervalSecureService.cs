﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IPriceIntervalSecureService
	{
		Collection<IPriceInterval> GetAll();
	}
}
