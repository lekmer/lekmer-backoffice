﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductListSecureService
	{
		IBlockProductList Create();
		int Save(ISystemUserFull systemUserFull, IBlockProductList block, Collection<IBlockProductListProduct> products);
		IBlockProductList GetById(int id);
	}
}
