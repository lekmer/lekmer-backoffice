﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockSharedWishListSecureService
	{
		IBlockSharedWishList Create();
		IBlockSharedWishList GetById(int id);
		int Save(ISystemUserFull systemUserFull, IBlockSharedWishList block);
		void Delete(ISystemUserFull systemUserFull, int blockId);
	}
}
