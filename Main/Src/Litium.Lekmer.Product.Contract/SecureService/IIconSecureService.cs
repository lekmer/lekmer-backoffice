﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IIconSecureService
	{
		IIcon Save(ISystemUserFull systemUserFull, IIcon icon, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations);
		void Delete(ISystemUserFull systemUserFull, int iconId);
		IIcon GetByIdSecure(int iconId);
		Collection<IIcon> GetAll();
		IconCollection GetAll(int page, int pageSize, string sortBy, bool? sortByDescending);

		// Translations.
		void SaveTranslations(ISystemUserFull systemUserFull, int iconId, int languageId, string title, string description);
		Collection<ITranslationGeneric> GetAllTitleTranslations(int iconId);
		Collection<ITranslationGeneric> GetAllDescriptionTranslations(int iconId);

		// Product Icons.
		void SaveProductIcon(ISystemUserFull systemUserFull, int productId, IEnumerable<int> iconsId);
		Collection<IIcon> GetAllByProduct(int productId, int categoryId, int? brandId);
		Collection<IIcon> GetAllByCategoryAndBrand(int categoryId, int? brandId);

		// Brand Icons.
		void SaveBrandIcon(ISystemUserFull systemUserFull, int brandId, IEnumerable<int> iconsId);
		Collection<IIcon> GetAllByBrand(int brandId);

		// Category Icons.
		void SaveCategoryIcon(ISystemUserFull systemUserFull, int categoryId, IEnumerable<int> iconsId);
		Collection<IIcon> GetAllByCategory(int categoryId);
	}
}