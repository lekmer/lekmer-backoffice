﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IRecommendedPriceSecureService
	{
		Collection<IRecommendedPrice> GetAllByProduct(int productId);
		void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<IRecommendedPrice> recommendedPrices);
	}
}