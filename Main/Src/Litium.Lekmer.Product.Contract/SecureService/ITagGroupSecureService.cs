﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface ITagGroupSecureService
	{
		ITagGroup GetById(int id);
		Collection<ITagGroup> GetAll();
		Collection<ITagGroup> GetAllWithoutTags();
	}
}