﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IPackageSecureService
	{
		void SavePackageProducts(ISystemUserFull systemUserFull, int packageId, IEnumerable<int> productIds);

		void DeletePackageProducts(ISystemUserFull systemUserFull, int packageId);


		IPackage Create();

		IPackage Save(ISystemUserFull systemUserFull,
			IPackage package,
			Collection<IPriceListItem> priceListItems,
			Collection<ITranslationGeneric> webShopTitleTranslations,
			Collection<ITranslationGeneric> descriptionTranslations,
			IEnumerable<ITranslationGeneric> generalInfoTranslations,
			Collection<IProductRegistryRestrictionItem> registryRestrictions);

		void Delete(ISystemUserFull systemUserFull, IPackage package);

		void SetOfflineByIncludeProduct(ISystemUserFull systemUserFull, int productId);

		PackageCollection Search(int channelId, IProductSearchCriteria searchCriteria, int page, int pageSize);

		IPackage GetByIdSecure(int channelId, int packageId);

		IPackage GetByProductIdSecure(int channelId, int productId);

		Collection<ITranslationGeneric> GetAllGeneralInfoTranslationsByPackage(int id);
	}
}