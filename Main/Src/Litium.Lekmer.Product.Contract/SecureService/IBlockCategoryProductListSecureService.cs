﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IBlockCategoryProductListSecureService
	{
		IBlockCategoryProductList Create();
		int Save(ISystemUserFull systemUserFull, IBlockCategoryProductList block, Collection<IBlockCategoryProductListCategory> blockCategories);
		IBlockCategoryProductList GetById(int blockId);
	}
}
