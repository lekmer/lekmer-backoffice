﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductRelationListSecureService
	{
		IBlockProductRelationList Create();
		int Save(ISystemUserFull systemUserFull, IBlockProductRelationList block, Collection<IBlockProductRelationListItem> items);
		IBlockProductRelationList GetById(int blockId);
	}
}