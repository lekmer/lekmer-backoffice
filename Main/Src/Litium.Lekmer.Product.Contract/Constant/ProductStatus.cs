﻿namespace Litium.Lekmer.Product
{
	public enum ProductStatus
	{
		Online = 0,
		Offline = 1,
		ReadyToTranslate = 2
	}
}