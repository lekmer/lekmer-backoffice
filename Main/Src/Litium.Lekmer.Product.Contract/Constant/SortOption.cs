namespace Litium.Lekmer.Product
{
	public enum SortOption
	{
		Unknown,
		TitleAsc,
		TitleDesc,
		PriceAsc,
		PriceDesc,
		Popularity,
		IsNewFrom,
		DiscountPercentDesc
	}
}