﻿namespace Litium.Lekmer.Product
{
	public enum StockStatus
	{
		Active = 0,
		Passive = 1
	}
}