﻿namespace Litium.Lekmer.Product
{
	public enum RelationListType
	{
		None = 0,
		Accessories = 1,
		Similar = 2,
		UpSell = 3,
		CrossSell = 4,
		Variant = 1000001,
		ColorVariant = 1000002
	}
}