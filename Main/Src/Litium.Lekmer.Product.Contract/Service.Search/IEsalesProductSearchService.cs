using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IEsalesProductSearchService
	{
		ISearchSuggestions SearchSuggestions(IUserContext context, string text, int pageNumber, int pageSize);
		ISearchSuggestions SearchProductsWithFilter(IUserContext context, string text, IFilterQuery query, int pageNumber, int pageSize);
		ISearchSuggestions SearchProducts(IUserContext context, string text, IFilterQuery query, int pageNumber, int pageSize);
	}
}