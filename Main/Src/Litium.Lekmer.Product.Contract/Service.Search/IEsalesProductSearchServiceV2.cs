using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IEsalesProductSearchServiceV2
	{
		ISearchSuggestions SearchProducts(IUserContext context, string text, IFilterQuery query, int pageNumber, int pageSize);
	}
}