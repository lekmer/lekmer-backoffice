using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IEsalesIncrementalSearchServiceV2
	{
		ISearchSuggestions Search(IUserContext context, string text, int pageNumber, int pageSize);
	}
}