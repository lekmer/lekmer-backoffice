﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.Order.MonitorConsole
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static readonly string _delimiter = ",";
		private static readonly string _tab1 = "\r\n\t";
		private static readonly string _tab2 = "\r\n\t\t";
		private static readonly string _tab3 = "\r\n\t\t\t";

		private static readonly string _orderTemplate = _tab1 + "\"order\": [{0}" + _tab1 + "]";
		private static readonly string _orderRowTemplate = _tab3 + "\"time\" : \"{0}\"," + _tab3 + "\"image_url\" : \"{1}\"," + _tab3 + "\"order_country\" : \"{2}\"," + _tab3 + "\"order_city\" : \"{3}\"," + _tab3 + "\"order_quantity\" : \"{4}\"," + _tab3 + "\"order_total\" : \"{5}\"";

		private static readonly string _orderTotalToday = _tab1 + "\"ordertotal_today{0}\": {1}";
		private static readonly string _orderTotalYesterday = _tab1 + "\"ordertotal_yesterday{0}\": {1}";
		private static readonly string _valueTotalToday = _tab1 + "\"valuetotal_today{0}\": {1}";
		private static readonly string _valueTotalYesterday = _tab1 + "\"valuetotal_yesterday{0}\": {1}";

		private static OrderMonitorSetting _setting;
		private static OrderMonitorSetting Setting
		{
			get { return _setting ?? (_setting = new OrderMonitorSetting()); }
		}

		private static Collection<IChannel> _channels;
		private static Collection<IChannel> Channels
		{
			get
			{
				if (_channels == null)
				{
					var channelService = IoC.Resolve<IChannelSecureService>();
					_channels = channelService.GetAll();
				}

				return _channels;
			}
		}

		private static IProductService _productService;
		private static IProductService ProductService
		{
			get { return _productService ?? (_productService = IoC.Resolve<IProductService>()); }
		}

		private static IMediaUrlService _mediaUrlService;
		private static IMediaUrlService MediaUrlService
		{
			get { return _mediaUrlService ?? (_mediaUrlService = IoC.Resolve<IMediaUrlService>()); }
		}

		private static IUserContext _userContext;
		private static IUserContext UserContext
		{
			get
			{
				if (_userContext == null)
				{
					_userContext = IoC.Resolve<IUserContext>();
					_userContext.Channel = IoC.Resolve<IChannel>();
					_userContext.Channel.Id = 1;
					_userContext.Channel.Language = IoC.Resolve<ILanguage>();
					_userContext.Channel.Language.Id = 1;
				}

				return _userContext;
			}
		}

		static void Main()
		{
			DateTime date = DateTime.Now;

			_log.Info("Monitor Order Console - Started");
			Console.WriteLine("Monitor Order Console - Started");

			try
			{
				CreateFiles(date);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				_log.Error(ex);
			}

			_log.Info("Monitor Order Console - Done");
			Console.WriteLine("Monitor Order Console - Done");
		}

		private static void CreateFiles(DateTime date)
		{
			string orderFilePath = GetOrderFilePath();
			if (File.Exists(orderFilePath))
			{
				File.Delete(orderFilePath);
			}

			string totalFilePath = GetTotalFilePath();
			if (File.Exists(totalFilePath))
			{
				File.Delete(totalFilePath);
			}

			_log.Info("Get orders...");
			var orders = GetOrders(date);
			if (orders.Count <= 0)
			{
				_log.Info("There are no orders.");
				return;
			}

			_log.InfoFormat("File preparing... //{0}", orderFilePath);
			using (Stream stream = File.OpenWrite(orderFilePath))
			{
				BuildOrderFile(stream, orders);
			}
			_log.InfoFormat("File saved... //{0}", orderFilePath);

			_log.InfoFormat("File preparing... //{0}", totalFilePath);
			using (Stream stream = File.OpenWrite(totalFilePath))
			{
				BuildTotalOrderFile(stream);
			}
			_log.InfoFormat("File saved... //{0}", totalFilePath);
		}


		private static void BuildOrderFile(Stream savedSiteMap, IEnumerable<IOrder> orders)
		{
			var orderInfo = string.Empty;

			foreach (IOrderFull order in orders)
			{
				var channel = GetOrderChannel(order.ChannelId);
				var userContext = IoC.Resolve<IUserContext>();
				userContext.Channel = channel;

				var time = order.CreatedDate.ToString(CultureInfo.InvariantCulture);
				var country = channel != null && channel.Country != null ? channel.Country.Title : string.Empty;
				var city = GetOrderCity(order);
				var total = GetOrderTotal(order);
				var quantity = 0;
				var imageUrl = string.Empty;
				var needImage = true;

				string mediaUrl = MediaUrlService.ResolveMediaArchiveExternalUrl(channel);

				foreach (var orderItem in order.GetOrderItems())
				{
					if (needImage)
					{
						var product = ProductService.GetById(userContext, orderItem.ProductId);
						if (product.Image != null)
						{
							var image = product.Image;

							imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image);

							needImage = false;
						}
					}

					quantity += orderItem.Quantity;
				}

				string orderRow = string.Format(CultureInfo.InvariantCulture, _orderRowTemplate, time, imageUrl, country, city, quantity, total);
				orderInfo += _tab2 + "{" + orderRow + _tab2 + "}" + _delimiter;
			}

			var info = "{" + string.Format(CultureInfo.InvariantCulture, _orderTemplate, RemoveRedundantDelimiter(orderInfo)) + "\r\n}";

			var writer = new StreamWriter(savedSiteMap);
			writer.WriteLine(info);
			writer.Flush();
			writer.Close();
		}

		private static void BuildTotalOrderFile(Stream savedSiteMap)
		{
			var statistics = GetOrdersStatistic();

			var currentDate = DateTime.Now;
			var yesterdayDate = currentDate.AddDays(-1);

			var statisticsToday = statistics.Where(s => s.Date.Year == currentDate.Year && s.Date.Month == currentDate.Month && s.Date.Day == currentDate.Day);
			var statisticsYesterday = statistics.Where(s => s.Date.Year == yesterdayDate.Year && s.Date.Month == yesterdayDate.Month && s.Date.Day == yesterdayDate.Day);

			string orderTotalToday = string.Empty;
			string orderTotalYesterday = string.Empty;
			string totalValueToday = string.Empty;
			string totalValueYesterday = string.Empty;

			foreach (var statistic in statisticsToday)
			{
				var channel = GetChannelAbbreviuation(statistic.Channel);
				orderTotalToday += string.Format(CultureInfo.InvariantCulture, _orderTotalToday, channel, statistic.NumberOfOrders) + _delimiter;
				totalValueToday += string.Format(CultureInfo.InvariantCulture, _valueTotalToday, channel, statistic.TotalOrderValue) + _delimiter;
			}

			foreach (var statistic in statisticsYesterday)
			{
				var channel = GetChannelAbbreviuation(statistic.Channel);
				orderTotalYesterday += string.Format(CultureInfo.InvariantCulture, _orderTotalYesterday, channel, statistic.NumberOfOrders) + _delimiter;
				totalValueYesterday += string.Format(CultureInfo.InvariantCulture, _valueTotalYesterday, channel, statistic.TotalOrderValue) + _delimiter;
			}

			string totalSumary = orderTotalToday + orderTotalYesterday + totalValueToday + totalValueYesterday;

			var statisticInfo = "{" + RemoveRedundantDelimiter(totalSumary) + "\r\n}";

			var writer = new StreamWriter(savedSiteMap);
			writer.WriteLine(statisticInfo);
			writer.Flush();
			writer.Close();
		}


		private static IChannel GetOrderChannel(int channelId)
		{
			return Channels.FirstOrDefault(ch => ch.Id == channelId);
		}

		private static string GetChannelAbbreviuation(string channelname)
		{
			switch (channelname)
			{
				case "Sweden":
					return "_se";
				case "Denmark":
					return "_dk";
				case "Norway":
					return "_no";
				case "Finland":
					return "_fi";
				default:
					return string.Empty;
			}
		}

		private static string GetOrderCity(IOrderFull order)
		{
			return order.BillingAddress != null ? order.BillingAddress.City : string.Empty;
		}

		private static string GetOrderTotal(IOrderFull order)
		{
			var lekmerOrderFull = (ILekmerOrderFull)order;

			var orderTotal = 0m;

			var freightCost = lekmerOrderFull.GetTotalFreightCost();
			var paymentCost = lekmerOrderFull.PaymentCost;
			var orderAmount = order.Payments != null && order.Payments.Count > 0 ? order.Payments[0].Price : 0m;

			orderTotal = freightCost + paymentCost + orderAmount;

			return orderTotal.ToString(CultureInfo.InvariantCulture);
		}

		private static Collection<OrderStatistic> GetOrdersStatistic()
		{
			var repository = new OrderMonitorRepository();
			return repository.GetOrderStatistics();
		}

		private static string RemoveRedundantDelimiter(string value)
		{
			var lastDelimiterPosition = value.LastIndexOf(_delimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				value = value.Substring(0, lastDelimiterPosition);
			}
			return value;
		}

		private static string GetOrderFilePath()
		{
			string dirr = Setting.DestinationDirectory;
			string fileName = Setting.OrderFileName;
			return Path.Combine(dirr, fileName);
		}

		private static string GetTotalFilePath()
		{
			string dirr = Setting.DestinationDirectory;
			string fileName = Setting.TotalFileName;
			return Path.Combine(dirr, fileName);
		}

		private static Collection<IOrder> GetOrders(DateTime date)
		{
			var orderService = IoC.Resolve<IOrderService>();
			return ((ILekmerOrderService)orderService).GetOrders(UserContext, date, Setting.MinutesAfterPurchase);
		}
	}
}