﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[ServiceContract]
	public interface ISuggestions
	{
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		InsertDataResponse InsertData(InsertDataRequest request);

		[OperationContract]
		[WebGet(UriTemplate = "Session({sessionId})/Product({productId})", ResponseFormat = WebMessageFormat.Json)]
		GetDataResponse GetData(string sessionId, string productId);
	}
}
