﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[DataContract]
	[Serializable]
	public class ProductSuggestion
	{
		[DataMember]
		public string ProductId { get; set; }

		[DataMember]
		public Collection<string> SuggestionIds { get; set; }
	}
}
