﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[DataContract]
	[Serializable]
	public class InsertDataRequest
	{
		[DataMember]
		public SessionSuggestion Suggestion { get; set; }
	}
}
