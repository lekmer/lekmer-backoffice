﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomerService : ICustomerService
	{
		ILekmerCustomer Create(IUserContext context, bool hasInformation, bool isUser, bool isFacebookUser);
		ICustomer GetByFacebookId(IUserContext context, string facebookId);
	}
}