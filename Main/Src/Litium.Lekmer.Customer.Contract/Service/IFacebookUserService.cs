﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Customer
{
	public interface IFacebookUserService
	{
		IFacebookUser Create(IUserContext context);
		void Save(IUserContext context, IFacebookUser user);
	}
}
