﻿namespace Litium.Lekmer.Customer
{
	public interface IMyAccountSetting
	{
		int NewPasswordLinkLifeCycleValue { get; }
		string FacebookAppId { get; }
		string FacebookAppSecret { get; }
		string FacebookCookieMask { get; }
		string FacebookAlgoritm { get; }
		string FacebookTokenRequestUrl { get; }
		string FacebookUserProfileRequestUrl { get; }
		string FacebookPictureDataRequestUrl { get; }
		string FacebookPictureRequestUrl { get; }
	}
}