﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerDeliveryAddress : IDeliveryAddress, ILekmerAddress
	{
	}
}
