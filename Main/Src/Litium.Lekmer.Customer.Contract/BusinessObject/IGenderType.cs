﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public interface IGenderType : IBusinessObjectBase
	{
		int Id { get; set; }
		string ErpId { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
	}
}