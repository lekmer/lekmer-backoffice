﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomerInformation : ICustomerInformation
	{
		int GenderTypeId { get; set; }
		bool IsCompany { get; set; }
		int? AlternateAddressId { get; set; }
		ILekmerAddress AlternateAddress { get; set; }
	}
}
