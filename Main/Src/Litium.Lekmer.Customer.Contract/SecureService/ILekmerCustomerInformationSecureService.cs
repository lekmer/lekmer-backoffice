﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomerInformationSecureService : ICustomerInformationSecureService
	{
		void DeleteByCustomer(ISystemUserFull systemUserFull, int customerId);
	}
}