using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Product;

namespace Litium.Lekmer.GoogleFeed
{
	[Serializable]
	public class GoogleFeedInfo : IGoogleFeedInfo
	{
		public ILekmerProductView Product { get; set; }
		public ISizeTable SizeTable { get; set; }
		public Collection<IProductImageGroupFull> ImageGroups { get; set; }
		public string FirstLevelCategoryTitle { get; set; }
		public string SecondLevelCategoryTitle { get; set; }
		public string ThirdLevelCategoryTitle { get; set; }
	}
}