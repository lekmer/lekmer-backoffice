﻿using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.Foundation;
using Litium.Scensum.FtpServer;
using log4net.Config;

namespace Litium.Lekmer.FtpServer.Service
{
	public partial class FtpService : ServiceBase
	{
		private static readonly Thread _serverThread = CreateThread();

		public FtpService()
		{
			InitializeComponent();
		}

		private static void Start()
		{
			_serverThread.Start();
		}

		private static void Abort()
		{
			if (_serverThread != null)
			{
				_serverThread.Abort();
			}
		}

		private static Thread CreateThread()
		{
			return new Thread(RunFtpServer);
		}

		private static void RunFtpServer()
		{
			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();

			XmlConfigurator.Configure();

			Initializer.InitializeAll();

			RoutingRegistration.RegisterRoutes();

			ServerStarter.Start(new Log());
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			Start();
		}

		protected override void OnStop()
		{
			Abort();
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}
