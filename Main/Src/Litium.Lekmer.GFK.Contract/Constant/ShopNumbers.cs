﻿using System.ComponentModel;

namespace Litium.Lekmer.GFK
{
	public enum ShopNumbers
	{
		[Description("001")]
		Sweden = 1,
		[Description("002")]
		Norway = 2,
		[Description("003")]
		Denmark = 3,
		[Description("004")]
		Finland = 4,
		[Description("005")]
		Netherlands = 5
	}
}