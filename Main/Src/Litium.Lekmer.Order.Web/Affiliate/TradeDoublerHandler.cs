using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Web
{
	public class TradeDoublerHandler : HandlerBase
	{
		protected override void ProcessRequest()
		{
			string tradeDoublerId = Request.QueryString["tduid"];
			AffiliateCookie.SetTradeDoublerId(tradeDoublerId);

			string url = Request.QueryString["url"];
			if (url.IsNullOrTrimmedEmpty())
			{
				url = "~/";
			}

			Response.PermanentRedirect(url);
		}
	}
}