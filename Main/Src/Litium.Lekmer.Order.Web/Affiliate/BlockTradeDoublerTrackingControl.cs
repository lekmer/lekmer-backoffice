using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order.Web
{
	public class BlockTradeDoublerTrackingControl : BlockControlBase
	{
		StringBuilder _trackingUrlBuilder = new StringBuilder();
		private int _renderedOrderItemCount;
		private const int maxLength = 2000;
		private Collection<IOrderItem> _orderItems;
		private const int _baseLength = 200;
		private int _currentLength;
		private readonly IOrderSession _orderSession;
		private readonly IOrderService _orderService;
		private readonly IFormatter _formatter;
		private readonly ITradeDoubleProductGroupService _tradeDoubleProductGroupService;
		public BlockTradeDoublerTrackingControl(
			ITemplateFactory templateFactory, IBlockService blockService,
			IOrderSession orderSession, IOrderService orderService, IFormatter formatter, ITradeDoubleProductGroupService tradeDoubleProductGroupService)
			: base(templateFactory, blockService)
		{
			_orderSession = orderSession;
			_orderService = orderService;
			_formatter = formatter;
			_tradeDoubleProductGroupService = tradeDoubleProductGroupService;
		}

		protected override BlockContent RenderCore()
		{
			int? orderId = _orderSession.OrderId;
			if (!orderId.HasValue)
			{
				return RenderNoOrderInSession();
			}

			var order = _orderService.GetFullById(UserContext.Current, orderId.Value);
			if (order == null)
			{
				return RenderNoOrderInSession();
			}
			_orderItems = order.GetOrderItems();
			if (_orderItems.Count == 0)
			{
				return RenderNoOrderInSession();
			}
			string tradeDoublerId = AffiliateCookie.GetTradeDoublerId();
			bool hasTradeDoublerId = !tradeDoublerId.IsNullOrTrimmedEmpty();

			string trackingUrl;

			int count = 0;
			while (_renderedOrderItemCount < _orderItems.Count && count < _orderItems.Count)
			{
				_currentLength = _baseLength;
				string orderNumberSufFix = count == 0 ? "" : "_" + (count + 1);
				trackingUrl = GenerateTrackingUrl(hasTradeDoublerId, tradeDoublerId, order, orderNumberSufFix);
				_trackingUrlBuilder.AppendLine(RenderTrackingUrl(hasTradeDoublerId, trackingUrl));
				count++;
			}

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, _trackingUrlBuilder.ToString(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private string RenderTrackingUrl(bool hasTradeDoublerId, string trackingUrl)
		{
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddCondition("HasTradeDoublerId", hasTradeDoublerId);
			fragmentContent.AddVariable("TrackingUrl", trackingUrl, VariableEncoding.HtmlEncodeLight);
			return fragmentContent.Render();
		}

		private string GenerateTrackingUrl(bool hasTradeDoublerId, string tradeDoublerId, IOrderFull order, string orderNumberSufFix)
		{
			var orderValue = (Math.Round(GetOrderValue(order), 2, MidpointRounding.AwayFromZero));
			string secretCode = Template.GetSettingOrNull("SecretCode") ?? string.Empty;

			const string checksumVersion = "v04";
			string ov = orderValue.ToString("0.##", CultureInfo.InvariantCulture);
			string checksum = checksumVersion + Md5Hasher.HashIt(secretCode + order.Number + ov);

			Fragment fragmentTrackingUrl = Template.GetFragment("TrackingUrl");
			fragmentTrackingUrl.AddVariable("TradeDoublerId", tradeDoublerId, VariableEncoding.UrlEncode);
			fragmentTrackingUrl.AddVariable("Channel.Currency.Iso", Channel.Current.Currency.Iso, VariableEncoding.UrlEncode);
			fragmentTrackingUrl.AddVariable("Order.Number", order.Number + orderNumberSufFix, VariableEncoding.UrlEncode);
			fragmentTrackingUrl.AddVariable("Checksum", checksum, VariableEncoding.UrlEncode);
			fragmentTrackingUrl.AddCondition("HasTradeDoublerId", hasTradeDoublerId);
			if (hasTradeDoublerId)
				_currentLength += tradeDoublerId.Length;
			_currentLength += order.Number.Length;
			_currentLength += checksum.Length;
			fragmentTrackingUrl.AddVariable("Iterate:OrderItem", RenderOrderItemList(hasTradeDoublerId), VariableEncoding.None);

			return fragmentTrackingUrl.Render();
		}


		private string RenderOrderItemList(bool hasTradeDoublerId)
		{
			var itemBuilder = new StringBuilder();
			for (int i = _renderedOrderItemCount; i < _orderItems.Count; i++)
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddVariable("OrderItem.ErpId", _orderItems[i].ErpId, VariableEncoding.UrlEncode);
				//This code was removed from source 2011-09-02 - added for iSales 2011-10-14

				ITradeDoubleProductGroup pductGroup = _tradeDoubleProductGroupService.GetProductGroupMapping(Channel.Current, _orderItems[i].ProductId);
				string pg = "";
				if (pductGroup != null)
				{
					pg = pductGroup.ProductGroupId;

				}
				else
				{
					pg = Channel.Current.Title + " sale";
				}
				//This change is based on the mail conversation with Sinan Tuncyurek and Dino
				//fragmentOrderItem.AddVariable("OrderItem.GR", pductGroup!=null?pductGroup.ProductGroupId.ToString():"0", VariableEncoding.UrlEncode);
				fragmentOrderItem.AddVariable("OrderItem.GR", pg, VariableEncoding.UrlEncode);
				fragmentOrderItem.AddVariable("OrderItem.Title", LekmerEncoder.UrlEncodeWithParentheses(_orderItems[i].Title), VariableEncoding.None);
				fragmentOrderItem.AddVariable("OrderItem.Quantity", _formatter.FormatNumber(Channel.Current, _orderItems[i].Quantity));
				fragmentOrderItem.AddVariable("OrderItem.Price", _orderItems[i].ActualPrice.ExcludingVat.ToString(CultureInfo.InvariantCulture));
				fragmentOrderItem.AddCondition("HasTradeDoublerId", hasTradeDoublerId);
				string temp = fragmentOrderItem.Render();
				if (_currentLength + temp.Length > maxLength)
				{
					break;
				}
				itemBuilder.Append(temp);
				_currentLength += temp.Length;
				_renderedOrderItemCount++;
			}

			return itemBuilder.ToString();
		}
		private static decimal GetOrderValue(IOrderFull order)
		{
			return order.GetActualPriceSummary().ExcludingVat + GetFreightExcludingVat(order);
		}

		private static decimal GetFreightExcludingVat(IOrderFull order)
		{
			return ((ILekmerOrderFull)order).GetTotalFreightCost() / 1.25m;
		}

		private static BlockContent RenderNoOrderInSession()
		{
			return new BlockContent();
		}
	}
}