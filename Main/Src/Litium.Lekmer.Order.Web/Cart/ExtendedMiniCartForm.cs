using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class ExtendedMiniCartForm : ControlBase
	{
		private readonly string _checkoutPostUrl;

		public ExtendedMiniCartForm(string checkoutPostUrl)
		{
			_checkoutPostUrl = checkoutPostUrl;
		}

		protected virtual string PostUrl
		{
			get { return _checkoutPostUrl; }
		}

		protected virtual string PostModeValue
		{
			get { return "mini-checkout"; }
		}

		protected virtual string PostModeChangeQuantityValue
		{
			get { return "change-quantity"; }
		}


		public bool IsChangeQuantityPost()
		{
			return PostMode.Equals(PostModeChangeQuantityValue);
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.PostMode.ChangeQuantity.Value", PostModeChangeQuantityValue);
		}
	}
}