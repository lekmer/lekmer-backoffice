using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Cart;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCartControl : CartControl
	{
		private CartHelper _cartHelper;

		public LekmerCartControl(
			ICartService cartService, ICartSession cartSession, IFormatter formatter, ITemplateFactory templateFactory)
			: base(cartService, cartSession, formatter, templateFactory)
		{
			LekmerFormatter = (ILekmerFormatter)formatter;
		}

		protected ILekmerFormatter LekmerFormatter { get; private set; }

		/// <summary>
		/// Renders a cart with items.
		/// </summary>
		/// <returns>Rendered cart. Null if no items is added to the cart.</returns>
		protected override string RenderCart()
		{
			_cartHelper = new CartHelper();

			var cart = IoC.Resolve<ICartSession>().Cart as ILekmerCartFull;
			if (cart == null)
			{
				return null;
			}

			Collection<ILekmerCartItem> cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (cartItems.Count == 0)
			{
				return null;
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			var availSession = IoC.Resolve<IAvailSession>();
			int? productId = availSession.AvailProductId;

			string trackingCode = availSession.TrackingCode;
			bool isRecommendedProduct = !string.IsNullOrEmpty(trackingCode);

			fragmentContent.AddCondition("AddToAvail", productId.HasValue);
			fragmentContent.AddCondition("Product.IsRecommended", isRecommendedProduct);

			if (productId.HasValue)
			{
				fragmentContent.AddVariable("Product.Id", productId.Value.ToString(CultureInfo.InvariantCulture));
				availSession.AvailProductId = null;
			}
			if (isRecommendedProduct)
			{
				fragmentContent.AddVariable("TrackingCode", trackingCode);
				availSession.TrackingCode = null;
			}

			fragmentContent.AddVariable("Iterate:CartItem", RenderCartItems(new Collection<ICartItem>(cartItems.Cast<ICartItem>().ToArray())), VariableEncoding.None);

			// Cart info
			_cartHelper.RenderCartInfo(fragmentContent, cart, LekmerFormatter);

			// Cart price
			_cartHelper.RenderCartPrices(fragmentContent, cart, LekmerFormatter);

			return fragmentContent.Render();
		}

		protected override string RenderCartItem(ICartItem cartItem)
		{
			var productIdAdded = Session["CartItemAdded"] as int?;
			bool itemWasAdded = productIdAdded.HasValue && productIdAdded.Value == cartItem.Product.Id;

			Fragment fragment = Template.GetFragment("CartItem");
			fragment.AddEntity(cartItem.Product);
			fragment.AddVariable("Product.CampaignInfoPriceHidden", cartItem.Product.CampaignInfo.Price.IncludingVat.ToString());
			fragment.AddVariable("CartItem.Quantity", Formatter.FormatNumber(Channel.Current, cartItem.Quantity));

			var lekmerCartItem = (ILekmerCartItem)cartItem;

			// Price
			_cartHelper.RenderCartItemPrices(fragment, lekmerCartItem, LekmerFormatter);

			fragment.AddCondition("ItemWasAdded", itemWasAdded);

			fragment.AddCondition("CartItem.HasSize", lekmerCartItem.SizeId.HasValue);
			fragment.AddEntity(GetSize(lekmerCartItem));

			bool isPackageWithSizes = _cartHelper.IsPackageWithSizes(lekmerCartItem);
			fragment.AddCondition("CartItem.IsPackageWithSizes", isPackageWithSizes);
			if (isPackageWithSizes)
			{
				fragment.AddVariable("CartItem.PackageElementsHashCode", lekmerCartItem.PackageElements.GetPackageElementsHashCode().ToString());
			}

			return fragment.Render();
		}

		private static IProductSize GetSize(ILekmerCartItem item)
		{
			return !item.SizeId.HasValue
					   ? IoC.Resolve<IProductSize>()
					   : IoC.Resolve<IProductSizeService>().GetById(item.Product.Id, item.SizeId.Value);
		}
	}
}