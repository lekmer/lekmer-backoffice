﻿using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Order.Web.OrderHistory;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Order.Web.OrderHistory
{
	public abstract class LekmerOrderDetailPageHandler : OrderDetailPageHandler
	{
		protected override AreaHandler CreateAreaHandlerInstance(IContentAreaFull contentArea)
		{
			return LekmerAreaHandlerFactory.CreateAreaHandlerInstance(ContentPage, ContentNodeTreeItem, contentArea, this);
		}
	}
}