﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.OrderHistory;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class BlockOrderHistoryDetailControl : BlockControlBase
	{
		private const string _tagGroupMatchPattern = @"\[TagGroup\(\""(.+)\""\)\]";

		private IOrderFull _order;
		private readonly ICustomerSession _customerSession;
		private readonly IPaymentTypeService _paymentTypeService;
		private readonly ILekmerProductService _productService;

		public IOrderFull Order
		{
			get
			{
				if (_order != null) return _order;

				var pageHandler = ContentPageHandler as OrderDetailPageHandler;
				if (pageHandler == null) return null;

				_order = pageHandler.Order;

				return _order;
			}
		}

		public BlockOrderHistoryDetailControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICustomerSession customerSession,
			IPaymentTypeService paymentTypeService,
			IProductService productService)
			: base(templateFactory, blockService)
		{
			_customerSession = customerSession;
			_paymentTypeService = paymentTypeService;
			_productService = (ILekmerProductService) productService;
		}

		protected override BlockContent RenderCore()
		{
			if (Order == null) return new BlockContent(null, "[ Can't render order history detail on this page ]");
			if (!_customerSession.IsSignedIn) return new BlockContent();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(), VariableEncoding.None);
			RenderPaymentTypeVariables(fragmentContent);
			fragmentContent.AddEntity(Order);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		protected virtual string RenderOrderItemList()
		{
			var itemBuilder = new StringBuilder();
			foreach (var orderItem in Order.GetOrderItems())
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddEntity(orderItem);
				RenderTagGroups(fragmentOrderItem, (ILekmerOrderItem) orderItem);
				RenderProductUrls(fragmentOrderItem, orderItem.ProductId);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}
			return itemBuilder.ToString();
		}

		protected virtual void RenderPaymentTypeVariables(Fragment fragment)
		{
			if (Order.Payments.Count > 0)
			{
				IPaymentType paymentType = _paymentTypeService.GetAll(UserContext.Current).FirstOrDefault(pt => pt.Id == Order.Payments[0].PaymentTypeId);
				if (paymentType != null)
				{
					fragment.AddVariable("Order.PaymentType.Title", paymentType.Title);
					fragment.AddVariable("Order.PaymentType.Alias", AliasHelper.GetAliasValue("Payment.PaymentType_" + paymentType.CommonName), VariableEncoding.None);
				}
			}
		}

		protected virtual void RenderTagGroups(Fragment fragment, ILekmerOrderItem orderItem)
		{
			fragment.AddCondition("OrderItem.HasTagGroups", orderItem.TagGroups.Count > 0);
			fragment.AddRegexVariable(
				_tagGroupMatchPattern,
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;
					return RenderTagGroup(tagGroupCommonName, orderItem);
				},
				VariableEncoding.None);
		}
		protected virtual string RenderTagGroup(string commonName, ILekmerOrderItem orderItem)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var tagGroup = orderItem.TagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count <= 0)
			{
				return null;
			}

			var fragmentTagGroup = Template.GetFragment("TagGroupItem");
			fragmentTagGroup.AddVariable("TagGroup.OriginalTitle", tagGroup.Title, VariableEncoding.None);
			fragmentTagGroup.AddVariable("TagGroup.CommonName", tagGroup.CommonName, VariableEncoding.None);
			fragmentTagGroup.AddVariable("TagGroup.AliasTitle", AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName), VariableEncoding.None);
			fragmentTagGroup.AddCondition("OrderItem.HasTags", tagGroup.Tags.Count > 0);
			fragmentTagGroup.AddVariable("Iterate:TagItem", RenderTags(tagGroup), VariableEncoding.None);
			return fragmentTagGroup.Render();
		}
		protected virtual string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();
			int index = 0;
			int tagsCount = tagGroup.Tags.Count;
			foreach (ITag tag in tagGroup.Tags)
			{
				var fragmentTag = Template.GetFragment("TagItem");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, index++, tagsCount);
				tagBuilder.AppendLine(fragmentTag.Render());
			}
			return tagBuilder.ToString();
		}
		protected virtual void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}

		protected virtual void RenderProductUrls(Fragment fragment, int productId)
		{
			string absoluteUrl = string.Empty;
			string relativeUrl = string.Empty;

			var product = _productService.GetById(UserContext.Current, productId);
			if (product != null)
			{
				var lekmerProduct = (ILekmerProduct) product;
				absoluteUrl = UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl);
				relativeUrl = UrlHelper.ResolveUrlHttp(lekmerProduct.LekmerUrl);
			}

			fragment.AddVariable("Product.Url", absoluteUrl);
			fragment.AddVariable("Product.Url.RootRelative", relativeUrl);
		}
	}
}