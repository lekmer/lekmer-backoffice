using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class CurrentCartForm : ControlBase
	{
		private readonly string _postUrl;
		protected virtual string PostUrl
		{
			get { return _postUrl; }
		}

		protected virtual string PostModeValue
		{
			get { return "current-cart-mode"; }
		}

		protected virtual string DeleteAllFormName
		{
			get { return "current-cart-delete-all"; }
		}

		public CurrentCartForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public bool IsCurrentCartPost()
		{
			return PostMode.Equals(PostModeValue);
		}
		public bool IsDeleteAllPost()
		{
			var deleteAllFormName = Request.Form[DeleteAllFormName];
			return deleteAllFormName != null;
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl.CurrentCart", PostUrl);
			fragment.AddVariable("Form.PostMode.Name.CurrentCart", PostModeName);
			fragment.AddVariable("Form.PostMode.Value.CurrentCart", PostModeValue);
			fragment.AddVariable("Form.DeleteAll.Name.CurrentCart", DeleteAllFormName);
		}
	}
}