using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class SavedCartForm : ControlBase
	{
		private readonly string _postUrl;
		protected virtual string PostUrl
		{
			get { return _postUrl; }
		}

		protected virtual string PostModeValue
		{
			get { return "saved-cart-mode"; }
		}

		protected virtual string DeleteAllFormName
		{
			get { return "saved-cart-delete-all"; }
		}

		public virtual string DeleteCartItemFormName
		{
			get { return "sc-delete-item"; }
		}

		public SavedCartForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public bool IsSavedCartPost()
		{
			return PostMode.Equals(PostModeValue);
		}
		public bool IsDeleteAllPost()
		{
			var deleteAllFormName = Request.Form[DeleteAllFormName];
			return deleteAllFormName != null;
		}

		public bool IsDeleteItemPost(string key)
		{
			var deleteItemFormName = Request.Form[key];
			return deleteItemFormName != null;
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl.SavedCart", PostUrl);
			fragment.AddVariable("Form.PostMode.Name.SavedCart", PostModeName);
			fragment.AddVariable("Form.PostMode.Value.SavedCart", PostModeValue);
			fragment.AddVariable("Form.DeleteAll.Name.SavedCart", DeleteAllFormName);
		}
	}
}