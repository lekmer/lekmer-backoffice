﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class Delivery : ControlBase
	{
		private ILekmerDeliveryMethodService _deliveryMethodService;
		private ICartSession _cartSession;
		private CartHelper _cartHelper;

		private Collection<IDeliveryMethod> _deliveryMethods;
		private ITemplateFactory _templateFactory;
		private Template _template;

		private int _paymentTypeId;
		private int _formDeliveryMethodId;
		private int _formOptionalDeliveryMethodId;
		private int _deliveryMethodId;
		private int _optionalDeliveryMethodId;

		private IFormatter _formatter;


		protected string DeliveryMethodFormName
		{
			get { return "deliveryMethod"; }
		}

		protected string OptionalDeliveryMethodFormName
		{
			get { return "delivery-method-optional"; }
		}

		protected bool IsChangeDeliveryMethodPostMode
		{
			get { return IsPostBack && PostMode.Equals("changeDeliveryMethod", StringComparison.InvariantCultureIgnoreCase); }
		}


		protected ICartFull Cart
		{
			get { return _cartSession.Cart; }
		}

		protected CartHelper CartHelper
		{
			get { return _cartHelper ?? (_cartHelper = new CartHelper()); }
		}


		public int PaymentTypeId
		{
			get { return _paymentTypeId; }
			set { _paymentTypeId = value; }
		}

		protected int FormDeliveryMethodId
		{
			get
			{
				if (_formDeliveryMethodId == 0)
				{
					string deliveryMethodRaw = Request.Form[DeliveryMethodFormName];
					int.TryParse(deliveryMethodRaw, out _formDeliveryMethodId);
				}
				return _formDeliveryMethodId;
			}
		}

		protected int FormOptionalDeliveryMethodId
		{
			get
			{
				if (_formOptionalDeliveryMethodId == 0)
				{
					string deliveryMethodRaw = Request.Form[OptionalDeliveryMethodFormName];
					int.TryParse(deliveryMethodRaw, out _formOptionalDeliveryMethodId);
				}
				return _formOptionalDeliveryMethodId;
			}
		}

		protected int DeliveryMethodId
		{
			get
			{
				if (_deliveryMethodId == 0)
				{
					_deliveryMethodId = FormDeliveryMethodId;
				}
				return _deliveryMethodId;
			}
			set { _deliveryMethodId = value; }
		}

		protected int OptionalDeliveryMethodId
		{
			get
			{
				if (_optionalDeliveryMethodId == 0)
				{
					_optionalDeliveryMethodId = FormOptionalDeliveryMethodId;
				}
				return _optionalDeliveryMethodId;
			}
			set { _optionalDeliveryMethodId = value; }
		}

		protected int ProfileDeliveryMethodId { get; set; }

		protected int ProfileOptionalDeliveryMethodId { get; set; }


		protected string ModelCommonName
		{
			get { return "DeliveryType"; }
		}

		// Methods

		public virtual void Initialize()
		{
			_deliveryMethodService = (ILekmerDeliveryMethodService)IoC.Resolve<IDeliveryMethodService>();
			_cartSession = IoC.Resolve<ICartSession>();
			_templateFactory = IoC.Resolve<ITemplateFactory>();
			_template = _templateFactory.Create(ModelCommonName);
			_formatter = IoC.Resolve<IFormatter>();

			InitializeProfileDelivery();
		}

		public virtual string Render()
		{
			Initialize();

			if (PaymentTypeId > 0)
			{
				Fragment fragment = _template.GetFragment("DeliveryMethodList");

				RenderDeliveryMethods(fragment);

				fragment.AddVariable("Form.DeliveryMethod.Name", DeliveryMethodFormName);
				fragment.AddVariable("Form.OptionalDeliveryMethod.Name", OptionalDeliveryMethodFormName);

				return fragment.Render();
			}

			return "";
		}


		public virtual decimal GetActualDeliveryPrice(ICartFull cart)
		{
			var freightCost = 0m;

			var orderDelivery = GetOrderDelivery(cart);
			if (orderDelivery != null)
			{
				freightCost = orderDelivery.FreightCost;
			}

			return freightCost;
		}

		public virtual decimal GetActualOptionalDeliveryPrice(ICartFull cart)
		{
			var freightCost = 0m;

			var orderOptionalDelivery = GetOrderOptionalDelivery(cart);
			if (orderOptionalDelivery != null)
			{
				freightCost = orderOptionalDelivery.FreightCost;
			}

			return freightCost;
		}

		public virtual decimal GetActualDiapersDeliveryPrice(ICartFull cart)
		{
			var freightCost = 0m;

			var orderDiapersDelivery = GetOrderDiapersDelivery(cart);
			if (orderDiapersDelivery != null)
			{
				freightCost = orderDiapersDelivery.FreightCost;
			}

			return freightCost;
		}


		public virtual IDeliveryMethod GetOrderDelivery(ICartFull cart)
		{
			ILekmerDeliveryMethod orderDelivery = null;

			if (DeliveryMethodId == 0)
			{
				return null;
			}

			if (DeliveryMethodId > 0)
			{
				var deliveryMethod = (ILekmerDeliveryMethod)_deliveryMethodService.GetById(UserContext.Current, DeliveryMethodId);
				if (deliveryMethod != null)
				{
					orderDelivery = ObjectCopier.Clone(deliveryMethod);
					orderDelivery.FreightCost = CalculateDeliveryFreightCostActual(orderDelivery, cart);

					CalculateDeliveryFreightCostBasedOnItemsType(orderDelivery, cart);
				}
			}

			return orderDelivery;
		}

		public virtual IDeliveryMethod GetOrderOptionalDelivery(ICartFull cart)
		{
			ILekmerDeliveryMethod orderOptionalDelivery = null;

			if (DeliveryMethodId == 0)
			{
				return null;
			}

			if (OptionalDeliveryMethodId > 0)
			{
				// Check if optional delivery method is connected to main delivery method
				var deliveryMethod = (ILekmerDeliveryMethod)_deliveryMethodService.GetOptionalByDeliveryMethod(UserContext.Current, DeliveryMethodId);
				if (deliveryMethod != null && deliveryMethod.Id == OptionalDeliveryMethodId)
				{
					orderOptionalDelivery = ObjectCopier.Clone(deliveryMethod);
					orderOptionalDelivery.FreightCost = CalculateDeliveryFreightCostActual(orderOptionalDelivery, cart);
				}
			}

			orderOptionalDelivery = (ILekmerDeliveryMethod)GetOrderOptionalDeliveryBasedOnItemsType(cart, orderOptionalDelivery);

			return orderOptionalDelivery;
		}

		public virtual IDeliveryMethod GetOrderDiapersDelivery(ICartFull cart)
		{
			if (DeliveryMethodId == 0)
			{
				return null;
			}

			IDeliveryMethod diapersDelivery = GetOrderDiapersDeliveryBasedOnItemsType(cart);

			return diapersDelivery;
		}

		/// <summary>
		/// When cart contains only diapers - main delivery is not in use (only for private person)
		/// </summary>
		public virtual bool CheckForDeliveryUsage(ICartFull cart)
		{
			if (DeliveryMethodId != (int)DeliveryMethodType.Post) // Only private persons, ignore companies
			{
				return true;
			}

			if (CartHelper.DiapersAreSellable(UserContext.Current.Channel))
			{
				var lekmerCart = (ILekmerCartFull)cart;
				if (lekmerCart.ItemsType == OrderItemsType.Diapers) // only diapers
				{
					return false;
				}
			}

			return true;
		}


		/// <summary>
		/// Validate if selected delivery method is valid and allowed one.
		/// </summary>
		/// <returns></returns>
		public virtual ValidationResult Validate(bool companyMode, ICartFull cart)
		{
			var validationResult = new ValidationResult();

			var deliveryMethod = (ILekmerDeliveryMethod)GetOrderDelivery(cart);
			var optionalDeliveryMethod = (ILekmerDeliveryMethod)GetOrderOptionalDelivery(cart);
			var diapersDeliveryMethod = (ILekmerDeliveryMethod)GetOrderDiapersDelivery(cart);

			if (deliveryMethod == null)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.DeliveryNotProvided"));
			}
			else
			{
				if (companyMode && deliveryMethod.IsCompany == false)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.DeliveryNotAllowedForCompany"));
				}
				else if (companyMode == false && deliveryMethod.IsCompany)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.DeliveryNotAllowedForPerson"));
				}
			}

			// Check if selected optional delivery method is actually one that connected to main as an optional.
			if (deliveryMethod != null && optionalDeliveryMethod != null)
			{
				IDeliveryMethod allowedOptionalDeliveryMethod = _deliveryMethodService.GetOptionalByDeliveryMethod(UserContext.Current, deliveryMethod.Id);
				if (allowedOptionalDeliveryMethod == null || allowedOptionalDeliveryMethod.Id != optionalDeliveryMethod.Id)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.OptionalDeliveryNotAllowed"));
				}
			}

			// Check if diapers delivery method still actual.
			if (diapersDeliveryMethod != null)
			{
				IDeliveryMethod orderDiapersDeliveryBasedOnItemsType = GetOrderDiapersDeliveryBasedOnItemsType(cart);
				if (orderDiapersDeliveryBasedOnItemsType == null || orderDiapersDeliveryBasedOnItemsType.Id != diapersDeliveryMethod.Id)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.DiapersDeliveryNotAllowed"));
				}
			}

			return validationResult;
		}


		protected virtual void RenderDeliveryMethods(Fragment fragment)
		{
			var productsBuilder = new StringBuilder();

			_deliveryMethods = _deliveryMethodService.GetAllByPaymentType(UserContext.Current, PaymentTypeId);

			bool isDefaultAvaliable = _deliveryMethods.Cast<ILekmerDeliveryMethod>().Any(lekmerDeliveryMethod => lekmerDeliveryMethod.ChannelDefault);

			foreach (ILekmerDeliveryMethod deliveryMethod in _deliveryMethods)
			{
				productsBuilder.Append(MapDeliveryMethod(deliveryMethod, isDefaultAvaliable));

				var optionalDeliveryMethod = (ILekmerDeliveryMethod)_deliveryMethodService.GetOptionalByDeliveryMethod(UserContext.Current, deliveryMethod.Id);
				if (optionalDeliveryMethod != null)
				{
					// When cart contains diapers, optional delivery method is not available
					optionalDeliveryMethod = (ILekmerDeliveryMethod)GetOrderOptionalDeliveryBasedOnItemsType(Cart, optionalDeliveryMethod);
				}
				if (optionalDeliveryMethod != null)
				{
					productsBuilder.Append(MapOptionalDeliveryMethod(optionalDeliveryMethod));
				}
			}

			fragment.AddVariable("Iterate:DeliveryMethod", productsBuilder.ToString, VariableEncoding.None);
		}

		protected virtual string MapDeliveryMethod(ILekmerDeliveryMethod deliveryMethod, bool isDefaultAvaliable)
		{
			Fragment fragment = _template.GetFragment("DeliveryMethod");

			if (DeliveryMethodId == 0)
			{
				if (ProfileDeliveryMethodId > 0)
				{
					// Selected from profile
					DeliveryMethodId = ProfileDeliveryMethodId;
				}
				else if (isDefaultAvaliable)
				{
					if (deliveryMethod.ChannelDefault)
					{
						// Selected by default from channel settings
						DeliveryMethodId = deliveryMethod.Id;
					}
				}
				else
				{
					// Selected first
					DeliveryMethodId = deliveryMethod.Id;
				}
			}

			bool isSelected = DeliveryMethodId == deliveryMethod.Id;

			fragment.AddCondition("DeliveryMethod.IsSelected", isSelected);

			MapDeliveryMethodEntity(fragment, deliveryMethod);

			return fragment.Render();
		}

		protected virtual string MapOptionalDeliveryMethod(ILekmerDeliveryMethod deliveryMethod)
		{
			Fragment fragment = _template.GetFragment("OptionalDeliveryMethod");

			if (OptionalDeliveryMethodId == 0)
			{
				if (ProfileOptionalDeliveryMethodId > 0)
				{
					// Selected from profile
					OptionalDeliveryMethodId = ProfileOptionalDeliveryMethodId;
				}
				else if (deliveryMethod.ChannelDefault)
				{
					// Selected by default from channel ettings
					OptionalDeliveryMethodId = deliveryMethod.Id;
				}
			}

			bool isSelected = OptionalDeliveryMethodId == deliveryMethod.Id;

			fragment.AddCondition("DeliveryMethod.IsSelected", isSelected);

			MapDeliveryMethodEntity(fragment, deliveryMethod);

			return fragment.Render();
		}

		protected virtual string MapDeliveryMethodEntity(Fragment fragment, ILekmerDeliveryMethod deliveryMethod)
		{
			fragment.AddVariable("DeliveryMethod.Id", deliveryMethod.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("DeliveryMethod.Title", deliveryMethod.Title);
			fragment.AddVariable("DeliveryMethod.Alias", AliasHelper.GetAliasValue("Order.Checkout.Delivery.DeliveryMethod_" + deliveryMethod.CommonName), VariableEncoding.None);

			decimal originalFreightCost = CalculateDeliveryFreightCostOriginal(deliveryMethod, Cart);
			decimal? campaignFreightCost = CalculateDeliveryFreightCostCampaign(deliveryMethod, Cart);

			fragment.AddVariable("DeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, originalFreightCost));
			fragment.AddCondition("DeliveryMethod.IsAffectedByCampaign", campaignFreightCost.HasValue);

			if (campaignFreightCost.HasValue)
			{
				fragment.AddVariable("DeliveryMethod.CampaignFreightCost", _formatter.FormatPrice(Channel.Current, campaignFreightCost.Value));
			}

			fragment.AddCondition("DeliveryMethod.IsCompany", deliveryMethod.IsCompany);

			return fragment.Render();
		}

		protected virtual void InitializeProfileDelivery()
		{
			var checkoutSession = IoC.Resolve<ICheckoutSession>();
			ICheckoutProfile checkoutProfile = checkoutSession.Profile;

			if (checkoutProfile == null)
			{
				checkoutSession.InitializeProfile();
				checkoutProfile = checkoutSession.Profile;
			}

			if (checkoutProfile != null)
			{
				if (FormDeliveryMethodId > 0)
				{
					// Save to profile
					checkoutProfile.DeliveryMethodId = FormDeliveryMethodId;
					checkoutProfile.OptionalDeliveryMethodId = FormOptionalDeliveryMethodId;
				}

				if (FormOptionalDeliveryMethodId > 0)
				{
					// Save to profile
					checkoutProfile.OptionalDeliveryMethodId = FormOptionalDeliveryMethodId;
				}

				ProfileDeliveryMethodId = checkoutProfile.DeliveryMethodId;
				ProfileOptionalDeliveryMethodId = checkoutProfile.OptionalDeliveryMethodId;
			}
		}


		// Calculate delivery freight cost

		protected virtual decimal CalculateDeliveryFreightCostActual(ILekmerDeliveryMethod deliveryMethod, ICartFull cart)
		{
			decimal originalFreightCost = CalculateDeliveryFreightCostOriginal(deliveryMethod, cart);
			decimal? campaignFreightCost = CalculateDeliveryFreightCostCampaign(deliveryMethod, cart);

			return campaignFreightCost.HasValue ? campaignFreightCost.Value : originalFreightCost;
		}

		protected virtual decimal CalculateDeliveryFreightCostOriginal(ILekmerDeliveryMethod deliveryMethod, ICartFull cart)
		{
			var freightCost = 0m;

			if (deliveryMethod != null)
			{
				freightCost = deliveryMethod.FreightCost;

				if (deliveryMethod.FreightCostDependOnWeight && cart != null)
				{
					var cartWeight = ((ILekmerCartFull) cart).CalculateCartItemsWeight(UserContext.Current);

					var deliveryMethodWeightPrice = _deliveryMethodService.GetMatchingWeightPrice(deliveryMethod, cartWeight);

					if (deliveryMethodWeightPrice != null)
					{
						freightCost = deliveryMethodWeightPrice.FreightCost;
					}
				}
			}

			return freightCost;
		}

		protected virtual decimal? CalculateDeliveryFreightCostCampaign(ILekmerDeliveryMethod deliveryMethod, ICartFull cart)
		{
			decimal? freightCost = null;

			if (deliveryMethod != null && cart != null)
			{
				if (deliveryMethod.FreightCostDependOnWeight == false) //When freight cost depend on weight - campaigns are not in use !!!
				{
					var campaignInfo = cart.CampaignInfo as ILekmerCartCampaignInfo;
					if (campaignInfo != null && campaignInfo.CampaignFreightCostDictionary.ContainsKey(deliveryMethod.Id))
					{
						freightCost = campaignInfo.CampaignFreightCostDictionary[deliveryMethod.Id];
					}
				}
			}

			return freightCost;
		}

		// Custom delivery logic

		protected virtual void CalculateDeliveryFreightCostBasedOnItemsType(ILekmerDeliveryMethod orderDelivery, ICartFull cart)
		{
			if (CartHelper.DiapersAreSellable(UserContext.Current.Channel) == false)
			{
				return;
			}

			if (orderDelivery.Id != (int)DeliveryMethodType.Post && orderDelivery.Id != (int)DeliveryMethodType.DoorstepDelivery)
			{
				return;
			}

			var lekmerCart = (ILekmerCartFull)cart;

			// When cart contains only diapers - set standard delivery freight to 0m.
			if (CartHelper.ContainsOnlyDiapers(lekmerCart))
			{
				orderDelivery.FreightCost = 0m;
			}
		}

		protected virtual IDeliveryMethod GetOrderOptionalDeliveryBasedOnItemsType(ICartFull cart, ILekmerDeliveryMethod selectedOptionalDelivery)
		{
			if (cart == null)
			{
				return null;
			}

			var lekmerCart = (ILekmerCartFull)cart;

			// When cart contains only diapers - do not use optional delivery.
			if (CartHelper.ContainsOnlyDiapers(lekmerCart))
			{
				return null;
			}

			// When cart contains both diapers and non-diapers - always use optional delivery in DK.
			if (selectedOptionalDelivery == null && CartHelper.HomeDeliveryIsObligatory(Channel.Current) && CartHelper.CheckForMixedItems(lekmerCart))
			{
				var deliveryMethod = (ILekmerDeliveryMethod)_deliveryMethodService.GetById(UserContext.Current, (int)DeliveryMethodType.DoorstepDelivery);
				if (deliveryMethod != null)
				{
					selectedOptionalDelivery = ObjectCopier.Clone(deliveryMethod);
					selectedOptionalDelivery.FreightCost = CalculateDeliveryFreightCostActual(selectedOptionalDelivery, cart);
				}
			}

			return selectedOptionalDelivery;
		}

		protected virtual IDeliveryMethod GetOrderDiapersDeliveryBasedOnItemsType(ICartFull cart)
		{
			if (cart == null)
			{
				return null;
			}

			ILekmerDeliveryMethod orderDiapersDelivery = null;

			if (CartHelper.DiapersAreSellable(UserContext.Current.Channel) == false)
			{
				return null;
			}

			if (DeliveryMethodId != (int)DeliveryMethodType.Post) // Only private persons, ignore companies
			{
				return null;
			}

			var lekmerCart = (ILekmerCartFull)cart;

			if (lekmerCart.ItemsType.IsFlagSet(OrderItemsType.Diapers))
			{
				var deliveryMethod = (ILekmerDeliveryMethod)_deliveryMethodService.GetById(UserContext.Current, (int)DeliveryMethodType.DoorstepDiapers);
				if (deliveryMethod != null)
				{
					orderDiapersDelivery = ObjectCopier.Clone(deliveryMethod);
					orderDiapersDelivery.FreightCost = CalculateDeliveryFreightCostActual(orderDiapersDelivery, cart);
				}
			}

			return orderDiapersDelivery;
		}
	}
}