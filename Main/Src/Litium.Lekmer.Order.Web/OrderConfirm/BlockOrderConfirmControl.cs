using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.Order.Web
{
	public class BlockOrderConfirmControl : BlockControlBase
	{
		private readonly string _httpScheme = "http://";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly ICheckoutSession _checkoutSession;
		private readonly IOrderSession _orderSession;
		private readonly ILekmerOrderService _orderService;
		private readonly ICountryService _countryService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;
		private readonly ILekmerChannelService _channelService;
		private readonly ICartCampaignService _cartCampaignService;
		private readonly IProductCampaignService _productCampaignService;
		private readonly IPaymentTypeService _paymentTypeService;
		private readonly IDeliveryMethodService _deliveryMethodService;
		private readonly IProductService _productService;
		private readonly ICategoryService _categoryService;
		private readonly IMediaUrlService _mediaUrlService;
		private readonly ILekmerFormatter _formatter;

		private string _paymentTypeTitle;
		private string _paymentTypeAlias;

		private string _deliveryMethodId;
		private string _deliveryMethodTitle;
		private string _deliveryMethodAlias;

		private bool _optionalDeliveryMethodIsUsed;
		private string _optionalDeliveryMethodId;
		private string _optionalDeliveryMethodTitle;
		private string _optionalDeliveryMethodAlias;

		private bool _diapersDeliveryMethodIsUsed;
		private string _diapersDeliveryMethodId;
		private string _diapersDeliveryMethodTitle;
		private string _diapersDeliveryMethodAlias;

		private bool _klarnaCheckoutIsUsed;
		private string _klarnaCheckoutSnippet;

		private Collection<IChannel> _channels;
		protected Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = _channelService.GetAll()); }
		}

		public BlockOrderConfirmControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICheckoutSession checkoutSession,
			IOrderSession orderSession,
			IOrderService orderService,
			ICountryService countryService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			IChannelService channelService,
			ICartCampaignService cartCampaignService,
			IProductCampaignService productCampaignService,
			IPaymentTypeService paymentTypeService,
			IDeliveryMethodService deliveryMethodService,
			IProductService productService,
			ICategoryService categoryService,
			IMediaUrlService mediaUrlService,
			IFormatter formatter)
			: base(templateFactory, blockService)
		{
			_checkoutSession = checkoutSession;
			_orderSession = orderSession;
			_orderService = (ILekmerOrderService)orderService;
			_countryService = countryService;
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
			_channelService = (ILekmerChannelService)channelService;
			_cartCampaignService = cartCampaignService;
			_productCampaignService = productCampaignService;
			_paymentTypeService = paymentTypeService;
			_deliveryMethodService = deliveryMethodService;
			_productService = productService;
			_categoryService = categoryService;
			_mediaUrlService = mediaUrlService;
			_formatter = (ILekmerFormatter)formatter;
		}

		protected override BlockContent RenderCore()
		{
			IOrderFull order = FindRegularOrder();
			order = FindKlarnaCheckoutOrder(order);

			if (order == null)
			{
				return RenderNoOrderInSession();
			}

			return RenderBody(order);
		}

		private void RenderHeadAndFooter(ILekmerOrderFull order, out string head, out string footer)
		{
			ICountry country = _countryService.GetById(order.BillingAddress.CountryId);

			Fragment fHead = Template.GetFragment("Head");
			Fragment fFooter = Template.GetFragment("Footer");

			string headOrderItems;
			string footerOrderItems;
			RenderHeadAndFooterOrderItems(order, out headOrderItems, out footerOrderItems);
			fHead.AddVariable("Iterate:HeadOrderItem", headOrderItems, VariableEncoding.None);
			fFooter.AddVariable("Iterate:FooterOrderItem", footerOrderItems, VariableEncoding.None);

			AddVariable("Order.Id.Json", order.Id.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.Number.Json", order.Number, fHead, fFooter);
			AddVariable("Order.Email.Json", order.Email, fHead, fFooter);
			AddVariable("Order.PriceTotal.Json", order.GetActualPriceTotal().IncludingVat.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.PriceTotalexVatexFright.Json", order.GetActualPriceSummary().ExcludingVat.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.PriceItemsSummaryExclVat.Json", order.GetActualPriceSummary().ExcludingVat.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.VatItemsSummary.Json", order.GetActualVatSummary().ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.VatTotal.Json", order.GetActualVatTotal().ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.FreightCost.Json", order.GetActualFreightCost().ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.BillingAddress.City.Json", order.BillingAddress.City.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.BillingAddress.Country.Json", country.Title.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			AddVariable("Order.PaymentType.Title.Json", _paymentTypeTitle, fHead, fFooter);
			AddVariable("Order.PaymentType.Alias.Json", _paymentTypeAlias, fHead, fFooter);
			AddVariable("Order.DeliveryMethod.Id.Json", _deliveryMethodId, fHead, fFooter);
			AddVariable("Order.DeliveryMethod.Title.Json", _deliveryMethodTitle, fHead, fFooter);
			AddVariable("Order.DeliveryMethod.Alias.Json", _deliveryMethodAlias, fHead, fFooter);

			head = fHead.Render();
			footer = fFooter.Render();
		}
		private void RenderHeadAndFooterOrderItems(ILekmerOrderFull order, out string headOrderItems, out string footerOrderItems)
		{
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in order.GetOrderItems())
			{
				string thirdLevelCategoryTitle;
				string secondLevelCategoryTitle;
				string firstLevelCategoryTitle;
				var product = GetCategoriesTitle(orderItem.ProductId, out thirdLevelCategoryTitle, out secondLevelCategoryTitle, out firstLevelCategoryTitle);

				string productAbsoluteUrl;
				string imageUrl;
				GetProduatAndImageUrls(product, out productAbsoluteUrl, out imageUrl);

				var actualPrice = orderItem.ActualPrice;

				Fragment headOrderItem = Template.GetFragment("HeadOrderItem");
				Fragment footerOrderItem = Template.GetFragment("FooterOrderItem");

				AddVariable("OrderItem.ErpId.Json", orderItem.ErpId, headOrderItem, footerOrderItem);
				AddVariable("OrderItem.Title.Json", orderItem.Title, headOrderItem, footerOrderItem);
				AddVariable("OrderItem.Price.Json", actualPrice.IncludingVat.ToString(CultureInfo.InvariantCulture), headOrderItem, footerOrderItem);
				AddVariable("OrderItem.PriceExclVat.Json", actualPrice.ExcludingVat.ToString(CultureInfo.InvariantCulture), headOrderItem, footerOrderItem);
				AddVariable("OrderItem.Quantity.Json", orderItem.Quantity.ToString(CultureInfo.InvariantCulture), headOrderItem, footerOrderItem);
				AddVariable("OrderItem.ThirdLevelCategoryTitle.Json", thirdLevelCategoryTitle, headOrderItem, footerOrderItem);
				AddVariable("OrderItem.SecondLevelCategoryTitle.Json", secondLevelCategoryTitle, headOrderItem, footerOrderItem);
				AddVariable("OrderItem.FirstLevelCategoryTitle.Json", firstLevelCategoryTitle, headOrderItem, footerOrderItem);
				AddVariable("OrderItem.Product.Url.Json", productAbsoluteUrl, headOrderItem, footerOrderItem);
				AddVariable("OrderItem.Product.ImageUrl.Json", imageUrl, headOrderItem, footerOrderItem);

				headItemBuilder.AppendLine(headOrderItem.Render());
				footerItemBuilder.AppendLine(footerOrderItem.Render());
			}

			headOrderItems = headItemBuilder.ToString();
			footerOrderItems = footerItemBuilder.ToString();
		}
		public void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}

		private BlockContent RenderBody(IOrderFull order)
		{
			var lekmerOrderFull = (ILekmerOrderFull)order;

			var orderItems = order.GetOrderItems();

			Fragment fragmentContent = Template.GetFragment("Content");

			// Klarna Checkout
			fragmentContent.AddVariable("KlarnaCheckoutConfirmation", RenderKlarnaCheckoutConfirmation(), VariableEncoding.None);
			fragmentContent.AddCondition("KlarnaConfirmation.IsUsed", _klarnaCheckoutIsUsed);

			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(orderItems), VariableEncoding.None);
			fragmentContent.AddEntity(order);

			Collection<IGiftCardViaMailInfo> giftCardViaMailInfoCollection = _giftCardViaMailInfoService.GetAllByOrder(order.Id);
			if (giftCardViaMailInfoCollection != null && giftCardViaMailInfoCollection.Count > 0)
			{
				fragmentContent.AddCondition("IsGiftCardWillBeSent", true);
				fragmentContent.AddVariable("Iterate:GiftCardViaEmail", RenderGiftCardsViaEmailList(giftCardViaMailInfoCollection), VariableEncoding.None);
			}
			else
			{
				fragmentContent.AddCondition("IsGiftCardWillBeSent", false);
			}

			// Payment type.
			GetPaymentTypeVariables(order);
			fragmentContent.AddVariable("Order.PaymentType.Title", _paymentTypeTitle);
			fragmentContent.AddVariable("Order.PaymentType.Alias", _paymentTypeAlias, VariableEncoding.None);

			// Delivery method.
			GetDeliveryMethodVariables(order.DeliveryMethodId);

			fragmentContent.AddVariable("Order.DeliveryMethod.Id", _deliveryMethodId);
			fragmentContent.AddVariable("Order.DeliveryMethod.Title", _deliveryMethodTitle);
			fragmentContent.AddVariable("Order.DeliveryMethod.Alias", _deliveryMethodAlias, VariableEncoding.None);

			// Optional delivery method.
			GetOptionalDeliveryMethodVariables(lekmerOrderFull.OptionalDeliveryMethodId);

			fragmentContent.AddCondition("Order.OptionalDeliveryMethod.IsUsed", _optionalDeliveryMethodIsUsed);
			fragmentContent.AddVariable("Order.OptionalDeliveryMethod.Id", _optionalDeliveryMethodId);
			fragmentContent.AddVariable("Order.OptionalDeliveryMethod.Title", _optionalDeliveryMethodTitle);
			fragmentContent.AddVariable("Order.OptionalDeliveryMethod.Alias", _optionalDeliveryMethodAlias, VariableEncoding.None);

			// Diapers delivery method.
			GetDiapersDeliveryMethodVariables(lekmerOrderFull.DiapersDeliveryMethodId);

			fragmentContent.AddCondition("Order.DiapersDeliveryMethod.IsUsed", _diapersDeliveryMethodIsUsed);
			fragmentContent.AddVariable("Order.DiapersDeliveryMethod.Id", _diapersDeliveryMethodId);
			fragmentContent.AddVariable("Order.DiapersDeliveryMethod.Title", _diapersDeliveryMethodTitle);
			fragmentContent.AddVariable("Order.DiapersDeliveryMethod.Alias", _diapersDeliveryMethodAlias, VariableEncoding.None);

			fragmentContent.AddCondition("Order.IsProductAbove60LExist", lekmerOrderFull.IsProductAbove60LExist(UserContext.Current, orderItems));

			fragmentContent.AddCondition("Order.ContainsMixedItems", lekmerOrderFull.CheckForMixedItems(UserContext.Current, orderItems));
			fragmentContent.AddCondition("Order.HasDropShipItems", lekmerOrderFull.IsDropShipItemExist(orderItems));

			string head;
			string footer;
			RenderHeadAndFooter(lekmerOrderFull, out head, out footer);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderOrderItemList(IEnumerable<IOrderItem> orderItems)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in orderItems)
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddEntity(orderItem);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

		private string RenderGiftCardsViaEmailList(IEnumerable<IGiftCardViaMailInfo> giftCardViaMailInfoCollection)
		{
			var itemBuilder = new StringBuilder();

			foreach (IGiftCardViaMailInfo giftCardViaMailInfo in giftCardViaMailInfoCollection)
			{
				Fragment giftCardViaEmail = Template.GetFragment("GiftCardViaEmail");

				IChannel channel = GetChannel(giftCardViaMailInfo);

				giftCardViaEmail.AddVariable("GiftCard.DiscountAmount", _formatter.FormatPriceChannelOrLessDecimals(channel, giftCardViaMailInfo.DiscountValue));
				giftCardViaEmail.AddVariable("GiftCard.DateToSend", _formatter.FormatShortDate(channel, giftCardViaMailInfo.DateToSend));
				giftCardViaEmail.AddVariable("GiftCard.Currency", channel.Currency.Iso);
				giftCardViaEmail.AddVariable("GiftCard.CampaignTitle", GetCampaignTitle(giftCardViaMailInfo));

				itemBuilder.AppendLine(giftCardViaEmail.Render());
			}

			return itemBuilder.ToString();
		}

		private IChannel GetChannel(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			var channel = Channels.FirstOrDefault(c => c.Id == giftCardViaMailInfo.ChannelId);
			return channel;
		}

		private string GetCampaignTitle(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			string title = string.Empty;
			
			var cartCampaign = _cartCampaignService.GetById(UserContext.Current, giftCardViaMailInfo.CampaignId);
			if (cartCampaign != null)
			{
				title = cartCampaign.Title;
			}
			else
			{
				var productCampaign = _productCampaignService.GetById(UserContext.Current, giftCardViaMailInfo.CampaignId);
				if (productCampaign != null)
				{
					title = productCampaign.Title;
				}
			}

			return title;
		}

		private IProduct GetCategoriesTitle(int productId, out string thirdLevelCategoryTitle, out string secondLevelCategoryTitle, out string firstLevelCategoryTitle)
		{
			thirdLevelCategoryTitle = string.Empty;
			secondLevelCategoryTitle = string.Empty;
			firstLevelCategoryTitle = string.Empty;

			var product = _productService.GetById(UserContext.Current, productId);
			var categoryTree = _categoryService.GetAllAsTree(UserContext.Current);
			if (product != null)
			{
				ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(product.CategoryId);
				if (categoryTreeItem != null && categoryTreeItem.Category != null)
				{
					thirdLevelCategoryTitle = categoryTreeItem.Category.Title;

					categoryTreeItem = categoryTreeItem.Parent;
					if (categoryTreeItem != null && categoryTreeItem.Category != null)
					{
						secondLevelCategoryTitle = categoryTreeItem.Category.Title;

						categoryTreeItem = categoryTreeItem.Parent;
						if (categoryTreeItem != null && categoryTreeItem.Category != null)
						{
							firstLevelCategoryTitle = categoryTreeItem.Category.Title;
						}
					}
				}
			}

			return product;
		}

		private IOrderFull FindRegularOrder()
		{
			int? orderId = _orderSession.OrderId;
			if (orderId.HasValue)
			{
				IOrderFull order = _orderService.GetFullById(UserContext.Current, orderId.Value);
				return order;
			}

			return null;
		}

		private IOrderFull FindKlarnaCheckoutOrder(IOrderFull orderRegular)
		{
			Uri checkoutId = _checkoutSession.KlarnaCheckoutId;
			if (checkoutId != null)
			{
				string confirmationContent = string.Empty;
				try
				{
					confirmationContent = KlarnaFactory.CreateKlarnaCheckoutClient(UserContext.Current.Channel.CommonName).GetKlarnaConfirmationContent(checkoutId);
				}
				catch (Exception ex)
				{
					_log.Error("Error occurred while getting Klarna confirmation content for 'klarna_order' url = " + checkoutId, ex);
				}

				// Klarna order has correct status
				if (confirmationContent.HasValue())
				{
					ClearSession();

					_klarnaCheckoutIsUsed = true;
					_klarnaCheckoutSnippet = confirmationContent;

					List<string> splittedUri = checkoutId.ToString().Split('/').ToList();
					string orderKlarnaId = splittedUri.Last();
					IOrderFull orderKlarna = _orderService.GetFullByKcoId(UserContext.Current, orderKlarnaId);

					return orderKlarna;
				}
			}

			return orderRegular;
		}

		private string RenderKlarnaCheckoutConfirmation()
		{
			Fragment fragment = Template.GetFragment("KlarnaCheckoutConfirmation");

			fragment.AddCondition("KlarnaConfirmation.IsUsed", _klarnaCheckoutIsUsed);
			fragment.AddVariable("KlarnaConfirmationSnippet", _klarnaCheckoutSnippet, VariableEncoding.None);

			return fragment.Render();
		}

		private BlockContent RenderNoOrderInSession()
		{
			return new BlockContent(Template.GetFragment("NoOrderInSessionHead").Render(), Template.GetFragment("NoOrderInSession").Render());
		}

		private void GetPaymentTypeVariables(IOrderFull order)
		{
			_paymentTypeTitle = string.Empty;
			_paymentTypeAlias = string.Empty;

			if (order.Payments.Count > 0)
			{
				IPaymentType paymentType = _paymentTypeService.GetAll(UserContext.Current).FirstOrDefault(pt => pt.Id == order.Payments[0].PaymentTypeId);
				if (paymentType != null)
				{
					_paymentTypeTitle = paymentType.Title;
					_paymentTypeAlias = AliasHelper.GetAliasValue("Order.Checkout.OrderConfirmation.PaymentType_" + paymentType.CommonName);
				}
			}
		}

		private void GetDeliveryMethodVariables(int deliveryMethodId)
		{
			var deliveryMethod = _deliveryMethodService.GetById(UserContext.Current, deliveryMethodId);

			_deliveryMethodId = deliveryMethodId.ToString(CultureInfo.InvariantCulture);

			if (deliveryMethod != null)
			{
				_deliveryMethodTitle = deliveryMethod.Title;
				_deliveryMethodAlias = AliasHelper.GetAliasValue("Order.Checkout.OrderConfirmation.DeliveryMethod_" + deliveryMethod.CommonName);
			}
			else
			{
				_deliveryMethodTitle = string.Empty;
				_deliveryMethodAlias = string.Empty;
			}
		}

		private void GetOptionalDeliveryMethodVariables(int? deliveryMethodId)
		{
			_optionalDeliveryMethodIsUsed = false;
			_optionalDeliveryMethodId = string.Empty;
			_optionalDeliveryMethodTitle = string.Empty;
			_optionalDeliveryMethodAlias = string.Empty;

			if (deliveryMethodId.HasValue)
			{
				_optionalDeliveryMethodIsUsed = true;

				var deliveryMethod = _deliveryMethodService.GetById(UserContext.Current, deliveryMethodId.Value);

				_optionalDeliveryMethodId = deliveryMethodId.Value.ToString(CultureInfo.InvariantCulture);

				if (deliveryMethod != null)
				{
					_optionalDeliveryMethodTitle = deliveryMethod.Title;
					_optionalDeliveryMethodAlias = AliasHelper.GetAliasValue("Order.Checkout.OrderConfirmation.DeliveryMethod_" + deliveryMethod.CommonName);
				}
			}
		}

		private void GetDiapersDeliveryMethodVariables(int? deliveryMethodId)
		{
			_diapersDeliveryMethodIsUsed = false;
			_diapersDeliveryMethodId = string.Empty;
			_diapersDeliveryMethodTitle = string.Empty;
			_diapersDeliveryMethodAlias = string.Empty;

			if (deliveryMethodId.HasValue)
			{
				_diapersDeliveryMethodIsUsed = true;

				var deliveryMethod = _deliveryMethodService.GetById(UserContext.Current, deliveryMethodId.Value);

				_diapersDeliveryMethodId = deliveryMethodId.Value.ToString(CultureInfo.InvariantCulture);

				if (deliveryMethod != null)
				{
					_diapersDeliveryMethodTitle = deliveryMethod.Title;
					_diapersDeliveryMethodAlias = AliasHelper.GetAliasValue("Order.Checkout.OrderConfirmation.DeliveryMethod_" + deliveryMethod.CommonName);
				}
			}
		}

		private void ClearSession()
		{
			IoC.Resolve<ICartCookieService>().DeleteShoppingCardGuid();
			IoC.Resolve<ICartSession>().Cart = null;

			_checkoutSession.KlarnaCheckoutId = null;
			_checkoutSession.CleanProfile();

			IoC.Resolve<IVoucherSession>().Voucher = null;
			IoC.Resolve<IUserContextFactory>().FlushCache();
		}

		private void GetProduatAndImageUrls(IProduct product, out string productAbsoluteUrl, out string imageUrl)
		{
			productAbsoluteUrl = string.Empty;
			imageUrl = string.Empty;

			if (product != null)
			{
				var lekmerProduct = (ILekmerProduct)product;
				productAbsoluteUrl = UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl);

				var mediaUrl = _mediaUrlService.ResolveMediaArchiveExternalUrl(UserContext.Current.Channel);
				var imageLink = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, product.Image, product.DisplayTitle);
				imageUrl = PrepareImageLink(imageLink);
			}
		}
		private string PrepareImageLink(string imageLink)
		{
			if (imageLink.StartsWith("//"))
			{
				imageLink = imageLink.Substring(2);
			}
			else if (imageLink.StartsWith(_httpScheme))
			{
				return imageLink;
			}

			imageLink = _httpScheme + imageLink;
			return imageLink;
		}
	}
}