﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutCompanyForm : ControlBase, ILekmerCheckoutCompanyForm
	{
		public string IsCompanyModeFormName
		{
			get { return "checkout-iscompany"; }
		}

		public bool IsCompanyMode { get; set; }

		public bool RequestHasCompanyMode
		{
			get { return Request.Form[IsCompanyModeFormName].HasValue(); }
		}

		public virtual void MapFromRequestToForm()
		{
			IsCompanyMode = !string.IsNullOrEmpty(Request.Form[IsCompanyModeFormName]) && bool.Parse(Request.Form[IsCompanyModeFormName]);
		}

		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Checkout.IsCompanyMode.Name", IsCompanyModeFormName);
		}

		public virtual void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Checkout.IsCompanyMode.Value", IsCompanyMode.ToString().ToLower());
		}

		public virtual void MapFromFormToProfile(ICheckoutProfile checkoutProfile)
		{
			if (checkoutProfile == null) throw new ArgumentNullException("checkoutProfile");

			checkoutProfile.IsCompany = IsCompanyMode;
		}

		public virtual void MapFromProfileToForm(ICheckoutProfile checkoutProfile)
		{
			if (checkoutProfile == null) throw new ArgumentNullException("checkoutProfile");

			IsCompanyMode = checkoutProfile.IsCompany;
		}
	}
}