﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Qliro;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	/// <summary>
	/// Payment control - Qliro logic
	/// </summary>
	public partial class Payment
	{
		// Properties.
		private IQliroPaymentTypeService _qliroPaymentTypeService;
		protected IQliroPaymentTypeService QliroPaymentTypeService
		{
			get { return _qliroPaymentTypeService ?? (_qliroPaymentTypeService = IoC.Resolve<IQliroPaymentTypeService>()); }
		}

		private Collection<IQliroPaymentType> _availableQliroPaymentTypes;
		protected Collection<IQliroPaymentType> AvailableQliroPaymentTypes
		{
			get { return _availableQliroPaymentTypes ?? (_availableQliroPaymentTypes = QliroPaymentTypeService.GetAllByChannel(UserContext.Current.Channel)); }
		}

		private IQliroClient _qliroClient;
		protected IQliroClient QliroClient
		{
			get { return _qliroClient ?? (_qliroClient = QliroFactory.CreateQliroClient(UserContext.Current.Channel.CommonName)); }
		}

		private IQliroCalculation _qliroCalculation;
		protected IQliroCalculation QliroCalculation
		{
			get { return _qliroCalculation ?? (_qliroCalculation = IoC.Resolve<IQliroCalculation>()); }
		}

		protected string ProfileQliroPartPaymentId { get; set; }

		public string ActiveQliroPartPaymentId { get; private set; }

		public bool IsQliroPaymentActive
		{
			get
			{
				var paymentMethodType = (PaymentMethodType)ActivePaymentTypeId;

				switch (paymentMethodType)
				{
					case PaymentMethodType.QliroInvoice:
					case PaymentMethodType.QliroPartPayment:
					case PaymentMethodType.QliroSpecialPartPayment:
					case PaymentMethodType.QliroCompanyInvoice:
						return true;
				}

				return false;
			}
		}


		// Methods
		protected virtual void InitializeActiveQliroPartPayment()
		{
			if (PaymentTypeHelper.IsQliroPartPayment(ActivePaymentTypeId) == false) // just for storing selected option in profile
			{
				if (PaymentForm.QliroPartPaymentId.HasValue())
				{
					ActiveQliroPartPaymentId = PaymentForm.QliroPartPaymentId;
				}
				return;
			}

			string qliroPartPaymentId = PaymentForm.QliroPartPaymentId;
			bool paymentTypeIdIsValid = IsValidQliroPartPaymentType(ActivePaymentTypeId, qliroPartPaymentId);

			if (paymentTypeIdIsValid == false)
			{
				qliroPartPaymentId = ProfileQliroPartPaymentId;
				paymentTypeIdIsValid = IsValidQliroPartPaymentType(ActivePaymentTypeId, qliroPartPaymentId);
			}

			if (paymentTypeIdIsValid) // == true
			{
				ActiveQliroPartPaymentId = qliroPartPaymentId;
			}
		}


		// Render
		protected virtual void RenderPaymentTypeExtensionForQliroAll(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isQliroPayment = PaymentTypeHelper.IsQliroPayment(paymentTypeId);
			bool isQliroPaymentAndSelected = paymentTypeIsSelected && isQliroPayment;

			fragment.AddCondition("isQliroPayment", isQliroPayment);
			fragment.AddCondition("isQliroPaymentAndSelected", isQliroPaymentAndSelected);

			if (isQliroPayment)
			{
				decimal lowestMonthlyFee = 0; // (decimal)GetLowestMonthlyFee(paymentTypeId);
				fragment.AddVariable("Qliro.InvoiceCharge", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, lowestMonthlyFee));

				var qliroSetting = QliroClient.QliroSetting;
				fragment.AddVariable("Qliro.ClientRef", qliroSetting.ClientRef.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);
			}
		}

		protected virtual void RenderPaymentTypeExtensionForQliroPersonInvoice(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isQliroPersonInvoice = PaymentTypeHelper.IsQliroPersonInvoicePayment(paymentTypeId);
			bool isQliroPersonInvoiceAndSelected = paymentTypeIsSelected && isQliroPersonInvoice;

			fragment.AddCondition("isQliroInvoice", isQliroPersonInvoice);
			fragment.AddCondition("isQliroInvoiceAndSelected", isQliroPersonInvoiceAndSelected);
		}

		protected virtual void RenderPaymentTypeExtensionForQliroPartPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isQliroPartPayment = PaymentTypeHelper.IsQliroPartPayment(paymentTypeId);
			bool isQliroPartPaymentAndSelected = paymentTypeIsSelected && isQliroPartPayment;

			string qliroPartPaymentContent = string.Empty;
			if (isQliroPartPaymentAndSelected)
			{
				qliroPartPaymentContent = RenderQliroPartPaymentAlternativ(paymentTypeId);
			}

			fragment.AddCondition("isQliroPartPayment", isQliroPartPayment);
			fragment.AddCondition("isQliroPartPaymentAndSelected", isQliroPartPaymentAndSelected);

			fragment.AddVariable("Iterate:QliroPartPayment", qliroPartPaymentContent, VariableEncoding.None);
		}

		protected virtual void RenderPaymentTypeExtensionForQliroSpecialPartPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isQliroSpecialPartPayment = PaymentTypeHelper.IsQliroSpecialPartPayment(paymentTypeId);
			bool isQliroSpecialPartPaymentAndSelected = paymentTypeIsSelected && isQliroSpecialPartPayment;

			fragment.AddCondition("isQliroSpecialPartPayment", isQliroSpecialPartPayment);
			fragment.AddCondition("isQliroSpecialPartPaymentAndSelected", isQliroSpecialPartPaymentAndSelected);

			string qliroSpecialPartPaymentId = "none";
			if (isQliroSpecialPartPayment)
			{
				string specialPartPaymentId = GetQliroSpecialPartPaymentId(OrderTotalSum);
				if (specialPartPaymentId.HasValue())
				{
					qliroSpecialPartPaymentId = specialPartPaymentId;
				}
			}

			fragment.AddVariable("Qliro.SpecialPartPayment.Id", qliroSpecialPartPaymentId);
		}

		protected virtual void RenderPaymentTypeExtensionForQliroCompanyInvoice(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isQliroCompanyInvoice = PaymentTypeHelper.IsQliroCompanyInvoicePayment(paymentTypeId);
			bool isQliroCompanyInvoiceAndSelected = paymentTypeIsSelected && isQliroCompanyInvoice;

			fragment.AddCondition("isQliroCompanyInvoice", isQliroCompanyInvoice);
			fragment.AddCondition("isQliroCompanyInvoiceAndSelected", isQliroCompanyInvoiceAndSelected);
		}


		/// <summary>
		/// Gets the Qliro special part payment type code.
		/// </summary>
		protected virtual string GetQliroSpecialPartPaymentId(double totalOrderCost)
		{
			if (totalOrderCost <= 0)
			{
				totalOrderCost = 1;
			}

			string specialPaymentTypeCode = QliroClient.QliroSetting.SpecialPaymentTypeCode;

			IQliroPaymentType qliroPaymentType = AvailableQliroPaymentTypes
				.Where(pt => pt.MinPurchaseAmount <= (decimal)totalOrderCost)
				.Where(pt => pt.InterestCalculation != QliroInterestCalculationType.Annuity)
				.FirstOrDefault(pt => pt.Code.Equals(specialPaymentTypeCode, StringComparison.InvariantCultureIgnoreCase));

			if (qliroPaymentType != null)
			{
				return qliroPaymentType.Code;
			}

			return null;
		}


		/// <summary>
		/// List the part payment alternatives.
		/// </summary>
		/// <param name="paymentTypeId"> </param>
		protected virtual string RenderQliroPartPaymentAlternativ(int paymentTypeId)
		{
			decimal totalOrderCost = (decimal)OrderTotalSum;
			var partPaymentBuilder = new StringBuilder();

			foreach (IQliroPaymentType qliroPaymentType in AvailableQliroPaymentTypes)
			{
				if (qliroPaymentType.Code == "ACCOUNT" && totalOrderCost >= qliroPaymentType.MinPurchaseAmount)
				{
					decimal monthlyCost = QliroCalculation.CalculateMonthlyCost(totalOrderCost, qliroPaymentType);

					string description = AliasHelper.GetAliasValue("Order.Checkout.Payment.Qliro.PartPaymentAccount").Replace("x", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyCost));

					partPaymentBuilder.Append(RenderQliroPartPaymentSubType(description, qliroPaymentType.Code));
				}
			}

			foreach (IQliroPaymentType qliroPaymentType in AvailableQliroPaymentTypes)
			{
				if (qliroPaymentType.Code != "ACCOUNT" && totalOrderCost >= qliroPaymentType.MinPurchaseAmount)
				{
					string description;
					decimal monthlyCost = QliroCalculation.CalculateMonthlyCost(totalOrderCost, qliroPaymentType);

					if (qliroPaymentType.InterestCalculation == QliroInterestCalculationType.Annuity)
					{
						description = AliasHelper.GetAliasValue("Order.Checkout.Payment.Qliro.PartPayment").Replace("x", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyCost));
						description = description.Replace("y", qliroPaymentType.NoOfMonths.ToString(CultureInfo.InvariantCulture));
					}
					else
					{
						continue;
					}

					partPaymentBuilder.Append(RenderQliroPartPaymentSubType(description, qliroPaymentType.Code));
				}
			}

			return partPaymentBuilder.ToString();
		}

		protected virtual string RenderQliroPartPaymentSubType(string description, string qliroPaymentTypeId)
		{
			bool selected = ActiveQliroPartPaymentId == qliroPaymentTypeId;

			Fragment fragmentPartPayment = _template.GetFragment("PartPayment");

			fragmentPartPayment.AddCondition("IsSelected", selected);
			fragmentPartPayment.AddVariable("PartPayment.Title", description, VariableEncoding.None);
			fragmentPartPayment.AddVariable("PartPayment.Id", qliroPaymentTypeId);

			return fragmentPartPayment.Render();
		}


		// Validation
		protected virtual bool IsValidQliroPartPaymentType(int paymentTypeId, string partPaymentTypeId)
		{
			bool isPaymentValid = false;

			if (partPaymentTypeId.HasValue())
			{
				isPaymentValid = AvailableQliroPaymentTypes.Any(p => p.Code == partPaymentTypeId);
			}

			return isPaymentValid;
		}
	}
}