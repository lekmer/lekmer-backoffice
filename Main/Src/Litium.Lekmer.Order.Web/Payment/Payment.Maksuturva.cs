﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	/// <summary>
	/// Payment control - Maksuturva logic
	/// </summary>
	public partial class Payment
	{
		// Properties.
		private ISubPaymentTypeService _subPaymentTypeService;
		protected ISubPaymentTypeService SubPaymentTypeService
		{
			get { return _subPaymentTypeService ?? (_subPaymentTypeService = IoC.Resolve<ISubPaymentTypeService>()); }
		}

		private Collection<ISubPaymentType> _availableMaksuturvaSubPaymentTypes;
		protected Collection<ISubPaymentType> AvailableMaksuturvaSubPaymentTypes
		{
			get { return _availableMaksuturvaSubPaymentTypes ?? (_availableMaksuturvaSubPaymentTypes = SubPaymentTypeService.GetByPayment((int)PaymentMethodType.Maksuturva)); }
		}

		protected string ProfileMaksuturvaSubPaymentId { get; set; }

		public string ActiveMaksuturvaSubPaymentId { get; private set; }

		// Methods
		protected virtual void InitializeActiveMaksuturvaSubPayment()
		{
			if (PaymentTypeHelper.IsMaksuturvaPayment(ActivePaymentTypeId) == false)
			{
				if (PaymentForm.MaksuturvaSubPaymentId.HasValue()) // just for storing selected option in profile
				{
					ActiveMaksuturvaSubPaymentId = PaymentForm.MaksuturvaSubPaymentId;
				}
				return;
			}

			string maksuturvaSubPaymentId = PaymentForm.MaksuturvaSubPaymentId;
			bool paymentTypeIdIsValid = IsValidMaksuturvaSubPayment(maksuturvaSubPaymentId);

			if (paymentTypeIdIsValid == false)
			{
				maksuturvaSubPaymentId = ProfileMaksuturvaSubPaymentId;
				paymentTypeIdIsValid = IsValidMaksuturvaSubPayment(maksuturvaSubPaymentId);
			}

			if (paymentTypeIdIsValid) // == true
			{
				ActiveMaksuturvaSubPaymentId = maksuturvaSubPaymentId;
			}
		}

		// Render
		protected virtual void RenderPaymentTypeExtensionForMaksuturvaPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isMaksuturvaPayment = PaymentTypeHelper.IsMaksuturvaPayment(paymentTypeId);
			bool isMaksuturvaPaymentAndSelected = paymentTypeIsSelected && isMaksuturvaPayment;

			string maksuturvaPaymentContent = string.Empty;
			if (isMaksuturvaPaymentAndSelected)
			{
				maksuturvaPaymentContent = RenderMaksuturvaSubPaymentList();
			}

			fragment.AddCondition("isMaksuturvaPayment", isMaksuturvaPayment);
			fragment.AddCondition("isMaksuturvaPaymentAndSelected", isMaksuturvaPaymentAndSelected);

			fragment.AddVariable("Iterate:MaksuturvaSubPayment", maksuturvaPaymentContent, VariableEncoding.None);
		}

		/// <summary>
		/// List the maksuturva sub payments.
		/// </summary>
		protected virtual string RenderMaksuturvaSubPaymentList()
		{
			var subPaymentsBuilder = new StringBuilder();
			foreach (var subPaymentType in AvailableMaksuturvaSubPaymentTypes)
			{
				subPaymentsBuilder.Append(RenderMaksuturvaSubPayment(subPaymentType));
			}

			return subPaymentsBuilder.ToString();
		}

		protected virtual string RenderMaksuturvaSubPayment(ISubPaymentType subPaymentType)
		{
			bool selected = ActiveMaksuturvaSubPaymentId == subPaymentType.Code;

			Fragment fragmentMaksuturvaSubPayment = _template.GetFragment("MaksuturvaSubPayment");

			fragmentMaksuturvaSubPayment.AddCondition("IsSelected", selected);
			fragmentMaksuturvaSubPayment.AddVariable("MaksuturvaSubPayment.Title", AliasHelper.GetAliasValue("Order.Checkout.Payment.SubPaymentType_" + subPaymentType.CommonName), VariableEncoding.None);
			fragmentMaksuturvaSubPayment.AddVariable("MaksuturvaSubPayment.Id", subPaymentType.Code);

			return fragmentMaksuturvaSubPayment.Render();
		}


		// Validation
		protected virtual bool IsValidMaksuturvaSubPayment(string maksuturvaSubPaymentId)
		{
			bool isPaymentValid = false;

			if (maksuturvaSubPaymentId.HasValue())
			{
				isPaymentValid = AvailableMaksuturvaSubPaymentTypes.Any(p => p.Code == maksuturvaSubPaymentId);
			}

			return isPaymentValid;
		}
	}
}