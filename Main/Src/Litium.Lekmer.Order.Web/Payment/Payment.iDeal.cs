﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	/// <summary>
	/// Payment control - iDeal logic
	/// </summary>
	public partial class Payment
	{
		// Render
		protected virtual void RenderPaymentTypeExtensionForiDealPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isDibsiDealPayment = PaymentTypeHelper.IsDibsiDealPayment(paymentTypeId);
			bool isDibsiDealPaymentAndSelected = paymentTypeIsSelected && isDibsiDealPayment;

			fragment.AddCondition("isDibsiDealPayment", isDibsiDealPayment);
			fragment.AddCondition("isDibsiDealPaymentAndSelected", isDibsiDealPaymentAndSelected);
		}
	}
}