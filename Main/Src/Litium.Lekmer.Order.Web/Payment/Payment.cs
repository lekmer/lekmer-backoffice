﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Klarna.Core;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Collector;
using Litium.Lekmer.Payment.Qliro;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public partial class Payment : ControlBase
	{
		public class PaymentTypeSelectedEventArgs : EventArgs
		{
			public int PaymentTypeId { get; private set; }

			public PaymentTypeSelectedEventArgs(int paymentTypeId)
			{
				PaymentTypeId = paymentTypeId;
			}
		}

		// Events.
		public event EventHandler<PaymentTypeSelectedEventArgs> PaymentTypeSelected;

		// Members.
		private ITemplateFactory _templateFactory;
		private Template _template;

		// Properties.
		private IPaymentTypeService _paymentTypeService;
		protected IPaymentTypeService PaymentTypeService
		{
			get { return _paymentTypeService ?? (_paymentTypeService = IoC.Resolve<IPaymentTypeService>()); }
		}

		private Collection<IPaymentType> _availablePaymentTypes;
		protected Collection<IPaymentType> AvailablePaymentTypes
		{
			get { return _availablePaymentTypes ?? (_availablePaymentTypes = PaymentTypeService.GetAll(UserContext.Current)); }
		}

		private ILekmerFormatter _formatter;
		protected ILekmerFormatter Formatter
		{
			get { return _formatter ?? (_formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>()); }
		}

		protected string ModelCommonName
		{
			get { return "PaymentType"; }
		}

		private IPaymentForm _paymentForm;
		public IPaymentForm PaymentForm
		{
			get { return _paymentForm ?? (_paymentForm = new PaymentForm()); }
		}

		private ILekmerCheckoutCompanyForm _companyForm;
		public ILekmerCheckoutCompanyForm CompanyForm
		{
			get { return _companyForm ?? (_companyForm = new LekmerCheckoutCompanyForm()); }
		}

		private ICheckoutSession _checkoutSession;
		public ICheckoutSession CheckoutSession
		{
			get { return _checkoutSession ?? (_checkoutSession = IoC.Resolve<ICheckoutSession>()); }
		}

		private ICheckoutProfile _checkoutProfile;
		public ICheckoutProfile CheckoutProfile
		{
			get
			{
				if (_checkoutProfile == null)
				{
					_checkoutProfile = CheckoutSession.Profile;
				}
				if (_checkoutProfile == null)
				{
					CheckoutSession.InitializeProfile();
					_checkoutProfile = CheckoutSession.Profile;
				}

				return _checkoutProfile;
			}
		}

		protected int DefaultPersonPaymentId { get; set; }
		protected int DefaultCompanyPaymentId { get; set; }

		protected int ProfilePaymentTypeId { get; set; }

		protected bool IsCompany { get; set; }
		protected bool IsPerson
		{
			get { return !IsCompany; }
		}

		public double OrderTotalSum { get; set; }

		public int ActivePaymentTypeId { get; private set; }

		public bool IsCreditCardPaymentActive
		{
			get
			{
				return ActivePaymentTypeId == (int)PaymentMethodType.DibsCreditCard;
			}
		}

		public bool IsiDealPaymentActive
		{
			get
			{
				return ActivePaymentTypeId == (int)PaymentMethodType.DibsiDeal;
			}
		}


		// Methods
		public virtual void Initialize()
		{
			_templateFactory = IoC.Resolve<ITemplateFactory>();
			_template = _templateFactory.Create(ModelCommonName);

			InitializeDefaultPayment();
			InitializeProfilePayment();
			InitializePersonCompany();

			PaymentForm.MapFromRequest();

			InitializeActivePayment();
			InitializeActiveKlarnaPartPayment();
			InitializeActiveMaksuturvaSubPayment();
			InitializeActiveQliroPartPayment();
			InitializeActiveCollectorPartPayment();

			OnPaymentTypeSelected();

			UpdateProfilePayment();
		}

		public virtual string Render()
		{
			Fragment fragment = _template.GetFragment("PaymentTypeList");

			if (AvailablePaymentTypes.Count > 0)
			{
				var paymentTypesBuilder = new StringBuilder();

				int index = 0;
				foreach (IPaymentType paymentType in AvailablePaymentTypes)
				{
					string paymentTypeContent = RenderPaymentType(paymentType, index++, AvailablePaymentTypes.Count);

					paymentTypesBuilder.Append(paymentTypeContent);
				}

				fragment.AddVariable("Iterate:PaymentType", paymentTypesBuilder.ToString(), VariableEncoding.None);

				bool klarnaCheckoutIsAvailable = AvailablePaymentTypes.Any(p => PaymentTypeHelper.IsKlarnaCheckoutPayment(p.Id));
				fragment.AddCondition("KlarnaCheckout.IsAvailable", klarnaCheckoutIsAvailable);
				fragment.AddCondition("KlarnaCheckout.IsSelected", PaymentTypeHelper.IsKlarnaCheckoutPayment(ActivePaymentTypeId));
			}

			PaymentForm.MapFieldNamesToFragment(fragment);

			return fragment.Render();
		}

		public virtual Collection<IOrderPayment> GetOrderPayments(IOrderFull order)
		{
			decimal priceIncludingVat = order.GetActualPriceSummary().IncludingVat;
			decimal vatSummary = order.GetActualVatSummary();
			return GetOrderPayments(priceIncludingVat, vatSummary);
		}
		private Collection<IOrderPayment> GetOrderPayments(decimal priceIncludingVat, decimal vatSummary)
		{
			int paymentTypeId = ActivePaymentTypeId;

			var orderPayment = (ILekmerOrderPayment) IoC.Resolve<IOrderPayment>();
			orderPayment.PaymentTypeId = paymentTypeId;
			orderPayment.Price = priceIncludingVat;
			orderPayment.Vat = vatSummary;

			// Klarna
			if (PaymentTypeHelper.IsKlarnaInvoicePayment(paymentTypeId))
			{
				orderPayment.KlarnaEid = GetKlarnaClient(paymentTypeId).KlarnaSetting.EID;
				orderPayment.KlarnaPClass = -1;
			}

			if (PaymentTypeHelper.IsKlarnaPartPayment(paymentTypeId))
			{
				orderPayment.KlarnaEid = GetKlarnaClient(paymentTypeId).KlarnaSetting.EID;
				orderPayment.KlarnaPClass = ActiveKlarnaPartPaymentId;
			}

			if (PaymentTypeHelper.IsKlarnaSpecialPartPayment(paymentTypeId))
			{
				orderPayment.KlarnaEid = GetKlarnaClient(paymentTypeId).KlarnaSetting.EID;
				orderPayment.KlarnaPClass = GetKlarnaSpecialPartPaymentId(paymentTypeId, OrderTotalSum);
			}

			// Maksuturva
			if (paymentTypeId == (int)PaymentMethodType.Maksuturva)
			{
				orderPayment.MaksuturvaCode = ActiveMaksuturvaSubPaymentId;
			}

			// Qliro
			if (PaymentTypeHelper.IsQliroPersonInvoicePayment(paymentTypeId))
			{
				orderPayment.QliroClientRef = QliroClient.QliroSetting.ClientRef;
				orderPayment.QliroPaymentCode = null;
			}

			if (PaymentTypeHelper.IsQliroPartPayment(paymentTypeId))
			{
				orderPayment.QliroClientRef = QliroClient.QliroSetting.ClientRef;
				orderPayment.QliroPaymentCode = ActiveQliroPartPaymentId;
			}

			if (PaymentTypeHelper.IsQliroSpecialPartPayment(paymentTypeId))
			{
				orderPayment.QliroClientRef = QliroClient.QliroSetting.ClientRef;
				orderPayment.QliroPaymentCode = GetQliroSpecialPartPaymentId(OrderTotalSum);
			}

			if (PaymentTypeHelper.IsQliroCompanyInvoicePayment(paymentTypeId))
			{
				orderPayment.QliroClientRef = QliroClient.QliroSetting.ClientRef;
				orderPayment.QliroPaymentCode = null;
			}

			// Collector
			if (PaymentTypeHelper.IsCollectorPersonInvoicePayment(paymentTypeId))
			{
				orderPayment.CollectorStoreId = CollectorClient.CollectorSetting.StoreId;
				orderPayment.CollectorPaymentCode = null;
			}

			if (PaymentTypeHelper.IsCollectorPartPayment(paymentTypeId))
			{
				orderPayment.CollectorStoreId = CollectorClient.CollectorSetting.StoreId;
				orderPayment.CollectorPaymentCode = ActiveCollectorPartPaymentId;
			}

			if (PaymentTypeHelper.IsCollectorSpecialPartPayment(paymentTypeId))
			{
				orderPayment.CollectorStoreId = CollectorClient.CollectorSetting.StoreId;
				orderPayment.CollectorPaymentCode = GetCollectorSpecialPartPaymentId(OrderTotalSum);
			}

			return new Collection<IOrderPayment>
			{
				orderPayment
			};
		}

		public virtual decimal GetPaymentCost()
		{
			var payment = GetOrderPayment();
			return payment != null ? payment.Cost : 0m;
		}

		public virtual ILekmerPaymentType GetOrderPayment()
		{
			if (ActivePaymentTypeId == 0)
			{
				return null;
			}
			return (ILekmerPaymentType)AvailablePaymentTypes.SingleOrDefault(t => t.Id == ActivePaymentTypeId);
		}

		/// <summary>
		/// Gets the lowest MonthlyFee for the partPartPayment alternatives.
		/// </summary>
		/// <param name="paymentTypeId"> </param>
		/// <returns>int</returns>
		/// TODO: Add Qliro specifiec function
		public virtual double GetLowestMonthlyFee(int paymentTypeId)
		{
			return GetKlarnaClient(paymentTypeId).CalculateCheapestMonthlyFee();
		}

		/// <summary>
		/// Validate if selected payment method is valid one.
		/// </summary>
		/// <returns></returns>
		public virtual ValidationResult Validate()
		{
			var validationResult = new ValidationResult();

			if (ActivePaymentTypeId == 0)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentIncorrect"));
			}
			else if (IsValidPaymentType(ActivePaymentTypeId) == false)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentIncorrect"));
			}

			if (PaymentTypeHelper.IsKlarnaPartPayment(ActivePaymentTypeId))
			{
				if (ActiveKlarnaPartPaymentId <= 0)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PartPaymentNotProvided"));
				}

				PClass pClass = GetKlarnaClient(ActivePaymentTypeId).GetPClasses().Cast<PClass>().FirstOrDefault(p => p.PClassID == ActiveKlarnaPartPaymentId);
				if (pClass == null || pClass.MinAmountForPClass > OrderTotalSum)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PartPaymentIncorrect"));
				}
			}

			if (PaymentTypeHelper.IsQliroPartPayment(ActivePaymentTypeId))
			{
				if (ActiveQliroPartPaymentId.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.QliroPartPaymentNotProvided"));
				}

				IQliroPaymentType qliroPaymentType = AvailableQliroPaymentTypes.FirstOrDefault(p => p.Code == ActiveQliroPartPaymentId);
				if (qliroPaymentType == null || qliroPaymentType.MinPurchaseAmount > (decimal)OrderTotalSum)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.QliroPartPaymentIncorrect"));
				}
			}

			if (PaymentTypeHelper.IsCollectorPartPayment(ActivePaymentTypeId))
			{
				if (ActiveCollectorPartPaymentId.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CollectorPartPaymentNotProvided"));
				}

				ICollectorPaymentType collectorPaymentType = AvailableCollectorPaymentTypes.FirstOrDefault(p => p.Code == ActiveCollectorPartPaymentId);
				if (collectorPaymentType == null || collectorPaymentType.MinPurchaseAmount > (decimal)OrderTotalSum)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CollectorPartPaymentIncorrect"));
				}
			}

			if (PaymentTypeHelper.IsMaksuturvaPayment(ActivePaymentTypeId))
			{
				if (ActiveMaksuturvaSubPaymentId.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.MaksuturvaSubPaymentIncorrect"));
				}
				else if(IsValidMaksuturvaSubPayment(ActiveMaksuturvaSubPaymentId) == false)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.MaksuturvaSubPaymentIncorrect"));
				}
			}

			return validationResult;
		}

		// Private

		// Initialize
		protected virtual void InitializeDefaultPayment()
		{
			if (AvailablePaymentTypes == null) return;

			var lekmerAvailablePaymentTypes = AvailablePaymentTypes.Cast<ILekmerPaymentType>();

			IPaymentType defaultPersonPaymentType = lekmerAvailablePaymentTypes.FirstOrDefault(pt => pt.IsPersonDefault);
			if (defaultPersonPaymentType != null)
			{
				DefaultPersonPaymentId = defaultPersonPaymentType.Id;
			}

			IPaymentType defaultCompanyPaymentType = lekmerAvailablePaymentTypes.FirstOrDefault(pt => pt.IsCompanyDefault);
			if (defaultCompanyPaymentType != null)
			{
				DefaultCompanyPaymentId = defaultCompanyPaymentType.Id;
			}

			if (DefaultPersonPaymentId > 0 && DefaultCompanyPaymentId > 0) return;

			//IPaymentType klarnaCheckoutPaymentType = AvailablePaymentTypes.FirstOrDefault(p => PaymentTypeHelper.IsKlarnaCheckoutPayment(p.Id));
			IPaymentType invoicePaymentType = AvailablePaymentTypes.FirstOrDefault(p => PaymentTypeHelper.IsKlarnaInvoicePayment(p.Id));
			IPaymentType qliroPersonInvoicePaymentType = AvailablePaymentTypes.FirstOrDefault(p => PaymentTypeHelper.IsQliroPersonInvoicePayment(p.Id));
			IPaymentType qliroCompanyInvoicePaymentType = AvailablePaymentTypes.FirstOrDefault(p => PaymentTypeHelper.IsQliroCompanyInvoicePayment(p.Id));
			IPaymentType collectorPersonInvoicePaymentType = AvailablePaymentTypes.FirstOrDefault(p => PaymentTypeHelper.IsCollectorPersonInvoicePayment(p.Id));
			IPaymentType creditCardPayment = AvailablePaymentTypes.SingleOrDefault(p => PaymentTypeHelper.IsDibsCreditCardPayment(p.Id));
			IPaymentType iDealPayment = AvailablePaymentTypes.SingleOrDefault(p => PaymentTypeHelper.IsDibsiDealPayment(p.Id));

			if (DefaultPersonPaymentId <= 0)
			{
				defaultPersonPaymentType = invoicePaymentType ?? qliroPersonInvoicePaymentType ?? collectorPersonInvoicePaymentType ?? iDealPayment ?? creditCardPayment;
				if (defaultPersonPaymentType != null)
				{
					DefaultPersonPaymentId = defaultPersonPaymentType.Id;
				}
			}

			if (DefaultCompanyPaymentId <= 0)
			{
				defaultCompanyPaymentType = invoicePaymentType ?? qliroCompanyInvoicePaymentType ?? collectorPersonInvoicePaymentType ?? iDealPayment ?? creditCardPayment;
				if (defaultCompanyPaymentType != null)
				{
					DefaultCompanyPaymentId = defaultCompanyPaymentType.Id;
				}
			}
		}

		protected virtual void InitializeProfilePayment()
		{
			var checkoutProfile = CheckoutProfile;
			if (checkoutProfile != null)
			{
				ProfilePaymentTypeId = checkoutProfile.PaymentMethodId;
				ProfileKlarnaPartPaymentId = checkoutProfile.KlarnaPartPaymentId;
				ProfileMaksuturvaSubPaymentId = checkoutProfile.MaksuturvaSubPaymentId;
				ProfileQliroPartPaymentId = checkoutProfile.QliroPartPaymentId;
				ProfileCollectorPartPaymentId = checkoutProfile.CollectorPartPaymentId;
			}
		}

		protected virtual void UpdateProfilePayment()
		{
			var checkoutProfile = CheckoutProfile;
			if (checkoutProfile != null)
			{
				checkoutProfile.PaymentMethodId = ActivePaymentTypeId;

				if (ActiveKlarnaPartPaymentId > 0)
				{
					checkoutProfile.KlarnaPartPaymentId = ActiveKlarnaPartPaymentId;
				}

				if (ActiveMaksuturvaSubPaymentId.HasValue())
				{
					checkoutProfile.MaksuturvaSubPaymentId = ActiveMaksuturvaSubPaymentId;
				}

				if (ActiveQliroPartPaymentId.HasValue())
				{
					checkoutProfile.QliroPartPaymentId = ActiveQliroPartPaymentId;
				}

				if (ActiveCollectorPartPaymentId.HasValue())
				{
					checkoutProfile.CollectorPartPaymentId = ActiveCollectorPartPaymentId;
				}
			}
		}

		protected virtual void InitializePersonCompany()
		{
			if (CompanyForm.RequestHasCompanyMode)
			{
				CompanyForm.MapFromRequestToForm();
			}
			else
			{
				CompanyForm.MapFromProfileToForm(CheckoutProfile);
			}

			IsCompany = CompanyForm.IsCompanyMode;
		}

		protected virtual void InitializeActivePayment()
		{
			int paymentTypeId = PaymentForm.PaymentTypeId;
			bool paymentTypeIdIsValid = IsValidPaymentType(paymentTypeId);

			if (paymentTypeIdIsValid) // == true
			{
				paymentTypeIdIsValid = IsValidPaymentTypeAndCustomerType(paymentTypeId); // Payment could be not valid for companies
			}

			if (paymentTypeIdIsValid == false)
			{
				paymentTypeId = ProfilePaymentTypeId;
				paymentTypeIdIsValid = IsValidPaymentType(paymentTypeId);
			}

			if (paymentTypeIdIsValid) // == true
			{
				paymentTypeIdIsValid = IsValidPaymentTypeAndCustomerType(paymentTypeId); // Payment could be not valid for companies
			}

			if (paymentTypeIdIsValid == false)
			{
				paymentTypeId = IsPerson ? DefaultPersonPaymentId : DefaultCompanyPaymentId;
			}

			ActivePaymentTypeId = paymentTypeId;
		}

		protected virtual void OnPaymentTypeSelected()
		{
			if (PaymentTypeSelected != null)
			{
				var paymentTypeSelectedEventArgs = new PaymentTypeSelectedEventArgs(ActivePaymentTypeId);
				PaymentTypeSelected(this, paymentTypeSelectedEventArgs);
			}
		}


		// Render
		protected virtual string RenderPaymentType(IPaymentType paymentType, int index, int count)
		{
			Fragment fragment = _template.GetFragment("PaymentType");

			int paymentTypeId = paymentType.Id;
			bool isSelected = ActivePaymentTypeId == paymentTypeId;
			bool isVisible = IsValidPaymentTypeAndCustomerType(paymentTypeId);

			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);

			fragment.AddCondition("IsSelected", isSelected);
			fragment.AddCondition("IsVisible", isVisible);

			fragment.AddVariable("PaymentType.Title", AliasHelper.GetAliasValue("Order.Checkout.Payment.PaymentType_" + paymentType.CommonName), VariableEncoding.None);
			fragment.AddVariable("PaymentType.Id", paymentTypeId.ToString(CultureInfo.InvariantCulture));

			var lekmerPaymentType = (ILekmerPaymentType) paymentType;
			fragment.AddCondition("PaymentType.HasCost", lekmerPaymentType.Cost > 0);
			fragment.AddVariable("PaymentType.Cost", Formatter.FormatPrice(Channel.Current, lekmerPaymentType.Cost));

			RenderPaymentTypeExtensionForCommon(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForiDealPayment(fragment, paymentTypeId, isSelected);

			RenderPaymentTypeExtensionForKlarnaAll(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForKlarnaInvoice(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForKlarnaPartPayment(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForKlarnaSpecialPartPayment(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForKlarnaCheckout(fragment, paymentTypeId, isSelected);

			RenderPaymentTypeExtensionForMaksuturvaPayment(fragment, paymentTypeId, isSelected);

			RenderPaymentTypeExtensionForQliroAll(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForQliroPersonInvoice(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForQliroPartPayment(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForQliroSpecialPartPayment(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForQliroCompanyInvoice(fragment, paymentTypeId, isSelected);

			RenderPaymentTypeExtensionForCollectorAll(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForCollectorPersonInvoice(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForCollectorPartPayment(fragment, paymentTypeId, isSelected);
			RenderPaymentTypeExtensionForCollectorSpecialPartPayment(fragment, paymentTypeId, isSelected);

			return fragment.Render();
		}

		protected virtual void RenderPaymentTypeExtensionForCommon(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isCommonPayment = PaymentTypeHelper.IsCommonPayment(paymentTypeId);
			bool isCommonPaymentAndSelected = paymentTypeIsSelected && isCommonPayment;

			fragment.AddCondition("isCommonPayment", isCommonPayment);
			fragment.AddCondition("isCommonPaymentAndSelected", isCommonPaymentAndSelected);
		}


		// Validation
		protected virtual bool IsValidPaymentType(int paymentTypeId)
		{
			bool isPaymentValid = false;

			if (paymentTypeId > 0)
			{
				isPaymentValid = AvailablePaymentTypes.Any(type => type.Id == paymentTypeId);
			}

			return isPaymentValid;
		}

		protected virtual bool IsValidPaymentTypeAndCustomerType(int paymentTypeId)
		{
			if (IsCompany)
			{
				return IsPaymentTypeValidForCompany(paymentTypeId);
			}

			return IsPaymentTypeValidForPerson(paymentTypeId);
		}

		protected virtual bool IsPaymentTypeValidForPerson(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.QliroCompanyInvoice:
					return false;
			}

			return true;
		}

		protected virtual bool IsPaymentTypeValidForCompany(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType) paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.DibsCreditCard:
				case PaymentMethodType.DibsiDeal:
				case PaymentMethodType.KlarnaInvoice:
				case PaymentMethodType.KlarnaAdvanced:
				case PaymentMethodType.QliroCompanyInvoice:
				case PaymentMethodType.CollectorInvoice:
				case PaymentMethodType.MoneybookersCarteBlue:
				case PaymentMethodType.MoneybookersIDeal:
				case PaymentMethodType.Maksuturva:
					return true;
			}

			return false;
		}
	}
}