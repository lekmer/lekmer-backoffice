using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.TwoSell
{
	[Serializable]
	public class RecommenderList : IRecommenderList
	{
		public int Id { get; set; }
		public string SessionId { get; set; }
		public Collection<IRecommenderItem> Items { get; set; }
	}
}