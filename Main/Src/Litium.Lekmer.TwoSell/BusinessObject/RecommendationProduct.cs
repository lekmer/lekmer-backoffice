﻿using System;

namespace Litium.Lekmer.TwoSell
{
	[Serializable]
	public class RecommendationProduct : IRecommendationProduct
	{
		public int Id { get; set; }
		public int RecommenderItemId { get; set; }
		public int ProductId { get; set; }
		public int Ordinal { get; set; }
	}
}