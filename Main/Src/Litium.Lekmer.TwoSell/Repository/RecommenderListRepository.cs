﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell.Repository
{
	public class RecommenderListRepository
	{
		protected virtual DataMapperBase<IRecommenderList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRecommenderList>(dataReader);
		}

		protected virtual DataMapperBase<IRecommenderItem> CreateRecommenderItemDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRecommenderItem>(dataReader);
		}

		protected virtual DataMapperBase<IRecommendationProduct> CreateRecommendationProductDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRecommendationProduct>(dataReader);
		}


		public virtual int Save(IRecommenderList recommenderList)
		{
			if (recommenderList == null)
			{
				throw new ArgumentNullException("recommenderList");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("RecommenderListId", recommenderList.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("SessionId", recommenderList.SessionId, SqlDbType.VarChar)
			};

			var dbSettings = new DatabaseSetting("RecommenderListRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pRecommenderListSave]", parameters, dbSettings);
		}

		public virtual IRecommenderList GetBySessionAndProduct(string sessionId, int? productId)
		{
			var dbSettings = new DatabaseSetting("RecommenderListRepository.GetBySessionAndProduct");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SessionId", sessionId, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
			};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pRecommenderListGetBySessionAndProduct]", parameters, dbSettings))
			{
				IRecommenderList recommenderList = CreateDataMapper(dataReader).ReadRow();

				if (recommenderList != null)
				{
					dataReader.NextResult();
					IRecommenderItem recommenderItem = CreateRecommenderItemDataMapper(dataReader).ReadRow();

					if (recommenderItem != null)
					{
						recommenderList.Items.Add(recommenderItem);

						dataReader.NextResult();
						Collection<IRecommendationProduct> recommendationProducts = CreateRecommendationProductDataMapper(dataReader).ReadMultipleRows();

						if (recommendationProducts != null)
						{
							recommenderItem.Suggestions = recommendationProducts;
						}
					}
				}
				return recommenderList;
			}
		}
	}
}
