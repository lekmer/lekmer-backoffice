﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell.Repository
{
	public class RecommenderItemRepository
	{
		public virtual int Save(IRecommenderItem recommenderItem)
		{
			if (recommenderItem == null)
			{
				throw new ArgumentNullException("recommenderItem");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("RecommenderItemId", recommenderItem.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("RecommenderListId", recommenderItem.RecommenderListId, SqlDbType.Int),
				ParameterHelper.CreateParameter("ProductId", recommenderItem.ProductId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("RecommenderItemRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pRecommenderItemSave]", parameters, dbSettings);
		}
	}
}
