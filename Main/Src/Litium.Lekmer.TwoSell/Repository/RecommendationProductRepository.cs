﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell.Repository
{
	public class RecommendationProductRepository
	{
		public virtual int Save(IRecommendationProduct recommendationProduct)
		{
			if (recommendationProduct == null)
			{
				throw new ArgumentNullException("recommendationProduct");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("RecommendationProductId", recommendationProduct.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("RecommenderItemId", recommendationProduct.RecommenderItemId, SqlDbType.Int),
				ParameterHelper.CreateParameter("ProductId", recommendationProduct.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("Ordinal", recommendationProduct.Ordinal, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("RecommendationProductRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pRecommendationProductSave]", parameters, dbSettings);
		}

		public virtual void DeleteAllByItem(int recommenderItemId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("RecommenderItemId", recommenderItemId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("RecommendationProductRepository.DeleteAllByItem");

			new DataHandler().ExecuteCommand("[productlek].[pRecommendationProductDeleteAllByItem]", parameters, dbSettings);
		}
	}
}
