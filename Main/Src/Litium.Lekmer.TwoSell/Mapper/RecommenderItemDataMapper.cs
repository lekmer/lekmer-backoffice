using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell.Mapper
{
	public class RecommenderItemDataMapper : DataMapperBase<IRecommenderItem>
	{
		public RecommenderItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRecommenderItem Create()
		{
			var recommenderItem = IoC.Resolve<IRecommenderItem>();

			recommenderItem.Id = MapValue<int>("RecommenderItem.RecommenderItemId");
			recommenderItem.ProductId = MapNullableValue<int>("RecommenderItem.ProductId");

			recommenderItem.Suggestions = new Collection<IRecommendationProduct>();

			return recommenderItem;
		}
	}
}