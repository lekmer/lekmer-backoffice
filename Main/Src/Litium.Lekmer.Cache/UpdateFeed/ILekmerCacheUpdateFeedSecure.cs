﻿using System;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	public interface ILekmerCacheUpdateFeedSecure
	{
		/// <summary>
		/// Deletes update items older then <paramref name="toDate"/>.
		/// </summary>
		/// <param name="toDate">Items older then this should be removed.</param>
		void DeleteOldUpdateItems(DateTime toDate);
	}
}