﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Cache.UpdateFeed;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	public class LekmerCacheUpdateFeed : ICacheUpdateFeed
	{
		private readonly LekmerCacheUpdateFeedRepository _repository;
		private long _lastFetchId;

		public LekmerCacheUpdateFeed()
		{
			_repository = new LekmerCacheUpdateFeedRepository();
		}

		public LekmerCacheUpdateFeed(LekmerCacheUpdateFeedRepository repository)
		{
			_repository = repository;
		}

		/// <summary>
		/// Get new update items that are newer then <paramref name="fromDate"/>.
		/// </summary>
		/// <param name="fromDate">Items newer then this date should be fetched.</param>
		/// <returns>A list of new updates.</returns>
		public virtual IList<ICacheUpdateFeedItem> GetNewUpdateItems(DateTime fromDate)
		{
			Collection<ILekmerCacheUpdateFeedItem> lekmerItems = _repository.GetNewUpdateItems(_lastFetchId);

			var items = new Collection<ICacheUpdateFeedItem>();

			foreach (ILekmerCacheUpdateFeedItem item in lekmerItems)
			{
				items.Add(item);

				if (item.Id > _lastFetchId)
				{
					_lastFetchId = item.Id;
				}
			}

			return items;
		}

		/// <summary>
		/// Adds a new update to the feed.
		/// </summary>
		/// <param name="updateItem">The update item.</param>
		public virtual void AddUpdateItem(ICacheUpdateFeedItem updateItem)
		{
			_repository.AddUpdateItem(updateItem);
		}

		/// <summary>
		/// Deletes update items older then <paramref name="toDate"/>.
		/// </summary>
		/// <param name="toDate">Items older then this should be removed.</param>
		public virtual void DeleteOldUpdateItems(DateTime toDate)
		{
			//Do nothing. The special service will take care that.
			//_repository.DeleteOldUpdateItems(toDate);
		}
	}
}