﻿using System;
using Litium.Framework.Cache.UpdateFeed;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	/// <summary>
	/// Update feed item that contains information of what to be cleared from the cache.
	/// </summary>
	public class LekmerCacheUpdateFeedItem : CacheUpdateFeedItem, ILekmerCacheUpdateFeedItem
	{
		/// <summary>
		/// Update feed item that contains information of what to be cleared from the cache.
		/// </summary>
		/// <param name="id">Id of the feed item.</param>
		/// <param name="managerName">Name of cache manager.</param>
		/// <param name="updateType">Type of update.</param>
		/// <param name="key">Key of the cache item to be removed.</param>
		/// <param name="creationDate">Creation date of the item.</param>
		public LekmerCacheUpdateFeedItem(long id, string managerName, CacheUpdateType updateType, string key, DateTime creationDate)
			: base(managerName, updateType, key, creationDate)
		{
			Id = id;
		}

		/// <summary>
		/// Update feed item that contains information of what to be cleared from the cache.
		/// </summary>
		/// <param name="id">Id of the feed item.</param>
		/// <param name="managerName">Name of cache manager.</param>
		/// <param name="updateType">Type of update.</param>
		/// <param name="creationDate">Creation date of the item.</param>
		public LekmerCacheUpdateFeedItem(long id, string managerName, CacheUpdateType updateType, DateTime creationDate)
			: base(managerName, updateType, creationDate)
		{
			Id = id;
		}

		/// <summary>
		/// Id of the feed item.
		/// </summary>
		public long Id { get; set; }
	}
}