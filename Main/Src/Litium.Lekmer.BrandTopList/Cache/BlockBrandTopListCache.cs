﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.BrandTopList
{
	public sealed class BlockBrandTopListCache : ScensumCacheBase<BlockBrandTopListKey, IBlockBrandTopList>
	{
		private static readonly BlockBrandTopListCache _instance = new BlockBrandTopListCache();

		private BlockBrandTopListCache()
		{
		}

		public static BlockBrandTopListCache Instance
		{
			get
			{
				return _instance;
			}
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (IChannel channel in channels)
			{
				Remove(new BlockBrandTopListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockBrandTopListKey : ICacheKey
	{
		public BlockBrandTopListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return ChannelId + "-" + BlockId;
			}
		}
	}
}