﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListRepository
	{
		protected virtual DataMapperBase<IBlockBrandTopList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBrandTopList>(dataReader);
		}

		public virtual void Save(IBlockBrandTopList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllCategories", block.IncludeAllCategories, SqlDbType.Bit),
					ParameterHelper.CreateParameter("OrderStatisticsDayCount", block.OrderStatisticsDayCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("LinkContentNodeId", block.LinkContentNodeId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandTopListSave]", parameters, new DatabaseSetting("BlockBrandTopListRepository.Save"));
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandTopListDelete]", parameters, new DatabaseSetting("BlockBrandTopListRepository.Delete"));
		}

		public virtual IBlockBrandTopList GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandTopListGetByIdSecure]", parameters, new DatabaseSetting("BlockBrandTopListRepository.GetByIdSecure")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockBrandTopList GetById(int languageId, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandTopListGetById]", parameters, new DatabaseSetting("BlockBrandTopListRepository.GetById")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}