using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Litium.Framework.DataAccess;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert=Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.BrandTopList
{
	public class TopListBrandRepository
	{
		private const char DELIMITER = ',';

		public virtual BrandIdCollection GetIdAllByBlock(int channelId, int blockId, bool includeAllCategories, Collection<ICategoryView> categories, DateTime countOrdersFrom, int positionFrom, int positionTo)
		{
			string categoryIdList = categories == null
				? null
				: Convert.ToStringIdentifierList(categories.Select(c => c.Id), DELIMITER);

			int totalCount = 0;
			var brandIds = new BrandIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IncludeAllCategories", includeAllCategories, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@CategoryIds", categoryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", DELIMITER, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("@CountOrdersFrom", countOrdersFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@From", positionFrom, SqlDbType.Int),
					ParameterHelper.CreateParameter("@To", positionTo, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("TopListBrandRepository.GetIdAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTopListBrandGetIdAllByBlock]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}

				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						brandIds.Add(dataReader.GetInt32(0));
					}
				}
			}

			brandIds.TotalCount = totalCount;

			return brandIds;
		}
	}
}