﻿using System.Collections.ObjectModel;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Core;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListBrandService : IBlockBrandTopListBrandService
	{
		protected BlockBrandTopListBrandRepository Repository { get; private set; }

		public BlockBrandTopListBrandService(BlockBrandTopListBrandRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IBlockBrandTopListBrand> GetAllByBlock(IUserContext context, IBlockBrandTopList block)
		{
			return Repository.GetAllByBlock(context.Channel.Id, block.Id);
		}
	}
}