using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.BrandTopList.Contract;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListCategorySecureService : IBlockBrandTopListCategorySecureService
	{
		protected BlockBrandTopListCategoryRepository Repository { get; private set; }

		public BlockBrandTopListCategorySecureService(BlockBrandTopListCategoryRepository repository)
		{
			Repository = repository;
		}

		public virtual void Save(int blockId, Collection<IBlockBrandTopListCategory> blockCategories)
		{
			if (blockCategories == null)
			{
				throw new ArgumentNullException("blockCategories");
			}

			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAll(blockId);

				foreach (var blockCategory in blockCategories)
				{
					blockCategory.BlockId = blockId;

					Repository.Save(blockCategory);
				}

				transaction.Complete();
			}
		}

		public virtual Collection<IBlockBrandTopListCategory> GetAllByBlock(int blockId)
		{
			return Repository.GetAllByBlockSecure(blockId);
		}
	}
}