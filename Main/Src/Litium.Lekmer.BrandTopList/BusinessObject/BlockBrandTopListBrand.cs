﻿using System;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BrandTopList
{
	[Serializable]
	public class BlockBrandTopListBrand : BusinessObjectBase, IBlockBrandTopListBrand
	{
		private int _blockId;
		private int _position;
		private IBrand _brand;

		public int BlockId
		{
			get
			{
				return _blockId;
			}
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public int Position
		{
			get
			{
				return _position;
			}
			set
			{
				CheckChanged(_position, value);
				_position = value;
			}
		}

		public IBrand Brand
		{
			get
			{
				return _brand;
			}
			set
			{
				CheckChanged(_brand, value);
				_brand = value;
			}
		}
	}
}