using System;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.BrandTopList
{
	[Serializable]
	public class BlockBrandTopListCategory : BusinessObjectBase, IBlockBrandTopListCategory
	{
		private int _blockId;
		private bool _includeSubcategories;
		private ICategory _category;

		public int BlockId
		{
			get
			{
				return _blockId;
			}
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public bool IncludeSubcategories
		{
			get
			{
				return _includeSubcategories;
			}
			set
			{
				CheckChanged(_includeSubcategories, value);
				_includeSubcategories = value;
			}
		}

		public ICategory Category
		{
			get
			{
				return _category;
			}
			set
			{
				CheckChanged(_category, value);
				_category = value;
			}
		}
	}
}