﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListCategoryDataMapper : DataMapperBase<IBlockBrandTopListCategory>
	{
		private DataMapperBase<ICategory> _categoryDataMapper;

		public BlockBrandTopListCategoryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_categoryDataMapper = DataMapperResolver.Resolve<ICategory>(DataReader);
		}

		protected override IBlockBrandTopListCategory Create()
		{
			IBlockBrandTopListCategory blockBrandTopListCategory = IoC.Resolve<IBlockBrandTopListCategory>();

			blockBrandTopListCategory.BlockId = MapValue<int>("BlockBrandTopListCategory.BlockId");
			blockBrandTopListCategory.IncludeSubcategories = MapValue<bool>("BlockBrandTopListCategory.IncludeSubcategories");
			blockBrandTopListCategory.Category = _categoryDataMapper.MapRow();

			blockBrandTopListCategory.SetUntouched();

			return blockBrandTopListCategory;
		}
	}
}