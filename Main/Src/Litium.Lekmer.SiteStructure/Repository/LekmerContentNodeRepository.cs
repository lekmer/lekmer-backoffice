﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class LekmerContentNodeRepository : ContentNodeRepository
	{
		public virtual void ChangeStatus(string contentPageIds, bool isSetOnline, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("IsSetOnline", isSetOnline, SqlDbType.Bit),
					ParameterHelper.CreateParameter("ContentPageIds", contentPageIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("LekmerContentNodeRepository.ChangeStatus");

			new DataHandler().ExecuteCommand("[lekmer].[pContentNodeChangeStatus]", parameters, dbSettings);
		}
	}
}