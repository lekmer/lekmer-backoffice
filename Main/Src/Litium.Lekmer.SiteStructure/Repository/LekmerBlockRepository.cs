﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class LekmerBlockRepository : BlockRepository
	{
		public void Save(ILekmerBlock lekmerBlock)
		{
			if (lekmerBlock == null)
			{
				throw new ArgumentNullException("lekmerBlock");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", lekmerBlock.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@StartDate", lekmerBlock.StartDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@EndDate", lekmerBlock.EndDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@StartDailyIntervalMinutes", lekmerBlock.StartDailyInterval.HasValue ? (int?)lekmerBlock.StartDailyInterval.Value.TotalMinutes : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@EndDailyIntervalMinutes", lekmerBlock.EndDailyInterval.HasValue ? (int?)lekmerBlock.EndDailyInterval.Value.TotalMinutes : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ShowOnDesktop", lekmerBlock.ShowOnDesktop, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@ShowOnMobile", lekmerBlock.ShowOnMobile, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@CanBeUsedAsFallbackOption", lekmerBlock.CanBeUsedAsFallbackOption, SqlDbType.Bit)
				};

			DatabaseSetting dbSettings = new DatabaseSetting("LekmerBlockRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pLekmerBlockSave]", parameters, dbSettings);
		}
	}
}