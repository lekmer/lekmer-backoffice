﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class LekmerBlockRichText : LekmerBlockBase, IBlockRichText
	{
		private string _content;

		public string Content
		{
			get { return _content; }
			set
			{
				CheckChanged(_content, value);

				_content = value;
			}
		}
	}
}