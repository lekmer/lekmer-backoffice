﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public abstract class LekmerBlockBase : BlockBase, ILekmerBlock
	{
		private DateTime? _startDate;
		private DateTime? _endDate;
		private TimeSpan? _startDailyInterval;
		private TimeSpan? _endDailyInterval;
		private bool? _showOnDesktop;
		private bool? _showOnMobile;
		private bool? _canBeUsedAsFallbackOption;
		private bool _useFallback;

		public virtual DateTime? StartDate
		{
			get { return _startDate; }
			set
			{
				CheckChanged(_startDate, value);
				_startDate = value;
			}
		}

		public virtual DateTime? EndDate
		{
			get { return _endDate; }
			set
			{
				CheckChanged(_endDate, value);
				_endDate = value;
			}
		}

		public virtual TimeSpan? StartDailyInterval
		{
			get { return _startDailyInterval; }
			set
			{
				CheckChanged(_startDailyInterval, value);
				_startDailyInterval = value;
			}
		}

		public virtual TimeSpan? EndDailyInterval
		{
			get { return _endDailyInterval; }
			set
			{
				CheckChanged(_endDailyInterval, value);
				_endDailyInterval = value;
			}
		}

		public virtual bool IsInTimeLimit
		{
			get
			{
				var now = DateTime.Now;

				if (StartDate.HasValue && now < StartDate) return false;
				if (EndDate.HasValue && now > EndDate) return false;

				//Both daily interval boundaries are required to set limitation
				if (!StartDailyInterval.HasValue || !EndDailyInterval.HasValue) return true;

				if (now.TimeOfDay < StartDailyInterval) return false;
				if (now.TimeOfDay > EndDailyInterval) return false;

				return true;
			}
		}

		public virtual bool? ShowOnDesktop
		{
			get { return _showOnDesktop; }
			set
			{
				CheckChanged(_showOnDesktop, value);
				_showOnDesktop = value;
			}
		}

		public virtual bool? ShowOnMobile
		{
			get { return _showOnMobile; }
			set
			{
				CheckChanged(_showOnMobile, value);
				_showOnMobile = value;
			}
		}

		public virtual bool? CanBeUsedAsFallbackOption
		{
			get { return _canBeUsedAsFallbackOption; }
			set
			{
				CheckChanged(_canBeUsedAsFallbackOption, value);
				_canBeUsedAsFallbackOption = value;
			}
		}

		public virtual bool UseFallback
		{
			get { return _useFallback; }
			set
			{
				CheckChanged(_useFallback, value);
				_useFallback = value;
			}
		}

		public override void ConvertTo<T>(T block)
		{
			base.ConvertTo(block);

			var lekmerBlock = block as ILekmerBlock;
			if (lekmerBlock != null)
			{
				lekmerBlock.StartDate = StartDate;
				lekmerBlock.EndDate = EndDate;
				lekmerBlock.StartDailyInterval = StartDailyInterval;
				lekmerBlock.EndDailyInterval = EndDailyInterval;
				lekmerBlock.ShowOnDesktop = ShowOnDesktop;
				lekmerBlock.ShowOnMobile = ShowOnMobile;
				lekmerBlock.CanBeUsedAsFallbackOption = CanBeUsedAsFallbackOption;
			}
		}
	}
}
