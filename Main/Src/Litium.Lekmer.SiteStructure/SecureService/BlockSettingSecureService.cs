﻿using Litium.Lekmer.SiteStructure.Cache;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	public class BlockSettingSecureService : IBlockSettingSecureService
	{
		protected BlockSettingRepository Repository { get; private set; }

		public BlockSettingSecureService(BlockSettingRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockSetting Create()
		{
			var blockSetting = IoC.Resolve<IBlockSetting>();
			blockSetting.ColumnCount = 1;
			blockSetting.RowCount = 1;
			blockSetting.TotalItemCount = 10;
			blockSetting.Status = BusinessObjectStatus.New;
			return blockSetting;
		}

		public virtual void Save(IBlockSetting blockSetting)
		{
			Repository.Save(blockSetting);
			BlockSettingCache.Instance.Remove(new BlockSettingKey(blockSetting.BlockId));
		}

		public virtual void Delete(int blockId)
		{
			Repository.Delete(blockId);
			BlockSettingCache.Instance.Remove(new BlockSettingKey(blockId));
		}
	}
}