﻿using System.Collections.Generic;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure
{
	public class LekmerContentNodeSecureService : ContentNodeSecureService, ILekmerContentNodeSecureService
	{
		private const char DELIMITER = ',';

		public LekmerContentNodeSecureService(IAccessValidator accessValidator, ContentNodeRepository repository, IAccessSecureService accessSecureService)
			: base(accessValidator, repository, accessSecureService)
		{
		}

		public virtual void ChangeStatus(ISystemUserFull systemUserFull, List<int> contentPageIds, bool isSetOnline)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			var repository = (LekmerContentNodeRepository) Repository;
			repository.ChangeStatus(Scensum.Foundation.Convert.ToStringIdentifierList(contentPageIds), isSetOnline, DELIMITER);
		}
	}
}