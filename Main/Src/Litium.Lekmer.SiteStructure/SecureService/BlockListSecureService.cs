﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	public class BlockListSecureService : IBlockListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockListSecureService(
			IAccessValidator accessValidator,
			BlockListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public IBlockList Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockList = IoC.Resolve<IBlockList>();
			blockList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockList.Setting = BlockSettingSecureService.Create();
			blockList.Status = BusinessObjectStatus.New;
			return blockList;
		}

		public IBlockList GetById(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetByIdSecure(blockId);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			var blockList = Create();

			blockList.ContentNodeId = contentNodeId;
			blockList.ContentAreaId = contentAreaId;
			blockList.BlockTypeId = blockTypeId;
			blockList.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockList.Title = title;
			blockList.TemplateId = null;
			blockList.Id = Save(systemUserFull, blockList);

			return blockList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}


				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}
		}
	}
}