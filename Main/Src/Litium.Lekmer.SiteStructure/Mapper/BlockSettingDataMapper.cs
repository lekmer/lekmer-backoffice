﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Mapper
{
	public class BlockSettingDataMapper : DataMapperBase<IBlockSetting>
	{
		public BlockSettingDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBlockSetting Create()
		{
			var blockSetting = IoC.Resolve<IBlockSetting>();
			blockSetting.BlockId = MapValue<int>("BlockSetting.BlockId");
			blockSetting.ColumnCount = MapValue<int>("BlockSetting.ColumnCount");
			blockSetting.ColumnCountMobile = MapNullableValue<int?>("BlockSetting.ColumnCountMobile");
			blockSetting.RowCount = MapValue<int>("BlockSetting.RowCount");
			blockSetting.RowCountMobile = MapNullableValue<int?>("BlockSetting.RowCountMobile");
			blockSetting.TotalItemCount = MapValue<int>("BlockSetting.TotalItemCount");
			blockSetting.TotalItemCountMobile = MapNullableValue<int?>("BlockSetting.TotalItemCountMobile");
			return blockSetting;
		}
	}
}