﻿using System;
using System.Data;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Mapper;

namespace Litium.Lekmer.SiteStructure.Mapper
{
	public class LekmerBlockDataMapper : BlockDataMapper
	{
		public LekmerBlockDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IBlock Create()
		{
			var block = (ILekmerBlock) base.Create();

			block.StartDate = MapNullableValue<DateTime?>("Block.StartDate");
			block.EndDate = MapNullableValue<DateTime?>("Block.EndDate");

			int? startMinutes = MapNullableValue<int?>("Block.StartDailyIntervalMinutes");
			block.StartDailyInterval = startMinutes.HasValue ? (TimeSpan?) TimeSpan.FromMinutes(startMinutes.Value) : null;
			int? endMinutes = MapNullableValue<int?>("Block.EndDailyIntervalMinutes");
			block.EndDailyInterval = endMinutes.HasValue ? (TimeSpan?) TimeSpan.FromMinutes(endMinutes.Value) : null;

			block.ShowOnDesktop = MapNullableValue<bool?>("Block.ShowOnDesktop");
			block.ShowOnMobile = MapNullableValue<bool?>("Block.ShowOnMobile");
			block.CanBeUsedAsFallbackOption = MapNullableValue<bool?>("Block.CanBeUsedAsFallbackOption");

			return block;
		}
	}
}