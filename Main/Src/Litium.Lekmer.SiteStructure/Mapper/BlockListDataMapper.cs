﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure.Mapper
{
	public class BlockListDataMapper : DataMapperBase<IBlockList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockEsales = IoC.Resolve<IBlockList>();
			block.ConvertTo(blockEsales);
			blockEsales.Setting = _blockSettingDataMapper.MapRow();
			blockEsales.SetUntouched();
			return blockEsales;
		}
	}
}
