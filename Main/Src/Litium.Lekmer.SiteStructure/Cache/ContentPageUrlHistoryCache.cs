﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.SiteStructure.Cache
{
	public sealed class ContentPageUrlHistoryCache : ScensumCacheBase<ContentPageUrlHistoryKey, Collection<IContentPageUrlHistory>>
	{
		private static readonly ContentPageUrlHistoryCache _instance = new ContentPageUrlHistoryCache();

		private ContentPageUrlHistoryCache()
		{
		}

		public static ContentPageUrlHistoryCache Instance
		{
			get { return _instance; }
		}
	}

	public class ContentPageUrlHistoryKey : ICacheKey
	{
		public ContentPageUrlHistoryKey(int channelId, string urlTitle)
		{
			ChannelId = channelId;
			UrlTitle = urlTitle;
		}

		public int ChannelId { get; set; }
		public string UrlTitle { get; set; }

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + UrlTitle; }
		}
	}
}