﻿using System.Collections.ObjectModel;
using Litium.Lekmer.SiteStructure.Cache;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public class ContentPageUrlHistoryService : IContentPageUrlHistoryService
	{
		protected ContentPageUrlHistoryRepository Repository { get; private set; }

		public ContentPageUrlHistoryService(ContentPageUrlHistoryRepository repository)
		{
			Repository = repository;
		}

		public Collection<IContentPageUrlHistory> GetAllByUrlTitle(IUserContext context, string urlTitle)
		{
			return ContentPageUrlHistoryCache.Instance.TryGetItem(
				new ContentPageUrlHistoryKey(context.Channel.Id, urlTitle),
				delegate { return Repository.GetAllByUrlTitle(context.Channel.Id, urlTitle); });
		}

		public Collection<IContentPageUrlHistory> GetAll(IUserContext context)
		{
			return Repository.GetAll(context.Channel.Id);
		}

		public void Update(int contentPageUrlHistoryId)
		{
			Repository.Update(contentPageUrlHistoryId);
		}

		public void DeleteExpiredItems()
		{
			Repository.DeleteExpiredItems();

			ContentPageUrlHistoryCache.Instance.Flush();
		}

		public void CleanUp(int maxCount)
		{
			Repository.CleanUp(maxCount);

			ContentPageUrlHistoryCache.Instance.Flush();
		}
	}
}