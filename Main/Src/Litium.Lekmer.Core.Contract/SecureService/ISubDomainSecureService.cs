using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ISubDomainSecureService
	{
		Collection<ISubDomain> GetAllByContentType(int contentTypeId);
		Collection<ISubDomain> GetAllByContentType(IChannel channel, int contentTypeId);
	}
}