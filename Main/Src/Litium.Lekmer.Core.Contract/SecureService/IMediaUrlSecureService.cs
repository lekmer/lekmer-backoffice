﻿using Litium.Scensum.Core;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Core
{
	public interface IMediaUrlSecureService
	{
		string GetBackOfficeMediaUrl();
		string ResolveMediaArchiveExternalUrl(IChannel channel);
		string ResolveMediaArchiveExternalUrl(IChannel channel, IImage image);
	}
}
