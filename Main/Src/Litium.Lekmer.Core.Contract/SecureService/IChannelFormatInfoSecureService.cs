﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface IChannelFormatInfoSecureService
	{
		int Save(ISystemUserFull adminUserFull, IChannelFormatInfo channel);
	}
}
