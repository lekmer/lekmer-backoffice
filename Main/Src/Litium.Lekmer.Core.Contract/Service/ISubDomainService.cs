using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ISubDomainService
	{
		Collection<ISubDomain> GetAllByContentType(IChannel channel, int contentTypeId);
	}
}