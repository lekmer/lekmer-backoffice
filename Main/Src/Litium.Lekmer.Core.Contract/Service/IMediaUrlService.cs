using Litium.Scensum.Core;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Core
{
	public interface IMediaUrlService
	{
		string ResolveStaticMediaUrl(IChannel channel);
		string ResolveMediaArchiveUrl(IChannel channel);
		string ResolveMediaArchiveUrl(IChannel current, IMediaItem mediaItem);
		string ResolveMediaArchiveExternalUrl(IChannel channel);
		string ResolveMediaArchiveExternalUrl(IChannel channel, IMediaItem mediaItem);
	}
}