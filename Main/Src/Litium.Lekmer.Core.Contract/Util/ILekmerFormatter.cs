﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	/// <summary>
	/// Handles formatting of different values.
	/// </summary>
	public interface ILekmerFormatter : IFormatter
	{
		/// <summary>
		/// Special format on price for checkout.
		/// </summary>
		/// <param name="channel">The channel to format the price for.</param>
		/// <param name="price">The price to format.</param>
		/// <returns>The formatted price. Shows two decimals if price contains decimals otherwise no decimals</returns>
		string FormatPriceTwoOrLessDecimals(IChannel channel, decimal price);

		/// <summary>
		/// Special format on price with minimum decimals.
		/// </summary>
		/// <param name="channel">The channel to format the price for.</param>
		/// <param name="price">The price to format.</param>
		/// <returns>The formatted price. Shows channel specified decimals if price contains decimals otherwise no decimals</returns>
		string FormatPriceChannelOrLessDecimals(IChannel channel, decimal price);
	}
}
