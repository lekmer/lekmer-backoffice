﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.TwosellExport.Contract;

namespace Litium.Lekmer.TwosellExport
{
	public class TwosellExportSetting : SettingBase, ITwosellExportSetting
	{
		protected override string StorageName
		{
			get { return "TwosellExport"; }
		}
		protected virtual string GroupName
		{
			get { return "TwosellExportSetting"; }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}

		public string OrderInfoFileName
		{
			get { return GetString(GroupName, "OrderInfoFileName"); }
		}
		public string OrderInfoFileDelimiter
		{
			get { return GetString(GroupName, "OrderInfoFileDelimiter"); }
		}
		public int DaysAfterPurchase
		{
			get { return GetInt32(GroupName, "DaysAfterPurchase"); }
		}

		public virtual string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}
		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);
			return dictionary.ContainsKey(channelName);
		}
		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);

			if (dictionary.ContainsKey(channelName))
			{
				return dictionary[channelName];
			}

			return channelName;
		}
		public ICollection<string> ImageGroups
		{
			get { return ConvertToList(GetString(GroupName, "ImageGroups")); }
		}
		public ICollection<string> ImageSizes
		{
			get { return ConvertToList(GetString(GroupName, "ImageSizes")); }
		}
		public ICollection<string> ColorTagGroup
		{
			get { return ConvertToList(GetString(GroupName, "ColorTagGroup")); }
		}
		public ICollection<string> CategoryLevelUrl
		{
			get { return ConvertToList(GetString(GroupName, "CategoryLevelUrl")); }
		}
		public string ProductInfoFileName
		{
			get { return GetString(GroupName, "ProductInfoFileName"); }
		}
		public int PageSize
		{
			get { return GetInt32(GroupName, "PageSize", 100); }
		}

		// value1,value2, ... , valueN => List<value>
		protected virtual ICollection<string> ConvertToList(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
	}
}