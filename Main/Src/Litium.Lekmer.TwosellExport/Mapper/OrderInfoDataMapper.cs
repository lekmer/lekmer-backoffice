﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwosellExport
{
	public class OrderInfoDataMapper : DataMapperBase<IOrderInfo>
	{
		public OrderInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IOrderInfo Create()
		{
			var orderInfo = IoC.Resolve<IOrderInfo>();
			orderInfo.Date = MapValue<DateTime>("OrderDate");
			orderInfo.OrderId = MapValue<int>("OrderId");
			orderInfo.ProductId = MapValue<int>("ProductId");
			orderInfo.ErpId = MapValue<string>("ProductErpId");
			orderInfo.Quantity = MapValue<int>("Quantity");
			orderInfo.Price = MapValue<decimal>("Price");
			orderInfo.CustomerIp = MapValue<string>("IP");
			orderInfo.Email = MapValue<string>("Email");
			orderInfo.LekmerId = MapValue<string>("LekmerId");
			return orderInfo;
		}
	}
}