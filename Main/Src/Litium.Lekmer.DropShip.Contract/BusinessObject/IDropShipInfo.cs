﻿using System;

namespace Litium.Lekmer.DropShip
{
	public interface IDropShipInfo
	{
		int Id  { get; set; }
		string ExternalOrderId { get; set; }
		string OfferId { get; set; }
		string Email { get; set; }
		string Phone { get; set; }
		string SocialSecurityNumber { get; set; }
		string BillingFirstName { get; set; }
		string BillingLastName { get; set; }
		string BillingCompany { get; set; }
		string BillingCo { get; set; }
		string BillingStreet { get; set; }
		string BillingStreetNr { get; set; }
		string BillingEntrance { get; set; }
		string BillingFloor { get; set; }
		string BillingApartmentNumber { get; set; }
		string BillingZip { get; set; }
		string BillingCity { get; set; }
		string BillingCountry { get; set; }
		string DeliveryFirstName { get; set; }
		string DeliveryLastName { get; set; }
		string DeliveryCompany { get; set; }
		string DeliveryCo { get; set; }
		string DeliveryStreet { get; set; }
		string DeliveryStreetNr { get; set; }
		string DeliveryApartmentNumber { get; set; }
		string DeliveryFloor { get; set; }
		string DeliveryEntrance { get; set; }
		string DeliveryZip { get; set; }
		string DeliveryCity { get; set; }
		string DeliveryCountry { get; set; }
		string Discount { get; set; }
		string Comment { get; set; }
		string SupplierNo { get; set; }
		string OrderId { get; set; }
		string OrderDate { get; set; }
		string CustomerNumber { get; set; }
		string NotificationNumber { get; set; }
		string PaymentMethod { get; set; }
		string ProductArticleNumber { get; set; }
		string ProductTitle { get; set; }
		string Quantity { get; set; }
		string Price { get; set; }
		string ProductDiscount { get; set; }
		string ProductSoldSum { get; set; }
		string ProductStatus { get; set; }
		string InvoiceDate { get; set; }
		string VatProc { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime? SendDate { get; set; }
		string CsvFileName { get; set; }
		string PdfFileName { get; set; }

		void PopulateDeliveryInfo();
	}
}