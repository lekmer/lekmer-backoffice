﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.DropShip
{
	public interface IDropShipSharedService
	{
		string SaveCsvFile(KeyValuePair<string, Collection<IDropShipInfo>> dropShipInfo);
		string SavePdfFile(KeyValuePair<string, Collection<IDropShipInfo>> dropShipInfo);
		void SendMessage(string supplierNumber, string csvFilePath, string pdfFilePath, bool isFileSent);
	}
}