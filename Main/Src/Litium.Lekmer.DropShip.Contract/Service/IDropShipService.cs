﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.DropShip
{
	public interface IDropShipService
	{
		void ImportDropShipData(string filePath);
		Collection<IDropShipInfo> GetAllNotSent();
		Collection<IDropShipInfo> GetAllByOrderAndSupplier(int orderId, string supplierNo);
		void Update(int id, string csvFileName, string pdfFileName);
	}
}