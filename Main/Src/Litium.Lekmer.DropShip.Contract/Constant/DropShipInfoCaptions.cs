﻿namespace Litium.Lekmer.DropShip
{
	public static class DropShipInfoCaptions
	{
		public const string Orderinformation = "Orderinformation";
		public const string Ordernummer = "Ordernummer";
		public const string Registerad = "Registerad";
		public const string Kundnummer = "Kundnummer";
		public const string Aviseringsnummer = "Aviseringsnummer";
		public const string Betalningsmetod = "Betalningsmetod";
		public const string Leveranssätt = "Leveranssätt";
		public const string Dropship = "Dropship";
		public const string Faktureringsadress = "Faktureringsadress";
		public const string FirstName = "First name";
		public const string LastName = "Last name";
		public const string Company = "Company";
		public const string Co = "Co";
		public const string Street = "Street";
		public const string StreetNr = "Street nr";
		public const string Entrance = "Entrance";
		public const string Floor = "Floor";
		public const string ApartmentNo = "Apartment no";
		public const string ZipCode = "Zip code";
		public const string City = "City";
		public const string Country = "Country";
		public const string Cellphone = "Cellphone";
		public const string Email = "Email";
		public const string Leveransadress = "Leveransadress";
		public const string SupplierArtNo = "Supplier ArtNo";
		public const string LekmerArtNo = "Lekmer ArtNo";
		public const string Produkt = "Produkt";
		public const string Antal = "Antal";
		public const string PrisSt = "Pris/st";
		public const string Rabbat = "Rabbat %";
		public const string Summa = "Summa";
		public const string TotalRabatt = "Totalrabatt";
		public const string TotalSumma = "Total summa";
	}
}