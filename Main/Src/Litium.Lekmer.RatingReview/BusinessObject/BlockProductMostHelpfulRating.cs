﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockProductMostHelpfulRating : BlockBase, IBlockProductMostHelpfulRating
	{
		private IBlockRating _blockRating;
		private Collection<IBlockRatingItem> _blockRatingItems;

		public IBlockRating BlockRating
		{
			get { return _blockRating; }
			set
			{
				CheckChanged(_blockRating, value);
				_blockRating = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockRatingItem> BlockRatingItems
		{
			get { return _blockRatingItems; }
			set
			{
				CheckChanged(_blockRatingItems, value);
				_blockRatingItems = value;
			}
		}
	}
}