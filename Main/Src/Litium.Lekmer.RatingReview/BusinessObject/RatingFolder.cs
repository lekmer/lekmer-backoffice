﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingFolder : BusinessObjectBase, IRatingFolder
	{
		private int _id;
		private int? _parentRatingFolderId;
		private string _title;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int? ParentFolderId
		{
			get { return ParentRatingFolderId; }
			set { ParentRatingFolderId = value; }
		}

		public int? ParentRatingFolderId
		{
			get { return _parentRatingFolderId; }
			set
			{
				CheckChanged(_parentRatingFolderId, value);
				_parentRatingFolderId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
	}
}