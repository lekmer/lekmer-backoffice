﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingReviewUser : BusinessObjectBase, IRatingReviewUser
	{
		private int _id;
		private int? _customerId;
		private string _token;
		private DateTime _createdDate;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int? CustomerId
		{
			get { return _customerId; }
			set
			{
				CheckChanged(_customerId, value);
				_customerId = value;
			}
		}

		public string Token
		{
			get { return _token; }
			set
			{
				CheckChanged(_token, value);
				_token = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}
	}
}