﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class Review : BusinessObjectBase, IReview
	{
		private int _id;
		private int _ratingReviewFeedbackId;
		private string _authorName;
		private string _title;
		private string _message;
		private string _email;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int RatingReviewFeedbackId
		{
			get { return _ratingReviewFeedbackId; }
			set
			{
				CheckChanged(_ratingReviewFeedbackId, value);
				_ratingReviewFeedbackId = value;
			}
		}

		public string AuthorName
		{
			get { return _authorName; }
			set
			{
				CheckChanged(_authorName, value);
				_authorName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Message
		{
			get { return _message; }
			set
			{
				CheckChanged(_message, value);
				_message = value;
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}
	}
}