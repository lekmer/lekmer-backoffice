﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingGroupCategory : BusinessObjectBase, IRatingGroupCategory
	{
		private int _ratingGroupId;
		private int _categoryId;
		private bool _includeSubcategories;

		public int RatingGroupId
		{
			get { return _ratingGroupId; }
			set
			{
				CheckChanged(_ratingGroupId, value);
				_ratingGroupId = value;
			}
		}

		public int CategoryId
		{
			get { return _categoryId; }
			set
			{
				CheckChanged(_categoryId, value);
				_categoryId = value;
			}
		}

		public bool IncludeSubcategories
		{
			get { return _includeSubcategories; }
			set
			{
				CheckChanged(_includeSubcategories, value);
				_includeSubcategories = value;
			}
		}
	}
}