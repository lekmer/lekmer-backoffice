﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingGroupProduct : BusinessObjectBase, IRatingGroupProduct
	{
		private int _ratingGroupId;
		private int _productId;

		public int RatingGroupId
		{
			get { return _ratingGroupId; }
			set
			{
				CheckChanged(_ratingGroupId, value);
				_ratingGroupId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}
	}
}