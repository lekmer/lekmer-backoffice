﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockBestRatedProductList : BlockBase, IBlockBestRatedProductList
	{
		private int? _categoryId;
		private int? _ratingId;
		private IBlockSetting _setting;
		private Collection<IBlockBestRatedProductListBrand> _blockBestRatedProductListBrands;

		public int? CategoryId
		{
			get
			{
				return _categoryId;
			}
			set
			{
				CheckChanged(_categoryId, value);
				_categoryId = value;
			}
		}

		public int? RatingId
		{
			get
			{
				return _ratingId;
			}
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockBestRatedProductListBrand> BlockBestRatedProductListBrands
		{
			get
			{
				return _blockBestRatedProductListBrands;
			}
			set
			{
				CheckChanged(_blockBestRatedProductListBrands, value);
				_blockBestRatedProductListBrands = value;
			}
		}
	}
}