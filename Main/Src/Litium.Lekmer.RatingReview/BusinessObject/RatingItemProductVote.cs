﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingItemProductVote : BusinessObjectBase, IRatingItemProductVote 
	{
		private int _ratingReviewFeedbackId;
		private int _ratingId;
		private int _ratingItemId;
		private IRating _rating;
		private IRatingItem _ratingItem;

		public int RatingReviewFeedbackId
		{
			get { return _ratingReviewFeedbackId; }
			set
			{
				CheckChanged(_ratingReviewFeedbackId, value);
				_ratingReviewFeedbackId = value;
			}
		}

		public int RatingId
		{
			get { return _ratingId; }
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}

		public int RatingItemId
		{
			get { return _ratingItemId; }
			set
			{
				CheckChanged(_ratingItemId, value);
				_ratingItemId = value;
			}
		}

		public IRating Rating
		{
			get { return _rating; }
			set
			{
				CheckChanged(_rating, value);
				_rating = value;
			}
		}

		public IRatingItem RatingItem
		{
			get { return _ratingItem; }
			set
			{
				CheckChanged(_ratingItem, value);
				_ratingItem = value;
			}
		}
	}
}