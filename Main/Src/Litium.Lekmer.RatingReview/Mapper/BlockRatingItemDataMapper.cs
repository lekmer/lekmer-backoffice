﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockRatingItemDataMapper : DataMapperBase<IBlockRatingItem>
	{
		public BlockRatingItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBlockRatingItem Create()
		{
			var blockRatingItem = IoC.Resolve<IBlockRatingItem>();

			blockRatingItem.BlockId = MapValue<int>("BlockRatingItem.BlockId");
			blockRatingItem.RatingItemId = MapValue<int>("BlockRatingItem.RatingItemId");

			blockRatingItem.SetUntouched();

			return blockRatingItem;
		}
	}
}