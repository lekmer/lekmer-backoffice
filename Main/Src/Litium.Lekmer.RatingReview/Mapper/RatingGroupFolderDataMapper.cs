﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingGroupFolderDataMapper : DataMapperBase<IRatingGroupFolder>
	{
		public RatingGroupFolderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingGroupFolder Create()
		{
			var ratingGroupFolder = IoC.Resolve<IRatingGroupFolder>();

			ratingGroupFolder.Id = MapValue<int>("RatingGroupFolder.RatingGroupFolderId");
			ratingGroupFolder.ParentRatingGroupFolderId = MapNullableValue<int?>("RatingGroupFolder.ParentRatingGroupFolderId");
			ratingGroupFolder.Title = MapValue<string>("RatingGroupFolder.Title");

			ratingGroupFolder.SetUntouched();

			return ratingGroupFolder;
		}
	}
}