﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingItemProductScoreDataMapper : DataMapperBase<IRatingItemProductScore>
	{
		public RatingItemProductScoreDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingItemProductScore Create()
		{
			var ratingItemProductScore = IoC.Resolve<IRatingItemProductScore>();

			ratingItemProductScore.ChannelId = MapValue<int>("RatingItemProductScore.ChannelId");
			ratingItemProductScore.ProductId = MapValue<int>("RatingItemProductScore.ProductId");
			ratingItemProductScore.RatingId = MapValue<int>("RatingItemProductScore.RatingId");
			ratingItemProductScore.RatingItemId = MapValue<int>("RatingItemProductScore.RatingItemId");
			ratingItemProductScore.HitCount = MapValue<int>("RatingItemProductScore.HitCount");

			ratingItemProductScore.SetUntouched();

			return ratingItemProductScore;
		}
	}
}