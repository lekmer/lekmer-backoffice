using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockLatestFeedbackListBrandDataMapper : DataMapperBase<IBlockLatestFeedbackListBrand>
	{
		private DataMapperBase<IBrand> _brandDataMapper;

		public BlockLatestFeedbackListBrandDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_brandDataMapper = DataMapperResolver.Resolve<IBrand>(DataReader);
		}

		protected override IBlockLatestFeedbackListBrand Create()
		{
			var blockLatestFeedbackListBrand = IoC.Resolve<IBlockLatestFeedbackListBrand>();

			blockLatestFeedbackListBrand.BlockId = MapValue<int>("BlockLatestFeedbackListBrand.BlockId");
			blockLatestFeedbackListBrand.Brand = _brandDataMapper.MapRow();

			blockLatestFeedbackListBrand.SetUntouched();

			return blockLatestFeedbackListBrand;
		}
	}
}