﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingReviewFeedbackDataMapper<T> : DataMapperBase<T>
		where T : class, IRatingReviewFeedback
	{
		public RatingReviewFeedbackDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override T Create()
		{
			var ratingReviewFeedback = IoC.Resolve<T>();

			ratingReviewFeedback.Id = MapValue<int>("RatingReviewFeedback.RatingReviewFeedbackId");
			ratingReviewFeedback.ChannelId = MapValue<int>("RatingReviewFeedback.ChannelId");
			ratingReviewFeedback.ProductId = MapValue<int>("RatingReviewFeedback.ProductId");
			ratingReviewFeedback.OrderId = MapNullableValue<int?>("RatingReviewFeedback.OrderId");
			ratingReviewFeedback.RatingReviewStatusId = MapValue<int>("RatingReviewFeedback.RatingReviewStatusId");
			ratingReviewFeedback.LikeHit = MapValue<int>("RatingReviewFeedback.LikeHit");
			ratingReviewFeedback.Inappropriate = MapValue<bool>("RatingReviewFeedback.Inappropriate");
			ratingReviewFeedback.IPAddress = MapValue<string>("RatingReviewFeedback.IpAddress");
			ratingReviewFeedback.CreatedDate = MapValue<DateTime>("RatingReviewFeedback.CreatedDate");
			ratingReviewFeedback.RatingReviewUserId = MapValue<int>("RatingReviewFeedback.RatingReviewUserId");

			ratingReviewFeedback.SetUntouched();

			return ratingReviewFeedback;
		}
	}

	public class RatingReviewFeedbackDataMapper : RatingReviewFeedbackDataMapper<IRatingReviewFeedback>
	{
		public RatingReviewFeedbackDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
	}
}