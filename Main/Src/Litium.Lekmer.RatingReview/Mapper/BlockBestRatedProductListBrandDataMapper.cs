using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockBestRatedProductListBrandDataMapper : DataMapperBase<IBlockBestRatedProductListBrand>
	{
		private DataMapperBase<IBrand> _brandDataMapper;

		public BlockBestRatedProductListBrandDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_brandDataMapper = DataMapperResolver.Resolve<IBrand>(DataReader);
		}

		protected override IBlockBestRatedProductListBrand Create()
		{
			var blockBestRatedProductListBrand = IoC.Resolve<IBlockBestRatedProductListBrand>();

			blockBestRatedProductListBrand.BlockId = MapValue<int>("BlockBestRatedProductListBrand.BlockId");
			blockBestRatedProductListBrand.Brand = _brandDataMapper.MapRow();

			blockBestRatedProductListBrand.SetUntouched();

			return blockBestRatedProductListBrand;
		}
	}
}