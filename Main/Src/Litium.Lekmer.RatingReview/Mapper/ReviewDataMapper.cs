﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class ReviewDataMapper : DataMapperBase<IReview>
	{
		public ReviewDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IReview Create()
		{
			var review = IoC.Resolve<IReview>();

			review.Id = MapValue<int>("Review.ReviewId");
			review.RatingReviewFeedbackId = MapValue<int>("Review.RatingReviewFeedbackId");
			review.AuthorName = MapValue<string>("Review.AuthorName");
			review.Title = MapNullableValue<string>("Review.Title");
			review.Message = MapNullableValue<string>("Review.Message");
			review.Email = MapNullableValue<string>("Review.Email");

			review.SetUntouched();

			return review;
		}
	}
}