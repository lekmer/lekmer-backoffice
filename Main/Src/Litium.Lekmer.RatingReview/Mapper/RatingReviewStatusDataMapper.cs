﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingReviewStatusDataMapper : DataMapperBase<IRatingReviewStatus>
	{
		public RatingReviewStatusDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingReviewStatus Create()
		{
			var ratingReviewStatus = IoC.Resolve<IRatingReviewStatus>();

			ratingReviewStatus.Id = MapValue<int>("RatingReviewStatus.RatingReviewStatusId");
			ratingReviewStatus.CommonName = MapValue<string>("RatingReviewStatus.CommonName");
			ratingReviewStatus.Title = MapValue<string>("RatingReviewStatus.Title");

			ratingReviewStatus.SetUntouched();

			return ratingReviewStatus;
		}
	}
}