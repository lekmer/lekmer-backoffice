using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductMostHelpfulRatingService : IBlockProductMostHelpfulRatingService
	{
		protected IBlockService BlockService { get; private set; }
		protected IBlockRatingService BlockRatingService { get; private set; }
		protected IBlockRatingItemService BlockRatingItemService { get; private set; }

		public BlockProductMostHelpfulRatingService(IBlockService blockService, IBlockRatingService blockRatingService, IBlockRatingItemService blockRatingItemService)
		{
			BlockService = blockService;
			BlockRatingService = blockRatingService;
			BlockRatingItemService = blockRatingItemService;
		}

		public virtual IBlockProductMostHelpfulRating GetById(IUserContext context, int id)
		{
			return BlockProductMostHelpfulRatingCache.Instance.TryGetItem(
				new BlockProductMostHelpfulRatingKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockProductMostHelpfulRating GetByIdCore(IUserContext context, int id)
		{
			IBlock block = BlockService.GetById(context, id);

			var blockProductMostHelpfulRating = IoC.Resolve<IBlockProductMostHelpfulRating>();

			block.ConvertTo(blockProductMostHelpfulRating);

			var ratings = BlockRatingService.GetAllByBlock(id);
			if (ratings != null && ratings.Count > 0)
			{
				blockProductMostHelpfulRating.BlockRating = ratings[0];
			}

			blockProductMostHelpfulRating.BlockRatingItems = BlockRatingItemService.GetAllByBlock(id);

			return blockProductMostHelpfulRating;
		}
	}
}