﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingItemProductVoteService : IRatingItemProductVoteService
	{
		protected RatingItemProductVoteRepository Repository { get; private set; }

		public RatingItemProductVoteService(RatingItemProductVoteRepository repository)
		{
			Repository = repository;
		}

		public virtual IRatingItemProductVote Insert(IRatingItemProductVote ratingItemProductVote)
		{
			if (ratingItemProductVote == null)
			{
				throw new ArgumentNullException("ratingItemProductVote");
			}

			Repository.EnsureNotNull();

			Repository.Insert(ratingItemProductVote);

			return ratingItemProductVote;
		}

		public virtual Collection<IRatingItemProductVote> GetAllByFeedback(int ratingReviewFeedbackId)
		{
			Repository.EnsureNotNull();

			return RatingItemProductVoteListCache.Instance.TryGetItem(
				new RatingItemProductVoteListKey(ratingReviewFeedbackId), 
				() => Repository.GetAllByFeedback(ratingReviewFeedbackId));
		}
	}
}