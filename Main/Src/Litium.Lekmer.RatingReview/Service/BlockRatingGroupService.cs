﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class BlockRatingGroupService : IBlockRatingGroupService
	{
		protected BlockRatingGroupRepository Repository { get; private set; }

		public BlockRatingGroupService(BlockRatingGroupRepository blockRatingGroupRepository)
		{
			Repository = blockRatingGroupRepository;
		}

		public virtual Collection<IBlockRatingGroup> GetAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByBlock(blockId);
		}
	}
}