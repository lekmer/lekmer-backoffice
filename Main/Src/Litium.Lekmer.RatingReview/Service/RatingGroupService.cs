﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupService : IRatingGroupService
	{
		protected RatingGroupRepository Repository { get; private set; }
		protected IRatingGroupProductService RatingGroupProductService { get; private set; }
		protected IRatingGroupCategoryService RatingGroupCategoryService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }

		public RatingGroupService(RatingGroupRepository ratingGroupRepository, IRatingGroupProductService ratingGroupProductService, IRatingGroupCategoryService ratingGroupCategoryService, ICategoryService categoryService)
		{
			Repository = ratingGroupRepository;
			RatingGroupProductService = ratingGroupProductService;
			RatingGroupCategoryService = ratingGroupCategoryService;
			CategoryService = categoryService;
		}

		public virtual IRatingGroup GetById(int ratingGroupId)
		{
			Repository.EnsureNotNull();

			return RatingGroupCache.Instance.TryGetItem(
				new RatingGroupCacheKey(ratingGroupId),
				() => Repository.GetById(ratingGroupId));
		}

		public virtual Collection<IRatingGroup> GetAllByProduct(IUserContext context, int productId, int productCategoryId)
		{
			Repository.EnsureNotNull();
			RatingGroupProductService.EnsureNotNull();
			RatingGroupCategoryService.EnsureNotNull();
			CategoryService.EnsureNotNull();

			var ratingGroupIds = new Dictionary<int, int>();

			// By Product
			var ratingGroupProducts = RatingGroupProductService.GetAllByProduct(productId);

			foreach (var ratingGroupProduct in ratingGroupProducts)
			{
				ratingGroupIds[ratingGroupProduct.RatingGroupId] = ratingGroupProduct.RatingGroupId;
			}

			// By Product Category
			var ratingGroupCategories = RatingGroupCategoryService.GetAllByCategory(productCategoryId);

			foreach (var ratingGroupCategory in ratingGroupCategories)
			{
				ratingGroupIds[ratingGroupCategory.RatingGroupId] = ratingGroupCategory.RatingGroupId;
			}

			// By Parent Categories
			var categoryTree = CategoryService.GetAllAsTree(context);

			ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(productCategoryId);

			if (categoryTreeItem != null && categoryTreeItem.Category != null && categoryTreeItem.Category.ParentCategoryId.HasValue)
			{
				ICategoryTreeItem parentCategoryTreeItem = categoryTree.FindItemById(categoryTreeItem.Category.ParentCategoryId.Value);

				while (parentCategoryTreeItem != null)
				{
					var ratingGroupParentCategories = RatingGroupCategoryService.GetAllByCategory(parentCategoryTreeItem.Category.Id);

					foreach (var ratingGroupCategory in ratingGroupParentCategories)
					{
						if (ratingGroupCategory.IncludeSubcategories)
						{
							ratingGroupIds[ratingGroupCategory.RatingGroupId] = ratingGroupCategory.RatingGroupId;
						}
					}

					if (parentCategoryTreeItem.Category.ParentCategoryId.HasValue)
					{
						parentCategoryTreeItem = categoryTree.FindItemById(parentCategoryTreeItem.Category.ParentCategoryId.Value);
					}
					else
					{
						parentCategoryTreeItem = null;
					}
				}
			}

			// Get Rating Groups
			return RatingGroupListCache.Instance.TryGetItem(
				new RatingGroupListKey(context.Channel.Id, productId),
				() => GetAllByIds(ratingGroupIds.Values));
		}

		protected virtual Collection<IRatingGroup> GetAllByIds(IEnumerable<int> ratingGroupIds)
		{
			return new Collection<IRatingGroup>(ratingGroupIds.Select(GetById).ToList());
		}
	}
}