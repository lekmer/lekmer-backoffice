﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewFeedbackLikeService : IRatingReviewFeedbackLikeService
	{
		protected RatingReviewFeedbackLikeRepository Repository { get; private set; }

		public RatingReviewFeedbackLikeService(RatingReviewFeedbackLikeRepository repository)
		{
			Repository = repository;
		}

		public IRatingReviewFeedbackLike Create(IUserContext context)
		{
			var ratingReviewFeedbackLike = IoC.Resolve<IRatingReviewFeedbackLike>();
			ratingReviewFeedbackLike.CreatedDate = DateTime.Now;
			ratingReviewFeedbackLike.SetUntouched();
			return ratingReviewFeedbackLike;
		}

		public int Insert(IUserContext context, IRatingReviewFeedbackLike ratingReviewFeedbackLike)
		{
			if (ratingReviewFeedbackLike == null)
			{
				throw new ArgumentNullException("ratingReviewFeedbackLike");
			}

			Repository.EnsureNotNull();

			int feedbackLikeId = 0;
			using (var transaction = new TransactedOperation())
			{
				// Insert Feedback Like
				feedbackLikeId = Repository.Insert(ratingReviewFeedbackLike);

				transaction.Complete();
			}

			return feedbackLikeId;
		}
	}
}