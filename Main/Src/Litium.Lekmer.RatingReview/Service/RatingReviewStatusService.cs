using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewStatusService : IRatingReviewStatusService
	{
		protected RatingReviewStatusRepository Repository { get; private set; }

		public RatingReviewStatusService(RatingReviewStatusRepository repository)
		{
			Repository = repository;
		}

		public virtual IRatingReviewStatus GetByCommonName(string commonName)
		{
			Repository.EnsureNotNull();

			return RatingReviewStatusCache.Instance.TryGetItem(
				new RatingReviewStatusKey(commonName),
				() => Repository.GetByCommonName(commonName));
		}
	}
}