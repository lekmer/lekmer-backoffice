﻿using System.Collections.ObjectModel;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockBestRatedProductListBrandService : IBlockBestRatedProductListBrandService
	{
		protected BlockBestRatedProductListBrandRepository Repository { get; private set; }

		public BlockBestRatedProductListBrandService(BlockBestRatedProductListBrandRepository repository)
		{
			Repository = repository;
		}

		public Collection<IBlockBestRatedProductListBrand> GetAllByBlock(IUserContext context, int blockId)
		{
			return Repository.GetAllByBlock(context.Channel.Id, blockId);
		}
	}
}