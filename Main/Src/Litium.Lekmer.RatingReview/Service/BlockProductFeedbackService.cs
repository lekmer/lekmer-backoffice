﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductFeedbackService : IBlockProductFeedbackService
	{
		protected BlockProductFeedbackRepository Repository { get; private set; }

		public BlockProductFeedbackService(BlockProductFeedbackRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductFeedback GetById(IUserContext context, int id)
		{
			Repository.EnsureNotNull();

			return BlockProductFeedbackCache.Instance.TryGetItem(
				new BlockProductFeedbackKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockProductFeedback GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}