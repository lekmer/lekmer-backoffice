﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingService : IRatingService
	{
		protected RatingRepository Repository { get; private set; }
		protected IRatingItemService RatingItemService { get; private set; }

		public RatingService(RatingRepository ratingRepository, IRatingItemService ratingItemService)
		{
			Repository = ratingRepository;
			RatingItemService = ratingItemService;
		}

		public virtual IRating GetById(IUserContext context, int ratingId)
		{
			return RatingCache.Instance.TryGetItem(
				new RatingCacheKey(context.Channel.Id, ratingId),
				() => GetByIdCore(context, ratingId));
		}

		public virtual Collection<IRating> GetAllByGroup(IUserContext context, int groupId)
		{
			return RatingListCache.Instance.TryGetItem(
				new RatingListKey(context.Channel.Id, groupId),
				() => GetAllByGroupCore(context, groupId));
		}

		protected virtual IRating GetByIdCore(IUserContext context, int ratingId)
		{
			Repository.EnsureNotNull();
			RatingItemService.EnsureNotNull();

			var rating = Repository.GetById(context.Channel.Id, ratingId);
			PopulateRatingItems(context, rating);

			return rating;
		}

		protected virtual Collection<IRating> GetAllByGroupCore(IUserContext context, int groupId)
		{
			Repository.EnsureNotNull();
			RatingItemService.EnsureNotNull();

			var ratings = Repository.GetAllByGroup(context.Channel.Id, groupId);
			foreach (IRating rating in ratings)
			{
				PopulateRatingItems(context, rating);
			}

			return ratings;
		}

		protected virtual void PopulateRatingItems(IUserContext context, IRating rating)
		{
			if (rating == null)
			{
				return;
			}

			rating.RatingItems.Clear();
			rating.RatingItems.AddRange(RatingItemService.GetAllByRating(context, rating.Id));
		}
	}
}