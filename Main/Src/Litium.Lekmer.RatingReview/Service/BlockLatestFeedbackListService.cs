﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockLatestFeedbackListService : IBlockLatestFeedbackListService
	{
		protected BlockLatestFeedbackListRepository Repository { get; private set; }
		protected IBlockLatestFeedbackListBrandService BlockLatestFeedbackListBrandService { get; private set; }

		public BlockLatestFeedbackListService(
			BlockLatestFeedbackListRepository repository,
			IBlockLatestFeedbackListBrandService blockLatestFeedbackListBrandService)
		{
			Repository = repository;
			BlockLatestFeedbackListBrandService = blockLatestFeedbackListBrandService;
		}

		public virtual IBlockLatestFeedbackList GetById(IUserContext context, int blockId)
		{
			Repository.EnsureNotNull();

			return BlockLatestFeedbackListCache.Instance.TryGetItem(
				new BlockLatestFeedbackListKey(context.Channel.Id, blockId),
				() => GetByIdCore(context, blockId));
		}

		protected virtual IBlockLatestFeedbackList GetByIdCore(IUserContext context, int blockId)
		{
			var blockLatestFeedbackList = Repository.GetById(context.Channel, blockId);

			blockLatestFeedbackList.BlockLatestFeedbackListBrands = BlockLatestFeedbackListBrandService.GetAllByBlock(context, blockId);

			return blockLatestFeedbackList;
		}
	}
}