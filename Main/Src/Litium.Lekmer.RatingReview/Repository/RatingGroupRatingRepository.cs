﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingGroupRatingRepository
	{
		protected virtual DataMapperBase<IRatingGroupRating> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingGroupRating>(dataReader);
		}

		public virtual void Save(IRatingGroupRating ratingGroupRating)
		{
			if (ratingGroupRating == null)
			{
				throw new ArgumentNullException("ratingGroupRating");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupRating.RatingGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingGroupRating.RatingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Ordinal", ratingGroupRating.Ordinal, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRatingRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupRatingSave]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingGroupId, int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRatingRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupRatingDelete]", parameters, dbSettings);
		}

		public virtual Collection<IRatingGroupRating> GetAllByGroup(int ratingGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRatingRepository.GetAllByGroup");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupRatingGetAllByGroup]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRatingGroupRating> GetAllByRating(int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRatingRepository.GetAllByRating");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupRatingGetAllByRating]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}