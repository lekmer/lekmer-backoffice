﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockRatingRepository
	{
		protected virtual DataMapperBase<IBlockRating> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockRating>(dataReader);
		}

		public virtual void Save(int blockId, int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockRating_Save]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockRating_Delete]", parameters, dbSettings);
		}

		public virtual Collection<IBlockRating> GetAllByBlock(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockRating_GetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}