﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingGroupFolderRepository
	{
		protected virtual DataMapperBase<IRatingGroupFolder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingGroupFolder>(dataReader);
		}

		protected virtual DataMapperBase<INode> CreateDataMapperNode(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INode>(dataReader);
		}

		public virtual int Save(IRatingGroupFolder ratingGroupFolder)
		{
			if (ratingGroupFolder == null)
			{
				throw new ArgumentNullException("ratingGroupFolder");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupFolderId", ratingGroupFolder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ParentRatingGroupFolderId", ratingGroupFolder.ParentRatingGroupFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", ratingGroupFolder.Title, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("RatingGroupFolderRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingGroupFolderSave]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingGroupFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupFolderId", ratingGroupFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupFolderRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupFolderDelete]", parameters, dbSettings);
		}

		public virtual IRatingGroupFolder GetById(int ratingGroupFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupFolderId", ratingGroupFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupFolderRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupFolderGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IRatingGroupFolder> GetAll()
		{
			var dbSettings = new DatabaseSetting("RatingGroupFolderRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupFolderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SelectedId", selectedId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupFolderRepository.GetTree");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupFolderGetTree]", parameters, dbSettings))
			{
				return CreateDataMapperNode(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRatingGroupFolder> GetAllByParent(int parentId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ParentId", parentId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupFolderRepository.GetAllByParent");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupFolderGetAllByParent]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}