﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingItemProductScoreRepository
	{
		protected virtual DataMapperBase<IRatingItemProductScore> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingItemProductScore>(dataReader);
		}

		protected virtual DataMapperBase<IRatingItemProductScoreSummary> CreateDataMapperSummary(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingItemProductScoreSummary>(dataReader);
		}

		public virtual void Insert(IRatingItemProductScore ratingItemProductScore)
		{
			if (ratingItemProductScore == null)
			{
				throw new ArgumentNullException("ratingItemProductScore");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", ratingItemProductScore.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", ratingItemProductScore.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingItemProductScore.RatingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingItemId", ratingItemProductScore.RatingItemId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemProductScoreRepository.Insert");

			new DataHandler().ExecuteCommand("[review].[pRatingItemProductScoreInsert]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingReviewFeedbackId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemProductScoreRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingItemProductScoreDecrement]", parameters, dbSettings);
		}

		public virtual Collection<IRatingItemProductScoreSummary> GetSummaryByRatingAndProduct(int channelId, int ratingId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
				};

			var dbSettings = new DatabaseSetting("RatingItemProductScoreRepository.GetSummaryByRatingAndProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]", parameters, dbSettings))
			{
				return CreateDataMapperSummary(dataReader).ReadMultipleRows();
			}
		}

		public virtual ProductIdCollection GetBestRatedProductsByRating(int channelId, int ratingId, string categoryIds, string brandIds, char delimiter, int numberOfItems, int? customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryIds", categoryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandIds", brandIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("NumberOfItems", numberOfItems, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemProductScoreRepository.GetBestRatedProductsByRating");

			var productIds = new ProductIdCollection();

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingItemProductScoreGetBestRatedProductsByRating]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}
	}
}