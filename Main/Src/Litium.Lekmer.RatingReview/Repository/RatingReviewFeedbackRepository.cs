﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingReviewFeedbackRepository
	{
		protected virtual DataMapperBase<IRatingReviewFeedback> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingReviewFeedback>(dataReader);
		}

		protected virtual DataMapperBase<IRatingReviewFeedbackRecord> CreateRecordDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingReviewFeedbackRecord>(dataReader);
		}

		public virtual int Save(IRatingReviewFeedback feedback)
		{
			if (feedback == null)
			{
				throw new ArgumentNullException("feedback");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", feedback.Id > 0 ? (object) feedback.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", feedback.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", feedback.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", feedback.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingReviewStatusId", feedback.RatingReviewStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LikeHit", feedback.LikeHit, SqlDbType.Int),
					ParameterHelper.CreateParameter("Inappropriate", feedback.Inappropriate, SqlDbType.Bit),
					ParameterHelper.CreateParameter("IPAddress", feedback.IPAddress, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", feedback.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("RatingReviewUserId", feedback.RatingReviewUserId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingReviewFeedback_Save]", parameters, dbSettings);
		}

		public virtual void SetStatus(int ratingReviewFeedbackId, int statusId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusId", statusId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.SetStatus");

			new DataHandler().ExecuteCommand("[review].[pRatingReviewFeedback_SetStatus]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingReviewFeedbackId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingReviewFeedback_Delete]", parameters, dbSettings);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flag")]
		public virtual void UpdateInappropriateFlag(int ratingReviewFeedbackId, bool inappropriateFlag)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int),
					ParameterHelper.CreateParameter("InappropriateFlag", inappropriateFlag, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.UpdateInappropriateFlag");

			new DataHandler().ExecuteCommand("[review].[pRatingReviewFeedback_UpdateInappropriateFlag]", parameters, dbSettings);
		}

		public virtual RatingReviewFeedbackRecordCollection Search(IReviewSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null)
			{
				throw new ArgumentNullException("searchCriteria");
			}

			var itemsCount = 0;
			var reviewRecordCollection = new RatingReviewFeedbackRecordCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@Author", searchCriteria.Author, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Message", searchCriteria.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@StatusId", searchCriteria.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CreatedFrom", searchCriteria.CreatedFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@CreatedTo", searchCriteria.CreatedTo, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@ProductId", searchCriteria.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductTitle", searchCriteria.ProductTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@OrderId", searchCriteria.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", searchCriteria.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@RatingId", searchCriteria.RatingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@InappropriateContent", searchCriteria.InappropriateContent, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@SortBy", searchCriteria.SortBy, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@SortDescending", searchCriteria.SortByDescending, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.Search");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_Search]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}

				if (dataReader.NextResult())
				{
					reviewRecordCollection = CreateRecordDataMapper(dataReader).ReadMultipleRows<RatingReviewFeedbackRecordCollection>();
				}
			}

			reviewRecordCollection.TotalCount = itemsCount;

			return reviewRecordCollection;
		}

		public virtual IRatingReviewFeedback GetById(int ratingReviewFeedbackId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_GetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IRatingReviewFeedbackRecord GetRecordById(int ratingReviewFeedbackId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetRecordById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pReviewRecordGetByFeedback]", parameters, dbSettings))
			{
				return CreateRecordDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IRatingReviewFeedback GetMostHelpful(int channelId, int productId, int ratingId, string ratingItemIds, char delimiter, int ratingReviewStatusId, bool isInappropriate)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingItemIds", ratingItemIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("RatingReviewStatusId", ratingReviewStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IsInappropriate", isInappropriate, SqlDbType.Bit),
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetMostHelpful");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_GetMostHelpfulByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IRatingReviewFeedback GetMostHelpful(int channelId, string productIds, int ratingId, string ratingItemIds, char delimiter, int ratingReviewStatusId, bool isInappropriate, int? customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductIds", productIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingItemIds", ratingItemIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("RatingReviewStatusId", ratingReviewStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IsInappropriate", isInappropriate, SqlDbType.Bit),
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetMostHelpful");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_GetMostHelpfulByProductAndVariants]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual IRatingReviewFeedback GetByProductAndOrder(int channelId, int productId, int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetByProductAndOrder");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_GetByProductAndOrder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IRatingReviewFeedback> GetLatest(int channelId, string categoryIds, string brandIds, char delimiter, int numberOfItems, int ratingReviewStatusId, bool isInappropriate, int? customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryIds", categoryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandIds", brandIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("NumberOfItems", numberOfItems, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingReviewStatusId", ratingReviewStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IsInappropriate", isInappropriate, SqlDbType.Bit),
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetLatest");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_GetLatest]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual Collection<IRatingReviewFeedback> GetLatestByProduct(int channelId, string productIds, char delimiter, int numberOfItems, int ratingReviewStatusId, bool isInappropriate, int? customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductIds", productIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("NumberOfItems", numberOfItems, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingReviewStatusId", ratingReviewStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IsInappropriate", isInappropriate, SqlDbType.Bit),
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackRepository.GetLatestByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewFeedback_GetLatestByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}