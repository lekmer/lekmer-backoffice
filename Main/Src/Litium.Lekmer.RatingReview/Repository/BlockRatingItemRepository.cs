﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockRatingItemRepository
	{
		protected virtual DataMapperBase<IBlockRatingItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockRatingItem>(dataReader);
		}

		public virtual void Save(int blockId, int ratingItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingItemId", ratingItemId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingItemRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockRatingItemSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingItemRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockRatingItemDelete]", parameters, dbSettings);
		}

		public virtual Collection<IBlockRatingItem> GetAllByBlock(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingItemRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockRatingItemGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}