﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockRatingGroupRepository
	{
		protected virtual DataMapperBase<IBlockRatingGroup> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockRatingGroup>(dataReader);
		}

		public virtual void Save(int blockId, int ratingGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingGroupRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockRatingGroupSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingGroupRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockRatingGroupDelete]", parameters, dbSettings);
		}

		public virtual Collection<IBlockRatingGroup> GetAllByBlock(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockRatingGroupRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockRatingGroupGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}