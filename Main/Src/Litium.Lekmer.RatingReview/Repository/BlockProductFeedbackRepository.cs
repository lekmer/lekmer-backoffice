﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockProductFeedbackRepository
	{
		protected virtual DataMapperBase<IBlockProductFeedback> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductFeedback>(dataReader);
		}

		public virtual IBlockProductFeedback GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockProductFeedbackRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockProductFeedbackGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductFeedback GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockProductFeedbackRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockProductFeedbackGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockProductFeedback block)
		{
			if (block == null) throw new ArgumentNullException("block");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("AllowReview", block.AllowReview, SqlDbType.Bit)
				};

			var dbSetting = new DatabaseSetting("BlockProductFeedbackRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockProductFeedbackSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockProductFeedbackRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockProductFeedbackDelete]", parameters, dbSettings);
		}
	}
}