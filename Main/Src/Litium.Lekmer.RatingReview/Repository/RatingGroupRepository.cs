﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingGroupRepository
	{
		protected virtual DataMapperBase<IRatingGroup> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingGroup>(dataReader);
		}

		public virtual int Save(IRatingGroup ratingGroup)
		{
			if (ratingGroup == null)
			{
				throw new ArgumentNullException("ratingGroup");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroup.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingGroupFolderId", ratingGroup.RatingGroupFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CommonName", ratingGroup.CommonName, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Title", ratingGroup.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", ratingGroup.Description, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingGroup_Save]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingGroup_Delete]", parameters, dbSettings);
		}

		public virtual IRatingGroup GetById(int ratingGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroup_GetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IRatingGroup> GetAllByFolder(int ratingGroupFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupFolderId", ratingGroupFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRepository.GetAllByFolder");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroup_GetAllByFolder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRatingGroup> Search(string searchCriteria)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchCriteria", searchCriteria, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("RatingGroupRepository.Search");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroup_Search]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}