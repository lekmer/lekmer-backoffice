﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingReviewUserCache : ScensumCacheBase<RatingReviewUserKey, IRatingReviewUser>
	{
		private static readonly RatingReviewUserCache _instance = new RatingReviewUserCache();

		private RatingReviewUserCache()
		{
		}

		public static RatingReviewUserCache Instance
		{
			get { return _instance; }
		}
	}

	public abstract class RatingReviewUserKey : ICacheKey
	{
		public abstract string Key { get; }
	}

	public class RatingReviewUserIdKey : RatingReviewUserKey
	{
		public RatingReviewUserIdKey(int userId)
		{
			UserId = userId;
		}

		public int UserId { get; set; }

		public override string Key
		{
			get { return UserId.ToString(CultureInfo.InvariantCulture); }
		}
	}

	public class RatingReviewUserCustomerIdKey : RatingReviewUserKey
	{
		public RatingReviewUserCustomerIdKey(int customerId)
		{
			CustomerId = customerId;
		}

		public int CustomerId { get; set; }

		public override string Key
		{
			get { return CustomerId.ToString(CultureInfo.InvariantCulture); }
		}
	}

	public class RatingReviewUserTokenKey : RatingReviewUserKey
	{
		public RatingReviewUserTokenKey(string token)
		{
			Token = token;
		}

		public string Token { get; set; }

		public override string Key
		{
			get { return Token; }
		}
	}
}