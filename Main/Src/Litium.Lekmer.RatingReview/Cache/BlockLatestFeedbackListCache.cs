﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class BlockLatestFeedbackListCache : ScensumCacheBase<BlockLatestFeedbackListKey, IBlockLatestFeedbackList>
	{
		private static readonly BlockLatestFeedbackListCache _instance = new BlockLatestFeedbackListCache();

		private BlockLatestFeedbackListCache()
		{
		}

		public static BlockLatestFeedbackListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockLatestFeedbackListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockLatestFeedbackListKey : ICacheKey
	{
		public BlockLatestFeedbackListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}