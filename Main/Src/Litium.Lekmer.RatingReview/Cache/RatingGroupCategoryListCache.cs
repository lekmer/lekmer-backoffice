﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingGroupCategoryListCache : ScensumCacheBase<RatingGroupCategoryListKey, Collection<IRatingGroupCategory>>
	{
		private static readonly RatingGroupCategoryListCache _instance = new RatingGroupCategoryListCache();

		private RatingGroupCategoryListCache()
		{
		}

		public static RatingGroupCategoryListCache Instance
		{
			get { return _instance; }
		}
	}

	public class RatingGroupCategoryListKey : ICacheKey
	{
		public RatingGroupCategoryListKey(int categoryId)
		{
			CategoryId = categoryId;
		}

		public int CategoryId { get; set; }

		public string Key
		{
			get { return CategoryId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}