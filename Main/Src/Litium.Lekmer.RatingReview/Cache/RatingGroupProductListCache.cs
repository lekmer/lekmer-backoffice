﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingGroupProductListCache : ScensumCacheBase<RatingGroupProductListKey, Collection<IRatingGroupProduct>>
	{
		private static readonly RatingGroupProductListCache _instance = new RatingGroupProductListCache();

		private RatingGroupProductListCache()
		{
		}

		public static RatingGroupProductListCache Instance
		{
			get { return _instance; }
		}
	}

	public class RatingGroupProductListKey : ICacheKey
	{
		public RatingGroupProductListKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}