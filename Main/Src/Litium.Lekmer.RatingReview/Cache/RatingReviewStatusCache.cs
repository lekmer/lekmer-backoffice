﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingReviewStatusCache : ScensumCacheBase<RatingReviewStatusKey, IRatingReviewStatus>
	{
		private static readonly RatingReviewStatusCache _instance = new RatingReviewStatusCache();

		private RatingReviewStatusCache()
		{
		}

		public static RatingReviewStatusCache Instance
		{
			get { return _instance; }
		}
	}

	public class RatingReviewStatusKey : ICacheKey
	{
		public RatingReviewStatusKey(string commonName)
		{
			CommonName = commonName;
		}

		public string CommonName { get; set; }

		public string Key
		{
			get { return CommonName; }
		}
	}
}