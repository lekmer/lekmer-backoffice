﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public class CategoryUtility
	{
		public virtual Collection<int> ResolveCategoryWithChildren(IUserContext context, int categoryId)
		{
			var categoryIds = new Collection<int>();

			ICategoryTree categoryTree = IoC.Resolve<ICategoryService>().GetAllAsTree(context);
			ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(categoryId);
			if (categoryTreeItem == null)
			{
				return categoryIds;
			}

			ResolveCategoryWithChildren(categoryIds, categoryTreeItem);

			return categoryIds;
		}

		protected virtual void ResolveCategoryWithChildren(Collection<int> categoryIds, ICategoryTreeItem categoryTreeItem)
		{
			if (categoryIds == null)
			{
				throw new ArgumentNullException("categoryIds");
			}

			if (categoryTreeItem == null)
			{
				throw new ArgumentNullException("categoryTreeItem");
			}

			categoryIds.Add(categoryTreeItem.Category.Id);

			foreach (ICategoryTreeItem child in categoryTreeItem.Children)
			{
				ResolveCategoryWithChildren(categoryIds, child);
			}
		}
	}
}