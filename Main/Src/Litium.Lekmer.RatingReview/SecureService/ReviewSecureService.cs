﻿using System;
using System.Collections.Generic;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.RatingReview
{
	public class ReviewSecureService : IReviewSecureService
	{
		protected ReviewRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ReviewSecureService(ReviewRepository reviewRepository, IAccessValidator accessValidator)
		{
			Repository = reviewRepository;
			AccessValidator = accessValidator;
		}

		public IReview Save(ISystemUserFull systemUserFull, IReview review)
		{
			if (review == null)
			{
				throw new ArgumentNullException("review");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentReview);

			review.Id = Repository.Save(review);

			return review;
		}

		public virtual void Update(ISystemUserFull systemUserFull, IEnumerable<int> feedbackIdList, IEnumerable<string> authorNameList)
		{
			if (feedbackIdList == null)
			{
				throw new ArgumentNullException("feedbackIdList");
			}

			if (authorNameList == null)
			{
				throw new ArgumentNullException("authorNameList");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentReview);

			Repository.Update(Convert.ToStringIdentifierList(feedbackIdList), Convert.ToStringIdentifierList(authorNameList), Convert.DefaultListSeparator);
		}


		public virtual void ReviewHistoryInsert(ISystemUserFull systemUserFull, int reviewId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentReview);

			Repository.ReviewHistoryInsert(reviewId, systemUserFull.Id);
		}
	}
}