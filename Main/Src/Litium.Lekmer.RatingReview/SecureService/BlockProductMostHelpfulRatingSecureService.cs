using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductMostHelpfulRatingSecureService : IBlockProductMostHelpfulRatingSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockRatingSecureService BlockRatingSecureService { get; private set; }
		protected IBlockRatingItemSecureService BlockRatingItemSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockProductMostHelpfulRatingSecureService(
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockRatingSecureService blockRatingSecureService,
			IBlockRatingItemSecureService blockRatingItemSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockRatingSecureService = blockRatingSecureService;
			BlockRatingItemSecureService = blockRatingItemSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockProductMostHelpfulRating Create()
		{
			AccessSecureService.EnsureNotNull();

			var block = IoC.Resolve<IBlockProductMostHelpfulRating>();

			block.AccessId = AccessSecureService.GetByCommonName("All").Id;

			block.BlockRatingItems = new Collection<IBlockRatingItem>();

			block.Status = BusinessObjectStatus.New;

			return block;
		}

		public virtual IBlockProductMostHelpfulRating GetById(int id)
		{
			IBlock block = BlockSecureService.GetById(id);

			var blockProductMostHelpfulRating = IoC.Resolve<IBlockProductMostHelpfulRating>();

			block.ConvertTo(blockProductMostHelpfulRating);

			var ratings = BlockRatingSecureService.GetAllByBlock(id);
			if (ratings != null && ratings.Count > 0)
			{
				blockProductMostHelpfulRating.BlockRating = ratings[0];
			}

			blockProductMostHelpfulRating.BlockRatingItems = BlockRatingItemSecureService.GetAllByBlock(id);

			return blockProductMostHelpfulRating;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockProductMostHelpfulRating blockProductMostHelpfulRating = Create();

			blockProductMostHelpfulRating.ContentNodeId = contentNodeId;
			blockProductMostHelpfulRating.ContentAreaId = contentAreaId;
			blockProductMostHelpfulRating.BlockTypeId = blockTypeId;
			blockProductMostHelpfulRating.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockProductMostHelpfulRating.Title = title;
			blockProductMostHelpfulRating.TemplateId = null;
			blockProductMostHelpfulRating.Id = Save(systemUserFull, blockProductMostHelpfulRating);

			return blockProductMostHelpfulRating;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductMostHelpfulRating block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			
			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				var ratings = new Collection<IBlockRating>();
				if (block.BlockRating != null)
				{
					ratings.Add(block.BlockRating);
				}
				
				BlockRatingSecureService.SaveAll(systemUserFull, block.Id, ratings);
				BlockRatingItemSecureService.SaveAll(systemUserFull, block.Id, block.BlockRatingItems);

				transactedOperation.Complete();
			}

			BlockProductMostHelpfulRatingCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				BlockRatingSecureService.Delete(systemUserFull, blockId);
				BlockRatingItemSecureService.Delete(systemUserFull, blockId);

				transactedOperation.Complete();
			}

			BlockProductMostHelpfulRatingCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}