﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupSecureService : IRatingGroupSecureService
	{
		protected RatingGroupRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IRatingGroupRatingSecureService RatingGroupRatingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public RatingGroupSecureService(RatingGroupRepository ratingGroupRepository, IAccessValidator accessValidator, IRatingGroupRatingSecureService ratingGroupRatingSecureService, IChannelSecureService channelSecureService)
		{
			Repository = ratingGroupRepository;
			AccessValidator = accessValidator;
			RatingGroupRatingSecureService = ratingGroupRatingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IRatingGroup Save(ISystemUserFull systemUserFull, IRatingGroup ratingGroup)
		{
			if (ratingGroup == null)
			{
				throw new ArgumentNullException("ratingGroup");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				ratingGroup.Id = Repository.Save(ratingGroup);

				transaction.Complete();
			}

			RatingGroupCache.Instance.Remove(new RatingGroupCacheKey(ratingGroup.Id));
			RatingListCache.Instance.Remove(ratingGroup.Id, ChannelSecureService.GetAll());
			RatingGroupListCache.Instance.Flush();

			return ratingGroup;
		}

		public virtual IRatingGroup SaveWithRatings(ISystemUserFull systemUserFull, IRatingGroup ratingGroup, Collection<IRatingGroupRating> newRatings)
		{
			if (ratingGroup == null)
			{
				throw new ArgumentNullException("ratingGroup");
			}

			if (newRatings == null)
			{
				throw new ArgumentNullException("newRatings");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			RatingGroupRatingSecureService.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			var existedRatings = RatingGroupRatingSecureService.GetAllByGroup(ratingGroup.Id);

			using (var transaction = new TransactedOperation())
			{
				ratingGroup.Id = Repository.Save(ratingGroup);

				foreach (var existedRating in existedRatings)
				{
					if (newRatings.FirstOrDefault(r => r.RatingId == existedRating.RatingId && r.RatingGroupId == ratingGroup.Id) == null)
					{
						RatingGroupRatingSecureService.Delete(systemUserFull, ratingGroup.Id, existedRating.RatingId);
					}
				}

				foreach (var newRating in newRatings)
				{
					newRating.RatingGroupId = ratingGroup.Id;
					RatingGroupRatingSecureService.Save(systemUserFull, newRating);
				}

				transaction.Complete();
			}

			RatingGroupCache.Instance.Remove(new RatingGroupCacheKey(ratingGroup.Id));
			RatingListCache.Instance.Remove(ratingGroup.Id, ChannelSecureService.GetAll());
			RatingGroupListCache.Instance.Flush();

			return ratingGroup;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int ratingGroupId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(ratingGroupId);

				transaction.Complete();
			}

			RatingGroupCache.Instance.Remove(new RatingGroupCacheKey(ratingGroupId));
			RatingListCache.Instance.Remove(ratingGroupId, ChannelSecureService.GetAll());
			RatingGroupListCache.Instance.Flush();
		}

		public virtual IRatingGroup GetById(int ratingGroupId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(ratingGroupId);
		}

		public virtual Collection<IRatingGroup> GetAllByFolder(int ratingGroupFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByFolder(ratingGroupFolderId);
		}

		public virtual Collection<IRatingGroup> Search(string searchCriteria)
		{
			Repository.EnsureNotNull();

			return Repository.Search(searchCriteria);
		}
	}
}