﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupFolderSecureService : IRatingGroupFolderSecureService
	{
		protected RatingGroupFolderRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IRatingGroupSecureService RatingGroupSecureService { get; private set; }

		public RatingGroupFolderSecureService(RatingGroupFolderRepository ratingGroupFolderRepository, IAccessValidator accessValidator, IRatingGroupSecureService ratingGroupSecureService)
		{
			Repository = ratingGroupFolderRepository;
			AccessValidator = accessValidator;
			RatingGroupSecureService = ratingGroupSecureService;
		}

		public virtual IRatingGroupFolder Create()
		{
			var ratingGroupFolder = IoC.Resolve<IRatingGroupFolder>();
			ratingGroupFolder.Status = BusinessObjectStatus.New;
			return ratingGroupFolder;
		}

		public virtual IRatingGroupFolder Save(ISystemUserFull systemUserFull, IRatingGroupFolder ratingGroupFolder)
		{
			if (ratingGroupFolder == null)
			{
				throw new ArgumentNullException("ratingGroupFolder");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				ratingGroupFolder.Id = Repository.Save(ratingGroupFolder);

				transaction.Complete();
			}

			return ratingGroupFolder;
		}

		public virtual bool TryDelete(ISystemUserFull systemUserFull, int ratingGroupFolderId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			RatingGroupSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				if (!TryDeleteCore(ratingGroupFolderId))
				{
					return false;
				}

				transaction.Complete();

				return true;
			}
		}

		public virtual IRatingGroupFolder GetById(int ratingGroupFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(ratingGroupFolderId);
		}

		public virtual Collection<IRatingGroupFolder> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}

		public virtual Collection<IRatingGroupFolder> GetAllByParent(int parentRatingFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByParent(parentRatingFolderId);
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			Repository.EnsureNotNull();

			return Repository.GetTree(selectedId);
		}


		protected virtual bool TryDeleteCore(int ratingGroupFolderId)
		{
			if (RatingGroupSecureService.GetAllByFolder(ratingGroupFolderId).Count > 0)
			{
				return false;
			}

			if (!TryDeleteChildren(ratingGroupFolderId))
			{
				return false;
			}

			Repository.Delete(ratingGroupFolderId);

			return true;
		}

		protected virtual bool TryDeleteChildren(int parentRatingGroupFolderId)
		{
			foreach (IRatingGroupFolder ratingGroupFolder in Repository.GetAllByParent(parentRatingGroupFolderId))
			{
				if (!TryDeleteCore(ratingGroupFolder.Id))
				{
					return false;
				}
			}

			return true;
		}
	}
}