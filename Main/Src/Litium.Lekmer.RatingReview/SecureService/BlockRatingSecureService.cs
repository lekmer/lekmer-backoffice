﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockRatingSecureService : IBlockRatingSecureService
	{
		protected BlockRatingRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public BlockRatingSecureService(BlockRatingRepository blockRatingRepository, IAccessValidator accessValidator)
		{
			Repository = blockRatingRepository;
			AccessValidator = accessValidator;
		}

		public virtual void SaveAll(ISystemUserFull systemUserFull, int blockId, Collection<IBlockRating> ratings)
		{
			if (ratings == null)
			{
				throw new ArgumentNullException("ratings");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(blockId);

				foreach (var rating in ratings)
				{
					Repository.Save(blockId, rating.RatingId);
				}

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(blockId);

				transaction.Complete();
			}
		}

		public virtual Collection<IBlockRating> GetAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByBlock(blockId);
		}
	}
}