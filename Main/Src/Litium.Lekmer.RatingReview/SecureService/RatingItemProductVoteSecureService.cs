﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingItemProductVoteSecureService : IRatingItemProductVoteSecureService
	{
		protected RatingItemProductVoteRepository Repository { get; private set; }

		public RatingItemProductVoteSecureService(RatingItemProductVoteRepository ratingItemRepository)
		{
			Repository = ratingItemRepository;
		}

		public virtual Collection<IRatingItemProductVote> GetAllByFeedback(int ratingReviewFeedbackId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByFeedback(ratingReviewFeedbackId);
		}
	}
}