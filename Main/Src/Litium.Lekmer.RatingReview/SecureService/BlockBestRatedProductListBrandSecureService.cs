using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class BlockBestRatedProductListBrandSecureService : IBlockBestRatedProductListBrandSecureService
	{
		protected BlockBestRatedProductListBrandRepository Repository { get; private set; }

		public BlockBestRatedProductListBrandSecureService(BlockBestRatedProductListBrandRepository repository)
		{
			Repository = repository;
		}

		public virtual void Save(int blockId, Collection<IBlockBestRatedProductListBrand> blockBrands)
		{
			if (blockBrands == null)
			{
				throw new ArgumentNullException("blockBrands");
			}

			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAll(blockId);

				foreach (var blockBrand in blockBrands)
				{
					blockBrand.BlockId = blockId;

					Repository.Save(blockBrand);
				}

				transaction.Complete();
			}
		}

		public Collection<IBlockBestRatedProductListBrand> GetAllByBlock(int blockId)
		{
			return Repository.GetAllByBlockSecure(blockId);
		}
	}
}