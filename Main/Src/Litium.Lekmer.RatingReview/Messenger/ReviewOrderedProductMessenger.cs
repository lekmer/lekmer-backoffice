﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;

namespace Litium.Lekmer.RatingReview
{
	public class ReviewOrderedProductMessenger : MessengerBase<ReviewOrderedProductMessageArgs>
	{
		private string _customerName;

		protected ReviewOrderedProductEmailTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "ReviewOrderedProduct"; }
		}

		protected override Message CreateMessage(ReviewOrderedProductMessageArgs messageArgs)
		{
			if (messageArgs == null
				|| messageArgs.Order.Customer == null 
				|| messageArgs.Order.Customer.CustomerInformation == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			string firstName = messageArgs.Order.Customer.CustomerInformation.FirstName ?? string.Empty;
			string lastName = messageArgs.Order.Customer.CustomerInformation.LastName ?? string.Empty;
			_customerName = firstName + " " + lastName;

			Template = new ReviewOrderedProductEmailTemplate(messageArgs.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name = _customerName,
				Address = messageArgs.Order.Email
			});

			return message;
		}

		private string RenderMessage(ReviewOrderedProductMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("Message");

			htmlFragment.AddVariable("Customer.Name", _customerName);
			htmlFragment.AddVariable("Order.Number", messageArgs.Order.Number);
			htmlFragment.AddVariable("Order.CreatedDate", messageArgs.Order.CreatedDate.Date.ToString(CultureInfo.InvariantCulture));
			htmlFragment.AddVariable("FeedbackUrl", string.Format(CultureInfo.CurrentCulture,
				"http://{0}/{1}/?token={2}&t={3}", messageArgs.Channel.ApplicationName, messageArgs.PageUrl, messageArgs.Order.FeedbackToken, DateTime.Now.Ticks));

			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}