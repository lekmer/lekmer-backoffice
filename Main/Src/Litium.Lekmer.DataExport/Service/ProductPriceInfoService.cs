using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.DataExport.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.DataExport
{
	public class ProductPriceInfoService : IProductPriceInfoService
	{
		protected ProductPriceInfoRepository Repository { get; private set; }

		public ProductPriceInfoService(ProductPriceInfoRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IProductPriceInfo> GetAll(IUserContext context)
		{
			context.EnsureNotNull();

			return Repository.GetAll(context.Channel.Id);
		}

		public virtual void Save(IUserContext context, Collection<IProductPriceInfo> productPriceInfoList)
		{
			context.EnsureNotNull();
			productPriceInfoList.EnsureNotNull();

			int channelId = context.Channel.Id;
			var productIdsToRemove = new ProductIdCollection();

			foreach (IProductPriceInfo productPriceInfo in productPriceInfoList)
			{
				if (channelId != productPriceInfo.ChannelId)
				{
					throw new InvalidOperationException("The ProductPriceInfo.ChannelId differs from UserContext.Channel.Id");
				}

				if (productPriceInfo.IsNew || productPriceInfo.IsEdited)
				{
					Repository.Save(productPriceInfo);
				}
				else if (productPriceInfo.IsDeleted)
				{
					productIdsToRemove.Add(productPriceInfo.ProductId);
				}
			}

			Repository.Delete(channelId, Convert.ToStringIdentifierList(productIdsToRemove), Convert.DefaultListSeparator);
		}
	}
}