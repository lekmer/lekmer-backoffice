﻿using System;
using System.Reflection;
using Litium.Lekmer.RatingReview;
using log4net;
using log4net.Config;

namespace Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main()
		{
			try
			{
				XmlConfigurator.Configure();
				_log.InfoFormat("Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console started.");

				Execute();

				_log.InfoFormat("Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console completed.");
			}
			catch (Exception ex)
			{
				_log.Error("Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console failed.", ex);
				throw;
			}
		}

		private static void Execute()
		{
			var setting = new GiftCardViaEmailMessengerSetting();
			var giftCardViaEmailMessengerHelper = new GiftCardViaEmailMessengerHelper(setting.MaxVoucherQuantity(), setting.ValidInDays(), setting.NumberOfTokens(), _log);
			giftCardViaEmailMessengerHelper.ProcessMessages();
		}
	}
}