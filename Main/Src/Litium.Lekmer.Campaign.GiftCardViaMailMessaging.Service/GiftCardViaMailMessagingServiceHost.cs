﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using Litium.Lekmer.Common.Job;
using log4net;
using log4net.Config;

namespace Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service
{
	public partial class GiftCardViaMailMessagingServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private object _syncToken = new object();
		private Collection<IJob> _jobs = new Collection<IJob>();

		public GiftCardViaMailMessagingServiceHost()
		{
			InitializeComponent();

			XmlConfigurator.Configure();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			try
			{
				foreach (IJob job in _jobs)
				{
					job.StopJob();
				}

				_log.Info("Service stopped successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Error occurred while stopping service.", ex);
			}
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var giftCardViaMailJob = new GiftCardViaMailJob
				{
					ScheduleSetting = new GiftCardViaMailScheduleSetting(),
					IntervalCalculator = intervalCalculator,
					SyncToken = _syncToken,
					Log = _log
				};
			_jobs.Add(giftCardViaMailJob);
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}