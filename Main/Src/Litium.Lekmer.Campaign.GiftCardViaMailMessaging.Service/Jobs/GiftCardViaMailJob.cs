﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.RatingReview;
using log4net;

namespace Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service
{
	public class GiftCardViaMailJob : BaseJob
	{
		private GiftCardViaEmailMessengerSetting _giftCardViaEmailMessengerSetting;
		protected GiftCardViaEmailMessengerSetting GiftCardViaEmailMessengerSetting
		{
			get { return _giftCardViaEmailMessengerSetting ?? (_giftCardViaEmailMessengerSetting = new GiftCardViaEmailMessengerSetting()); }
		}

		public ILog Log { get; set; }

		public override string Name
		{
			get { return "GiftCardViaMail"; }
		}

		protected override void ExecuteAction()
		{
			var giftCardViaEmailMessengerHelper = new GiftCardViaEmailMessengerHelper(GiftCardViaEmailMessengerSetting.MaxVoucherQuantity(), GiftCardViaEmailMessengerSetting.ValidInDays(), GiftCardViaEmailMessengerSetting.NumberOfTokens(), Log);
			giftCardViaEmailMessengerHelper.ProcessMessages();
		}
	}
}
