﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service
{
	[RunInstaller(true)]
	public partial class GiftCardViaMailMessagingServiceHostInstaller : Installer
	{
		public GiftCardViaMailMessagingServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}