﻿namespace Litium.Lekmer.DataExport
{
	public interface IProductPriceInfoExportService
	{
		void ExportProductPriceInfo();
	}
}
