using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Product;

namespace Litium.Lekmer.GoogleFeed
{
	public interface IGoogleFeedInfo
	{
		ILekmerProductView Product { get; set; }
		ISizeTable SizeTable { get; set; }
		Collection<IProductImageGroupFull> ImageGroups { get; set; }
		string FirstLevelCategoryTitle { get; set; }
		string SecondLevelCategoryTitle { get; set; }
		string ThirdLevelCategoryTitle { get; set; }
	}
}