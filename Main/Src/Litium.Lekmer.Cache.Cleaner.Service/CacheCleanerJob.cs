﻿using System;
using Litium.Lekmer.Cache.UpdateFeed;
using Litium.Lekmer.Cache.Setting;
using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Cache.Cleaner.Service
{
	public class CacheCleanerJob : BaseJob
	{
		private ILekmerCacheUpdateFeedSecure _cacheUpdateFeedSecure;
		private LekmerCacheUpdateCleanerSetting _cacheCleanerSetting;

		public override string Group
		{
			get { return "Cache"; }
		}

		public override string Name
		{
			get { return "CacheCleanerJob"; }
		}

		public virtual ILekmerCacheUpdateFeedSecure CacheUpdateFeedSecure
		{
			get { return _cacheUpdateFeedSecure ?? (_cacheUpdateFeedSecure = IoC.Resolve<ILekmerCacheUpdateFeedSecure>()); }
			set { _cacheUpdateFeedSecure = value; }
		}

		public virtual LekmerCacheUpdateCleanerSetting CacheCleanerSetting
		{
			get { return _cacheCleanerSetting ?? (_cacheCleanerSetting = new LekmerCacheUpdateCleanerSetting()); }
			set { _cacheCleanerSetting = value; }
		}

		protected override void ExecuteAction()
		{
			CacheUpdateFeedSecure.DeleteOldUpdateItems(DateTime.UtcNow.AddMinutes(-CacheCleanerSetting.CacheUpdateTtlMinutes));
		}
	}
}
