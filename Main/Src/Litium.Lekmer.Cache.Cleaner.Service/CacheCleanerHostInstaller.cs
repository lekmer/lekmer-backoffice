﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Litium.Lekmer.Cache.Cleaner.Service
{
	[RunInstaller(true)]
	public partial class CacheCleanerHostInstaller : System.Configuration.Install.Installer
	{
		public CacheCleanerHostInstaller()
		{
			InitializeComponent();
		}
	}
}
