﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.NewsletterSubscriber.Service
{
	public class NewsletterSubscriberScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "NewsletterSubscriber"; }
		}

		protected override string GroupName
		{
			get { return "NewsletterSubscriberJob"; }
		}
	}
}