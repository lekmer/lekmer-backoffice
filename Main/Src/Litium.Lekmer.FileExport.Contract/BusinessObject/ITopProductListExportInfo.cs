using System.Collections.ObjectModel;

namespace Litium.Lekmer.FileExport
{
	public interface ITopProductListExportInfo
	{
		string PageTitle { get; set; }
		string BlockTitle { get; set; }
		Collection<IProductExportInfo> Products { get; set; }
	}
}