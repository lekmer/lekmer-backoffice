﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.FileExport.Service
{
	public interface ISitemapService
	{
		Collection<ISitemapItem> GetItems(IUserContext context);
	}
}
