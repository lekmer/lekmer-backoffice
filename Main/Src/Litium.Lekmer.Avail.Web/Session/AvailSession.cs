﻿using System.Web;
using System.Web.SessionState;
using Litium.Lekmer.Core.Web;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailSession : IAvailSession
	{
		private static HttpSessionState Session
		{
			get { return HttpContext.Current.Session; }
		}

		public virtual int? AvailProductId
		{
			get
			{
				if (Session == null) return null;
				return Session["AvailProductId"] as int?;
			}
			set
			{
				if (Session == null) return;
				Session["AvailProductId"] = value;
			}
		}

		public virtual string TrackingCode
		{
			get
			{
				if (Session == null) return null;
				return Session["AvailTrackingCode"] as string;
			}
			set
			{
				if (Session == null) return;
				Session["AvailTrackingCode"] = value;
			}
		}

		public virtual string AvailSessionId
		{
			get
			{
				if (Session == null) return null;
				return Session["AvailSessionId"] as string;
			}
			set
			{
				if (Session == null) return;
				Session["AvailSessionId"] = value;
			}
		}
	}
}
