﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Litium.Lekmer.Avail.Setting;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;
using Channel = Litium.Scensum.Core.Web.Channel;

namespace Litium.Lekmer.Avail.Web.Helper
{
	public class AvailLoggingHelper
	{
		private readonly string _quote = @"""";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		
		private AvailSetting _availSetting;
		protected AvailSetting AvailSetting
		{
			get { return _availSetting ?? (_availSetting = new AvailSetting("AvailSettings")); }
		}

		private string _availSessionId;
		protected string AvailSessionId
		{
			get
			{
				if (_availSessionId == null)
				{
					var availSessionId = AvailSessionCookie.GetAvailSession();
					if (availSessionId != null)
					{
						_availSessionId = Scensum.Foundation.Utilities.Encoder.EncodeJavaScript(availSessionId);
					}
					else
					{
						_log.Warn("Avail SessionId can not find.");
						_availSessionId = null;
					}
				}

				return _availSessionId;
			}
		}

		protected string IdentificationId
		{
			get
			{
				var customerSession = IoC.Resolve<ICustomerSession>();
				var customerIdentificationId = CustomerIdentificationCookie.GetCustomerIdentificationId(customerSession.SignedInCustomer);

				if (customerIdentificationId.HasValue())
				{
					customerIdentificationId = Scensum.Foundation.Utilities.Encoder.EncodeJavaScript(customerIdentificationId);
				}

				return customerIdentificationId;
			}
		}


		public bool SendAvailLogAddedToCart(int productId, string trackingCode)
		{
			var valid = false;

			try
			{
				if (UseServerLogging())
				{
					string availTrackingCode = string.Empty;
					if (!string.IsNullOrEmpty(trackingCode))
					{
						availTrackingCode = string.Format(@",""TrackingCode"":""{0}""", trackingCode);
					}

					if (AvailSessionId != null)
					{
						var parameters = "?r=js&s=" + AvailSessionId + @"&q={""ret0"":[""logAddedToCart"",{""Product"":""" + productId + @"""" + availTrackingCode + "}]}";
						valid = SendRequest(parameters, "Logged");
					}
				}
			}
			catch (Exception exception)
			{
				_log.Error("Error while send LogAddedToCart to Avail.", exception);
			}

			return valid;
		}

		public bool SendAvailLogRemovedFromCart(int productId)
		{
			var valid = false;

			try
			{
				if (UseServerLogging())
				{
					var cart = IoC.Resolve<ICartSession>().Cart as ILekmerCartFull;
					if (cart != null)
					{
						var cartItems = cart.GetCartItems();
						var sameCartItem = cartItems.FirstOrDefault(cartItem => cartItem.Product.Id.Equals(productId));
						if (sameCartItem != null)
						{
							return false;
						}
					}

					if (AvailSessionId != null)
					{
						var parameters = "?r=js&s=" + AvailSessionId + @"&q={""ret0"":[""logRemovedFromCart"",{""Product"":""" + productId + @"""}]}";
						valid = SendRequest(parameters, string.Format("{0} removed from cart", productId));
					}
				}
			}
			catch (Exception exception)
			{
				_log.Error("Error while send LogRemovedFromCart to Avail.", exception);
			}

			return valid;
		}

		public bool SendAvailLogPurchase(IOrderFull order)
		{
			var valid = false;

			try
			{
				if (UseServerLogging() && order != null)
				{
					var orderItems = order.GetOrderItems();
					if (orderItems.Count > 0)
					{
						var productIds = new StringBuilder();
						var prices = new StringBuilder();
						int counter = 1;
						foreach (var item in orderItems)
						{
							if (counter < orderItems.Count)
							{
								productIds.Append(_quote + item.ProductId + _quote + ",");
								prices.Append(_quote + item.ActualPrice.ExcludingVat.ToString("0.00", CultureInfo.InvariantCulture) + _quote + ",");
							}
							else
							{
								productIds.Append(_quote + item.ProductId + _quote);
								prices.Append(_quote + item.ActualPrice.ExcludingVat.ToString("0.00", CultureInfo.InvariantCulture) + _quote);
							}
							counter++;
						}

						if (AvailSessionId != null)
						{
							string identificationId = IdentificationId ?? string.Empty;

							var parameters = "?r=js&s=" + AvailSessionId + @"&q={""ret0"":[""logPurchase"",{""User"":""" + identificationId + @""",""Products"":[" + productIds + @"],""Prices"":[" + prices + @"],""OrderID"":""" + order.Id + @""",""Currency"":""" + Channel.Current.Currency.Iso + @"""}]}";

							valid = SendRequest(parameters, "Logged");
						}
					}
				}
			}
			catch (Exception exception)
			{
				_log.Error("Error while send LogPurchase to Avail.", exception);
			}

			return valid;
		}


		private bool UseServerLogging()
		{
			bool useServerLogging;
			return bool.TryParse(AvailSetting.UseServerLogging(), out useServerLogging) && useServerLogging;
		}

		private string GetPostUri(string availCustomerId)
		{
			return string.Format("http://service.avail.net/2009-02-13/dynamic/{0}/scr", availCustomerId);
		}

		private bool SendRequest(string parameters, string validationMessage)
		{
			bool valid = false;

			var availCustomerId = AvailSetting.CustomerId();
			if (!string.IsNullOrEmpty(availCustomerId))
			{
				var request = (HttpWebRequest)WebRequest.Create(GetPostUri(availCustomerId) + parameters);
				var response = (HttpWebResponse)request.GetResponse();

				if (response.StatusCode == HttpStatusCode.OK)
				{
					Stream responseStream = response.GetResponseStream();
					if (responseStream != null)
					{
						var reader = new StreamReader(responseStream, Encoding.UTF8);
						string initialResponse = reader.ReadToEnd();
						valid = initialResponse.Contains(validationMessage);
						reader.Close();
					}
				}
				response.Close();
			}

			return valid;
		}
	}
}