﻿using System;
using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Avail.Web.Component
{
    public class ComponentAvailCartPredictions : ComponentControl
    {
        public ComponentAvailCartPredictions(ITemplateFactory templateFactory) 
            : base(templateFactory)
        {
        }

        protected override string ModelCommonName
        {
            get { return "AvailCartPredictionsComponent"; }
        }

        public override ComponentContent Render()
        {
            var cart = IoC.Resolve<ICartSession>().Cart;
            if (cart == null)
            {
                return new ComponentContent();
            }
            var cartItems = IoC.Resolve<ICartSession>().Cart.GetCartItems();

            if (cartItems.Count <= 0)
            {
                return new ComponentContent();
            }

            var fragmentHead = Template.GetFragment("Head");
           // fragmentHead.AddEntity(Component);
            fragmentHead.AddVariable("Param.Cart.Items", RenderCartItemsArrayParam());
            var fragmentContent = Template.GetFragment("Content");
            //fragmentContent.AddEntity(Component);
            return new ComponentContent(fragmentHead.Render(), fragmentContent.Render());
        }

        private static string RenderCartItemsArrayParam()
        {
            var itemBuilder = new StringBuilder();

            var cart = IoC.Resolve<ICartSession>().Cart;
            if (cart == null)
            {
                return null;
            }

            var cartItems = IoC.Resolve<ICartSession>().Cart.GetCartItems();

            itemBuilder.Append("new Array(");

            for (int i = 0; i < cartItems.Count; i++)
            {
                if (i == 0)//first or the only item in array
                {
                    itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", cartItems[i].Product.Id));
                }
                else
                {
                    itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", cartItems[i].Product.Id));
                }
            }

            itemBuilder.Append(")");

            return itemBuilder.ToString();

        }

    }
}