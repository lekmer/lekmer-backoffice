﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web.AvailCombine
{
    public class AvailGridControl<TItem> : GridControl<TItem> where TItem : class
    {	/// <summary>
        /// Name of row fragment. Will also be variable name in list fragment prefixed by "Iterate:".
        /// </summary>
        public bool WasSearched { get; set; }

        /// <summary>
        /// Name of item fragment. Will also be variable name in row fragment prefixed by "Iterate:".
        /// </summary>
        public string TrackingCode { get; set; }
        public string RelativeUrl { get; set; }
        public int? ContentNodeId { get; set; }
        public string Addedtobasket { get; set; }
        public bool IsSecureConnection { get; set; }
        /// <summary>
        /// Renders an item.
        /// Uses the ItemFragmentName fragment. It renders the item with an entity mapper for the item.
        /// It also has the conditions IsFirst and IsLast.
        /// </summary>
        /// <param name="columnIndex">The index of the column. Index zero is the first column.</param>
        /// <param name="item">The item to render</param>
        /// <returns>Rendered empty space.</returns>
        protected override string RenderItem(int columnIndex, TItem item)
        {
            Fragment fragmentItem = Template.GetFragment(ItemFragmentName);
            AddPositionCondition(fragmentItem, columnIndex, ColumnCount);
            fragmentItem.AddEntity(item);
            fragmentItem.AddEntity(Channel.Current);
            fragmentItem.AddVariable("TrackingCode", TrackingCode);
            fragmentItem.AddCondition("Product.IsRecommended", !string.IsNullOrEmpty(TrackingCode));
            fragmentItem.AddCondition("WasSearched", WasSearched);
            fragmentItem.AddEntity(Channel.Current);
            fragmentItem.AddVariable("Page.CurrentRelativeUrl", RelativeUrl, VariableEncoding.None);
            fragmentItem.AddVariable("Page.CurrentRelativeUrl.UrlEncode", RelativeUrl, VariableEncoding.UrlEncode);
            fragmentItem.AddCondition("Page.HttpsEnabled", IsSecureConnection);
            fragmentItem.AddVariable("Addedtobasket", Addedtobasket);
            fragmentItem.AddVariable("ActiveContentNodeId", ContentNodeId.HasValue ? ContentNodeId.Value.ToString() : string.Empty);
            return fragmentItem.Render();
        }
    }
}
