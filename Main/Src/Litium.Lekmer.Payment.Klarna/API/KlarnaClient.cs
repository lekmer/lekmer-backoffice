﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Klarna.Core;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;
using Address = Klarna.Core.Address;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaClient : IKlarnaClient
	{
		private IKlarnaSetting _klarnaSetting;
		private Encoding _klarnaEncoding;

		protected Encoding KlarnaEncoding
		{
			get
			{
				// Klarna uses iso-8859-1 encoding for product titles.
				return _klarnaEncoding ?? (_klarnaEncoding = Encoding.GetEncoding("iso-8859-1"));
			}
		}

		protected IKlarnaApi KlarnaApi { get; set; }

		public IKlarnaSetting KlarnaSetting
		{
			get { return _klarnaSetting; }
			set
			{
				_klarnaSetting = value;
				KlarnaApi.KlarnaSetting = value;
			}
		}


		public KlarnaClient(IKlarnaApi klarnaApi)
		{
			KlarnaApi = klarnaApi;
		}


		public virtual void FetchPClasses()
		{
			KlarnaApi.FetchPClasses();
		}

		public virtual List<object> GetPClasses()
		{
			return KlarnaApi.GetPClasses();
		}

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Norway, Sweden and Denmark.
		/// </summary>
		public virtual IKlarnaGetAddressResult GetAddress(IChannel channel, string civicNumber, string ip)
		{
			ValidateArgument(civicNumber);

			// Format civic so Kreditor will accept it.
			civicNumber = FormatCivicNumber(civicNumber, channel);

			int pnoEncoding = GetCivicNumberEncoding(channel);
			IKlarnaGetAddressResponse response = KlarnaApi.GetAddress(civicNumber, pnoEncoding, ip);

			return ConvertResponseToGetAddressResult(response);
		}

		/// <summary>
		/// Reserves an amount at Kreditor. Can only be used in Sweden, Denmark, Norway and Finland.
		/// Mattias added Bookplus
		/// </summary>
		public virtual IKlarnaReservationResult ReserveAmount(IChannel channel, ICustomer customer, IOrderFull order, int partPaymentId, decimal totalAmount, double? yearlySalary)
		{
			KlarnaApi.CreateOrderValueSpecificClient((double) totalAmount);

			ValidateArguments(customer, order);

			var lekmerOrderFull = (ILekmerOrderFull)order;

			// Set delivery address for kreditor api.
			SetAddress(channel, customer, order.DeliveryAddress, false);
			// Set billing address for kreditor api.
			SetAddress(channel, customer, order.BillingAddress, true);

			// Make goods list (product list) for kreditor api.
			MakeGoods(order.GetOrderItems(), lekmerOrderFull.GetTotalFreightCost());

			string civicNumber = customer.CustomerInformation.CivicNumber;
			// Format civicnumber so kreditor will accept it.
			civicNumber = FormatCivicNumber(civicNumber, channel);
			bool isCompany = customer.CustomerInformation.IsCompany();
			int? gender = GetGenterType(channel, customer);

			IKlarnaReservationResult result;

			if (isCompany)
			{
				var orderBillingAddress = (ILekmerOrderAddress)order.BillingAddress;
				result = KlarnaApi.ReserveAmountCompany(civicNumber, gender, (double) totalAmount, orderBillingAddress.Reference ?? "", order.Id, order.IP, partPaymentId);
			}
			else
			{
				result = KlarnaApi.ReserveAmount(civicNumber, gender, (double) totalAmount, order.Id, order.IP, partPaymentId, yearlySalary ?? 0);
			}

			return result;
		}

		public virtual IKlarnaResult CancelReservation(int orderId, string reservationNumber)
		{
			IKlarnaResult klarnaResult = KlarnaApi.CancelReservation(orderId, reservationNumber);
			return klarnaResult;
		}

		public virtual IKlarnaResult CheckOrderStatus(int orderId, string reservationNumber)
		{
			IKlarnaResult klarnaResult = KlarnaApi.CheckOrderStatus(orderId, reservationNumber);
			return klarnaResult;
		}

		public virtual double CalculateCheapestMonthlyFee()
		{
			return KlarnaApi.CalculateCheapestMonthlyFee();
		}
		public virtual double CalculateCheapestMonthlyCost(double sum, bool isCheckOut)
		{
			var flags = (int) (isCheckOut ? API.CalculateOn.CheckoutPage : API.CalculateOn.ProductPage);
			return KlarnaApi.CalculateCheapestMonthlyCost(sum, flags);
		}
		public virtual double CalculateMonthlyCost(double sum, object pClass, bool isCheckOut)
		{
			var flags = (int)(isCheckOut ? API.CalculateOn.CheckoutPage : API.CalculateOn.ProductPage);
			return KlarnaApi.CalculateMonthlyCost(sum, pClass, flags);
		}

		public virtual void Initialize(string channelCommonName)
		{
			KlarnaApi.Initialize(channelCommonName);
			KlarnaSetting.Initialize(channelCommonName);
		}


		private void SetAddress(IChannel channel, ICustomer customer, IOrderAddress address, bool isBillingAddress)
		{
			string houseNumber = string.Empty;
			string houseExtension = string.Empty;

			var lekmerOrderAddress = address as ILekmerOrderAddress;
			if(lekmerOrderAddress != null)
			{
				houseNumber = lekmerOrderAddress.HouseNumber;
				houseExtension = lekmerOrderAddress.HouseExtension;
			}

			string cellNo = string.IsNullOrEmpty(customer.CustomerInformation.CellPhoneNumber) ? string.Empty : customer.CustomerInformation.CellPhoneNumber;

			string firstName = "";
			string lastName = "";
			string company = "";

			if (customer.CustomerInformation.IsCompany())
			{
				company = customer.CustomerInformation.LastName;
			}
			else
			{
				firstName = customer.CustomerInformation.FirstName;
				lastName = customer.CustomerInformation.LastName;
			}

			if (isBillingAddress)
			{
				KlarnaApi.SetBillingAddress(
					customer.CustomerInformation.Email,
					"", // telNo
					cellNo,
					firstName,
					lastName,
					"", // careof
					company,
					address.StreetAddress,
					address.PostalCode,
					address.City,
					channel.Country.Iso,
					houseNumber ?? string.Empty,
					houseExtension ?? string.Empty);
			}
			else
			{
				KlarnaApi.SetShippingAddress(
					customer.CustomerInformation.Email,
					"", // telNo
					cellNo,
					firstName,
					lastName,
					"", // careof
					company,
					address.StreetAddress,
					address.PostalCode,
					address.City,
					channel.Country.Iso,
					houseNumber ?? string.Empty,
					houseExtension ?? string.Empty);
			}
		}

		private void MakeGoods(ICollection<IOrderItem> orderRowList, decimal freightCost)
		{
			foreach (ILekmerOrderItem orderRow in orderRowList)
			{
				if (ProductExtensions.IsProduct(orderRow.ProductTypeId))
				{
					AddArticle(orderRow);
				}
				else if (ProductExtensions.IsPackage(orderRow.ProductTypeId))
				{
					AddPackage(orderRow);
				}
				else
				{
					throw new InvalidOperationException("Unknown product type!");
				}
			}

			KlarnaApi.AddShipment(1, "", "Shipping", (double)freightCost, 25);
		}


		private void AddArticle(ILekmerOrderItem orderItem)
		{
			string rowDescription = FormatArticleTitle(orderItem.Title);
			double vatPercent = (double)orderItem.GetOriginalVatPercent();
			double dicountPercent = orderItem.RowDiscountPercent(true);

			string erpId = orderItem.OrderItemSize.SizeId.HasValue ? orderItem.OrderItemSize.ErpId : orderItem.ErpId + "-000";

			KlarnaApi.AddArticle(
				orderItem.Quantity,
				erpId,
				rowDescription,
				(double)orderItem.OriginalPrice.IncludingVat,
				vatPercent,
				dicountPercent);
		}

		private void AddPackage(ILekmerOrderItem orderItem)
		{
			if (orderItem.PackageOrderItems == null)
			{
				throw new InvalidOperationException("lekmerOrderItem.PackageOrderItems can't be null when it is package");
			}

			string rowDescription = FormatArticleTitle(orderItem.Title);
			double vatPercent = (double)orderItem.GetOriginalVatPercent();
			decimal packageDiscount = orderItem.ActualPrice.IncludingVat;

			foreach (IPackageOrderItem packageOrderItem in orderItem.PackageOrderItems)
			{
				AddPackageItem(packageOrderItem);
				packageDiscount -= packageOrderItem.OriginalPrice.IncludingVat;
			}

			string erpId = orderItem.OrderItemSize.SizeId.HasValue ? orderItem.OrderItemSize.ErpId : orderItem.ErpId + "-000";

			KlarnaApi.AddArticle(
				orderItem.Quantity,
				erpId,
				rowDescription,
				(double)packageDiscount,
				vatPercent,
				0.0 // dicountPercent
			);
		}

		private void AddPackageItem(IPackageOrderItem packageOrderItem)
		{
			string rowDescription = FormatArticleTitle(packageOrderItem.Title);
			double vatPercent = (double)packageOrderItem.GetOriginalVatPercent();

			string erpId = packageOrderItem.SizeId.HasValue ? packageOrderItem.SizeErpId : packageOrderItem.ErpId + "-000";

			KlarnaApi.AddArticle(
				packageOrderItem.Quantity,
				erpId,
				rowDescription,
				(double)packageOrderItem.OriginalPrice.IncludingVat,
				vatPercent,
				0.0 // dicountPercent
			);
		}

		private string FormatArticleTitle(string title)
		{
			if (title.HasValue())
			{
				byte[] bytes = KlarnaEncoding.GetBytes(title);
				title = KlarnaEncoding.GetString(bytes);
			}
			else
			{
				title = string.Empty;
			}

			return title;
		}

		private void ValidateArgument(string argument)
		{
			if (argument == null)
			{
				throw new ArgumentNullException("argument");
			}
		}
		private void ValidateArguments(ICustomer customer, IOrder order)
		{
			if (customer == null)
			{
				throw new ArgumentNullException("customer");
			}

			if (order == null)
			{
				throw new ArgumentNullException("order");
			}
		}

		/// <summary>
		/// Formats a civicnumber so Kreditor can accept it. In Sweden they accept "-" signs and a year format of 1950. 
		/// In Denmark, Norway and Finland they dont accept year format of 1950, here we need to remove 19. Also "-" signs
		/// are not allowed.
		/// </summary>
		private string FormatCivicNumber(string civicNumber, IChannel channel)
		{
			var countryIso = channel.Country.Iso.ToLower();
			if (string.Compare(countryIso, "se", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "fi", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber;
			}

			if (string.Compare(countryIso, "nl", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "dk", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "no", StringComparison.InvariantCultureIgnoreCase) == 0 )
			{
				// Kreditor doesn't seem to like - signs.
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber.Replace("-", "");
			}

			// If country isn't supported, return input string.
			return civicNumber;
		}
		private int GetCivicNumberEncoding(IChannel channel)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "nl":
					return (int)API.Encoding.Dutch;
				case "dk":
					return (int)API.Encoding.Danish;
				case "fi":
					return (int)API.Encoding.Finnish;
				case "no":
					return (int)API.Encoding.Norwegian;
				case "se":
					return (int)API.Encoding.Swedish;
				default:
					throw new ArgumentException(string.Format("CountryISO: \"{0}\" not supported.", channel.Country.Iso));
			}
		}
		private int? GetGenterType(IChannel channel, ICustomer customer)
		{
			int? gender = null;

			var lekmerCustomerInformation = customer.CustomerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				if (string.Compare(channel.Country.Iso.ToLower(), "nl", StringComparison.InvariantCultureIgnoreCase) == 0)
				{
					if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.Man)
					{
						gender = (int?) API.Gender.Male; //1
					}
					else if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.Woman)
					{
						gender = (int?) API.Gender.Female; //0
					}
				}
			}

			return gender;
		}

		/// <summary>
		/// Converts a Kreditor response item to a IAddress
		/// </summary>
		private IKlarnaGetAddressResult ConvertResponseToGetAddressResult(IKlarnaGetAddressResponse response)
		{
			var klarnaAddresses = new Collection<IKlarnaAddress>();

			if (response.Addresses != null && response.Addresses.Count > 0)
			{
				foreach (var responseAddress in response.Addresses)
				{
					var address = responseAddress as Address;
					if (address != null)
					{
						klarnaAddresses.Add(new KlarnaAddress
							{
								FirstName = address.Firstname,
								LastName = address.Lastname,
								StreetAddress = address.Street,
								PostalCode = address.ZipCode,
								City = address.City,
								IsCompany = address.IsCompany,
								CompanyName = address.Company
							});
					}
				}
			}

			var result = new KlarnaGetAddressResult
			{
				ResponseCode = response.ResponseCode,
				FaultCode = response.FaultCode,
				FaultReason = response.FaultReason,
				KlarnaAddresses = klarnaAddresses,
				Duration = response.Duration,
				KlarnaTransactionId = response.KlarnaTransactionId
			};

			return result;
		}
	}
}

