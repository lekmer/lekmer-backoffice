﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Newtonsoft.Json.Linq;
using log4net;
using KlarnaCheckout = Klarna.Checkout;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaCheckoutClient : IKlarnaCheckoutClient
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Encoding _klarnaEncoding;

		protected Encoding KlarnaEncoding
		{
			get
			{
				// Klarna uses iso-8859-1 encoding for product titles.
				return _klarnaEncoding ?? (_klarnaEncoding = Encoding.GetEncoding("iso-8859-1"));
			}
		}

		private ILekmerProductService _productService;
		protected ILekmerProductService ProductService
		{
			get { return _productService ?? (_productService = (ILekmerProductService)IoC.Resolve<IProductService>()); }
		}

		private IProductSizeService _productSizeService;
		protected IProductSizeService ProductSizeService
		{
			get { return _productSizeService ?? (_productSizeService = IoC.Resolve<IProductSizeService>()); }
		}

		public IKlarnaCheckoutSetting KlarnaSetting { get; set; }

		public virtual string GetKlarnaCheckoutContent(IUserContext context, List<ICartItem> items, decimal freightCost, decimal? discountAmount, ref Uri orderLocation)
		{
			string klarnaContent = string.Empty;

			try
			{
				Dictionary<string, object> klarnaCart = CreateKlarnaCart(context, items, freightCost, discountAmount);

				KlarnaCheckout.Order order = null;

				Uri resourceUri = null;
				if (orderLocation != null)
				{
					resourceUri = orderLocation;
				}

				if (resourceUri != null)
				{
					try
					{
						order = MakeOrder(resourceUri);
						order.Fetch();
						// Reset cart.
						var data = new Dictionary<string, object> { { "cart", klarnaCart } };
						order.Update(data);
					}
					catch (Exception ex)
					{
						order = null;
						orderLocation = null;
						_log.Error("Error occured while order updating", ex);
					}
				}

				if (order == null)
				{
					var merchant = new Dictionary<string, object>
						{
							{"id", KlarnaSetting.EID.ToString(CultureInfo.InvariantCulture)},
							{"terms_uri", KlarnaSetting.TermsUrl},
							{"checkout_uri", KlarnaSetting.CheckoutUrl},
							{"confirmation_uri", KlarnaSetting.ConfirmationUrl},
							{"push_uri", KlarnaSetting.PushUrl}
						};

					var klarnaGui = new Dictionary<string, object>
						{
							{"layout", IoC.Resolve<ICommonSession>().IsViewportMobile ? "mobile" : "desktop"}
						};

					var data = new Dictionary<string, object>
						{
							{"purchase_country", context.Channel.Country.Iso},
							{"purchase_currency", context.Channel.Currency.Iso},
							{"locale", GetLocale(context.Channel)},
							{"merchant", merchant},
							{"cart", klarnaCart},
							{"gui", klarnaGui}
						};

					order = MakeOrder(null);
					order.Create(data);
					order.Fetch();
				}

				orderLocation = order.Location;

				// Checkout content.
				var gui = order.GetValue("gui") as JObject;
				if (gui != null)
				{
					var snippet = gui["snippet"];
					if (snippet != null)
					{
						klarnaContent = snippet.ToString();
					}
				}
			}
			catch (KlarnaCheckout.ConnectorException ex)
			{
				var webException = ex.InnerException as WebException;
				if (webException != null)
				{
					// Here you can check for timeouts, and other connection related errors.
					// webException.Response could contain the response object.
					_log.Error("Error occured while klarna checkout processing", ex);
				}
				else
				{
					// In case there wasn't a WebException where you could get the response
					// (e.g. a protocol error, bad digest, etc) you might still be able to
					// get a hold of the response object.
					// ex.Data["Response"] as IHttpResponse

					_log.Error("Error occured while klarna checkout processing", ex);
				}

				// Additional data might be available in ex.Data.
				if (ex.Data.Contains("internal_message"))
				{
					// For instance, Content-Type application/vnd.klarna.error-v1+json has "internal_message".
					var internalMessage = (string)ex.Data["internal_message"];
					_log.ErrorFormat("Klarna checkout error internal message = {0}", internalMessage);
				}

				throw;
			}
			catch (Exception ex)
			{
				_log.Error("Error occured while klarna checkout processing", ex);
				throw;
			}

			return klarnaContent;
		}
		public virtual string GetKlarnaConfirmationContent(Uri orderLocation)
		{
			string confirmationContent = string.Empty;

			try
			{
				Uri checkoutId = orderLocation;
				KlarnaCheckout.Order order = MakeOrder(checkoutId);
				order.Fetch();
				if ((string)order.GetValue("status") == "checkout_incomplete")
				{
					_log.ErrorFormat("Order with checkout URI {0} has checkout_incomplete status", checkoutId);
					return null;
				}

				var gui = order.GetValue("gui") as JObject;
				if (gui != null)
				{
					var snippet = gui["snippet"];
					if (snippet != null)
					{
						confirmationContent = snippet.ToString();
					}
				}
			}
			catch (KlarnaCheckout.ConnectorException ex)
			{
				var webException = ex.InnerException as WebException;
				if (webException != null)
				{
					// Here you can check for timeouts, and other connection related errors.
					// webException.Response could contain the response object.
					_log.Error("Error occured while klarna confirmation processing", ex);
				}
				else
				{
					// In case there wasn't a WebException where you could get the response
					// (e.g. a protocol error, bad digest, etc) you might still be able to
					// get a hold of the response object.
					// ex.Data["Response"] as IHttpResponse

					_log.Error("Error occured while klarna confirmation processing", ex);
				}

				// Additional data might be available in ex.Data.
				if (ex.Data.Contains("internal_message"))
				{
					// For instance, Content-Type application/vnd.klarna.error-v1+json has "internal_message".
					var internalMessage = (string)ex.Data["internal_message"];
					_log.ErrorFormat("Klarna checkout confirmation error internal message = {0}", internalMessage);
				}

				throw;
			}
			catch (Exception ex)
			{
				_log.Error("Error occured while klarna confirmation processing", ex);
				throw;
			}

			return confirmationContent;
		}
		public KlarnaCheckout.Order MakeOrder(Uri resourceUri)
		{
			KlarnaCheckout.Order order;
			var connector = KlarnaCheckout.Connector.Create(KlarnaSetting.SecretKey);

			if (resourceUri != null)
			{
				order = new KlarnaCheckout.Order(connector, resourceUri)
				{
					ContentType = KlarnaSetting.ContentType
				};
			}
			else
			{
				order = new KlarnaCheckout.Order(connector)
				{
					BaseUri = new Uri(KlarnaSetting.Url),
					ContentType = KlarnaSetting.ContentType
				};
			}

			return order;
		}

		private Dictionary<string, object> CreateKlarnaCart(IUserContext context, IEnumerable<ICartItem> cartItems, decimal freightCost, decimal? discountAmount)
		{
			var klarnaCartItems = new List<Dictionary<string, object>>();

			foreach (ILekmerCartItem cartItem in cartItems)
			{
				if (cartItem.Product.IsProduct())
				{
					klarnaCartItems.Add(MakeKlarnaCartItem(cartItem));
				}
				else if (cartItem.Product.IsPackage())
				{
					klarnaCartItems.AddRange(MakeKlarnaCartPackage(context, cartItem));
				}
				else
				{
					throw new InvalidOperationException("Unknown product type!");
				}
			}

			klarnaCartItems.Add(MakeKlarnaShippingItem(freightCost));

			if (discountAmount.HasValue && discountAmount.Value > 0)
			{
				klarnaCartItems.Add(MakeKlarnaDiscountItem(discountAmount.Value));
			}

			var klarnaCart = new Dictionary<string, object> { { "items", klarnaCartItems } };
			return klarnaCart;
		}

		private Dictionary<string, object> MakeKlarnaCartItem(ILekmerCartItem cartItem)
		{
			string erpId = cartItem.Product.ErpId + "-000"; // no size

			if (cartItem.SizeId.HasValue)
			{
				IProductSize productSize = ProductSizeService.GetById(cartItem.Product.Id, cartItem.SizeId.Value);
				if (productSize != null)
				{
					erpId = productSize.ErpId;
				}
			}

			return new Dictionary<string, object>
				{
					{"type", "physical"},
					{"reference", erpId},
					{"name", GetCartItemName(cartItem.Product.DisplayTitle)},
					{"quantity", cartItem.Quantity},
					{"unit_price", ConvertToInt(cartItem.Product.CampaignInfo.Price.IncludingVat)},
					{"tax_rate", ConvertToInt(((ILekmerPriceListItem) cartItem.Product.Price).GetVatPercent())}
				};
		}

		private List<Dictionary<string, object>> MakeKlarnaCartPackage(IUserContext context, ILekmerCartItem cartItem)
		{
			var klarnaPackageItems = new List<Dictionary<string, object>>();

			ProductIdCollection productIds = ProductService.GetIdAllByPackageMasterProduct(context, cartItem.Product.Id);
			ProductCollection products = ProductService.PopulateProductsWithOnlyInPackage(context, productIds);

			string erpId = cartItem.Product.ErpId + "-000"; // no size

			if (cartItem.SizeId.HasValue)
			{
				IProductSize productSize = ProductSizeService.GetById(cartItem.Product.Id, cartItem.SizeId.Value);
				if (productSize != null)
				{
					erpId = productSize.ErpId;
				}
			}

			string packageName = GetCartItemName(cartItem.Product.DisplayTitle);
			decimal vatPercent = ((ILekmerPriceListItem)cartItem.Product.Price).GetVatPercent();
			decimal packageDiscount = cartItem.Product.CampaignInfo.Price.IncludingVat;

			foreach (IProduct product in products)
			{
				klarnaPackageItems.Add(MakeKlarnaCartPackageItem(cartItem, product));
				packageDiscount -= product.Price.PriceIncludingVat;
			}

			klarnaPackageItems.Add(new Dictionary<string, object>
				{
					{"type", "physical"},
					{"reference", erpId},
					{"name", GetCartItemName(packageName)},
					{"quantity", cartItem.Quantity},
					{"unit_price", ConvertToInt(packageDiscount)},
					{"tax_rate", ConvertToInt(vatPercent)}
				}
			);

			return klarnaPackageItems;
		}
		private Dictionary<string, object> MakeKlarnaCartPackageItem(ILekmerCartItem cartItem, IProduct packageItem)
		{
			string erpId = packageItem.ErpId + "-000"; // no size

			if (cartItem.PackageElements != null)
			{
				ICartItemPackageElement cartItemPackageElement = cartItem.PackageElements.FirstOrDefault(p => p.ProductId == packageItem.Id);
				if (cartItemPackageElement != null && cartItemPackageElement.SizeId.HasValue)
				{
					IProductSize productSize = ProductSizeService.GetById(cartItemPackageElement.ProductId, cartItemPackageElement.SizeId.Value);
					if (productSize != null)
					{
						erpId = productSize.ErpId;
					}
				}
			}

			return new Dictionary<string, object>
				{
					{"type", "physical"},
					{"reference", erpId},
					{"name", GetCartItemName(packageItem.DisplayTitle)},
					{"quantity", cartItem.Quantity},
					{"unit_price", ConvertToInt(packageItem.Price.PriceIncludingVat)},
					{"tax_rate", ConvertToInt(((ILekmerPriceListItem) packageItem.Price).GetVatPercent())}
				};
		}

		private Dictionary<string, object> MakeKlarnaShippingItem(decimal freightCost)
		{
			return new Dictionary<string, object>
				{
					{"type", "shipping_fee"},
					{"reference", "SHIPPING"},
					{"name", AliasHelper.GetAliasValue("Order.Klarna.ShippingFee")},
					{"quantity", 1},
					{"unit_price", ConvertToInt(freightCost)},
					{"tax_rate", 2500}
				};
		}
		private Dictionary<string, object> MakeKlarnaDiscountItem(decimal voucherDiscountAmount)
		{
			return new Dictionary<string, object>
				{
					{"type", "discount"},
					{"reference", "DISCOUNT"},
					{"name", AliasHelper.GetAliasValue("Order.Klarna.Discount")},
					{"quantity", 1},
					{"unit_price", ConvertToInt(-voucherDiscountAmount)},
					{"tax_rate", 2500}
				};
		}

		private string GetCartItemName(string title)
		{
			if (title.HasValue())
			{
				byte[] bytes = KlarnaEncoding.GetBytes(title);
				title = KlarnaEncoding.GetString(bytes);
			}
			else
			{
				title = string.Empty;
			}

			return title;
		}
		private string GetLocale(IChannel channel)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "fi":
					return "fi-fi";
				case "no":
					return "nb-no";
				case "se":
					return "sv-se";
				default:
					return channel.Culture.CommonName;
			}
		}

		private int ConvertToInt(decimal value)
		{
			return (int)Math.Round(value * 100);
		}
	}
}
