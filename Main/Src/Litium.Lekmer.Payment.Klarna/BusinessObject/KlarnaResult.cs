﻿namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaResult : IKlarnaResult
	{
		public int KlarnaTransactionId { get; set; }
		public int ResponseCode { get; set; }
		public int FaultCode { get; set; }
		public string FaultReason { get; set; }
		public long Duration { get; set; }
		public string ResponseContent { get; set; }
	}
}
