﻿using System.Collections.Generic;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaGetAddressResponse : KlarnaResult, IKlarnaGetAddressResponse
	{
		public List<object> Addresses { get; set; }
	}
}
