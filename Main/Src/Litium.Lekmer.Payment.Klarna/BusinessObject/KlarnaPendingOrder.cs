﻿using System;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaPendingOrder : IKlarnaPendingOrder
	{
		public int Id { get; set; }
		public int ChannelId { get; set; }
		public int OrderId { get; set; }
		public DateTime FirstAttempt { get; set; }
		public DateTime LastAttempt { get; set; }
	}
}
