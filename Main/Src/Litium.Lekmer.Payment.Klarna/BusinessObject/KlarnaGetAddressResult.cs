﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaGetAddressResult : KlarnaResult, IKlarnaGetAddressResult
	{
		public Collection<IKlarnaAddress> KlarnaAddresses { get; set; }
	}
}