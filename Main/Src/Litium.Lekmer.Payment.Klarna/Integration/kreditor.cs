// File:      kreditor.cs
// Author:    JOhan Bevemyr <jb@kreditor.se>
// Copyright: 2005 Kreditor AB
// Date:      20 September 2005
// Purpose:   Provide an ASP.NET component for communicating with
//            the Kreditor server.

using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using CookComputing.XmlRpc;
using System.Net.Sockets;
using System.Threading;

namespace Kreditor
{

    [Serializable]
    public class KreditorException : ApplicationException
    {
        // constructors
        //
        public KreditorException(int TheCode, string TheString)
            : base("Klarna returned a fault.")
        {
            m_faultCode = TheCode;
            m_faultString = TheString;
        }
        // deserialization constructor
        protected KreditorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            m_faultCode = (int)info.GetValue("m_faultCode", typeof(int));
            m_faultString = (String)info.GetValue("m_faultString", typeof(string));
        }
        // properties
        //
        public int FaultCode
        {
            get { return m_faultCode; }
        }

        public string FaultString
        {
            get { return m_faultString; }
        }

        public string FaultReason
        {
            get { return m_faultString; }
        }

        // public methods
        //
        public override void GetObjectData(SerializationInfo info,
                                           StreamingContext context)
        {
            info.AddValue("m_faultCode", m_faultCode);
            info.AddValue("m_faultString", m_faultString);
            base.GetObjectData(info, context);
        }

        // data
        //
        int m_faultCode;
        string m_faultString;
    }

    public class client : XmlRpcClientProtocol
    {

        public client()
        {
           // this.Url = "https://payment.klarna.com";
        }

        public int NumHosts
        {
            get { return num_hosts; }
            set { num_hosts = value; }
        }

        public string ClientstatHost
        {
            get { return clientstat_host; }
            set { clientstat_host = value; }
        }

        public int ClientstatPort
        {
            get { return clientstat_port; }
            set { clientstat_port = value; }
        }

        public bool SendStats
        {
            get { return send_stats; }
            set { send_stats = value; }
        }

        public bool SelectHosts
        {
            get { return select_hosts; }
            set { select_hosts = value; }
        }

        private int num_hosts = 3;
        private string clientstat_host = "clientstat.kreditor.se";
        private int clientstat_port = 80;
        private bool send_stats = true;
        private bool select_hosts = true;

        public int Status;
        public int FaultCode;
        public string FaultString;
        public string Value;
        public string[] ValueArr;
        public string[,] Arrarr;
        public object[] ObjArr;
        public XmlRpcStruct ValueStruct;
        public int ValueInt;

        private static string PROTO_VSN = "4.0";
        private static string CLIENT_VSN = "asp.net_bc:1.35";

        // Response codes
        public static int ERROR = 1;
        public static int OK = 2;

        // Flags for add_invoice
        public static int AUTO_ACTIVATE = (1 << 0);
        public static int TEST_MODE = (1 << 1);
        public static int KRED_TEST_MODE = (1 << 1);
        public static int MANUAL_AUTO_ACTIVATE = (1 << 2);
        public static int PRE_PAY = (1 << 3);
        public static int SMS_PASSWD = (1 << 6);

        // Flags for mk_goods_flags
        public static int PRINT_1000 = (1 << 0);
        public static int PRINT_100 = (1 << 1);
        public static int PRINT_10 = (1 << 2);
        public static int IS_SHIPMENT = (1 << 3);
        public int ISSHIPMENT = (1 << 3);
        public static int IS_HANDLING = (1 << 4);
        public static int INC_VAT = (1 << 5);
        public int INCVAT = (1 << 5);

        // Shipment types
        public static int NORMAL_SHIPMENT = 1;
        public static int EXPRESS_SHIPMENT = 2;
        public static int KRED_NORMAL_SHIPMENT = 1;
        public static int KRED_EXPRESS_SHIPMENT = 2;

        // Charge type
        public static int SHIPMENT = 1;
        public static int KRED_HANDLING = 2;

        //Currencies
        public static int KRED_SEK = 0;
        public static int KRED_NOK = 1;
        public static int KRED_EUR = 2;
        public static int KRED_DKK = 3;

        //pno_encodings
        public static int KRED_PNO_SE = 2;
        public static int KRED_PNO_NO = 3;
        public static int KRED_PNO_FI = 4;
        public static int KRED_PNO_DK = 5;
        public static int KRED_PNO_DE = 6;
        public static int KRED_PNO_NL = 7;

        //send options
        public static int KRED_SEND_BY_MAIL = 4;
        public static int KRED_SEND_BY_EMAIL = 8;


        public static int KRED_PRESERVE_RESERVATION = 16;
        public static int KRED_PHONE_TRANSACTION = 512;
        public static int KRED_SEND_PHONE_PIN = 1024;

        //address types
        public static int GA_OLD = 1;
        public static int GA_NEW = 2;

        public static int PNO_FAKE = 1; // Fake
        public static int PNO_SE = 2; // Swedish
        public static int PNO_NO = 3; // Norwegian
        public static int PNO_FI = 4; // Finnish
        public static int PNO_DK = 5; // Danish
        public static int PNO_DE = 6; // German
        public static int PNO_NL = 7; // Dutch
        //monthly cost
        public static int KRED_ACTUAL_COST = 1;
        public static int KRED_LIMIT_COST = 0;

        // Countries
        public static int KRED_ISO3166_AF = 1;   // AFGHANISTAN
        public static int KRED_ISO3166_AX = 2;   // �LAND ISLANDS
        public static int KRED_ISO3166_AL = 3;   // ALBANIA
        public static int KRED_ISO3166_DZ = 4;   // ALGERIA
        public static int KRED_ISO3166_AS = 5;   // AMERICAN SAMOA
        public static int KRED_ISO3166_AD = 6;   // ANDORRA
        public static int KRED_ISO3166_AO = 7;   // ANGOLA
        public static int KRED_ISO3166_AI = 8;   // ANGUILLA
        public static int KRED_ISO3166_AQ = 9;   // ANTARCTICA
        public static int KRED_ISO3166_AG = 10;  // ANTIGUA AND BARBUDA
        public static int KRED_ISO3166_AR = 11;  // ARGENTINA
        public static int KRED_ISO3166_AM = 12;  // ARMENIA
        public static int KRED_ISO3166_AW = 13;  // ARUBA
        public static int KRED_ISO3166_AU = 14;  // AUSTRALIA
        public static int KRED_ISO3166_AT = 15;  // AUSTRIA
        public static int KRED_ISO3166_AZ = 16;  // AZERBAIJAN
        public static int KRED_ISO3166_BS = 17;  // BAHAMAS
        public static int KRED_ISO3166_BH = 18;  // BAHRAIN
        public static int KRED_ISO3166_BD = 19;  // BANGLADESH
        public static int KRED_ISO3166_BB = 20;  // BARBADOS
        public static int KRED_ISO3166_BY = 21;  // BELARUS
        public static int KRED_ISO3166_BE = 22;  // BELGIUM
        public static int KRED_ISO3166_BZ = 23;  // BELIZE
        public static int KRED_ISO3166_BJ = 24;  // BENIN
        public static int KRED_ISO3166_BM = 25;  // BERMUDA
        public static int KRED_ISO3166_BT = 26;  // BHUTAN
        public static int KRED_ISO3166_BO = 27;  // BOLIVIA
        public static int KRED_ISO3166_BA = 28;  // BOSNIA AND HERZEGOVINA
        public static int KRED_ISO3166_BW = 29;  // BOTSWANA
        public static int KRED_ISO3166_BV = 30;  // BOUVET ISLAND
        public static int KRED_ISO3166_BR = 31;  // BRAZIL
        public static int KRED_ISO3166_IO = 32;  // BR. INDIAN OCEAN TERRITORY
        public static int KRED_ISO3166_BN = 33;  // BRUNEI DARUSSALAM
        public static int KRED_ISO3166_BG = 34;  // BULGARIA
        public static int KRED_ISO3166_BF = 35;  // BURKINA FASO
        public static int KRED_ISO3166_BI = 36;  // BURUNDI
        public static int KRED_ISO3166_KH = 37;  // CAMBODIA
        public static int KRED_ISO3166_CM = 38;  // CAMEROON
        public static int KRED_ISO3166_CA = 39;  // CANADA
        public static int KRED_ISO3166_CV = 40;  // CAPE VERDE
        public static int KRED_ISO3166_KY = 41;  // CAYMAN ISLANDS
        public static int KRED_ISO3166_CF = 42;  // CENTRAL AFRICAN REPUBLIC
        public static int KRED_ISO3166_TD = 43;  // CHAD
        public static int KRED_ISO3166_CL = 44;  // CHILE
        public static int KRED_ISO3166_CN = 45;  // CHINA
        public static int KRED_ISO3166_CX = 46;  // CHRISTMAS ISLAND
        public static int KRED_ISO3166_CC = 47;  // COCOS (KEELING) ISLANDS
        public static int KRED_ISO3166_CO = 48;  // COLOMBIA
        public static int KRED_ISO3166_KM = 49;  // COMOROS
        public static int KRED_ISO3166_CG = 50;  // CONGO
        public static int KRED_ISO3166_CD = 51;  // CONGO, DEMOCRATIC REP. OF
        public static int KRED_ISO3166_CK = 52;  // COOK ISLANDS
        public static int KRED_ISO3166_CR = 53;  // RICA
        public static int KRED_ISO3166_CI = 54;  // D'IVOIRE
        public static int KRED_ISO3166_HR = 55;  // CROATIA
        public static int KRED_ISO3166_CU = 56;  // CUBA
        public static int KRED_ISO3166_CY = 57;  // CYPRUS
        public static int KRED_ISO3166_CZ = 58;  // CZECH REPUBLIC
        public static int KRED_ISO3166_DK = 59;  // DENMARK
        public static int KRED_ISO3166_DJ = 60;  // DJIBOUTI
        public static int KRED_ISO3166_DM = 61;  // DOMINICA
        public static int KRED_ISO3166_DO = 62;  // DOMINICAN REPUBLIC
        public static int KRED_ISO3166_EC = 63;  // ECUADOR
        public static int KRED_ISO3166_EG = 64;  // EGYPT
        public static int KRED_ISO3166_SV = 65;  // EL SALVADOR
        public static int KRED_ISO3166_GQ = 66;  // EQUATORIAL GUINEA
        public static int KRED_ISO3166_ER = 67;  // ERITREA
        public static int KRED_ISO3166_EE = 68;  // ESTONIA
        public static int KRED_ISO3166_ET = 69;  // ETHIOPIA
        public static int KRED_ISO3166_FK = 70;  // FALKLAND ISLANDS (MALVINAS)
        public static int KRED_ISO3166_FO = 71;  // FAROE ISLANDS
        public static int KRED_ISO3166_FJ = 72;  // FIJI
        public static int KRED_ISO3166_FI = 73;  // FINLAND
        public static int KRED_ISO3166_FR = 74;  // FRANCE
        public static int KRED_ISO3166_GF = 75;  // FRENCH GUIANA
        public static int KRED_ISO3166_PF = 76;  // FRENCH POLYNESIA
        public static int KRED_ISO3166_TF = 77;  // FRENCH SOUTHERN TERRITORIES
        public static int KRED_ISO3166_GA = 78;  // GABON
        public static int KRED_ISO3166_GM = 79;  // GAMBIA
        public static int KRED_ISO3166_GE = 80;  // GEORGIA
        public static int KRED_ISO3166_DE = 81;  // GERMANY
        public static int KRED_ISO3166_GH = 82;  // GHANA
        public static int KRED_ISO3166_GI = 83;  // GIBRALTAR
        public static int KRED_ISO3166_GR = 84;  // GREECE
        public static int KRED_ISO3166_GL = 85;  // GREENLAND
        public static int KRED_ISO3166_GD = 86;  // GRENADA
        public static int KRED_ISO3166_GP = 87;  // GUADELOUPE
        public static int KRED_ISO3166_GU = 88;  // GUAM
        public static int KRED_ISO3166_GT = 89;  // GUATEMALA
        public static int KRED_ISO3166_GG = 90;  // GUERNSEY
        public static int KRED_ISO3166_GN = 91;  // GUINEA
        public static int KRED_ISO3166_GW = 92;  // GUINEA-BISSAU
        public static int KRED_ISO3166_GY = 93;  // GUYANA
        public static int KRED_ISO3166_HT = 94;  // HAITI
        public static int KRED_ISO3166_HM = 95;  // HEARD AND MCDONALD ISLANDS
        public static int KRED_ISO3166_VA = 96;  // HOLY SEE(VATICAN CITY)
        public static int KRED_ISO3166_HN = 97;  // HONDURAS
        public static int KRED_ISO3166_HK = 98;  // HONG KONG
        public static int KRED_ISO3166_HU = 99;  // HUNGARY
        public static int KRED_ISO3166_IS = 100; // ICELAND
        public static int KRED_ISO3166_IN = 101; // INDIA
        public static int KRED_ISO3166_ID = 102; // INDONESIA
        public static int KRED_ISO3166_IR = 103; // IRAN, ISLAMIC REPUBLIC OF
        public static int KRED_ISO3166_IQ = 104; // IRAQ
        public static int KRED_ISO3166_IE = 105; // IRELAND
        public static int KRED_ISO3166_IM = 106; // ISLE OF MAN
        public static int KRED_ISO3166_IL = 107; // ISRAEL
        public static int KRED_ISO3166_IT = 108; // ITALY
        public static int KRED_ISO3166_JM = 109; // JAMAICA
        public static int KRED_ISO3166_JP = 110; // JAPAN
        public static int KRED_ISO3166_JE = 111; // JERSEY
        public static int KRED_ISO3166_JO = 112; // JORDAN
        public static int KRED_ISO3166_KZ = 113; // KAZAKHSTAN
        public static int KRED_ISO3166_KE = 114; // KENYA
        public static int KRED_ISO3166_KI = 115; // KIRIBATI
        public static int KRED_ISO3166_KP = 116; // KOREA, D.P. REPUBLIC OF
        public static int KRED_ISO3166_KR = 117; // KOREA, REPUBLIC OF
        public static int KRED_ISO3166_KW = 118; // KUWAIT
        public static int KRED_ISO3166_KG = 119; // KYRGYZSTAN
        public static int KRED_ISO3166_LA = 120; // LAO P.DEMOCRATIC REPUBLIC
        public static int KRED_ISO3166_LV = 121; // LATVIA
        public static int KRED_ISO3166_LB = 122; // LEBANON
        public static int KRED_ISO3166_LS = 123; // LESOTHO
        public static int KRED_ISO3166_LR = 124; // LIBERIA
        public static int KRED_ISO3166_LY = 125; // LIBYAN ARAB JAMAHIRIYA
        public static int KRED_ISO3166_LI = 126; // LIECHTENSTEIN
        public static int KRED_ISO3166_LT = 127; // LITHUANIA
        public static int KRED_ISO3166_LU = 128; // LUXEMBOURG
        public static int KRED_ISO3166_MO = 129; // MACAO
        public static int KRED_ISO3166_MK = 130; // MACEDONIA, F.Y. REPUBLIC OF
        public static int KRED_ISO3166_MG = 131; // MADAGASCAR
        public static int KRED_ISO3166_MW = 132; // MALAWI
        public static int KRED_ISO3166_MY = 133; // MALAYSIA
        public static int KRED_ISO3166_MV = 134; // MALDIVES
        public static int KRED_ISO3166_ML = 135; // MALI
        public static int KRED_ISO3166_MT = 136; // MALTA
        public static int KRED_ISO3166_MH = 137; // MARSHALL ISLANDS
        public static int KRED_ISO3166_MQ = 138; // MARTINIQUE
        public static int KRED_ISO3166_MR = 139; // MAURITANIA
        public static int KRED_ISO3166_MU = 140; // MAURITIUS
        public static int KRED_ISO3166_YT = 141; // MAYOTTE
        public static int KRED_ISO3166_MX = 142; // MEXICO
        public static int KRED_ISO3166_FM = 143; // MICRONESIA
        public static int KRED_ISO3166_MD = 144; // MOLDOVA, REPUBLIC OF
        public static int KRED_ISO3166_MC = 145; // MONACO
        public static int KRED_ISO3166_MN = 146; // MONGOLIA
        public static int KRED_ISO3166_MS = 147; // MONTSERRAT
        public static int KRED_ISO3166_MA = 148; // MOROCCO
        public static int KRED_ISO3166_MZ = 149; // MOZAMBIQUE
        public static int KRED_ISO3166_MM = 150; // MYANMAR
        public static int KRED_ISO3166_NA = 151; // NAMIBIA
        public static int KRED_ISO3166_NR = 152; // NAURU
        public static int KRED_ISO3166_NP = 153; // NEPAL
        public static int KRED_ISO3166_NL = 154; // NETHERLANDS
        public static int KRED_ISO3166_AN = 155; // NETHERLANDS ANTILLES
        public static int KRED_ISO3166_NC = 156; // NEW CALEDONIA
        public static int KRED_ISO3166_NZ = 157; // NEW ZEALAND
        public static int KRED_ISO3166_NI = 158; // NICARAGUA
        public static int KRED_ISO3166_NE = 159; // NIGER
        public static int KRED_ISO3166_NG = 160; // NIGERIA
        public static int KRED_ISO3166_NU = 161; // NIUE
        public static int KRED_ISO3166_NF = 162; // NORFOLK ISLAND
        public static int KRED_ISO3166_MP = 163; // NORTHERN MARIANA ISLANDS
        public static int KRED_ISO3166_NO = 164; // NORWAY
        public static int KRED_ISO3166_OM = 165; // OMAN
        public static int KRED_ISO3166_PK = 166; // PAKISTAN
        public static int KRED_ISO3166_PW = 167; // PALAU
        public static int KRED_ISO3166_PS = 168; // PALESTINIAN T.OCCUPIED
        public static int KRED_ISO3166_PA = 169; // PANAMA
        public static int KRED_ISO3166_PG = 170; // PAPUA NEW GUINEA
        public static int KRED_ISO3166_PY = 171; // PARAGUAY
        public static int KRED_ISO3166_PE = 172; // PERU
        public static int KRED_ISO3166_PH = 173; // PHILIPPINES
        public static int KRED_ISO3166_PN = 174; // PITCAIRN
        public static int KRED_ISO3166_PL = 175; // POLAND
        public static int KRED_ISO3166_PT = 176; // PORTUGAL
        public static int KRED_ISO3166_PR = 177; // PUERTO RICO
        public static int KRED_ISO3166_QA = 178; // QATAR
        public static int KRED_ISO3166_RE = 179; // REUNION
        public static int KRED_ISO3166_RO = 180; // ROMANIA
        public static int KRED_ISO3166_RU = 181; // RUSSIAN FEDERATION
        public static int KRED_ISO3166_RW = 182; // RWANDA
        public static int KRED_ISO3166_SH = 183; // SAINT HELENA
        public static int KRED_ISO3166_KN = 184; // SAINT KITTS AND NEVIS
        public static int KRED_ISO3166_LC = 185; // SAINT LUCIA
        public static int KRED_ISO3166_PM = 186; // SAINT PIERRE AND MIQUELON
        public static int KRED_ISO3166_VC = 187; // ST VINCENT A.T GRENADINES
        public static int KRED_ISO3166_WS = 188; // SAMOA
        public static int KRED_ISO3166_SM = 189; // SAN MARINO
        public static int KRED_ISO3166_ST = 190; // SAO TOME AND PRINCIPE
        public static int KRED_ISO3166_SA = 191; // SAUDI ARABIA
        public static int KRED_ISO3166_SN = 192; // SENEGAL
        public static int KRED_ISO3166_CS = 193; // SERBIA AND MONTENEGRO
        public static int KRED_ISO3166_SC = 194; // SEYCHELLES
        public static int KRED_ISO3166_SL = 195; // SIERRA LEONE
        public static int KRED_ISO3166_SG = 196; // SINGAPORE
        public static int KRED_ISO3166_SK = 197; // SLOVAKIA
        public static int KRED_ISO3166_SI = 198; // SLOVENIA
        public static int KRED_ISO3166_SB = 199; // SOLOMON ISLANDS
        public static int KRED_ISO3166_SO = 200; // SOMALIA
        public static int KRED_ISO3166_ZA = 201; // SOUTH AFRICA
        public static int KRED_ISO3166_GS = 202; // S.GEORGIA, S.SANDWICH ISLS
        public static int KRED_ISO3166_ES = 203; // SPAIN
        public static int KRED_ISO3166_LK = 204; // SRI LANKA
        public static int KRED_ISO3166_SD = 205; // SUDAN
        public static int KRED_ISO3166_SR = 206; // SURINAME
        public static int KRED_ISO3166_SJ = 207; // SVALBARD AND JAN MAYEN
        public static int KRED_ISO3166_SZ = 208; // SWAZILAND
        public static int KRED_ISO3166_SE = 209; // SWEDEN
        public static int KRED_ISO3166_CH = 210; // SWITZERLAND
        public static int KRED_ISO3166_SY = 211; // SYRIAN ARAB REPUBLIC
        public static int KRED_ISO3166_TW = 212; // TAIWAN PROVINCE OF CHINA
        public static int KRED_ISO3166_TJ = 213; // TAJIKISTAN
        public static int KRED_ISO3166_TZ = 214; // TANZANIA, U.R. OF
        public static int KRED_ISO3166_TH = 215; // THAILAND
        public static int KRED_ISO3166_TL = 216; // TIMOR-LESTE
        public static int KRED_ISO3166_TG = 217; // TOGO
        public static int KRED_ISO3166_TK = 218; // TOKELAU
        public static int KRED_ISO3166_TO = 219; // TONGA
        public static int KRED_ISO3166_TT = 220; // TRINIDAD AND TOBAGO
        public static int KRED_ISO3166_TN = 221; // TUNISIA
        public static int KRED_ISO3166_TR = 222; // TURKEY
        public static int KRED_ISO3166_TM = 223; // TURKMENISTAN
        public static int KRED_ISO3166_TC = 224; // TURKS AND CAICOS ISLANDS
        public static int KRED_ISO3166_TV = 225; // TUVALU
        public static int KRED_ISO3166_UG = 226; // UGANDA
        public static int KRED_ISO3166_UA = 227; // UKRAINE
        public static int KRED_ISO3166_AE = 228; // UNITED ARAB EMIRATES
        public static int KRED_ISO3166_GB = 229; // UNITED KINGDOM
        public static int KRED_ISO3166_US = 230; // UNITED STATES
        public static int KRED_ISO3166_UM = 231; // U.S MINOR OUTLYING ISLANDS
        public static int KRED_ISO3166_UY = 232; // URUGUAY
        public static int KRED_ISO3166_UZ = 233; // UZBEKISTAN
        public static int KRED_ISO3166_VU = 234; // VANUATU
        public static int KRED_ISO3166_VE = 235; // VENEZUELA
        public static int KRED_ISO3166_VN = 236; // VIET NAM
        public static int KRED_ISO3166_VG = 237; // VIRGIN ISLANDS, BRITISH
        public static int KRED_ISO3166_VI = 238; // VIRGIN ISLANDS, US
        public static int KRED_ISO3166_WF = 239; // WALLIS AND FUTUNA
        public static int KRED_ISO3166_EH = 240; // WESTERN SAHARA
        public static int KRED_ISO3166_YE = 241; // YEMEN
        public static int KRED_ISO3166_ZM = 242; // ZAMBIA
        public static int KRED_ISO3166_ZW = 243; // ZIMBABWE

        // Languages
        public static int KRED_ISO639_AA = 1;      // Afar
        public static int KRED_ISO639_AB = 2;      // Abkhazian
        public static int KRED_ISO639_AE = 3;      // Avestan
        public static int KRED_ISO639_AF = 4;      // Afrikaans
        public static int KRED_ISO639_AM = 5;      // Amharic
        public static int KRED_ISO639_AR = 6;      // Arabic
        public static int KRED_ISO639_AS = 7;      // Assamese
        public static int KRED_ISO639_AY = 8;      // Aymara
        public static int KRED_ISO639_AZ = 9;      // Azerbaijani
        public static int KRED_ISO639_BA = 10;     // Bashkir
        public static int KRED_ISO639_BE = 11;     // Byelorussian; Belarusian
        public static int KRED_ISO639_BG = 12;     // Bulgarian
        public static int KRED_ISO639_BH = 13;     // Bihari
        public static int KRED_ISO639_BI = 14;     // Bislama
        public static int KRED_ISO639_BN = 15;     // Bengali; Bangla
        public static int KRED_ISO639_BO = 16;     // Tibetan
        public static int KRED_ISO639_BR = 17;     // Breton
        public static int KRED_ISO639_BS = 18;     // Bosnian
        public static int KRED_ISO639_CA = 19;     // Catalan
        public static int KRED_ISO639_CE = 20;     // Chechen
        public static int KRED_ISO639_CH = 21;     // Chamorro
        public static int KRED_ISO639_CO = 22;     // Corsican
        public static int KRED_ISO639_CS = 23;     // Czech
        public static int KRED_ISO639_CU = 24;     // Church Slavic
        public static int KRED_ISO639_CV = 25;     // Chuvash
        public static int KRED_ISO639_CY = 26;     // Welsh
        public static int KRED_ISO639_DA = 27;     // Danish
        public static int KRED_ISO639_DE = 28;     // German
        public static int KRED_ISO639_DZ = 29;     // Dzongkha; Bhutani
        public static int KRED_ISO639_EL = 30;     // Greek
        public static int KRED_ISO639_EN = 31;     // English
        public static int KRED_ISO639_EO = 32;     // Esperanto
        public static int KRED_ISO639_ES = 33;     // Spanish
        public static int KRED_ISO639_ET = 34;     // Estonian
        public static int KRED_ISO639_EU = 35;     // Basque
        public static int KRED_ISO639_FA = 36;     // Persian
        public static int KRED_ISO639_FI = 37;     // Finnish
        public static int KRED_ISO639_FJ = 38;     // Fijian; Fiji
        public static int KRED_ISO639_FO = 39;     // Faroese
        public static int KRED_ISO639_FR = 40;     // French
        public static int KRED_ISO639_FY = 41;     // Frisian
        public static int KRED_ISO639_GA = 42;     // Irish
        public static int KRED_ISO639_GD = 43;     // Scots; Gaelic
        public static int KRED_ISO639_GL = 44;     // Gallegan; Galician
        public static int KRED_ISO639_GN = 45;     // Guarani
        public static int KRED_ISO639_GU = 46;     // Gujarati
        public static int KRED_ISO639_GV = 47;     // Manx
        public static int KRED_ISO639_HA = 48;     // Hausa (?)
        public static int KRED_ISO639_HE = 49;     // Hebrew (formerly iw)
        public static int KRED_ISO639_HI = 50;     // Hindi
        public static int KRED_ISO639_HO = 51;     // Hiri Motu
        public static int KRED_ISO639_HR = 52;     // Croatian
        public static int KRED_ISO639_HU = 53;     // Hungarian
        public static int KRED_ISO639_HY = 54;     // Armenian
        public static int KRED_ISO639_HZ = 55;     // Herero
        public static int KRED_ISO639_IA = 56;     // Interlingua
        public static int KRED_ISO639_ID = 57;     // Indonesian (formerly in)
        public static int KRED_ISO639_IE = 58;     // Interlingue
        public static int KRED_ISO639_IK = 59;     // Inupiak
        public static int KRED_ISO639_IO = 60;     // Ido
        public static int KRED_ISO639_IS = 61;     // Icelandic
        public static int KRED_ISO639_IT = 62;     // Italian
        public static int KRED_ISO639_IU = 63;     // Inuktitut
        public static int KRED_ISO639_JA = 64;     // Japanese
        public static int KRED_ISO639_JV = 65;     // Javanese
        public static int KRED_ISO639_KA = 66;     // Georgian
        public static int KRED_ISO639_KI = 67;     // Kikuyu
        public static int KRED_ISO639_KJ = 68;     // Kuanyama
        public static int KRED_ISO639_KK = 69;     // Kazakh
        public static int KRED_ISO639_KL = 70;     // Kalaallisut; Greenlandic
        public static int KRED_ISO639_KM = 71;     // Khmer; Cambodian
        public static int KRED_ISO639_KN = 72;     // Kannada
        public static int KRED_ISO639_KO = 73;     // Korean
        public static int KRED_ISO639_KS = 74;     // Kashmiri
        public static int KRED_ISO639_KU = 75;     // Kurdish
        public static int KRED_ISO639_KV = 76;     // Komi
        public static int KRED_ISO639_KW = 77;     // Cornish
        public static int KRED_ISO639_KY = 78;     // Kirghiz
        public static int KRED_ISO639_LA = 79;     // Latin
        public static int KRED_ISO639_LB = 80;     // Letzeburgesch
        public static int KRED_ISO639_LN = 81;     // Lingala
        public static int KRED_ISO639_LO = 82;     // Lao; Laotian
        public static int KRED_ISO639_LT = 83;     // Lithuanian
        public static int KRED_ISO639_LV = 84;     // Latvian; Lettish
        public static int KRED_ISO639_MG = 85;     // Malagasy
        public static int KRED_ISO639_MH = 86;     // Marshall
        public static int KRED_ISO639_MI = 87;     // Maori
        public static int KRED_ISO639_MK = 88;     // Macedonian
        public static int KRED_ISO639_ML = 89;     // Malayalam
        public static int KRED_ISO639_MN = 90;     // Mongolian
        public static int KRED_ISO639_MO = 91;     // Moldavian
        public static int KRED_ISO639_MR = 92;     // Marathi
        public static int KRED_ISO639_MS = 93;     // Malay
        public static int KRED_ISO639_MT = 94;     // Maltese
        public static int KRED_ISO639_MY = 95;     // Burmese
        public static int KRED_ISO639_NA = 96;     // Nauru
        public static int KRED_ISO639_NB = 97;     // Norwegian Bokm�l
        public static int KRED_ISO639_ND = 98;     // Ndebele, North
        public static int KRED_ISO639_NE = 99;     // Nepali
        public static int KRED_ISO639_NG = 100;    // Ndonga
        public static int KRED_ISO639_NL = 101;    // Dutch
        public static int KRED_ISO639_NN = 102;    // Norwegian Nynorsk
        public static int KRED_ISO639_NO = 103;    // Norwegian
        public static int KRED_ISO639_NR = 104;    // Ndebele, South
        public static int KRED_ISO639_NV = 105;    // Navajo
        public static int KRED_ISO639_NY = 106;    // Chichewa; Nyanja
        public static int KRED_ISO639_OC = 107;    // Occitan; Proven�al
        public static int KRED_ISO639_OM = 108;    // (Afan) Oromo
        public static int KRED_ISO639_OR = 109;    // Oriya
        public static int KRED_ISO639_OS = 110;    // Ossetian; Ossetic
        public static int KRED_ISO639_PA = 111;    // Panjabi; Punjabi
        public static int KRED_ISO639_PI = 112;    // Pali
        public static int KRED_ISO639_PL = 113;    // Polish
        public static int KRED_ISO639_PS = 114;    // Pashto, Pushto
        public static int KRED_ISO639_PT = 115;    // Portuguese
        public static int KRED_ISO639_QU = 116;    // Quechua
        public static int KRED_ISO639_RM = 117;    // Rhaeto-Romance
        public static int KRED_ISO639_RN = 118;    // Rundi; Kirundi
        public static int KRED_ISO639_RO = 119;    // Romanian
        public static int KRED_ISO639_RU = 120;    // Russian
        public static int KRED_ISO639_RW = 121;    // Kinyarwanda
        public static int KRED_ISO639_SA = 122;    // Sanskrit
        public static int KRED_ISO639_SC = 123;    // Sardinian
        public static int KRED_ISO639_SD = 124;    // Sindhi
        public static int KRED_ISO639_SE = 125;    // Northern Sami
        public static int KRED_ISO639_SG = 126;    // Sango; Sangro
        public static int KRED_ISO639_SI = 127;    // Sinhalese
        public static int KRED_ISO639_SK = 128;    // Slovak
        public static int KRED_ISO639_SL = 129;    // Slovenian
        public static int KRED_ISO639_SM = 130;    // Samoan
        public static int KRED_ISO639_SN = 131;    // Shona
        public static int KRED_ISO639_SO = 132;    // Somali
        public static int KRED_ISO639_SQ = 133;    // Albanian
        public static int KRED_ISO639_SR = 134;    // Serbian
        public static int KRED_ISO639_SS = 135;    // Swati; Siswati
        public static int KRED_ISO639_ST = 136;    // Sesotho; Sotho, Southern
        public static int KRED_ISO639_SU = 137;    // Sundanese
        public static int KRED_ISO639_SV = 138;    // Swedish
        public static int KRED_ISO639_SW = 139;    // Swahili
        public static int KRED_ISO639_TA = 140;    // Tamil
        public static int KRED_ISO639_TE = 141;    // Telugu
        public static int KRED_ISO639_TG = 142;    // Tajik
        public static int KRED_ISO639_TH = 143;    // Thai
        public static int KRED_ISO639_TI = 144;    // Tigrinya
        public static int KRED_ISO639_TK = 145;    // Turkmen
        public static int KRED_ISO639_TL = 146;    // Tagalog
        public static int KRED_ISO639_TN = 147;    // Tswana; Setswana
        public static int KRED_ISO639_TO = 148;    // Tonga (?)
        public static int KRED_ISO639_TR = 149;    // Turkish
        public static int KRED_ISO639_TS = 150;    // Tsonga
        public static int KRED_ISO639_TT = 151;    // Tatar
        public static int KRED_ISO639_TW = 152;    // Twi
        public static int KRED_ISO639_TY = 153;    // Tahitian
        public static int KRED_ISO639_UG = 154;    // Uighur
        public static int KRED_ISO639_UK = 155;    // Ukrainian
        public static int KRED_ISO639_UR = 156;    // Urdu
        public static int KRED_ISO639_UZ = 157;    // Uzbek
        public static int KRED_ISO639_VI = 158;    // Vietnamese
        public static int KRED_ISO639_VO = 159;    // Volapuk
        public static int KRED_ISO639_WA = 160;    // Walloon
        public static int KRED_ISO639_WO = 161;    // Wolof
        public static int KRED_ISO639_XH = 162;    // Xhosa
        public static int KRED_ISO639_YI = 163;    // Yiddish (formerly ji)
        public static int KRED_ISO639_YO = 164;    // Yoruba
        public static int KRED_ISO639_ZA = 165;    // Zhuang
        public static int KRED_ISO639_ZH = 166;    // Chinese
        public static int KRED_ISO639_ZU = 167;    // Zulu


        public static int KRED_ANNUITY_PCLASS = 0;
        public static int KRED_DIVISOR_PCLASS = 1;
        public static int KRED_ONETIME_PCLASS = 2;

        public string get_strerror()
        {
            return strerror(FaultString);
        }

        // GET_ADDRESSES
        public int get_addresses(string pno, int eid, string secret,
                                 int pno_encoding, int type)
        {
            string checksum = "";
            checksum += eid + ":";
            checksum += pno + ":";
            checksum += secret;
            string digest = mk_digest(checksum);
            Status = this.KredCall("get_addresses",
                              new Object[]{PROTO_VSN, CLIENT_VSN, pno, eid, digest,
                                     pno_encoding, type},
                              eid, secret, "Arrarr");
            return Status;
        }

        [XmlRpcMethod("get_addresses")]
        public string[,] get_addresses_rpc(Object[] p, int eid, string secret)
        {
            return (string[,])Invoke("get_addresses_rpc", p, eid, secret);
        }


        // RESERVE_AMOUNT
        public int reserve_amount(string pno, int amount, string referens,
                                  string referens_code, string orderid1,
                                  string orderid2, System.Object lev_addr,
                                  System.Object f_addr, string email,
                                  string phone, string cell, string client_ip,
                                  int flags, int currency, int country,
                                  int language, int eid, string secret,
                                  int pno_encoding, int pclass, int ysalary,
                                  System.Object[] goodsList)
        {
            string checksum = eid + ":" + pno + ":" + amount + ":" + secret;
            string digest = mk_digest(checksum);
            Status = this.KredCall("reserve_amount",
                              new Object[]{PROTO_VSN, CLIENT_VSN, pno, amount,
                                     referens, referens_code, orderid1, orderid2,
                                     lev_addr, f_addr, email, phone, cell,
                                     client_ip, flags, currency, country,
                                     language, eid, digest, pno_encoding, pclass,
                                     ysalary, goodsList},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("reserve_amount")]
        public string reserve_amount_rpc(Object[] p)
        {
            return (string)Invoke("reserve_amount_rpc", p);
        }


        // CANCEL_RESERVATION
        public int cancel_reservation(string rno, int eid, string secret)
        {
            string digest = mk_digest(eid + ":" + rno + ":" + secret);
            Status = this.KredCall("cancel_reservation",
                              new Object[] { PROTO_VSN, CLIENT_VSN, rno, eid, digest },
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("cancel_reservation")]
        public string cancel_reservation_rpc(Object[] p)
        {
            return (string)Invoke("cancel_reservation_rpc", p);
        }


        // CHANGE_RESERVATION
        public int change_reservation(string rno, int newAmount, int eid,
                                      string secret)
        {
            string digest = mk_digest(eid + ":" + rno + ":" + newAmount + ":" + secret);
            Status = this.KredCall("change_reservation",
                              new Object[]{PROTO_VSN, CLIENT_VSN, rno, newAmount, eid,
                                     digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("change_reservation")]
        public string change_reservation_rpc(Object[] p)
        {
            return (string)Invoke("change_reservation_rpc", p);
        }


        // CHECK_RESERVATION
        public int check_reservation(int eid, string rno, int amount,
                                     System.Object address, string secret, string pno)
        {
            string digest = mk_digest(eid + ":" + rno + ":" + amount + ":" + secret);
            try
            {
                Value =
                    check_reservation_rpc(new Object[]{
					PROTO_VSN, CLIENT_VSN, eid, rno, amount, address, digest, pno});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }
            return Status;
        }

        [XmlRpcMethod("check_reservation")]
        public string check_reservation_rpc(Object[] p)
        {
            return (string)Invoke("check_reservation_rpc", p);
        }
        //SPLIT_RESERVATION
        public int split_reservation(string rno, int splitAmount, string orderid1,
                                     string orderid2, int flags, int eid,
                                     string secret)
        {
            string digest = mk_digest(eid + ":" + rno + ":" + splitAmount + ":" + secret);
            Status = this.KredCall("split_reservation",
                              new Object[]{PROTO_VSN, CLIENT_VSN, rno, splitAmount,
                                     orderid1, orderid2, flags, eid, digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("split_reservation")]
        public string split_reservation_rpc(Object[] p)
        {
            return (string)Invoke("split_reservation_rpc", p);
        }

        // xxx This is the old activate_reservation without OCR and PIN
        public int activate_reservation(string rno, string pno,
                                  System.Object[] goodsList, string referens,
                                  string referens_code, string orderid1,
                                  string orderid2, System.Object lev_addr,
                                  System.Object f_addr, int shipmenttype,
                                  string email, string phone, string cell,
                                  string client_ip, int flags, int currency,
                                  int country, int language, int eid,
                                  string secret, int pno_encoding, int pclass,
                                  int ysalary)
        {
            return activate_reservation(rno, pno, "",
                                    goodsList, referens,
                                    referens_code, orderid1,
                                    orderid2, lev_addr,
                                    f_addr, shipmenttype,
                                    email, phone, cell,
                                    client_ip, flags, currency,
                                    country, language, eid,
                                    secret, pno_encoding, pclass,
                                    ysalary, "");

        }


        //ACTIVATE_RESERVATION
        public int activate_reservation(string rno, string pno, string ocr,
                                        System.Object[] goodsList, string referens,
                                        string referens_code, string orderid1,
                                        string orderid2, System.Object lev_addr,
                                        System.Object f_addr, int shipmenttype,
                                        string email, string phone, string cell,
                                        string client_ip, int flags, int currency,
                                        int country, int language, int eid,
                                        string secret, int pno_encoding, int pclass,
                                        int ysalary, string pin)
        {
            //Digest
            string checksum = eid + ":" + pno + ":";
            for (int i = 0; i < goodsList.Length; i++)
            {
                XmlRpcStruct tot = (goodsList[i] as XmlRpcStruct);
                XmlRpcStruct goods = (tot["goods"] as XmlRpcStruct);
                checksum += goods["artno"] + ":" + tot["qty"] + ":";
            }
            checksum += secret;
            string digest = mk_digest(checksum);
            Status = this.KredCall("activate_reservation",
                              new Object[]{PROTO_VSN, CLIENT_VSN, rno, pno, ocr,
                                     goodsList, referens, referens_code,
                                     orderid1, orderid2, lev_addr, f_addr,
                                     shipmenttype, email, phone, cell, client_ip,
                                     flags, currency, country, language, eid,
                                     digest, pno_encoding, pclass, ysalary, pin},
                              eid, secret, "ValueArr");
            return Status;
        }

        [XmlRpcMethod("activate_reservation")]
        public string[] activate_reservation_rpc(Object[] p)
        {
            return (string[])Invoke("activate_reservation_rpc", p);
        }

        // HAS_ACCOUNT
        public int has_account(int eid, string pno, string secret,
                               int pno_encoding)
        {
            string checksum = "";
            checksum += eid + ":";
            checksum += pno + ":";
            checksum += secret;
            string digest = mk_digest(checksum);
            Status = this.KredCall("has_account",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, pno, digest,
                                     pno_encoding},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("has_account")]
        public string has_account_rpc(Object[] p, int eid, string secret)
        {
            return (string)Invoke("has_account_rpc", p, eid, secret);
        }

        // IS INVOICE PAID
        public int is_invoice_paid(string ocr, int eid, string secret)
        {
            string digest = mk_digest(eid + ":" + ocr + ":" + secret);
            Status = this.KredCall("is_invoice_paid",
                              new Object[] { PROTO_VSN, CLIENT_VSN, ocr, eid, digest },
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("is_invoice_paid")]
        public string is_invoice_paid_rpc(Object[] p)
        {
            return (string)Invoke("is_invoice_paid_rpc", p);
        }


        //RESERVE_OCR_NUMS
        public int reserve_ocr_nums(int no, int eid, string secret)
        {
            string digest = mk_digest(eid + ":" + no + ":" + secret);
            Status = this.KredCall("reserve_ocr_nums",
                              new Object[] { PROTO_VSN, CLIENT_VSN, no, eid, digest },
                              eid, secret, "ValueArr");
            return Status;
        }

        [XmlRpcMethod("reserve_ocr_nums")]
        public string[] reserve_ocr_nums_rpc(Object[] p)
        {
            return (string[])Invoke("reserve_ocr_nums_rpc", p);
        }


        //RESERVE_OCR_NUMS with email
        public int reserve_ocr_nums(int no, string email, int eid, string secret)
        {
            string digest = mk_digest(eid + ":" + no + ":" + secret);
            Status = this.KredCall("reserve_ocr_nums2",
                              new Object[]{PROTO_VSN, CLIENT_VSN, no, email, eid,
                                     digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("reserve_ocr_nums")]
        public string reserve_ocr_nums2_rpc(Object[] p)
        {
            return (string)Invoke("reserve_ocr_nums_rpc", p);
        }


        //SET CUSTOMER NO
        public int set_customer_no(string pno, string cust_no, int eid,
                                   string secret, int pno_encoding)
        {
            string digest = mk_digest(eid + ":" + pno + ":" + cust_no + ":" + secret);
            Status = this.KredCall("set_customer_no",
                              new Object[]{PROTO_VSN, CLIENT_VSN, pno, cust_no, eid,
                                     digest, pno_encoding},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("set_customer_no")]
        public string set_customer_no_rpc(Object[] p)
        {
            return (string)Invoke("set_customer_no_rpc", p);
        }


        //GET CUSTOMER NO
        public int get_customer_no(string pno, int eid, string secret,
                                   int pno_encoding)
        {
            string digest = mk_digest(eid + ":" + pno + ":" + secret);
            Status = this.KredCall("get_customer_no",
                              new Object[]{PROTO_VSN, CLIENT_VSN, pno, eid, digest,
                                     pno_encoding},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("get_customer_no")]
        public string get_customer_no_rpc(Object[] p)
        {
            return (string)Invoke("get_customer_no_rpc", p);
        }


        //reserve_amount/activate_reservation mk_address
        public XmlRpcStruct mk_address_se(string fname, string lname, string street,
                                          string zip, string city)
        {
            return mk_address(fname, lname, street, zip, city, "se", "", ""); //TODO not "SE"
        }

        public XmlRpcStruct mk_address_no(string fname, string lname, string street,
                                          string zip, string city)
        {
            return mk_address(fname, lname, street, zip, city, "no", "", ""); //TODO not "NO"
        }

        public XmlRpcStruct mk_address_dk(string fname, string lname, string street,
                                          string zip, string city)
        {
            return mk_address(fname, lname, street, zip, city, "dk", "", "");
        }

        public XmlRpcStruct mk_address_fi(string fname, string lname, string street,
                                          string zip, string city)
        {
            return mk_address(fname, lname, street, zip, city, "fi", "", "");
        }

        public XmlRpcStruct mk_address_de(string fname, string lname, string street,
                                          string zip, string city, string house_number)
        {
            return mk_address(fname, lname, street, zip, city, "de", house_number, "");
        }

        public XmlRpcStruct mk_address_nl(string fname, string lname, string street,
                                  string zip, string city, string house_number, string house_extension)
        {
            return mk_address(fname, lname, street, zip, city, "nl", house_number, house_extension);
        }

        public XmlRpcStruct mk_address(string fname, string lname, string street,
                                       string zip, string city, string country, string house_number, string house_extension)
        {
            XmlRpcStruct address = new XmlRpcStruct();
            address.Add("fname", fname);
            address.Add("lname", lname);
            address.Add("street", street);
            address.Add("zip", zip);
            address.Add("city", city);
            address.Add("country", country);
            address.Add("house_number", house_number);
            address.Add("house_extension", house_extension);
            return address;
        }

        //MK_ADDR									

        public XmlRpcStruct mk_addr_se(string careof, string street,
                        int postno, string city,
                        string telno, string cellno,
                        string email)
        {
            return mk_addr(careof, street, postno, city, KRED_ISO3166_SE,
                       telno, cellno, email, "", "");
        }

        public XmlRpcStruct mk_addr_no(string careof, string street,
                        int postno, string city,
                        string telno, string cellno,
                        string email)
        {
            return mk_addr(careof, street, postno, city, KRED_ISO3166_NO,
                       telno, cellno, email, "", "");
        }

        public XmlRpcStruct mk_addr_dk(string careof, string street,
                        int postno, string city,
                        string telno, string cellno,
                        string email)
        {
            return mk_addr(careof, street, postno, city, KRED_ISO3166_DK,
                       telno, cellno, email, "", "");
        }

        public XmlRpcStruct mk_addr_fi(string careof, string street,
                        int postno, string city,
                        string telno, string cellno,
                        string email)
        {
            return mk_addr(careof, street, postno, city, KRED_ISO3166_FI,
                       telno, cellno, email, "", "");
        }

        public XmlRpcStruct mk_addr_de(string careof, string street,
                    int postno, string city,
                    string telno, string cellno,
                    string email, string house_number)
        {
            return mk_addr(careof, street, postno, city, KRED_ISO3166_DE,
                       telno, cellno, email, house_number, "");
        }

        public XmlRpcStruct mk_addr_de(string careof, string street,
            string postno, string city,
            string telno, string cellno,
            string email, string house_number, string house_extension)
        {
            return mk_addr(careof, street, postno, city, KRED_ISO3166_DE,
                       telno, cellno, email, house_number, house_extension);
        }

        public XmlRpcStruct mk_addr(string careof, string street,
                        int postno, string city,
                        int country, string telno,
                        string cellno, string email, string house_number, string house_extension)
        {

            XmlRpcStruct addr = new XmlRpcStruct();
            addr.Add("careof", careof);
            addr.Add("street", street);
            addr.Add("postno", postno);
            addr.Add("city", city);
            addr.Add("country", country);
            addr.Add("telno", telno);
            addr.Add("cellno", cellno);
            addr.Add("email", email);
            addr.Add("house_number", house_number);
            addr.Add("house_extension", house_extension);

            return addr;
        }

        public XmlRpcStruct mk_addr(string careof, string street,
                 string postno, string city,
                 int country, string telno,
                 string cellno, string email, string house_number, string house_extension)
        {

            XmlRpcStruct addr = new XmlRpcStruct();
            addr.Add("careof", careof);
            addr.Add("street", street);
            addr.Add("postno", postno);
            addr.Add("city", city);
            addr.Add("country", country);
            addr.Add("telno", telno);
            addr.Add("cellno", cellno);
            addr.Add("email", email);
            addr.Add("house_number", house_number);
            addr.Add("house_extension", house_extension);

            return addr;
        }

        //MK_GOODS
        public XmlRpcStruct mk_goods(int qty, string artno, string title, int price,
                                     double vat, double discount)
        {
            XmlRpcStruct goods = new XmlRpcStruct();
            goods.Add("artno", artno);
            goods.Add("title", title);
            goods.Add("price", price);
            goods.Add("vat", vat);
            goods.Add("discount", discount);
            XmlRpcStruct tot = new XmlRpcStruct();
            tot.Add("goods", goods);
            tot.Add("qty", qty);
            return tot;
        }

        public XmlRpcStruct mk_goods_flags(int qty, string artno, string title,
                                           int price, double vat, double discount,
                                           int flags)
        {
            XmlRpcStruct goods = new XmlRpcStruct();
            goods.Add("artno", artno);
            goods.Add("title", title);
            goods.Add("price", price);
            goods.Add("vat", vat);
            goods.Add("discount", discount);
            goods.Add("flags", flags);
            XmlRpcStruct tot = new XmlRpcStruct();
            tot.Add("goods", goods);
            tot.Add("qty", qty);
            return tot;
        }

        public XmlRpcStruct mk_artno(int qty, string artno)
        {
            XmlRpcStruct a = new XmlRpcStruct();
            a.Add("artno", artno);
            a.Add("qty", qty);
            return a;
        }

        //MK_DIGEST
        private string mk_digest(string str)
        {
            byte[] bHashResult = new byte[21];
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bMD5Hash = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
            bHashResult = md5.ComputeHash(bMD5Hash);
            return Convert.ToBase64String(bHashResult);
        }
        public int add_invoice_se(int eid,
                       string estoreUser,
                       string secret,
                       string estoreOrderNo,
                       System.Object[] goodsList,
                       int shipmentfee,
                       int shipmenttype,
                       int handlingfee,
                       string pno,
                       string fname,
                       string lname,
                       System.Object addr,
                       string passwd,
                       string clientIp,
                       string newPasswd,
                       int flags,
                       string comment,
                       string ready_date,
                       string rand_str)
        {
            return add_invoice(eid, estoreUser, secret, estoreOrderNo,
                       goodsList, shipmentfee, shipmenttype,
                       handlingfee, pno, fname, lname, addr,
                       passwd, clientIp, newPasswd, flags, comment,
                       ready_date, rand_str, KRED_SEK,
                       KRED_ISO3166_SE, KRED_ISO639_SV, PNO_SE);
        }

        public int add_invoice_no(int eid,
                       string estoreUser,
                       string secret,
                       string estoreOrderNo,
                       System.Object[] goodsList,
                       int shipmentfee,
                       int shipmenttype,
                       int handlingfee,
                       string pno,
                       string fname,
                       string lname,
                       System.Object addr,
                       string passwd,
                       string clientIp,
                       string newPasswd,
                       int flags,
                       string comment,
                       string ready_date,
                       string rand_str)
        {
            return add_invoice(eid, estoreUser, secret, estoreOrderNo,
                       goodsList, shipmentfee, shipmenttype,
                       handlingfee, pno, fname, lname, addr,
                       passwd, clientIp, newPasswd, flags, comment,
                       ready_date, rand_str, KRED_NOK,
                       KRED_ISO3166_NO, KRED_ISO639_NB, PNO_NO);
        }

        public int add_invoice_dk(int eid,
                       string estoreUser,
                       string secret,
                       string estoreOrderNo,
                       System.Object[] goodsList,
                       int shipmentfee,
                       int shipmenttype,
                       int handlingfee,
                       string pno,
                       string fname,
                       string lname,
                       System.Object addr,
                       string passwd,
                       string clientIp,
                       string newPasswd,
                       int flags,
                       string comment,
                       string ready_date,
                       string rand_str)
        {
            return add_invoice(eid, estoreUser, secret, estoreOrderNo,
                       goodsList, shipmentfee, shipmenttype,
                       handlingfee, pno, fname, lname, addr,
                       passwd, clientIp, newPasswd, flags, comment,
                       ready_date, rand_str, KRED_DKK,
                       KRED_ISO3166_DK, KRED_ISO639_DA, PNO_DK);
        }

        public int add_invoice_fi(int eid,
                       string estoreUser,
                       string secret,
                       string estoreOrderNo,
                       System.Object[] goodsList,
                       int shipmentfee,
                       int shipmenttype,
                       int handlingfee,
                       string pno,
                       string fname,
                       string lname,
                       System.Object addr,
                       string passwd,
                       string clientIp,
                       string newPasswd,
                       int flags,
                       string comment,
                       string ready_date,
                       string rand_str)
        {
            return add_invoice(eid, estoreUser, secret, estoreOrderNo,
                       goodsList, shipmentfee, shipmenttype,
                       handlingfee, pno, fname, lname, addr,
                       passwd, clientIp, newPasswd, flags, comment,
                       ready_date, rand_str, KRED_EUR,
                       KRED_ISO3166_FI, KRED_ISO639_FI, PNO_FI);
        }

        public int add_invoice(int eid,
                       string estoreUser,
                       string secret,
                       string estoreOrderNo,
                       System.Object[] goodsList,
                       int shipmentfee,
                       int shipmenttype,
                       int handlingfee,
                       string pno,
                       string fname,
                       string lname,
                       System.Object addr,
                       string passwd,
                       string clientIp,
                       string newPasswd,
                       int flags,
                       string comment,
                       string ready_date,
                       string rand_str,
                       int currency,
                       int country,
                       int language,
                       int pno_encoding)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                       goodsList, shipmentfee, shipmenttype,
                       handlingfee, pno, fname, lname, addr,
                       passwd, clientIp, newPasswd, flags, comment,
                       ready_date, rand_str, currency,
                       country, language, pno_encoding,
                       -1, 0);
        }

        public int add_transaction_se(int eid,
                               string estoreUser,
                               string secret,
                               string estoreOrderNo,
                               System.Object[] goodsList,
                               int shipmentfee,
                               int shipmenttype,
                               int handlingfee,
                               string pno,
                               string fname,
                               string lname,
                               System.Object addr,
                               string passwd,
                               string clientIp,
                               string newPasswd,
                               int flags,
                               string comment,
                               string ready_date,
                               string rand_str,
                               int pclass,
                               int ysalary)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                               goodsList, shipmentfee, shipmenttype,
                               handlingfee, pno, fname, lname, addr,
                               passwd, clientIp, newPasswd, flags, comment,
                               ready_date, rand_str, KRED_SEK,
                               KRED_ISO3166_SE, KRED_ISO639_SV, PNO_SE,
                               pclass, ysalary);
        }

        public int add_transaction_no(int eid,
                               string estoreUser,
                               string secret,
                               string estoreOrderNo,
                               System.Object[] goodsList,
                              int shipmentfee,
                               int shipmenttype,
                               int handlingfee,
                               string pno,
                              string fname,
                               string lname,
                               System.Object addr,
                               string passwd,
                              string clientIp,
                               string newPasswd,
                               int flags,
                               string comment,
                               string ready_date,
                               string rand_str,
                               int pclass,
                               int ysalary)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                              goodsList, shipmentfee, shipmenttype,
                               handlingfee, pno, fname, lname, addr,
                               passwd, clientIp, newPasswd, flags, comment,
                               ready_date, rand_str, KRED_NOK,
                               KRED_ISO3166_NO, KRED_ISO639_NB, PNO_NO,
                               pclass, ysalary);
        }

        public int add_transaction_dk(int eid,
                               string estoreUser,
                               string secret,
                               string estoreOrderNo,
                               System.Object[] goodsList,
                              int shipmentfee,
                               int shipmenttype,
                               int handlingfee,
                               string pno,
                              string fname,
                               string lname,
                               System.Object addr,
                               string passwd,
                              string clientIp,
                               string newPasswd,
                               int flags,
                               string comment,
                               string ready_date,
                               string rand_str,
                               int pclass,
                               int ysalary)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                              goodsList, shipmentfee, shipmenttype,
                               handlingfee, pno, fname, lname, addr,
                               passwd, clientIp, newPasswd, flags, comment,
                               ready_date, rand_str, KRED_DKK,
                               KRED_ISO3166_DK, KRED_ISO639_DA, PNO_DK,
                               pclass, ysalary);
        }

        public int add_transaction_fi(int eid,
                               string estoreUser,
                               string secret,
                               string estoreOrderNo,
                               System.Object[] goodsList,
                              int shipmentfee,
                               int shipmenttype,
                               int handlingfee,
                               string pno,
                              string fname,
                               string lname,
                               System.Object addr,
                               string passwd,
                              string clientIp,
                               string newPasswd,
                               int flags,
                               string comment,
                               string ready_date,
                               string rand_str,
                               int pclass,
                               int ysalary)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                              goodsList, shipmentfee, shipmenttype,
                               handlingfee, pno, fname, lname, addr,
                               passwd, clientIp, newPasswd, flags, comment,
                               ready_date, rand_str, KRED_EUR,
                               KRED_ISO3166_FI, KRED_ISO639_FI, PNO_FI,
                               pclass, ysalary);
        }

        public int add_transaction_de(int eid,
                                string estoreUser,
                                string secret,
                                string estoreOrderNo,
                                System.Object[] goodsList,
                                int shipmentfee,
                                int shipmenttype,
                                int handlingfee,
                                string pno,
                                string fname,
                                string lname,
                                System.Object addr,
                                string passwd,
                                string clientIp,
                                string newPasswd,
                                int flags,
                                string comment,
                                string ready_date,
                                string rand_str,
                                int pclass,
                                int ysalary)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                               goodsList, shipmentfee, shipmenttype,
                               handlingfee, pno, fname, lname, addr,
                               passwd, clientIp, newPasswd, flags, comment,
                               ready_date, rand_str, KRED_EUR,
                               KRED_ISO3166_DE, KRED_ISO639_DE, PNO_DE,
                               pclass, ysalary);
        }

        public int add_transaction_nl(int eid,
                                string estoreUser,
                                string secret,
                                string estoreOrderNo,
                                System.Object[] goodsList,
                                int shipmentfee,
                                int shipmenttype,
                                int handlingfee,
                                string pno,
                                string fname,
                                string lname,
                                System.Object addr,
                                string passwd,
                                string clientIp,
                                string newPasswd,
                                int flags,
                                string comment,
                                string ready_date,
                                string rand_str,
                                int pclass,
                                int ysalary)
        {
            return add_transaction(eid, estoreUser, secret, estoreOrderNo,
                               goodsList, shipmentfee, shipmenttype,
                               handlingfee, pno, fname, lname, addr,
                               passwd, clientIp, newPasswd, flags, comment,
                               ready_date, rand_str, KRED_EUR,
                               KRED_ISO3166_NL, KRED_ISO639_NL, PNO_NL,
                               pclass, ysalary);
        }

        public int add_transaction(int eid,
                       string estoreUser,
                       string secret,
                       string estoreOrderNo,
                       System.Object[] goodsList,
                       int shipmentfee,
                       int shipmenttype,
                       int handlingfee,
                       string pno,
                       string fname,
                       string lname,
                       System.Object addr,
                       string passwd,
                       string clientIp,
                       string newPasswd,
                       int flags,
                       string comment,
                       string ready_date,
                       string rand_str,
                       int currency,
                       int country,
                       int language,
                       int pno_encoding,
                       int pclass,
                       int ysalary)
        {
            string checksum = "";
            for (int i = 0; i < goodsList.Length; i++)
            {
                XmlRpcStruct tot = (goodsList[i] as XmlRpcStruct);
                XmlRpcStruct goods = (tot["goods"] as XmlRpcStruct);

                checksum += goods["title"] + ":";
            }

            checksum += secret;

            string digest = mk_digest(checksum);

            Regex re = new Regex("-");
            Regex re2 = new Regex(" ");
            pno = re.Replace(pno, "");
            pno = re2.Replace(pno, "");

            try
            {
                Value =
              add_invoice_rpc(new Object[]{
		  PROTO_VSN,
		  CLIENT_VSN,
		  eid,
		  estoreUser,
		  digest,
		  estoreOrderNo,
		  goodsList,
		  shipmentfee,
		  shipmenttype,
		  handlingfee,
		  pno,
		  fname,
		  lname,
		  addr,
		  passwd,
		  clientIp,
		  newPasswd,
		  flags,
		  comment,
		  ready_date,
		  rand_str,
		  currency,
		  country,
		  language,
		  pno_encoding,
		  pclass,
		  ysalary
		});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }

            return Status;
        }

        [XmlRpcMethod("add_invoice")]
        public string add_invoice_rpc(Object[] p)
        {
            return (string)Invoke("add_invoice_rpc", p);
        }

        public int csid_to_pno(int eid,
                           string secret,
                       int csid)
        {
            string digest = mk_digest(eid + ":" + csid.ToString() + ":" + secret);

            try
            {
                Value =
              csid_to_pno_rpc(new Object[]{
		  PROTO_VSN,
		  CLIENT_VSN,
		  eid,
		  digest,
		  csid});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }

            return Status;
        }

        [XmlRpcMethod("csid_to_pno")]
        public string csid_to_pno_rpc(Object[] p)
        {
            return (string)Invoke("csid_to_pno_rpc", p);
        }



        public int activate_invoice(int eid,
                         string invno,
                         string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);

            try
            {
                Value =
              activate_invoice_rpc(new Object[]{
		  PROTO_VSN,
		  CLIENT_VSN,
		  eid,
		  invno,
		  digest});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }

            return Status;
        }

        [XmlRpcMethod("activate_invoice")]
        public string activate_invoice_rpc(Object[] p)
        {
            return (string)Invoke("activate_invoice_rpc", p);
        }


        public int activate_invoice_passwd(int eid,
                         string invno,
                         string passwd,
                         string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);

            try
            {
                Value =
              activate_invoice_passwd_rpc(new Object[]{
		  PROTO_VSN,
		  CLIENT_VSN,
		  eid,
		  invno,
		  passwd,
		  digest});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }

            return Status;
        }

        [XmlRpcMethod("activate_invoice_passwd")]
        public string activate_invoice_passwd_rpc(Object[] p)
        {
            return (string)Invoke("activate_invoice_passwd_rpc", p);
        }

        // effective rate for credit with annuity payments
        public int apr_annuity(int sum, int months, int monthfee, int rate, int startfee, int currency)
        {
            double r_min, r_max, r_guess, apr_sum, precision;
            int period_cost;
            precision = 0.001;


            r_guess = 50.00;  // we guess rate is somewhere (below) this value (for faster iteration)
            r_min = 0.0; // lower limit is 0%
            r_max = 10000.0; // upper limit. The value we are looking for should be way lower

            period_cost = total_monthly_cost(sum, months, monthfee, rate, startfee, currency);
            for (int i = 0; i < 1000; i++)
            {
                apr_sum = 0;
                for (int k = 1; k < months + 1; k++)
                {
                    apr_sum += period_cost / (Math.Pow(1 + r_guess / 100, ((double)k / 12)));
                }
                if (apr_sum + precision > sum && apr_sum - precision < sum)
                {
                    return (int)(r_guess * 100);
                }
                else if (apr_sum > sum)
                {
                    r_min = r_guess;
                }
                else
                {
                    r_max = r_guess;
                }
                r_guess = (r_min + r_max) / 2;
            }
            throw new Exception("Du har angivit fel v�rden");
        }

        public int total_credit_purchase_cost(int sum, int pc, int currency)
        {
            int months, monthfee, rate, startfee;
            try
            {
                if (currency == KRED_NOK && pclass.get_type(pc) == KRED_DIVISOR_PCLASS)
                {
                    months = 12;
                }
                else
                {
                    months = pclass.get_months(pc);
                }
                monthfee = pclass.get_month_fee(pc);
                startfee = pclass.get_start_fee(pc);
                rate = pclass.get_rate(pc);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -4;
                FaultString = "Felaktig kampanjkod" + e;
                throw new KreditorException(FaultCode, FaultString);
            }
            return months * total_monthly_cost(sum, months, monthfee, rate, startfee,
                           currency);
        }

        public int total_monthly_cost(int sum, int pc, int currency)
        {
            int months, monthfee, rate, startfee;
            try
            {
                months = pclass.get_months(pc);
                monthfee = pclass.get_month_fee(pc);
                startfee = pclass.get_start_fee(pc);
                rate = pclass.get_rate(pc);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -4;
                FaultString = "Felaktig kampanjkod" + e;
                throw new KreditorException(FaultCode, FaultString);
            }
            return total_monthly_cost(sum, months, monthfee, rate, startfee, currency);
        }
        // calculates the total monthly cost (annuity payments) i.e the cost per 
        // month including all fees, the purchase sum and interest, taking account:
        // - that the startfee is paid at the first payment (with no interest) and 
        // - the monthly fee is paid with every payment before any amortizing is done.
        // This cost should be as close as possible as the real cost will be.
        public int total_monthly_cost(int sum, int months, int monthfee, int rate, int startfee, int currency)
        {
            double R, K0, S, A, N, sum_j_to_N = 0, T;
            //reference: Avbetalningsplaner.pdf
            K0 = (double)sum;
            A = (double)monthfee;
            S = (double)startfee;
            R = Math.Pow((1.0 + (double)rate / 10000), 1.0 / 12.0);
            N = months;

            if (rate < 0)
            {
                throw new Exception("Du har angivit en negativ r�nta");
            }
            else if (rate == 0)
            {
                T = (1 / N) * (K0 + N * A + S);
            }
            else
            {
                // the total cost of fees plus the cost of paying off the monthly fee first and after that amortize 
                for (int j = 2; j < N + 1; j++)
                {
                    sum_j_to_N = sum_j_to_N + Math.Pow(R, N - j) * A;
                }
                T = ((R - 1) / (Math.Pow(R, N) - 1)) * (Math.Pow(R, N) * K0 + sum_j_to_N + Math.Pow(R, N - 1) * (A + S));
            }
            return (int)Math.Round(T, 0);
        }


        public int periodic_cost(int eid, int sum, int pc, int country,
                         int flags, string secret)
        {
            int months, monthfee, startfee, rate;
            double dailyrate, monthpayment, mountscost;
            try
            {
                months = pclass.get_months(pc);
                monthfee = pclass.get_month_fee(pc);
                startfee = pclass.get_start_fee(pc);
                rate = pclass.get_rate(pc);
                dailyrate = daily_rate(rate);
                monthpayment = calc_monthpayment(sum + startfee,
                                 dailyrate, months);
                mountscost = monthpayment + monthfee;
                return (int)Math.Round(mountscost);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -4;
                FaultString = "Felaktig kampanjkod" + e;
                throw new KreditorException(FaultCode, FaultString);
            }
        }

        public int periodic_cost(int sum, int months, int monthfee,
                  int rate, int startfee,
                                  int currency, int flags)
        {
            double dailyrate, monthpayment, monthscost;
            dailyrate = daily_rate(rate);
            monthpayment = calc_monthpayment(sum + startfee, dailyrate, months);
            if (flags.Equals(KRED_ACTUAL_COST))
            {
                monthscost = monthpayment + 0.5;
            }
            else if (flags.Equals(KRED_LIMIT_COST) || flags.Equals(0))
            {
                monthscost = monthpayment + monthfee + 0.5;
            }
            else
            {
                throw new KreditorException(-2, "bad_flags");
            }

            return (int)monthscost;
        }

        public int OneTimePayCost(int sum, int months, int monthfee,
                                   int rate, int startfee,
                                   int currency, int flags)
        {
            double cost=0;
            
            if (flags==1)
            {
                cost = sum + startfee;
            }
            else if ((flags == 0))
            {
                cost = sum + startfee + (monthfee * months);
            }
            else
            {
                throw new KreditorException(-2, "bad_flags");
            }

            return (int) cost;
        }

        /*************************************************************************
      * API: monthly_cost_new
      *************************************************************************/
        public int calc_monthly_cost(int sum, int pc, int flags, int currency)
        {
            int months;
            int monthsfee;
            int type;
            int rate;
            int startfee;
            try
            {
                months = pclass.get_months(pc);
                monthsfee = pclass.get_month_fee(pc);
                type = pclass.get_type(pc);
                rate = pclass.get_rate(pc);
                startfee = pclass.get_start_fee(pc);
            }
            catch (Exception e)
            {
                throw new KreditorException(-2, e.Message);
            }

            return monthly_cost_new2(sum, rate, months, monthsfee, startfee, flags,
                                     currency, type);
        }

        /*************************************************************************
       * API: monthly_cost_new2
       *************************************************************************/
        public int monthly_cost_new2(int sum, int rate, int months,
                                         int monthsfee, int startfee,
                                         int flags, int currency,
                                         int type)
        {
            if (type.Equals(KRED_ANNUITY_PCLASS))
            {
                return periodic_cost(sum, months, monthsfee, rate, startfee,
                                     currency, flags);
            }
            else if (type.Equals(KRED_DIVISOR_PCLASS))
            {
                return monthly_cost(sum, rate, months, monthsfee, flags, currency);
            }
            else if ((type.Equals(KRED_ONETIME_PCLASS)))
                return OneTimePayCost(sum, months, monthsfee, rate, startfee, currency, flags);
            throw new KreditorException(-2, "bad_pclass_type");
        }

        public double daily_rate(int rate)
        {
            double x, y, z;
            x = rate;
            y = x / 10000 + 1;
            z = 1.0 / 365.25;
            return Math.Pow(y, z);
        }

        public double calc_monthpayment(int sum, double dailyrate, int months)
        {
            double denom, sum2, totdates;
            sum2 = sum;
            totdates = (months - 1) * 30.0;
            denom = calc_denom(dailyrate, totdates);
            totdates += 60;
            return (Math.Pow(dailyrate, totdates) * sum2 / denom);
        }

        public double calc_denom(double dailyrate, double totdates)
        {
            double startdates, sum;
            startdates = 0.0;
            sum = 1.0;
            while (totdates > startdates)
            {
                startdates += 30;
                sum += Math.Pow(dailyrate, startdates);
            }
            return sum;
        }

        public int monthly_cost(int sum, double rate, int months, int monthsfee, int flags, int currency)
        {
            if (rate < 100)
                rate *= 100;

            double lowest_monthly_payment = 0, interest_value, periodic_cost;
            int average_interest_period, result;

            switch (currency)
            {
                case 0:
                    lowest_monthly_payment = 5000.00;
                    break;
                case 1:
                    lowest_monthly_payment = 9500.00;
                    break;
                case 2:
                    lowest_monthly_payment = 895.00;
                    break;
                case 3:
                    lowest_monthly_payment = 8900.00;
                    break;
                default:
                    throw new Exception("Du har angivet en felaktig valuta");
            }

            average_interest_period = 45;
            double calcRate = (rate / 10000);
            interest_value = (average_interest_period / 365.0) * calcRate * sum;
            periodic_cost = (sum + interest_value) / months;

            if (flags == 1)
            {
                result = round_up(periodic_cost, currency);
            }
            else if (flags == 0)
            {

                periodic_cost = periodic_cost + monthsfee;

                if (periodic_cost < lowest_monthly_payment)
                {
                    result = (int)lowest_monthly_payment;
                }
                else
                {
                    result = round_up(periodic_cost, currency);
                }

            }
            else
            {
                throw new Exception("Du har angivit en felaktig flagga");
            }

            return result;
        }

        public int round_up(double value, int currency) //value, currency
        {
            int divisor = 0;

            if (currency == 2)
                divisor = 10;
            else
                divisor = 100;

            double result = divisor * Math.Round(((divisor / 2) + value) / divisor); //We want to roundup to closest integer.

            return (int)result;
        }

        [XmlRpcMethod("activate_part")]
        public XmlRpcStruct activate_part_rpc(Object[] p)
        {
            return (XmlRpcStruct)Invoke("activate_part_rpc", p);
        }

        public int activate_part(int eid, string invno, System.Object[] artnos, string secret)
        {
            string checksum = "";
            int status;
            for (int i = 0; i < artnos.Length; i++)
            {
                XmlRpcStruct a = (artnos[i] as XmlRpcStruct);
                checksum += a["artno"] + ":" + a["qty"].ToString() + ":";
            }

            string digest = mk_digest(eid + ":" + invno + ":" + checksum + secret);

            status = this.KredCall("activate_part", new Object[]{PROTO_VSN, CLIENT_VSN, eid, invno, artnos,
                                     digest},
                            eid, secret, "struct");

            return status;
        }

        [XmlRpcMethod("invoice_amount")]
        public int invoice_amount_rpc(object[] p)
        {
            return (int)Invoke("invoice_amount_rpc", p);
        }

        public int invoice_amount(int eid, string invno, string secret)
        {
            int status = this.KredCall("invoice_amount", new object[] { PROTO_VSN, CLIENT_VSN, eid, invno, mk_digest(eid + ":" + invno + ":" + secret) }, eid, secret, "int");

            return status;
        }

        [XmlRpcMethod("invoice_part_amount")]
        public int invoice_part_amount_rpc(object[] p)
        {
            return (int)Invoke("invoice_part_amount_rpc", p);
        }

        public int invoice_part_amount(int eid, string invno, object[] artnos, string secret)
        {
            int status;
            string checksum = "";
            for (int i = 0; i < artnos.Length; i++)
            {
                XmlRpcStruct a = (artnos[i] as XmlRpcStruct);
                checksum += a["artno"] + ":" + a["qty"].ToString() + ":";
            }
            string digest = mk_digest(eid + ":" + invno + ":" + checksum + secret);

            status = this.KredCall("invoice_part_amount",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, invno, artnos,
                                     digest},
                              eid, secret, "int");


            return status;
        }

        public int get_pclasses(int eid, int cur, string secret)
        {
            string digest = mk_digest(eid + ":" + cur + ":" + secret);

            Status = this.KredCall("get_pclasses",
                                    new Object[] { PROTO_VSN, CLIENT_VSN, eid, cur, digest },
                                    eid, secret, "Arrarr");
            return Status;
        }

        public int fetch_pclasses(int eid, int cur, string secret, int country, int language)
        {
            string digest = mk_digest(eid + ":" + cur + ":" + secret);

            Status = this.KredCall("fetch_pclasses",
                                    new Object[] { PROTO_VSN, CLIENT_VSN, eid, cur, digest, country, language },
                                    eid, secret, "ObjArr");
            return Status;
        }

        [XmlRpcMethod("get_pclasses")]
        public object[] fetch_pclasses_rpc(Object[] p)
        {
            return (object[])Invoke("get_pclasses_rpc", p);
        }

        [XmlRpcMethod("get_pclasses")]
        public string[,] get_pclasses_rpc(Object[] p)
        {
            return (string[,])Invoke("get_pclasses_rpc", p);
        }

        public int invoice_add(int eid, string invno, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = this.KredCall("invoice_address",
                              new Object[] { PROTO_VSN, CLIENT_VSN, eid, invno, digest },
                              eid, secret, "ValueArr");
            return Status;
        }

        [XmlRpcMethod("invoice_address")]
        public string[] invoice_address_rpc(Object[] p)
        {
            return (string[])Invoke("invoice_address_rpc", p);
        }


        public int invoice_info(int eid, string invno, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = this.KredCall("invoice_info",
                              new Object[] { PROTO_VSN, CLIENT_VSN, eid, invno, digest },
                              eid, secret, "ValueArr");
            return Status;
        }

        [XmlRpcMethod("invoice_info")]
        public string[] invoice_info_rpc(Object[] p)
        {
            return (string[])Invoke("invoice_info_rpc", p);
        }


        public int send_invoice(int eid, string invno, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = this.KredCall("send_invoice",
                              new Object[] { PROTO_VSN, CLIENT_VSN, eid, invno, digest },
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("send_invoice")]
        public string send_invoice_rpc(Object[] p)
        {
            return (string)Invoke("send_invoice_rpc", p);
        }


        public int email_invoice(int eid, string invno, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = this.KredCall("email_invoice",
                              new Object[] { PROTO_VSN, CLIENT_VSN, eid, invno, digest },
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("email_invoice")]
        public string email_invoice_rpc(Object[] p)
        {
            return (string)Invoke("email_invoice_rpc", p);
        }

        //DELETE_INVOICE


        public int delete_invoice(int eid, string invno, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = KredCall("delete_invoice",
                              new Object[] { PROTO_VSN, CLIENT_VSN, eid, invno, digest },
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("delete_invoice")]
        public string delete_invoice_rpc(Object[] p)
        {
            return (string)Invoke("delete_invoice_rpc", p);
        }


        public int return_invoice(int eid, string invno, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = this.KredCall("return_invoice",
                              new Object[] { PROTO_VSN, CLIENT_VSN, eid, invno, digest },
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("return_invoice")]
        public string return_invoice_rpc(Object[] p)
        {
            return (string)Invoke("return_invoice_rpc", p);
        }



        public int credit_invoice(int eid, string invno, string credno,
                                  string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = this.KredCall("credit_invoice",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, invno, credno,
                                     digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("credit_invoice")]
        public string credit_invoice_rpc(Object[] p)
        {
            return (string)Invoke("credit_invoice_rpc", p);
        }

        public int return_part(int eid, string invno, System.Object[] artnos,
                               string secret)
        {
            string checksum = "";
            for (int i = 0; i < artnos.Length; i++)
            {
                XmlRpcStruct a = (artnos[i] as XmlRpcStruct);
                checksum += a["artno"] + ":" + a["qty"].ToString() + ":";
            }
            string digest = mk_digest(eid + ":" + invno + ":" + checksum + secret);
            Status = this.KredCall("return_part",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, invno, artnos,
                                     digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("return_part")]
        public string return_part_rpc(Object[] p)
        {
            return (string)Invoke("return_part_rpc", p);
        }


        public int credit_part(int eid, string invno, System.Object[] artnos,
                               string credno, string secret)
        {
            string checksum = "";
            for (int i = 0; i < artnos.Length; i++)
            {
                XmlRpcStruct a = (artnos[i] as XmlRpcStruct);
                checksum += a["artno"] + ":" + a["qty"].ToString() + ":";
            }
            string digest = mk_digest(eid + ":" + invno + ":" + checksum + secret);
            Status = this.KredCall("credit_part",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, invno, artnos,
                                     credno, digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("credit_part")]
        public string credit_part_rpc(Object[] p)
        {
            return (string)Invoke("credit_part_rpc", p);
        }

        public int return_amount(int eid, string invno, int amount,
                              int vat, string secret)
        {
            string digest = mk_digest(eid + ":" + invno + ":" + secret);
            Status = KredCall("return_amount",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, invno, amount,
									 vat, digest},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("return_amount")]
        public string return_amount_rpc(Object[] p)
        {
            return (string)Invoke("return_amount_rpc", p);
        }

        public int update_goods_qty(int eid, string invno, string secret,
                                    string artno, int newqty)
        {
            string digest = mk_digest(invno + ":" + artno + ":" + newqty + ":" + secret);
            Status = this.KredCall("update_goods_qty",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, digest, invno,
                                     artno, newqty},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("update_goods_qty")]
        public string update_goods_qty_rpc(Object[] p)
        {
            return (string)Invoke("update_goods_qty_rpc", p);
        }


        public int update_orderno(int eid, string invno, string secret,
                                  string estoreOrderNo)
        {
            string digest = mk_digest(invno + ":" + estoreOrderNo + ":" + secret);
            Status = this.KredCall("update_orderno",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, digest, invno,
                                     estoreOrderNo},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("update_orderno")]
        public string update_orderno_rpc(Object[] p)
        {
            return (string)Invoke("update_orderno_rpc", p);
        }


        public int update_notes(int eid, string invno, string secret, string notes)
        {
            string digest = mk_digest(invno + ":" + notes + ":" + secret);
            Status = this.KredCall("update_notes",
                              new Object[]{PROTO_VSN, CLIENT_VSN, eid, digest, invno,
                                     notes},
                              eid, secret, "Value");
            return Status;
        }

        [XmlRpcMethod("update_notes")]
        public string update_notes_rpc(Object[] p)
        {
            return (string)Invoke("update_notes_rpc", p);
        }

        public int update_email(int eid,
                    string secret,
                    string pno,
                    string email)
        {
            string digest = mk_digest(pno + ":" + email + ":" + secret);

            try
            {
                Value =
              update_email_rpc(new Object[]{
		  PROTO_VSN,
		  CLIENT_VSN,
		  eid,
		  digest,
		  pno,
		  email});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }
            return Status;
        }

        [XmlRpcMethod("update_email")]
        public string update_email_rpc(Object[] p)
        {
            return (string)Invoke("update_email_rpc", p);
        }


        public int update_charge_amount(int eid,
                           string invno,
                           string secret,
                           int type,
                           int newamount)
        {
            string digest = mk_digest(invno + ":" + type + ":" + newamount + ":" + secret);

            try
            {
                Value =
              update_charge_amount_rpc(new Object[]{
		  PROTO_VSN,
		  CLIENT_VSN,
		  eid,
		  digest,
		  invno,
		  type,
		  newamount});
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                throw new KreditorException(FaultCode, FaultString);
            }
            catch (Exception e)
            {
                Status = ERROR;
                FaultCode = -99;
                FaultString = e.ToString();
                throw new KreditorException(FaultCode, FaultString);
            }

            return Status;
        }

        [XmlRpcMethod("update_charge_amount")]
        public string update_charge_amount_rpc(Object[] p)
        {
            return (string)Invoke("update_charge_amount_rpc", p);
        }

        public static string strerror(string reason)
        {
            string res;

            switch (reason)
            {
                case "unknown_estore":
                case "invalid_estore_secret":
                    res = ("Det har uppst�tt ett integrationsfel mellan butiken " +
                          "och Klarna(felkod: " + reason + ").  Kontakta Webbutiken " +
                          "f�r mer information eller v�lj ett annat s�tt att betala.");
                    break;

                case "estore_blacklisted":
                case "customer_blacklisted":
                    res = ("Ett problem har uppst�tt i kommunikationen med Klarna, " +
                           "som sk�ter v�r fakturering. V�nligen kontakta " +
                           "Klarna (http://www.faktureramig.se/) " +
                           "f�r mer information eller v�lj ett annat s�tt att betala.");
                    break;

                case "bad_customer_password":
                    res = "Du har angivit ett felaktigt l�senord.";
                    break;

                case "dead":
                case "no_such_person":
                case "pno":
                case "invalid_pno":
                    res = ("Du har angivit ett felaktigt personnummer, var v�nlig " +
                          "och kontrollera om du har r�kat skriva fel.");
                    break;

                case "under_aged":
                case "customer_not_18":
                    res = "Tyv�rr m�ste man vara 18 �r f�r att kunna f� handla genom Klarna.";
                    break;

                case "estore_overrun":
                case "amount":
                case "customer_credit_overrun":
                    res = ("Tyv�rr kan inte Klarna, som sk�ter v�r " +
                           "fakturering, godk�nna ditt k�p d� du har " +
                           "�verskridit din kreditgr�ns. V�nligen kontakta " +
                           "Klarnas kundtj�nst (http://www.faktureramig.se) " +
                           "f�r att tillsammans hitta en l�sning eller v�lj ett " +
                           "annat s�tt att betala.");
                    break;

                case "blocked":
                case "customer_not_accepted":
                case "unpaid_bills":
                    res = ("Tyv�rr kan inte Klarna, som sk�ter v�r " +
                           "fakturering, godk�nna k�pet efter kreditkontroll. " +
                           "V�nligen kontakta " +
                           "Klarnas kundtj�nst (http://www.faktureramig.se) f�r att " +
                           "tillsammans hitta en l�sning eller v�lj ett annat s�tt att betala.");
                    break;

                case "bad_name":
                    res = ("Du har angivit ett namn som inte st�mmer �verens " +
                           "med ditt personnummer. Var v�nlig och kontrollera " +
                           "om du har r�kat skriva fel eller v�lj ett annat s�tt att betala.");
                    break;

                case "foreign_addr":
                case "bad_addr":
                case "address":
                    res = ("Du har angivit en adress som inte st�mmer �verens " +
                           "med ditt personnummer. Var v�nlig och kontrollera " +
                           "om du har r�kat skriva fel eller v�lj ett annat s�tt att betala.");
                    break;

                case "postno":
                case "bad_postno":
                    res = ("Du har angivit ett felaktigt postnummer. Var v�nlig " +
                          "och kontrollera om du har r�kat skriva fel eller v�lj ett " +
                          "annat s�tt att betala");
                    break;

                case "no_such_subscription":
                    res = ("This error means that an operation such as freeze " +
                           "refered to a customer subscription that doesn't " +
                           "exist");
                    break;
                case "not_unique_subscription_no":
                    res = ("The estore must provide estore order numbers that " +
                           "are unique for that estore, if they don't this error " +
                           "is returned from the XML-RPC operations");
                    break;
                case "terminated":
                    res = ("It's not possible to i.e freeze a terminated " +
                           "subscription");
                    break;
                case "already_set":
                    res = "It's not possible to i.e freeze a frozen subscription";
                    break;

                case "need_email_addr":
                    res = ("This error may be returned when the subscription is " +
                           "created and (a) the subscription type requires auth " +
                           "by email and (b) the XML-RPC from the estore that " +
                           "tries to create the subscription doesn't have a " +
                           "valid email addr in the email field");
                    break;
                case "no address":
                case "no_address":
                    res = ("Det finns ingen adress registrerad f�r denna personnummer");
                    break;
                default:
                    res = ("Ett problem har uppst�tt i kommunikationen med Klarna, " +
                           "som sk�ter v�r fakturering. V�nligen kontakta " +
                           "Klarna (http://www.faktureramig.se/) f�r mer " +
                           "information eller v�lj ett annat s�tt att betala." +
                           "(Felkod: " + reason + ")");

                    break;
            }

            return res;
        }
        // RETURN ERROR IN SWEDISH
        public static string strerror_se(string reason)
        {
            string res;

            switch (reason)
            {
                case "unknown_estore":
                case "invalid_estore_secret":
                    res = ("Det har uppst�tt ett integrationsfel mellan butiken " +
                          "och Klarna(felkod: " + reason + ").  Kontakta Webbutiken " +
                          "f�r mer information eller v�lj ett annat s�tt att betala.");
                    break;

                case "estore_blacklisted":
                case "customer_blacklisted":
                    res = ("Ett problem har uppst�tt i kommunikationen med Klarna, " +
                           "som sk�ter v�r fakturering. V�nligen kontakta " +
                           "Klarna (http://www.faktureramig.se/) " +
                           "f�r mer information eller v�lj ett annat s�tt att betala.");
                    break;

                case "bad_customer_password":
                    res = "Du har angivit ett felaktigt l�senord.";
                    break;

                case "dead":
                case "no_such_person":
                case "pno":
                case "invalid_pno":
                    res = ("Du har angivit ett felaktigt personnummer, var v�nlig " +
                          "och kontrollera om du har r�kat skriva fel.");
                    break;

                case "under_aged":
                case "customer_not_18":
                    res = "Tyv�rr m�ste man vara 18 �r f�r att kunna f� handla genom Klarna.";
                    break;

                case "estore_overrun":
                case "amount":
                case "customer_credit_overrun":
                    res = ("Tyv�rr kan inte Klarna, som sk�ter v�r " +
                           "fakturering, godk�nna ditt k�p d� du har " +
                           "�verskridit din kreditgr�ns. V�nligen kontakta " +
                           "Klarnas kundtj�nst (http://www.faktureramig.se) " +
                           "f�r att tillsammans hitta en l�sning eller v�lj ett " +
                           "annat s�tt att betala.");
                    break;

                case "blocked":
                case "customer_not_accepted":
                case "unpaid_bills":
                    res = ("Tyv�rr kan inte Klarna, som sk�ter v�r " +
                           "fakturering, godk�nna k�pet efter kreditkontroll. " +
                           "V�nligen kontakta " +
                           "Klarnas kundtj�nst (http://www.faktureramig.se) f�r att " +
                           "tillsammans hitta en l�sning eller v�lj ett annat s�tt att betala.");
                    break;

                case "bad_name":
                    res = ("Du har angivit ett namn som inte st�mmer �verens " +
                           "med ditt personnummer. Var v�nlig och kontrollera " +
                           "om du har r�kat skriva fel eller v�lj ett annat s�tt att betala.");
                    break;

                case "foreign_addr":
                case "bad_addr":
                case "address":
                    res = ("Du har angivit en adress som inte st�mmer �verens " +
                           "med ditt personnummer. Var v�nlig och kontrollera " +
                           "om du har r�kat skriva fel eller v�lj ett annat s�tt att betala.");
                    break;

                case "postno":
                case "bad_postno":
                    res = ("Du har angivit ett felaktigt postnummer. Var v�nlig " +
                          "och kontrollera om du har r�kat skriva fel eller v�lj ett " +
                          "annat s�tt att betala");
                    break;

                case "no_such_subscription":
                    res = ("This error means that an operation such as freeze " +
                           "refered to a customer subscription that doesn't " +
                           "exist");
                    break;
                case "not_unique_subscription_no":
                    res = ("The estore must provide estore order numbers that " +
                           "are unique for that estore, if they don't this error " +
                           "is returned from the XML-RPC operations");
                    break;
                case "terminated":
                    res = ("It's not possible to i.e freeze a terminated " +
                           "subscription");
                    break;
                case "already_set":
                    res = "It's not possible to i.e freeze a frozen subscription";
                    break;

                case "need_email_addr":
                    res = ("This error may be returned when the subscription is " +
                           "created and (a) the subscription type requires auth " +
                           "by email and (b) the XML-RPC from the estore that " +
                           "tries to create the subscription doesn't have a " +
                           "valid email addr in the email field");
                    break;
                case "no address":
                case "no_address":
                    res = ("Det finns ingen adress registrerad f�r denna personnummer");
                    break;
                default:
                    res = ("Ett problem har uppst�tt i kommunikationen med Klarna, " +
                           "som sk�ter v�r fakturering. V�nligen kontakta " +
                           "Klarna (http://www.faktureramig.se/) f�r mer " +
                           "information eller v�lj ett annat s�tt att betala." +
                           "(Felkod: " + reason + ")");

                    break;
            }

            return res;
        }

        // RETURN ERROR IN FINNISH
        public static string strerror_fi(string reason)
        {
            string res;

            switch (reason)
            {
                case "unknown_estore":
                case "invalid_estore_secret":
                    res = ("On syntynyt integrointivirhe kaupan ja Klarnan " +
                           "v�lill� (felkod: " + reason + "). Ota yhteytt� " +
                           "verkkokauppaan tai valitse toinen maksutapa.");
                    break;

                case "estore_blacklisted":
                case "customer_blacklisted":
                    res = ("Jotain on tapahtunut kommunikaatiossa laskupalvelun " +
                           "hoitajan, Klarnan, kanssa. Ole yst�v�llinen ja ota " +
                           "yhteytt� Klarnaan (http://www.laskulla.fi) tai " +
                           "valitse toinen maksutapa.");
                    break;

                case "bad_customer_password":
                    res = "Olet sy�tt�nyt v��r�n salasanan.";
                    break;

                case "dead":
                case "no_such_person":
                case "pno":
                case "invalid_pno":
                    res = ("Olet sy�tt�ny v��r�n henkil�tunnuksen. " +
                           "Ole hyv� ja tarkista tunnuksesi.");
                    break;

                case "under_aged":
                case "customer_not_18":
                    res = ("Sinun t�ytyy valitettavasti olla 18 vuotta, " +
                           "jotta voit ostaa Klarnan laskupalvelulla.");
                    break;

                case "estore_overrun":
                case "amount":
                case "customer_credit_overrun":
                    res = ("Klarna, joka hoitaa laskupalvelua, ei voi valitettavasti " +
                          "hyv�ksy� ostostasi, koska olet ylitt�nyt luottorajasi. " +
                          "Ole yst�v�llinen ja ota yhteytt� Klarnan " +
                          "asiakaspalveluun (http://www.laskulla.fi) tai valitse " +
                          "toinen maksutapa.");
                    break;

                case "blocked":
                case "customer_not_accepted":
                case "unpaid_bills":
                    res = ("Klarna, joka hoitaa laskupalvelua, ei voi valitettavasti " +
                           "hyv�ksy� ostostasi luottotarkistuksen j�lkeen. Ole " +
                           "yst�v�llinen ja ota yhteytt� Kreditorin asiakaspalveluun " +
                           "(http://www.laskulla.fi) tai valitse toinen maksutapa.");
                    break;

                case "bad_name":
                    res = ("Olet ilmoittanut nimen, joka ei t�sm�� henkil�tunnukseesi. " +
                           "Ole yst�v�llinen ja tarkista jos olet kirjoittanut sen v��rin " +
                           "tai valitse toinen maksutapa.");
                    break;

                case "foreign_addr":
                case "bad_addr":
                case "address":
                    res = ("Olet ilmoittanut osoitteen, joka ei t�sm�� henkil�tunnukseesi. " +
                           "Ole yst�v�llinen ja tarkista, jos olet kirjoittanut sen v��rin " +
                           "tai valitse toinen maksutapa.");
                    break;

                case "postno":
                case "bad_postno":
                    res = ("Olet ilmoittanut v��r�n postinumeron. Ole yst�v�llinen ja " +
                           "tarkista, jos olet sen kirjoittanut v��rin tai valitse " +
                           "toinen maksutapa.");
                    break;

                case "no_such_subscription":
                    res = ("This error means that an operation such as freeze " +
                           "refered to a customer subscription that doesn't " +
                           "exist");
                    break;
                case "not_unique_subscription_no":
                    res = ("The estore must provide estore order numbers that " +
                           "are unique for that estore, if they don't this error " +
                           "is returned from the XML-RPC operations");
                    break;
                case "terminated":
                    res = ("It's not possible to i.e freeze a terminated " +
                           "subscription");
                    break;
                case "already_set":
                    res = "It's not possible to i.e freeze a frozen subscription";
                    break;

                case "need_email_addr":
                    res = ("This error may be returned when the subscription is " +
                           "created and (a) the subscription type requires auth " +
                           "by email and (b) the XML-RPC from the estore that " +
                           "tries to create the subscription doesn't have a " +
                           "valid email addr in the email field");
                    break;
                default:
                    res = ("Jotain on tapahtunut kommunikaatiossa laskupalvelun " +
                           "hoitajan, Klarnan, kanssa. Ole yst�v�llinen ja ota " +
                           "yhteytt� Klarnaan (http://www.laskulla.fi) tai valitse " +
                           "toinen maksutapa. (Virhekoodi: " + reason + ")");

                    break;
            }

            return res;
        }
        // RETURN ERROR IN DANISH
        public static string strerror_dk(string reason)
        {
            string res;

            switch (reason)
            {
                case "unknown_estore":
                case "invalid_estore_secret":
                    res = ("Et problem har opst�et (fejlkod: " + reason + "), " +
                           "v�r venlig at kontakte butikken for mere information " +
                           "eller v�lg en anden betalingsmetode.");
                    break;

                case "estore_blacklisted":
                case "customer_blacklisted":
                    res = ("Et problem har opst�et med kommunikationen til Klarna " +
                           "(http://www.faktureremig.dk) som h�ndterer vores fakturering. " +
                           "Var venlig at kontakte Klarna for mere information eller " +
                           "v�lg en anden betalingsmetode.");
                    break;

                case "bad_customer_password":
                    res = "Fejl i login, forkert password.";
                    break;

                case "dead":
                case "no_such_person":
                case "pno":
                case "invalid_pno":
                    res = ("Fejl i login, forkert personnummer. V�r venlig at kontrollere " +
                           "at du indtastet det korrekte personnummer.");
                    break;

                case "under_aged":
                case "customer_not_18":
                    res = "For at handle gennem Klarna skal du v�re fyldt 18 �r.";
                    break;

                case "estore_overrun":
                case "amount":
                case "customer_credit_overrun":
                    res = ("Klarna som ansvarer for vores fakturaer beklager meget " +
                           "at m�tte meddele dem at din betaling ikke kan accepteres " +
                           "p� grund af at den overskrider din kreditgr�nse. V�r venlig at " +
                           "kontakte Klarna (http://www.faktureremig.dk) s� at vi kan " +
                           "finde en l�sning eller v�lg en anden betalingsmetode.");
                    break;

                case "blocked":
                case "customer_not_accepted":
                case "unpaid_bills":
                    res = ("Klarna som ansvarer for vores fakturaer beklager meget at " +
                           "m�tte meddele dem at din betaling ikke kan accepteres p� grund " +
                           "af at du ikke er godkendt af vores kreditoplysning. V�r venlig " +
                           "at kontakte Klarna (http://www.faktureremig.dk) s� at vi kan " +
                           "finde en l�sning eller v�lg en anden betalingsmetode.");
                    break;

                case "bad_name":
                    res = ("Navnet du har intastet stemmer ikke overens med dit personnummer. " +
                           "V�r venlig at kontrollere at du har intastet det korrekte navn " +
                           "og pr�v igen.");
                    break;

                case "foreign_addr":
                case "bad_addr":
                case "address":
                    res = ("Den adresse som du har intastet stemmer ikke overnes med dit " +
                           "personnummer. V�r venlig at kontrollere at du har intastet den " +
                           "korrekte adresse og pr�v igen.");
                    break;

                case "postno":
                case "bad_postno":
                    res = ("Du har intastet en postnummer som ikke existerer. Venlig at " +
                           "kontrollere at du har intastet det korrekte postnummer og pr�v igen.");
                    break;

                case "no_such_subscription":
                    res = ("This error means that an operation such as freeze " +
                           "refered to a customer subscription that doesn't " +
                           "exist");
                    break;
                case "not_unique_subscription_no":
                    res = ("The estore must provide estore order numbers that " +
                           "are unique for that estore, if they don't this error " +
                           "is returned from the XML-RPC operations");
                    break;
                case "terminated":
                    res = ("It's not possible to i.e freeze a terminated " +
                           "subscription");
                    break;
                case "already_set":
                    res = "It's not possible to i.e freeze a frozen subscription";
                    break;

                case "need_email_addr":
                    res = ("This error may be returned when the subscription is " +
                           "created and (a) the subscription type requires auth " +
                           "by email and (b) the XML-RPC from the estore that " +
                           "tries to create the subscription doesn't have a " +
                           "valid email addr in the email field");
                    break;
                default:
                    res = ("Et problem har opst�et med kommunikationen til " +
                           "Klarna (http://www.faktureremig.dk) som h�ndterer " +
                           "vores fakturering. Var venlig at kontakte Klarna for " +
                           "mere information eller v�lg en anden " +
                           "betalingsmetode. (fejlkod: " + reason + ")");

                    break;
            }

            return res;
        }
        // RETURN ERROR IN NORWEGIAN
        public static string strerror_no(string reason)
        {
            string res;

            switch (reason)
            {
                case "unknown_estore":
                case "invalid_estore_secret":
                    res = ("Det oppstod en feil mellom butikken og Klarna " +
                           "(feilkode: " + reason + "). Vennligst kontakt butikken for " +
                           "mer informasjon eller velg en annen betalingsmetode.");
                    break;

                case "estore_blacklisted":
                case "customer_blacklisted":
                    res = ("Det har oppst�tt et problem i kommunikasjonen med Klarna " +
                           "som h�ndterer faktureringen. Vennligst kontakt Klarna " +
                           "(http://www.fakturermeg.no) for mer informasjon eller velg " +
                           "en annen betalingsmetode.");
                    break;

                case "bad_customer_password":
                    res = "Du har oppgitt feil passord.";
                    break;

                case "dead":
                case "no_such_person":
                case "pno":
                case "invalid_pno":
                    res = "Du har oppgitt feil personnummer. Vennligst kontroller og fors�k igjen.";
                    break;

                case "under_aged":
                case "customer_not_18":
                    res = "Beklager, men du m� v�re minst 18 �r for � handle via Klarna.";
                    break;

                case "estore_overrun":
                case "amount":
                case "customer_credit_overrun":
                    res = ("Beklager, men Klarna som h�ndterer v�re fakturaer, kan ikke " +
                           "akseptere betalingsmetoden ettersom kj�pesummen overstiger din " +
                           "kredittgrense. Vennligst kontakt Klarna (http://www.fakturermeg.no) " +
                           "for mer informasjon eller velg en annen betalingsmetode.");
                    break;

                case "blocked":
                case "customer_not_accepted":
                case "unpaid_bills":
                    res = ("Beklager, men Klarna som h�ndterer v�re fakturaer, kan " +
                           "ikke akseptere betalingsmetoden etter en kredittsjekk. " +
                           "Vennligst kontakt Klarna (http://www.fakturermeg.no) for mer " +
                           "informasjon eller velg en annen betalingsmetode.");
                    break;

                case "bad_name":
                    res = ("Du har oppgitt et navn som ikke stemmer overens med det " +
                           "oppgitte personnummeret. Vennligst kontroller og fors�k igjen.");
                    break;

                case "foreign_addr":
                case "bad_addr":
                case "address":
                    res = ("Du har oppgitt en adresse som ikke stemmer overens med det " +
                           "oppgitte personnummeret. Vennligst kontroller og fors�k igjen.");
                    break;

                case "postno":
                case "bad_postno":
                    res = ("Du har oppgitt et postnummer som ikke eksisterer. Vennligst " +
                           "kontroller og fors�k igjen.");
                    break;

                case "no_such_subscription":
                    res = ("This error means that an operation such as freeze " +
                           "refered to a customer subscription that doesn't " +
                           "exist");
                    break;
                case "not_unique_subscription_no":
                    res = ("The estore must provide estore order numbers that " +
                           "are unique for that estore, if they don't this error " +
                           "is returned from the XML-RPC operations");
                    break;
                case "terminated":
                    res = ("It's not possible to i.e freeze a terminated " +
                           "subscription");
                    break;
                case "already_set":
                    res = "It's not possible to i.e freeze a frozen subscription";
                    break;

                case "need_email_addr":
                    res = ("This error may be returned when the subscription is " +
                           "created and (a) the subscription type requires auth " +
                           "by email and (b) the XML-RPC from the estore that " +
                           "tries to create the subscription doesn't have a " +
                           "valid email addr in the email field");
                    break;
                default:
                    res = ("Det har oppst�tt et problem i kommunikasjonen med " +
                           "Klarna som h�ndterer faktureringen. Vennligst kontakt " +
                           "Klarna (http://www.fakturermeg.no) for mer informasjon " +
                           "eller velg en annen betalingsmetode. (errorcode: " + reason + ")");

                    break;
            }

            return res;
        }
        // RETURN ERROR IN ENGLISH
        public static string strerror_en(string reason)
        {
            string res;

            switch (reason)
            {
                case "unknown_estore":
                case "invalid_estore_secret":
                    res = ("An integration problem has occured between " +
                           "the Store and Klarna (error code: " + reason +
                           "). Please contact the Store for more information " +
                           "or choose another payment method.");
                    break;

                case "estore_blacklisted":
                case "customer_blacklisted":
                    res = ("A problem has occured in the communication to Klarna " +
                           "that handles our invoicing. Please contact " +
                           "Klarna (http://www.kreditor.se) for more information " +
                           "or choose another payment method.");
                    break;

                case "bad_customer_password":
                    res = "The password you have given was not accepted.";
                    break;

                case "dead":
                case "no_such_person":
                case "pno":
                case "invalid_pno":
                    res = ("The social security number you have given was not " +
                           "accepted. Please check for misspellings and try again, " +
                           "or choose another payment method.");
                    break;

                case "under_aged":
                case "customer_not_18":
                    res = ("You have to be above the age of 18 to be able to " +
                          "make purchases through Klarna.");
                    break;

                case "estore_overrun":
                case "amount":
                case "customer_credit_overrun":
                    res = ("Klarna, that handles our invoicing, cannot accept " +
                           "your purchase because it exceeds your accepted limit. " +
                           "Please contact Klarna (http://www.klarna.com) to solve " +
                           "this or choose another payment method.");
                    break;

                case "blocked":
                case "customer_not_accepted":
                case "unpaid_bills":
                    res = ("Klarna, that handles our invoicing, cannot accept " +
                           "your purchase after a credit check. Please contact " +
                           "Klarna (http://www.klarna.com) to solve this or " +
                           "choose another payment method.");
                    break;

                case "bad_name":
                    res = ("The name you have given does not correspond with " +
                           "the social security number you submitted. Please " +
                           "check for misspellings and try again, or choose " +
                           "another payment method.");
                    break;

                case "foreign_addr":
                case "bad_addr":
                case "address":
                    res = ("The address you have given does not correspond " +
                           "with your personal identification number you submitted. " +
                           "Please check for misspellings and try again, or choose " +
                           "another payment method.");
                    break;

                case "postno":
                case "bad_postno":
                    res = ("The post number you have given is not valid. Please " +
                           "check for misspellings and try again, or choose " +
                           "another payment method.");
                    break;

                case "no_such_subscription":
                    res = ("This error means that an operation such as freeze " +
                           "refered to a customer subscription that doesn't " +
                           "exist");
                    break;
                case "not_unique_subscription_no":
                    res = ("The estore must provide estore order numbers that " +
                           "are unique for that estore, if they don't this error " +
                           "is returned from the XML-RPC operations");
                    break;
                case "terminated":
                    res = ("It's not possible to i.e freeze a terminated " +
                           "subscription");
                    break;
                case "already_set":
                    res = "It's not possible to i.e freeze a frozen subscription";
                    break;

                case "need_email_addr":
                    res = ("This error may be returned when the subscription is " +
                           "created and (a) the subscription type requires auth " +
                           "by email and (b) the XML-RPC from the estore that " +
                           "tries to create the subscription doesn't have a " +
                           "valid email addr in the email field");
                    break;
                case "no address":
                case "no_address":
                    res = ("There is no registered address for this social security number");
                    break;
                default:
                    res = ("A problem has occured in the communication to " +
                           "Klarna that handles our invoicing. Please contact " +
                           "Klarna (http://www.klarna.com) for more information " +
                           "or choose another payment method.(Felkod: " + reason + ")");

                    break;
            }

            return res;
        }

        private void sendStat(string function, int time, int selectTime, int Status,
                              int eid, string secret)
        {
            if (SendStats)
            {
                string data = "";
                string checksum = "";
                data += eid + "|";
                checksum += eid + "|";
                data += function + "|";
                checksum += function + "|";
                data += time + "|";
                checksum += time + "|";
                data += selectTime + "|";
                checksum += selectTime + "|";
                data += Status + "|";
                checksum += Status + "|";
                data += Url + "|";
                checksum += Url + "|";
                checksum += secret;
                string digest = mk_digest(checksum);
                data += digest;
                UdpClient udpClient = new UdpClient();
                try
                {
                    udpClient.Connect(ClientstatHost, ClientstatPort);
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(data);
                    udpClient.Send(sendBytes, sendBytes.Length);
                    udpClient.Close();
                }
                catch (Exception) { }
            }
        }

        public void SelectHost()
        {
            if (SelectHosts)
            {
                TimedTcpClient myclient;
                string protocol = "", hostname, portStr = "80", subHost = "";
                string domainHost = "";
                int port = 80;
                if (this.Url.IndexOf("http://") == 0)
                {
                    protocol = "http://";
                }
                else if (this.Url.IndexOf("https://") == 0)
                {
                    protocol = "https://";
                }
                hostname = this.Url.Substring(protocol.Length);
                int colonPos = hostname.IndexOf(":");
                if (colonPos > -1)
                {
                    portStr = hostname.Substring(colonPos + 1);
                    hostname = hostname.Substring(0, colonPos);
                }
                int dotPos = hostname.IndexOf(".");
                if (dotPos > -1)
                {
                    subHost = hostname.Substring(0, dotPos);
                    domainHost = hostname.Substring(dotPos + 1);
                }
                try
                {
                    port = Int32.Parse(portStr);
                }
                catch (Exception)
                {
                    if (protocol.Equals("https://"))
                    {
                        port = 443;
                    }
                }
                for (int i = 1; i <= this.NumHosts; i++)
                {
                    myclient = new TimedTcpClient();
                    try
                    {
                        if (i > 1)
                        {
                            hostname = subHost + i.ToString() + "." + domainHost;
                        }
                        else
                        {
                            hostname = subHost + "." + domainHost;
                        }
                        myclient.Connect(hostname, port);
                        myclient.Close();
                        this.Url = protocol + hostname + ":" + port;
                        break;
                    }
                    catch (Exception)
                    {
                        myclient.Close();
                    }
                }
            }
        }

        private int KredCall(string function, Object[] p, int eid, string secret,
                             string retVar)
        {
            string rpcFunction = function + "_rpc";
            DateTime startDateTime = DateTime.Now;
            this.SelectHost();
            DateTime selectDateTime = DateTime.Now;
            TimeSpan selectDuration = selectDateTime - startDateTime;
            int selectTime = (int)selectDuration.TotalMilliseconds;
            bool allOk = true;
            int sendStatus = 0;
            try
            {
                if (retVar == "Arrarr")
                {
                    Arrarr = (string[,])Invoke(rpcFunction, p);
                }
                else if (retVar == "ValueArr")
                {
                    ValueArr = (string[])Invoke(rpcFunction, p);
                }
                else if (retVar == "ObjArr")
                {
                    ObjArr = (object[])Invoke(rpcFunction, p);
                }
                else if (retVar == "struct")
                    ValueStruct = (XmlRpcStruct)Invoke(rpcFunction, p);
                else if (retVar == "int")
                    ValueInt = (int)Invoke(rpcFunction, p);
                else
                {
                    Value = (string)Invoke(rpcFunction, p);
                }
                Status = OK;
            }
            catch (XmlRpcFaultException fex)
            {
                Status = ERROR;
                sendStatus = FaultCode = fex.FaultCode;
                FaultString = fex.FaultString;
                allOk = false;
            }
            catch (Exception e)
            {
                Status = ERROR;
                sendStatus = FaultCode = -99;
                FaultString = e.ToString();
                allOk = false;
            }
            DateTime stopDateTime = DateTime.Now;
            TimeSpan duration = stopDateTime - startDateTime;
            int time = (int)duration.TotalMilliseconds;
            sendStat(function, time, selectTime, sendStatus, eid, secret);
            if (!allOk)
            {
                throw new KreditorException(FaultCode, FaultString);
            }
            return Status;
        }
    } // class Client


    public class TimedTcpClient : TcpClient
    {
        private int timeout = 4000;
        private Thread timeoutConnect;
        private string host;
        private int port;

        public TimedTcpClient() { }

        public TimedTcpClient(int timeout)
        {
            this.timeout = timeout;
        }

        public int Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        public new void Connect(string host, int port)
        {
            this.host = host;
            this.port = port;
            timeoutConnect = new Thread(new ThreadStart(ThreadConnect));
            timeoutConnect.Start();
            int until = 20;
            int i;
            for (i = 0; i < until; i++)
            {
                Thread.Sleep(((int)timeout / until));
                if (!timeoutConnect.IsAlive)
                {
                    break;
                }
            }
            if (i == until)
            {
                throw (new Exception(this.host));
            }
        }

        private void ThreadConnect()
        {
            try
            {
                base.Connect(this.host, this.port);
                Close();
            }
            catch (Exception)
            {
                Thread.Sleep(timeout * 2);
            }
        }
    } // class TimedTcpClient

} // namespace Kreditor
