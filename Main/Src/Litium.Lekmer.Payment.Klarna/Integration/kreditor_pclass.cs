using System;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core.Web;

namespace Kreditor
{
	public class pclass
	{
		public bool correct_currency(int pclass, int curr)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return (pclassSetting.Currency == curr);
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int get_months(int pclass)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.NoOfMonths;
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int get_month_fee(int pclass)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.MonthlyFee;
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int get_start_fee(int pclass)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.OneTimeFee;
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int get_rate(int pclass)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.Rate;
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int get_type(int pclass)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.Type;
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int get_min_sum(int pclass)
		{
			var klarnaSetting = KlarnaFactory.CreateKlarnaSetting();
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.MinimumTotalCost;
				}
			}

			throw new Exception("Fel pclass");
		}

		public static int[] get_pclass_ids_se()
		{
			return new[] { 100, 101, 102, 103, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123 };
		}
	}
}
