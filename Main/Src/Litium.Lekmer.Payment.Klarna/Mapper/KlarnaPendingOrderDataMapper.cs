using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna.Mapper
{
	public class KlarnaPendingOrderDataMapper : DataMapperBase<IKlarnaPendingOrder>
	{
		public KlarnaPendingOrderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IKlarnaPendingOrder Create()
		{
			var klarnaPendingOrder = IoC.Resolve<IKlarnaPendingOrder>();

			klarnaPendingOrder.Id = MapValue<int>("KlarnaPendingOrder.KlarnaPendingOrderId");
			klarnaPendingOrder.ChannelId = MapValue<int>("KlarnaPendingOrder.ChannelId");
			klarnaPendingOrder.OrderId = MapValue<int>("KlarnaPendingOrder.OrderId");
			klarnaPendingOrder.FirstAttempt = MapValue<DateTime>("KlarnaPendingOrder.FirstAttempt");
			klarnaPendingOrder.LastAttempt = MapValue<DateTime>("KlarnaPendingOrder.LastAttempt");

			return klarnaPendingOrder;
		}
	}
}