﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaAdvancedSetting : KlarnaSetting, IKlarnaAdvancedSetting
	{
		private const string _storageName = "KlarnaAdvanced";

		protected override string StorageName
		{
			get { return _storageName; }
		}

		public KlarnaAdvancedSetting(IKlarnaSettingParser klarnaSettingParser)
			: base(klarnaSettingParser)
		{
		}
	}
}