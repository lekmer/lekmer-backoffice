﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class TimeOutData : ITimeOutData
	{
		public int OrderValue { get; set; }
		public int TimeOutValue { get; set; }
	}
}