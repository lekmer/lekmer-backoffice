﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaSettingParser : IKlarnaSettingParser
	{
		public virtual IEnumerable<ITimeOutData> ParseTimeoutSteps(string rawTimeoutSteps)
		{
			if (string.IsNullOrEmpty(rawTimeoutSteps))
			{
				return new ITimeOutData[0];
			}

			var result = rawTimeoutSteps.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(step =>
				{
					var stepInfo = step.Split(':');

					var timeOutData = IoC.Resolve<ITimeOutData>();

					timeOutData.OrderValue = int.Parse(stepInfo[0]);
					timeOutData.TimeOutValue = int.Parse(stepInfo[1]);

					return timeOutData;
				}).ToArray();

			return result;
		}
	}
}