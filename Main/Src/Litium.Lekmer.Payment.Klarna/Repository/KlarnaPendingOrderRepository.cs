﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna.Repository
{
	public class KlarnaPendingOrderRepository
	{
		protected virtual DataMapperBase<IKlarnaPendingOrder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IKlarnaPendingOrder>(dataReader);
		}

		public virtual int Insert(IKlarnaPendingOrder klarnaPendingOrder)
		{
			var dbSettings = new DatabaseSetting("KlarnaPendingOrderRepository.Insert");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", klarnaPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", klarnaPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", klarnaPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", klarnaPendingOrder.LastAttempt, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pKlarnaPendingOrderInsert]", parameters, dbSettings);
		}

		public virtual void Update(IKlarnaPendingOrder klarnaPendingOrder)
		{
			var dbSettings = new DatabaseSetting("KlarnaPendingOrderRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaPendingOrderId", klarnaPendingOrder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", klarnaPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", klarnaPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", klarnaPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", klarnaPendingOrder.LastAttempt, SqlDbType.DateTime)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pKlarnaPendingOrderUpdate]", parameters, dbSettings);
		}

		public virtual void Delete(int klarnaPendingOrderId)
		{
			var dbSettings = new DatabaseSetting("KlarnaPendingOrderRepository.Delete");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaPendingOrderId", klarnaPendingOrderId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pKlarnaPendingOrderDelete]", parameters, dbSettings);
		}

		public virtual Collection<IKlarnaPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			var dbSettings = new DatabaseSetting("KlarnaPendingOrderRepository.GetPendingOrdersForStatusCheck");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CheckToDate", checkToDate, SqlDbType.DateTime)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pKlarnaPendingOrderGetPendingOrdersForStatusCheck]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
