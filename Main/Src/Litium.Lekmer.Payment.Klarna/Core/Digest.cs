/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Security.Cryptography;

namespace Klarna.Core
{
    public partial class API
    {
        /// <summary>
        /// Builds the digest.
        /// </summary>
        /// <returns>
        /// The hashed SHA-512 digest of the supplied objects.
        /// </returns>
        /// <param name='args'>
        /// Arguments to hash a digest on
        /// </param>
        public string buildDigest (params object[] args)
        {
            string digest = "";
            foreach (var arg in args) {
                // If argument is null or an empty string, move on. Since the arg
                // can be an int, we have to add it to an empty string to reliably
                // check its length.
                if (arg == null || (arg + "").Length == 0) {
                    continue;
                }
                // It it isn't the first element in the digest, add a ':' to separate
                if (digest.Length > 0) {
                    digest += ":";
                }
                // Add the current argument to the digest
                digest += arg;
            }

            return CreateDigest (digest);
        }

        /// <summary>
        /// Creates a digest to secure calls to Klarna.
        /// </summary>
        /// <param name="str">
        /// The string to create the digest from
        /// </param>
        /// <param name="provider">
        /// The CryptoServiceProvider (HashAlgorithm) object, e.g. SHA512CryptoProvider
        /// </param>
        /// <returns>
        /// Digest to be used in calls to Klarna
        /// </returns>
        public string CreateDigest (string str, HashAlgorithm provider)
        {
            byte[] bHash = System.Text.Encoding.GetEncoding (
                "ISO-8859-1"
            ).GetBytes (str);
            byte[] bHashResult;
            bHashResult = provider.ComputeHash (bHash);

            return Convert.ToBase64String (bHashResult);
        }

        /// <summary>
        /// Creates a digest to secure calls to Klarna.
        /// </summary>
        /// <param name="str">
        /// The string to create the digest from
        /// </param>
        /// <returns>
        /// Digest to be used in calls to Klarna
        /// </returns>
        public string CreateDigest (string str)
        {
            return CreateDigest (str, new SHA512Managed ());
        }
    }
}
