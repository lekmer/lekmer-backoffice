﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections.Generic;
using System.Text;

using System.Xml.Serialization;

using Klarna.Core.Structs;
using CookComputing.XmlRpc;

namespace Klarna.Core
{
    /// <summary>
    /// This class is only used with get_addresses
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Member variables
        /// </summary>
        private string company;
        private string fname;
        private string lname;
        private string street;
        private string zip;
        private string city;
        private string country;
        private bool isCompany;

        /// <summary>
        /// Returns the company name
        /// </summary>
        public string Company { get { return this.company; } set { this.company = value; } }

        /// <summary>
        /// Returns the firstname
        /// </summary>
        public string Firstname { get { return this.fname; } set { this.fname = value; } }

        /// <summary>
        /// Returns the lastname
        /// </summary>
        public string Lastname { get { return this.lname; } set { this.lname = value; } }

        /// <summary>
        /// Returns the street
        /// </summary>
        public string Street { get { return this.street; } set { this.street = value; } }

        /// <summary>
        /// Returns the zip code
        /// </summary>
        public string ZipCode { get { return this.zip; } set { this.zip = value; } }

        /// <summary>
        /// Returns the city
        /// </summary>
        public string City { get { return this.city; } set { this.city = value; } }

        /// <summary>
        /// Returns the klarna country code
        /// </summary>
        public string Country { get { return this.country; } set { this.country = value; } }

        /// <summary>
        /// Returns true if it is a company. False otherwise
        /// </summary>
        public bool IsCompany { get { return this.isCompany; } set { this.isCompany = value; } }

        public Address (string company, string fname, string lname, string street, string zip, string city, string country)
        {
            this.company = company;
            this.fname = fname;
            this.lname = lname;
            this.street = street;
            this.zip = zip;
            this.city = city;
            this.country = country;
        }
    }

    /// <summary>
    /// This class is used when sending add_invoice and reserve_amount calls
    /// </summary>
    public class BillingAddress : KlarnaAddress
    {
        public BillingAddress (string email, string telno, string cellno, string fname, string lname, string careof, string street, string zip, string city, API.Country country,
             string houseNumber, string houseExtension, string company)
            : base(email, telno, cellno, fname, lname, careof, street, zip, city, country,
                houseNumber, houseExtension, company)
        {

        }

        public BillingAddress (string email, string telno, string cellno, string fname, string lname, string careof, string street, string zip, string city, API.Country country,
             string houseNumber, string houseExtension)
            : base(email, telno, cellno, fname, lname, careof, street, zip, city, country,
                houseNumber, houseExtension, "")
        {

        }
        public BillingAddress () : base () {}
    }

    /// <summary>
    /// This class is used when sending add_invoice and reserve_amount calls
    /// </summary>
    public class ShippingAddress : KlarnaAddress
    {
        public ShippingAddress (string email, string telno, string cellno, string fname, string lname, string careof, string street, string zip, string city, API.Country country,
             string houseNumber, string houseExtension, string company)
            : base(email, telno, cellno, fname, lname, careof, street, zip, city, country,
                houseNumber, houseExtension, company)
        {

        }

        public ShippingAddress (string email, string telno, string cellno, string fname, string lname, string careof, string street, string zip, string city, API.Country country,
             string houseNumber, string houseExtension)
            : base(email, telno, cellno, fname, lname, careof, street, zip, city, country,
                houseNumber, houseExtension, "")
        {

        }

        public ShippingAddress () : base () {}

    }

    /// <summary>
    /// This class is only used to be extended by shippingaddress/billingaddress
    /// </summary>
    public class KlarnaAddress
    {
        /// <summary>
        /// Member variables
        /// </summary>
        private string email;
        private string telno;
        private string cellno;
        private string fname;
        private string lname;
        private string street;
        private string careof;
        private string zip;
        private string city;
        private API.Country country;
        private string houseNumber;
        private string houseExtension;
        private string company;

        /// <summary>
        /// Public properties to get and set all member variables
        /// </summary>
        [XmlRpcMember("email")]
        public string Email { get { return this.email; } set { this.email = value; } }

        [XmlRpcMember("telno")]
        public string PhoneNumber { get { return this.telno; } set { this.telno = value; } }

        [XmlRpcMember("cellno")]
        public string CellPhoneNumber { get { return this.cellno; } set { this.cellno = value; } }

        [XmlRpcMember("fname")]
        public string Firstname { get { return this.fname; } set { this.fname = value; } }

        [XmlRpcMember("lname")]
        public string Lastname { get { return this.lname; } set { this.lname = value; } }

        [XmlRpcMember("street")]
        public string Street { get { return this.street; } set { this.street = value; } }

        [XmlRpcMember("careof")]
        public string CareOf { get { return this.careof; } set { this.careof = value; } }

        [XmlRpcMember("zip")]
        public string ZipCode { get { return this.zip; } set { this.zip = value; } }

        [XmlRpcMember("city")]
        public string City { get { return this.city; } set { this.city = value; } }

        [XmlRpcMember("country")]
        public API.Country Country { get { return this.country; } set { this.country = value; } }

        [XmlRpcMember("house_number")]
        public string HouseNumber { get { return this.houseNumber; } set { this.houseNumber = value; } }

        [XmlRpcMember("house_extension")]
        public string HouseExtension { get { return this.houseExtension; } set { this.houseExtension = value; } }

        [XmlRpcMember("company")]
        public string CompanyName { get { return this.company; } set { this.company = value; } }

        public KlarnaAddress () {}

        public KlarnaAddress (string email, string telno, string cellno, string fname, string lname, string careof, string street, string zip, string city, API.Country country,
            string houseNumber, string houseExtension, string company)
        {
            this.email = email;
            this.telno = telno;
            this.cellno = cellno;
            this.fname = fname;
            this.lname = lname;
            this.careof = careof;
            this.street = street;
            this.zip = zip;
            this.city = city;
            this.country = country;
            this.houseNumber = houseNumber;
            this.houseExtension = houseExtension;
            this.company = company;
        }
    }
}
