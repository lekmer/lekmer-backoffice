/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using CookComputing.XmlRpc;

namespace Klarna.Core.Structs
{
    public class UpdateInfo : XmlRpcStruct
    {
        public override void Add(object key, object value)
        {
            base.Add(key, value);
        }

        public override object this[object key]
        {
            get
            {
                return base[key];
            }
            set
            {
                base[key] = value;
            }
        }

        public AddressStruct? Billing {
            get {
                return (AddressStruct?)this["bill_addr"];
            }
            internal set {
                Add("bill_addr", value);
            }
        }

        public AddressStruct? Shipping {
            get {
                return (AddressStruct?)this["dlv_addr"];
            }
            internal set {
                Add("dlv_addr", value);
            }
        }

        public string OrderId1 {
            get {
                return this["orderid1"].ToString();
            }
            internal set {
                Add("orderid1", value);
            }
        }

        public string OrderId2 {
            get {
                return this["orderid2"].ToString();
            }
            internal set {
                Add("orderid2", value);
            }
        }

        public Array GoodsList {
            get {
                return (Array)this["goods_list"];
            }
            internal set {
                Add("goods_list", value);
            }
        }
    }
}
