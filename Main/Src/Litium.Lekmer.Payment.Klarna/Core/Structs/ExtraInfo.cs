/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using CookComputing.XmlRpc;

namespace Klarna.Core.Structs
{
    /// <summary>
    /// class that holds all extra information. Doesnt use struct as
    /// we need to default some values
    /// </summary>
    public class ExtraInfo : XmlRpcStruct
    {
        public override object this[object key]
        {
            get
            {
                return base[key];
            }
            set
            {
                base[key] = value;
            }
        }

        public override void Add(object key, object value)
        {
            base.Add(key, value);
        }
        /// <summary>
        /// Customer number. Use EStoreUser to be able to search by
        /// customer ID in Klarna Online
        /// </summary>
        public string CustomerNumber {
            get {
                return this["cust_no"].ToString();
            }
            set {
                this.Add("cust_no", value);
            }
        }

        /// <summary>
        /// Used to be able to search on the customer ID in Klarna Online.
        /// </summary>
        public string EStoreUser {
            get {
                return this["estore_user"].ToString();
            }
            set {
                this.Add("estore_user", value);
            }
        }

        /// <summary>
        /// Customers maiden name
        /// </summary>
        public string MaidenName {
            get {
                return this["maiden_name"].ToString();
            }
            set {
                this.Add("maiden_name", value);
            }
        }

        /// <summary>
        /// Customers place of birth
        /// </summary>
        public string PlaceOfBirth {
            get {
                return this["place_of_birth"].ToString();
            }
            set {
                this.Add("place_of_birth", value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public string Password {
            get {
                return this["password"].ToString();
            }
            set {
                this.Add("password", value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public string NewPassword {
            get {
                return this["new_password"].ToString();
            }
            set {
                this.Add("new_password", value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public string Captcha {
            get {
                return this["captcha"].ToString();
            }
            set {
                this.Add("captcha", value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public int PowerOfAttorneyGroup {
            get {
                return (int) this["poa_group"];
            }
            set {
                this.Add("poa_group", value);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public string PowerOfAttorneyPNO {
            get {
                return this["poa_pno"].ToString();
            }
            set {
                this.Add("poa_pno", value);
            }
        }

        /// <summary>
        /// The date when the goods will be ready to ship.
        /// Is used together with auto activation
        /// </summary>
        public string ReadyDate {
            get {
                return this["ready_date"].ToString();
            }
            set {
                this.Add("ready_date", value);
            }
        }

        /// <summary>
        /// A random string used to identify the call from Klarna when
        /// using Pre Paid invoicing
        /// </summary>
        public string RandomString {
            get {
                return this["rand_string"].ToString();
            }
            set {
                this.Add("rand_string", value);
            }
        }

        /// <summary>
        /// Behaviour class. Works like customer groups
        /// </summary>
        public int BehaviourClass {
            get {
                return (int) this["bclass"];
            }
            set {
                this.Add("bclass", value);
            }
        }

        /// <summary>
        /// The pin code sent to the customer when using Klarna Mobile
        /// </summary>
        public string PhonePIN {
            get {
                return this["pin"].ToString();
            }
            set {
                this.Add("pin", value);
            }
        }
    }
}
