﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Reflection;

using Klarna.Core;
using Klarna.Core.Structs;

namespace Klarna.Core.PClasses
{
    class XMLStorage : PCStorage
    {

        /// <summary>
        /// The internal XML document
        /// </summary>
        protected XmlDocument dom;


        /// <summary>
        /// XML version for the DOM document
        /// </summary>
        protected string version = "1.0";

        /// <summary>
        /// Encoding for the DOM document
        /// </summary>
        protected string encoding = "ISO-8859-1";

        /// <summary>
        /// Class constructor
        /// </summary>
        public XMLStorage()
        {
            this.dom = new XmlDocument();
            this.dom.PreserveWhitespace = true;
        }

        /// <summary>
        /// Checks if the file is writeable, readable or if the directory is
        /// </summary>
        protected void CheckURI(string uri)
        {
            try
            {
                if (File.Exists(uri))
                {
                    File.Delete(uri);
                }
            }
            catch (Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new Exception("Error (" + method + ")" + e.Message);
            }
        }

        /// <summary>
        /// See PCStorage.Load()
        /// </summary>
        /// <param name="uri"></param>
        public override void Load(string uri)
        {
            try
            {
                if(!File.Exists(uri))
                {
                    //Do not fail, if file doesn't exist.
                    return;
                }
                try
                {
                    this.dom.Load(uri);
                }
                catch(Exception e)
                {
                    throw new Exception("Failed to parse: " + uri);
                }
                MemoryStream memStream = new MemoryStream();
                this.dom.Save(memStream);
                memStream.Position = 0;
                XPathDocument xpath = new XPathDocument(memStream);
                XPathNavigator nav = xpath.CreateNavigator();
                foreach(XPathNavigator estoreNode in nav.Select("klarna/estore"))
                {
                    int eid = Convert.ToInt32(estoreNode.GetAttribute("id", ""));
                    foreach(XPathNavigator pclassNode in estoreNode.Select("pclass"))
                    {
                        int pid = Convert.ToInt32(pclassNode.GetAttribute("pid", ""));
                        XPathNavigator descNode = pclassNode.SelectSingleNode("description");
                        XPathNavigator monthsNode = pclassNode.SelectSingleNode("months");
                        XPathNavigator startfeeNode = pclassNode.SelectSingleNode("startfee");
                        XPathNavigator invoicefeeNode = pclassNode.SelectSingleNode("invoicefee");
                        XPathNavigator interestrateNode = pclassNode.SelectSingleNode("interestrate");
                        XPathNavigator minamountNode = pclassNode.SelectSingleNode("minamount");
                        PClass.PClassType type = (PClass.PClassType)Convert.ToInt32(pclassNode.GetAttribute("type", ""));
                        XPathNavigator countryNode = pclassNode.SelectSingleNode("country");
                        XPathNavigator expireNode = pclassNode.SelectSingleNode("expire");

                        String desc = descNode.Value;
                        int months = XmlConvert.ToInt32(monthsNode.Value);
                        double startfee = XmlConvert.ToDouble(startfeeNode.Value);
                        double invoicefee = XmlConvert.ToDouble(invoicefeeNode.Value);
                        double interestrate = XmlConvert.ToDouble(interestrateNode.Value);
                        double minamount = XmlConvert.ToDouble(minamountNode.Value);
                        API.Country country = (API.Country)XmlConvert.ToInt32(countryNode.Value);
                        DateTime expire = XmlConvert.ToDateTime(expireNode.Value, XmlDateTimeSerializationMode.Local);

                        PClass pclass = new PClass(
                            eid,
                            pid,
                            desc,
                            months,
                            startfee,
                            invoicefee,
                            interestrate,
                            minamount,
                            country,
                            type,
                            expire
                            );

                        this.AddPClass(pclass);
                    }

                }

            }
            catch (Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new KlarnaException("Error in (" + method + ") " + e.Message);
            }
        }

        protected List<XmlElement> createFields(PClass pclass)
        {
            List<XmlElement> list = new List<XmlElement>();
            XmlElement description = this.dom.CreateElement("description");
            XmlText descriptionText = this.dom.CreateTextNode(pclass.Description);
            description.AppendChild(descriptionText);
            list.Add(description);
            XmlElement months = this.dom.CreateElement("months");
            months.InnerText = XmlConvert.ToString(pclass.Months);
            list.Add(months);
            XmlElement startfee = this.dom.CreateElement("startfee");
            startfee.InnerText = XmlConvert.ToString(pclass.StartFee);
            list.Add(startfee);
            XmlElement invoicefee = this.dom.CreateElement("invoicefee");
            invoicefee.InnerText = XmlConvert.ToString(pclass.InvoiceFee);
            list.Add(invoicefee);
            XmlElement interestrate = this.dom.CreateElement("interestrate");
            interestrate.InnerText = XmlConvert.ToString(pclass.InterestRate);
            list.Add(interestrate);
            XmlElement minamount = this.dom.CreateElement("minamount");
            minamount.InnerText = XmlConvert.ToString(pclass.MinAmountForPClass);
            list.Add(minamount);
            XmlElement country = this.dom.CreateElement("country");
            country.InnerText = XmlConvert.ToString(Convert.ToInt32(pclass.Country));
            list.Add(country);
            XmlElement expire = this.dom.CreateElement("expire");
            expire.InnerText = XmlConvert.ToString(pclass.ExpireDate, XmlDateTimeSerializationMode.Local);
            list.Add(expire);
            return list;
        }

        /// <summary>
        /// See PCStorage.Save()
        /// </summary>
        /// <param name="uri"></param>
        public override void Save(string uri)
        {
            try
            {
                this.CheckURI(uri);

                //Reset the DOMDocument
                try
                {
                    this.dom = new XmlDocument();
                    XmlDeclaration xmlDeclaration = this.dom.CreateXmlDeclaration(this.version, this.encoding, null);
                    this.dom.InsertBefore(xmlDeclaration, this.dom.DocumentElement);
                    XmlElement klarnaElement = this.dom.CreateElement("klarna");
                    this.dom.AppendChild(klarnaElement);
                }
                catch (Exception e)
                {
                    throw new Exception("Failed to load the inital XML.");
                }

                foreach(KeyValuePair<int, List<PClass>> estore in this.pclasses)
                {
                    XmlElement estoreElement = this.dom.CreateElement("estore");
                    int eid = estore.Key;
                    XmlAttribute id = this.dom.CreateAttribute("id");
                    id.Value = Convert.ToString(eid);
                    estoreElement.Attributes.Append(id);

                    foreach (PClass pclass in estore.Value)
                    {
                        if(eid != pclass.EID)
                        {
                            //This should never occur
                            continue;
                        }

                        XmlElement pclassElement = this.dom.CreateElement("pclass");

                        foreach (XmlElement element in this.createFields(pclass))
                        {
                            pclassElement.AppendChild(element);
                        }

                        XmlAttribute pid, type;
                        pid = this.dom.CreateAttribute("pid");
                        type = this.dom.CreateAttribute("type");

                        pid.Value = XmlConvert.ToString((Convert.ToInt32(pclass.PClassID)));
                        type.Value = XmlConvert.ToString(Convert.ToInt32(pclass.Type));

                        pclassElement.Attributes.Append(pid);
                        pclassElement.Attributes.Append(type);

                        estoreElement.AppendChild(pclassElement);
                    }
                    this.dom.DocumentElement.AppendChild(estoreElement);
                }

                try
                {
                    this.dom.Save(uri);
                }
                catch(Exception e)
                {
                    throw new Exception("Failed to save the XML Document");
                }

            }
            catch (Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new KlarnaException("Error in (" + method + ") " + e.Message + " !");
            }
        }

        /// <summary>
        /// See PCStorage.Clear()
        /// </summary>
        /// <param name="uri"></param>
        public override void Clear(string uri)
        {
            try
            {
                this.CheckURI(uri);
                this.pclasses = null;
            }
            catch(Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new KlarnaException("Error in (" + method + ") " + e.Message);
            }
        }
    }
}
