﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Data;
using System.Text;
using Klarna.Core;
using Klarna.Core.Structs;


namespace Klarna.Core.PClasses
{
    abstract class PCStorage
    {
        /// <summary>
        /// A Dictionary of PClasses with EID as key
        /// </summary>
        protected Dictionary<int, List<PClass>> pclasses = new Dictionary<int,List<PClass>>();

        /// <summary>
        /// Adds a PClass to the storage
        /// </summary>
        /// <param name="pclass">PClass object</param>
        public void AddPClass (PClass pclass)
        {
            try
            {

                if (this.pclasses == null)
                {
                    this.pclasses = new Dictionary<int, List<PClass>> ();
                }

                if (pclass.Description == null)
                {
                    //Something went wrong, do not save these!
                    return;
                }

                if (!this.pclasses.ContainsKey (pclass.EID))
                {
                    List<PClass> pclassList = new List<PClass> ();
                    this.pclasses[pclass.EID] = pclassList;
                }

                foreach (PClass p in (List<PClass>)this.pclasses[pclass.EID])
                {
                    if (p.PClassID.Equals (pclass.PClassID))
                    {
                        //Replace pclass
                        this.pclasses[pclass.EID][this.pclasses[pclass.EID].IndexOf (p)] = pclass;

                        return;
                    }
                }

                this.pclasses[pclass.EID].Add(pclass);
            }
            catch (Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new KlarnaException("Error in ( " + method + ") " + e.Message);
            }

        }

        /// <summary>
        ///  Gets the PClass by ID
        /// </summary>
        /// <param name="id">PClass ID</param>
        /// <param name="eid">Estore ID</param>
        /// <returns>PClass object</returns>
        public PClass getPClass (int id, int eid)
        {
            if (this.pclasses == null)
            {
                throw new KlarnaException ("No match for that eid!");
            }
            if (this.pclasses[eid] == null)
            {
                throw new KlarnaException ("No match for that eid!");
            }

            foreach (PClass p in (List<PClass>)this.pclasses[eid])
            {
                if (p.PClassID.Equals (id))
                {
                    return p;
                }
            }

            throw new KlarnaException("No such pclass available");
        }

        /// <summary>
        /// Returns a Dictionary of PClasses, keyed with estore id.
        /// If type is specified, only that type will be returned
        /// </summary>
        /// <param name="country">API.Country</param>
        /// <param name="type">PClass.PClassType</param>
        /// <returns>Dictionary with PClasses</returns>
        public Dictionary<int, List<PClass>> getPClasses(API.Country country, PClass.PClassType type)
        {
            Dictionary<int, List<PClass>> tmp = new Dictionary<int, List<PClass>>();
            /* Lekmer: Fix bug with getting pclasses for specifiec eid
            //List<PClass> tmpList = new List<PClass>();
            */
            if (this.pclasses != null)
            {
                foreach (List<PClass> list in this.pclasses.Values)
                {
                    /* Lekmer: Fix bug with getting pclasses for specifiec eid */
                    List<PClass> tmpList = new List<PClass>();
                    /* --- */
                    foreach (PClass p in list)
                    {
                        if(type == PClass.PClassType.NULL)
                        {
                            if (p.Country.Equals(country))
                            {
                                tmpList.Add(p);
                            }
                        }
                        else if(p.Country.Equals(country) && p.Type.Equals(type))
                        {
                            tmpList.Add(p);
                        }
                        tmp[p.EID] = tmpList;
                    }
                }
            }
            return tmp;
        }

        /// <summary>
        /// Loads the PClasses and calls AddPClass() to store them in runtime.
        /// </summary>
        /// <param name="uri">URI to file for stored PClasses</param>
        abstract public void Load(string uri);

        /// <summary>
        /// Takes the internal PClass list and stores it.
        /// </summary>
        /// <param name="uri">URI to file for stored PClasses</param>
        abstract public void Save(string uri);

        /// <summary>
        /// Removes the internally stored pclasses
        /// </summary>
        /// <param name="uri">URI to file for stored PClasses</param>
        abstract public void Clear(string uri);
    }
}
