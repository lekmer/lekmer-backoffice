﻿using System;
using System.Collections.Generic;
using System.Reflection;
using CookComputing.XmlRpc;
using Klarna.Core.PClasses;
using Litium.Lekmer.Payment.Klarna.Setting;

namespace Klarna.Core
{
	public partial class API
	{
		// Use lazy loading
		private IAPI _api;
		private IAPI api
		{
			get
			{
				return _api ?? (_api = XmlRpcProxyGen.Create<IAPI>());
			}
		}

		public virtual KlarnaServer Mode
		{
			get { return mode; }
			set { mode = value; }
		}

		public API(string channelCommonName, IKlarnaSetting klarnaSetting, int timeout)
		{
			xmlrpcClient = new XmlRpcClientProtocol
				{
					Url = klarnaSetting.Url,
					Timeout = timeout
				};

			SetConfig(channelCommonName, klarnaSetting);
		}

		public API(XmlRpcClientProtocol xmlrpc, string channelCommonName, IKlarnaSetting klarnaSetting, int timeout)
		{
			xmlrpcClient = xmlrpc;
			xmlrpcClient.Url = klarnaSetting.Url;
			xmlrpcClient.Timeout = timeout;

			SetConfig(channelCommonName, klarnaSetting);
		}

		public double GetCheapestMonthlyFee()
		{
			double lowestFee = 0.0;

			List<PClass> pClasses = GetPClasses(PClass.PClassType.NULL);

			foreach (PClass p in pClasses)
			{
				lowestFee = Math.Round(p.InvoiceFee, 2);
				break;
			}

			foreach (PClass p in pClasses)
			{
				double monthlyFee = Math.Round(p.InvoiceFee, 2);
				if (monthlyFee < lowestFee)
				{
					lowestFee = monthlyFee;
				}
			}

			return lowestFee;
		}

		private void SetConfig(string channelCommonName, IKlarnaSetting klarnaSetting)
		{
			// Channel specific variables.
			Currency channelCurrency;
			Country channelCountry;
			Language channelLanguage;
			Encoding channelEncoding;

			SetChannelSpecificVariables(channelCommonName, out channelCurrency, out channelCountry, out channelLanguage, out channelEncoding);

			var klarnaConfig = new KlarnaConfig
				{
					EID = klarnaSetting.EID,
					Secret = klarnaSetting.SecretKey,
					Country = channelCountry,
					Language = channelLanguage,
					Currency = channelCurrency,
					Encoding = channelEncoding,
					Mode = KlarnaServer.Live,
					PCStorage = klarnaSetting.PCStorage,
					PCURI = klarnaSetting.PCURI
				};

			Config(klarnaConfig);
		}

		private void SetChannelSpecificVariables(string channelCommonName, out Currency channelCurrency, out Country channelCountry, out Language channelLanguage, out Encoding channelEncoding)
		{
			switch (channelCommonName)
			{
				case "Netherlands":
					channelCurrency = Currency.EUR;
					channelCountry = Country.Netherlands;
					channelLanguage = Language.Dutch;
					channelEncoding = Encoding.Dutch;
					break;
				case "Denmark":
					channelCurrency = Currency.DKK;
					channelCountry = Country.Denmark;
					channelLanguage = Language.Danish;
					channelEncoding = Encoding.Danish;
					break;
				case "Finland":
					channelCurrency = Currency.EUR;
					channelCountry = Country.Finland;
					channelLanguage = Language.Finnish;
					channelEncoding = Encoding.Finnish;
					break;
				case "Norway":
					channelCurrency = Currency.NOK;
					channelCountry = Country.Norway;
					channelLanguage = Language.Norwegian;
					channelEncoding = Encoding.Norwegian;
					break;
				case "Sweden":
					channelCurrency = Currency.SEK;
					channelCountry = Country.Sweden;
					channelLanguage = Language.Swedish;
					channelEncoding = Encoding.Swedish;
					break;
				default:
					throw new ArgumentException(string.Format("Channel with common name: \"{0}\" not supported.", channelCommonName));
			}
		}

		/// <summary>
		/// Returns a list of pclasses mathcing the country and pclass type.
		/// If PClass.PClassType.NULL sent, returns all pclasses
		/// Now with caching!
		/// </summary>
		/// <param name="ptype">PClass.PClassType</param>
		/// <returns>List of PClasses</returns>
		private List<PClass> pclassList(PClass.PClassType ptype)
		{
			var cacheKey = new PClassCacheKey(this.country, ptype, this.eid);
			List<PClass> pClasses = PClassCache.Instance.GetData(cacheKey);
			if (pClasses != null)
			{
				return pClasses;
			}

			if (this.xmlstorage == null)
			{
				//Attempts to load previously downladed pclasses
				this.xmlstorage = new XMLStorage();
			}
			this.xmlstorage.Load(this.pcuri);
			try
			{
				Dictionary<int, List<PClass>> dictionary = new Dictionary<int, List<PClass>>();
				dictionary = this.xmlstorage.getPClasses(this.country, ptype);
				pClasses = dictionary[this.eid];

				PClassCache.Instance.Add(cacheKey, pClasses);

				return pClasses;
			}
			catch (Exception e)
			{
				string method = MethodInfo.GetCurrentMethod().Name;
				throw new KlarnaException("Error in (" + method + ") " + e.Message);
			}
		}
	}
}