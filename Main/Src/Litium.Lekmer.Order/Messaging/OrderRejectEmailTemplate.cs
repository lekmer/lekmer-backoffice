﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class OrderRejectEmailTemplate : Template
	{
		public OrderRejectEmailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public OrderRejectEmailTemplate(IChannel channel)
			: base("OrderRejectEmail", channel, false)
		{
		}
	}
}