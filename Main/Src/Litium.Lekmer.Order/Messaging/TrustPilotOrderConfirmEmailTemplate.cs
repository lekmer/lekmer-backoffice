﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class TrustPilotOrderConfirmEmailTemplate : Template
	{
		public TrustPilotOrderConfirmEmailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public TrustPilotOrderConfirmEmailTemplate(IChannel channel)
			: base("TrustPilotOrderConfirmEmail", channel, false)
		{
		}
	}
}