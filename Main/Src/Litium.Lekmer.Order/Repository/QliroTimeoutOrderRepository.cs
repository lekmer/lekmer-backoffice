﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public class QliroTimeoutOrderRepository
	{
		protected virtual DataMapperBase<IQliroTimeoutOrder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IQliroTimeoutOrder>(dataReader);
		}

		public virtual Collection<IQliroTimeoutOrder> GetQliroTimeoutOrders()
		{
			var dbSettings = new DatabaseSetting("QliroTimeoutOrderRepository.GetQliroTimeoutOrders");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pQliroTimeoutOrderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Update(int transactionId)
		{
			var dbSettings = new DatabaseSetting("QliroTimeoutOrderRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", transactionId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pQliroTimeoutOrderUpdate]", parameters, dbSettings);
		}
	}
}