﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
    public class TradeDoublerProductGroupRepository
    {
        protected virtual DataMapperBase<ITradeDoubleProductGroup> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<ITradeDoubleProductGroup>(dataReader);
        }

        public ITradeDoubleProductGroup GetProductGroupMapping(int channelId, int productId)
        {
            var dbSettings = new DatabaseSetting("TradeDoublerProductGroupRepository.GetProductGroupMapping");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
            using (
                IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pGetProductGroupMapping]", parameters,
                                                                         dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }
    }
}
