﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderAddressRepository : OrderAddressRepository
	{
		public virtual void SaveLekmerPart(ILekmerOrderAddress address)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("OrderAddressId", address.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("HouseNumber", address.HouseNumber, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("HouseExtension", address.HouseExtension, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("Reference", address.Reference, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("DoorCode", address.DoorCode, SqlDbType.NVarChar)
			};

			var dbSettings = new DatabaseSetting("LekmerOrderAddressRepository.Save");

			new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pOrderAddressSave]", parameters, dbSettings);
		}

		public virtual void Delete(int orderAddressId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderAddressId", orderAddressId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerOrderAddressRepository.Delete");
			new DataHandler().ExecuteCommand("[orderlek].[pOrderAddressDelete]", parameters, dbSettings);
		}
	}
}