﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository.Campaign;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderProductCampaignRepository : OrderProductCampaignRepository
	{
		public override void Save(int orderItemId, IOrderCampaign orderProductCampaign)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", orderProductCampaign.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CampaignId", orderProductCampaign.CampaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderItemId", orderItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", orderProductCampaign.Title, SqlDbType.NVarChar, 500)
				};
			var dbSettings = new DatabaseSetting("OrderProductCampaignRepository.Save");
			orderProductCampaign.Id = new DataHandler().ExecuteCommandWithReturnValue("[order].[pOrderProductCampaignSave]", parameters, dbSettings);
		}
	}
}