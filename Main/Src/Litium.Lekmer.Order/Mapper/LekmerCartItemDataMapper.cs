using System.Data;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerCartItemDataMapper : CartItemDataMapper
	{
		public LekmerCartItemDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICartItem Create()
		{
			var cartitem = (ILekmerCartItem)base.Create();
			cartitem.SizeId = MapNullableValue<int?>("LekmerCartItem.SizeId");
			cartitem.IsAffectedByCampaign = MapValue<bool>("LekmerCartItem.IsAffectedByCampaign");
			cartitem.IPAddress = MapNullableValue<string>("LekmerCartItem.IPAddress");
			return cartitem;
		}
	}
}