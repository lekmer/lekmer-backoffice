using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class OrderVoucherInfoDataMapper : DataMapperBase<IOrderVoucherInfo>
	{
		public OrderVoucherInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IOrderVoucherInfo Create()
		{
			var voucherInfo = IoC.Resolve<IOrderVoucherInfo>();

			voucherInfo.VoucherCode = MapNullableValue<string>("OrderVoucherInfo.VoucherCode");
			voucherInfo.DiscountType = MapNullableValue<int?>("OrderVoucherInfo.DiscountType");
			voucherInfo.DiscountValue = MapNullableValue<decimal?>("OrderVoucherInfo.DiscountValue");
			voucherInfo.AmountUsed = MapNullableValue<decimal?>("OrderVoucherInfo.AmountUsed");
			voucherInfo.AmountLeft = MapNullableValue<decimal?>("OrderVoucherInfo.AmountLeft");
			voucherInfo.VoucherStatus = MapValue<int>("OrderVoucherInfo.VoucherStatus");

			voucherInfo.SetUntouched();

			return voucherInfo;
		}
	}
}