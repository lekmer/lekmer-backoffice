using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class CartItemPackageElementDataMapper : DataMapperBase<ICartItemPackageElement>
	{
		public CartItemPackageElementDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICartItemPackageElement Create()
		{
			var cartItemPackageElement = IoC.Resolve<ICartItemPackageElement>();

			cartItemPackageElement.Id = MapValue<int>("CartItemPackageElement.CartItemPackageElementId");
			cartItemPackageElement.CartItemId = MapValue<int>("CartItemPackageElement.CartItemId");
			cartItemPackageElement.ProductId = MapValue<int>("CartItemPackageElement.ProductId");
			cartItemPackageElement.SizeId = MapNullableValue<int?>("CartItemPackageElement.SizeId");

			cartItemPackageElement.Status = BusinessObjectStatus.Untouched;

			return cartItemPackageElement;
		}
	}
}