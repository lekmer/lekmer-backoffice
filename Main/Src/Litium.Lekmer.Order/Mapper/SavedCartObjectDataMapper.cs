using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class SavedCartObjectDataMapper : DataMapperBase<ISavedCartObject>
	{
		public SavedCartObjectDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISavedCartObject Create()
		{
			var savedCart = IoC.Resolve<ISavedCartObject>();

			savedCart.Id = MapValue<int>("SavedCartId");
			savedCart.CartId = MapValue<int>("CartId");
			savedCart.CartGuid = MapValue<Guid>("CartGuid");
			savedCart.ChannelId = MapValue<int>("ChannelId");
			savedCart.Email = MapValue<string>("Email");
			savedCart.IsReminderSent = MapValue<bool>("IsReminderSent");
			savedCart.IsNeedReminder = MapValue<bool>("IsNeedReminder");
			savedCart.Status = BusinessObjectStatus.Untouched;

			return savedCart;
		}
	}
}