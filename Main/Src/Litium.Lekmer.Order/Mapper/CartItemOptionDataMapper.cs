using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order.Mapper
{
	public class CartItemOptionDataMapper : DataMapperBase<ICartItemOption>
	{
		private DataMapperBase<IProduct> _productDataMapper;

		public CartItemOptionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_productDataMapper = DataMapperResolver.Resolve<IProduct>(DataReader);

		}

		protected override ICartItemOption Create()
		{
			var cartItemOption = IoC.Resolve<ICartItemOption>();

			cartItemOption.Id = MapValue<int>("CartItemOption.CartItemOptionId");
			cartItemOption.CartId = MapValue<int>("CartItemOption.CartId");
			cartItemOption.Product = _productDataMapper.MapRow();
			cartItemOption.SizeId = MapNullableValue<int?>("CartItemOption.SizeId");
			cartItemOption.Quantity = MapValue<int>("CartItemOption.Quantity");
			cartItemOption.CreatedDate = MapValue<DateTime>("CartItemOption.CreatedDate");
			cartItemOption.Status = BusinessObjectStatus.Untouched;

			return cartItemOption;
		}
	}
}