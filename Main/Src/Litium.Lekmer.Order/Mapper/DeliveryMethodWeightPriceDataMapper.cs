﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class DeliveryMethodWeightPriceDataMapper : DataMapperBase<IDeliveryMethodWeightPrice>
	{
		public DeliveryMethodWeightPriceDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IDeliveryMethodWeightPrice Create()
		{
			var deliveryMethodWeightPrice = IoC.Resolve<IDeliveryMethodWeightPrice>();

			deliveryMethodWeightPrice.WeightFrom = MapNullableValue<decimal?>("WeightFrom");
			deliveryMethodWeightPrice.WeightTo = MapNullableValue<decimal?>("WeightTo");
			deliveryMethodWeightPrice.FreightCost = MapValue<decimal>("FreightCost");

			return deliveryMethodWeightPrice;
		}
	}
}