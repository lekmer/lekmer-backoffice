﻿using System;
using System.Data;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerCartDataMapper : CartDataMapper
	{
		public LekmerCartDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICartFull Create()
		{
			var cart = (ILekmerCartFull) base.Create();
			cart.UpdatedDate = MapNullableValue<DateTime?>("Cart.UpdatedDate");
			cart.IPAddress = MapNullableValue<string>("Cart.IPAddress");
			return cart;
		}
	}
}