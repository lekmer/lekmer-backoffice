using System.Data;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerOrderPaymentDataMapper : OrderPaymentDataMapper
	{
		private int _captured;
		private int _klarnaEid;
		private int _klarnaPClass;
		private int _maksuturvaCode;
		private int _qliroClientRef;
		private int _qliroPaymentCode;
		private int _collectorStoreId;
		private int _collectorPaymentCode;

		public LekmerOrderPaymentDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_captured = OrdinalOf("OrderPayment.Captured");
			_klarnaEid = OrdinalOf("OrderPayment.KlarnaEID");
			_klarnaPClass = OrdinalOf("OrderPayment.KlarnaPClass");
			_maksuturvaCode = OrdinalOf("OrderPayment.MaksuturvaCode");
			_qliroClientRef = OrdinalOf("OrderPayment.QliroClientRef");
			_qliroPaymentCode = OrdinalOf("OrderPayment.QliroPaymentCode");
			_collectorStoreId = OrdinalOf("OrderPayment.CollectorStoreId");
			_collectorPaymentCode = OrdinalOf("OrderPayment.CollectorPaymentCode");
		}

		protected override IOrderPayment Create()
		{
			var orderPayment = base.Create() as ILekmerOrderPayment;

			if (orderPayment != null)
			{
				orderPayment.Captured = MapNullableValue<bool?>(_captured);
				orderPayment.KlarnaEid = MapNullableValue<int?>(_klarnaEid);
				orderPayment.KlarnaPClass = MapNullableValue<int?>(_klarnaPClass);
				orderPayment.MaksuturvaCode = MapNullableValue<string>(_maksuturvaCode);
				orderPayment.QliroClientRef = MapNullableValue<string>(_qliroClientRef);
				orderPayment.QliroPaymentCode = MapNullableValue<string>(_qliroPaymentCode);
				orderPayment.CollectorStoreId = MapNullableValue<int>(_collectorStoreId);
				orderPayment.CollectorPaymentCode = MapNullableValue<string>(_collectorPaymentCode);

				orderPayment.Status = BusinessObjectStatus.Untouched;
			}

			return orderPayment;
		}
	}
}