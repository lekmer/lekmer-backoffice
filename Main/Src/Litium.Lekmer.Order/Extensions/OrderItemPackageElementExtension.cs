﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Order
{
	public static class OrderItemPackageElementExtension
	{
		public static int? GetPackageElementsHashCode(this Collection<IPackageOrderItem> packageElements)
		{
			if (packageElements == null || packageElements.Count == 0) return null;

			var elements = new List<IPackageOrderItem>(packageElements);
			elements.Sort(delegate(IPackageOrderItem element1, IPackageOrderItem element2)
			{
				if (element1.ProductId < element2.ProductId)
					return -1;
				if (element1.ProductId > element2.ProductId)
					return 1;
				return 0;
			});

			string elementsInfo = string.Empty;
			foreach (var element in elements)
			{
				elementsInfo += element.ProductId + element.SizeId + element.ErpId;
			}

			var hashCode = elementsInfo.GetHashCode();
			if (hashCode < 0)
			{
				hashCode = hashCode*(-1);
			}

			return hashCode;
		}
	}
}