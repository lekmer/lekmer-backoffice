using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	public class CartItemOptionService : ICartItemOptionService
	{
		protected CartItemOptionRepository Repository { get; private set; }

		public CartItemOptionService(CartItemOptionRepository cartItemOptionRepository)
		{
			Repository = cartItemOptionRepository;
		}

		public virtual ICartItemOption Create()
		{
			var cartItemOption = IoC.Resolve<ICartItemOption>();

			var product = IoC.Resolve<IProduct>();
			cartItemOption.Product = product;

			var priceListItem = IoC.Resolve<IPriceListItem>();
			cartItemOption.Product.Price = priceListItem;

			cartItemOption.CreatedDate = DateTime.Now;
			cartItemOption.Status = BusinessObjectStatus.New;

			return cartItemOption;
		}

		public virtual void Save(ICartItemOption cartItemOption)
		{
			if (cartItemOption == null)
			{
				throw new ArgumentNullException("cartItemOption");
			}

			Repository.Save(cartItemOption);
		}

		public virtual void Delete(int cartItemOptionId)
		{
			Repository.Delete(cartItemOptionId);
		}

		public virtual Collection<ICartItemOption> GetAllByCart(IUserContext context, int cartId)
		{
			Collection<ICartItemOption> items = Repository.GetAllByCart(cartId, context.Customer, context.Channel.Id);
			return items ?? new Collection<ICartItemOption>();
		}
	}
}