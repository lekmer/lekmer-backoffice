using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public class DeliveryMethodWeightPriceService : IDeliveryMethodWeightPriceService
	{
		protected DeliveryMethodWeightPriceRepository Repository { get; private set; }

		public DeliveryMethodWeightPriceService(DeliveryMethodWeightPriceRepository deliveryMethodWeightPriceRepository)
		{
			Repository = deliveryMethodWeightPriceRepository;
		}

		public Collection<IDeliveryMethodWeightPrice> GetAllByDeliveryMethod(IUserContext context, int deliveryMethodId)
		{
			var items = Repository.GetAllByDeliveryMethod(deliveryMethodId, context.Channel.Id);
			return items ?? new Collection<IDeliveryMethodWeightPrice>();
		}
	}
}