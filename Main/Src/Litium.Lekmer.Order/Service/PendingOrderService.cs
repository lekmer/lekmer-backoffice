﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Lekmer.Payment.Maksuturva;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using Litium.Lekmer.Payment.Qliro;
using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Order
{
	public class PendingOrderService : IPendingOrderService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Collection<IChannel> _channels;
		private DateTime _checkDate;
		private IUserContext _userContext;

		protected ILekmerChannelService ChannelService { get; private set; }
		protected ILekmerOrderService OrderService { get; private set; }
		protected ILekmerOrderSecureService OrderSecureService { get; private set; }
		protected IOrderStatusService OrderStatusService { get; private set; }
		protected IOrderItemStatusService OrderItemStatusService { get; private set; }
		protected IGiftCardViaMailInfoService GiftCardViaMailInfoService { get; private set; }
		protected IKlarnaPendingOrderService KlarnaPendingOrderService { get; private set; }
		protected IKlarnaPendingIntervalSetting KlarnaPendingIntervalSetting { get; private set; }
		protected IMaksuturvaPendingOrderService MaksuturvaPendingOrderService { get; private set; }
		protected IMaksuturvaPaymentProvider MaksuturvaPaymentProvider { get; private set; }
		protected IMaksuturvaSetting MaksuturvaSetting { get; private set; }
		protected IQliroPendingOrderService QliroPendingOrderService { get; private set; }
		protected IQliroPendingIntervalSetting QliroPendingIntervalSetting { get; private set; }

		public PendingOrderService(
			IChannelService channelService,
			IOrderService orderService,
			IOrderSecureService orderSecureService,
			IOrderStatusService orderStatusService,
			IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			IKlarnaPendingOrderService klarnaPendingOrderService,
			IKlarnaPendingIntervalSetting klarnaPendingIntervalSetting,
			IMaksuturvaPendingOrderService maksuturvaPendingOrderService,
			IMaksuturvaPaymentProvider maksuturvaPaymentProvider,
			IMaksuturvaSetting maksuturvaSetting,
			IQliroPendingOrderService qliroPendingOrderService,
			IQliroPendingIntervalSetting qliroPendingIntervalSetting)
		{
			ChannelService = (ILekmerChannelService)channelService;
			OrderService = (ILekmerOrderService)orderService;
			OrderSecureService = (ILekmerOrderSecureService)orderSecureService;
			OrderStatusService = orderStatusService;
			OrderItemStatusService = orderItemStatusService;
			GiftCardViaMailInfoService = giftCardViaMailInfoService;

			KlarnaPendingOrderService = klarnaPendingOrderService;
			KlarnaPendingIntervalSetting = klarnaPendingIntervalSetting;

			MaksuturvaPendingOrderService = maksuturvaPendingOrderService;
			MaksuturvaPaymentProvider = maksuturvaPaymentProvider;
			MaksuturvaSetting = maksuturvaSetting;

			QliroPendingOrderService = qliroPendingOrderService;
			QliroPendingIntervalSetting = qliroPendingIntervalSetting;
		}


		#region Klarna

		public virtual void CheckKlarnaOrders()
		{
			DateTime checkToDate = DateTime.Now.AddHours(-KlarnaPendingIntervalSetting.OrderStatusCheckInterval);

			// Get pending orders
			Collection<IKlarnaPendingOrder> pendingOrders = KlarnaPendingOrderService.GetPendingOrdersForStatusCheck(checkToDate);

			// Check order status
			foreach (var pendingOrder in pendingOrders)
			{
				try
				{
					CheckOrder(pendingOrder);
				}
				catch (Exception ex)
				{
					LogError("Unable check order.", ex, pendingOrder.OrderId);
					SnoozeNextOrderStatusCheck(pendingOrder);
				}
			}
		}


		protected virtual void CheckOrder(IKlarnaPendingOrder pendingOrder)
		{
			IUserContext userContext = GetUserContext(pendingOrder.ChannelId);

			var order = (ILekmerOrderFull)OrderService.GetFullById(userContext, pendingOrder.OrderId);
			string reservationId = GetOrderReservationId(order);

			var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsKlarnaPayment(p.PaymentTypeId));
			if (orderPayment == null)
			{
				throw new InvalidOperationException("Order does not contain any Klarna payments!");
			}

			IKlarnaClient klarnaClient = GetKlarnaClient(orderPayment.PaymentTypeId, userContext.Channel.CommonName);

			// Check order status
			IKlarnaResult result = klarnaClient.CheckOrderStatus(order.Id, reservationId);

			SnoozeNextOrderStatusCheck(pendingOrder);

			// Handle new order status
			switch (result.ResponseCode)
			{
				case (int)KlarnaResponseCode.Ok:
					ConfirmOrder(userContext, order);
					break;
				case (int)KlarnaResponseCode.Denied:
					RejectOrder(userContext, order, true);
					break;
			}

			// Remove penging order
			switch (result.ResponseCode)
			{
				case (int)KlarnaResponseCode.Ok:
				case (int)KlarnaResponseCode.Denied:
					KlarnaPendingOrderService.Delete(pendingOrder.Id);
					break;
			}

			if (result.ResponseCode == (int)KlarnaResponseCode.Pending)
			{
				CheckOrderForGuaranteedDecision(pendingOrder, order);
			}
		}

		protected virtual void SnoozeNextOrderStatusCheck(IKlarnaPendingOrder klarnaPendingOrder)
		{
			klarnaPendingOrder.LastAttempt = DateTime.Now;
			KlarnaPendingOrderService.Update(klarnaPendingOrder);
		}

		protected virtual void CheckOrderForGuaranteedDecision(IKlarnaPendingOrder klarnaPendingOrder, ILekmerOrderFull order)
		{
			if (klarnaPendingOrder.FirstAttempt < DateTime.Now.AddHours(-KlarnaPendingIntervalSetting.DecisionGuaranteedInterval))
			{
				string message = string.Format(CultureInfo.InvariantCulture,
					"After {0} hours Klarna still return pending order status. \r\n Order id: {1} Status: {2} Create date: {3}",
					KlarnaPendingIntervalSetting.DecisionGuaranteedInterval,
					klarnaPendingOrder.OrderId,
					order.OrderStatus.CommonName,
					order.CreatedDate);

				_log.Fatal(message);
			}
		}

		// Payment type checks
		protected virtual IKlarnaClient GetKlarnaClient(int paymentTypeId, string channelCommonName)
		{
			if (PaymentTypeHelper.IsKlarnaSimplePayment(paymentTypeId))
			{
				return KlarnaFactory.CreateKlarnaClient(channelCommonName);
			}
			if (PaymentTypeHelper.IsKlarnaAdvancedPayment(paymentTypeId))
			{
				return KlarnaFactory.CreateKlarnaAdvancedClient(channelCommonName);
			}
			return null;
		}

		#endregion

		#region Maksuturva

		public virtual void CheckMaksuturvaOrders()
		{
			_checkDate = DateTime.Now;
			Collection<IMaksuturvaPendingOrder> pendingOrders = MaksuturvaPendingOrderService.GetPendingOrdersForStatusCheck(_checkDate);
			foreach (var pendingOrder in pendingOrders)
			{
				_userContext = GetUserContext(pendingOrder.ChannelId);
				MaksuturvaSetting.Initialize(_userContext.Channel.CommonName);
				var order = (ILekmerOrderFull) OrderService.GetFullById(_userContext, pendingOrder.OrderId);

				try
				{
					CheckMaksuturvaOrder(pendingOrder, order);
				}
				catch (Exception ex)
				{
					LogError("Unable check order.", ex, pendingOrder.OrderId);
					SetNextOrderStatusCheck(pendingOrder, order);
				}
			}
		}


		protected virtual void CheckMaksuturvaOrder(IMaksuturvaPendingOrder pendingOrder, ILekmerOrderFull order)
		{
			bool isConfirmed = false;

			bool isPaid = IsPaid(order.Id, _userContext.Channel.CommonName);
			if (isPaid)
			{
				isConfirmed = ConfirmMaksuturvaOrder(order);
			}

			if (isConfirmed)
			{
				MaksuturvaPendingOrderService.Delete(pendingOrder.OrderId);
			}
			else
			{
				bool isNeedCheckAgain = IsNeedCheckAgain(pendingOrder, order);
				if (isNeedCheckAgain)
				{
					SetNextOrderStatusCheck(pendingOrder, order);
				}
				else
				{
					RejectMaksuturvaOrder(order);
					MaksuturvaPendingOrderService.Delete(pendingOrder.OrderId);
				}
			}
		}

		protected virtual bool ConfirmMaksuturvaOrder(ILekmerOrderFull order)
		{
			bool isConfirmed = false;

			string orderStatus = order.OrderStatus.CommonName;
			bool isPaymentPending = orderStatus == OrderStatusName.PaymentPending;
			bool isRejectedByPaymentProvider = orderStatus == OrderStatusName.RejectedByPaymentProvider;
			bool isPaymentOnHold = orderStatus == OrderStatusName.PaymentOnHold;

			if (isPaymentPending || isRejectedByPaymentProvider || isPaymentOnHold)
			{
				OrderService.OrderVoucherConsume(_userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(_userContext, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(_userContext, OrderStatusName.PaymentConfirmed));

				// Set order payment id
				if ((isPaymentPending || isRejectedByPaymentProvider) && order.Payments.Count > 0)
				{
					order.Payments.First().ReferenceId = order.Id.ToString(CultureInfo.InvariantCulture);
				}

				OrderService.SaveFull(_userContext, order);

				OrderService.UpdateNumberInStock(_userContext, order);
				GiftCardViaMailInfoService.Save(_userContext, order);

				if (isPaymentPending || isRejectedByPaymentProvider)
				{
					OrderSecureService.SendConfirm(order);
				}
				// else - Order confirmation has been sent already !!!
				isConfirmed = true;
			}
			else
			{
				LogError("Unable confirm order - status differs from PaymentPending, RejectedByPaymentProvider, PaymentOnHold.", order);
			}

			return isConfirmed;
		}

		protected virtual void RejectMaksuturvaOrder(ILekmerOrderFull order)
		{
			string orderStatus = order.OrderStatus.CommonName;

			if (orderStatus == OrderStatusName.PaymentPending || orderStatus == OrderStatusName.PaymentOnHold)
			{
				OrderService.OrderVoucherRelease(_userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(_userContext, OrderStatusName.RejectedByPaymentProvider);
				OrderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);

				OrderService.SendRejectMail(_userContext, order);
			}
			else
			{
				LogError("Unable reject order - status differs from PaymentPending, PaymentOnHold.", order);
			}
		}

		protected virtual void SetNextOrderStatusCheck(IMaksuturvaPendingOrder pendingOrder, ILekmerOrderFull order)
		{
			//var currentNextAttempt = pendingOrder.NextAttempt;
			if (pendingOrder.FirstAttempt == null)
			{
				pendingOrder.FirstAttempt = pendingOrder.NextAttempt;
			}
			pendingOrder.LastAttempt = pendingOrder.NextAttempt;
			pendingOrder.NextAttempt = MaksuturvaSetting.GetOrderCheckDate(order.OrderStatus.CommonName, order.CreatedDate, _checkDate);

			MaksuturvaPendingOrderService.Update(pendingOrder);
		}

		protected virtual bool IsNeedCheckAgain(IMaksuturvaPendingOrder pendingOrder, ILekmerOrderFull order)
		{
			bool isNeedCheckAgain = true;

			if (pendingOrder.FirstAttempt != null && pendingOrder.FirstAttempt < _checkDate.AddDays(-MaksuturvaSetting.DecisionGuaranteedInterval))
			{
				string message = string.Format(CultureInfo.InvariantCulture,
					"After {0} days order still in not payed status. \r\n Order id: {1} Status: {2} Create date: {3}",
					MaksuturvaSetting.DecisionGuaranteedInterval,
					pendingOrder.OrderId,
					order.OrderStatus.CommonName,
					order.CreatedDate);

				_log.Fatal(message);

				isNeedCheckAgain = false;
			}

			return isNeedCheckAgain;
		}

		protected virtual bool IsPaid(int paymentId, string channelName)
		{
			MaksuturvaResponseCode responseCode = MaksuturvaPaymentProvider.GetMaksuturvaPaymentStatus(channelName, paymentId);
			bool isPaid = responseCode == MaksuturvaResponseCode.WebStoreNotYetCompensated || responseCode == MaksuturvaResponseCode.WebStoreCompensated;
			return isPaid;
		}

		#endregion

		#region Qliro

		public virtual void CheckQliroOrders()
		{
			DateTime checkToDate = DateTime.Now.AddHours(-QliroPendingIntervalSetting.OrderStatusCheckInterval);

			// Get pending orders
			Collection<IQliroPendingOrder> pendingOrders = QliroPendingOrderService.GetPendingOrdersForStatusCheck(checkToDate);

			// Check order status
			foreach (var pendingOrder in pendingOrders)
			{
				try
				{
					CheckOrder(pendingOrder);
				}
				catch (Exception ex)
				{
					LogError("Unable check order.", ex, pendingOrder.OrderId);
					SnoozeNextOrderStatusCheck(pendingOrder);
				}
			}
		}


		protected virtual void CheckOrder(IQliroPendingOrder pendingOrder)
		{
			IUserContext userContext = GetUserContext(pendingOrder.ChannelId);

			var order = (ILekmerOrderFull)OrderService.GetFullById(userContext, pendingOrder.OrderId);
			string reservationId = GetOrderReservationId(order);

			var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsQliroPayment(p.PaymentTypeId));
			if (orderPayment == null)
			{
				throw new InvalidOperationException("Order does not contain any Qliro payments!");
			}

			IQliroClient qliroClient = GetQliroClient(userContext.Channel.CommonName);

			// Check order status
			IQliroGetStatusResult result = qliroClient.CheckOrderStatus(userContext.Channel, order.Id, reservationId);

			SnoozeNextOrderStatusCheck(pendingOrder);

			// Handle new order status
			switch (result.InvoiceStatus)
			{
				case QliroInvoiceStatus.Ok:
					ConfirmOrder(userContext, order);
					break;
				case QliroInvoiceStatus.Denied:
					RejectOrder(userContext, order, QliroPendingIntervalSetting.IsSendRejectMessage);
					break;
			}

			// Remove penging order
			switch (result.InvoiceStatus)
			{
				case QliroInvoiceStatus.Ok:
				case QliroInvoiceStatus.Denied:
					QliroPendingOrderService.Delete(pendingOrder.Id);
					break;
			}

			if (result.InvoiceStatus == QliroInvoiceStatus.Holding)
			{
				CheckOrderForGuaranteedDecision(pendingOrder, order);
			}
		}

		protected virtual void SnoozeNextOrderStatusCheck(IQliroPendingOrder qliroPendingOrder)
		{
			qliroPendingOrder.LastAttempt = DateTime.Now;
			QliroPendingOrderService.Update(qliroPendingOrder);
		}

		protected virtual void CheckOrderForGuaranteedDecision(IQliroPendingOrder qliroPendingOrder, ILekmerOrderFull order)
		{
			if (qliroPendingOrder.FirstAttempt < DateTime.Now.AddHours(-QliroPendingIntervalSetting.DecisionGuaranteedInterval))
			{
				string message = string.Format(CultureInfo.InvariantCulture,
					"After {0} hours Qliro still return pending order status. \r\n Order id: {1} Status: {2} Create date: {3}",
					QliroPendingIntervalSetting.DecisionGuaranteedInterval,
					qliroPendingOrder.OrderId,
					order.OrderStatus.CommonName,
					order.CreatedDate);

				_log.Fatal(message);
			}
		}

		protected virtual IQliroClient GetQliroClient(string channelCommonName)
		{
			return QliroFactory.CreateQliroClient(channelCommonName);
		}

		#endregion


		protected virtual void ConfirmOrder(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentOnHold)
			{
				OrderService.OrderVoucherConsume(userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(userContext, OrderStatusName.PaymentConfirmed));

				OrderService.SaveFull(userContext, order);

				OrderService.UpdateNumberInStock(userContext, order);
				GiftCardViaMailInfoService.Save(userContext, order);

				// Order confirmation has been sent already !!!
			}
			else
			{
				LogError("Unable confirm order - status differs from PaymentOnHold.", order);
			}
		}

		protected virtual void RejectOrder(IUserContext userContext, ILekmerOrderFull order, bool isSendRejectMessage)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentOnHold)
			{
				OrderService.OrderVoucherRelease(userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.RejectedByPaymentProvider);
				OrderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);

				if (isSendRejectMessage)
				{
					OrderService.SendRejectMail(userContext, order);
				}
			}
			else
			{
				LogError("Unable reject order - status differs from PaymentOnHold.", order);
			}
		}

		protected virtual string GetOrderReservationId(ILekmerOrderFull order)
		{
			if (order.Payments.Count > 0)
			{
				return order.Payments.First().ReferenceId;
			}

			_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not find order reservation number. Order id = {0}", order.Id));

			return string.Empty;
		}


		protected virtual IUserContext GetUserContext(int channelId)
		{
			var userContext = IoC.Resolve<IUserContext>();

			userContext.Channel = GetChannel(channelId);

			return userContext;
		}

		protected virtual IChannel GetChannel(int channelId)
		{
			if (_channels == null)
			{
				_channels = ChannelService.GetAll();
			}

			var channel = _channels.FirstOrDefault(c => c.Id == channelId);

			if (channel == null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not find channel. Id = {0}", channelId));
			}

			return channel;
		}

		private void LogError(string message, ILekmerOrderFull order)
		{
			_log.ErrorFormat("{0}\r\nOrder id: {1} Status: {2}", message, order.Id, order.OrderStatus.CommonName);
		}

		private void LogError(string message, Exception exception, int orderId)
		{
			string errorMessage = string.Format("{0}\r\nOrder id: {1}", message, orderId);
			_log.Error(errorMessage, exception);
		}
	}
}