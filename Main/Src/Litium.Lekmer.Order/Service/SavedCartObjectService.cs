﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public class SavedCartObjectService : ISavedCartObjectService
	{
		protected virtual SavedCartObjectRepository SavedCartObjectRepository { get; private set; }

		public SavedCartObjectService(SavedCartObjectRepository savedCartObjectRepository)
		{
			SavedCartObjectRepository = savedCartObjectRepository;
		}

		public virtual ISavedCartObject Create(int cartid, int channelId, string email)
		{
			var savedCart = IoC.Resolve<ISavedCartObject>();
			savedCart.CartId = cartid;
			savedCart.ChannelId = channelId;
			savedCart.Email = email;
			savedCart.Status = BusinessObjectStatus.New;
			return savedCart;
		}

		public virtual void InsertSavedCart(ISavedCartObject savedCart)
		{
			if (savedCart == null) throw new ArgumentNullException("savedCart");

			SavedCartObjectRepository.InsertSavedCart(savedCart);
		}

		public virtual void UpdateSavedCart(ISavedCartObject savedCart)
		{
			if (savedCart == null) throw new ArgumentNullException("savedCart");

			SavedCartObjectRepository.UpdateSavedCart(savedCart);
		}

		public virtual Collection<ISavedCartObject> GetSavedCartForReminder(int savedCartReminderInDays)
		{
			return SavedCartObjectRepository.GetSavedCartForReminder(savedCartReminderInDays);
		}
	}
}