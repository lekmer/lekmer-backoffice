﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public class OrderVoucherInfoService : IOrderVoucherInfoService
	{
		protected OrderVoucherInfoRepository Repository { get; private set; }

		public OrderVoucherInfoService(OrderVoucherInfoRepository repository)
		{
			Repository = repository;
		}

		public virtual IOrderVoucherInfo Create()
		{
			var orderVoucherInfo = IoC.Resolve<IOrderVoucherInfo>();

			orderVoucherInfo.Status = BusinessObjectStatus.New;

			return orderVoucherInfo;
		}

		public virtual IOrderVoucherInfo GetByOrderId(int orderId)
		{
			Repository.EnsureNotNull();

			IOrderVoucherInfo orderVoucherInfo = Repository.GetByOrderId(orderId);

			if (orderVoucherInfo != null)
			{
				orderVoucherInfo.SetUntouched();
			}

			return orderVoucherInfo;
		}

		public virtual void Save(int orderId, IOrderVoucherInfo orderVoucherInfo)
		{
			if (orderVoucherInfo == null) throw new ArgumentNullException("orderVoucherInfo");

			Repository.EnsureNotNull();

			Repository.Save(orderId, orderVoucherInfo);

			orderVoucherInfo.SetUntouched();
		}

		public virtual void UpdateStatus(int orderId, IOrderVoucherInfo orderVoucherInfo)
		{
			if (orderVoucherInfo == null) throw new ArgumentNullException("orderVoucherInfo");

			Repository.EnsureNotNull();

			Repository.UpdateStatus(orderId, orderVoucherInfo);
		}
	}
}
