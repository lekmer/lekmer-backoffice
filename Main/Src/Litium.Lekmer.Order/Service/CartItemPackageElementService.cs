using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public class CartItemPackageElementService : ICartItemPackageElementService
	{
		protected CartItemPackageElementRepository Repository { get; private set; }

		public CartItemPackageElementService(CartItemPackageElementRepository cartItemPackageElementRepository)
		{
			Repository = cartItemPackageElementRepository;
		}


		public virtual ICartItemPackageElement Create()
		{
			var cartItemPackageElement = IoC.Resolve<ICartItemPackageElement>();

			cartItemPackageElement.Status = BusinessObjectStatus.New;

			return cartItemPackageElement;
		}


		public virtual void Save(ICartItemPackageElement cartItemPackageElement)
		{
			if (cartItemPackageElement == null)
			{
				throw new ArgumentNullException("cartItemPackageElement");
			}

			int id = Repository.Save(cartItemPackageElement);

			cartItemPackageElement.Id = id;
			cartItemPackageElement.SetUntouched();
		}

		public virtual void Save(int cartItemId, Collection<ICartItemPackageElement> cartItemPackageElementList)
		{
			if (cartItemPackageElementList == null)
			{
				throw new ArgumentNullException("cartItemPackageElementList");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (ICartItemPackageElement cartItemPackageElement in cartItemPackageElementList)
				{
					cartItemPackageElement.CartItemId = cartItemId;
					Save(cartItemPackageElement);
				}

				transactedOperation.Complete();
			}
		}


		public virtual void DeleteByCartItem(int cartItemId)
		{
			Repository.DeleteByCartItem(cartItemId);
		}


		public virtual Collection<ICartItemPackageElement> GetAllByCart(IUserContext context, int cartId)
		{
			Collection<ICartItemPackageElement> items = Repository.GetAllByCart(cartId);
			return items ?? new Collection<ICartItemPackageElement>();
		}
	}
}