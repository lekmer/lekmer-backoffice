using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderItemService : OrderItemService, ILekmerOrderItemService
	{
		protected OrderItemSizeRepository OrderItemSizeRepository;
		protected IBrandService BrandService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected IPackageOrderItemService PackageOrderItemService { get; private set; }

		public LekmerOrderItemService(
			OrderItemRepository orderItemRepository,
			OrderItemSizeRepository orderItemSizeRepository,
			IOrderProductCampaignService orderProductCampaignService,
			IBrandService brandService,
			ITagGroupService tagGroupService,
			IPackageOrderItemService packageOrderItemService)
			: base(orderItemRepository, orderProductCampaignService)
		{
			OrderItemSizeRepository = orderItemSizeRepository;
			BrandService = brandService;
			TagGroupService = tagGroupService;
			PackageOrderItemService = packageOrderItemService;
		}

		public override int Save(IUserContext context, IOrderItem orderItem)
		{
			var orderItemId = base.Save(context, orderItem);

			var lekmerOrderItem = (ILekmerOrderItem)orderItem;
			if (lekmerOrderItem.OrderItemSize.SizeId.HasValue)
			{
				lekmerOrderItem.OrderItemSize.OrderItemId = orderItemId;
				OrderItemSizeRepository.Save(context, lekmerOrderItem.OrderItemSize);
			}

			if (ProductExtensions.IsPackage(lekmerOrderItem.ProductTypeId))
			{
				if (lekmerOrderItem.PackageOrderItems == null)
				{
					throw new InvalidOperationException("lekmerOrderItem.PackageOrderItems can't be null when it is package");
				}

				foreach (var packageOrderItem in lekmerOrderItem.PackageOrderItems)
				{
					packageOrderItem.OrderItemId = orderItemId;
					packageOrderItem.OrderId = orderItem.OrderId;
					packageOrderItem.OrderItemStatus = orderItem.OrderItemStatus;
					packageOrderItem.PackageOrderItemId = PackageOrderItemService.Save(packageOrderItem);
				}
			}

			return orderItemId;
		}

		public override Collection<IOrderItem> GetAllByOrder(IUserContext context, int orderId)
		{
			var orderItems = Repository.GetAllByOrder(orderId);
			foreach (ILekmerOrderItem orderItem in orderItems)
			{
				if (orderItem.BrandId.HasValue)
				{
					orderItem.Brand = BrandService.GetById(context, orderItem.BrandId.Value);
				}
				orderItem.TagGroups = TagGroupService.GetAllByProduct(context, orderItem.ProductId);
				if (ProductExtensions.IsPackage(orderItem.ProductTypeId))
				{
					orderItem.PackageOrderItems = PackageOrderItemService.GetAllByOrderItem(orderItem.Id);
				}
				
			}
			return orderItems;
		}

		public virtual Collection<IOrderItem> GetAllByOrderWithoutBrandAndTags(int orderId)
		{
			var orderItems = Repository.GetAllByOrder(orderId);
			foreach (ILekmerOrderItem orderItem in orderItems)
			{
				if (ProductExtensions.IsPackage(orderItem.ProductTypeId))
				{
					orderItem.PackageOrderItems = PackageOrderItemService.GetAllByOrderItem(orderItem.Id);
				}
			}
			return orderItems;
		}
	}
}