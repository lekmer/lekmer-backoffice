﻿using System;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderPayment : OrderPayment, ILekmerOrderPayment
	{
		private bool? _captured;
		private int? _klarnaEid;
		private int? _klarnaPClass;
		private string _maksuturvaCode;
		private string _qliroClientRef;
		private string _qliroPaymentCode;
		private int? _collectorStoreId;
		private string _collectorPaymentCode;

		public bool? Captured
		{
			get { return _captured; }
			set
			{
				CheckChanged(_captured, value);
				_captured = value;
			}
		}

		public int? KlarnaEid
		{
			get { return _klarnaEid; }
			set
			{
				CheckChanged(_klarnaEid, value);
				_klarnaEid = value;
			}
		}

		public int? KlarnaPClass
		{
			get { return _klarnaPClass; }
			set
			{
				CheckChanged(_klarnaPClass, value);
				_klarnaPClass = value;
			}
		}

		public string MaksuturvaCode
		{
			get { return _maksuturvaCode; }
			set
			{
				CheckChanged(_maksuturvaCode, value);
				_maksuturvaCode = value;
			}
		}

		public string QliroClientRef
		{
			get { return _qliroClientRef; }
			set
			{
				CheckChanged(_qliroClientRef, value);
				_qliroClientRef = value;
			}
		}

		public string QliroPaymentCode
		{
			get { return _qliroPaymentCode; }
			set
			{
				CheckChanged(_qliroPaymentCode, value);
				_qliroPaymentCode = value;
			}
		}

		public int? CollectorStoreId
		{
			get { return _collectorStoreId; }
			set
			{
				CheckChanged(_collectorStoreId, value);
				_collectorStoreId = value;
			}
		}

		public string CollectorPaymentCode
		{
			get { return _collectorPaymentCode; }
			set
			{
				CheckChanged(_collectorPaymentCode, value);
				_collectorPaymentCode = value;
			}
		}
	}
}