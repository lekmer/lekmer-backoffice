using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class CartItemPackageElement : BusinessObjectBase, ICartItemPackageElement
	{
		private int _id;
		private int _cartItemId;
		private int _productId;
		private int? _sizeId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int CartItemId
		{
			get { return _cartItemId; }
			set
			{
				CheckChanged(_cartItemId, value);
				_cartItemId = value;
			}
		}

		public int? SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}
	}
}