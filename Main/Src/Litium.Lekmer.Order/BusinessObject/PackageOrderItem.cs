using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class PackageOrderItem : BusinessObjectBase, IPackageOrderItem
	{
		public int PackageOrderItemId { get; set; }
		public int OrderItemId { get; set; }
		public int OrderId { get; set; }
		public int Quantity { get; set; }
		public Price PackagePrice { get; set; }
		public Price ActualPrice { get; set; }
		public Price OriginalPrice { get; set; }
		public IOrderItemStatus OrderItemStatus { get; set; }
		public int? SizeId { get; set; }
		public string SizeErpId { get; set; }
		public int ProductId { get; set; }
		public string ErpId { get; set; }
		public string EanCode { get; set; }
		public string Title { get; set; }

		public virtual decimal GetOriginalVatAmount()
		{
			return OriginalPrice.IncludingVat - OriginalPrice.ExcludingVat;
		}

		public virtual decimal GetActualVatAmount()
		{
			return PackagePrice.IncludingVat - PackagePrice.ExcludingVat;
		}

		public virtual decimal GetOriginalVatPercent()
		{
			decimal vatPercent = 0m;

			if (OriginalPrice.ExcludingVat != 0)
			{
				vatPercent = (GetOriginalVatAmount() / OriginalPrice.ExcludingVat) * 100;
				vatPercent = vatPercent.RoundToInteger();
			}

			return vatPercent;
		}
	}
}