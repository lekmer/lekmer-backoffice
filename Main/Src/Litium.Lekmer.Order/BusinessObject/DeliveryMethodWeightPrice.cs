﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class DeliveryMethodWeightPrice : BusinessObjectBase, IDeliveryMethodWeightPrice
	{
		private decimal? _weightFrom;
		private decimal? _weightTo;
		private decimal _freightCost;

		public decimal? WeightFrom
		{
			get { return _weightFrom; }
			set
			{
				CheckChanged(_weightFrom, value);
				_weightFrom = value;
			}
		}

		public decimal? WeightTo
		{
			get { return _weightTo; }
			set
			{
				CheckChanged(_weightTo, value);
				_weightTo = value;
			}
		}

		public decimal FreightCost
		{
			get { return _freightCost; }
			set
			{
				CheckChanged(_freightCost, value);
				_freightCost = value;
			}
		}
	}
}