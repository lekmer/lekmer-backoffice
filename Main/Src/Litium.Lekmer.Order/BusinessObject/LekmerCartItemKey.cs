namespace Litium.Lekmer.Order
{
	public struct LekmerCartItemKey
	{
		private readonly int _productId;
		private readonly decimal _price;
		private readonly int? _sizeId;
		private readonly int? _hashCode;

		public LekmerCartItemKey(int productId, decimal price, int? sizeId, int? hashCode)
		{
			_productId = productId;
			_price = price;
			_sizeId = sizeId;
			_hashCode = hashCode;
		}

		public int ProductId
		{
			get { return _productId; }
		}

		public decimal Price
		{
			get { return _price; }
		}

		public int? SizeId
		{
			get { return _sizeId; }
		}

		public int? HashCode
		{
			get { return _hashCode; }
		}

		#region Equality members

		public bool Equals(LekmerCartItemKey other)
		{
			return other._productId == _productId && other._price == _price && other._sizeId == _sizeId && other._hashCode == _hashCode;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (obj.GetType() != typeof (LekmerCartItemKey)) return false;
			return Equals((LekmerCartItemKey) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (_productId * (_sizeId ?? 1) * 397 * (_hashCode ?? 1)) ^ _price.GetHashCode();
			}
		}

		public static bool operator ==(LekmerCartItemKey left, LekmerCartItemKey right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(LekmerCartItemKey left, LekmerCartItemKey right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}