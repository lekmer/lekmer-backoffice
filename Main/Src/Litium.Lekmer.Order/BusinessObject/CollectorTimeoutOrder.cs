﻿using Litium.Lekmer.Payment.Collector;

namespace Litium.Lekmer.Order
{
	public class CollectorTimeoutOrder : ICollectorTimeoutOrder
	{
		public int TransactionId { get; set; }
		public CollectorTransactionStatus? StatusCode { get; set; }
		public CollectorInvoiceStatus? InvoiceStatus { get; set; }
		public int OrderId { get; set; }
		public string InvoiceNo { get; set; }
		public int ChannelId { get; set; }
	}
}
