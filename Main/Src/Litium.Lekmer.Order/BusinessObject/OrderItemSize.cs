using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class OrderItemSize : IOrderItemSize
	{
		public int OrderItemId { get; set; }
		public int? SizeId { get; set; }
		public string ErpId { get; set; }
	}
}