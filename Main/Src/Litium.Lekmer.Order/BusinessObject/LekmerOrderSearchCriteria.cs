﻿using System;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderSearchCriteria : CustomerSearchCriteria, ILekmerOrderSearchCriteria
	{
		public int OrderId
		{
			get; 
			set; 
		}
	}
}