using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerCartItem : CartItem, ILekmerCartItem
	{
		private int? _sizeId;
		private bool _isAffectedByCampaign;
		private string _ipAddress;
		private Collection<ICartItemPackageElement> _packageElements;

		public int? SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}

		public bool IsAffectedByCampaign
		{
			get { return _isAffectedByCampaign; }
			set
			{
				CheckChanged(_isAffectedByCampaign, value);
				_isAffectedByCampaign = value;
			}
		}

		public string IPAddress
		{
			get { return _ipAddress; }
			set
			{
				CheckChanged(_ipAddress, value);
				_ipAddress = value;
			}
		}

		public Collection<ICartItemPackageElement> PackageElements
		{
			get { return _packageElements; }
			set
			{
				CheckChanged(_packageElements, value);
				_packageElements = value;
			}
		}

		public virtual void SplitPackageElements(ref Collection<int> itemWithoutSizes, ref Collection<IPackageElement> itemWithSizes)
		{
			if (PackageElements == null || PackageElements.Count == 0) return;

			foreach (var item in PackageElements)
			{
				if (item.SizeId.HasValue && item.SizeId > 0)
				{
					itemWithSizes.Add(new PackageElement {ProductId = item.ProductId, SizeId = item.SizeId});
				}
				else
				{
					itemWithoutSizes.Add(item.ProductId);
				}
			}
		}

		public virtual void ChangeQuantityForFreeProduct(int quantity)
		{
			if (IsAffectedByCampaign)
			{
				if (quantity != 0)
				{
					//TODO: Move that kind of logic on cart level
					var cartItemOptionService = IoC.Resolve<ICartItemOptionService>();
					var cartItemOption = cartItemOptionService.Create();
					cartItemOption.Id = Id;
					cartItemOption.Product.Id = Product.Id;
					cartItemOption.CartId = CartId;
					cartItemOption.SizeId = SizeId;
					cartItemOption.Quantity = quantity * (-1);
					cartItemOptionService.Save(cartItemOption);
				}
			}

			ChangeQuantity(quantity);
		}

		public virtual decimal GetDiscountPercent(bool isDiscountIncludingVat)
		{
			decimal discountPercent;
			decimal discount;

			var originalPrice = Product.Price;
			var actualPrice = GetActualPrice();

			if (isDiscountIncludingVat)
			{
				discount = (originalPrice.PriceIncludingVat - actualPrice.IncludingVat);
				if (discount < 0)
				{
					discount = 0;
				}
				discountPercent = (discount * 100) / originalPrice.PriceIncludingVat;
			}
			else
			{
				discount = (originalPrice.PriceExcludingVat - actualPrice.ExcludingVat);
				if (discount < 0)
				{
					discount = 0;
				}
				discountPercent = (discount * 100) / originalPrice.PriceExcludingVat;
			}

			return discountPercent;
		}

		public void CleanPackageElementsId()
		{
			if (PackageElements == null || PackageElements.Count == 0) return;

			foreach (var item in PackageElements)
			{
				item.Id = -1;
			}
		}
	}
}