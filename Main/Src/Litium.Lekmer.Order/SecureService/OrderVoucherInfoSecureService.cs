﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public class OrderVoucherInfoSecureService : IOrderVoucherInfoSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected OrderVoucherInfoRepository Repository { get; private set; }

		public OrderVoucherInfoSecureService(IAccessValidator accessValidator, OrderVoucherInfoRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual IOrderVoucherInfo GetByOrderId(int orderId)
		{
			Repository.EnsureNotNull();

			IOrderVoucherInfo orderVoucherInfo = Repository.GetByOrderId(orderId);

			if (orderVoucherInfo != null)
			{
				orderVoucherInfo.SetUntouched();
			}

			return orderVoucherInfo;
		}

		public void Delete(ISystemUserFull systemUserFull, int orderId)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			Repository.Delete(orderId);
		}
	}
}