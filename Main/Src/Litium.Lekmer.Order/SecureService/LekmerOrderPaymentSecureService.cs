﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderPaymentSecureService : OrderPaymentSecureService, ILekmerOrderPaymentSecureService
	{
		protected LekmerOrderPaymentRepository LekmerRepository { get; private set; }

		public LekmerOrderPaymentSecureService(IAccessValidator accessValidator, OrderPaymentRepository repository)
			: base(accessValidator, repository)
		{
			LekmerRepository = (LekmerOrderPaymentRepository) repository;
		}

		public virtual void DeleteByOrder(ISystemUserFull systemUserFull, int orderId)
		{
			LekmerRepository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			LekmerRepository.DeleteByOrder(orderId);
		}
	}
}