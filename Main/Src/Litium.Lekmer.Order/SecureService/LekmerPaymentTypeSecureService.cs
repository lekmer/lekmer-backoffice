﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerPaymentTypeSecureService : PaymentTypeSecureService
	{
		protected LekmerPaymentTypeRepository LekmerRepository { get; private set; }

		public LekmerPaymentTypeSecureService(IAccessValidator accessValidator, PaymentTypeRepository repository)
			: base(accessValidator, repository)
		{
			LekmerRepository = repository as LekmerPaymentTypeRepository;
		}

		public override IPaymentType GetById(int paymentTypeId)
		{
			return LekmerRepository.GetByIdSecure(paymentTypeId);
		}
	}
}