﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderAuditSecureService : OrderAuditSecureService, ILekmerOrderAuditSecureService
	{
		protected LekmerOrderAuditRepository LekmerRepository { get; private set; }

		public LekmerOrderAuditSecureService(IAccessValidator accessValidator, OrderAuditRepository repository)
			: base(accessValidator, repository)
		{
			LekmerRepository = (LekmerOrderAuditRepository) repository;
		}

		public virtual void DeleteByOrder(ISystemUserFull systemUserFull, int orderId)
		{
			LekmerRepository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			LekmerRepository.DeleteByOrder(orderId);
		}
	}
}