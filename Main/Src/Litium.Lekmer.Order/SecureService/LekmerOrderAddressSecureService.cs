﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderAddressSecureService : OrderAddressSecureService, ILekmerOrderAddressSecureService
	{
		protected LekmerOrderAddressRepository LekmerRepository { get; private set; }

		public LekmerOrderAddressSecureService(IAccessValidator accessValidator, OrderAddressRepository repository)
			: base(accessValidator, repository)
		{
			LekmerRepository = repository as LekmerOrderAddressRepository;
		}

		public override int Save(ISystemUserFull systemUserFull, IOrderAddress orderAddress)
		{
			if (orderAddress == null) throw new ArgumentNullException("orderAddress");

			LekmerRepository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);

			using (var transactedOperation = new TransactedOperation())
			{
				orderAddress.Id = LekmerRepository.Save(orderAddress);
				LekmerRepository.SaveLekmerPart(orderAddress as ILekmerOrderAddress);

				orderAddress.SetUntouched();

				transactedOperation.Complete();
			}

			return orderAddress.Id;
		}

		public void Delete(ISystemUserFull systemUserFull, int orderAddressId)
		{
			LekmerRepository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			LekmerRepository.Delete(orderAddressId);
		}
	}
}