using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderItemSecureService : OrderItemSecureService
	{
		protected OrderItemSizeRepository _orderItemSizeRepository;
		protected IPackageOrderItemSecureService PackageOrderItemSecureService { get; private set; }

		public LekmerOrderItemSecureService(
			IAccessValidator accessValidator,
			OrderItemRepository repository,
			IOrderItemCampaignInfoSecureService orderItemCampaignInfoSecureService,
			IOrderProductCampaignSecureService orderProductCampaignSecureService,
			IPackageOrderItemSecureService packageOrderItemSecureService)
		: base(
			accessValidator,
			repository,
			orderItemCampaignInfoSecureService,
			orderProductCampaignSecureService
		)
		{
			_orderItemSizeRepository = IoC.Resolve<OrderItemSizeRepository>();
			PackageOrderItemSecureService = packageOrderItemSecureService;
		}

		protected override void FillOrderItem(IOrderItem orderItem)
		{
			base.FillOrderItem(orderItem);
			((ILekmerOrderItem) orderItem).PackageOrderItems = PackageOrderItemSecureService.GetAllByOrderItem(orderItem.Id);
		}

		public override void Save(ISystemUserFull systemUserFull, IOrderItem orderItem)
		{
			if (orderItem == null) throw new ArgumentNullException("orderItem");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(orderItem);
				OrderProductCampaignSecureService.Save(orderItem);
				foreach (var packageOrderItem in ((ILekmerOrderItem)orderItem).PackageOrderItems)
				{
					packageOrderItem.Quantity = orderItem.Quantity;
					packageOrderItem.OrderItemStatus = orderItem.OrderItemStatus;
					PackageOrderItemSecureService.Save(systemUserFull, packageOrderItem);
				}
				transactedOperation.Complete();
			}
		}

		public override void Delete(ISystemUserFull systemUserFull, IOrderItem orderItem)
		{
			if (orderItem == null) throw new ArgumentNullException("orderItem");

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				_orderItemSizeRepository.Delete(((ILekmerOrderItem) orderItem).OrderItemSize);
				PackageOrderItemSecureService.Delete(systemUserFull, orderItem.Id);
				base.Delete(systemUserFull, orderItem);

				transactedOperation.Complete();
			}
		}
	}
}