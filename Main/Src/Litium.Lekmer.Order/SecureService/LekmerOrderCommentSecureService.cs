﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderCommentSecureService : OrderCommentSecureService, ILekmerOrderCommentSecureService
	{
		protected LekmerOrderCommentRepository LekmerRepository { get; private set; }

		public LekmerOrderCommentSecureService(IAccessValidator accessValidator, OrderCommentRepository repository)
			: base(accessValidator, repository)
		{
			LekmerRepository = (LekmerOrderCommentRepository) repository;
		}

		public virtual void DeleteByOrder(ISystemUserFull systemUserFull, int orderId)
		{
			LekmerRepository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			LekmerRepository.DeleteByOrder(orderId);
		}
	}
}