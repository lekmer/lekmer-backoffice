﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public class LekmerOrderCartCampaignSecureService : OrderCartCampaignSecureService, ILekmerOrderCartCampaignSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected LekmerOrderCartCampaignRepository LekmerRepository { get; private set; }

		public LekmerOrderCartCampaignSecureService(IAccessValidator accessValidator, OrderCartCampaignRepository repository)
			: base(repository)
		{
			AccessValidator = accessValidator;
			LekmerRepository = (LekmerOrderCartCampaignRepository) repository;
		}

		public virtual void DeleteByOrder(ISystemUserFull systemUserFull, int orderId)
		{
			LekmerRepository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			LekmerRepository.DeleteByOrder(orderId);
		}
	}
}