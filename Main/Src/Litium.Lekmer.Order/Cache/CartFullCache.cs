﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Cache
{
	public sealed class CartFullCache : ScensumCacheBase<CartFullKey, ICartFull>
	{
		private static readonly CartFullCache _instance = new CartFullCache();

		private CartFullCache()
		{
		}

		public static CartFullCache Instance
		{
			get { return _instance; }
		}
	}

	public class CartFullKey : ICacheKey
	{
		public CartFullKey(int channelId, int cartId)
		{
			ChannelId = channelId;
			CartId = cartId;
		}

		public int ChannelId { get; set; }
		public int CartId { get; set; }

		public string Key
		{
			get
			{
				return string.Concat(ChannelId.ToString(CultureInfo.InvariantCulture), "-", CartId.ToString(CultureInfo.InvariantCulture));
			}
		}
	}
}