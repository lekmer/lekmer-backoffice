﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Cache
{
	public sealed class BlockCheckoutCache : ScensumCacheBase<BlockCheckoutKey, IBlockCheckout>
	{
		private static readonly BlockCheckoutCache _instance = new BlockCheckoutCache();

		private BlockCheckoutCache()
		{
		}

		public static BlockCheckoutCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			foreach (var channel in channels)
			{
				Remove(new BlockCheckoutKey(channel.Id, blockId));
			}
		}
	}

	public class BlockCheckoutKey : ICacheKey
	{
		public BlockCheckoutKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return string.Format("{0}-{1}", ChannelId.ToString(CultureInfo.InvariantCulture), BlockId);
			}
		}
	}
}