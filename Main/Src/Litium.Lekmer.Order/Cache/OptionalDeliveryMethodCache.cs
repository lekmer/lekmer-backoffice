﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Cache
{
	public sealed class OptionalDeliveryMethodCache : ScensumCacheBase<OptionalDeliveryMethodKey, IDeliveryMethod>
	{
		private static readonly OptionalDeliveryMethodCache _instance = new OptionalDeliveryMethodCache();

		private OptionalDeliveryMethodCache()
		{
		}

		public static OptionalDeliveryMethodCache Instance
		{
			get { return _instance; }
		}
	}

	public class OptionalDeliveryMethodKey : ICacheKey
	{
		public OptionalDeliveryMethodKey(int channelId, int deliveryMethodId)
		{
			ChannelId = channelId;
			DeliveryMethodId = deliveryMethodId;
		}

		public int ChannelId { get; set; }

		public int DeliveryMethodId { get; set; }

		public string Key
		{
			get
			{
				return string.Concat(
					"c_",
					ChannelId.ToString(CultureInfo.InvariantCulture),
					"_d_",
					DeliveryMethodId.ToString(CultureInfo.InstalledUICulture)
				);
			}
		}
	}
}