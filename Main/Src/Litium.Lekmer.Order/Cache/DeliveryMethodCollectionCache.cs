﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Cache
{
	public sealed class DeliveryMethodCollectionCache : ScensumCacheBase<DeliveryMethodCollectionKey, Collection<IDeliveryMethod>>
	{
		private static readonly DeliveryMethodCollectionCache _instance = new DeliveryMethodCollectionCache();

		private DeliveryMethodCollectionCache()
		{
		}

		public static DeliveryMethodCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public abstract class DeliveryMethodCollectionKey : ICacheKey
	{
		public abstract string Key { get; }
	}

	public class DeliveryMethodCollectionKeyAll : DeliveryMethodCollectionKey
	{
		public DeliveryMethodCollectionKeyAll(int channelId)
		{
			ChannelId = channelId;
		}

		public int ChannelId { get; set; }

		public override string Key
		{
			get { return "c_" + ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}

	public class DeliveryMethodCollectionKeyByPaymentType : DeliveryMethodCollectionKey
	{
		public DeliveryMethodCollectionKeyByPaymentType(int channelId, int paymentType)
		{
			ChannelId = channelId;
			PaymentTypeId = paymentType;
		}

		public int ChannelId { get; set; }
		public int PaymentTypeId { get; set; }

		public override string Key
		{
			get { return string.Concat("c_", ChannelId.ToString(CultureInfo.InvariantCulture), "_pt_", PaymentTypeId.ToString(CultureInfo.InvariantCulture)); }
		}
	}
}