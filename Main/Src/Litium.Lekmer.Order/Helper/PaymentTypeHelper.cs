﻿namespace Litium.Lekmer.Order
{
	public static class PaymentTypeHelper
	{
		// Payment type checks
		public static bool IsCommonPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.DibsCreditCard:
				case PaymentMethodType.DibsiDeal:
				case PaymentMethodType.KlarnaCheckout:
				case PaymentMethodType.MoneybookersCarteBlue:
				case PaymentMethodType.MoneybookersIDeal:
					return true;
			}

			return false;
		}

		public static bool IsRedirectionPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.DibsCreditCard:
				case PaymentMethodType.DibsiDeal:
				case PaymentMethodType.MoneybookersCarteBlue:
				case PaymentMethodType.MoneybookersIDeal:
				case PaymentMethodType.Maksuturva:
					return true;
			}

			return false;
		}

		// Dibs

		public static bool IsDibsPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.DibsCreditCard:
				case PaymentMethodType.DibsiDeal:
					return true;
			}

			return false;
		}

		public static bool IsDibsCreditCardPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.DibsCreditCard:
					return true;
			}

			return false;
		}

		public static bool IsDibsiDealPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.DibsiDeal:
					return true;
			}

			return false;
		}

		// Klarna

		public static bool IsKlarnaPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaInvoice:
				case PaymentMethodType.KlarnaPartPayment:
				case PaymentMethodType.KlarnaSpecialPartPayment:
				case PaymentMethodType.KlarnaAdvanced:
				case PaymentMethodType.KlarnaAdvancedPartPayment:
				case PaymentMethodType.KlarnaAdvancedSpecialPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsKlarnaInvoicePayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaInvoice:
				case PaymentMethodType.KlarnaAdvanced:
					return true;
			}

			return false;
		}

		public static bool IsKlarnaPartPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaPartPayment:
				case PaymentMethodType.KlarnaAdvancedPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsKlarnaSpecialPartPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaSpecialPartPayment:
				case PaymentMethodType.KlarnaAdvancedSpecialPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsKlarnaSimplePayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaInvoice:
				case PaymentMethodType.KlarnaPartPayment:
				case PaymentMethodType.KlarnaSpecialPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsKlarnaAdvancedPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaAdvanced:
				case PaymentMethodType.KlarnaAdvancedPartPayment:
				case PaymentMethodType.KlarnaAdvancedSpecialPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsKlarnaCheckoutPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.KlarnaCheckout:
					return true;
			}

			return false;
		}


		public static bool IsMaksuturvaPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.Maksuturva:
					return true;
			}

			return false;
		}

		public static bool IsMoneybookersPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.MoneybookersCarteBlue:
				case PaymentMethodType.MoneybookersIDeal:
					return true;
			}

			return false;
		}

		// Qliro

		public static bool IsQliroPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.QliroInvoice:
				case PaymentMethodType.QliroPartPayment:
				case PaymentMethodType.QliroSpecialPartPayment:
				case PaymentMethodType.QliroCompanyInvoice:
					return true;
			}

			return false;
		}

		public static bool IsQliroPersonInvoicePayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.QliroInvoice:
					return true;
			}

			return false;
		}

		public static bool IsQliroCompanyInvoicePayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.QliroCompanyInvoice:
					return true;
			}

			return false;
		}

		public static bool IsQliroPartPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.QliroPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsQliroSpecialPartPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.QliroSpecialPartPayment:
					return true;
			}

			return false;
		}

		// Collector

		public static bool IsCollectorPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.CollectorInvoice:
				case PaymentMethodType.CollectorPartPayment:
				case PaymentMethodType.CollectorSpecialPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsCollectorPersonInvoicePayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.CollectorInvoice:
					return true;
			}

			return false;
		}

		public static bool IsCollectorPartPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.CollectorPartPayment:
					return true;
			}

			return false;
		}

		public static bool IsCollectorSpecialPartPayment(int paymentTypeId)
		{
			var paymentMethodType = (PaymentMethodType)paymentTypeId;

			switch (paymentMethodType)
			{
				case PaymentMethodType.CollectorSpecialPartPayment:
					return true;
			}

			return false;
		}
	}
}