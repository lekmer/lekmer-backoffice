using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class OrderHelper
	{
		public virtual void RenderOrderPrices(Fragment fragment, ILekmerOrderFull item, ILekmerFormatter formatter, IChannel channel)
		{
			Price originalPriceSummary = item.GetOriginalPriceSummary();
			Price actualOrderItemsPriceTotal = item.GetActualPriceSummary();
			Price actualPriceTotal = item.GetActualPriceTotal();
			Price discountPriceTotal = item.GetDiscountPriceSummary();
			decimal actualVat = item.GetActualVatTotal();

			// price with channel decimals / possible rounding
			fragment.AddVariable("Order.TotalCostIncludingVat", formatter.FormatPrice(channel, actualPriceTotal.IncludingVat));
			fragment.AddVariable("Order.TotalCostExcludingVat", formatter.FormatPrice(channel, actualPriceTotal.ExcludingVat));
			fragment.AddVariable("Order.TotalVat", formatter.FormatPrice(channel, actualVat));
			fragment.AddVariable("Order.DiscountPriceIncludingVat", formatter.FormatPrice(channel, discountPriceTotal.IncludingVat));
			fragment.AddVariable("Order.OriginalItemPriceSummaryIncludingVat", formatter.FormatPrice(channel, originalPriceSummary.IncludingVat));
			fragment.AddVariable("Order.OriginalItemPriceSummaryExcludingVat", formatter.FormatPrice(channel, originalPriceSummary.ExcludingVat));

			// price with decimals / without rounding
			fragment.AddVariable("Order.TotalCostIncludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPriceTotal.IncludingVat));
			fragment.AddVariable("Order.TotalCostExcludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPriceTotal.ExcludingVat));
			fragment.AddVariable("Order.TotalVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualVat));
			fragment.AddVariable("Order.OriginalItemPriceSummaryIncludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, originalPriceSummary.IncludingVat));
			fragment.AddVariable("Order.OriginalItemPriceSummaryExcludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, originalPriceSummary.ExcludingVat));
			
			fragment.AddVariable("Order.ItemPriceSummaryIncludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualOrderItemsPriceTotal.IncludingVat));
			fragment.AddVariable("Order.ItemPriceSummaryExcludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualOrderItemsPriceTotal.ExcludingVat));
			fragment.AddVariable("Order.DiscountPriceIncludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, discountPriceTotal.IncludingVat));
		}
	}
}