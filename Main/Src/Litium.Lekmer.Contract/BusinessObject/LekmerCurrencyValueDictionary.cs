﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerCurrencyValueDictionary : Dictionary<int, decimal>
	{
		public LekmerCurrencyValueDictionary() { }
		protected LekmerCurrencyValueDictionary(SerializationInfo info, StreamingContext context) : base(info, context) {}

		public void Add(CurrencyValue currencyValue)
		{
			Add(currencyValue.Currency.Id, currencyValue.MonetaryValue);
		}

		public void AddRange(Collection<CurrencyValue> currencyValues)
		{
			foreach (CurrencyValue currencyValue in currencyValues)
			{
				Add(currencyValue);
			}
		}

		public void AddItem(int currencyId, decimal monetaryValue)
		{
			Add(currencyId, monetaryValue);
		}
	}
}