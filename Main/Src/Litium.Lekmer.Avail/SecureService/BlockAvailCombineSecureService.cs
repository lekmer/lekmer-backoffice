﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Avail.Cache;
using Litium.Lekmer.Avail.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Avail
{
	public class BlockAvailCombineSecureService : IBlockAvailCombineSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockAvailCombineRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockAvailCombineSecureService(
			IAccessValidator accessValidator,
			BlockAvailCombineRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockAvailCombine Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockTopList = IoC.Resolve<IBlockAvailCombine>();
			blockTopList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockTopList.Setting = BlockSettingSecureService.Create();
			blockTopList.Status = BusinessObjectStatus.New;
			return blockTopList;
		}

		public virtual IBlockAvailCombine GetById(int id)
		{
			return BlockAvailCombineCache.Instance.TryGetItem(new BlockAvailCombineKey(id), delegate { return Repository.GetById(id); });
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			IBlockAvailCombine blockRichText = Create();
			blockRichText.ContentNodeId = contentNodeId;
			blockRichText.ContentAreaId = contentAreaId;
			blockRichText.BlockTypeId = blockTypeId;
			blockRichText.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockRichText.Title = title;
			blockRichText.TemplateId = null;
			blockRichText.UseCartPredictions = false;
			blockRichText.UseClickStreamPredictions = false;
			blockRichText.UseLogPurchase = false;
			blockRichText.UsePersonalPredictions = false;
			blockRichText.UseProductSearchPredictions = false;
			blockRichText.UseProductsPredictions = false;
			blockRichText.UseProductsPredictionsFromClicksCategory = false;
			blockRichText.UseProductsPredictionsFromClicksProduct = false;
			blockRichText.Id = Save(systemUserFull, blockRichText);
			return blockRichText;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockAvailCombine block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			// Remove BlockAvailCombine from cache by block id.
			BlockAvailCombineCache.Instance.Remove(new BlockAvailCombineKey(block.Id));

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}

			// Remove BlockAvailCombine from cache by block id.
			BlockAvailCombineCache.Instance.Remove(new BlockAvailCombineKey(blockId));
		}
	}
}