﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Avail.Mapper
{
	public class BlockAvailCombineDataMapper : DataMapperBase<IBlockAvailCombine>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockAvailCombineDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}
		protected override IBlockAvailCombine Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockAvailCombine = IoC.Resolve<IBlockAvailCombine>();
			block.ConvertTo(blockAvailCombine);
			blockAvailCombine.UseClickStreamPredictions = MapValue<bool>("UseClickStreamPredictions");
			blockAvailCombine.UseCartPredictions = MapValue<bool>("UseCartPredictions");
			blockAvailCombine.UseLogPurchase = MapValue<bool>("UseLogPurchase");
			blockAvailCombine.UsePersonalPredictions = MapValue<bool>("UsePersonalPredictions");
			blockAvailCombine.UseProductSearchPredictions = MapValue<bool>("UseProductSearchPredictions");
			blockAvailCombine.UseProductsPredictions = MapValue<bool>("UseProductsPredictions");
			blockAvailCombine.UseProductsPredictionsFromClicksCategory = MapValue<bool>("UseProductsPredictionsFromClicksCategory");
			blockAvailCombine.UseProductsPredictionsFromClicksProduct = MapValue<bool>("UseProductsPredictionsFromClicksProduct");
			blockAvailCombine.UseLogClickedOn = MapValue<bool>("UseLogClickedOn");
			blockAvailCombine.GetClickHistoryFromCookie = MapValue<bool>("GetClickHistoryFromCookie");
			blockAvailCombine.Setting = _blockSettingDataMapper.MapRow();
			blockAvailCombine.Status = BusinessObjectStatus.Untouched;
			return blockAvailCombine;
		}
	}
}

