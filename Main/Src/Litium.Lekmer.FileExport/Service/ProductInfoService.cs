﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.FileExport.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.TopList;
using log4net;

namespace Litium.Lekmer.FileExport.Service
{
	public class ProductInfoService : IProductInfoService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Dictionary<int, Dictionary<int, IBrand>> _brands; // brand_id -> channel_id -> brand
		private IProductTagHash _productTagHash;

		private ICategoryTree _categoryTree;
		protected IContentNodeService ContentNodeService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected IProductImageGroupService ProductImageGroupService { get; private set; }
		protected IBlockTopListService BlockTopListService { get; private set; }
		protected ITopListProductService TopListProductService { get; private set; }
		protected IImageSizeSecureService ImageSizeSecureService { get; private set; }
		protected ITagGroupSecureService TagGroupSecureService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected ITagService TagService { get; private set; }
		protected IProductTagService ProductTagService { get; private set; }
		protected IProductPopularityService ProductPopularityService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected IFileExportSetting FileExportSetting { get; private set; }

		public ProductInfoService(IContentNodeService contentNodeService, IBlockSecureService blockSecureService, ICategoryService categoryService,
			ILekmerProductService lekmerProductService, IBrandService brandService, IProductImageGroupService productImageGroupService,
			ITopListProductService topListProductService, IBlockTopListService blockTopListService, IImageSizeSecureService imageSizeSecureService,
			ITagGroupService tagGroupService, ITagService tagService, IProductTagService productTagService,
			ITagGroupSecureService tagGroupSecureService, IProductPopularityService productPopularityService, IProductSizeService productSizeService,
			IFileExportSetting fileExportSetting)
		{
			ContentNodeService = contentNodeService;
			BlockSecureService = blockSecureService;
			CategoryService = categoryService;
			LekmerProductService = lekmerProductService;
			BrandService = brandService;
			ProductImageGroupService = productImageGroupService;
			TopListProductService = topListProductService;
			BlockTopListService = blockTopListService;
			ImageSizeSecureService = imageSizeSecureService;
			TagGroupSecureService = tagGroupSecureService;
			TagGroupService = tagGroupService;
			TagService = tagService;
			ProductTagService = productTagService;
			ProductPopularityService = productPopularityService;
			ProductSizeService = productSizeService;
			FileExportSetting = fileExportSetting;
		}


		public virtual Collection<IProductExportInfo> GetCompleteProductList(IUserContext context, bool needGetCategoryUrl, bool needImages, bool needSizes)
		{
			_log.InfoFormat("Reading complete product data is started. //{0}", context.Channel.CommonName);

			_categoryTree = CategoryService.GetAllAsTree(context);

			var completeList = new Collection<IProductExportInfo>();

			var pageInfo = IoC.Resolve<IPageTracker>().Initialize(FileExportSetting.PageSize, 1);

			_log.Info("Reading complete product data  - all / all.");

			while (pageInfo.HasNext)
			{
				ProductCollection products = LekmerProductService.GetViewAllForExport(context, pageInfo.Current, pageInfo.PageSize);

				Collection<IProductExportInfo> productsExportInfo = PopulateProductsExportInfo(context, products, needGetCategoryUrl, needImages, needSizes);

				AddToCollection(completeList, productsExportInfo);

				pageInfo.MoveToNext(products.TotalCount);

				_log.InfoFormat("Reading complete product data  - {0} / {1}.", pageInfo.ItemsLeft, pageInfo.ItemsTotalCount);
			}

			_log.InfoFormat("Reading complete product data is done. //{0}", context.Channel.CommonName);

			return completeList;
		}

		public virtual Collection<ITopProductListExportInfo> GetTopProductList(IUserContext context)
		{
			_log.InfoFormat("Reading top product list is started. //{0}", context.Channel.CommonName);

			_categoryTree = CategoryService.GetAllAsTree(context);

			var topProductList = new Collection<ITopProductListExportInfo>();

			IContentNodeTree tree = ContentNodeService.GetAllAsTree(context);
			string topListFolderCommonName = FileExportSetting.TopListFolderCommonName;
			var topListFolder = tree.FindItemByCommonName(topListFolderCommonName);

			if (topListFolder != null)
			{
				_log.InfoFormat("Reading top list folder //{0}", topListFolder.ContentNode.Title);

				bool isFolder = topListFolder.ContentNode.ContentNodeTypeId == (int)ContentNodeTypeInfo.Folder;
				bool isOnline = topListFolder.ContentNode.ContentNodeStatusId != (int)ContentNodeStatusInfo.Offline;

				if (isFolder && isOnline)
				{
					foreach (var topListPage in topListFolder.Children)
					{
						bool isPage = topListPage.ContentNode.ContentNodeTypeId == (int)ContentNodeTypeInfo.Page;
						isOnline = topListPage.ContentNode.ContentNodeStatusId != (int)ContentNodeStatusInfo.Offline;

						_log.InfoFormat("Reading top list page //{0}", topListPage.ContentNode.Title);

						if (isPage && isOnline)
						{
							Collection<IBlock> blocks = BlockSecureService.GetAllByContentNode(topListPage.Id);

							foreach (var block in blocks)
							{
								bool isTopListBlock = block.BlockType.CommonName.Equals(BlockTypeInfo.ProductTopList.ToString());
								if (!isTopListBlock)
								{
									continue;
								}

								_log.InfoFormat("Reading top list block //{0}", block.Title);

								IBlockTopList blockTopList = BlockTopListService.GetById(context, block.Id);

								ProductIdCollection productIdCollection = TopListProductService.GetIdAllByBlock(context, blockTopList, 1, int.MaxValue);
								_log.InfoFormat("Reading top list products //{0}", productIdCollection.Count);
								ProductCollection productCollection = LekmerProductService.PopulateViewProductsForExport(context, productIdCollection);

								Collection<IProductExportInfo> productsExportInfo = PopulateProductsExportInfo(context, productCollection, true, true, true);

								var topProductListExportInfo = IoC.Resolve<ITopProductListExportInfo>();

								topProductListExportInfo.PageTitle = topListPage.ContentNode.Title;
								topProductListExportInfo.BlockTitle = block.Title;
								topProductListExportInfo.Products = productsExportInfo;

								topProductList.Add(topProductListExportInfo);
							}
						}
					}
				}
			}

			_log.InfoFormat("Reading top product list is done. //{0}", context.Channel.CommonName);

			return topProductList;
		}

		public virtual Collection<IProductImageGroup> GetAllowedImageGroups()
		{
			ICollection<IProductImageGroup> allGroups = ProductImageGroupService.GetAll();
			if (allGroups == null)
			{
				return null;
			}

			ICollection<string> groups = FileExportSetting.ImageGroups;
			if (groups == null)
			{
				return null;
			}

			var allowedGroups = new Collection<IProductImageGroup>();
			var allowedGroupsDic = allowedGroups.ToDictionary(s => s.CommonName);
			var allGroupsDic = allGroups.ToDictionary(s => s.CommonName);

			foreach (string groupCommonName in groups)
			{
				if (!allowedGroupsDic.ContainsKey(groupCommonName))
				{
					if (allGroupsDic.ContainsKey(groupCommonName))
					{
						IProductImageGroup imageGroup = allGroupsDic[groupCommonName];
						if (imageGroup != null)
						{
							allowedGroups.Add(imageGroup);
							allowedGroupsDic[groupCommonName] = imageGroup;
						}
					}
				}
			}

			return allowedGroups;
		}

		public virtual Collection<IImageSize> GetAllowedImageSizes()
		{
			ICollection<IImageSize> allImageSizes = ImageSizeSecureService.GetAll();
			if (allImageSizes == null)
			{
				return null;
			}

			ICollection<string> sizes = FileExportSetting.ImageSizes;
			if (sizes == null)
			{
				return null;
			}

			var allowedSizes = new Collection<IImageSize>();
			var allowedSizesDic = allowedSizes.ToDictionary(s => s.CommonName);
			var allImageSizesDic = allImageSizes.ToDictionary(s => s.CommonName);

			foreach (string sizeCommonName in sizes)
			{
				if (!allowedSizesDic.ContainsKey(sizeCommonName))
				{
					if (allImageSizesDic.ContainsKey(sizeCommonName))
					{
						IImageSize imageSize = allImageSizesDic[sizeCommonName];
						if (imageSize != null)
						{
							allowedSizes.Add(imageSize);
							allowedSizesDic[sizeCommonName] = imageSize;
						}
					}
				}
			}

			return allowedSizes;
		}

		public virtual Collection<ITagGroup> GetAllowedTagGroups()
		{
			ICollection<ITagGroup> allTagGroups = TagGroupSecureService.GetAll();
			if (allTagGroups == null)
			{
				return null;
			}

			ICollection<string> tagGroups = FileExportSetting.ColorTagGroup;
			if (tagGroups == null)
			{
				return null;
			}

			var allowedGroups = new Collection<ITagGroup>();
			var allowedGroupsDic = allowedGroups.ToDictionary(s => s.CommonName);
			var allGroupsDic = allTagGroups.ToDictionary(s => s.CommonName);

			foreach (string groupCommonName in tagGroups)
			{
				if (!allowedGroupsDic.ContainsKey(groupCommonName))
				{
					if (allGroupsDic.ContainsKey(groupCommonName))
					{
						ITagGroup tagGroup = allGroupsDic[groupCommonName];
						if (tagGroup != null)
						{
							allowedGroups.Add(tagGroup);
							allowedGroupsDic[groupCommonName] = tagGroup;
						}
					}
				}
			}

			return allowedGroups;
		}

		public virtual Dictionary<string, string> GetAllowedCategoryLevelUrls()
		{
			ICollection<string> categoryLevelUrls = FileExportSetting.CategoryLevelUrl;
			if (categoryLevelUrls == null)
			{
				return null;
			}

			return categoryLevelUrls.ToDictionary(c => c);
		}

		public virtual void SupplementWithPopularity(IUserContext context, Collection<IProductExportInfo> productExportInfoCollection)
		{
			Collection<IProductPopularity> productPopularities = ProductPopularityService.GetAll(context);

			if (productPopularities == null) return;

			Dictionary<int, IProductPopularity> productPopularityDictionary = productPopularities.ToDictionary(p => p.ProductId);

			foreach (IProductExportInfo productExportInfo in productExportInfoCollection)
			{
				if (productPopularityDictionary.ContainsKey(productExportInfo.Product.Id))
				{
					IProductPopularity productPopularity = productPopularityDictionary[productExportInfo.Product.Id];
					productExportInfo.Popularity = productPopularity.Popularity;
					productExportInfo.SalesAmount = productPopularity.SalesAmount;
				}
			}
		}


		protected virtual Collection<IProductExportInfo> PopulateProductsExportInfo(IUserContext context, ProductCollection productsBatch, bool needGetCategoryUrl, bool needImages, bool needSizes)
		{
			var completeList = new Collection<IProductExportInfo>();

			PreloadAllBrands(context);
			PreloadAllTags(context);

			foreach (ILekmerProductView product in productsBatch)
			{
				var productExportInfo = IoC.Resolve<IProductExportInfo>();

				productExportInfo.Product = product;

				// Brand
				SupplementWithBrandInfo(context, productExportInfo);

				// Categories
				SupplementWithCategoryInfo(context, productExportInfo, needGetCategoryUrl);

				// Images
				if (needImages)
				{
					productExportInfo.ImageGroups = ProductImageGroupService.GetAllFullByProduct(product.Id);
				}

				// Tags
				SupplementWithTagsInfo(context, product);

				completeList.Add(productExportInfo);
			}

			//Sizes
			if (needSizes)
			{
				SupplementWithSizeInfo(productsBatch);
			}

			return completeList;
		}


		protected virtual void SupplementWithBrandInfo(IUserContext userContext, IProductExportInfo productExportInfo)
		{
			if (productExportInfo.Product.BrandId.HasValue)
			{
				productExportInfo.Product.Brand = FindBrand(userContext.Channel.Id, productExportInfo.Product.BrandId.Value);
				productExportInfo.BrandUrl = GetBrandUrl(userContext, productExportInfo);
			}
		}

		protected virtual void SupplementWithCategoryInfo(IUserContext context, IProductExportInfo productExportInfo, bool needGetCategoryUrl)
		{
			ICategoryTreeItem categoryTreeItem = _categoryTree.FindItemById(productExportInfo.Product.CategoryId);
			if (categoryTreeItem != null)
			{
				productExportInfo.ThirdCategoryLevelUrl = GetCategoryUrl(context, categoryTreeItem, needGetCategoryUrl, ResolveProductUrl(context, productExportInfo.Product.LekmerUrl));

				ICategory category = categoryTreeItem.Category;
				if (category != null)
				{
					productExportInfo.CategoryTitle = category.Title;
				}

				categoryTreeItem = categoryTreeItem.Parent;
				if (categoryTreeItem != null)
				{
					productExportInfo.SecondCategoryLevelUrl = GetCategoryUrl(context, categoryTreeItem, needGetCategoryUrl, productExportInfo.ThirdCategoryLevelUrl);

					ICategory parentCategory = categoryTreeItem.Category;
					if (parentCategory != null)
					{
						productExportInfo.ParentCategoryTitle = parentCategory.Title;
					}

					categoryTreeItem = categoryTreeItem.Parent;
					if (categoryTreeItem != null)
					{
						productExportInfo.FirstCategoryLevelUrl = GetCategoryUrl(context, categoryTreeItem, needGetCategoryUrl, productExportInfo.SecondCategoryLevelUrl);

						ICategory mainCategory = categoryTreeItem.Category;
						if (mainCategory != null)
						{
							productExportInfo.MainCategoryTitle = mainCategory.Title;
						}
					}
				}
			}
		}

		protected virtual void SupplementWithTagsInfo(IUserContext userContext, ILekmerProductView product)
		{
			product.TagGroups = _productTagHash.GetProductTags(userContext.Channel.Id, product.Id);
		}

		protected virtual void SupplementWithSizeInfo(ProductCollection products)
		{
			var productIds = new ProductIdCollection(products.Select(p => p.Id).ToArray());
			Collection<IProductSize> productSizes = ProductSizeService.GetAllByProductIdList(productIds);
			Dictionary<int, Collection<IProductSize>> productSizesByProduct = productSizes.GroupBy(ps => ps.ProductId).ToDictionary(ps => ps.Key, ps => new Collection<IProductSize>(ps.ToArray()));

			foreach (ILekmerProductView product in products)
			{
				if (productSizesByProduct.ContainsKey(product.Id))
				{
					Collection<IProductSize> sizes = productSizesByProduct[product.Id];

					product.ProductSizes = sizes;
				}
			}
		}


		protected virtual void PreloadAllBrands(IUserContext userContext)
		{
			if (_brands == null)
			{
				_brands = new Dictionary<int, Dictionary<int, IBrand>>();
			}

			if (_brands.ContainsKey(userContext.Channel.Id))
			{
				return;
			}

			BrandCollection brandCollection = BrandService.GetAll(userContext);

			var channelBrands = new Dictionary<int, IBrand>();
			_brands[userContext.Channel.Id] = channelBrands;

			foreach (IBrand brand in brandCollection)
			{
				channelBrands[brand.Id] = brand;
			}
		}

		protected virtual void PreloadAllTags(IUserContext userContext)
		{
			if (_productTagHash == null)
			{
				_productTagHash = IoC.Resolve<IProductTagHash>();
			}

			if (!_productTagHash.ContainProductTags())
			{
				Collection<IProductTag> productTags = ProductTagService.GetAll();
				_productTagHash.Add(productTags);
			}

			if (!_productTagHash.ContainChannel(userContext.Channel.Id))
			{
				Collection<ITagGroup> tagGroups = TagGroupService.GetAll(userContext);
				_productTagHash.Add(userContext.Channel.Id, tagGroups);
			}
		}


		protected virtual IBrand FindBrand(int channelId, int brandId)
		{
			if (_brands.ContainsKey(channelId))
			{
				if (_brands[channelId].ContainsKey(brandId))
				{
					return _brands[channelId][brandId];
				}
			}

			return null;
		}


		protected virtual void AddToCollection(Collection<IProductExportInfo> completeList, Collection<IProductExportInfo> productsToAdd)
		{
			foreach (IProductExportInfo product in productsToAdd)
			{
				completeList.Add(product);
			}
		}

		protected virtual string GetCategoryUrl(IUserContext context, ICategoryTreeItem categoryTreeItem, bool needGetCategoryUrl, string previousObjectUrl)
		{
			if (!needGetCategoryUrl)
			{
				return null;
			}

			int? contentNodeId = GetCategoryContentNodeId(context, categoryTreeItem);

			if (contentNodeId.HasValue)
			{
				string categoryUrl = GetContentNodeUrl(context, contentNodeId.Value);
				if (categoryUrl != null)
				{
					return ResolveUrlHttp(context, categoryUrl);
				}
			}

			if (string.IsNullOrEmpty(previousObjectUrl))
			{
				return string.Empty;
			}

			previousObjectUrl = RemoveLastSlash(previousObjectUrl);
			string appUrl = RemoveLastSlash(GetApplicationUrl(context));

			if (previousObjectUrl == appUrl)
			{
				return string.Empty;
			}

			string url = string.Empty;

			int position = previousObjectUrl.LastIndexOf('/');
			if (position > 0)
			{
				url = previousObjectUrl.Substring(0, position + 1);
			}

			return url;
		}

		protected virtual int? GetCategoryContentNodeId(IUserContext context, ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null || categoryTreeItem.Category == null)
			{
				return null;
			}

			if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
			{
				return categoryTreeItem.Category.ProductParentContentNodeId.Value;
			}

			foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
			{
				if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
				{
					return parentItem.Category.ProductParentContentNodeId.Value;
				}
			}

			return null;
		}

		protected virtual string GetBrandUrl(IUserContext context, IProductExportInfo productExportInfo)
		{
			if (productExportInfo.Product.Brand != null)
			{
				if (productExportInfo.Product.Brand.ContentNodeId.HasValue)
				{
					string brandUrl = GetContentNodeUrl(context, productExportInfo.Product.Brand.ContentNodeId.Value);
					if (brandUrl != null)
					{
						return ResolveUrlHttp(context, brandUrl);
					}
				}
			}

			return string.Empty;
		}

		protected virtual string GetContentNodeUrl(IUserContext context, int contentNodeId)
		{
			var treeItem = ContentNodeService.GetTreeItemById(context, contentNodeId);
			if (treeItem == null || treeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return treeItem.Url;
		}

		protected virtual string ResolveUrlHttp(IUserContext context, string relativeUrl)
		{
			if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

			if (!relativeUrl.StartsWith("~/", StringComparison.Ordinal))
			{
				return relativeUrl;
			}

			return GetApplicationUrl(context) + relativeUrl.Substring(1);
		}

		protected virtual string ResolveProductUrl(IUserContext context, string productUrl)
		{
			return GetApplicationUrl(context) + '/' + productUrl;
		}

		protected virtual string GetApplicationUrl(IUserContext context)
		{
			return "http://" + context.Channel.ApplicationName;
		}

		protected virtual string RemoveLastSlash(string url)
		{
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.Substring(0, url.Length - 1);
			}

			return url;
		}
	}
}