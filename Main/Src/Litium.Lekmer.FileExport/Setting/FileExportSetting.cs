﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.FileExport.Setting
{
	public class FileExportSetting : SettingBase, IFileExportSetting
	{
		protected override string StorageName
		{
			get { return "FileExport"; }
		}

		protected virtual string GroupName
		{
			get { return "FileExportSettings"; }
		}


		public string ApplicationName
		{
			get { return GetString(GroupName, "ApplicationName"); }
		}

		public int PageSize
		{
			get { return GetInt32(GroupName, "PageSize", 100); }
		}

		public string TopListFolderCommonName
		{
			get { return GetString(GroupName, "TopListFolderCommonName"); }
		}


		public string DestinationDirectoryProduct
		{
			get { return GetString(GroupName, "DestinationDirectoryProduct"); }
		}

		public string DestinationDirectorySmartly
		{
			get { return GetString(GroupName, "DestinationDirectorySmartly"); }
		}

		public string DestinationDirectorySitemap
		{
			get { return GetString(GroupName, "DestinationDirectorySitemap"); }
		}

		public string DestinationDirectoryTopList
		{
			get { return GetString(GroupName, "DestinationDirectoryTopList"); }
		}


		public string XmlFileNameProductExporter
		{
			get { return GetString(GroupName, "XmlFileNameProductExporter"); }
		}

		public string XmlFileNameSmartlyExporter
		{
			get { return GetString(GroupName, "XmlFileNameSmartlyExporter"); }
		}

		public string XmlFileNameSitemapExporter
		{
			get { return GetString(GroupName, "XmlFileNameSitemapExporter"); }
		}

		public string XmlFileNameTopListExporter
		{
			get { return GetString(GroupName, "XmlFileNameTopListExporter"); }
		}

		public string XmlFileNameKeybrokerProductExporter
		{
			get { return GetString(GroupName, "XmlFileNameKeybrokerProductExporter"); }
		}

		public string XmlFileNameMarinProductExporter
		{
			get { return GetString(GroupName, "XmlFileNameMarinProductExporter"); }
		}

		public string CsvFileNameMarinProductExporter
		{
			get { return GetString(GroupName, "CsvFileNameMarinProductExporter"); }
		}

		public string KeybrokerXmlSchemaDefinition
		{
			get { return GetString(GroupName, "KeybrokerXmlSchemaDefinition"); }
		}


		public virtual string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}

		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);

			if (dictionary.ContainsKey(channelName))
			{
				return dictionary[channelName];
			}

			return channelName;
		}

		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);

			if (dictionary.ContainsKey(channelName))
			{
				return true;
			}

			return false;
		}

		public virtual string ImageGroupsRaw
		{
			get { return GetString(GroupName, "ImageGroups"); }
		}

		public virtual string ImageSizesRaw
		{
			get { return GetString(GroupName, "ImageSizes"); }
		}

		public virtual string ColorTagGroupRaw
		{
			get { return GetString(GroupName, "ColorTagGroup"); }
		}

		public virtual string CategoryLevelUrlRaw
		{
			get { return GetString(GroupName, "CategoryLevelUrl"); }
		}

		public ICollection<string> ImageGroups
		{
			get { return ConvertToList(ImageGroupsRaw); }
		}

		public ICollection<string> ImageSizes
		{
			get { return ConvertToList(ImageSizesRaw); }
		}

		public ICollection<string> ColorTagGroup
		{
			get { return ConvertToList(ColorTagGroupRaw); }
		}

		public ICollection<string> CategoryLevelUrl
		{
			get { return ConvertToList(CategoryLevelUrlRaw); }
		}

		// Marin CSV file settings
		public bool IsExportXml
		{
			get { return GetBoolean(GroupName, "IsExportXml", false); }
		}

		public bool IsExportCsv
		{
			get { return GetBoolean(GroupName, "IsExportCsv", false); }
		}

		public string CsvFileDelimiter
		{
			get { return GetString(GroupName, "CsvFileDelimiter"); }
		}

		public string MarinFtpUrl
		{
			get { return GetString(GroupName, "MarinFtpUrl"); }
		}

		public string MarinFtpUsername
		{
			get { return GetString(GroupName, "MarinFtpUsername"); }
		}

		public string MarinFtpPassword
		{
			get { return GetString(GroupName, "MarinFtpPassword"); }
		}

		public int FileLifeCycleInDays
		{
			get { return GetInt32(GroupName, "FileLifeCycleInDays"); }
		}

		public string FileToDeleteMask
		{
			get { return GetString(GroupName, "FileToDeleteMask"); }
		}

		public virtual string CsvFileImageGroupsRaw
		{
			get { return GetString(GroupName, "CsvFileImageGroups"); }
		}

		public ICollection<string> CsvFileImageGroups
		{
			get { return ConvertToList(ImageGroupsRaw); }
		}

		public virtual string CsvFileImageSizesRaw
		{
			get { return GetString(GroupName, "CsvFileImageSizes"); }
		}

		public ICollection<string> CsvFileImageSizes
		{
			get { return ConvertToList(ImageSizesRaw); }
		}


		// value1,value2, ... , valueN => List<value>
		protected virtual ICollection<string> ConvertToList(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
	}
}