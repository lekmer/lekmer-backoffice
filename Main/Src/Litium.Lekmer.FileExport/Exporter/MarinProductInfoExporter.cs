﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Lekmer.FileExport.CsvFile;
using Litium.Lekmer.FileExport.Service;
using Litium.Lekmer.FileExport.Setting;
using Litium.Lekmer.FileExport.XmlFile;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.FileExport.Exporter
{
	public class MarinProductInfoExporter : IExporter
	{
		private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IEnumerable<IUserContext> _userContexts;
		private Collection<ICampaign> _campaigns;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected IProductInfoService ProductInfoService { get; private set; }
		protected IFileExportSetting FileExportSetting { get; private set; }
		protected ICampaignSecureService CampaignSecureService { get; private set; }

		public MarinProductInfoExporter(ILekmerChannelService lekmerChannelService, IProductInfoService productInfoService, IFileExportSetting fileExportSetting, ICampaignSecureService campaignSecureService)
		{
			LekmerChannelService = lekmerChannelService;
			ProductInfoService = productInfoService;
			FileExportSetting = fileExportSetting;
			CampaignSecureService = campaignSecureService;
		}

		public void Execute()
		{
			if (!FileExportSetting.ApplicationName.Equals(ExporterConstants.LekmerApplication))
			{
				return;
			}

			Log.Info("Execution is started.");

			_userContexts = GetUserContextForAllChannels();
			_campaigns = CampaignSecureService.GetAllProductCampaigns();

			ExecuteMarinProductList();
			RemoveOldFiles();

			Log.Info("Executed.");
		}

		public virtual void ExecuteMarinProductList()
		{
			Log.Info("Complete product list is started.");

			var allowedImageGroups = ProductInfoService.GetAllowedImageGroups();
			var allowedImageSizes = ProductInfoService.GetAllowedImageSizes();
			var allowedTagGroups = ProductInfoService.GetAllowedTagGroups();
			var allowedCategoryLevelUrls = ProductInfoService.GetAllowedCategoryLevelUrls();

			foreach (IUserContext context in _userContexts)
			{
				if (!FileExportSetting.GetChannelNameReplacementExists(context.Channel.CommonName))
				{
					Log.Info("Skipping channel: " + context.Channel.CommonName);
					continue;
				}

				Stopwatch stopwatch = Stopwatch.StartNew();

				var products = ProductInfoService.GetCompleteProductList(context, true, false, true);

				// Save XML
				if (FileExportSetting.IsExportXml)
				{
					string xmlFilePath = GetFilePathForMarinProductList(context, FileExportSetting.XmlFileNameMarinProductExporter);

					Log.InfoFormat("Complete product list - xml file saving... //{0}", xmlFilePath);

					if (File.Exists(xmlFilePath))
					{
						File.Delete(xmlFilePath);
					}

					using (Stream stream = File.OpenWrite(xmlFilePath))
					{
						var marinProductInfoXmlFile = new MarinProductInfoXmlFile();
						marinProductInfoXmlFile.Initialize(allowedTagGroups, _campaigns, allowedCategoryLevelUrls);
						marinProductInfoXmlFile.InitializeImages(allowedImageGroups, allowedImageSizes);
						marinProductInfoXmlFile.Save(stream, context, products);
					}
				}

				// Save CSV
				if (FileExportSetting.IsExportCsv)
				{
					string fileName = string.Format(FileExportSetting.CsvFileNameMarinProductExporter, DateTime.Now.Date.ToString("yyyyMMdd"));
					string csvFilePath = GetFilePathForMarinProductList(context, fileName);
					Log.InfoFormat("Complete product list - csv file saving... //{0}", csvFilePath);

					if (File.Exists(csvFilePath))
					{
						File.Delete(csvFilePath);
					}

					using (Stream stream = File.OpenWrite(csvFilePath))
					{
						var marinProductInfoCsvFile = new MarinProductInfoCsvFile();
						marinProductInfoCsvFile.Initialize(allowedTagGroups, _campaigns, allowedCategoryLevelUrls, FileExportSetting.CsvFileDelimiter);
						marinProductInfoCsvFile.InitializeImages(allowedImageGroups, allowedImageSizes);
						marinProductInfoCsvFile.Save(stream, context, products);
					}

					Log.InfoFormat("File saved... //{0}", csvFilePath);

					Log.Info("File start uploaded to FTP...");
					UploadToFtp(context.Channel.CommonName, csvFilePath);
				}

				stopwatch.Stop();
				Log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}

			Log.Info("Complete product list is completed.");
		}

		protected virtual void RemoveOldFiles()
		{
			var dir = FileExportSetting.DestinationDirectoryProduct;
			var files = Directory.GetFiles(dir, FileExportSetting.FileToDeleteMask, SearchOption.TopDirectoryOnly).ToList();
			foreach (var file in files)
			{
				var currentTime = DateTime.Now;
				var creationTime = File.GetCreationTime(file);
				if (creationTime < currentTime.AddDays(FileExportSetting.FileLifeCycleInDays * -1))
				{
					if (File.Exists(file))
					{
						try
						{
							File.Delete(file);
						}
						catch (Exception)
						{
							Log.ErrorFormat("Cannot delete file {0}", file);
						}
					}
				}
			}
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}

		protected virtual string GetFilePathForMarinProductList(IUserContext context, string fileName)
		{
			string dirr = FileExportSetting.DestinationDirectoryProduct;

			string channelName = FileExportSetting.GetChannelNameReplacement(context.Channel.CommonName);
			fileName = fileName.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, fileName);
		}

		protected virtual void UploadToFtp(string channelCommonName, string filePath)
		{
			try
			{
				string channelName = FileExportSetting.GetChannelNameReplacement(channelCommonName);

				string fileName = string.Format(FileExportSetting.CsvFileNameMarinProductExporter, DateTime.Now.Date.ToString("yyyyMMdd"));
				fileName = fileName.Replace("[ChannelName]", channelName);

				var ftpFileUrl = string.Format(FileExportSetting.MarinFtpUrl, channelName, fileName);
				var username = FileExportSetting.MarinFtpUsername;
				var password = FileExportSetting.MarinFtpPassword;

				var fileInfo = new FileInfo(filePath);
				var fileContents = new byte[fileInfo.Length];

				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpFileUrl));
				ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
				ftpRequest.Proxy = null;
				ftpRequest.UseBinary = true;
				ftpRequest.KeepAlive = false;
				ftpRequest.Credentials = new NetworkCredential(username, password);
				ftpRequest.ContentLength = fileContents.Length;
				ftpRequest.Timeout = 1000000;
				ftpRequest.ReadWriteTimeout = 1000000;

				using (FileStream stream = fileInfo.OpenRead())
				{
					stream.Read(fileContents, 0, System.Convert.ToInt32(fileInfo.Length));
				}

				using (Stream writer = ftpRequest.GetRequestStream())
				{
					writer.Write(fileContents, 0, fileContents.Length);
				}
			}
			catch (WebException webex)
			{
				Log.Error(webex);
			}
		}
	}
}