using System;

namespace Litium.Lekmer.FileExport
{
	[Serializable]
	public class SitemapItem : ISitemapItem
	{
		public string Url { get; set; }
		public decimal Priority { get; set; }
		public string ChangeFreq { get; set; }
		public DateTime? LastMod { get; set; }
	}
}