using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.FileExport
{
	[Serializable]
	public class TopProductListExportInfo : ITopProductListExportInfo
	{
		public string PageTitle { get; set; }
		public string BlockTitle { get; set; }
		public Collection<IProductExportInfo> Products { get; set; }
	}
}