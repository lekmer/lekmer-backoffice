﻿using System;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter
{
	public class FragmentVariableInfo
	{
		private readonly string _fragmentContent;

		public FragmentVariableInfo(Fragment fragmentItem)
		{
			if (fragmentItem == null)
			{
				throw new ArgumentNullException("fragmentItem");
			}

			_fragmentContent = fragmentItem.Render();
		}

		public virtual bool HasVariable(string variableName)
		{
			return _fragmentContent.Contains("[" + variableName + "]");
		}
	}
}
