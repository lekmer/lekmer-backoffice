﻿using System;
using System.Collections.Generic;
using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Campaign.CampaignTagging.Service
{
	public class CampaignTaggingJob : BaseJob
	{
		private ILekmerProductCampaignService _productCampaignService;
		private IProductCampaignService _campaignService;
		private ITagService _tagService;
		private ILekmerPercentagePriceDiscountActionService _percentagePriceDiscountService;
		private IProductDiscountActionService _productDiscountService;
		private IFixedDiscountActionService _fixedDiscountService;
		private IGiftCardViaMailProductActionService _giftCardViaMailProductService;
		private IFixedPriceActionService _fixedPriceService;

		private Dictionary<int, int> _campaignProductIds; 

		public ILog Log { get; set; }

		public override string Name
		{
			get { return "CampaignTagging"; }
		}

		protected void Initialize()
		{
			_productCampaignService = (ILekmerProductCampaignService) IoC.Resolve<IProductCampaignService>();
			_campaignService = IoC.Resolve<IProductCampaignService>();
			_tagService = IoC.Resolve<ITagService>();
			_percentagePriceDiscountService = (ILekmerPercentagePriceDiscountActionService)IoC.Resolve<IProductActionPluginService>("PercentagePriceDiscount");
			_productDiscountService = (IProductDiscountActionService) IoC.Resolve<IProductActionPluginService>("ProductDiscount");
			_fixedDiscountService = (IFixedDiscountActionService) IoC.Resolve<IProductActionPluginService>("FixedDiscount");
			_giftCardViaMailProductService = (IGiftCardViaMailProductActionService)IoC.Resolve<IProductActionPluginService>("GiftCardViaMailProduct");
			_fixedPriceService = (IFixedPriceActionService) IoC.Resolve<IProductActionPluginService>("FixedPrice");
		}

		protected override void ExecuteAction()
		{
			Initialize();

			var campaignToTag = _productCampaignService.CampaignToTagGet();

			if(campaignToTag == null) return;
			if(campaignToTag.ProcessingDate != null && campaignToTag.ProcessingDate > DateTime.Now) return;

			var campaignId = campaignToTag.CampaignId;

			// Delete all tags and marks by Campaign ID.
			var productTagsForDeleting = _tagService.GetAllProductTagIdsByCampaign(campaignId);
			_tagService.DeleteProductTagsUsage(campaignId);
			_tagService.DeleteProductTagsByIds(productTagsForDeleting);

			var currentDate = DateTime.Now;
			var userContext = GetUserContext();

			var productCampaign = _campaignService.GetById(userContext, campaignId);
			var lekmerCampaign = productCampaign != null ? (ILekmerCampaign) productCampaign : null;
			if (productCampaign != null
				&& productCampaign.CampaignStatus.CommonName == "Online"
				&& lekmerCampaign.TagId.HasValue
				&& ((productCampaign.StartDate == null || productCampaign.StartDate < currentDate) && (productCampaign.EndDate == null || productCampaign.EndDate > currentDate)))
			{
				// Insert tags.
				GetCampaignProductIds(productCampaign.Actions);
				var tagId = lekmerCampaign.TagId.Value;
				foreach (var productId in _campaignProductIds.Keys)
				{
					_tagService.SaveProductTag(productId, tagId, campaignId);
				}
			}

			_productCampaignService.CampaignToTagDelete(campaignToTag.Id);
		}

		private IUserContext GetUserContext()
		{
			var channel = IoC.Resolve<IChannel>();
			channel.Id = 1;

			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = channel;

			return userContext;
		}

		private void GetCampaignProductIds(IEnumerable<IProductAction> actions)
		{
			_campaignProductIds = new Dictionary<int, int>();

			foreach (var action in actions)
			{
				if (action is ILekmerPercentagePriceDiscountAction)
				{
					AddProductIds(_percentagePriceDiscountService.GetProductIds(action.Id));
					continue;
				}
				if (action is IProductDiscountAction)
				{
					AddProductIds(_productDiscountService.GetProductIds(action.Id));
					continue;
				}
				if (action is IFixedDiscountAction)
				{
					AddProductIds(_fixedDiscountService.GetProductIds(action.Id));
					continue;
				}
				if (action is IGiftCardViaMailProductAction)
				{
					AddProductIds(_giftCardViaMailProductService.GetProductIds(action.Id));
					continue;
				}
				if (action is IFixedPriceAction)
				{
					AddProductIds(_fixedPriceService.GetProductIds(action.Id));
					continue;
				}
			}
		}

		private void AddProductIds(IEnumerable<int> productIds)
		{
			foreach (var productId in productIds)
			{
				if (!_campaignProductIds.ContainsKey(productId))
				{
					_campaignProductIds.Add(productId, productId);
				}
			}
		}
	}
}