﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Campaign.CampaignTagging.Service
{
	public class CampaignTaggingScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "CampaignTaggingService"; }
		}

		protected override string GroupName
		{
			get { return "CampaignTaggingJob"; }
		}
	}
}