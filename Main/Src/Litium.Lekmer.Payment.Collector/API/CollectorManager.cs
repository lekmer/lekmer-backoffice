﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using log4net;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorManager
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private bool _isFinished;
		private readonly object _thisLock = new object();
		protected CollectorAddInvoiceResponse AddInvoiceResponse { get; set; }
		protected ICollectorTransactionService CollectorTransactionService { get; private set; }
		public CollectorAddInvoiceRequest AddInvoiceRequest { get; set; }


		public CollectorManager(ICollectorTransactionService collectorTransactionService)
		{
			CollectorTransactionService = collectorTransactionService;
		}

		public virtual CollectorAddInvoiceResponse ExecuteAddInvoice()
		{
			AddInvoiceResponse = new CollectorAddInvoiceResponse();

			var thread = new Thread(AddInvoice)
				{
					Name = string.Format("Collector_Order_{0}", AddInvoiceRequest.Request.OrderNo)
				};

			thread.Start();
			thread.Join(AddInvoiceRequest.CollectorSetting.Customer7SecResponseTimeout);

			CollectorAddInvoiceResponse result;

			lock (_thisLock)
			{
				result = AddInvoiceResponse.IsCompleated ? AddInvoiceResponse : null;
				_isFinished = true;
			}

			return result;
		}

		private void AddInvoice()
		{
			var duration = new Stopwatch();
			duration.Start();

			var addInvoiceResponse = new CollectorAddInvoiceResponse();

			try
			{
				((InvoiceServiceV31Client)AddInvoiceRequest.InvoiceServiceClient).InnerChannel.OperationTimeout = TimeSpan.FromMilliseconds(AddInvoiceRequest.CollectorSetting.Service7SecResponseTimeout);
				addInvoiceResponse.Response = AddInvoiceRequest.InvoiceServiceClient.AddInvoice(AddInvoiceRequest.Request);
			}
			catch (Exception ex)
			{
				addInvoiceResponse.AddInvoiceException = ex;
			}
			duration.Stop();



			lock (_thisLock)
			{
				AddInvoiceResponse.Response = addInvoiceResponse.Response;
				AddInvoiceResponse.AddInvoiceException = addInvoiceResponse.AddInvoiceException;
				AddInvoiceResponse.IsCompleated = true;
			}

			bool isFinished;

			lock (_thisLock)
			{
				isFinished = _isFinished;
			}

			if (isFinished)
			{
				try
				{
					SaveResponseResults(duration);
				}
				catch (Exception exception)
				{
					_log.FatalFormat("FATAL ERROR OCCURED. {0}", exception);
				}
			}
		}

		private void SaveResponseResults(Stopwatch duration)
		{
			int transactionId = CollectorTransactionService.CreateAddInvoiceTimeout(
				AddInvoiceRequest.CollectorSetting.StoreId,
				AddInvoiceRequest.Request.RegNo,
				AddInvoiceRequest.OrderId,
				AddInvoiceRequest.ProductCode
			);

			ICollectorAddInvoiceResult collectorResult;
			if (AddInvoiceResponse.Response != null)
			{
				var addInvoiceResponse = AddInvoiceResponse.Response;
				collectorResult = new CollectorAddInvoiceResult
					{
						StatusCode = (int)CollectorTransactionStatus.Ok,
						AvailableReservationAmount = addInvoiceResponse.AvailableReservationAmount,
						InvoiceNo = addInvoiceResponse.InvoiceNo,
						InvoiceStatus = addInvoiceResponse.InvoiceStatus,
						InvoiceUrl = addInvoiceResponse.InvoiceUrl,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};
			}
			else
			{
				var statusCode = (int)CollectorTransactionStatus.UnspecifiedError;
				string faultCode = null;
				string faultMessage = null;

				var exception = AddInvoiceResponse.AddInvoiceException;

				if (exception is TimeoutException)
				{
					statusCode = (int) CollectorTransactionStatus.TimeoutResponse;
				}
				else if (exception is FaultException)
				{
					var faultException = (FaultException)exception;

					faultCode = faultException.Code.Name;
					faultMessage = faultException.Message;

					// SOAP fault
					statusCode = (int)CollectorTransactionStatus.SoapFault;
				}

				collectorResult = new CollectorAddInvoiceResult
				{
					StatusCode = statusCode,
					FaultCode = faultCode,
					FaultMessage = faultMessage,
					ErrorMessage = exception.ToString(),
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};
			}

			CollectorTransactionService.SaveAddInvoiceResponse(collectorResult);
		}
	}
}