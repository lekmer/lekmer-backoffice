﻿using Litium.Lekmer.Payment.Collector.Setting;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector
{
	public static class CollectorFactory
	{
		public static ICollectorSetting CreateCollectorSetting()
		{
			return CreateCollectorSetting(Channel.Current.CommonName);
		}

		public static ICollectorSetting CreateCollectorSetting(string channelCommonName)
		{
			var collectorSetting = IoC.Resolve<ICollectorSetting>();

			collectorSetting.Initialize(channelCommonName);

			return collectorSetting;
		}

		public static ICollectorClient CreateCollectorClient(string channelCommonName)
		{
			var collectorClient = IoC.Resolve<ICollectorClient>();

			collectorClient.CollectorSetting = CreateCollectorSetting(channelCommonName);
			collectorClient.Initialize(channelCommonName);

			return collectorClient;
		}
	}
}