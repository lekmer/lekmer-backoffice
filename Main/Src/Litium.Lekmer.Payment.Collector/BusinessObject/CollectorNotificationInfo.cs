﻿namespace Litium.Lekmer.Payment.Collector
{
	/// <summary>
	/// http://www.some-url.com/?InvoiceNo=XXXXOrderNo=XXXX&InvoiceStatus=X.
	/// </summary>
	public class CollectorNotificationInfo : ICollectorNotificationInfo
	{
		public string InvoiceNo { get; set; }
		public string OrderNoRaw { get; set; }
		public string InvoiceStatusRaw { get; set; }
		public int OrderNo { get; set; }
		public int InvoiceStatus { get; set; }
	}
}
