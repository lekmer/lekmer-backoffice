﻿using System;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorPendingOrder : ICollectorPendingOrder
	{
		public int Id { get; set; }
		public int ChannelId { get; set; }
		public int OrderId { get; set; }
		public DateTime FirstAttempt { get; set; }
		public DateTime LastAttempt { get; set; }
	}
}
