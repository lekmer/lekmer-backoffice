using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector.Mapper
{
	public class CollectorPaymentTypeDataMapper : DataMapperBase<ICollectorPaymentType>
	{
		public CollectorPaymentTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICollectorPaymentType Create()
		{
			var paymentType = IoC.Resolve<ICollectorPaymentType>();

			paymentType.Id = MapValue<int>("CollectorPaymentType.Id");
			paymentType.ChannelId = MapValue<int>("CollectorPaymentType.ChannelId");
			paymentType.Code = MapValue<string>("CollectorPaymentType.Code");
			paymentType.Description = MapNullableValue<string>("CollectorPaymentType.Description");

			paymentType.StartFee = MapValue<decimal>("CollectorPaymentType.StartFee");
			paymentType.InvoiceFee = MapValue<decimal>("CollectorPaymentType.InvoiceFee");
			paymentType.MonthlyInterestRate = MapValue<decimal>("CollectorPaymentType.MonthlyInterestRate");

			paymentType.NoOfMonths = MapValue<int>("CollectorPaymentType.NoOfMonths");
			paymentType.MinPurchaseAmount = MapValue<decimal>("CollectorPaymentType.MinPurchaseAmount");
			paymentType.@LowestPayment = MapValue<decimal>("CollectorPaymentType.LowestPayment");

			paymentType.PartPaymentType = (CollectorPartPaymentType)MapValue<int>("CollectorPaymentType.PartPaymentType");
			paymentType.InvoiceType = (CollectorInvoiceType)MapValue<int>("CollectorPaymentType.InvoiceType");

			paymentType.Ordinal = MapValue<int>("CollectorPaymentType.Ordinal");

			return paymentType;
		}
	}
}