using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector.Mapper
{
	public class CollectorPendingOrderDataMapper : DataMapperBase<ICollectorPendingOrder>
	{
		public CollectorPendingOrderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICollectorPendingOrder Create()
		{
			var klarnaPendingOrder = IoC.Resolve<ICollectorPendingOrder>();

			klarnaPendingOrder.Id = MapValue<int>("CollectorPendingOrder.CollectorPendingOrderId");
			klarnaPendingOrder.ChannelId = MapValue<int>("CollectorPendingOrder.ChannelId");
			klarnaPendingOrder.OrderId = MapValue<int>("CollectorPendingOrder.OrderId");
			klarnaPendingOrder.FirstAttempt = MapValue<DateTime>("CollectorPendingOrder.FirstAttempt");
			klarnaPendingOrder.LastAttempt = MapValue<DateTime>("CollectorPendingOrder.LastAttempt");

			return klarnaPendingOrder;
		}
	}
}