﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Collector
{
	public class OrderPackageNiceConverter : IOrderItemConverter
	{
		public virtual Collection<InvoiceRow> Convert(IOrderItem orderItem, bool taxFreeZone)
		{
			var lekmerOrderItem = (ILekmerOrderItem)orderItem;

			if (lekmerOrderItem.PackageOrderItems == null)
			{
				throw new InvalidOperationException("lekmerOrderItem.PackageOrderItems can't be null when it is package");
			}

			var invoiceRows = new Collection<InvoiceRow>();

			string rowDescription = ParameterConverter.FormatParameter(lekmerOrderItem.Title, 50);
			decimal vatPercent = ParameterConverter.FormatParameter(lekmerOrderItem.GetOriginalVatPercent(), 0);
			decimal packageDiscountIncludingVat = lekmerOrderItem.ActualPrice.IncludingVat;
			decimal packageDiscountExcludingVat = lekmerOrderItem.ActualPrice.ExcludingVat;

			foreach (IPackageOrderItem packageOrderItem in lekmerOrderItem.PackageOrderItems)
			{
				AddPackageItem(invoiceRows, packageOrderItem, taxFreeZone);
				packageDiscountIncludingVat -= packageOrderItem.OriginalPrice.IncludingVat;
				packageDiscountExcludingVat -= packageOrderItem.OriginalPrice.ExcludingVat;
			}

			string erpId = lekmerOrderItem.OrderItemSize.SizeId.HasValue ? lekmerOrderItem.OrderItemSize.ErpId : lekmerOrderItem.ErpId + "-000";

			packageDiscountIncludingVat = ParameterConverter.FormatParameter(packageDiscountIncludingVat, 2);
			packageDiscountExcludingVat = ParameterConverter.FormatParameter(packageDiscountExcludingVat, 2);

			var invoiceItem = new InvoiceRow
			{
				ArticleId = erpId,
				Description = rowDescription,
				Quantity = lekmerOrderItem.Quantity,
				UnitPrice = taxFreeZone ? packageDiscountExcludingVat : packageDiscountIncludingVat, // Price per unit incl. VAT.
				VAT = taxFreeZone ? 0m : vatPercent //VAT in percent.
			};

			invoiceRows.Add(invoiceItem);

			return invoiceRows;
		}

		protected virtual void AddPackageItem(Collection<InvoiceRow> invoiceItems, IPackageOrderItem packageOrderItem, bool taxFreeZone)
		{
			string rowDescription = ParameterConverter.FormatParameter(packageOrderItem.Title, 50);
			decimal unitPrice = ParameterConverter.FormatParameter(packageOrderItem.OriginalPrice.IncludingVat, 2);
			decimal vatPercent = ParameterConverter.FormatParameter(packageOrderItem.GetOriginalVatPercent(), 0);

			if (taxFreeZone)
			{
				unitPrice = ParameterConverter.FormatParameter(packageOrderItem.OriginalPrice.ExcludingVat, 2);
				vatPercent = 0m;
			}

			string erpId = packageOrderItem.SizeId.HasValue ? packageOrderItem.SizeErpId : packageOrderItem.ErpId + "-000";

			var invoiceItem = new InvoiceRow
			{
				ArticleId = erpId,
				Description = rowDescription,
				Quantity = packageOrderItem.Quantity,
				UnitPrice = unitPrice, // Price per unit incl. VAT.
				VAT = vatPercent //VAT in percent.
			};

			invoiceItems.Add(invoiceItem);
		}
	}
}
