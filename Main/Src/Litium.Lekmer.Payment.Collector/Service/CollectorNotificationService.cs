﻿using System.Collections.Specialized;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorNotificationService : ICollectorNotificationService
	{
		public virtual ICollectorNotificationInfo ParseFromQueryString(NameValueCollection queryString)
		{
			ICollectorNotificationInfo notificationInfo = new CollectorNotificationInfo();

			notificationInfo.InvoiceNo = queryString["InvoiceNo"];
			notificationInfo.OrderNoRaw = queryString["OrderNo"];
			notificationInfo.InvoiceStatusRaw = queryString["InvoiceStatus"];

			int orderNo;
			if (int.TryParse(notificationInfo.OrderNoRaw, out orderNo))
			{
				notificationInfo.OrderNo = orderNo;
			}

			int invoiceStatus;
			if (int.TryParse(notificationInfo.InvoiceStatusRaw, out invoiceStatus))
			{
				notificationInfo.InvoiceStatus = invoiceStatus;
			}

			return notificationInfo;
		}

		public virtual bool Validate(ICollectorNotificationInfo notificationInfo)
		{
			return
				notificationInfo.InvoiceNo.HasValue() &&
				notificationInfo.OrderNoRaw.HasValue() &&
				notificationInfo.OrderNo > 0 &&
				notificationInfo.InvoiceStatusRaw.HasValue() &&
				notificationInfo.OrderNo > 0;
		}
	}
}
