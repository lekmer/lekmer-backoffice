﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Common
{
	public interface IBlockContactUsService
	{
		IBlockContactUs GetById(IUserContext context, int id);
	}
}
