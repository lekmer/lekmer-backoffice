﻿using System.Web;

namespace Litium.Lekmer.Common
{
	public interface ICookieService
	{
		HttpCookie GetRequestCookie(string cookieName);
		void SetResponseCookie(HttpCookie cookie);
	}
}