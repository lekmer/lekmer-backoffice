﻿namespace Litium.Lekmer.Common.Job
{
	public interface IScheduleSetting
	{
		bool UseWeeklyExecution { get; }
		int WeeklyExecutionHour { get; }
		int WeeklyExecutionMinute { get; }
		bool WeeklyExecutionMonday { get; }
		bool WeeklyExecutionTuesday { get; }
		bool WeeklyExecutionWednesday { get; }
		bool WeeklyExecutionThursday { get; }
		bool WeeklyExecutionFriday { get; }
		bool WeeklyExecutionSaturday { get; }
		bool WeeklyExecutionSunday { get; }

		bool UseDailyExecution { get; }
		int DailyExecutionHour { get; }
		int DailyExecutionMinute { get; }

		bool UseRepeatExecution { get; }
		int RepeatExecutionIntervalMinutes { get; }
		int RepeatExecutionIntervalHours { get; }
		int RepeatExecutionStartMinute { get; }
		int RepeatExecutionStartHour { get; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "StartUp")]
		bool ExecuteOnStartUp { get; }
	}
}