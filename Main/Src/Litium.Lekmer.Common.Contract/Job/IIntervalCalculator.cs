﻿using System;

namespace Litium.Lekmer.Common.Job
{
	public interface IIntervalCalculator
	{
		TimeSpan NextWeeklyExecution(DateTime timeFrom, IScheduleSetting scheduleSetting);
		TimeSpan NextDailyExecution(DateTime timeFrom, IScheduleSetting scheduleSetting);
		TimeSpan FirstRepeatExecution(DateTime timeFrom, IScheduleSetting scheduleSetting);
		TimeSpan NextRepeatExecution(DateTime timeFrom, IScheduleSetting scheduleSetting);
	}
}
