﻿namespace Litium.Lekmer.Common.Job
{
	public interface IJob
	{
		string Group { get; }
		string Name { get; }
		IScheduleSetting ScheduleSetting { get; set; }
		object SyncToken { get; set; }

		void StartJob();
		void StopJob();
	}
}
