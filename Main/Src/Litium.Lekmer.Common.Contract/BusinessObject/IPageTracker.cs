namespace Litium.Lekmer.Common
{
	public interface IPageTracker
	{
		int Current { get; }
		int PageSize { get; }
		int ItemsLeft { get; }
		int ItemsTotalCount { get; }
		bool HasNext { get; }
		IPageTracker Initialize(int pageSize, int itemsTotalCount);
		void MoveToNext(int itemsTotalCount);
	}
}