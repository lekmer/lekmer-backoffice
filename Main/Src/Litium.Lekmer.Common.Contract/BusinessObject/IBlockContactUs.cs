﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Common
{
	public interface IBlockContactUs : IBlock
	{
		string Receiver { get; set; }
	}
}
