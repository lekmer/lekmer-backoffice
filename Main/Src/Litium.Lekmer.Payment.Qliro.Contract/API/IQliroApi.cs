﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using Litium.Lekmer.Payment.Qliro.Setting;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroApi
	{
		IQliroSetting QliroSetting { get; set; }

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Norway, Sweden and Denmark.
		/// </summary>
		IQliroGetAddressResponse GetAddress(string civicNumber, juridicalType juridicalType, string ip, string languageCode);

		/// <summary>
		/// Gets payment types for specific language.
		/// </summary>
		IQliroGetPaymentTypesResponse GetPaymentTypes(string languageCode);

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		IQliroReservationResult ReserveAmount(
			string languageCode,
			string civicNumber,
			pNoEncodingType pNoEncoding,
			genderType gender,
			juridicalType juridical,
			decimal amount,
			int orderId,
			address deliveryAddr,
			address billingAddr,
			string clientIp,
			string currencyCode,
			string countryCode,
			string preferredLanguage,
			string paymentType,
			string comment,
			string email,
			string cellularPhone,
			invoiceItemType[] invoiceItems
			);

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		IQliroReservationResult ReserveAmountTimeout(
			string languageCode,
			string civicNumber,
			pNoEncodingType pNoEncoding,
			genderType gender,
			juridicalType juridical,
			decimal amount,
			int orderId,
			address deliveryAddr,
			address billingAddr,
			string clientIp,
			string currencyCode,
			string countryCode,
			string preferredLanguage,
			string paymentType,
			string comment,
			string email,
			string cellularPhone,
			invoiceItemType[] invoiceItems
			);

		/// <summary>
		/// Checks status on your order
		/// </summary>
		IQliroGetStatusResult CheckOrderStatus(int orderId, string reservationNumber, string languageCode);

		void Initialize(string channelCommonName);
	}
}