﻿namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroTransactionService
	{
		int CreateGetAddress(string clientRef, string civicNumber);
		int CreateGetPaymentTypes(string clientRef);
		int CreateReservation(string clientRef, int mode, string civicNumber, int orderId, decimal amount, string currencyCode, string paymentType);
		int CreateReservationTimeout(string clientRef, int mode, string civicNumber, int orderId, decimal amount, string currencyCode, string paymentType);
		int CreateCheckOrderStatus(string clientRef, int orderId, string reservationNumber);

		void SaveResult(IQliroResult qliroResult);
		void SaveReservationResponse(IQliroReservationResult qliroReservationResult);
		void SaveCheckOrderStatusResponse(IQliroGetStatusResult qliroGetStatusResult);
	}
}
