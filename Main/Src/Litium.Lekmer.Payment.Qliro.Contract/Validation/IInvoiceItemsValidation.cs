﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IInvoiceItemsValidation
	{
		bool Validate(int orderId, invoiceItemType[] invoiceItems, decimal orderTotalAmount);
	}
}
