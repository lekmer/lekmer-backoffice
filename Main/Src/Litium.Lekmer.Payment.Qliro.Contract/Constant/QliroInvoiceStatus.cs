﻿namespace Litium.Lekmer.Payment.Qliro
{
	public enum QliroInvoiceStatus
	{
		Invalid = 0,
		Ok = 1,
		Denied = 2,
		Holding = 3,
		Timeout = 4,
		NoRisk = 5
	}
}
