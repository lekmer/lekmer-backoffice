﻿namespace Litium.Lekmer.Payment.Qliro.Setting
{
	public interface IQliroSetting
	{
		string Url { get; }
		string UserName { get; }
		string Password { get; }
		string ClientRef { get; }
		bool IgnoreCertificateValidation { get; }
		string ProductionStatus { get; }
		string CommunicationChannel { get; }

		string SpecialPaymentTypeCode { get; }

		bool Use7SecRule { get; }
		int DefaultResponseTimeout { get; }
		int Customer7SecResponseTimeout { get; }
		int Service7SecResponseTimeout { get; }

		void Initialize(string channelCommonName);
	}
}
