﻿namespace Litium.Lekmer.Payment.Qliro.Setting
{
	public interface IQliroPendingIntervalSetting
	{
		int OrderStatusCheckInterval { get; }
		int DecisionGuaranteedInterval { get; }
		bool IsSendRejectMessage { get; }
	}
}