﻿namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroResult
	{
		int TransactionId { get; set; }
		QliroTransactionStatus StatusCode { get; set; }
		int ReturnCode { get; set; }
		string Message { get; set; }
		string CustomerMessage { get; set; }
		long Duration { get; set; }
		string ResponseContent { get; set; }
	}
}
