﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroGetPaymentTypesResult : IQliroResult
	{
		Collection<IQliroPaymentType> PaymentTypes { get; set; }
	}
}
