﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroGetAddressResult : IQliroResult
	{
		Collection<IQliroAddress> QliroAddresses { get; set; }
	}
}