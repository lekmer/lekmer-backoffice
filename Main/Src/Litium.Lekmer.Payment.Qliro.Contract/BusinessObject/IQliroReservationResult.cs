﻿namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroReservationResult : IQliroResult
	{
		string ReservationId { get; set; }
		QliroInvoiceStatus InvoiceStatus { get; set; }
	}
}
