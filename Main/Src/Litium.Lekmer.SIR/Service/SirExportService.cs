﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.SIR.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.SIR
{
	public class SirExportService : ISirExportService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IUserContext _context;
		private ICategoryTree _categoryTree;
		private Collection<ICategoryView> _categories;

		protected SirExportRepository Repository { get; private set; }

		protected IUserContext Context
		{
			get { return _context ?? (_context = GetUserContext()); }
		}

		protected Collection<ICategoryView> Categories
		{
			get { return _categories ?? (_categories = CategoryService.GetViewAll(Context)); }
		}

		protected ISirSetting Setting { get; private set; }
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ICurrencySecureService CurrencyService { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }
		protected ILekmerProductSecureService ProductSecureService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ISupplierService SupplierService { get; private set; }

		public SirExportService(
			ISirSetting setting,
			ILekmerChannelService channelService,
			ICurrencySecureService currencyService,
			ILekmerProductService productService,
			IProductSecureService productSecureService,
			IProductSizeService productSizeService,
			ICategoryService categoryService,
			ISupplierService supplierService,
			SirExportRepository repository)
		{
			Setting = setting;
			ChannelService = channelService;
			CurrencyService = currencyService;
			ProductService = productService;
			ProductSecureService = (ILekmerProductSecureService) productSecureService;
			ProductSizeService = productSizeService;
			CategoryService = categoryService;
			SupplierService = supplierService;
			Repository = repository;
		}

		public virtual Collection<IProductStructure> GetProductStructureCollection()
		{
			var productStructureCollection = new Collection<IProductStructure>();

			var firstLevelCategories = Categories.Where(c => c.ParentCategoryId == null);
			foreach (var firstLevelCategory in firstLevelCategories)
			{
				int firstLevelCategoryId = firstLevelCategory.Id;
				var secondLevelCategories = Categories.Where(c => c.ParentCategoryId == firstLevelCategoryId);
				foreach (var secondLevelCategory in secondLevelCategories)
				{
					int secondLevelCategoryId = secondLevelCategory.Id;
					var thirdLevelCategories = Categories.Where(c => c.ParentCategoryId == secondLevelCategoryId);
					foreach (var thirdLevelCategory in thirdLevelCategories)
					{
						var productStructure = CreateProductStructure(firstLevelCategory, secondLevelCategory, thirdLevelCategory);
						productStructureCollection.Add(productStructure);
					}
				}
			}

			return productStructureCollection;
		}

		public virtual void RefreshProductsData()
		{
			_log.Info("Refresh products data...");

			Repository.RefreshProducts();

			_log.Info("Refresh products data - OK");
		}

		public virtual Collection<IProductInfo> GetProductInfoCollection(ISirBatch batch)
		{
			_log.Info("Reading product data...");

			Collection<IProductInfo> productInfoCollection = Repository.GetChangesFromDate(batch.ChangesFromDate);

			_log.Info("Reading product data - OK // " + productInfoCollection.Count + " items.");

			return productInfoCollection;
		}
		
		public virtual Collection<ISupplier> GetSupplierCollection()
		{
			Collection<ISupplier> supplierCollection = SupplierService.GetAll();
			return supplierCollection;
		}

		public virtual ISirBatch StartNewBatch()
		{
			return Repository.StartNewBatch();
		}

		public virtual ISirBatch CompleteBatch(ISirBatch batch)
		{
			return Repository.CompleteBatch(batch.Id);
		}


		protected virtual IProductStructure CreateProductStructure(ICategoryView category1Level, ICategoryView category2Level, ICategoryView category3Level)
		{
			var productStructure = IoC.Resolve<IProductStructure>();
			productStructure.Trade = GetCategoryErpId(category1Level.ErpId);
			productStructure.TradeDescription = category1Level.Title;
			productStructure.UpperProductGroup = GetCategoryErpId(category2Level.ErpId);
			productStructure.UpperProductGroupDescription = category2Level.Title;
			productStructure.ProductGroup = GetCategoryErpId(category3Level.ErpId);
			productStructure.ProductGroupDescription = category3Level.Title;
			return productStructure;
		}

		protected virtual void PreloadCategoryTree()
		{
			_categoryTree = new CategoryTree();
			_categoryTree.FillTree(Categories);
		}

		protected virtual IUserContext GetUserContext()
		{
			var context = IoC.Resolve<IUserContext>();
			context.Channel = ChannelService.GetAll().FirstOrDefault(c => c.Id == 1);
			return context;
		}

		protected virtual string GetCategoryErpId(string originalErpId)
		{
			return originalErpId.Substring(2);
		}
	}
}