﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.SIR
{
	public class SupplierFile
	{
		private bool _isInitialized;
		private string _delimiter;
		private Collection<ISupplier> _supplierCollection;
		private Encoding _encoding;

		public void Initialize(ISirSetting setting, Collection<ISupplier> supplierCollection)
		{
			_delimiter = setting.Delimiter;
			_supplierCollection = supplierCollection;

			_encoding = Encoding.GetEncoding(setting.CodePageName);

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap, _encoding);

			foreach (var supplier in _supplierCollection)
			{
				writer.WriteLine(AddRow(supplier));
			}

			writer.Flush();
			writer.Close();
		}

		protected string AddRow(ISupplier supplier)
		{
			string row = string.Empty;

			// Supplier no
			AddValue(ref row, supplier.SupplierNo);
			// Name
			AddValue(ref row, supplier.Name);
			// Address
			AddValue(ref row, supplier.Address ?? string.Empty);
			// Zip
			AddValue(ref row, supplier.ZipArea ?? string.Empty);
			// City
			AddValue(ref row, supplier.City ?? string.Empty);
			// Phone
			AddValue(ref row, supplier.Phone1 ?? supplier.Phone2 ?? string.Empty);

			int lastDelimiterPosition = row.LastIndexOf(_delimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				row = row.Substring(0, lastDelimiterPosition);
			}

			return row;
		}

		protected void AddValue(ref string row, string value)
		{
			row += value + _delimiter;
		}
	}
}