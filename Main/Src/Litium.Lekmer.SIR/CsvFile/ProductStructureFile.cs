﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace Litium.Lekmer.SIR
{
	public class ProductStructureFile
	{
		private bool _isInitialized;
		private string _delimiter;
		private Collection<IProductStructure> _productStructureCollection;
		private Encoding _encoding;

		public void Initialize(ISirSetting setting, Collection<IProductStructure> productStructureCollection)
		{
			_delimiter = setting.Delimiter;
			_productStructureCollection = productStructureCollection;

			_encoding = Encoding.GetEncoding(setting.CodePageName);

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap, _encoding);

			foreach (var productStructure in _productStructureCollection)
			{
				writer.WriteLine(AddRow(productStructure));
			}

			writer.Flush();
			writer.Close();
		}

		protected string AddRow(IProductStructure productStructure)
		{
			string row = string.Empty;

			// Trade
			AddValue(ref row, productStructure.Trade);
			// TradeDescription
			AddValue(ref row, productStructure.TradeDescription);
			// UpperProductGroup
			AddValue(ref row, productStructure.UpperProductGroup);
			// UpperProductGroupDescription
			AddValue(ref row, productStructure.UpperProductGroupDescription);
			// ProductGroup
			AddValue(ref row, productStructure.ProductGroup);
			// ProductGroupDescription
			AddValue(ref row, productStructure.ProductGroupDescription);

			int lastDelimiterPosition = row.LastIndexOf(_delimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				row = row.Substring(0, lastDelimiterPosition);
			}

			return row;
		}

		protected void AddValue(ref string row, string value)
		{
			row += value + _delimiter;
		}
	}
}