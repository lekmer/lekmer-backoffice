﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Text;

namespace Litium.Lekmer.SIR
{
	public class ProductsFile
	{
		private readonly string _priceFormat = "0.00";

		private bool _isInitialized;
		private string _delimiter;
		private Collection<IProductInfo> _productInfoCollection;
		private Encoding _encoding;

		public void Initialize(ISirSetting setting, Collection<IProductInfo> productInfoCollection)
		{
			_delimiter = setting.Delimiter;
			_productInfoCollection = productInfoCollection;

			_encoding = Encoding.GetEncoding(setting.CodePageName);

			_isInitialized = true;
		}

		public virtual void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap, _encoding);

			foreach (var productInfo in _productInfoCollection)
			{
				writer.WriteLine(AddRow(productInfo));
			}

			writer.Flush();
			writer.Close();
		}

		protected virtual string AddRow(IProductInfo productInfo)
		{
			string row = string.Empty;

			AddValue(ref row, productInfo.ArticleNumber);
			AddValue(ref row, productInfo.CategoryErpId);
			AddValue(ref row, productInfo.ArticleDescription);
			AddValue(ref row, productInfo.EanCode);

			// Our price
			string ourPrice = string.Empty;
			if (productInfo.OurPrice.HasValue)
			{
				ourPrice = productInfo.OurPrice.Value.ToString(_priceFormat, CultureInfo.InvariantCulture);
			}
			AddValue(ref row, ourPrice);

			// List price
			string listPrice = string.Empty;
			if (productInfo.ListPrice.HasValue)
			{
				listPrice = productInfo.ListPrice.Value.ToString(_priceFormat, CultureInfo.InvariantCulture);
			}
			AddValue(ref row, listPrice);

			AddValue(ref row, productInfo.SupplierId);
			AddValue(ref row, productInfo.Brand);
			AddValue(ref row, productInfo.LekmerArtNo);
			AddValue(ref row, productInfo.SupplierArtNo);

			// Purchase price
			string averagePrice = string.Empty;
			if (productInfo.AveragePrice.HasValue)
			{
				averagePrice = productInfo.AveragePrice.Value.ToString(_priceFormat, CultureInfo.InvariantCulture);
			}
			AddLastValue(ref row, averagePrice);

			// Purchase currency
			//AddValue(ref row, productInfo.PurchaseCurrencyIso);

			return row;
		}

		protected virtual void AddValue(ref string row, string value)
		{
			row = string.Concat(row, value, _delimiter);
		}

		protected virtual void AddLastValue(ref string row, string value)
		{
			row += value;
		}
	}
}