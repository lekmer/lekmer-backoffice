using System;

namespace Litium.Lekmer.SIR
{
	[Serializable]
	public class ProductInfo : IProductInfo
	{
		public string ArticleNumber { get; set; }
		public int ProductId { get; set; }
		public string CategoryErpId { get; set; }
		public string ArticleDescription { get; set; }
		public string EanCode { get; set; }
		public decimal? OurPrice { get; set; }
		public decimal? ListPrice { get; set; }
		public string SupplierId { get; set; }
		public string Brand { get; set; }
		public string LekmerArtNo { get; set; }
		public string SupplierArtNo { get; set; }
		public decimal? AveragePrice { get; set; }
	}
}