using System;

namespace Litium.Lekmer.SIR
{
	[Serializable]
	public class ProductStructure : IProductStructure
	{
		public string Trade { get; set; }
		public string TradeDescription { get; set; }
		public string UpperProductGroup { get; set; }
		public string UpperProductGroupDescription { get; set; }
		public string ProductGroup { get; set; }
		public string ProductGroupDescription { get; set; }
	}
}