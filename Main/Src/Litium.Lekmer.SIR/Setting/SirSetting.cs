﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.SIR
{
	public class SirSetting : SettingBase, ISirSetting
	{
		protected override string StorageName
		{
			get { return "SirExport"; }
		}
		protected virtual string GroupName
		{
			get { return "SirExportSettings"; }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}
		public string ProductStructureFileName
		{
			get { return GetString(GroupName, "ProductStructureFileName"); }
		}
		public string ProductsFileName
		{
			get { return GetString(GroupName, "ProductsFileName"); }
		}
		public string SuppliersFileName
		{
			get { return GetString(GroupName, "SuppliersFileName"); }
		}
		public string Delimiter
		{
			get { return GetString(GroupName, "Delimiter"); }
		}
		public int PageSize
		{
			get { return GetInt32(GroupName, "PageSize", 100); }
		}
		public string CodePageName
		{
			get { return GetString(GroupName, "CodePageName"); }
		}
	}
}