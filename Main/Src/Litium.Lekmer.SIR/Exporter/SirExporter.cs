﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.SIR
{
	public class SirExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected ISirSetting Setting { get; private set; }
		protected ISirExportService SirExportService { get; private set; }

		public SirExporter(ISirSetting sirSetting, ISirExportService sirExportService)
		{
			Setting = sirSetting;
			SirExportService = sirExportService;
		}

		public void Execute()
		{
			_log.Info("Execution started.");
			ExecuteExport();
			_log.Info("Execution complited.");
		}

		protected virtual void ExecuteExport()
		{
			ExecuteProductStructureExport();
			ExecuteProductInfoExport();
			ExecuteSupplierExport();
		}

		protected virtual void ExecuteProductStructureExport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			var filePath = GetFilePath(Setting.ProductStructureFileName);
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			_log.InfoFormat("File preparing... //{0}", filePath);

			_log.Info("Get product structure...");
			var productStructureCollection = SirExportService.GetProductStructureCollection();
			if (productStructureCollection.Count <= 0)
			{
				stopwatch.Stop();
				_log.Info("There are no product structure collection");
				return;
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				var productStructureFile = new ProductStructureFile();
				productStructureFile.Initialize(Setting, productStructureCollection);
				productStructureFile.Save(stream);
			}

			_log.InfoFormat("File saved... //{0}", filePath);

			stopwatch.Stop();
			_log.InfoFormat("Elapsed time {0}", stopwatch.Elapsed);
		}

		protected virtual void ExecuteProductInfoExport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			var filePath = GetFilePath(Setting.ProductsFileName);
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			ISirBatch sirBatch = SirExportService.StartNewBatch();

			SirExportService.RefreshProductsData();

			_log.InfoFormat("File preparing... //{0}", filePath);

			// use batch to get only changes
			Collection<IProductInfo> productInfoCollection = SirExportService.GetProductInfoCollection(sirBatch);

			using (Stream stream = File.OpenWrite(filePath))
			{
				var productsFile = new ProductsFile();
				productsFile.Initialize(Setting, productInfoCollection);
				productsFile.Save(stream);
			}

			sirBatch = SirExportService.CompleteBatch(sirBatch);

			_log.InfoFormat("File saved... //{0}", filePath);

			stopwatch.Stop();
			if (sirBatch.CompletedDate.HasValue)
			{
				_log.InfoFormat("Elapsed batch time {0}", sirBatch.CompletedDate - sirBatch.StartedDate);
			}
			_log.InfoFormat("Elapsed time {0}", stopwatch.Elapsed);
		}

		protected virtual void ExecuteSupplierExport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			var filePath = GetFilePath(Setting.SuppliersFileName);
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			_log.InfoFormat("File preparing... //{0}", filePath);

			_log.Info("Get Suppliers...");
			var supplierCollection = SirExportService.GetSupplierCollection();
			if (supplierCollection.Count <= 0)
			{
				stopwatch.Stop();
				_log.Info("There are no suppliers");
				return;
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				var supplierFile = new SupplierFile();
				supplierFile.Initialize(Setting, supplierCollection);
				supplierFile.Save(stream);
			}

			_log.InfoFormat("File saved... //{0}", filePath);

			stopwatch.Stop();
			_log.InfoFormat("Elapsed time {0}", stopwatch.Elapsed);
		}

		protected virtual string GetFilePath(string name)
		{
			string dirr = Setting.DestinationDirectory;
			string fileName = string.Format(name, DateTime.Now.ToString("yyyyMMdd-HHmmss"));
			return Path.Combine(dirr, fileName);
		}
	}
}