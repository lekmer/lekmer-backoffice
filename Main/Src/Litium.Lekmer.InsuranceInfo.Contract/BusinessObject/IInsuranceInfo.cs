﻿using System;

namespace Litium.Lekmer.InsuranceInfo.Contract
{
	public interface IInsuranceInfo
	{
		int OrderId { get; set; }
		string FirstName { get; set; }
		string LastName { get; set; }
		string CivicNumber { get; set; }
		string Adress { get; set; }
		string PostalCode { get; set; }
		string City { get; set; }
		string PhoneNumber { get; set; }
		string Email { get; set; }
		DateTime PurchaseDate { get; set; }
		string ParentCategory { get; set; }
		string Category { get; set; }
		string CategoryId { get; set; }
		string Brand { get; set; }
		string Modell { get; set; }
		decimal PurchaseAmount { get; set; }
		int Quantity { get; set; }
		decimal Premie { get; set; }
	}
}