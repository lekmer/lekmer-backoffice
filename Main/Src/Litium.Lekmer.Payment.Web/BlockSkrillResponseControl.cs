﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Skrill;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockSkrillResponseControl : BlockControlBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IOrderService _orderService;
		private readonly IOrderStatusService _orderStatusService;
		private readonly IContentNodeService _contentNodeService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;

		public BlockSkrillResponseControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IOrderService orderService,
			IOrderStatusService orderStatusService,
			IContentNodeService contentNodeService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService)
			: base(templateFactory, blockService)
		{
			_contentNodeService = contentNodeService;
			_orderService = orderService;
			_orderStatusService = orderStatusService;
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
		}

		private int _orderId;
		private int OrderId
		{
			get
			{
				if (_orderId <= 0)
				{
					if (!int.TryParse(Request["IDENTIFICATION.TRANSACTIONID"], out _orderId))
					{
						throw new ArgumentException("Parameter 'IDENTIFICATION.TRANSACTIONID' is missing or invalid.");
					}
				}

				return _orderId;
			}
		}

		private IOrderFull _order;
		private IOrderFull Order
		{
			get { return _order ?? (_order = _orderService.GetFullById(UserContext.Current, OrderId)); }
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "SkrillResponse"; }
		}

		protected override BlockContent RenderCore()
		{
			LogRequest();

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			string body = RenderBody();
			return new BlockContent(head, body, footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private string RenderBody()
		{
			string processingResult = Request["PROCESSING.RESULT"];
			if (!string.IsNullOrEmpty(processingResult))
			{
				bool requestCancelled;
				bool.TryParse(Request["FRONTEND.REQUEST.CANCELLED"], out requestCancelled);

				if (processingResult.Equals("ACK") && !requestCancelled)
				{
					return RenderSuccessBody();
				}
			}

			return RenderUnsuccessBody();
		}


		//RenderUnsuccessBody should check if the order has already been paid or not, if it is then this means
		//rendering unsuccess body is unnecessary and the customer should actually be redirected to the confirmation page
		private string RenderUnsuccessBody()
		{
			string redirectUrl;

			if (!OrderIsConfirmed())
			{
				redirectUrl = GetRedirectUrl("SkrillResponseUnsuccessful");
				ResetOrder();
			}
			else
			{
				redirectUrl = GetRedirectUrl("SkrillResponseSuccessful");
			}

			return RenderBody(redirectUrl);
		}

		private string RenderSuccessBody()
		{
			string redirectUrl = GetRedirectUrl("SkrillResponseSuccessful");
			if (!OrderIsConfirmed())
			{
				UpdateOrderStatus();
			}

			return RenderBody(redirectUrl);
		}

		private string RenderBody(string redirectUrl)
		{
			Fragment fragment = Template.GetFragment("Content");
			fragment.AddVariable("ResponseRedirectUrl", redirectUrl, VariableEncoding.None);
			return fragment.Render();
		}


		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

			if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
			{
				return UrlHelper.ResolveUrlHttp(contentNodeTreeItem.Url);
			}

			return UrlHelper.BaseUrlHttp;
		}

		private string AddQueryParameters(Dictionary<string, string> parameters, string url)
		{
			var query = new QueryBuilder();
			foreach (var parameter in parameters)
			{
				query.Add(parameter.Key, parameter.Value);
			}
			return url + query.ToString();
		}

		private bool VerifyResponse()
		{
			string securityHash = Request["SECURITYHASH"];

			string channelCommonName = Request["CRITERION.Channel.CommonName"];
			var setting = new SkrillSetting(channelCommonName);

			var parameters = new List<string>
			{
				ReplaceIfEmpty(Request["PAYMENT.CODE"]),
				ReplaceIfEmpty(Request["IDENTIFICATION.TRANSACTIONID"]),
				ReplaceIfEmpty(Request["IDENTIFICATION.UNIQUEID"]),
				ReplaceIfEmpty(Request["CLEARING.AMOUNT"]),
				ReplaceIfEmpty(Request["CLEARING.CURRENCY"]),
				ReplaceIfEmpty(Request["PROCESSING.RISK_SCORE"]),
				ReplaceIfEmpty(Request["TRANSACTION.MODE"]),
				ReplaceIfEmpty(Request["PROCESSING.RETURN.CODE"]),
				ReplaceIfEmpty(Request["PROCESSING.REASON.CODE"]),
				ReplaceIfEmpty(Request["PROCESSING.STATUS.CODE"]),
				setting.SecretWord
			};

			string stringToDigest = string.Empty;
			foreach (var parameter in parameters)
			{
				stringToDigest += parameter + "|";
			}
			stringToDigest = stringToDigest.Substring(0, stringToDigest.Length - 1);
			
			var hash = Sha1Hasher.HashIt(stringToDigest);

			return securityHash == hash;
		}

		private string ReplaceIfEmpty(string value)
		{
			return string.IsNullOrEmpty(value.Trim()) ? string.Empty : value;
		}

		// Unsuccesful
		private void ResetOrder()
		{
			if (Order.OrderStatus.CommonName == OrderStatusName.PaymentPending)
			{
				Order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.RejectedByPaymentProvider);
				_orderService.SaveFull(UserContext.Current, Order);
			}
			else
			{
				_log.InfoFormat("Order (orderId={0}) status was not changed because current status not 'PaymentPending'", OrderId.ToString(CultureInfo.InvariantCulture));
			}
		}

		// Successful
		private bool OrderIsConfirmed()
		{
			bool orderIsAlreadyConfirmed = false;

			if (Order != null)
			{
				string orderRefrerenceId = string.Empty;
				if (Order.Payments.Count > 0)
				{
					orderRefrerenceId = Order.Payments.First().ReferenceId;
				}

				if (Order.OrderStatus.CommonName == OrderStatusName.PaymentConfirmed && orderRefrerenceId.HasValue())
				{
					orderIsAlreadyConfirmed = true;
				}
			}

			return orderIsAlreadyConfirmed;
		}

		private void UpdateOrderStatus()
		{
			Order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);

			if (Order.Payments.Count > 0)
			{
				string verifyId = Request["IDENTIFICATION.UNIQUEID"];
				Order.Payments.First().ReferenceId = verifyId.IsNullOrEmpty() ? string.Empty : verifyId;
			}

			_orderService.SaveFull(UserContext.Current, Order);
			((ILekmerOrderService)_orderService).UpdateNumberInStock(UserContext.Current, Order);
			_giftCardViaMailInfoService.Save(UserContext.Current, Order);
			_orderService.SendConfirm(UserContext.Current, Order);
		}

		private void LogRequest()
		{
			string logRequest = string.Empty;

			foreach (var key in Request.Form.AllKeys)
			{
				logRequest += string.Format("{0} - {1}", key, Request[key]) + "\r\n";
			}
			_log.InfoFormat("SKRILL Request Form Parameters: \r\n {0}", logRequest);

			logRequest = string.Empty;
			foreach (var key in Request.Params.AllKeys)
			{
				logRequest += string.Format("{0} - {1}", key, Request.Params[key]) + "\r\n";
			}
			_log.InfoFormat("SKRILL Request Parameters: \r\n {0}", logRequest);

			try
			{
				_log.InfoFormat("SKRILL transaction_id - {0}", Request["transaction_id"]);
			}
			catch (Exception)
			{
				_log.Info("SKRILL Request transaction_id is missing parameter");
			}
		}
	}
}