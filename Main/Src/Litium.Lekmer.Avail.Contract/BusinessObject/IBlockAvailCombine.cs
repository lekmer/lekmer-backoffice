﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Avail
{
	public interface IBlockAvailCombine : IBlock
	{
		bool UseCartPredictions { get; set; }
		bool UseClickStreamPredictions { get; set; }
		bool UseLogPurchase { get; set; }
		bool UsePersonalPredictions { get; set; }
		bool UseProductSearchPredictions { get; set; }
		bool UseProductsPredictions { get; set; }
		bool UseProductsPredictionsFromClicksProduct { get; set; }
		bool UseProductsPredictionsFromClicksCategory { get; set; }
		bool UseLogClickedOn { get; set; }
		bool GetClickHistoryFromCookie { get; set; }
		IBlockSetting Setting { get; set; }
	}
}