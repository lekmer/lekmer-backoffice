﻿using System.Web;
using System.Web.SessionState;

namespace Litium.Lekmer.Common
{
	public class SessionStateService : ISessionStateService
	{
		private static HttpSessionState Session
		{
			get { return HttpContext.Current != null ? HttpContext.Current.Session : null; }
		}

		public virtual bool Available
		{
			get { return Session != null; }
		}

		public virtual bool Contains(string name)
		{
			return Session[name] != null;
		}

		public virtual void Remove(string name)
		{
			Session.Remove(name);
		}

		public virtual object this[string name]
		{
			get { return Session[name]; }
			set { Session[name] = value; }
		}

		public virtual string SessionId
		{
			get { return Session.SessionID; }
		}
	}
}