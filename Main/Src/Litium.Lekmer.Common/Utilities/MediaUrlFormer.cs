﻿using System.Globalization;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Common
{
	public static class MediaUrlFormer
	{
		private static ICommonSession _commonSession;

		private static ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
		}


		public static string ResolveOriginalSizeImageUrl(string mediaUrl, IMediaItem mediaItem, IMediaFormat mediaFormat)
		{
			string imageUrl = string.Format(
				CultureInfo.InvariantCulture,
				"{0}/{1}/{2}.{3}",
				/*0*/ mediaUrl,
				/*1*/ mediaItem.Id,
				/*2*/ UrlCleaner.CleanUp(mediaItem.Title),
				/*3*/ mediaFormat.Extension
				);

			return imageUrl;
		}

		public static string ResolveOriginalSizeImageUrl(string mediaUrl, IImage image)
		{
			return ResolveOriginalSizeImageUrl(mediaUrl, image, image.Title);
		}

		public static string ResolveOriginalSizeImageUrl(string mediaUrl, IImage image, string title)
		{
			string imageUrl = string.Format(
				CultureInfo.InvariantCulture,
				"{0}/{1}/{2}.{3}",
				/*0*/ mediaUrl,
				/*1*/ image.Id,
				/*2*/ UrlCleaner.CleanUp(title),
				/*3*/ image.FormatExtension
				);

			return imageUrl;
		}

		public static string ResolveCustomSizeImageUrl(string mediaUrl, IImage image, string sizeName)
		{
			return ResolveCustomSizeImageUrl(mediaUrl, image, image.Title, sizeName);
		}

		public static string ResolveCustomSizeImageUrl(string mediaUrl, IImage image, string title, string sizeName)
		{
			string urlFormat = CommonSession.IsViewportMobile 
				? "{0}/{1}/m/{2}/{3}.{4}" 
				: "{0}/{1}/{2}/{3}.{4}";

			string imageUrl = string.Format(
				CultureInfo.InvariantCulture,
				urlFormat,
				/*0*/ mediaUrl,
				/*1*/ image.Id,
				/*2*/ sizeName,
				/*3*/ UrlCleaner.CleanUp(title),
				/*4*/ image.FormatExtension
				);

			return imageUrl;
		}
	}
}
