﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Litium.Lekmer.Common
{
	public class SpreadsheetHelper
	{
		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public void CreateSpreadsheetWorkbook(string filePath, Collection<SpreadsheetDocumentData> dataCollection)
		{
			var document = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook);
			var workbookPart = document.AddWorkbookPart();
			workbookPart.Workbook = new Workbook { Sheets = new Sheets() };

			foreach (var data in dataCollection)
			{
				var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
				var sheetData = new SheetData();
				worksheetPart.Worksheet = new Worksheet(sheetData);

				var sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
				uint sheetId = 1;
				if (sheets.Elements<Sheet>().Any())
				{
					sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
				}

				var sheet = new Sheet
				{
					Id = workbookPart.GetIdOfPart(worksheetPart),
					SheetId = sheetId,
					Name = data.SheetName
				};
				sheets.Append(sheet);

				var columns = new List<string>();
				var headerRow = new Row();
				foreach (var column in data.Columns) // columns
				{
					columns.Add(column.Key);

					var cell = CreateCell(column.Value);
					headerRow.AppendChild(cell);
				}
				sheetData.AppendChild(headerRow);

				foreach (var row in data.Rows) // rows
				{
					var newRow = new Row();
					foreach (var column in columns)
					{
						var value = string.Empty;
						if (row.ContainsKey(column))
						{
							value = row[column] ?? string.Empty;
						}

						var cell = CreateCell(value);
						newRow.AppendChild(cell);
					}
					sheetData.AppendChild(newRow);
				}
			}

			workbookPart.Workbook.Save();
			document.Close();
		}
		private static Cell CreateCell(string value)
		{
			var cell = new Cell
				{
					DataType = CellValues.String,
					CellValue = new CellValue(value)
				};
			return cell;
		}

		[SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures"),
		 SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public Collection<Collection<string>> ReadSpreadsheetDom(Stream stream, Collection<string> returnColumns, bool allowEmptyField)
		{
			var result = new Collection<Collection<string>>();

			SharedStringTablePart sharedStringTablePart = null;
			using (var document = SpreadsheetDocument.Open(stream, false))
			{
				var workbookPart = document.WorkbookPart;
				if (workbookPart.SharedStringTablePart != null)
				{
					sharedStringTablePart = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
				}
				foreach (var worksheetPart in workbookPart.WorksheetParts)
				{
					//var worksheetPart = workbookPart.WorksheetParts.First();
					var sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

					var columnIndexes = new List<int>();
					var tempRows = sheetData.Elements<Row>();
					var rows = tempRows != null ? tempRows.ToList() : null;
					var rowsCount = rows != null ? rows.Count() : 0;

					var columnLetters = new List<string>();
					if (rowsCount > 0)
					{
						var headerRow = rows.First();
						var cells = headerRow.Elements<Cell>().ToList();
						for (int i = 0; i < cells.Count(); i++)
						{
							var value = GetCellValue(cells[i], sharedStringTablePart);
							if (!returnColumns.Contains(value)) continue;
							columnIndexes.Add(i);
							columnLetters.Add(GetColumnAddress(cells[i].CellReference));
						}
					}
					else
					{
						continue;
					}

					// Validate if all columns exists
					if (columnIndexes.Count != returnColumns.Count)
					{
						throw new SpreadsheetException("File does not contain all needed information. Please check if imported file contains all columns that will be imported.");
					}

					if (rowsCount > 1)
					{
						for (int i = 1; i < rowsCount; i++)
						{
							var resultItem = new Collection<string>();
							List<Cell> cells;
							if (allowEmptyField)
							{
								cells = GetCellsForRow(rows[i], columnLetters).ToList();
							}
							else
							{
								cells = rows[i].Elements<Cell>().ToList();
							}

							// Validate if all cells have a value.
							if (!allowEmptyField && cells.Count - 1 < columnIndexes.Max())
							{
								throw new SpreadsheetException("File does not contain all needed information. Please check if all cells have a value");
							}

							foreach (var index in columnIndexes)
							{
								var value = GetCellValue(cells[index], sharedStringTablePart);
								if (!allowEmptyField && string.IsNullOrEmpty(value))
								{
									throw new SpreadsheetException("File does not contain all needed information. Please check if all cells have a value");
								}
								resultItem.Add(value);
							}
							result.Add(resultItem);
						}
					}
				}
			}

			return result;
		}
		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private string GetCellValue(Cell cell, SharedStringTablePart sharedStringTablePart)
		{
			var value = cell.CellValue.Text;
			if (cell.DataType != null
				&& cell.DataType.Value == CellValues.SharedString
				&& sharedStringTablePart != null
				&& sharedStringTablePart.SharedStringTable != null)
			{
				value = sharedStringTablePart.SharedStringTable.ElementAt(int.Parse(value, CultureInfo.CurrentCulture)).InnerText;
			}

			return value.Trim();
		}

		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private string GetColumnAddress(string cellReference)
		{
			var regex = new Regex("[A-Za-z]+");
			Match match = regex.Match(cellReference);
			return match.Value;
		}

		private IEnumerable<Cell> GetCellsForRow(Row row, List<string> columnLetters)
		{
			int workIdx = 0;
			foreach (var cell in row.Descendants<Cell>())
			{
				//Get letter part of cell address
				var cellLetter = GetColumnAddress(cell.CellReference);

				//Get column index of the matched cell  
				int currentActualIdx = columnLetters.IndexOf(cellLetter);

				//Add empty cell if work index smaller than actual index
				for (; workIdx < currentActualIdx; workIdx++)
				{
					yield return new Cell() { DataType = null, CellValue = new CellValue(string.Empty) };
				}

				//Return cell with data from Excel row
				yield return cell.CellValue == null ? new Cell() {DataType = null, CellValue = new CellValue(string.Empty)} : cell;
				workIdx++;

				//Check if it's ending cell but there still is any unmatched columnLetters item   
				if (cell == row.LastChild)
				{
					//Append empty cells to enumerable 
					for (; workIdx < columnLetters.Count(); workIdx++)
					{
						yield return new Cell() { DataType = null, CellValue = new CellValue(string.Empty) };
					}
				}
			}
		}
	}
}