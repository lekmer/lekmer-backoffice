﻿using System;

namespace Litium.Lekmer.Common.Extensions
{
	public static class GlobalExtension
	{
		/// <summary>
		/// Ensures the specified instance is not null.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instance">The instance.</param>
		/// <exception cref="InvalidOperationException">Throw exception when instance is null.</exception>
		public static void EnsureNotNull<T>(this T instance) where T : class
		{
			if (instance == null)
			{
				throw new InvalidOperationException(typeof(T) + " must be set before calling current method.");
			}
		}

		/// <summary>
		/// Ensures the specified instance is not null.
		/// </summary>
		/// <typeparam name="TResult"></typeparam>
		/// <param name="instance">The instance.</param>
		/// <exception cref="InvalidOperationException">Throw exception when instance is null.</exception>
		public static TResult Cast<TResult>(this object instance)
			where TResult : class
		{
			return instance as TResult;
		}
	}
}
