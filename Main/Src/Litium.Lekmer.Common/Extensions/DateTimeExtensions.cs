﻿using System;

namespace Litium.Lekmer.Common.Extensions
{
	/// <summary>
	/// Some usefull System.DateTime extensions.
	/// </summary>
	public static class DateTimeExtensions
	{
		/// <summary>
		/// Indicates whether the specified System.DateTime object is empty.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static bool IsEmpty(this DateTime value)
		{
			return value == DateTime.MinValue;
		}

		/// <summary>
		/// Indicates whether the specified System.DateTime object is not empty.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static bool HasValue(this DateTime value)
		{
			return value != DateTime.MinValue;
		}
	}
}
