﻿using System.Text;

namespace Litium.Lekmer.Common.Extensions
{
	/// <summary>
	/// Some usefull System.String extensions.
	/// </summary>
	public static class StringExtensions
	{
		/// <summary>
		/// Indicates whether the specified System.String object is null or an System.String.Empty string.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static bool IsEmpty(this string value)
		{
			return value == null || value.Length == 0;
		}

		/// <summary>
		/// Indicates whether the specified System.String object is not null and not System.String.Empty string.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static bool HasValue(this string value)
		{
			return value != null && value.Length > 0;
		}


		/// <summary>
		/// Removes control characters and other non-UTF-8 characters
		/// The XML spec is: Char = #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static string StripIllegalXmlCharacters(this string value)
		{
			if (value == null) return null;

			var result = new StringBuilder();

			for (int i = 0; i < value.Length; i++)
			{
				char ch = value[i];
				if (
					(ch >= 0x0020 && ch <= 0xD7FF) ||
					(ch >= 0xE000 && ch <= 0xFFFD) ||
					ch == 0x0009 ||
					ch == 0x000A ||
					ch == 0x000D
					)
				{
					result.Append(ch);
				}
			}
			return result.ToString();
		}

		/// <summary>
		/// Replaces invalid XML characters in a string with their valid XML equivalent.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static string EscapeSpecialXmlCharacters(this string value)
		{
			if (value == null) return null;

			return System.Security.SecurityElement.Escape(value);
		}

		/// <summary>
		/// Encodes a string in a HTML string.
		/// </summary>
		/// <param name="value">The string to encode.</param>
		/// <returns>The encoded string.</returns>
		public static string EncodeHtml(this string value)
		{
			if (value == null) return null;

			return Scensum.Foundation.Utilities.Encoder.EncodeHtml(value).Replace("[", "&#91;").Replace("]", "&#93;");
		}

		/// <summary>
		/// Encodes a string in a javascript string.
		/// </summary>
		/// <param name="value">The string to encode.</param>
		/// <returns>The encoded string.</returns>
		public static string EncodeJavaScript(this string value)
		{
			if (value == null) return null;

			return Scensum.Foundation.Utilities.Encoder.EncodeJavaScript(value);
		}
	}
}
