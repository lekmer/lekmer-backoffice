using System.Web;
using FiftyOne.Foundation.Mobile.Detection;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Common
{
	public class CommonSession : ICommonSession
	{
		private static string _keyIsMobile = "IsMobile";
		private static string _keyIsViewPortMobile = "IsViewportMobile";

		protected ISessionStateService Session { get; private set; }

		public CommonSession(ISessionStateService session)
		{
			Session = session;
		}

		public bool IsDesktop
		{
			get { return !IsMobile; }
		}

		public bool IsMobile
		{
			get
			{
				if (!Session.Available) return false;

				bool isMobile;
				if (Session[_keyIsMobile] != null)
				{
					isMobile = (bool)Session[_keyIsMobile];
				}
				else
				{
					isMobile = CheckIsMobile();
					Session[_keyIsMobile] = isMobile;
				}

				return isMobile;
			}
		}

		public bool IsViewportDesktop
		{
			get { return !IsViewportMobile; }
		}

		public bool IsViewportMobile
		{
			get
			{
				if (!Session.Available) return false;

				bool isViewPortMobile;
				if (Session[_keyIsViewPortMobile] != null)
				{
					isViewPortMobile = (bool)Session[_keyIsViewPortMobile];
				}
				else
				{
					isViewPortMobile = CheckIsViewportMobile();
					Session[_keyIsViewPortMobile] = isViewPortMobile;
				}

				return isViewPortMobile;
			}
		}

		public virtual void ResetDetectedDevice()
		{
			Session.Remove(_keyIsMobile);
			Session.Remove(_keyIsViewPortMobile);
		}

		public virtual void SetDevice(string deviceType)
		{
			if (deviceType == "desktop")
			{
				Session[_keyIsMobile] = false;
				Session[_keyIsViewPortMobile] = false;
			}
			else if (deviceType == "mobile")
			{
				Session[_keyIsMobile] = true;
				Session[_keyIsViewPortMobile] = true;
			}
			else
			{
				ResetDetectedDevice();
			}
		}

		protected virtual bool CheckIsMobile()
		{
			if (LekmerWebSetting.Instance.DeviceDetectionEnabled)
			{
				Match deviceMatch = WebProvider.ActiveProvider.Match(HttpContext.Current.Request.Headers);
				if (deviceMatch != null)
				{
					bool? isMobile = deviceMatch.Results.GetBool("IsMobile");
					return isMobile ?? false;
				}
			}

			// Desktop by default when device detection is not enabled
			return false;
		}

		protected virtual bool CheckIsViewportMobile()
		{
			if (LekmerWebSetting.Instance.DeviceDetectionEnabled)
			{
				bool result = false;
				Match deviceMatch = WebProvider.ActiveProvider.Match(HttpContext.Current.Request.Headers);
				if (deviceMatch != null)
				{
					string screenPixelsWidth = deviceMatch.Results.GetString("ScreenPixelsWidth");
					int screenWidth;
					if (int.TryParse(screenPixelsWidth, out screenWidth))
					{
						result = screenWidth < 768;
					}
				}
				return result;
			}

			// Desktop by default when device detection is not enabled
			return false;
		}
	}
}