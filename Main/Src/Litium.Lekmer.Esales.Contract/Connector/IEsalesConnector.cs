﻿using Apptus.ESales.Connector;

namespace Litium.Lekmer.Esales.Connector
{
	public interface IEsalesConnector
	{
		Session EsalesSession { get; }
		void ImportAds(string fileName);
		void ImportProducts(string fileName);
		void Defragmentation();
		void Synchronize(bool ignore);
	}
}