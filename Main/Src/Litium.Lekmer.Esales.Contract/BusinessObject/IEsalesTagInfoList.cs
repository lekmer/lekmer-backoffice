﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesTagInfoList
	{
		void Add(IChannel channel, Collection<ITag> tagCollection);

		Collection<IEsalesTagInfo> GetEsalesTagInfoCollection();
	}
}