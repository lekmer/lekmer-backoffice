﻿using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesModel
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		int Ordinal { get; set; }

		Dictionary<string, IEsalesModelParameter> Parameters { get; set; }

		string TryGetParameterValue(string parameterName, string defaultValue);
	}
}
