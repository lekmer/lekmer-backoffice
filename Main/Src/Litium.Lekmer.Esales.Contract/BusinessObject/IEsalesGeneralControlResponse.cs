﻿namespace Litium.Lekmer.Esales
{
	public interface IEsalesGeneralControlResponse
	{
		IEsalesResponse EsalesGeneralResponse { get; set; }
	}
}