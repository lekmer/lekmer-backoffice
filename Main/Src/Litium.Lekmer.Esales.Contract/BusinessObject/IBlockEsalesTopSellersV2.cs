﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersV2 : ILekmerBlock
	{
		string PanelName { get; set; }
		int WindowLastEsalesValue { get; set; }
		bool IncludeAllCategories { get; set; }
		int? LinkContentNodeId { get; set; }
		string CustomUrl { get; set; }
		string UrlTitle { get; set; }
		IBlockSetting Setting { get; set; }

		Collection<IBlockEsalesTopSellersCategoryV2> Categories { get; set; }
		Collection<IBlockEsalesTopSellersProductV2> Products { get; set; }

		void PopulateEsalesParameters(Dictionary<string, string> parameters);
	}
}