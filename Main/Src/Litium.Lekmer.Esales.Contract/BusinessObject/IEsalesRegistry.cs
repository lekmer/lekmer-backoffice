﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesRegistry : IBusinessObjectBase
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		int? ChannelId { get; set; }
		int? SiteStructureRegistryId { get; set; }
	}
}