using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesTagInfo
	{
		int TagId { get; set; }

		string EsalesKey { get; set; }

		IEsalesChannelTagInfo DefaultTagInfo { get; }

		Collection<IEsalesChannelTagInfo> ChannelTagInfoCollection { get; }

		void Add(IChannel channel, ITag tag);
	}
}