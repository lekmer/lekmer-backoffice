﻿using System.Collections.Generic;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesRecommendV2 : ILekmerBlock
	{
		string PanelName { get; set; }
		int WindowLastEsalesValue { get; set; }
		IBlockSetting Setting { get; set; }

		void PopulateEsalesParameters(Dictionary<string, string> parameters);
	}
}
