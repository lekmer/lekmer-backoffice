﻿using Litium.Lekmer.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public interface IAdFolder : IBusinessObjectBase, IFolder
	{
		int Id { get; set; }
		int? ParentFolderId { get; set; }
		string Title { get; set; }
	}
}