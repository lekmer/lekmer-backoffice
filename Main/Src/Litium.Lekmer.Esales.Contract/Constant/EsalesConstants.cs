﻿namespace Litium.Lekmer.Esales
{
	public static class EsalesConstants
	{
		public const string LekmerApplication = "Lekmer";
		public const string HeppoApplication = "Heppo";

		public const string DefaultFakeFilter = "type:'product_fake'";
		public const string WindowLastTemplate = "window_last_{0}";
		public const string FilterTopSellersTemplate = "filter_{0}_top_sellers";
		public const string FilterProductListTemplate = "filter_{0}_product_list";
	}
}
