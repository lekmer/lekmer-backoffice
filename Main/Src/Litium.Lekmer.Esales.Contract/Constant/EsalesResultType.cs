﻿namespace Litium.Lekmer.Esales
{
	public enum EsalesResultType
	{
		Unknown,
		Completions,
		Corrections,
		Count,
		Products,
		Values,
		Ads,
		Phrases
	}
}
