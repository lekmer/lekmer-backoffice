using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesRecommendSecureService
	{
		IBlockEsalesRecommend Create();

		IBlockEsalesRecommend GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockEsalesRecommend block);
	}
}