using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesAdsSecureService
	{
		IBlockEsalesAds Create();

		IBlockEsalesAds GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockEsalesAds block);
	}
}