using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersProductSecureServiceV2
	{
		Collection<IBlockEsalesTopSellersProductV2> GetAllByBlock(int blockId, int channelId);
		void Save(int blockId, Collection<IBlockEsalesTopSellersProductV2> blockProducts);
	}
}