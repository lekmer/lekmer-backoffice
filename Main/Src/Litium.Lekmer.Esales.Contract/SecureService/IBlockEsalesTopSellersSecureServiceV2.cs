using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersSecureServiceV2
	{
		IBlockEsalesTopSellersV2 Create();
		IBlockEsalesTopSellersV2 GetById(int id);
		int Save(ISystemUserFull systemUserFull, IBlockEsalesTopSellersV2 block);
		int Save(
			ISystemUserFull systemUserFull,
			IBlockEsalesTopSellersV2 block,
			Collection<IBlockEsalesTopSellersCategoryV2> blockCategories,
			Collection<IBlockEsalesTopSellersProductV2> blockProducts,
			Collection<ITranslationGeneric> urlTitleTranslations);

		// Translations.
		Collection<ITranslationGeneric> GetAllUrlTitleTranslationsByBlock(int blockId);
	}
}