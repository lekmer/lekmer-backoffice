﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface ISearchIncrementalPanelSettingV2 : IEsalesPanelSetting
	{
		string AutocompletePanelName { get; }
		string DidYouMeanPanelName { get; }
		string ProductSuggestionsPanelName { get; }
		string CategoriesPanelName { get; }
		string BrandsPanelName { get; }
	}
}
