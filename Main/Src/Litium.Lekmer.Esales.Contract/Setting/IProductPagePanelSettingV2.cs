﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IProductPagePanelSettingV2 : IEsalesPanelSetting
	{
		string ProductInformationPanelName { get; }
	}
}