﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IEsalesPanelSetting
	{
		string PanelPath { get; }
	}
}
