﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface ISearchProductPanelSettingV2 : ISearchFacetPanelSetting
	{
		string ProductCountPanelName { get; }
		string ProductPanelName { get; }
		string FacetsFilterPanelName { get; }
		string FacetsAllPanelName { get; }
		string DidYouMeanPanelName { get; }
		string TopSearchesPanelName { get; }
	}
}