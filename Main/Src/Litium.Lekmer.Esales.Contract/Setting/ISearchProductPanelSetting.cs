﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface ISearchProductPanelSetting : ISearchFacetPanelSetting
	{
		string ProductPanelName { get; }
		string AutoCompletePanelName { get; }
		string DidYouMeanPanelName { get; }
		string CategoryPanelName { get; }
		string TagPanelName { get; }
		string BrandPanelName { get; }
		string BrandAllPanelName { get; }
	}
}