﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersProductServiceV2
	{
		Collection<IBlockEsalesTopSellersProductV2> GetAllByBlock(IUserContext context, int blockId);
	}
}