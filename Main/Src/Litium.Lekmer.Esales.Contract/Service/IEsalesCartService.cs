﻿using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesCartService
	{
		ProductCollection FindRecommend(IUserContext context, string panelPath, string fallbackPanelPath, int productId, ProductIdCollection cartProductIds, int itemsToReturn);
	}
}
