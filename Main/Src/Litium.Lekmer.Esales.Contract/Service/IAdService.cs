﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IAdService
	{
		IAd GetById(int adId);

		Collection<IAd> GetAll();
	}
}