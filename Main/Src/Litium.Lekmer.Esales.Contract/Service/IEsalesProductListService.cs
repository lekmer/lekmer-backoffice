﻿using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesProductListService
	{
		ProductCollection GetProducts(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent);
	}
}