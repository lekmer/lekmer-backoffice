using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesAdsService
	{
		IBlockEsalesAds GetById(IUserContext context, int blockId);
	}
}