﻿namespace Litium.Lekmer.Esales
{
	public interface IEsalesModelService
	{
		IEsalesModelComponent GetModelComponentById(int siteStructureRegistryId, int modelComponentById);
	}
}
