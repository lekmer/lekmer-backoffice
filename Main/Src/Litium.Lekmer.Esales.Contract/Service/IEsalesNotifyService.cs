﻿using Litium.Scensum.Order;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesNotifyService
	{
		void NotifyCustomerKey(string value);
		void NotifyProperty(string name, string value);
		void NotifyPayment(IOrderFull order);
		void NotifySessionEnd();
	}
}