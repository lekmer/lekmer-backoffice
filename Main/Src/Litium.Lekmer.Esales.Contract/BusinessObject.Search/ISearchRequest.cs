namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	public interface ISearchRequest : IEsalesRequest
	{
		/// <summary>
		/// The text to search for. The value can be a single word or a phrase.
		/// </summary>
		string SearchPhrase { get; set; }

		/// <summary>
		/// The text to filter for. The value can be a single word or a phrase.
		/// </summary>
		string FilterPhrase { get; set; }

		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		IPaging Paging { get; set; }
	}
}