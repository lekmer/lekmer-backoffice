using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesGeneralRequestV2 : IEsalesRequest
	{
		/// <summary>
		/// Additional request parameters.
		/// </summary>
		Dictionary<string, string> Parameters { get; set; }
	}
}