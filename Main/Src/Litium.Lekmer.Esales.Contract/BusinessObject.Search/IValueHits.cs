using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IValueHits : IResult
	{
		Collection<IValueInfo> ValuesInfo { get; set; }
	}
}