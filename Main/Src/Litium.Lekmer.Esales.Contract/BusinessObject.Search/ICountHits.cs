namespace Litium.Lekmer.Esales
{
	public interface ICountHits : IResult
	{
		int TotalHitCount { get; set; }
	}
}