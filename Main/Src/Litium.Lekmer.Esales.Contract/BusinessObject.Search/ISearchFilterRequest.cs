using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="ISearchRequest"/> to build a query and execute it.
	/// </summary>
	public interface ISearchFilterRequest : ISearchRequest
	{
		IFilterQuery FilterQuery { get; set; }
		IChannel Channel { get; set; }
	}
}