namespace Litium.Lekmer.Esales
{
	public interface IResult
	{
		EsalesResultType Type { get; }
	}
}