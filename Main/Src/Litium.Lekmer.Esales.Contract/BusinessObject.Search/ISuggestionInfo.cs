namespace Litium.Lekmer.Esales
{
	public interface ISuggestionInfo
	{
		int Rank { get; set; }
		double Relevance { get; set; }
		string Text { get; set; }
		string Ticket { get; set; }
	}
}