using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.TopList.Web
{
	public class BlockProductTopListEntityMapper : LekmerBlockEntityMapper<IBlockTopList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockTopList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));

			AddNavigationUrl(fragment, item);
		}

		protected virtual void AddNavigationUrl(Fragment fragment, IBlockTopList item)
		{
			var hasNavigationUrl = false;
			var navigationUrl = string.Empty;

			var customUrl = item.CustomUrl;
			if (!string.IsNullOrEmpty(customUrl))
			{
				hasNavigationUrl = true;
				navigationUrl = customUrl;
			}
			else if (item.LinkContentNodeId.HasValue)
			{
				var contentNode = IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current, item.LinkContentNodeId.Value);
				if (contentNode != null && !string.IsNullOrEmpty(contentNode.Url))
				{
					hasNavigationUrl = true;
					navigationUrl = UrlHelper.ResolveUrl(contentNode.Url);
				}
			}

			var hasButtonText = false;
			var buttonText = string.Empty;
			var aliasCommonName = string.Format(CultureInfo.CurrentCulture, item.AliasDefaultCommonName, item.Id);
			var alias = IoC.Resolve<IAliasSharedService>().GetByCommonName(UserContext.Current.Channel, aliasCommonName);
			if (alias != null && !string.IsNullOrEmpty(alias.Value))
			{
				hasButtonText = true;
				buttonText = alias.Value;
			}
			
			fragment.AddCondition("Block.HasNavigationUrl", hasNavigationUrl);
			fragment.AddVariable("Block.NavigationUrl", navigationUrl);
			fragment.AddCondition("Block.HasButtonText", hasButtonText);
			fragment.AddVariable("Block.ButtonText", buttonText);
		}
	}
}