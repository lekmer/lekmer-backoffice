﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.TwoSell
{
	public interface IRecommenderList
	{
		int Id { get; set; }
		string SessionId { get; set; }
		Collection<IRecommenderItem> Items { get; set; }
	}
}