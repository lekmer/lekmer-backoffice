﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.TwoSell
{
	public interface IRecommenderItem
	{
		int Id { get; set; }
		int RecommenderListId { get; set; }
		int? ProductId { get; set; }
		Collection<IRecommendationProduct> Suggestions { get; set; }
	}
}