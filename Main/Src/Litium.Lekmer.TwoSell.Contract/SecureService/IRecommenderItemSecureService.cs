﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.TwoSell
{
	public interface IRecommenderItemSecureService
	{
		IRecommenderItem Create();

		IRecommenderItem Save(IRecommenderItem recommenderItem);
		void Save(Collection<IRecommenderItem> recommenderItems);
	}
}