﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.ServiceCenter
{
	public class ServiceCenterMessageArgs
	{
		public IChannel Channel { get; set; }
		public string CategoryId { get; set; }
		public string CategoryValue { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }
		public string SenderFirstName { get; set; }
		public string SenderLastName { get; set; }
		public string SenderEmail { get; set; }
		public int? SenderCustomerId { get; set; }
		public int? SenderOrderNo { get; set; }
		public string CivicNumber { get; set; }

		public ServiceCenterMessageArgs(
			IChannel channel,
			string categoryId,
			string categoryValue,
			string title,
			string message,
			string senderFirstName,
			string senderLastName,
			string senderEmail,
			int? senderCustomerId,
			int? senderOrderNo,
			string civicNumber)
		{
			Channel = channel;
			CategoryId = categoryId;
			CategoryValue = categoryValue;
			Title = title;
			Message = message;
			SenderFirstName = senderFirstName;
			SenderLastName = senderLastName;
			SenderEmail = senderEmail;
			SenderCustomerId = senderCustomerId;
			SenderOrderNo = senderOrderNo;
			CivicNumber = civicNumber;
		}
	}
}