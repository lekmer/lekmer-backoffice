﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.ServiceCenter.Setting
{
	public class ServiceCenterSetting:SettingBase
	{
		#region Singleton

		private static readonly ServiceCenterSetting _instance = new ServiceCenterSetting();

		private ServiceCenterSetting()
		{
		}

		public static ServiceCenterSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "ServiceCenterSetting";

		protected override string StorageName
		{
			get { return "ServiceCenterSetting"; }
		}

		public string WebServiceUrl
		{
			get { return GetString(_groupName, "WebServiceUrl"); }
		}

		public string EmailTo
		{
			get { return GetString(_groupName, "EmailTo"); }
		}
		public string GetChannelEmailTo(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(EmailTo, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue, seperator);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
		// value1,value2, ... , valueN => List<value>
		public ICollection<string> ConvertToList(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { seperator }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
	}
}
