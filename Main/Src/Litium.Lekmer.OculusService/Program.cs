﻿using System;
using System.IO;
using System.Reflection;
using ServiceBase = System.ServiceProcess.ServiceBase;

namespace Litium.Lekmer.OculusService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			Environment.CurrentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[] 
			{ 
				new OculusServiceHost() 
			};

			ServiceBase.Run(ServicesToRun);
		}
	}
}