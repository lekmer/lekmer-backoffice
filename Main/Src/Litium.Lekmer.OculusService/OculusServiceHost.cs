﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using Litium.Lekmer.OculusService.Utils;
using log4net;
using log4net.Config;

namespace Litium.Lekmer.OculusService
{
	public partial class OculusServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(typeof(OculusServiceHost));
		private Timer _timer;

		public OculusServiceHost()
		{
			InitializeComponent();

			XmlConfigurator.Configure();
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
		}

		protected override void OnStart(string[] args)
		{
			Exception serviceError = null;
			try
			{
				_log.Info("Starting Lekmer Oculus Service.");

				_timer = ScheduledTaskUtil.SetupTimer();
				_timer.Elapsed += OnTimerElapsed;
				_timer.Start();
				
				_log.Info("Lekmer Oculus Service started successfuly.");
			}
			catch (Exception exception)
			{
				serviceError = new Exception("Unable to start service. See inner exception for details.", exception);
			}
			finally
			{
				if (serviceError != null)
				{
					try
					{
						_log.Error("Failed to start Lekmer Oculus Service.", serviceError);
					}
					catch (Exception exception)
					{
						
						Trace.WriteLine("Failed to start service. An error occurred:");
						Trace.WriteLine(serviceError.ToString());
						Trace.WriteLine("Failed to log error:");
						Trace.WriteLine(exception.ToString());
					}
					finally
					{
						Stop();
					}
				}
			}
		}

		protected override void OnStop()
		{
			try
			{
				_timer.Stop();

				_log.Info("Lekmer Oculus Service stopped successfuly.");

				base.OnStop();
			}
			catch (Exception exception)
			{
				_log.Error("Error occurred while stopping Lekmer Oculus Service.", exception);
			}
		}

		private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			try
			{
				_timer.Stop();

				_log.Error("ERROR occurred during Lekmer Oculus Service execution. Stopping service", e.ExceptionObject is Exception ? e.ExceptionObject as Exception : null);
			}
			finally
			{
				Stop();
			}
		}

		private void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				OculusXmlUtil oculusXmlUtil = new OculusXmlUtil(_log);
				oculusXmlUtil.ImportProductSimilar();
			}
			catch (Exception exception)
			{
				_log.Error("Failed to import similar products", exception);
			}
		}
	}
}