﻿using System.ComponentModel;
using System.Configuration.Install;


namespace Litium.Lekmer.OculusService
{
	[RunInstaller(true)]
	public partial class OculusServiceHostInstaller : Installer
	{
		public OculusServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}