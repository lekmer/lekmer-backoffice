﻿namespace Litium.Lekmer.OculusService
{
	partial class OculusServiceHostInstaller
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceHostProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			this.oculusServiceInstaller = new System.ServiceProcess.ServiceInstaller();
			// 
			// serviceHostProcessInstaller
			// 
			this.serviceHostProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
			this.serviceHostProcessInstaller.Password = null;
			this.serviceHostProcessInstaller.Username = null;
			// 
			// oculusServiceInstaller
			// 
			this.oculusServiceInstaller.ServiceName = "Lekmer Oculus Service";
			// 
			// OculusServiceHostInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceHostProcessInstaller,
            this.oculusServiceInstaller});

		}

		#endregion

		private System.ServiceProcess.ServiceProcessInstaller serviceHostProcessInstaller;
		private System.ServiceProcess.ServiceInstaller oculusServiceInstaller;
	}
}