﻿using System.Globalization;
using System.Text;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesTopSellersV2Control : LekmerBlockControlBase<IBlockEsalesTopSellersV2>
	{
		private readonly IBlockEsalesTopSellersServiceV2 _blockEsalesTopSellersServiceV2;

		private ProductCollection _products;

		public BlockEsalesTopSellersV2Control(
			ITemplateFactory templateFactory,
			IBlockEsalesTopSellersServiceV2 blockEsalesTopSellersServiceV2)
			: base(templateFactory)
		{
			_blockEsalesTopSellersServiceV2 = blockEsalesTopSellersServiceV2;
		}

		protected override IBlockEsalesTopSellersV2 GetBlockById(int blockId)
		{
			return _blockEsalesTopSellersServiceV2.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			if (_products.Count <= 0)
			{
				return new BlockContent();
			}
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			var productIds = RenderProductIdsArrayParam();
			string head = RenderFragment("Head", productIds);
			string footer = RenderFragment("Footer", productIds);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName, string productIds)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", productIds);
			return fragment.Render() ?? string.Empty;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};
			return grid.Render();
		}

		private string RenderProductIdsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			for (int i = 0; i < _products.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _products[i].Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", _products[i].Id));
				}
			}
			return itemBuilder.ToString();
		}

		private void Initialize()
		{
			if (EsalesGeneralResponse == null)
			{
				_products = new ProductCollection();
				return;
			}

			_products = _blockEsalesTopSellersServiceV2.GetTopSellersFromEsalesResponse(UserContext.Current, Block, EsalesGeneralResponse);
		}
	}
}