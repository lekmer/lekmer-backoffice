using System.Globalization;
using Litium.Lekmer.SiteStructure;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesCartEntityMapper : LekmerBlockEntityMapper<IBlockList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}