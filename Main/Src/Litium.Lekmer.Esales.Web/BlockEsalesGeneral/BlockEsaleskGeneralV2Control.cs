﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.Product;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Esales.Web
{
	/// <summary>
	/// eSales general control.
	/// </summary> 
	public class BlockEsalesGeneralV2Control : LekmerBlockControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		private readonly IEsalesGeneralServiceV2 _esalesGeneralServiceV2;
		private readonly IBlockEsalesRecommendServiceV2 _blockEsalesRecommendServiceV2;
		private readonly IBlockEsalesTopSellersServiceV2 _blockEsalesTopSellersServiceV2;

		public BlockEsalesGeneralV2Control(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IEsalesGeneralServiceV2 esalesGeneralV2Service,
			IBlockEsalesRecommendServiceV2 blockEsalesRecommendServiceV2,
			IBlockEsalesTopSellersServiceV2 blockEsalesTopSellersServiceV2)
			: base(templateFactory)
		{
			_blockService = blockService;
			_esalesGeneralServiceV2 = esalesGeneralV2Service;
			_blockEsalesRecommendServiceV2 = blockEsalesRecommendServiceV2;
			_blockEsalesTopSellersServiceV2 = blockEsalesTopSellersServiceV2;
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var parameters = new Dictionary<string, string>();

			foreach (IContentAreaFull area in ((IContentPageFull)ContentPage).ContentAreas)
			{
				var blocks = area.Blocks;
				var recommendV2Blocks = blocks.Where(b => b.BlockType.CommonName == BlockTypeInfo.EsalesRecommendV2.ToString());
				if (recommendV2Blocks.Any())
				{
					foreach (var block in recommendV2Blocks)
					{
						var blockEsalesRecommendV2 = _blockEsalesRecommendServiceV2.GetById(UserContext.Current, block.Id);
						blockEsalesRecommendV2.PopulateEsalesParameters(parameters);
					}
				}

				var topSellersV2Blocks = blocks.Where(b => b.BlockType.CommonName == BlockTypeInfo.EsalesTopSellersV2.ToString());
				if (topSellersV2Blocks.Any())
				{
					foreach (var block in topSellersV2Blocks)
					{
						var blockEsalesTopSellersV2 = _blockEsalesTopSellersServiceV2.GetById(UserContext.Current, block.Id);
						blockEsalesTopSellersV2.PopulateEsalesParameters(parameters);
					}
				}
			}

			switch (ContentPage.ContentPageTypeId)
			{
				case (int)SiteStructure.ContentPageType.ProductPage:
					{
						if (Product == null) return new BlockContent(null, "[ Can't render product general block on this page ]");

						var productId = Product.Id.ToString(CultureInfo.InvariantCulture);
						parameters.Add(EsalesArguments.ProductKey, productId);
						parameters.Add(EsalesArguments.Products, productId);

						if (((ILekmerProductView)Product).TotalNumberInStock > 0)
						{
							parameters.Add(EsalesArguments.FilterNoStock, EsalesConstants.DefaultFakeFilter);
						}

						IProductPagePanelSettingV2 panelSetting = new ProductPagePanelSettingV2(UserContext.Current.Channel.CommonName);
						var esalesResponse = (IEsalesResponse)_esalesGeneralServiceV2.GetResults(panelSetting.PanelPath, parameters);
						EsalesGeneralResponse = esalesResponse;
					}
					break;

				case (int)SiteStructure.ContentPageType.ContentPage:
					{
						IStartPagePanelSettingV2 panelSetting = new StartPagePanelSettingV2(UserContext.Current.Channel.CommonName);
						var esalesResponse = (IEsalesResponse)_esalesGeneralServiceV2.GetResults(panelSetting.PanelPath, parameters);
						EsalesGeneralResponse = esalesResponse;
					}
					break;
			}

			return new BlockContent(string.Empty, string.Empty, string.Empty);
		}
	}
}
