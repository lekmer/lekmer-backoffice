﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class AdInfoEntityMapper : EntityMapper<IAdInfo>
	{
		public override void AddEntityVariables(Fragment fragment, IAdInfo item)
		{
			fragment.AddVariable("Ad.Key", item.Key);
			fragment.AddVariable("Ad.Ticket", item.Ticket);
			fragment.AddVariable("Ad.CampaignKey", item.CampaignKey);

			fragment.AddVariable("Ad.Format", item.Format);
			fragment.AddVariable("Ad.Site", item.Site);
			fragment.AddVariable("Ad.Gender", item.Gender);
			fragment.AddVariable("Ad.ProductCategory", item.ProductCategory);
			fragment.AddVariable("Ad.Title", item.Title);
			fragment.AddVariable("Ad.LandingPageUrl", item.LandingPageUrl);
			fragment.AddVariable("Ad.ImageUrl", item.ImageUrl);
			fragment.AddVariable("Ad.Tags", item.Tags);
		}
	}
}