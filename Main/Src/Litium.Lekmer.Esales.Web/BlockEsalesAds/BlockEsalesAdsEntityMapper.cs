using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesAdsEntityMapper : LekmerBlockEntityMapper<IBlockEsalesAds>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockEsalesAds item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}