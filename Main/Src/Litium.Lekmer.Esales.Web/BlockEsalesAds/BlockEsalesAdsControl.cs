﻿using Litium.Lekmer.Avail.Web;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesAdsControl : BlockProductPageControlBase<IBlockEsalesAds>
	{
		private readonly IBlockEsalesAdsService _blockEsalesAdsService;
		private readonly IEsalesAdsService _esalesAdsService;

		private string _panelPath;
		private string _customerKey;
		private int _itemsToReturn;

		private IAdHits _ads;

		public BlockEsalesAdsControl(
			ITemplateFactory templateFactory,
			IBlockEsalesAdsService blockEsalesAdsService,
			IEsalesAdsService esalesAdsService)
			: base(templateFactory)
		{
			_blockEsalesAdsService = blockEsalesAdsService;
			_esalesAdsService = esalesAdsService;
		}

		protected override IBlockEsalesAds GetBlockById(int blockId)
		{
			return _blockEsalesAdsService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			if (_ads.AdsInfo.Count <= 0)
			{
				return new BlockContent();
			}

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("AdList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);
			string body = fragmentContent.Render() ?? string.Empty;

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, body, footer);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render() ?? string.Empty;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IAdInfo>
			{
				Items = _ads.AdsInfo,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "AdList",
				RowFragmentName = "AdRow",
				ItemFragmentName = "Ad",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}


		private void Initialize()
		{
			InitializePanelPath();
			InitializeCustomerKey();
			InitializeItemsToReturn();

			_ads = _esalesAdsService.FindAds(UserContext.Current, _panelPath, Block.Format, Block.Gender, Block.ProductCategory, _customerKey, _itemsToReturn);
		}

		private void InitializePanelPath()
		{
			IEsalesAdsPanelSetting panelSetting = new EsalesAdsPanelSetting();

			_panelPath = Block.PanelPath;

			if (_panelPath.IsEmpty())
			{
				_panelPath = panelSetting.PanelPath;
			}
		}

		private void InitializeCustomerKey()
		{
			_customerKey = string.Empty;

			var customerSession = IoC.Resolve<ICustomerSession>();
			_customerKey = CustomerIdentificationCookie.GetCustomerIdentificationId(customerSession.SignedInCustomer);
		}

		private void InitializeItemsToReturn()
		{
			_itemsToReturn = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
		}
	}
}