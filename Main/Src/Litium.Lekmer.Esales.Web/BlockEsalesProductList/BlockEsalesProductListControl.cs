﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Litium.Lekmer.Esales.Web.BlockEsalesProductList;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesProductListControl : BlockControlBase<IBlockEsalesProductList>, IBlockEsalesControl
	{
		private readonly IBlockEsalesProductListService _blockEsalesProductListService;
		private readonly IEsalesProductListService _esalesProductListService;
		private readonly IEsalesModelService _esalesModelService;

		private int _itemsToReturn;

		private ProductCollection _products;

		private IEsalesModelComponent _esalesModelComponent;

		private IEsalesPanelRequest _esalesPanelRequest;
		public IEsalesPanelRequest EsalesPanelRequest
		{
			get { return _esalesPanelRequest ?? (_esalesPanelRequest = CreateEsalesPanelRequest()); }
		}

		public IEsalesResponse EsalesResponse { get; set; }

		public new IBlockEsalesProductList Block { get; private set; }

		public BlockEsalesProductListControl(
			ITemplateFactory templateFactory,
			IBlockEsalesProductListService blockEsalesProductListService,
			IEsalesProductListService esalesProductListService,
			IEsalesModelService esalesModelService)
			: base(templateFactory)
		{
			_blockEsalesProductListService = blockEsalesProductListService;
			_esalesProductListService = esalesProductListService;
			_esalesModelService = esalesModelService;
		}

		protected override IBlockEsalesProductList GetBlockById(int blockId)
		{
			return _blockEsalesProductListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			if (EsalesResponse == null)
			{
				// if no eSales data available, render empty fragment
				return RenderNoResultsContent();
			}

			if (_products.Count <= 0)
			{
				return RenderNoResultsContent();
			}

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");

			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render() ?? string.Empty;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		protected virtual BlockContent RenderNoResultsContent()
		{
			var fragment = Template.GetFragment("Empty");

			fragment.AddEntity(Block);

			return new BlockContent(string.Empty, fragment.Render(), string.Empty);
		}

		private void Initialize()
		{
			InitializeBlock();
			InitializeEsalesModel();
			InitializeItemsToReturn();

			_products = GetProducts();
		}

		private void InitializeBlock()
		{
			if (ContentPage == null || ContentArea == null || BlockId == 0)
			{
				throw new InvalidOperationException("ContentPage, ContentArea and BlockId must be set before calling Render.");
			}

			if (Block == null)
			{
				Block = GetBlockById(BlockId);
			}

			if (Block == null)
			{
				throw new BlockNotFoundException(string.Format(CultureInfo.InvariantCulture, "Block with id {0} could not be found.", BlockId));
			}
		}

		private void InitializeEsalesModel()
		{
			if (ContentPage == null || Block == null)
			{
				throw new InvalidOperationException("ContentPage and Block must be set before calling InitializeEsalesModel.");
			}

			if (Block.EsalesSetting.EsalesModelComponentId.HasValue)
			{
				_esalesModelComponent = _esalesModelService.GetModelComponentById(ContentPage.SiteStructureRegistryId, Block.EsalesSetting.EsalesModelComponentId.Value);
			}
		}

		private void InitializeItemsToReturn()
		{
			_itemsToReturn = Math.Max(Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount, Block.Setting.ActualTotalItemCount);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ProductCollection GetProducts()
		{
			if (EsalesResponse != null)
			{
				return _esalesProductListService.GetProducts(UserContext.Current, EsalesResponse, _esalesModelComponent);
			}

			return new ProductCollection();
		}

		protected virtual IEsalesPanelRequest CreateEsalesPanelRequest()
		{
			InitializeBlock();
			InitializeEsalesModel();
			InitializeItemsToReturn();

			EsalesPanelRequestBuilder esalesPanelRequestBuilder = new EsalesPanelRequestBuilder
			{
				EsalesModelComponent = _esalesModelComponent,
				Channel = Channel.Current,
				ItemsToReturn = _itemsToReturn
			};

			IEsalesPanelRequest panelRequest = esalesPanelRequestBuilder.BuildEsalesPanelRequest();

			return panelRequest;
		}
	}
}
