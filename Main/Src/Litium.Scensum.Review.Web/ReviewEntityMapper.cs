using System.Globalization;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Review.Web
{
	public class ReviewEntityMapper : EntityMapper<IReview>
	{
		private readonly IFormatter _formatter;

		public ReviewEntityMapper(IFormatter formatter)
		{
			_formatter = formatter;
		}

		public override void AddEntityVariables(Fragment fragment, IReview item)
		{
			fragment.AddVariable("Review.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Review.Author", item.Author);
			fragment.AddVariable("Review.Message", item.Message);
			fragment.AddVariable("Review.Rating", item.Rating.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Review.CreatedDate", _formatter.FormatShortDate(Channel.Current, item.CreatedDate));
		}
	}
}