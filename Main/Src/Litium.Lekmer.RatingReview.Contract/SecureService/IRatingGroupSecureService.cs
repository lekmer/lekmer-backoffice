﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupSecureService
	{
		IRatingGroup Save(ISystemUserFull systemUserFull, IRatingGroup ratingGroup);

		IRatingGroup SaveWithRatings(ISystemUserFull systemUserFull, IRatingGroup ratingGroup, Collection<IRatingGroupRating> newRatings);

		void Delete(ISystemUserFull systemUserFull, int ratingGroupId);

		IRatingGroup GetById(int ratingGroupId);

		Collection<IRatingGroup> GetAllByFolder(int ratingGroupFolderId);

		Collection<IRatingGroup> Search(string searchCriteria);
	}
}