using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductLatestFeedbackListSecureService
	{
		IBlockProductLatestFeedbackList Create();

		IBlockProductLatestFeedbackList GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockProductLatestFeedbackList block);
	}
}