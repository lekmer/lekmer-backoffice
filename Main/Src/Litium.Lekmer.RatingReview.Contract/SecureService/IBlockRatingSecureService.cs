﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingSecureService
	{
		void SaveAll(ISystemUserFull systemUserFull, int blockId, Collection<IBlockRating> ratings);

		void Delete(ISystemUserFull systemUserFull, int blockId);

		Collection<IBlockRating> GetAllByBlock(int blockId);
	}
}