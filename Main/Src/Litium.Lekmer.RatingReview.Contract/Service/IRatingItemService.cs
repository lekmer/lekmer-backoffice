﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemService
	{
		Collection<IRatingItem> GetAllByRating(IUserContext context, int ratingId);
	}
}