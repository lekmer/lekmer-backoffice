using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingService
	{
		Collection<IBlockRating> GetAllByBlock(int blockId);
	}
}