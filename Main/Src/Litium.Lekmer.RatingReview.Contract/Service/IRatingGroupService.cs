﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		Collection<IRatingGroup> GetAllByProduct(IUserContext context, int productId, int productCategoryId);
		IRatingGroup GetById(int ratingGroupId);
	}
}