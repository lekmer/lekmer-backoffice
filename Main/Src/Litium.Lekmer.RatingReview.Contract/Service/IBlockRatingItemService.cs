using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingItemService
	{
		Collection<IBlockRatingItem> GetAllByBlock(int blockId);
	}
}