using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockBestRatedProductListService
	{
		IBlockBestRatedProductList GetById(IUserContext context, int blockId);
	}
}