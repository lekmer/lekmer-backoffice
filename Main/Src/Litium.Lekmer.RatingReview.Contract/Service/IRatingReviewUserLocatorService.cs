﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewUserLocatorService
	{
		IRatingReviewUser GetRatingReviewUser(IUserContext context);
		IRatingReviewUser GetRatingReviewUser(IUserContext context, int customerId);
	}
}