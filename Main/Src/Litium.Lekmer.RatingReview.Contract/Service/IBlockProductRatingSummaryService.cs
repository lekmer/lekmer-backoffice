using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductRatingSummaryService
	{
		IBlockProductRatingSummary GetById(IUserContext context, int id);
	}
}