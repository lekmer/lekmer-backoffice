﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupCategoryService
	{
		Collection<IRatingGroupCategory> GetAllByCategory(int categoryId);
	}
}