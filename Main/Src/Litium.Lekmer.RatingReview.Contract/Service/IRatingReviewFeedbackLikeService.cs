﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewFeedbackLikeService
	{
		IRatingReviewFeedbackLike Create(IUserContext context);

		int Insert(IUserContext context, IRatingReviewFeedbackLike ratingReviewFeedbackLike);
	}
}