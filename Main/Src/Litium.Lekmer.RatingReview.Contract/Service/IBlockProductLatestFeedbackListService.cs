using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductLatestFeedbackListService
	{
		IBlockProductLatestFeedbackList GetById(IUserContext context, int blockId);
	}
}