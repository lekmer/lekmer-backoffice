﻿namespace Litium.Lekmer.RatingReview
{
	public interface IReviewService
	{
		IReview Save(IReview review);

		IReview GetByFeedback(int ratingReviewFeedbackId);
	}
}