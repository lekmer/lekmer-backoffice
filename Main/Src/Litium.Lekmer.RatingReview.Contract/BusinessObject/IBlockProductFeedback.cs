﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductFeedback : IBlock
	{
		bool AllowReview { get; set; }
	}
}