﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupCategory : IBusinessObjectBase
	{
		int RatingGroupId { get; set; }
		int CategoryId { get; set; }
		bool IncludeSubcategories { get; set; }
	}
}