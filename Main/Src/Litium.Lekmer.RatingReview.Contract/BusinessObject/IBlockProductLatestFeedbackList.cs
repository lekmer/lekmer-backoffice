﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductLatestFeedbackList : IBlock
	{
		int NumberOfItems { get; set; }
	}
}