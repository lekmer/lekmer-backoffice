﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRating : IBusinessObjectBase
	{
		int Id { get; set; }
		int RatingFolderId { get; set; }
		int RatingRegistryId { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		string Description { get; set; }
		bool CommonForVariants { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<IRatingItem> RatingItems { get; set; }
	}
}