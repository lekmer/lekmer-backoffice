﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupRating : IBusinessObjectBase
	{
		int RatingGroupId { get; set; }
		int RatingId { get; set; }
		int Ordinal { get; set; }
	}
}