﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroup : IBusinessObjectBase
	{
		int Id { get; set; }
		int RatingGroupFolderId { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		string Description { get; set; }
	}
}