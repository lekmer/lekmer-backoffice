﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewStatus : IBusinessObjectBase
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
	}
}