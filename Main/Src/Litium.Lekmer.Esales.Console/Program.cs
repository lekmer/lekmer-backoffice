﻿using System;
using System.Linq;
using System.Reflection;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Lekmer.Cache.Setting;
using Litium.Lekmer.Esales.Exporter;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Esales.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main(string[] args)
		{
			if (args.Any(a => a == "help") || args.Length == 0)
			{
				System.Console.WriteLine("Posible arguments:");
				System.Console.WriteLine("    /full  runs export to esales of all products;");
				System.Console.WriteLine("    /inc  runs incremental export to esales of changed products;");
				return;
			}

			if (args.Any(a => a == "/full"))
			{
				InitializeCache();
				ExportProductToEsales();
			}

			if (args.Any(a => a == "/inc"))
			{
				InitializeCache();
				ExportProductToEsalesIncremental();
			}
		}

		private static void ExportProductToEsales()
		{
			try
			{
				var esalesProductInfoExporter = IoC.Resolve<IExporter>("EsalesProductInfoExporter");
				esalesProductInfoExporter.Execute();
			}
			catch (Exception ex)
			{
				_log.Error("Esales.Console failed.", ex);
			}
		}

		private static void ExportProductToEsalesIncremental()
		{
			try
			{
				var esalesProductInfoIncrementalExporter = IoC.Resolve<IExporter>("EsalesProductInfoIncrementalExporter");
				esalesProductInfoIncrementalExporter.Execute();
			}
			catch (Exception ex)
			{
				_log.Error("Esales.Console failed.", ex);
			}
		}

		private static void InitializeCache()
		{
			try
			{
				CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();

				CacheUpdateCleaner.WorkerCleanInterval = new LekmerCacheUpdateCleanerSetting().CacheUpdateIntervalSeconds*1000;
#if DEBUG
				CacheUpdateCleaner.WorkerCleanInterval = 1*1000; // Clean interval lowered in debug mode.
#endif
				CacheUpdateCleaner.StartWorker();
			}
			catch (Exception ex)
			{
				_log.Error("InitializeCache failed.", ex);
			}
		}
	}
}
