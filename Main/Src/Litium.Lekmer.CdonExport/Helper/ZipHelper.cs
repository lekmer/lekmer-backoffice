﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace Litium.Lekmer.CdonExport
{
	public class ZipHelper
	{
		public void ZipFile(string fileName, string zipName)
		{
			using (var stream = new ZipOutputStream(File.Create(zipName)))
			{
				var entry = new ZipEntry(Path.GetFileName(fileName))
				{
					DateTime = DateTime.Now
				};

				stream.PutNextEntry(entry);
				stream.SetLevel(9); // 0 - store only to 9 - means best compression

				using (var fileStream = File.OpenRead(fileName))
				{
					var buffer = new byte[4096];
					int sourceBytes;
					do
					{
						sourceBytes = fileStream.Read(buffer, 0, buffer.Length);
						stream.Write(buffer, 0, sourceBytes);
					} while (sourceBytes > 0);
				}

				stream.Finish();
				stream.Close();
			}
		}
	}
}