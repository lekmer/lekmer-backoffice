﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportSetting : SettingBase, ICdonExportSetting
	{
		protected override string StorageName
		{
			get { return "CdonExport"; }
		}

		protected virtual string GroupName
		{
			get { return "CdonExportSettings"; }
		}


		// Properties

		public int ExportInterval
		{
			get { return GetInt32(GroupName, "ExportInterval", 30); }
		}

		public int ExecutionHour
		{
			get { return GetInt32(GroupName, "ExecutionHour", 10); }
		}

		public int ExecutionMinute
		{
			get { return GetInt32(GroupName, "ExecutionMinute", 0); }
		}

		public int IncrementalExportInterval
		{
			get { return GetInt32(GroupName, "IncrementalExportInterval", 30); }
		}

		public int IncrementalExportExecutionHour
		{
			get { return GetInt32(GroupName, "IncrementalExportExecutionHour", 10); }
		}

		public int IncrementalExportExecutionMinute
		{
			get { return GetInt32(GroupName, "IncrementalExportExecutionMinute", 0); }
		}

		public string ExcludeChannels
		{
			get { return GetString(GroupName, "ExcludeChannels"); }
		}

		public string NotRoundPriceInChannels
		{
			get { return GetString(GroupName, "NotRoundPriceInChannels"); }
		}

		public string ApplicationName
		{
			get { return GetString(GroupName, "ApplicationName"); }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}

		public string FileNameFullExport
		{
			get { return GetString(GroupName, "FileNameFullExport"); }
		}

		public string FileNameIncrementalExport
		{
			get { return GetString(GroupName, "FileNameIncrementalExport"); }
		}

		public string ZipFileDirectory
		{
			get { return GetString(GroupName, "ZipFileDirectory"); }
		}

		public string ZipFileExtension
		{
			get { return GetString(GroupName, "ZipFileExtension"); }
		}

		public string SizeCommonName
		{
			get { return GetString(GroupName, "SizeCommonName"); }
		}

		public string ColorTagGroupCommonName
		{
			get { return GetString(GroupName, "ColorTagGroupCommonName"); }
		}

		public string ExcludeTagGroups
		{
			get { return GetString(GroupName, "ExcludeTagGroups"); }
		}

		public string SpecificTagGroupIds
		{
			get { return GetString(GroupName, "SpecificTagGroupIds"); }
		}

		public string NotShoesCategoryIds
		{
			get { return GetString(GroupName, "NotShoesCategoryIds"); }
		}

		public string TopLevelCategoryIds
		{
			get { return GetString(GroupName, "TopLevelCategoryIds"); }
		}

		public bool NeedToValidate
		{
			get { return GetBoolean(GroupName, "NeedToValidate"); }
		}

		public string XsdFilePath
		{
			get { return GetString(GroupName, "XsdFilePath"); }
		}

		public int PageSize
		{
			get { return GetInt32(GroupName, "PageSize", 100); }
		}

		public int BatchSize
		{
			get { return GetInt32(GroupName, "BatchSize", 100); }
		}

		public int MonthPurchasedAgo
		{
			get { return GetInt32(GroupName, "MonthPurchasedAgo", 0); }
		}

		// Tag <-> Alias logic
		public string ChannelTemplateIds
		{
			get { return GetString(GroupName, "ChannelTemplateIds"); }
		}

		public string GetChannelTemplateId(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ChannelTemplateIds);

			if (dictionary != null && dictionary.ContainsKey(channelName))
			{
				return dictionary[channelName];
			}

			return channelName;
		}

		public string FragmentName
		{
			get { return GetString(GroupName, "FragmentName"); }
		}

		public string AliasBegin
		{
			get { return GetString(GroupName, "AliasBegin"); }
		}

		public string AliasBeginReplace
		{
			get { return GetString(GroupName, "AliasBeginReplace"); }
		}

		public string AliasEnd
		{
			get { return GetString(GroupName, "AliasEnd"); }
		}

		public string AliasEndReplace
		{
			get { return GetString(GroupName, "AliasEndReplace"); }
		}

		public string MainKey
		{
			get { return GetString(GroupName, "MainKey"); }
		}


		// value1,value2, ... , valueN => List<value>
		protected virtual ICollection<string> ConvertToList(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
	}
}