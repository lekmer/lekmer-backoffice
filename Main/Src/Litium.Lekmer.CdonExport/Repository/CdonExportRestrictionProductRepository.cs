﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionProductRepository
	{
		protected virtual DataMapperBase<ICdonExportRestrictionItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICdonExportRestrictionItem>(dataReader);
		}

		// Products.

		public virtual void InsertIncludeProduct(ICdonExportRestrictionItem product, string productChannelIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductChannelIds", productChannelIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ProductId", product.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Reason", product.Reason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", product.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", product.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionProductRepository.InsertIncludeProduct");

			new DataHandler().ExecuteCommandWithReturnValue("[export].[pCdonExportIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertRestrictionProduct(ICdonExportRestrictionItem product, string productChannelIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductChannelIds", productChannelIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ProductId", product.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Reason", product.Reason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", product.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", product.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionProductRepository.InsertRestrictionProduct");

			new DataHandler().ExecuteCommandWithReturnValue("[export].[pCdonExportRestrictionProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(string productIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductIds", productIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionProductRepository.DeleteIncludeProducts");

			new DataHandler().ExecuteCommand("[export].[pCdonExportIncludeProductDeleteAllByProduct]", parameters, dbSettings);
		}

		public virtual void DeleteRestrictionProducts(string productIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductIds", productIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionProductRepository.DeleteRestrictionProducts");

			new DataHandler().ExecuteCommand("[export].[pCdonExportRestrictionProductDeleteAllByProduct]", parameters, dbSettings);
		}

		public virtual Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionProductRepository.GetIncludeAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportIncludeProductGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionProductRepository.GetRestrictionAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportRestrictionProductGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}