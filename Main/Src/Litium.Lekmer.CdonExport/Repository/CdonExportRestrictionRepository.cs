﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionRepository
	{
		public virtual void DeleteAll()
		{
			IDataParameter[] parameters = new IDataParameter[0];

			var dbSettings = new DatabaseSetting("CdonExportRestrictionRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[export].[pCdonExportRestrictionDeleteAll]", parameters, dbSettings);
		}

		public virtual Dictionary<int, int> GetRestrictedProductsCount()
		{
			var restrictedProductsCount = new Dictionary<int, int>();

			IDataParameter[] parameters = new IDataParameter[0];

			var dbSettings = new DatabaseSetting("CdonExportRestrictionRepository.GetRestrictedProductsCount");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportRestrictedProductsCount]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					int channelId = dataReader.GetInt32(1);
					if (!restrictedProductsCount.ContainsKey(channelId))
					{
						restrictedProductsCount.Add(channelId, dataReader.GetInt32(0));
					}
				}
			}

			return restrictedProductsCount;
		}
	}
}