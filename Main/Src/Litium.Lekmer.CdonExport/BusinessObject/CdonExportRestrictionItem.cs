﻿using System;
using System.Collections.Generic;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonExportRestrictionItem : BusinessObjectBase, ICdonExportRestrictionItem
	{
		private int _productRegistryId;
		private int _itemId;
		private string _reason;
		private int? _userId;
		private DateTime _createdDate;
		private int _channelId;

		public int ProductRegistryId
		{
			get { return _productRegistryId; }
			set
			{
				CheckChanged(_productRegistryId, value);
				_productRegistryId = value;
			}
		}

		public int ItemId
		{
			get { return _itemId; }
			set
			{
				CheckChanged(_itemId, value);
				_itemId = value;
			}
		}

		public string Reason
		{
			get { return _reason; }
			set
			{
				CheckChanged(_reason, value);
				_reason = value;
			}
		}

		public int? UserId
		{
			get { return _userId; }
			set
			{
				CheckChanged(_userId, value);
				_userId = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public Dictionary<int, int> Channels { get; set; }
	}
}