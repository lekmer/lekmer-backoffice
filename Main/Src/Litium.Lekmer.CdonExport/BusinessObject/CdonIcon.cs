using System;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonIcon : ICdonIcon
	{
		public int Id { get; set; }
		public int CdonIconId { get; set; }
		public string Title { get; set; }
	}
}