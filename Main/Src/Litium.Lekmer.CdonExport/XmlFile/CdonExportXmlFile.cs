﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Attribute = Litium.Lekmer.CdonExport.CdonProductAttribute;
using DefaultAttribute = Litium.Lekmer.CdonExport.CdonProductDefaultAttribute;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportXmlFile : BaseXmlFile
	{
		private readonly string _tagAttributeId = "99{0}";
		private readonly string _sizeAttributeId = "98{0}";
		private readonly string _colorAttributeId = "97{0}";
		private readonly string _noSizeHeppo = "251";
		private readonly string _noSizeLekmer = "000";
		private readonly string _colorSeperator = "/";
		private readonly string _commaSeperator = ",";
		private readonly char _erpSeperator = '-';

		private ICdonExportSetting _cdonExportSetting;
		private ICdonExportService _cdonExportService;
		private CdonExportType _cdonExportType;
		private RestrictionHelper _restrictionHelper;
		private IMediaUrlService _mediaUrlService;

		private bool _isInitialized;
		private XmlDocument _xmlDocument;
		private Dictionary<int, ICategoryTree> _classes;
		private Dictionary<int, Dictionary<int, IBrand>> _brands;
		private Dictionary<int, Dictionary<int, ITagGroup>> _tagGroups;
		private Collection<ICdonProductInfo> _products;
		private Collection<int> _mainCategories;
		private string[] _notRoundPriceInChannels;
		private Collection<int> _topLevelCategoryIds;
		private Dictionary<int, int> _productMapping;
		private Dictionary<int, Dictionary<string, string>> _tagAliasCollection;
		private Dictionary<int, ICdonIcon> _icons;

		private Dictionary<int, int> _productIconsUsed = new Dictionary<int, int>();
		private Dictionary<int, Dictionary<int, int>> _productTagsUsed = new Dictionary<int, Dictionary<int, int>>();
		private bool _ageFromMonthUsed = false;
		private bool _ageToMonthUsed = false;

		private bool _isTemaTagExported;

		private Collection<string> _specificTagGroupIds;
		protected Collection<string> SpecificTagGroupIds
		{
			get { return _specificTagGroupIds ?? (_specificTagGroupIds = new Collection<string>(_cdonExportSetting.SpecificTagGroupIds.Split(','))); }
		}

		public void Initialize(ICdonExportSetting cdonExportSetting, ICdonExportService cdonExportService, CdonExportType cdonExportType)
		{
			if (cdonExportSetting == null) throw new ArgumentNullException("cdonExportSetting");
			if (cdonExportService == null) throw new ArgumentNullException("cdonExportService");

			_cdonExportSetting = cdonExportSetting;
			_cdonExportService = cdonExportService;
			_cdonExportType = cdonExportType;
			_mediaUrlService = IoC.Resolve<IMediaUrlService>();

			if (_restrictionHelper == null)
			{
				_restrictionHelper = new RestrictionHelper(_cdonExportSetting, cdonExportService);
			}

			_mainCategories = new Collection<int>();

			_notRoundPriceInChannels = cdonExportSetting.NotRoundPriceInChannels.Split(',');

			_topLevelCategoryIds = new Collection<int>();
			foreach (var id in _cdonExportSetting.TopLevelCategoryIds.Split(','))
			{
				int categoryId;
				if (int.TryParse(id, out categoryId))
				{
					_topLevelCategoryIds.Add(categoryId);
				}
			}

			_productMapping = _cdonExportService.ProductMappingGetAll();

			_classes = _cdonExportService.GetAllCategoryTrees(_topLevelCategoryIds);
			_brands = _cdonExportService.GetAllBrands();
			_tagGroups = cdonExportService.GetAllTagGroups();
			_icons = cdonExportService.GetAllIcons();
			_tagAliasCollection = _cdonExportService.GetAllTagAliases();
			_products = _cdonExportType == CdonExportType.FULL ? _cdonExportService.GetAllProducts() : _cdonExportService.GetChangedProducts();

			// Exclude restricted.
			var filteredProducts = _restrictionHelper.FilterProducts(_products, FindCategoryTree(_classes, _classes.Keys.First()));

			_cdonExportService.PopulateVariantErpIds(filteredProducts);
			_products = _cdonExportService.PopulateVariants(filteredProducts);

			_isInitialized = true;
		}

		public int Save(Stream stream)
		{
			if (!_isInitialized)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			if (_cdonExportType == CdonExportType.INCR && _products.Count == 0)
			{
				return -1;
			}

			string importPreviousId = _cdonExportService.GetLastItem();
			string importId = GetImportId(importPreviousId);
			int cdonExportId = _cdonExportService.Save(importId, _cdonExportType, CdonExportStatus.InProgress);

			try
			{
				_xmlDocument = CreateXmlDocument();

				XmlNode cdonShoppingMallImport = AddCdonShoppingMallImportNode();
				AddImportInfoNode(cdonShoppingMallImport, importId, importPreviousId);
				AddClassesNode(cdonShoppingMallImport);
				AddDimensionsNode(cdonShoppingMallImport);
				AddAttributesNode(cdonShoppingMallImport);
				AddBrandsNode(cdonShoppingMallImport);
				AddVariationTemplatesNode(cdonShoppingMallImport);
				AddProductsNode(cdonShoppingMallImport);

				_xmlDocument.Save(stream);
				stream.Close();

				if (_cdonExportType == CdonExportType.INCR)
				{
					_cdonExportService.SetStatusForProductChanges(null, ProductChangeEventStatus.ActionApplied);
				}

				return cdonExportId;
			}
			catch (Exception exception)
			{
				_cdonExportService.Update(cdonExportId, CdonExportStatus.Failed);
				throw new Exception("Error occurred while creating or saving file", exception);
			}
		}

		// ShoppingMallImport.
		protected virtual XmlNode AddCdonShoppingMallImportNode()
		{
			// cdon_shopping_mall_import
			XmlNode cdonShoppingMallImport = AddNode(_xmlDocument, _xmlDocument, CdonExportXmlNodes.CdonShoppingMallImport);
			AddAttribute(_xmlDocument, cdonShoppingMallImport, CdonExportXmlNodes.Version, "1.0");
			return cdonShoppingMallImport;
		}

		// ImportInfo.
		protected virtual void AddImportInfoNode(XmlNode parentNode, string importId, string importPreviousId)
		{
			// import_info
			XmlNode importInfoNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ImportInfoNode);
			// import_source_id
			AddNode(_xmlDocument, importInfoNode, CdonExportXmlNodes.ImportSourceIdNode, _cdonExportSetting.ApplicationName);
			// import_id
			AddNode(_xmlDocument, importInfoNode, CdonExportXmlNodes.ImportIdNode, importId);
			// import_date
			AddNode(_xmlDocument, importInfoNode, CdonExportXmlNodes.ImportDateNode, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
			// import_type
			AddNode(_xmlDocument, importInfoNode, CdonExportXmlNodes.ImportTypeNode, _cdonExportType.ToString());
			// import_previous_id
			if (!importPreviousId.IsNullOrEmpty())
			{
				AddNode(_xmlDocument, importInfoNode, CdonExportXmlNodes.ImportPreviousIdNode, importPreviousId);
			}
		}

		// Classes.
		protected virtual void AddClassesNode(XmlNode parentNode)
		{
			if (_classes.Count <= 0) return;

			int defaultChannelKey = _classes.Keys.First();
			var categoryTreeItems = _classes[defaultChannelKey].Root.Children;

			AddClasses(parentNode, categoryTreeItems);
		}
		protected virtual void AddClasses(XmlNode parentNode, Collection<ICategoryTreeItem> categoryTreeItems)
		{
			if (categoryTreeItems.Count <= 0) return;

			// classes
			XmlNode classesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ClassesNode);

			foreach (var treeItem in categoryTreeItems)
			{
				if (!_restrictionHelper.PopulateIncorrectCategory(treeItem)) continue;

				var category = treeItem.Category;
				PopulateMainCategories(category);

				// class
				var classNode = AddNode(_xmlDocument, classesNode, CdonExportXmlNodes.ClassNode);
				// class_id
				AddNode(_xmlDocument, classNode, CdonExportXmlNodes.ClassIdNode, category.Id);
				// class_name
				AddNode(_xmlDocument, classNode, CdonExportXmlNodes.ClassNameNode, category.Title);
				// class_name_translations
				var classNameTranslationsNode = AddNode(_xmlDocument, classNode, CdonExportXmlNodes.ClassNameTranslationsNode);

				foreach (var channelId in _classes.Keys)
				{
					ICategoryTree categoryTree = FindCategoryTree(_classes, channelId);
					if (categoryTree == null) continue;

					ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(category.Id);
					if (categoryTreeItem == null) continue;

					// class_name_translation 
					var classNameTranslationNode = AddNode(_xmlDocument, classNameTranslationsNode, CdonExportXmlNodes.ClassNameTranslationNode);
					// language_iso_code
					AddNode(_xmlDocument, classNameTranslationNode, CdonExportXmlNodes.LanguageIsoCodeNode, _cdonExportService.GetCountryIsoCode(channelId));
					// class_name_translation
					AddNode(_xmlDocument, classNameTranslationNode, CdonExportXmlNodes.ClassNameTranslationNode, categoryTreeItem.Category.Title);
				}

				AddClasses(classNode, treeItem.Children);
			}
		}

		// Dimensions.
		protected virtual void AddDimensionsNode(XmlNode parentNode)
		{
			var dimentions = Enum.GetValues(typeof (DimensionType));
			if (dimentions.Length <= 0) return;

			// dimenstions
			XmlNode dimenstionsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.DimenstionsNode);

			foreach (DimensionType type in dimentions)
			{
				// dimension
				var dimensionNode = AddNode(_xmlDocument, dimenstionsNode, CdonExportXmlNodes.DimenstionNode);
				// dimension_id
				AddNode(_xmlDocument, dimensionNode, CdonExportXmlNodes.DimenstionIdNode, (int)type);
				// dimenstion_name
				AddNode(_xmlDocument, dimensionNode, CdonExportXmlNodes.DimenstionNameNode, type.ToString());
				// Units relates to dimension
				AddUnitsNodes(dimensionNode, type);
			}
		}
		protected virtual void AddUnitsNodes(XmlNode parentNode, DimensionType dimensionType)
		{
			switch (dimensionType)
			{
				case DimensionType.ShoeSizes:
					AddShoeSizeUnits(parentNode);
					break;
				//case DimensionType.Length:
				//    AddLenghtUnits(parentNode);
				//    break;
				//case DimensionType.Time:
				//    AddTimeUnits(parentNode);
				//    break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		private void AddShoeSizeUnits(XmlNode parentNode)
		{
			var dimentionUnits = Enum.GetValues(typeof(UnitTypeShoes));
			if (dimentionUnits.Length <= 0) return;

			// dimension_units
			XmlNode unitsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.DimensionUnits);

			foreach (UnitTypeShoes unitType in dimentionUnits)
			{
				// unit
				var unitNode = AddNode(_xmlDocument, unitsNode, CdonExportXmlNodes.UnitNode);
				// unit_id
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitIdNode, (int)unitType);
				// unit_name
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitNameNode, unitType.ToString());
				// unit_abbreviation
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitAbbreviationNode, GetEnumDescription(unitType));
				// unit_factor
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitFactorNode, "1");
			}
		}
		private void AddLenghtUnits(XmlNode parentNode)
		{
			// dimension_units
			XmlNode unitsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.DimensionUnits);

			foreach (UnitTypeLenght type in Enum.GetValues(typeof(UnitTypeLenght)))
			{
				// unit
				var unitNode = AddNode(_xmlDocument, unitsNode, CdonExportXmlNodes.UnitNode);
				// unit_id
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitIdNode, (int)type);
				// unit_name
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitNameNode, type.ToString());
				// unit_abbreviation
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitAbbreviationNode, GetEnumDescription(type));
				// unit_factor
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitFactorNode, "1"); // lagra förhållanden i DBn
			}
		}
		private void AddTimeUnits(XmlNode parentNode)
		{
			// dimension_units
			XmlNode unitsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.DimensionUnits);

			foreach (UnitTypeTime type in Enum.GetValues(typeof(UnitTypeTime)))
			{
				// unit
				var unitNode = AddNode(_xmlDocument, unitsNode, CdonExportXmlNodes.UnitNode);
				// unit_id
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitIdNode, (int)type);
				// unit_name
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitNameNode, type.ToString());
				// unit_abbreviation
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitAbbreviationNode, GetEnumDescription(type));
				// unit_factor
				AddNode(_xmlDocument, unitNode, CdonExportXmlNodes.UnitFactorNode, "1"); // lagra förhållanden i DBn
			}
		}

		// Brands.
		protected virtual void AddBrandsNode(XmlNode parentNode)
		{
			if (_brands.Count <= 0) return;

			// brands
			XmlNode brandsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.BrandsNode);

			foreach (var item in _brands)
			{
				int defaultChannelKey = item.Value.Keys.First();
				var defaultBand = item.Value[defaultChannelKey];

				// brand
				var brandNode = AddNode(_xmlDocument, brandsNode, CdonExportXmlNodes.BrandNode);
				// brand_id
				AddNode(_xmlDocument, brandNode, CdonExportXmlNodes.BrandIdNode, defaultBand.Id);
				// brand_name
				AddNode(_xmlDocument, brandNode, CdonExportXmlNodes.BrandNameNode, defaultBand.Title);
				// brand_description
				if (!defaultBand.Description.IsNullOrEmpty())
				{
					AddNode(_xmlDocument, brandNode, CdonExportXmlNodes.BrandDescriptionNode, defaultBand.Description);
				}
				// brand_url
				if (!defaultBand.ExternalUrl.IsNullOrEmpty())
				{
					AddNode(_xmlDocument, brandNode, CdonExportXmlNodes.BrandUrlNode, defaultBand.ExternalUrl);
				}
				// brand_image_url
				if (defaultBand.Image != null)
				{
					AddNode(_xmlDocument, brandNode, CdonExportXmlNodes.BrandImageUrlNode, GetImageUrl(defaultBand.Image, defaultChannelKey));
				}
				// brand_translations
				var brandTranslationsNode = AddNode(_xmlDocument, brandNode, CdonExportXmlNodes.BrandTranslationsNode);

				foreach (var channelId in item.Value.Keys)
				{
					IBrand brand = item.Value[channelId];
					if (brand == null) continue;

					// brand_translation
					var brandTranslationNode = AddNode(_xmlDocument, brandTranslationsNode, CdonExportXmlNodes.BrandTranslationNode);
					// language_iso_code
					AddNode(_xmlDocument, brandTranslationNode, CdonExportXmlNodes.LanguageIsoCodeNode, _cdonExportService.GetCountryIsoCode(channelId));
					// brand_name_translation
					AddNode(_xmlDocument, brandTranslationNode, CdonExportXmlNodes.BrandNameTranslationNode, brand.Title);
					// brand_description_translation
					if (!brand.Description.IsNullOrEmpty())
					{
						AddNode(_xmlDocument, brandTranslationNode, CdonExportXmlNodes.BrandDescriptionTransalationNode, brand.Description);
					}
					// brand_url_translation
					if (!brand.ExternalUrl.IsNullOrEmpty())
					{
						AddNode(_xmlDocument, brandTranslationNode, CdonExportXmlNodes.BrandUrlTransalationNode, brand.ExternalUrl);
					}
					// brand_image_url_translation
					if (brand.Image != null)
					{
						AddNode(_xmlDocument, brandTranslationNode, CdonExportXmlNodes.BrandImageUrlTranslationNode, GetImageUrl(brand.Image, channelId));
					}
				}
			}
		}

		// VariationTemplates.
		protected virtual void AddVariationTemplatesNode(XmlNode parentNode)
		{
			var variationTemplates = Enum.GetValues(typeof(TemplateType));
			if (variationTemplates.Length <= 0) return;

			// variation_templates
			XmlNode variationTemplatesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.VariationTemplatesNode);

			foreach (TemplateType type in variationTemplates)
			{
				// template
				var template = AddNode(_xmlDocument, variationTemplatesNode, CdonExportXmlNodes.VariationTemplateNode);
				// template id
				AddAttribute(_xmlDocument, template, CdonExportXmlNodes.IdNode, (int) type);
				// components
				AddTemplateComponentsNode(template, type);
			}
		}
		protected virtual void AddTemplateComponentsNode(XmlNode parentNode, TemplateType type)
		{
			switch (type)
			{
				case TemplateType.SizeOrColor:
					AddTemplateComponentNode(parentNode, Attribute.SIZE_EU);
					AddTemplateComponentNode(parentNode, Attribute.COLOR);
					break;
				case TemplateType.Color:
					AddTemplateComponentNode(parentNode, Attribute.COLOR);
					break;
				case TemplateType.Size:
					AddTemplateComponentNode(parentNode, Attribute.SIZE_EU);
					break;
				//case TemplateType.SizeAndColor: // Not used by heppo
				//    AddTemplateComponentNode(parentNode, Attribute.SIZE_EU, Attribute.COLOR);
				//    break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		protected virtual void AddTemplateComponentNode(XmlNode parentNode, Attribute attributeType)
		{
			// component (template)
			var component = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.VariationTemplateComponentNode);
			// attribute_id (template)
			AddNode(_xmlDocument, component, CdonExportXmlNodes.AttributeIdNode, (int)attributeType);
		}
		protected virtual void AddTemplateComponentNode(XmlNode parentNode, Attribute attributeType1, Attribute attributeType2)
		{
			// component (template)
			var component = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.VariationTemplateComponentNode);
			// attribute_id (template)
			AddNode(_xmlDocument, component, CdonExportXmlNodes.AttributeIdNode, (int)attributeType1);
			// secundary attribute_id (template)
			AddNode(_xmlDocument, component, CdonExportXmlNodes.AttributeIdNode, (int)attributeType2);
		}

		// Attributes.
		protected virtual void AddAttributesNode(XmlNode parentNode)
		{
			// attributes
			XmlNode attributesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.AttributesNode);

			// product_short_description
			var attributeShortDescription = AddAttribute(attributesNode, Attribute.SHORT_DESCRIPTION, AttributeType.ENTERED_VALUE, AttributeDataType.String, null, AttributeIsVariantType.FALSE);
			AddAttributeTranslations(attributeShortDescription, GetAttributeTranslations(Attribute.SHORT_DESCRIPTION));
			// product_size_eu_title
			var attributeSize = AddAttribute(attributesNode, Attribute.SIZE_EU, AttributeType.ENTERED_VALUE, AttributeDataType.String, (int)UnitTypeShoes.EU, AttributeIsVariantType.TRUE);
			AddAttributeTranslations(attributeSize, GetAttributeTranslations(Attribute.SIZE_EU));
			// product_color_value
			var attributeColor = AddAttribute(attributesNode, Attribute.COLOR, AttributeType.ENTERED_VALUE, AttributeDataType.String, null, AttributeIsVariantType.TRUE);
			AddAttributeTranslations(attributeColor, GetAttributeTranslations(Attribute.COLOR));
			// product_icon && product_age_from_month && product_age_to_month
			if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
			{
				var attributeIcon = AddAttribute(attributesNode, Attribute.ICON, AttributeType.SELECT_LIST, AttributeDataType.String, null, AttributeIsVariantType.FALSE);
				AddAttributeTranslations(attributeIcon, GetAttributeTranslations(Attribute.ICON));
				AddAttributeSelectListValues(attributeIcon, _icons.ToDictionary(i => i.Value.CdonIconId, i => i.Value.Title), null);

				var attributeFromMonth = AddAttribute(attributesNode, Attribute.AGE_FROM_MONTH, AttributeType.ENTERED_VALUE, AttributeDataType.String, null, AttributeIsVariantType.FALSE);
				AddAttributeTranslations(attributeFromMonth, GetAttributeTranslations(Attribute.AGE_FROM_MONTH));

				var attributeToMonth = AddAttribute(attributesNode, Attribute.AGE_TO_MONTH, AttributeType.ENTERED_VALUE, AttributeDataType.String, null, AttributeIsVariantType.FALSE);
				AddAttributeTranslations(attributeToMonth, GetAttributeTranslations(Attribute.AGE_TO_MONTH));
			}

			// tags
			foreach (var tagGroup in _tagGroups)
			{
				int defaultChannelKey = tagGroup.Value.Keys.First();
				var defaultTagGroup = tagGroup.Value[defaultChannelKey];

				if (SpecificTagGroupIds.Contains(defaultTagGroup.Id.ToString(CultureInfo.InvariantCulture)) && _cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
				{
					var attributeTema = AddAttribute(attributesNode, string.Format(_tagAttributeId, defaultTagGroup.Id), defaultTagGroup.Title, AttributeType.ENTERED_VALUE, AttributeDataType.String, null, AttributeIsVariantType.FALSE);
					AddAttributeTranslations(attributeTema, tagGroup.Value.ToDictionary(tg => _cdonExportService.GetCountryIsoCode(tg.Key), tg => tg.Value.Title));
					continue;
				}

				var allTagsByTagGroup = _cdonExportService.GetAllTagsByTagGroup(tagGroup.Value);
				if (allTagsByTagGroup[defaultChannelKey].Values.Count <= 0) continue;
				
				// attribute_tag_group								 Id korkar med de hårdkodade
				var attributeTagGroup = AddAttribute(attributesNode, string.Format(_tagAttributeId, defaultTagGroup.Id), defaultTagGroup.Title, AttributeType.SELECT_LIST, AttributeDataType.String, null, AttributeIsVariantType.FALSE);
				// attribute_tag_group_translations
				AddAttributeTranslations(attributeTagGroup, tagGroup.Value.ToDictionary(tg => _cdonExportService.GetCountryIsoCode(tg.Key), tg => tg.Value.Title));
				// attribute_tag_group_select_list_values
				var defaultSelectListValues = allTagsByTagGroup[defaultChannelKey];
				if (defaultSelectListValues.Count <= 0) continue;
				AddAttributeSelectListValues(attributeTagGroup, defaultSelectListValues.ToDictionary(t => t.Value.CdonTagId, t => t.Value.Value), allTagsByTagGroup.ToDictionary(t => t.Key, t => t.Value.ToDictionary(v => v.Value.CdonTagId, v => v.Value.Value)));
			}
		}

		protected virtual XmlNode AddAttribute(XmlNode parentNode, Attribute attribute, AttributeType attributeType, AttributeDataType dataType, int? unitId, AttributeIsVariantType isVariant)
		{
			return AddAttribute(parentNode, ((int)attribute).ToString(CultureInfo.InvariantCulture), attribute.ToString(), attributeType, dataType, unitId, isVariant);
		}
		protected virtual XmlNode AddAttribute(XmlNode parentNode, string id, string attributeName, AttributeType attributeType, AttributeDataType dataType, int? unitId, AttributeIsVariantType isVariant)
		{
			// attribute
			var attributeNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.AttributeNode);
			// attribute_id
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeIdNode, id);
			// attribute_type
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeTypeNode, attributeType.ToString());
			// attribute_name
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeNameNode, attributeName);
			// attribute_data_type
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeDataTypeNode, GetEnumDescription(dataType));
			// unit_id
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.UnitIdNode, unitId != null ? unitId.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
			// class_relations
			AddAttributeClassRelations(attributeNode);
			// attribute_is_variant
			if (isVariant == AttributeIsVariantType.TRUE)
			{
				AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeIsVariantNode, isVariant.ToString());
			}

			return attributeNode;
		}
		protected virtual void AddAttributeClassRelations(XmlNode parentNode)
		{
			// class_relations
			XmlNode classRelations = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductClassRelationsNode);

			foreach (int classId in _mainCategories)
			{
				// class_relation
				var classRelation = AddNode(_xmlDocument, classRelations, CdonExportXmlNodes.ProductClassRelationNode);
				// class_id
				AddNode(_xmlDocument, classRelation, CdonExportXmlNodes.ProductClassIdNode, classId);
			}
		}
		protected virtual void AddAttributeTranslations(XmlNode parentNode, Dictionary<string, string> channelValues)
		{
			// attribute_name_translations
			var attributeNameTranslationsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.AttributeNameTranslationsNode);

			foreach (var isoCode in channelValues.Keys)
			{
				string value = channelValues[isoCode];
				if (string.IsNullOrEmpty(value)) continue;

				// attribute_name_translation
				var attributeNameTranslationNode = AddNode(_xmlDocument, attributeNameTranslationsNode, CdonExportXmlNodes.AttributeNameTranslationNode);
				// language_iso_code
				AddNode(_xmlDocument, attributeNameTranslationNode, CdonExportXmlNodes.LanguageIsoCodeNode, isoCode);
				// attribute_name_translation
				AddNode(_xmlDocument, attributeNameTranslationNode, CdonExportXmlNodes.AttributeNameTranslationNode, value);
			}
		}
		protected virtual void AddAttributeSelectListValues(XmlNode parentNode, Dictionary<int, string> values, Dictionary<int, Dictionary<int, string>> translations)
		{
			// attribute_select_list_values
			var attributeSelectListValuesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.AttributeSelectListValuesNode);

			foreach (var selectListValue in values)
			{
				// attribute_select_list_value
				var attributeSelectListValueNode = AddNode(_xmlDocument, attributeSelectListValuesNode, CdonExportXmlNodes.AttributeSelectListValueNode);
				// attribute_select_list_value_id
				AddNode(_xmlDocument, attributeSelectListValueNode, CdonExportXmlNodes.AttributeSelectListValueIdNode, selectListValue.Key);
				// attribute_select_list_value_value
				AddNode(_xmlDocument, attributeSelectListValueNode, CdonExportXmlNodes.AttributeSelectListValueValueNode, selectListValue.Value);

				if (translations == null) continue;

				// attribute_select_list_value_translations
				var attributeSelectListValueTranslationsNode = AddNode(_xmlDocument, attributeSelectListValueNode, CdonExportXmlNodes.AttributeSelectListValueTranslationsNode);

				foreach (var channelId in translations.Keys)
				{
					var selectListValues = translations[channelId];
					var value = selectListValues[selectListValue.Key];
					if (string.IsNullOrEmpty(value)) continue;

					// attribute_select_list_value_translation
					var attributeSelectListValueTranslationNode = AddNode(_xmlDocument, attributeSelectListValueTranslationsNode, CdonExportXmlNodes.AttributeSelectListValueTranslationNode);
					// language_iso_code
					AddNode(_xmlDocument, attributeSelectListValueTranslationNode, CdonExportXmlNodes.LanguageIsoCodeNode, _cdonExportService.GetCountryIsoCode(channelId));
					// attribute_select_list_value_translation_value
					AddNode(_xmlDocument, attributeSelectListValueTranslationNode, CdonExportXmlNodes.AttributeSelectListValueTranslationValueNode, value);
				}
			}
		}

		// Products.
		protected virtual void AddProductsNode(XmlNode parentNode)
		{
			if (_products.Count <= 0) return;

			// products
			XmlNode productsNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductsNode);

			foreach (var item in _products)
			{
				if (!item.Available) continue;

				// product
				AddProductNode(productsNode, item);
			}
		}
		protected virtual void AddProductNode(XmlNode parentNode, ICdonProductInfo productInfo)
		{
			var defaultProductInfo = productInfo.DefaultProductInfo.Product;

			// except MISSING category
			if (_restrictionHelper.IsCorrectCategory(defaultProductInfo.CategoryId)) return;

			bool hasSizes = productInfo.ProductSizesCollection.Count > 0;
			bool hasVariants = productInfo.ProductVariantsCollection != null && productInfo.ProductVariantsCollection.Count > 0;

			if (hasVariants && !HasCorrectVariants(productInfo)) return;

			// product
			var productNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductNode);
			// product_id
			string productId = GetCdonProductId(defaultProductInfo, hasVariants, hasSizes);
			AddNode(_xmlDocument, productNode, CdonExportXmlNodes.ProductIdNode, productId);
			// product_quantity
			var quantity = GetProductQuantity(productInfo, hasVariants);
			AddNode(_xmlDocument, productNode, CdonExportXmlNodes.ProductQuantityNode, quantity);
			// product_status
			// product_expose_status
			CdonProductStatus status;
			CdonProductExposeStatus exposeStatus;
			GetProductStatuses(productInfo, hasVariants, defaultProductInfo, quantity, out status, out exposeStatus);
			AddNode(_xmlDocument, productNode, CdonExportXmlNodes.ProductStatusNode, status.ToString());
			AddNode(_xmlDocument, productNode, CdonExportXmlNodes.ProductExposeStatusNode, exposeStatus.ToString());
			// brand_id
			if (defaultProductInfo.BrandId.HasValue)
			{
				AddNode(_xmlDocument, productNode, CdonExportXmlNodes.BrandIdNode, defaultProductInfo.BrandId.Value);
			}
			// class_relations
			var classRelations = AddNode(_xmlDocument, productNode, CdonExportXmlNodes.ProductClassRelationsNode);
			// class_relation
			var classRelation = AddNode(_xmlDocument, classRelations, CdonExportXmlNodes.ProductClassRelationNode);
			// class_id
			AddNode(_xmlDocument, classRelation, CdonExportXmlNodes.ProductClassIdNode, defaultProductInfo.CategoryId);
			// product_countries
			AddProductCountries(productNode, productInfo, hasVariants);
			// product_attributes
			AddProductAttributes(productNode, productInfo, hasVariants);

			if (hasVariants || hasSizes)
			{
				// variation_attributes
				AddProductVariationAttributes(productNode, productInfo);

				// product_variations
				AddProductVariations(productNode, productInfo);
			}
		}

		// Product countries.
		protected virtual void AddProductCountries(XmlNode parentNode, ICdonProductInfo productInfo, bool hasVariants)
		{
			// product_countries
			XmlNode productCountriesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductCountriesNode);

			foreach (var channelProductInfo in productInfo.ChannelProductInfoCollection)
			{
				var channel = channelProductInfo.Channel;
				var product = channelProductInfo.Product;

				// product_country
				XmlNode productCountryNode = AddNode(_xmlDocument, productCountriesNode, CdonExportXmlNodes.ProductCountryNode);
				// country
				AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.CountryNode, channel.Country.Iso);
				// product_country_sellable_status
				var sellableStatus = GetProductCountrySellableStatus(product, channel.Id);
				if (hasVariants && productInfo.ProductVariantsCollection != null && productInfo.ProductVariantsCollection.Count > 0)
				{
					foreach (var variantInfo in productInfo.ProductVariantsCollection.Values)
					{
						var variant = variantInfo.ChannelProductInfoCollection.FirstOrDefault(pi => pi.Channel.Id == channel.Id);
						if (variant == null) continue;

						var status = GetProductCountrySellableStatus(variant.Product, channel.Id);
						if (status != CdonProductCountrySellableStatus.SELLABLE.ToString()) continue;

						sellableStatus = status;
						break;
					}
				}
				AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.ProductCountrySellableStatusNode, sellableStatus);
				// product_country_back_in_stock
				if (product.ExpectedBackInStock.HasValue)
				{
					AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.ProductCountryBackInStockNode, product.ExpectedBackInStock.Value.Date.ToString("yyyy-MM-dd"));
				}
				// product_country_currency
				AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.ProductCountryCurrencyNode, channel.Currency.Iso);

				decimal currentPrice = product.IsPriceAffectedByCampaign ? product.CampaignInfo.Price.IncludingVat : product.Price.PriceIncludingVat;
				// product_country_current_price
				var curPrice = currentPrice.ToString("0.00", CultureInfo.InvariantCulture);
				AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.ProductCountryCurrentPriceNode, curPrice);
				// product_country_ordinary_price
				var ordinaryPrice = product.Price.PriceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);
				AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.ProductCountryOrdinaryPriceNode, ordinaryPrice);
				// product_country_vat
				AddNode(_xmlDocument, productCountryNode, CdonExportXmlNodes.ProductCountryVatNode, (product.Price.VatPercentage / 100).ToString(CultureInfo.InvariantCulture));
			}
		}

		// Product attributes.
		protected virtual void AddProductAttributes(XmlNode parentNode, ICdonProductInfo productInfo, bool hasVariants)
		{
			var defaultProductInfo = productInfo.DefaultProductInfo.Product;
			var classId = defaultProductInfo.CategoryId.ToString(CultureInfo.InvariantCulture);

			// product_attributes
			XmlNode productAttributesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductAttributesNode);

			// product_attribute - // TITLE
			var title = GetProductTitle(defaultProductInfo, productInfo.DefaultProductInfo.Channel.Id);
			var titleTranslations = GetProductTitleTranslations(productInfo);
			AddProductDefaultAttributes(productAttributesNode, DefaultAttribute.CDON_PRODUCT_TITLE, title, titleTranslations, classId);
			// SHORT_DESCRIPTION
			var shortDescriptionTranslations = productInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.ShortDescription);
			AddProductDefaultAttributes(productAttributesNode, Attribute.SHORT_DESCRIPTION, defaultProductInfo.ShortDescription, shortDescriptionTranslations, classId);
			// product_attribute - // DESCRIPTION
			var description = defaultProductInfo.Description;
			if (description.IsNullOrEmpty())
			{
				description = GetProductDescription(defaultProductInfo, productInfo.DefaultProductInfo.Channel);
			}
			var descriptionTranslations = productInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.Description);
			descriptionTranslations = GetMissingDescriptionTranslations(productInfo, descriptionTranslations);
			AddProductDefaultAttributes(productAttributesNode, DefaultAttribute.CDON_PRODUCT_DESCRIPTION, description, descriptionTranslations, classId);

			if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
			{
				_productIconsUsed.Clear();
				// product_attribute - // ICON
				foreach (var icon in productInfo.ProductIconsCollection)
				{
					if (icon.Title.IsNullOrEmpty()) continue;
					if (!IsIconCommon(productInfo, icon.Id)) continue;

					if (!_productIconsUsed.ContainsKey(icon.Id))
					{
						_productIconsUsed.Add(icon.Id, icon.CdonIconId);
					}
				}

				if (_productIconsUsed.Count > 0)
				{
					AddProductDefaultSelectedAttributes(productAttributesNode, CdonExportXmlNodes.ProductAttributeNode, (int)Attribute.ICON, _productIconsUsed.Values.ToList(), classId);
				}

				// product_attribute - // AgeFromMonth
				_ageFromMonthUsed = IsAgeFromMonthCommon(productInfo);
				if (_ageFromMonthUsed)
				{
					AddProductDefaultAttributes(productAttributesNode, Attribute.AGE_FROM_MONTH, defaultProductInfo.AgeFromMonth, null, classId);
				}

				// product_attribute - // AgeToMonth
				_ageToMonthUsed = IsAgeToMonthCommon(productInfo);
				if (_ageToMonthUsed)
				{
					AddProductDefaultAttributes(productAttributesNode, Attribute.AGE_TO_MONTH, defaultProductInfo.AgeToMonth, null, classId);
				}
			}

			_productTagsUsed.Clear();
			// tag attributes.
			foreach (var tagGroup in _tagGroups)
			{
				var productTagGroup = defaultProductInfo.TagGroups.FirstOrDefault(tg => tg.Id == tagGroup.Key);
				if (productTagGroup == null || (productTagGroup.CommonName == _cdonExportSetting.ColorTagGroupCommonName && hasVariants)) continue;

				if (SpecificTagGroupIds.Contains(productTagGroup.Id.ToString(CultureInfo.InvariantCulture)) && _cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
				{
					_isTemaTagExported = false;

					var exportedTags = new Collection<ITag>();
					foreach (var tag in productTagGroup.Tags)
					{
						if (!IsTagCommon(productInfo, tag)) continue;
						exportedTags.Add(tag);
					}

					if (exportedTags.Count > 0 && exportedTags.Count == productTagGroup.Tags.Count)
					{
						string tema = GetTagTemaValue(productInfo.DefaultProductInfo, productTagGroup);
						var translations = GetTagTemaValueTranslations(productInfo, productTagGroup);
						AddAttribute(productAttributesNode, CdonExportXmlNodes.ProductAttributeNode, string.Format(_tagAttributeId, productTagGroup.Id), tema, translations, classId, false);
						_isTemaTagExported = true;
					}

					continue;
				}

				AddProductDefaultSelectedTagAttributes(productAttributesNode, CdonExportXmlNodes.ProductAttributeNode, tagGroup.Key, productTagGroup.Tags, classId, false, productInfo);
			}

			if (!hasVariants)
			{
				var defaultImage = defaultProductInfo.Image;

				// product_attribute - // FRONT_IMAGE
				if (defaultImage != null)
				{
					var imageUrl = GetImageUrl(defaultImage, productInfo.DefaultProductInfo.Channel.Id);
					var imageUrlTranslations = productInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.Image);
					AddProductDefaultAttributes(productAttributesNode, DefaultAttribute.CDON_PRODUCT_FRONT_IMAGE, imageUrl, imageUrlTranslations, classId);
				}

				// product_attribute - // EXTRA_IMAGE DON'T REMOVE
				foreach (var productImageGroup in productInfo.ProductImagesCollection)
				{
					foreach (var productImage in productImageGroup.ProductImages)
					{
						var image = productImage.Image;
						if (image == null) continue;

						if (defaultImage != null
							&& defaultImage.Id == image.Id
							&& defaultImage.MediaFolderId == image.MediaFolderId
							&& defaultImage.MediaFormatId == image.MediaFormatId) continue;

						var productImages = new Dictionary<int, object>();
						foreach (var cdonChannelProductInfo in productInfo.ChannelProductInfoCollection)
						{
							if (!productImages.ContainsKey(cdonChannelProductInfo.Channel.Id))
							{
								productImages[cdonChannelProductInfo.Channel.Id] = image;
							}
						}

						var imageUrl = GetImageUrl(image, productInfo.DefaultProductInfo.Channel.Id);
						AddProductDefaultAttributes(productAttributesNode, DefaultAttribute.CDON_PRODUCT_EXTRA_IMAGE, imageUrl, productImages, classId);
					}
				}
			}
		}
		protected virtual void AddProductDefaultAttributes(XmlNode parentNode, CdonProductDefaultAttribute attribute, string attributeValue, Dictionary<int, object> attributeValueTranslations, string classId)
		{
			AddAttribute(parentNode, CdonExportXmlNodes.ProductAttributeNode, attribute.ToString(), attributeValue, attributeValueTranslations, classId, false);
		}
		protected virtual void AddProductDefaultAttributes(XmlNode parentNode, Attribute attribute, object attributeValue, Dictionary<int, object> attributeValueTranslations, string classId)
		{
			if (attributeValue == null) return;
			AddAttribute(parentNode, CdonExportXmlNodes.ProductAttributeNode, ((int)attribute).ToString(CultureInfo.InvariantCulture), attributeValue.ToString(), attributeValueTranslations, classId, false);
		}
		protected virtual void AddProductDefaultSelectedTagAttributes(XmlNode parentNode, string attributeNodeName, int attributeId, Collection<ITag> tags, string classId, bool isVariantAttribute, ICdonProductInfo productInfo)
		{
			var exportedTags = new Collection<ITag>();
			if (isVariantAttribute)
			{
				foreach (var tag in tags)
				{
					if (_productTagsUsed.ContainsKey(attributeId) && _productTagsUsed[attributeId].ContainsKey(tag.Id)) continue;

					exportedTags.Add(tag);
				}
			}
			else
			{
				foreach (var tag in tags)
				{
					if (!IsTagCommon(productInfo, tag)) continue;

					exportedTags.Add(tag);
				}
			}

			if (exportedTags.Count <= 0) return;

			// attribute
			XmlNode attributeNode = AddNode(_xmlDocument, parentNode, attributeNodeName);

			var tagsUsed = new Dictionary<int, int>();
			if (!isVariantAttribute)
			{
				if (!_productTagsUsed.ContainsKey(attributeId))
				{
					_productTagsUsed[attributeId] = new Dictionary<int, int>();
				}
				tagsUsed = _productTagsUsed[attributeId];
			}

			// attribute_id
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeIdNode, string.Format(_tagAttributeId, attributeId));
			// attribute_select_list_value_ids
			XmlNode productAttributeSelectListValueIdsNode = AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.ProductAttributeSelectListValueIdsNode);

			foreach (var tag in exportedTags)
			{
				// attribute_select_list_value_id
				AddNode(_xmlDocument, productAttributeSelectListValueIdsNode, CdonExportXmlNodes.ProductAttributeSelectListValueIdNode, string.Format(_cdonExportService.TagAttributeValuePrefix, tag.Id));

				if (!isVariantAttribute && !tagsUsed.ContainsKey(tag.Id))
				{
					tagsUsed[tag.Id] = tag.Id;
				}
			}

			// class_id
			if (string.IsNullOrEmpty(classId)) return;
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.ClassIdNode, classId);
		}
		protected virtual void AddProductDefaultSelectedAttributes(XmlNode parentNode, string attributeNodeName, int attributeId, List<int> values, string classId)
		{
			// attribute
			XmlNode attributeNode = AddNode(_xmlDocument, parentNode, attributeNodeName);

			// attribute_id
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeIdNode, attributeId);
			// attribute_select_list_value_ids
			XmlNode productAttributeSelectListValueIdsNode = AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.ProductAttributeSelectListValueIdsNode);
			// attribute_select_list_value_id
			foreach (var value in values)
			{
				AddNode(_xmlDocument, productAttributeSelectListValueIdsNode, CdonExportXmlNodes.ProductAttributeSelectListValueIdNode, value);
			}
			// class_id
			if (string.IsNullOrEmpty(classId)) return;
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.ClassIdNode, classId);
		}

		// Product variation attributes.
		protected virtual void AddProductVariationAttributes(XmlNode parentNode, ICdonProductInfo productInfo)
		{
			// variation_attributes
			XmlNode productVariationAttributesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductVariationAttributesNode);

			#region variation attribute - SIZE

			var sizes = GetAllSizes(productInfo);
			foreach (var productSize in sizes)
			{
				// variation_attribute
				var productVariationAttributeNode = AddNode(_xmlDocument, productVariationAttributesNode, CdonExportXmlNodes.ProductVariationAttributeNode);
				// variation_attribute_id
				AddAttribute(_xmlDocument, productVariationAttributeNode, CdonExportXmlNodes.IdNode, string.Format(_sizeAttributeId, productSize.Key));
				// key_attributes
				AddProductVariationAttributeKeyAttributes(productVariationAttributeNode, Attribute.SIZE_EU, productSize.Value.SizeInfo.EuTitle, null);
			}

			#endregion

			#region variation attribute - COLOR

			if (productInfo.ProductVariantsCollection != null)
			{
				foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
				{
					var variationInfo = variationProductInfo.Value.DefaultProductInfo;

					if (variationInfo.Color == null) continue;

					// variation_attribute
					var productVariationAttributeNode = AddNode(_xmlDocument, productVariationAttributesNode, CdonExportXmlNodes.ProductVariationAttributeNode);
					// variation_attribute_id
					AddAttribute(_xmlDocument, productVariationAttributeNode, CdonExportXmlNodes.IdNode, string.Format(_colorAttributeId, variationInfo.Color.Id));
					// key_attributes
					string color = GetTagColorValue(variationInfo);
					var translations = GetTagColorValueTranslations(variationProductInfo.Value);
					AddProductVariationAttributeKeyAttributes(productVariationAttributeNode, Attribute.COLOR, color, translations);
					// additional_attributes
					AddProductVariationAttributeAdditionalAttributes(productVariationAttributeNode, variationProductInfo.Value, productInfo);
				}
			}

			#endregion
		}
		protected virtual void AddProductVariationAttributeKeyAttributes(XmlNode parentNode, Attribute attribute, string attributeValue, Dictionary<int, object> attributeValueTranslations)
		{
			// key_attributes
			XmlNode keyAttributes = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductVariationKeyAttributesNode);
			// attribute
			XmlNode attributeNode = AddNode(_xmlDocument, keyAttributes, CdonExportXmlNodes.AttributeNode);
			// attribute_id
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeIdNode, (int) attribute);
			// attribute_value
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeValueNode, attributeValue);

			if (attributeValueTranslations == null || attributeValueTranslations.Count <= 0) return;

			// attribute_value_translations
			var attributeValueTranslationsNode = AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeValueTranslationsNode);

			foreach (var valueTranslation in attributeValueTranslations)
			{
				var value = valueTranslation.Value;
				if (value == null || string.IsNullOrEmpty(value.ToString())) continue;

				// attribute_value_translation
				var attributeValueTranslationNode = AddNode(_xmlDocument, attributeValueTranslationsNode, CdonExportXmlNodes.AttributeValueTranslationNode);
				// language_iso_code
				AddNode(_xmlDocument, attributeValueTranslationNode, CdonExportXmlNodes.LanguageIsoCodeNode, _cdonExportService.GetCountryIsoCode(valueTranslation.Key));
				// attribute_value_translation_value
				AddNodeWithoutIllegalCharacters(_xmlDocument, attributeValueTranslationNode, CdonExportXmlNodes.AttributeValueTranslationValueNode, value.ToString());
			}
		}
		protected virtual void AddProductVariationAttributeAdditionalAttributes(XmlNode parentNode, ICdonProductInfo productInfo, ICdonProductInfo originalProductInfo)
		{
			var defaultOriginalProductInfo = originalProductInfo.DefaultProductInfo.Product;
			var defaultProductInfo = productInfo.DefaultProductInfo.Product;

			// additional_attributes
			XmlNode additionalAttributesNode = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductVariationAdditionalAttributesNode);

			// product_attribute - // TITLE
			var originalTitle = GetProductTitle(defaultOriginalProductInfo, originalProductInfo.DefaultProductInfo.Channel.Id);
			var originalTitleTranslations = GetProductTitleTranslations(originalProductInfo);

			var variationTitle = GetProductTitle(defaultProductInfo, productInfo.DefaultProductInfo.Channel.Id);

			var title = originalTitle == variationTitle ? null : variationTitle;
			var titleTranslations = GetProductTitleTranslations(productInfo);

			foreach (var originalTitleTranslation in originalTitleTranslations)
			{
				if (titleTranslations.ContainsKey(originalTitleTranslation.Key)
					&& titleTranslations[originalTitleTranslation.Key] != null
					&& string.Equals(titleTranslations[originalTitleTranslation.Key], originalTitleTranslation.Value))
				{
					titleTranslations[originalTitleTranslation.Key] = null;
				}
			}
			AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, DefaultAttribute.CDON_PRODUCT_TITLE, title, titleTranslations);

			// SHORT_DESCRIPTION
			var originalShortDescription = defaultOriginalProductInfo.ShortDescription;
			var originalShortDescriptionTranslations = originalProductInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.ShortDescription);

			var shortDescription = originalShortDescription == defaultProductInfo.ShortDescription ? null : defaultProductInfo.ShortDescription;
			var shortDescriptionTranslations = productInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.ShortDescription);

			foreach (var originalShortDescriptionTranslation in originalShortDescriptionTranslations)
			{
				if (shortDescriptionTranslations.ContainsKey(originalShortDescriptionTranslation.Key)
					&& shortDescriptionTranslations[originalShortDescriptionTranslation.Key] != null
					&& string.Equals(shortDescriptionTranslations[originalShortDescriptionTranslation.Key], originalShortDescriptionTranslation.Value))
				{
					shortDescriptionTranslations[originalShortDescriptionTranslation.Key] = null;
				}
			}
			AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, Attribute.SHORT_DESCRIPTION, shortDescription, shortDescriptionTranslations);

			// product_attribute - // DESCRIPTION
			var originalDescription = defaultOriginalProductInfo.Description;
			if (string.IsNullOrEmpty(originalDescription))
			{
				originalDescription = GetProductDescription(defaultOriginalProductInfo, originalProductInfo.DefaultProductInfo.Channel);
			}
			var originalDescriptionTranslations = originalProductInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.Description);
			originalDescriptionTranslations = GetMissingDescriptionTranslations(originalProductInfo, originalDescriptionTranslations);

			var description = defaultProductInfo.Description;
			if (string.IsNullOrEmpty(description))
			{
				description = GetProductDescription(defaultProductInfo, productInfo.DefaultProductInfo.Channel);
			}
			var descriptionTranslations = productInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.Description);
			descriptionTranslations = GetMissingDescriptionTranslations(productInfo, descriptionTranslations);

			description = originalDescription == description ? null : description;
			foreach (var originalDescriptionTranslation in originalDescriptionTranslations)
			{
				if (descriptionTranslations.ContainsKey(originalDescriptionTranslation.Key)
					&& descriptionTranslations[originalDescriptionTranslation.Key] != null
					&& string.Equals(descriptionTranslations[originalDescriptionTranslation.Key], originalDescriptionTranslation.Value))
				{
					descriptionTranslations[originalDescriptionTranslation.Key] = null;
				}
			}
			AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, DefaultAttribute.CDON_PRODUCT_DESCRIPTION, description, descriptionTranslations);

			// product_attribute - // FRONT_IMAGE
			var defaultImage = defaultProductInfo.Image;
			if (defaultImage != null)
			{
				var imageUrl = GetImageUrl(defaultImage, productInfo.DefaultProductInfo.Channel.Id);
				var imageUrlTranslations = productInfo.ChannelProductInfoCollection.ToDictionary(pi => pi.Channel.Id, pi => (object)pi.Product.Image);
				AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, DefaultAttribute.CDON_PRODUCT_FRONT_IMAGE, imageUrl, imageUrlTranslations);
			}
			// product_attribute - // EXTRA_IMAGE DON'T REMOVE
			foreach (var productImageGroup in productInfo.ProductImagesCollection)
			{
				foreach (var productImage in productImageGroup.ProductImages)
				{
					var image = productImage.Image;
					if (image == null) continue;

					if (defaultImage != null
						&& defaultImage.Id == image.Id
						&& defaultImage.MediaFolderId == image.MediaFolderId
						&& defaultImage.MediaFormatId == image.MediaFormatId) continue;

					var productImages = new Dictionary<int, object>();
					foreach (var cdonChannelProductInfo in productInfo.ChannelProductInfoCollection)
					{
						if (!productImages.ContainsKey(cdonChannelProductInfo.Channel.Id))
						{
							productImages[cdonChannelProductInfo.Channel.Id] = image;
						}
					}

					var imageUrl = GetImageUrl(image, productInfo.DefaultProductInfo.Channel.Id);
					AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, DefaultAttribute.CDON_PRODUCT_EXTRA_IMAGE, imageUrl, productImages);
				}
			}
			if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
			{
				// product_attribute - // ICON
				var icons = new Dictionary<int, int>();
				foreach (var icon in productInfo.ProductIconsCollection)
				{
					if (_productIconsUsed.ContainsKey(icon.Id)) continue;
					if (icon.Title.IsNullOrEmpty()) continue;

					if (!icons.ContainsKey(icon.Id))
					{
						icons.Add(icon.Id, icon.CdonIconId);
					}
				}

				if (icons.Count > 0)
				{
					AddProductDefaultSelectedAttributes(additionalAttributesNode, CdonExportXmlNodes.AttributeNode, (int)Attribute.ICON, icons.Values.ToList(), null);
				}

				// product_attribute - // AgeFromMonth
				if (!_ageFromMonthUsed)
				{
					AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, Attribute.AGE_FROM_MONTH, defaultProductInfo.AgeFromMonth.ToString(CultureInfo.InvariantCulture), null);
				}

				// product_attribute - // AgeToMonth
				if (!_ageToMonthUsed)
				{
					AddProductVariationAttributeAdditionalAttribute(additionalAttributesNode, Attribute.AGE_TO_MONTH, defaultProductInfo.AgeToMonth.ToString(CultureInfo.InvariantCulture), null);
				}
			}
			// tag attributes.
			foreach (var tagGroup in _tagGroups)
			{
				var productTagGroup = defaultProductInfo.TagGroups.FirstOrDefault(tg => tg.Id == tagGroup.Key);
				if (productTagGroup == null) continue;

				if (SpecificTagGroupIds.Contains(productTagGroup.Id.ToString(CultureInfo.InvariantCulture)) && _cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
				{
					if (!_isTemaTagExported && productTagGroup.Tags != null && productTagGroup.Tags.Count > 0)
					{
						string tema = GetTagTemaValue(productInfo.DefaultProductInfo, productTagGroup);
						var translations = GetTagTemaValueTranslations(productInfo, productTagGroup);
						AddAttribute(additionalAttributesNode, CdonExportXmlNodes.AttributeNode, string.Format(_tagAttributeId, productTagGroup.Id), tema, translations, null, true);
					}
					continue;
				}

				AddProductDefaultSelectedTagAttributes(additionalAttributesNode, CdonExportXmlNodes.AttributeNode, tagGroup.Key, productTagGroup.Tags, null, true, null);
			}

			if (!additionalAttributesNode.HasChildNodes && additionalAttributesNode.ParentNode != null)
			{
				additionalAttributesNode.ParentNode.RemoveChild(additionalAttributesNode);
			}
		}
		protected virtual void AddProductVariationAttributeAdditionalAttribute(XmlNode parentNode, Attribute attribute, string attributeValue, Dictionary<int, object> attributeValueTranslations)
		{
			AddAttribute(parentNode, CdonExportXmlNodes.AttributeNode, ((int)attribute).ToString(CultureInfo.InvariantCulture), attributeValue, attributeValueTranslations, null, true);
		}
		protected virtual void AddProductVariationAttributeAdditionalAttribute(XmlNode parentNode, DefaultAttribute attribute, string attributeValue, Dictionary<int, object> attributeValueTranslations)
		{
			AddAttribute(parentNode, CdonExportXmlNodes.AttributeNode, attribute.ToString(), attributeValue, attributeValueTranslations, null, true);
		}

		// Attributes.
		protected virtual void AddAttribute(XmlNode parentNode, string attributeNodeName, string attributeId, string attributeValue, Dictionary<int, object> attributeValueTranslations, string classId, bool allowIllegalCharacters)
		{
			bool isValueNullOrEmpty = string.IsNullOrEmpty(attributeValue);
			bool isTranslationsNullOrEmpty = IsTranslationsNullOrEmpty(attributeValueTranslations, attributeId);

			if (isValueNullOrEmpty && isTranslationsNullOrEmpty) return;

			// attribute
			XmlNode attributeNode = AddNode(_xmlDocument, parentNode, attributeNodeName);
			// attribute_id
			AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeIdNode, attributeId);
			// attribute_value
			if (!isValueNullOrEmpty)
			{
				if (!allowIllegalCharacters)
				{
					AddNodeWithoutIllegalCharacters(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeValueNode, attributeValue);
				}
				else
				{
					AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeValueNode, attributeValue);
				}
			}
			// class_id
			if (classId != null)
			{
				AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.ClassIdNode, classId);
			}

			if (isTranslationsNullOrEmpty) return;

			// attribute_value_translations
			var attributeValueTranslationsNode = AddNode(_xmlDocument, attributeNode, CdonExportXmlNodes.AttributeValueTranslationsNode);

			foreach (var valueTranslation in attributeValueTranslations)
			{
				if (valueTranslation.Value == null) continue;

				bool isImage = valueTranslation.Value is IImage;

				string value = isImage ? GetImageUrl((IImage)valueTranslation.Value, valueTranslation.Key) : valueTranslation.Value.ToString();
				if (string.IsNullOrEmpty(value)) continue;

				// attribute_value_translation
				var attributeValueTranslationNode = AddNode(_xmlDocument, attributeValueTranslationsNode, CdonExportXmlNodes.AttributeValueTranslationNode);
				// language_iso_code
				AddNode(_xmlDocument, attributeValueTranslationNode, CdonExportXmlNodes.LanguageIsoCodeNode, _cdonExportService.GetCountryIsoCode(valueTranslation.Key));
				// attribute_value_translation_value
				if (isImage)
				{
					AddNode(_xmlDocument, attributeValueTranslationNode, CdonExportXmlNodes.AttributeValueTranslationValueNode, value);
				}
				else
				{
					AddNodeWithoutIllegalCharacters(_xmlDocument, attributeValueTranslationNode, CdonExportXmlNodes.AttributeValueTranslationValueNode, value);
				}
			}
		}

		// Product variations.
		protected virtual void AddProductVariations(XmlNode parentNode, ICdonProductInfo cdonProductInfo)
		{
			var template = GetTemplateType(cdonProductInfo);

			// product_variations
			XmlNode productVariations = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductVariationsNode);
			// template_id
			AddNode(_xmlDocument, productVariations, CdonExportXmlNodes.ProductVariationsTemplateIdNode, (int)template);

			switch (template)
			{
				case TemplateType.Size:
					foreach (var productSize in cdonProductInfo.ProductSizesCollection)
					{
						// attributes - SIZE.
						var attributes = new Collection<string> {string.Format(_sizeAttributeId, productSize.SizeInfo.Id)};

						// product_variation
						AddProductVariation(productVariations, cdonProductInfo.DefaultProductInfo.Product, productSize.ErpId, productSize.NumberInStock, attributes);
					}
					break;
				case TemplateType.Color:
					foreach (var productVariant in cdonProductInfo.ProductVariantsCollection)
					{
						if (productVariant.Value.DefaultProductInfo.Color == null) continue;

						var product = productVariant.Value.DefaultProductInfo.Product;

						// attributes - COLOR.
						var attributeId = string.Format(_colorAttributeId, productVariant.Value.DefaultProductInfo.Color.Id);
						var attributes = new Collection<string> {attributeId};

						// product_variation
						var productVariation = AddProductVariation(productVariations, product, GetSkuId(product.ErpId), product.TotalNumberInStock, attributes);
						// product_countries
						AddProductCountries(productVariation, productVariant.Value, false);
					}
					break;
				case TemplateType.SizeOrColor:
					foreach (var productVariant in cdonProductInfo.ProductVariantsCollection)
					{
						var productInfo = productVariant.Value.DefaultProductInfo;
						if (productInfo.Color == null) continue;

						if (productVariant.Value.ProductSizesCollection.Count > 0)
						{
							foreach (var productSize in productVariant.Value.ProductSizesCollection)
							{
								// attributes - SIZE.
								var attributes = new Collection<string> {string.Format(_sizeAttributeId, productSize.SizeInfo.Id)};
								// attributes - COLOR.
								var attributeId = string.Format(_colorAttributeId, productInfo.Color.Id);
								attributes.Add(attributeId);

								// product_variation
								var productVariation = AddProductVariation(productVariations, productInfo.Product, productSize.ErpId, productSize.NumberInStock, attributes);
								// product_countries
								AddProductCountries(productVariation, productVariant.Value, false);
							}
						}
						else
						{
							// attributes - COLOR.
							var attributeId = string.Format(_colorAttributeId, productInfo.Color.Id);
							var attributes = new Collection<string> {attributeId};

							// product_variation
							var productVariation = AddProductVariation(productVariations, productInfo.Product, GetSkuId(productInfo.Product.ErpId), productInfo.Product.TotalNumberInStock, attributes);
							// product_countries
							AddProductCountries(productVariation, productVariant.Value, false);
						}
					}
					break;
			}
		}
		protected virtual XmlNode AddProductVariation(XmlNode parentNode, ILekmerProductView product, string skuId, int quantity, Collection<string> attributes)
		{
			// product_variation
			var productVariation = AddNode(_xmlDocument, parentNode, CdonExportXmlNodes.ProductVariationNode);
			// sku_id
			AddNode(_xmlDocument, productVariation, CdonExportXmlNodes.SkuIdNode, skuId);

			// product_status
			// product_expose_status
			var status = GetProductStatus(product.ProductStatusId);
			var exposeStatus = GetProductExposeStatus(product.IsBookable, quantity);
			AddNode(_xmlDocument, productVariation, CdonExportXmlNodes.ProductStatusNode, status.ToString());
			AddNode(_xmlDocument, productVariation, CdonExportXmlNodes.ProductExposeStatusNode, exposeStatus.ToString());

			// product_quantity
			AddNode(_xmlDocument, productVariation, CdonExportXmlNodes.ProductQuantityNode, quantity);
			// variation_attribute
			foreach (var attribute in attributes)
			{
				AddNode(_xmlDocument, productVariation, CdonExportXmlNodes.ProductVariationAttributeNode, attribute);
			}

			return productVariation;
		}

		protected virtual string GetImportId(string importPreviousId)
		{
			if (!string.IsNullOrEmpty(importPreviousId))
			{
				string[] idParameters = importPreviousId.Split('-');
				if (idParameters.Length == 2)
				{
					int id;
					if (int.TryParse(idParameters[1], out id))
					{
						return string.Format("{0}-{1}", _cdonExportSetting.ApplicationName.ToLower(), ++id);
					}
				}
			}

			return string.Format("{0}-{1}", _cdonExportSetting.ApplicationName.ToLower(), 1);
		}

		protected virtual void PopulateMainCategories(ICategoryView category)
		{
			if (category.ParentCategoryId == null)
			{
				_mainCategories.Add(category.Id);
			}
		}
		protected virtual ICategoryTree FindCategoryTree(Dictionary<int, ICategoryTree> categoryTrees, int channelId)
		{
			return categoryTrees.ContainsKey(channelId) ? categoryTrees[channelId] : null;
		}

		protected virtual int GetProductQuantity(ICdonProductInfo productInfo, bool hasVariants)
		{
			int quantity = 0;

			if (hasVariants)
			{
				foreach (var cdonProductInfo in productInfo.ProductVariantsCollection.Values)
				{
					quantity = quantity + CalculateProductSisezQuantity(cdonProductInfo);
				}
			}
			else
			{
				quantity = CalculateProductSisezQuantity(productInfo);
			}

			return quantity;
		}
		protected virtual int CalculateProductSisezQuantity(ICdonProductInfo productInfo)
		{
			int quantity = 0;

			if (productInfo.ProductSizesCollection != null && productInfo.ProductSizesCollection.Count > 0)
			{
				foreach (var productSize in productInfo.ProductSizesCollection)
				{
					quantity = quantity + productSize.NumberInStock;
				}
			}
			else
			{
				quantity = quantity + productInfo.DefaultProductInfo.Product.TotalNumberInStock;
			}

			return quantity;
		}
		protected virtual void GetProductStatuses(ICdonProductInfo productInfo, bool hasVariants, ILekmerProductView defaultProductInfo, int quantity, out CdonProductStatus status, out CdonProductExposeStatus exposeStatus)
		{
			status = CdonProductStatus.OFFLINE;
			exposeStatus = CdonProductExposeStatus.WATCHABLE;

			if (!hasVariants)
			{
				status = GetProductStatus(defaultProductInfo.ProductStatusId);
				exposeStatus = GetProductExposeStatus(defaultProductInfo.IsBookable, quantity);
			}
			else
			{
				foreach (KeyValuePair<int, ICdonProductInfo> variantProductInfo in productInfo.ProductVariantsCollection)
				{
					ILekmerProductView product = variantProductInfo.Value.DefaultProductInfo.Product;

					var variantStatus = GetProductStatus(product.ProductStatusId);
					if (variantStatus == CdonProductStatus.ONLINE)
					{
						status = CdonProductStatus.ONLINE;
					}

					var variantExposeStatus = GetProductExposeStatus(product.IsBookable, CalculateProductSisezQuantity(variantProductInfo.Value));
					if (variantExposeStatus == CdonProductExposeStatus.BUYABLE)
					{
						exposeStatus = CdonProductExposeStatus.BUYABLE;
					}
					if (exposeStatus != CdonProductExposeStatus.BUYABLE && variantExposeStatus == CdonProductExposeStatus.BOOKABLE)
					{
						exposeStatus = CdonProductExposeStatus.BOOKABLE;
					}
				}
			}
		}
		protected virtual CdonProductStatus GetProductStatus(int productStatusId)
		{
			return (CdonProductStatus)productStatusId == CdonProductStatus.ONLINE ? CdonProductStatus.ONLINE : CdonProductStatus.OFFLINE;
		}
		protected virtual CdonProductExposeStatus GetProductExposeStatus(bool isBookable, int quantity)
		{
			CdonProductExposeStatus cdonProductExposeStatus;

			if (quantity > 0)
			{
				cdonProductExposeStatus = CdonProductExposeStatus.BUYABLE;
			}
			else if (isBookable)
			{
				cdonProductExposeStatus = CdonProductExposeStatus.BOOKABLE;
			}
			else
			{
				cdonProductExposeStatus = CdonProductExposeStatus.WATCHABLE;
			}

			return cdonProductExposeStatus;
		}
		protected virtual string GetProductCountrySellableStatus(ILekmerProductView product, int channelid)
		{
			bool isHeppoRestricted = _restrictionHelper.IsHeppoRestricted(product.Id, product.CategoryId, product.BrandId, channelid);

			var cdonProductCountrySellableStatus = !isHeppoRestricted ? CdonProductCountrySellableStatus.SELLABLE : CdonProductCountrySellableStatus.NOT_SELLABLE;

			return cdonProductCountrySellableStatus.ToString();
		}
		protected virtual Dictionary<int, object> GetProductTitleTranslations(ICdonProductInfo productInfo)
		{
			var translations = new Dictionary<int, object>();

			foreach (var channelProductInfo in productInfo.ChannelProductInfoCollection)
			{
				var channelId = channelProductInfo.Channel.Id;
				if (!translations.ContainsKey(channelId))
				{
					var title = GetProductTitle(channelProductInfo.Product, channelId);
					translations.Add(channelId, title);
				}
			}

			return translations;
		}
		protected virtual string GetProductTitle(ILekmerProductView product, int channelId)
		{
			var title = product.DisplayTitle;

			if (product.BrandId.HasValue && _brands != null)
			{
				var channelBrand = _brands[product.BrandId.Value];
				if (channelBrand != null)
				{
					bool isBrandAdded = AddBrandToProductTitle(channelBrand, channelId, ref title);
					if (!isBrandAdded)
					{
						var key = channelBrand.Keys.First();
						AddBrandToProductTitle(channelBrand, key, ref title);
					}
				}
			}

			return title;
		}
		private bool AddBrandToProductTitle(Dictionary<int, IBrand> channelBrand, int channelId, ref string productTitle)
		{
			var brand = channelBrand[channelId];
			if (brand != null && !string.IsNullOrEmpty(brand.Title))
			{
				if (!productTitle.StartsWith(brand.Title, true, CultureInfo.InvariantCulture))
				{
					productTitle = brand.Title + " - " + productTitle;
				}
				return true;
			}

			return false;
		}
		protected virtual string GetProductDescription(ILekmerProductView product, IChannel channel)
		{
			var description = string.Empty;

			if (!_tagAliasCollection.ContainsKey(channel.Id))
			{
				return description;
			}

			var tagAliasAll = _tagAliasCollection[channel.Id];

			foreach (var tagGroup in product.TagGroups)
			{
				foreach (var tag in tagGroup.Tags)
				{
					var key = tagGroup.CommonName + "_" + tag.Value;
					if (tagAliasAll.ContainsKey(key))
					{
						var alias = _cdonExportService.GetAlias(channel, tagAliasAll[key]);
						if (!string.IsNullOrEmpty(alias))
						{
							description = description + string.Format("<li>{0}</li>", alias);
						}
					}
				}
			}

			if (!string.IsNullOrEmpty(description))
			{
				description = string.Format("<ul id=\"tagPoints\">{0}</ul>", description);
			}

			return description;
		}
		protected virtual Dictionary<int, object> GetMissingDescriptionTranslations(ICdonProductInfo productInfo, Dictionary<int, object> translations)
		{
			var correctedTranslations = new Dictionary<int, object>();

			foreach (var translation in translations)
			{
				if (string.IsNullOrEmpty(translation.Value.ToString()))
				{
					if (!_tagAliasCollection.ContainsKey(translation.Key))
					{
						correctedTranslations.Add(translation.Key, translation.Value);
						continue;
					}

					var description = string.Empty;

					var tagAliasAll = _tagAliasCollection[translation.Key];

					var product = productInfo.ChannelProductInfoCollection.FirstOrDefault(pi => pi.Channel.Id == translation.Key);
					if (product != null)
					{
						foreach (var tagGroup in product.Product.TagGroups)
						{
							foreach (var tag in tagGroup.Tags)
							{
								var key = tagGroup.CommonName + "_" + tag.Value;
								if (tagAliasAll.ContainsKey(key))
								{
									var alias = _cdonExportService.GetAlias(translation.Key, tagAliasAll[key]);
									if (!string.IsNullOrEmpty(alias))
									{
										description = description + string.Format("<li>{0}</li>", alias);
									}
								}
							}
						}

						if (!string.IsNullOrEmpty(description))
						{
							description = string.Format("<ul id=\"tagPoints\">{0}</ul>", description);
						}

						correctedTranslations.Add(translation.Key, description);
					}
				}
				else
				{
					correctedTranslations.Add(translation.Key, translation.Value);
				}
			}

			return correctedTranslations;
		}

		protected virtual string GetImageUrl(IImage image, int channelId)
		{
			IChannel channel = _cdonExportService.GetChannel(channelId);
			string sizeCommonName = _cdonExportSetting.SizeCommonName;
			string mediaUrl = _mediaUrlService.ResolveMediaArchiveExternalUrl(channel);

			if (sizeCommonName.IsEmpty())
			{
				return MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image);
			}

			return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, image, sizeCommonName);
		}

		protected virtual bool IsIconCommon(ICdonProductInfo productInfo, int iconId)
		{
			bool canBeExported = true;

			if (productInfo.ProductVariantsCollection != null)
			{
				foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
				{
					if (variationProductInfo.Value.ProductIconsCollection.FirstOrDefault(i => i.Id == iconId) == null)
					{
						canBeExported = false;
						break;
					}
				}
			}

			return canBeExported;
		}
		protected virtual bool IsTagCommon(ICdonProductInfo productInfo, ITag tag)
		{
			bool canBeExported = true;
			if (productInfo.ProductVariantsCollection != null)
			{
				foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
				{
					var tagGroup = variationProductInfo.Value.DefaultProductInfo.Product.TagGroups.FirstOrDefault(tg => tg.Id == tag.TagGroupId);
					if (tagGroup == null)
					{
						canBeExported = false;
						break;
					}

					if (tagGroup.Tags.FirstOrDefault(t => t.Id == tag.Id) == null)
					{
						canBeExported = false;
						break;
					}
				}
			}

			return canBeExported;
		}
		protected virtual bool IsAgeFromMonthCommon(ICdonProductInfo productInfo)
		{
			bool canBeExported = true;

			if (productInfo.ProductVariantsCollection != null)
			{
				foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
				{
					if (variationProductInfo.Value.DefaultProductInfo.Product.AgeFromMonth == productInfo.DefaultProductInfo.Product.AgeFromMonth) continue;

					canBeExported = false;
					break;
				}
			}

			return canBeExported;
		}
		protected virtual bool IsAgeToMonthCommon(ICdonProductInfo productInfo)
		{
			bool canBeExported = true;

			if (productInfo.ProductVariantsCollection != null)
			{
				foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
				{
					if (variationProductInfo.Value.DefaultProductInfo.Product.AgeToMonth == productInfo.DefaultProductInfo.Product.AgeToMonth) continue;

					canBeExported = false;
					break;
				}
			}

			return canBeExported;
		}

		protected virtual string GetEnumDescription(Enum value)
		{
			FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
			var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return attributes.Length > 0 ? attributes[0].Description : value.ToString();
		}
		protected virtual Dictionary<string, string> GetAttributeTranslations(Attribute attribute)
		{
			var channelTranslations = new Dictionary<string, string>();

			string attributeDescription = GetEnumDescription(attribute);
			var descriptions = attributeDescription.Split(',').ToList();
			foreach (var description in descriptions)
			{
				string[] translation = description.Split(':');
				if (translation.Length == 2 && !string.IsNullOrEmpty(translation[0]) && !string.IsNullOrEmpty(translation[1]))
				{
					if (!channelTranslations.ContainsKey(translation[1]))
					{
						channelTranslations.Add(translation[1], translation[0]);
					}
				}
			}

			return channelTranslations;
		}

		protected virtual Dictionary<int, IProductSize> GetAllSizes(ICdonProductInfo productInfo)
		{
			var sizes = new Dictionary<int, IProductSize>();

			bool hasVariants = productInfo.ProductVariantsCollection != null && productInfo.ProductVariantsCollection.Count > 0;
			if (!hasVariants)
			{
				sizes = productInfo.ProductSizesCollection.ToDictionary(s => s.SizeInfo.Id, s => s);
			}
			else
			{
				foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
				{
					if (variationProductInfo.Value.DefaultProductInfo.Color == null) continue;

					foreach (var size in variationProductInfo.Value.ProductSizesCollection)
					{
						if (!sizes.ContainsKey(size.SizeInfo.Id))
						{
							sizes.Add(size.SizeInfo.Id, size);
						}
					}
				}
			}

			return sizes;
		}

		protected virtual TemplateType GetTemplateType(ICdonProductInfo productInfo)
		{
			var template = TemplateType.SizeOrColor;

			bool hasVariants = productInfo.ProductVariantsCollection != null && productInfo.ProductVariantsCollection.Count > 0;
			if (!hasVariants)
			{
				template = TemplateType.Size;
			}
			else
			{
				bool hasSizes = false;
				foreach (var variantProductInfo in productInfo.ProductVariantsCollection)
				{
					if (variantProductInfo.Value.ProductSizesCollection.Count > 0)
					{
						hasSizes = true;
						break;
					}
				}

				if (!hasSizes)
				{
					template = TemplateType.Color;
				}
			}

			return template;
		}

		protected virtual bool HasCorrectVariants(ICdonProductInfo productInfo)
		{
			bool hasCorrectVariants = false;

			foreach (var variationProductInfo in productInfo.ProductVariantsCollection)
			{
				var variationInfo = variationProductInfo.Value.DefaultProductInfo;
				if (variationInfo.Color != null)
				{
					hasCorrectVariants = true;
					break;
				}
			}

			return hasCorrectVariants;
		}

		protected virtual bool IsTranslationsNullOrEmpty(Dictionary<int, object> attributeValueTranslations, string attributeId)
		{
			if (attributeValueTranslations == null
				|| attributeValueTranslations.Count <= 0
				|| attributeId.Equals(DefaultAttribute.CDON_PRODUCT_FRONT_IMAGE.ToString())
				|| attributeId.Equals(DefaultAttribute.CDON_PRODUCT_EXTRA_IMAGE.ToString())
				|| ((int)Attribute.ICON).ToString(CultureInfo.InvariantCulture) == attributeId)
			{
				return true;
			}

			bool hasTranslations = attributeValueTranslations.Any(valueTranslation => valueTranslation.Value != null && valueTranslation.Value.ToString() != string.Empty);
			return !hasTranslations;
		}

		protected virtual string GetCdonProductId(ILekmerProductView defaultProductInfo, bool hasVariants, bool hasSizes)
		{
			string cdonProductId;
			
			if (!hasVariants && !hasSizes)
			{
				cdonProductId = GetSkuId(defaultProductInfo.ErpId);

				var productErpIdSplitted = cdonProductId.Split(_erpSeperator);
				var articleId = int.Parse(productErpIdSplitted[0]);
				if (!_productMapping.ContainsKey(articleId))
				{
					_cdonExportService.ProductMappingSave(defaultProductInfo.Id, articleId);
				}
			}
			else
			{
				var productErpIdSplitted = defaultProductInfo.ErpId.Split(_erpSeperator);
				var articleId = int.Parse(productErpIdSplitted[0]);
				cdonProductId = _productMapping.ContainsKey(articleId) 
					? _productMapping[articleId].ToString(CultureInfo.InvariantCulture) 
					: _cdonExportService.ProductMappingSave(defaultProductInfo.Id, articleId).ToString(CultureInfo.InvariantCulture);
			}

			return cdonProductId;
		}

		protected  virtual string GetSkuId(string erpId)
		{
			var skuId = erpId;

			if (skuId.Split(_erpSeperator).Count() == 2)
			{
				var noSize = string.Empty;
				if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.HeppoApplication)
				{
					noSize = _noSizeHeppo;
				}
				else if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
				{
					noSize = _noSizeLekmer;
				}

				skuId = skuId + _erpSeperator + noSize;
			}

			return skuId;
		}

		protected virtual Dictionary<int, object> GetTagColorValueTranslations(ICdonProductInfo variationInfo)
		{
			var translations = new Dictionary<int, object>();

			foreach (var channelProductInfo in variationInfo.ChannelProductInfoCollection)
			{
				var channelId = channelProductInfo.Channel.Id;
				if (!translations.ContainsKey(channelId))
				{
					var color = GetTagColorValue(channelProductInfo);
					translations.Add(channelId, color);
				}
			}

			return translations;
		}

		protected virtual string GetTagColorValue(ICdonChannelProductInfo variationInfo)
		{
			string color = variationInfo.Color.Value;

			var colorTagGroup = variationInfo.Product.TagGroups.FirstOrDefault(tg => tg.CommonName == _cdonExportSetting.ColorTagGroupCommonName);
			if (colorTagGroup != null && colorTagGroup.Tags != null && colorTagGroup.Tags.Count > 0)
			{
				color = string.Empty;
				foreach (var tag in colorTagGroup.Tags)
				{
					if (!string.IsNullOrEmpty(tag.Value))
					{
						color = color + tag.Value + _colorSeperator;
					}
				}

				int lastDelimiterPosition = color.LastIndexOf(_colorSeperator, StringComparison.Ordinal);
				if (lastDelimiterPosition > 0)
				{
					color = color.Substring(0, lastDelimiterPosition);
				}
			}

			return color;
		}

		protected virtual Dictionary<int, object> GetTagTemaValueTranslations(ICdonProductInfo productInfo, ITagGroup tagGroup)
		{
			var translations = new Dictionary<int, object>();

			foreach (var channelProductInfo in productInfo.ChannelProductInfoCollection)
			{
				var channelId = channelProductInfo.Channel.Id;
				if (!translations.ContainsKey(channelId))
				{
					var tema = GetTagTemaValue(channelProductInfo, tagGroup);
					translations.Add(channelId, tema);
				}
			}

			return translations;
		}

		protected virtual string GetTagTemaValue(ICdonChannelProductInfo productInfo, ITagGroup tagGroup)
		{
			string tema = string.Empty;

			foreach (var tag in tagGroup.Tags)
			{
				if (!string.IsNullOrEmpty(tag.Value))
				{
					tema = tema + tag.Value + _commaSeperator;
				}
			}

			int lastDelimiterPosition = tema.LastIndexOf(_commaSeperator, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				tema = tema.Substring(0, lastDelimiterPosition);
			}

			return tema;
		}
	}
}