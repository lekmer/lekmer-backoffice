﻿using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Litium.Lekmer.CdonExport
{
	public class XmlFileValidator
	{
		private string _msgError = string.Empty;

		public bool Validate(string xmlFilePath, string xsdFilePath)
		{
			return Validate(GetFileStream(xmlFilePath), GetFileStream(xsdFilePath));
		}

		public bool Validate(Stream xml, Stream xsd)
		{
			ClearErrorMessage();

			var xmlTextReader = new XmlTextReader(xsd);

			var schema = new XmlSchemaSet();
			schema.Add(null, xmlTextReader);

			var settings = new XmlReaderSettings();
			settings.ValidationType = ValidationType.Schema;
			settings.Schemas.Add(schema);
			settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
			settings.ValidationEventHandler += ErrorHandler;

			// Validate XML data
			using (var reader = XmlReader.Create(xml, settings))
			{
				while (reader.Read()) { }
			}
			xml.Close();
			xsd.Close();

			return string.IsNullOrEmpty(_msgError);
		}

		// if a validation error occurred, this will return the message
		public string GetError()
		{
			return _msgError;
		}

		private void ErrorHandler(object sender, ValidationEventArgs args)
		{
			_msgError = _msgError + "\r\n" + args.Message;
		}

		private void ClearErrorMessage()
		{
			_msgError = string.Empty;
		}

		// returns a stream of the contents of the given filename
		private Stream GetFileStream(string filename)
		{
			try
			{
				return new FileStream(filename, FileMode.Open);
			}
			catch
			{
				return null;
			}
		}
	}
}