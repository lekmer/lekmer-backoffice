﻿using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionProductService : ICdonExportRestrictionProductService
	{
		protected CdonExportRestrictionProductRepository Repository { get; private set; }

		public CdonExportRestrictionProductService(CdonExportRestrictionProductRepository repository)
		{
			Repository = repository;
		}

		public Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			return Repository.GetIncludeAll();
		}

		public Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			return Repository.GetRestrictionAll();
		}
	}
}