﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template;
using log4net;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportService : ICdonExportService
	{
		public string TagAttributeValuePrefix
		{
			get { return "1{0}"; }
		}
		protected readonly string IconAttributeValueIdPrefix = "2{0}";

		public ILog Log { get; set; }

		private Dictionary<int,int> _distinctProductIds = new Dictionary<int, int>();

		private Collection<IChannel> _exportChannels;
		private IEnumerable<IUserContext> _userContexts;
		private IProductTagHash _productTagHash;
		private Collection<IProductChangeEvent> _productChanges;

		private Dictionary<int, Collection<IColor>> _channelColors;
		private Dictionary<int, Collection<IColor>> ChannelColors
		{
			get
			{
				if (_channelColors == null)
				{
					_channelColors = new Dictionary<int, Collection<IColor>>();

					foreach (IUserContext userContext in _userContexts)
					{
						if (_channelColors.ContainsKey(userContext.Channel.Id)) continue;

						_channelColors.Add(userContext.Channel.Id, ColorService.GetAll(userContext));
					}
				}

				return _channelColors;
			}
		}

		private Dictionary<int, Collection<ITagGroup>> _channelTagGroups;
		private Dictionary<int, Collection<ITagGroup>> ChannelTagGroups
		{
			get
			{
				if (_channelTagGroups == null)
				{
					_channelTagGroups = new Dictionary<int, Collection<ITagGroup>>();

					foreach (IUserContext userContext in _userContexts)
					{
						if (_channelTagGroups.ContainsKey(userContext.Channel.Id)) continue;
						
						_channelTagGroups.Add(userContext.Channel.Id, TagGroupService.GetAll(userContext));
					}
				}

				return _channelTagGroups;
			}
		}

		private RestrictionHelper _restrictionHelper;
		protected RestrictionHelper RestrictionHelper
		{
			get { return _restrictionHelper ?? (_restrictionHelper = new RestrictionHelper(CdonExportSetting)); }
		}

		protected ICdonExportSetting CdonExportSetting { get; private set; }
		protected CdonExportRepository CdonExportRepository { get; private set; }
		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ISizeService SizeService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected IProductTagService ProductTagService { get; private set; }
		protected IProductImageGroupService ProductImageGroupService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected IColorService ColorService { get; private set; }
		protected IProductColorService ProductColorService { get; private set; }
		protected IProductChangesService ProductChangesService { get; private set; }
		protected ICdonExportRestrictionBrandService CdonExportRestrictionBrandService { get; private set; }
		protected ICdonExportRestrictionCategoryService CdonExportRestrictionCategoryService { get; private set; }
		protected ICdonExportRestrictionProductService CdonExportRestrictionProductService { get; private set; }
		protected IProductRegistryRestrictionBrandSecureService RestrictionBrandSecureService { get; private set; }
		protected IProductRegistryRestrictionCategorySecureService RestrictionCategorySecureService { get; private set; }
		protected IProductRegistryRestrictionProductSecureService RestrictionProductSecureService { get; private set; }
		protected ITemplateSharedService TemplateSharedService { get; private set; }
		protected IAliasService AliasService { get; private set; }
		protected IIconSecureService IconSecureService { get; private set; }
		protected IMediaUrlService MediaUrlService;

		public CdonExportService(
			ICdonExportSetting cdonExportSetting,
			CdonExportRepository cdonExportRepository,
			ILekmerChannelService lekmerChannelService,
			ILekmerProductService lekmerProductService,
			ICategoryService categoryService,
			IBrandService brandService,
			ISizeService sizeService,
			ITagGroupService tagGroupService,
			IProductTagService productTagService,
			IProductImageGroupService productImageGroupService,
			IProductSizeService productSizeService,
			IColorService colorService,
			IProductColorService productColorService,
			IProductChangesService productChangesService,
			ICdonExportRestrictionBrandService cdonExportRestrictionBrandService,
			ICdonExportRestrictionCategoryService cdonExportRestrictionCategoryService,
			ICdonExportRestrictionProductService cdonExportRestrictionProductService,
			IProductRegistryRestrictionBrandSecureService restrictionBrandSecureService,
			IProductRegistryRestrictionCategorySecureService restrictionCategorySecureService,
			IProductRegistryRestrictionProductSecureService restrictionProductSecureService,
			ITemplateSharedService templateSharedService,
			IAliasService aliasService,
			IIconSecureService iconSecureService,
			IMediaUrlService mediaUrlService)
		{
			CdonExportSetting = cdonExportSetting;
			CdonExportRepository = cdonExportRepository;
			LekmerChannelService = lekmerChannelService;
			LekmerProductService = lekmerProductService;
			CategoryService = categoryService;
			BrandService = brandService;
			SizeService = sizeService;
			TagGroupService = tagGroupService;
			ProductTagService = productTagService;
			ProductImageGroupService = productImageGroupService;
			ProductSizeService = productSizeService;
			ColorService = colorService;
			ProductColorService = productColorService;
			ProductChangesService = productChangesService;
			CdonExportRestrictionBrandService = cdonExportRestrictionBrandService;
			CdonExportRestrictionCategoryService = cdonExportRestrictionCategoryService;
			CdonExportRestrictionProductService = cdonExportRestrictionProductService;
			RestrictionBrandSecureService = restrictionBrandSecureService;
			RestrictionCategorySecureService = restrictionCategorySecureService;
			RestrictionProductSecureService = restrictionProductSecureService;
			TemplateSharedService = templateSharedService;
			AliasService = aliasService;
			IconSecureService = iconSecureService;
			MediaUrlService = mediaUrlService;

			Initialize();
		}

		protected void Initialize()
		{
			// channels
			if (_exportChannels == null)
			{
				_exportChannels = new Collection<IChannel>();
			}

			string[] excludeChannels = CdonExportSetting.ExcludeChannels.Split(',');

			var allChannels = LekmerChannelService.GetAll();
			foreach (var channel in allChannels)
			{
				if (!excludeChannels.Contains(channel.CommonName))
				{
					_exportChannels.Add(channel);
				}
			}

			_userContexts = GetUserContextForAllChannels();
		}

		protected IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			return _exportChannels.Select(channel =>
			{
				var context = IoC.Resolve<IUserContext>();
				context.Channel = channel;
				return context;
			}).ToList();
		}


		public virtual Dictionary<int, ICategoryTree> GetAllCategoryTrees(Collection<int> topLevelCategoryIds)
		{
			Log.Info("Reading categories.");

			var categoryTrees = new Dictionary<int, ICategoryTree>();
			foreach (IUserContext userContext in _userContexts)
			{
				var categories = CategoryService.GetViewAll(userContext);
				foreach (var topLevelCategoryId in topLevelCategoryIds)
				{
					var category = categories.FirstOrDefault(c => c.Id == topLevelCategoryId);
					if (category != null)
					{
						category.ParentCategoryId = null;
					}
				}

				ICategoryTree tree = new CategoryTree();
				tree.FillTree(categories);
				categoryTrees[userContext.Channel.Id] = tree;
			}
			return categoryTrees;
		}

		public virtual Dictionary<int, Dictionary<int, IBrand>> GetAllBrands()
		{
			Log.Info("Reading brands.");

			var brands = new Dictionary<int, Dictionary<int, IBrand>>();

			foreach (IUserContext userContext in _userContexts)
			{
				BrandCollection brandCollection = BrandService.GetAll(userContext);

				foreach (IBrand brand in brandCollection)
				{
					if (!brands.ContainsKey(brand.Id))
					{
						brands[brand.Id] = new Dictionary<int, IBrand>();
					}

					Dictionary<int, IBrand> brandChannels = brands[brand.Id];

					if (!brandChannels.ContainsKey(userContext.Channel.Id))
					{
						brandChannels[userContext.Channel.Id] = brand;
					}
				}
			}

			return brands;
		}

		public virtual Dictionary<int, Dictionary<int, ITagGroup>> GetAllTagGroups()
		{
			Log.Info("Reading tags.");

			List<string> excludeTagGroups = CdonExportSetting.ExcludeTagGroups.Split(',').ToList();

			var tagGroups = new Dictionary<int, Dictionary<int, ITagGroup>>();

			foreach (IUserContext userContext in _userContexts)
			{
				var tagGroupCollection = ChannelTagGroups[userContext.Channel.Id];
				foreach (ITagGroup tagGroup in tagGroupCollection)
				{
					if (excludeTagGroups.Contains(tagGroup.CommonName)) continue;

					if (!tagGroups.ContainsKey(tagGroup.Id))
					{
						tagGroups[tagGroup.Id] = new Dictionary<int, ITagGroup>();
					}

					Dictionary<int, ITagGroup> tagGroupChannels = tagGroups[tagGroup.Id];

					if (!tagGroupChannels.ContainsKey(userContext.Channel.Id))
					{
						tagGroupChannels[userContext.Channel.Id] = tagGroup;
					}
				}
			}

			return tagGroups;
		}

		public virtual Collection<ISize> GetAllSizes()
		{
			Log.Info("Reading sizes.");

			var sizes = new Collection<ISize>();
			if (_userContexts.Any())
			{
				sizes = SizeService.GetAll(_userContexts.FirstOrDefault());
			}
			return sizes;
		}

		public virtual Dictionary<int, ICdonIcon> GetAllIcons()
		{
			Log.Info("Reading icons.");

			var icons = new Dictionary<int, ICdonIcon>();
			foreach (var icon in IconSecureService.GetAll())
			{
				if (icons.ContainsKey(icon.Id) || icon.Title.IsNullOrEmpty()) continue;

				icons.Add(icon.Id, GetCdonIcon(icon));
			}
			return icons;
		}

		public virtual Dictionary<int, Dictionary<string, string>> GetAllTagAliases()
		{
			Log.Info("Reading tag aliases.");

			var tagAliases = new Dictionary<int, Dictionary<string, string>>();
			foreach (IUserContext userContext in _userContexts)
			{
				if (tagAliases.ContainsKey(userContext.Channel.Id)) continue;

				var tagAliasHelper = new TagAliasHelper(CdonExportSetting, TemplateSharedService, TagGroupService);
				var tagAliasCollection = tagAliasHelper.GetAliasForTags(CdonExportSetting.GetChannelTemplateId(userContext.Channel.CommonName));
				tagAliases.Add(userContext.Channel.Id, tagAliasCollection);
			}
			return tagAliases;
		}

		public virtual Collection<ICdonProductInfo> GetAllProducts()
		{
			var cdonProductInfoCollection = new Collection<ICdonProductInfo>();

			var pageInfo = IoC.Resolve<IPageTracker>().Initialize(CdonExportSetting.PageSize, int.MaxValue);
			Log.Info("Reading product data  - all / all.");
			while (pageInfo.HasNext)
			{
				var productsInfo = GetNextProductListPortion(pageInfo);
				cdonProductInfoCollection.AddRange(productsInfo.GetCdonProductInfoCollection());
				pageInfo.MoveToNext(productsInfo.TotalCount);
				Log.InfoFormat("Reading product data  - {0} / {1}.", pageInfo.ItemsLeft, pageInfo.ItemsTotalCount);
				//break;
			}

			return cdonProductInfoCollection;
		}

		public Collection<ICdonProductInfo> GetChangedProducts()
		{
			var cdonProductInfoCollection = new Collection<ICdonProductInfo>();

			Log.Info("Reading changed products data...");

			_productChanges = new Collection<IProductChangeEvent>();
			while (_productChanges.Count < CdonExportSetting.BatchSize)
			{
				Collection<IProductChangeEvent> productChanges = ProductChangesService.GetCdonExportAllInQueue(CdonExportSetting.PageSize);
				if (productChanges.Count == 0) break;

				_productChanges.AddRange(productChanges);
				SetStatusForProductChanges(productChanges, ProductChangeEventStatus.InProgress);

				var productsInfo = GetNextProductListIncrementsPortion(productChanges);
				cdonProductInfoCollection.AddRange(productsInfo.GetCdonProductInfoCollection());

				Log.InfoFormat("Reading changed products data  - {0}.", _productChanges.Count);
			}

			Log.Info("Reading changed products data is done.");

			return cdonProductInfoCollection;
		}

		#region Variants

		public virtual void PopulateVariantErpIds(Collection<ICdonProductInfo> cdonProductInfoCollection)
		{
			foreach (var cdonProductInfo in cdonProductInfoCollection)
			{
				if (string.IsNullOrEmpty(cdonProductInfo.DefaultProductInfo.Product.ErpId)) continue;

				var erpIdSplitted = cdonProductInfo.DefaultProductInfo.Product.ErpId.Split('-');
				if (erpIdSplitted.Length != 2) continue;

				cdonProductInfo.VariantErpId = erpIdSplitted[0];
			}
		}

		public virtual Collection<ICdonProductInfo> PopulateVariants(Collection<ICdonProductInfo> cdonProductInfoCollection)
		{
			var excludedProducts = new Dictionary<int, int>();
			var groupedCdonProductInfoCollection = new Collection<ICdonProductInfo>();

			foreach (var cdonProductInfo in cdonProductInfoCollection)
			{
				if (excludedProducts.ContainsKey(cdonProductInfo.ProductId)) continue;

				// Add current product.
				ICdonProductInfo localCdonProductInfo = cdonProductInfo;
				var variants = cdonProductInfoCollection.Where(p => p.VariantErpId == localCdonProductInfo.VariantErpId && p.ProductId != localCdonProductInfo.ProductId).ToList();
				if (variants.Count > 0)
				{
					cdonProductInfo.ProductVariantsCollection.Add(cdonProductInfo.ProductId, cdonProductInfo);
					excludedProducts.Add(cdonProductInfo.ProductId, cdonProductInfo.ProductId);

					foreach (var variant in variants)
					{
						if (variant.Available && !cdonProductInfo.ProductVariantsCollection.ContainsKey(variant.ProductId))
						{
							cdonProductInfo.ProductVariantsCollection.Add(variant.ProductId, variant);
							excludedProducts.Add(variant.ProductId, variant.ProductId);
						}
					}
				}

				groupedCdonProductInfoCollection.Add(cdonProductInfo);
			}

			return groupedCdonProductInfoCollection;
		}

		#endregion

		#region Cdon Restrictions

		public virtual Dictionary<int, Collection<int>> GetRestrictedBrands()
		{
			var restrictedBrands = CdonExportRestrictionBrandService.GetRestrictionAll();
			return GetRestrictionItems(restrictedBrands);
		}
		public virtual Dictionary<int, Collection<int>> GetRestrictedCategories()
		{
			var restrictedCategories = CdonExportRestrictionCategoryService.GetRestrictionAll();
			return GetRestrictionItems(restrictedCategories);
		}
		public virtual Dictionary<int, Collection<int>> GetRestrictedProducts()
		{
			var restrictedProducts = CdonExportRestrictionProductService.GetRestrictionAll();
			return GetRestrictionItems(restrictedProducts);
		}

		public virtual Dictionary<int, Collection<int>> GetIncludeBrands()
		{
			var brandCollection = CdonExportRestrictionBrandService.GetIncludeAll();
			return GetRestrictionItems(brandCollection);
		}
		public virtual Dictionary<int, Collection<int>> GetIncludeCategories()
		{
			var categoryCollection = CdonExportRestrictionCategoryService.GetIncludeAll();
			return GetRestrictionItems(categoryCollection);
		}
		public virtual Dictionary<int, Collection<int>> GetIncludeProducts()
		{
			var productCollection = CdonExportRestrictionProductService.GetIncludeAll();
			return GetRestrictionItems(productCollection);
		}

		protected virtual Dictionary<int, Collection<int>> GetRestrictionItems(Collection<ICdonExportRestrictionItem> restrictionItems)
		{
			var restrictions = new Dictionary<int, Collection<int>>();

			foreach (var item in restrictionItems)
			{
				if (!restrictions.ContainsKey(item.ItemId))
				{
					restrictions[item.ItemId] = new Collection<int>();
				}

				Collection<int> channels = restrictions[item.ItemId];

				if (!channels.Contains(item.ChannelId))
				{
					channels.Add(item.ChannelId);
				}
			}

			return restrictions;
		}

		#endregion

		#region Heppo Restrictions

		public virtual Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedBrands()
		{
			var restrictionBrands = RestrictionBrandSecureService.GetAllWithChannel();
			return GetHeppoRestrictedItems(restrictionBrands);
		}
		public virtual Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedCategories()
		{
			var restrictionCategories = RestrictionCategorySecureService.GetAllWithChannel();
			return GetHeppoRestrictedItems(restrictionCategories);
		}
		public virtual Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedProducts()
		{
			var restrictionProducts = RestrictionProductSecureService.GetAllWithChannel();
			return GetHeppoRestrictedItems(restrictionProducts);
		}

		public virtual Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedItems(Collection<IProductRegistryRestrictionItem> restrictionItems)
		{
			var restrictionGroupedByItems = restrictionItems.GroupBy(b => b.ItemId);
			return restrictionGroupedByItems.ToDictionary(b => b.Key, b => new Collection<IProductRegistryRestrictionItem>(b.ToArray()));
		}

		#endregion

		public void SetStatusForProductChanges(Collection<IProductChangeEvent> productChanges, ProductChangeEventStatus status)
		{
			ProductChangesService.SetCdonExportStatus(productChanges ?? _productChanges, status);
		}

		public virtual string GetCountryIsoCode(int channelId)
		{
			string isoCode = string.Empty;

			var channel = _exportChannels.FirstOrDefault(ch => ch.Id == channelId);
			return channel != null ? channel.Country.Iso : isoCode;
		}

		public virtual IChannel GetChannel(int channelId)
		{
			return _exportChannels.FirstOrDefault(ch => ch.Id == channelId);
		}

		public virtual string GetChannelApplicationName(int channelId)
		{
			string appName = string.Empty;

			var channel = _exportChannels.FirstOrDefault(ch => ch.Id == channelId);
			return channel != null ? channel.ApplicationName : appName;
		}

		public virtual string GetAlias(IChannel channel, string aliasCommonName)
		{
			aliasCommonName = aliasCommonName.
				Replace(CdonExportSetting.AliasBeginReplace, string.Empty).
				Replace(CdonExportSetting.AliasBegin, string.Empty).
				Replace(CdonExportSetting.AliasEndReplace, string.Empty).
				Replace(CdonExportSetting.AliasEnd, string.Empty);

			var alias = AliasService.GetByCommonName(channel, aliasCommonName);
			return alias != null ? alias.Value : string.Empty;
		}
		public virtual string GetAlias(int channelId, string aliasCommonName)
		{
			var channel = _exportChannels.FirstOrDefault(ch => ch.Id == channelId);
			return channel != null ? GetAlias(channel, aliasCommonName) : string.Empty;
		}
		
		public virtual Dictionary<int, Dictionary<int, ICdonTag>> GetAllTagsByTagGroup(Dictionary<int, ITagGroup> allTagGroups)
		{
			var tags = new Dictionary<int, Dictionary<int, ICdonTag>>();

			foreach (var channelId in allTagGroups.Keys)
			{
				if (!tags.ContainsKey(channelId))
				{
					tags[channelId] = new Dictionary<int, ICdonTag>();
				}

				Dictionary<int, ICdonTag> tagInfo = tags[channelId];

				foreach (var tag in allTagGroups[channelId].Tags)
				{
					if (!tagInfo.ContainsKey(tag.Id))
					{
						tagInfo[tag.Id] = GetCdonTag(tag);
					}
				}
			}

			return tags;
		}

		#region Cdon Export

		public virtual string GetLastItem()
		{
			CdonExportRepository.EnsureNotNull();
			return CdonExportRepository.GetLastItem();
		}

		public virtual int Save(string importId, CdonExportType type, CdonExportStatus status)
		{
			CdonExportRepository.EnsureNotNull();
			return CdonExportRepository.Save(importId, type, status);
		}

		public virtual void Update(int cdonExportId, CdonExportStatus status)
		{
			CdonExportRepository.EnsureNotNull();
			CdonExportRepository.Update(cdonExportId, status);
		}

		#endregion

		#region Mapping
		
		public Dictionary<int, int> ProductMappingGetAll()
		{
			return CdonExportRepository.ProductMappingGetAll();
		}

		public int ProductMappingSave(int cdonProductId, int cdonArticleId)
		{
			return CdonExportRepository.ProductMappingSave(cdonProductId, cdonArticleId);
		}

		#endregion

		protected virtual ICdonProductInfoList GetNextProductListPortion(IPageTracker pageTracker)
		{
			ProductIdCollection productIdCollection = LekmerProductService.GetIdAllWithoutAnyFilterForCdonExport(pageTracker.Current, pageTracker.PageSize, RestrictionHelper.GetMonthPurchasedAgoValue());
			var productIds = GetNewProductIds(productIdCollection);

			var productInfoList = IoC.Resolve<ICdonProductInfoList>();
			productInfoList.TotalCount = productIdCollection.TotalCount;

			return GetProducts(productInfoList, new ProductIdCollection(productIds));
		}
		protected virtual ICdonProductInfoList GetNextProductListIncrementsPortion(Collection<IProductChangeEvent> productChanges)
		{
			var changedProductIdCollection = new ProductIdCollection(productChanges.Select(p => p.ProductId).Distinct().ToList());
			var productIdCollection = new List<int>(changedProductIdCollection);

			foreach (var productId in changedProductIdCollection)
			{
				var variantIds = CdonExportRepository.GetVariantIdAllByVariantErpId(productId);
				foreach (var variantId in variantIds)
				{
					if (!productIdCollection.Contains(variantId))
					{
						productIdCollection.Add(variantId);
					}
				}
			}

			productIdCollection.Sort();

			var productIds = GetNewProductIds(new ProductIdCollection(productIdCollection));

			var productInfoList = IoC.Resolve<ICdonProductInfoList>();
			productInfoList.TotalCount = productIdCollection.Count;

			return GetProducts(productInfoList, new ProductIdCollection(productIds));
		}
		protected virtual List<int> GetNewProductIds(ProductIdCollection productIdCollection)
		{
			var productIds = new List<int>();

			foreach (var id in productIdCollection)
			{
				if (!_distinctProductIds.ContainsKey(id))
				{
					_distinctProductIds.Add(id, id);
					productIds.Add(id);
				}
			}

			return productIds;
		}

		protected virtual ICdonProductInfoList GetProducts(ICdonProductInfoList productInfoList, ProductIdCollection productIdCollection)
		{
			productInfoList.AddOrReplace(productIdCollection);

			foreach (IUserContext userContext in _userContexts)
			{
				ProductCollection productCollection = LekmerProductService.PopulateViewProductsWithoutAnyFilter(userContext, productIdCollection, false);
				productInfoList.Add(userContext.Channel, productCollection);
			}

			PopulateAdditionalInfo(productInfoList);

			return productInfoList;
		}
		protected virtual void PopulateAdditionalInfo(ICdonProductInfoList cdonProductInfoList)
		{
			var productsNeedToRemove = new Collection<int>();

			PopulateTags();

			Collection<ICdonProductInfo> productInfoCollection = cdonProductInfoList.GetCdonProductInfoCollection();

			foreach (ICdonProductInfo cdonProductInfo in productInfoCollection)
			{
				foreach (ICdonChannelProductInfo channelProductInfo in cdonProductInfo.ChannelProductInfoCollection)
				{
					channelProductInfo.Product.TagGroups = _productTagHash.GetProductTags(channelProductInfo.Channel.Id, channelProductInfo.Product.Id);
					AddColor(channelProductInfo);
				}

				cdonProductInfo.ProductImagesCollection.AddRange(ProductImageGroupService.GetAllFullByProduct(cdonProductInfo.ProductId));
				if (CdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
				{
					AddIcons(cdonProductInfo);
				}

				bool needToRemove;
				cdonProductInfo.ProductSizesCollection.AddRange(GetProductSizesAllByProduct(cdonProductInfo.ProductId, out needToRemove));
				if (needToRemove)
				{
					productsNeedToRemove.Add(cdonProductInfo.ProductId);
				}
			}

			// Remove products with wrong sizes.
			foreach (var productId in productsNeedToRemove)
			{
				cdonProductInfoList.Delete(productId);
			}
		}
		protected virtual void PopulateTags()
		{
			if (_productTagHash != null) return;

			_productTagHash = IoC.Resolve<IProductTagHash>();
			_productTagHash.Add(ProductTagService.GetAll());

			foreach (IUserContext userContext in _userContexts)
			{
				var tagGroupCollection = ChannelTagGroups[userContext.Channel.Id];
				_productTagHash.Add(userContext.Channel.Id, tagGroupCollection);
			}
		}
		protected virtual void AddColor(ICdonChannelProductInfo channelProductInfo)
		{
			var productColor = ProductColorService.GetAllByProductErpId(channelProductInfo.Product.ErpId).FirstOrDefault(pc => pc.ProductId == channelProductInfo.Product.Id);
			if (productColor == null) return;

			var color = ChannelColors[channelProductInfo.Channel.Id].FirstOrDefault(c => c.Id == productColor.ColorId);
			if (color == null) return;

			channelProductInfo.Color = color;
		}
		protected virtual void AddIcons(ICdonProductInfo cdonProductInfo)
		{
			var product = cdonProductInfo.DefaultProductInfo.Product;

			var icons = IconSecureService.GetAllByProduct(cdonProductInfo.ProductId, product.CategoryId, product.BrandId);
			foreach (var icon in icons)
			{
				cdonProductInfo.ProductIconsCollection.Add(GetCdonIcon(icon));
			}
		}
		protected virtual ICdonIcon GetCdonIcon(IIcon icon)
		{
			var cdonIcon = IoC.Resolve<ICdonIcon>();
			cdonIcon.Id = icon.Id;
			cdonIcon.CdonIconId = int.Parse(string.Format(IconAttributeValueIdPrefix, icon.Id));
			cdonIcon.Title = icon.Title;
			return cdonIcon;
		}
		protected virtual ICdonTag GetCdonTag(ITag tag)
		{
			var cdonTag = IoC.Resolve<ICdonTag>();
			cdonTag.Id = tag.Id;
			cdonTag.CdonTagId = int.Parse(string.Format(TagAttributeValuePrefix, tag.Id));
			cdonTag.Value = tag.Value;
			return cdonTag;
		}
		protected virtual Collection<IProductSize> GetProductSizesAllByProduct(int productId, out bool needToRemove)
		{
			var sizes = ProductSizeService.GetAllByProduct(productId);
			return RestrictionHelper.GetProductSizesAllByProduct(sizes, out needToRemove);
		}
	}
}