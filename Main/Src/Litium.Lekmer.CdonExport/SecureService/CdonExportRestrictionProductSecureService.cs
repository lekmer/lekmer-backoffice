﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionProductSecureService : ICdonExportRestrictionProductSecureService
	{
		private const char DELIMITER = ',';

		protected CdonExportRestrictionProductRepository Repository { get; private set; }

		public CdonExportRestrictionProductSecureService(CdonExportRestrictionProductRepository repository)
		{
			Repository = repository;
		}

		public void InsertIncludeProduct(ICdonExportRestrictionItem product, List<int> productChannelIds)
		{
			Repository.InsertIncludeProduct(product, Scensum.Foundation.Convert.ToStringIdentifierList(productChannelIds), DELIMITER);
		}

		public void InsertRestrictionProduct(ICdonExportRestrictionItem product, List<int> productChannelIds)
		{
			Repository.InsertRestrictionProduct(product, Scensum.Foundation.Convert.ToStringIdentifierList(productChannelIds), DELIMITER);
		}

		public void DeleteIncludeProducts(List<int> productIds)
		{
			Repository.DeleteIncludeProducts(Scensum.Foundation.Convert.ToStringIdentifierList(productIds), DELIMITER);
		}

		public void DeleteRestrictionProducts(List<int> productIds)
		{
			Repository.DeleteRestrictionProducts(Scensum.Foundation.Convert.ToStringIdentifierList(productIds), DELIMITER);
		}

		public Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			return Repository.GetIncludeAll();
		}

		public Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			return Repository.GetRestrictionAll();
		}
	}
}