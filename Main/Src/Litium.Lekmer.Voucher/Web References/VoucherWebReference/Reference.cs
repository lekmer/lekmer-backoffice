﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.18063.
// 
#pragma warning disable 1591

namespace Litium.Lekmer.Voucher.VoucherWebReference {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="Service1Soap", Namespace="http://tempuri.org/")]
    public partial class Service1 : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback VoucherCheckOperationCompleted;
        
        private System.Threading.SendOrPostCallback VoucherConsumeOperationCompleted;
        
        private System.Threading.SendOrPostCallback VoucherReserveOperationCompleted;
        
        private System.Threading.SendOrPostCallback VoucherReleaseOperationCompleted;
        
        private System.Threading.SendOrPostCallback GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueOperationCompleted;
        
        private System.Threading.SendOrPostCallback VoucherCountGetByVoucherInfoOperationCompleted;
        
        private System.Threading.SendOrPostCallback VoucherBatchCreateOperationCompleted;
        
        private System.Threading.SendOrPostCallback VoucherGenerateCodesOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public Service1() {
            this.Url = "http://local.voucher.lekmer.se/Service1.asmx";
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event VoucherCheckCompletedEventHandler VoucherCheckCompleted;
        
        /// <remarks/>
        public event VoucherConsumeCompletedEventHandler VoucherConsumeCompleted;
        
        /// <remarks/>
        public event VoucherReserveCompletedEventHandler VoucherReserveCompleted;
        
        /// <remarks/>
        public event VoucherReleaseCompletedEventHandler VoucherReleaseCompleted;
        
        /// <remarks/>
        public event GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompletedEventHandler GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompleted;
        
        /// <remarks/>
        public event VoucherCountGetByVoucherInfoCompletedEventHandler VoucherCountGetByVoucherInfoCompleted;
        
        /// <remarks/>
        public event VoucherBatchCreateCompletedEventHandler VoucherBatchCreateCompleted;
        
        /// <remarks/>
        public event VoucherGenerateCodesCompletedEventHandler VoucherGenerateCodesCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherCheck", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public VoucherConsumeResult VoucherCheck(string code, string siteApplicationName) {
            object[] results = this.Invoke("VoucherCheck", new object[] {
                        code,
                        siteApplicationName});
            return ((VoucherConsumeResult)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherCheckAsync(string code, string siteApplicationName) {
            this.VoucherCheckAsync(code, siteApplicationName, null);
        }
        
        /// <remarks/>
        public void VoucherCheckAsync(string code, string siteApplicationName, object userState) {
            if ((this.VoucherCheckOperationCompleted == null)) {
                this.VoucherCheckOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherCheckOperationCompleted);
            }
            this.InvokeAsync("VoucherCheck", new object[] {
                        code,
                        siteApplicationName}, this.VoucherCheckOperationCompleted, userState);
        }
        
        private void OnVoucherCheckOperationCompleted(object arg) {
            if ((this.VoucherCheckCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherCheckCompleted(this, new VoucherCheckCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherConsume", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public VoucherConsumeResult VoucherConsume(string code, int orderId, decimal usedAmount, string siteApplicationName) {
            object[] results = this.Invoke("VoucherConsume", new object[] {
                        code,
                        orderId,
                        usedAmount,
                        siteApplicationName});
            return ((VoucherConsumeResult)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherConsumeAsync(string code, int orderId, decimal usedAmount, string siteApplicationName) {
            this.VoucherConsumeAsync(code, orderId, usedAmount, siteApplicationName, null);
        }
        
        /// <remarks/>
        public void VoucherConsumeAsync(string code, int orderId, decimal usedAmount, string siteApplicationName, object userState) {
            if ((this.VoucherConsumeOperationCompleted == null)) {
                this.VoucherConsumeOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherConsumeOperationCompleted);
            }
            this.InvokeAsync("VoucherConsume", new object[] {
                        code,
                        orderId,
                        usedAmount,
                        siteApplicationName}, this.VoucherConsumeOperationCompleted, userState);
        }
        
        private void OnVoucherConsumeOperationCompleted(object arg) {
            if ((this.VoucherConsumeCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherConsumeCompleted(this, new VoucherConsumeCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherReserve", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool VoucherReserve(string code) {
            object[] results = this.Invoke("VoucherReserve", new object[] {
                        code});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherReserveAsync(string code) {
            this.VoucherReserveAsync(code, null);
        }
        
        /// <remarks/>
        public void VoucherReserveAsync(string code, object userState) {
            if ((this.VoucherReserveOperationCompleted == null)) {
                this.VoucherReserveOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherReserveOperationCompleted);
            }
            this.InvokeAsync("VoucherReserve", new object[] {
                        code}, this.VoucherReserveOperationCompleted, userState);
        }
        
        private void OnVoucherReserveOperationCompleted(object arg) {
            if ((this.VoucherReserveCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherReserveCompleted(this, new VoucherReserveCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherRelease", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool VoucherRelease(string code) {
            object[] results = this.Invoke("VoucherRelease", new object[] {
                        code});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherReleaseAsync(string code) {
            this.VoucherReleaseAsync(code, null);
        }
        
        /// <remarks/>
        public void VoucherReleaseAsync(string code, object userState) {
            if ((this.VoucherReleaseOperationCompleted == null)) {
                this.VoucherReleaseOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherReleaseOperationCompleted);
            }
            this.InvokeAsync("VoucherRelease", new object[] {
                        code}, this.VoucherReleaseOperationCompleted, userState);
        }
        
        private void OnVoucherReleaseOperationCompleted(object arg) {
            if ((this.VoucherReleaseCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherReleaseCompleted(this, new VoucherReleaseCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public SimpleVoucherBatch GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue(decimal discountValue, string appName, string createdBy) {
            object[] results = this.Invoke("GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue", new object[] {
                        discountValue,
                        appName,
                        createdBy});
            return ((SimpleVoucherBatch)(results[0]));
        }
        
        /// <remarks/>
        public void GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueAsync(decimal discountValue, string appName, string createdBy) {
            this.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueAsync(discountValue, appName, createdBy, null);
        }
        
        /// <remarks/>
        public void GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueAsync(decimal discountValue, string appName, string createdBy, object userState) {
            if ((this.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueOperationCompleted == null)) {
                this.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGiftCardVoucherBatchIdGetByChannelCampaignDiscountValueOperationCompleted);
            }
            this.InvokeAsync("GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue", new object[] {
                        discountValue,
                        appName,
                        createdBy}, this.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueOperationCompleted, userState);
        }
        
        private void OnGiftCardVoucherBatchIdGetByChannelCampaignDiscountValueOperationCompleted(object arg) {
            if ((this.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompleted(this, new GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherCountGetByVoucherInfo", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string VoucherCountGetByVoucherInfo(int voucherInfoId) {
            object[] results = this.Invoke("VoucherCountGetByVoucherInfo", new object[] {
                        voucherInfoId});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherCountGetByVoucherInfoAsync(int voucherInfoId) {
            this.VoucherCountGetByVoucherInfoAsync(voucherInfoId, null);
        }
        
        /// <remarks/>
        public void VoucherCountGetByVoucherInfoAsync(int voucherInfoId, object userState) {
            if ((this.VoucherCountGetByVoucherInfoOperationCompleted == null)) {
                this.VoucherCountGetByVoucherInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherCountGetByVoucherInfoOperationCompleted);
            }
            this.InvokeAsync("VoucherCountGetByVoucherInfo", new object[] {
                        voucherInfoId}, this.VoucherCountGetByVoucherInfoOperationCompleted, userState);
        }
        
        private void OnVoucherCountGetByVoucherInfoOperationCompleted(object arg) {
            if ((this.VoucherCountGetByVoucherInfoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherCountGetByVoucherInfoCompleted(this, new VoucherCountGetByVoucherInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherBatchCreate", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string VoucherBatchCreate(System.DateTime validFrom, string prefix, string suffix, int quantity, decimal discountValue, string appName, int nbrOfToken, string createdBy, bool specialOffer) {
            object[] results = this.Invoke("VoucherBatchCreate", new object[] {
                        validFrom,
                        prefix,
                        suffix,
                        quantity,
                        discountValue,
                        appName,
                        nbrOfToken,
                        createdBy,
                        specialOffer});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherBatchCreateAsync(System.DateTime validFrom, string prefix, string suffix, int quantity, decimal discountValue, string appName, int nbrOfToken, string createdBy, bool specialOffer) {
            this.VoucherBatchCreateAsync(validFrom, prefix, suffix, quantity, discountValue, appName, nbrOfToken, createdBy, specialOffer, null);
        }
        
        /// <remarks/>
        public void VoucherBatchCreateAsync(System.DateTime validFrom, string prefix, string suffix, int quantity, decimal discountValue, string appName, int nbrOfToken, string createdBy, bool specialOffer, object userState) {
            if ((this.VoucherBatchCreateOperationCompleted == null)) {
                this.VoucherBatchCreateOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherBatchCreateOperationCompleted);
            }
            this.InvokeAsync("VoucherBatchCreate", new object[] {
                        validFrom,
                        prefix,
                        suffix,
                        quantity,
                        discountValue,
                        appName,
                        nbrOfToken,
                        createdBy,
                        specialOffer}, this.VoucherBatchCreateOperationCompleted, userState);
        }
        
        private void OnVoucherBatchCreateOperationCompleted(object arg) {
            if ((this.VoucherBatchCreateCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherBatchCreateCompleted(this, new VoucherBatchCreateCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/VoucherGenerateCodes", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string VoucherGenerateCodes(int voucherInfoId, int nbrOfVouchers, System.DateTime validTo) {
            object[] results = this.Invoke("VoucherGenerateCodes", new object[] {
                        voucherInfoId,
                        nbrOfVouchers,
                        validTo});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void VoucherGenerateCodesAsync(int voucherInfoId, int nbrOfVouchers, System.DateTime validTo) {
            this.VoucherGenerateCodesAsync(voucherInfoId, nbrOfVouchers, validTo, null);
        }
        
        /// <remarks/>
        public void VoucherGenerateCodesAsync(int voucherInfoId, int nbrOfVouchers, System.DateTime validTo, object userState) {
            if ((this.VoucherGenerateCodesOperationCompleted == null)) {
                this.VoucherGenerateCodesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnVoucherGenerateCodesOperationCompleted);
            }
            this.InvokeAsync("VoucherGenerateCodes", new object[] {
                        voucherInfoId,
                        nbrOfVouchers,
                        validTo}, this.VoucherGenerateCodesOperationCompleted, userState);
        }
        
        private void OnVoucherGenerateCodesOperationCompleted(object arg) {
            if ((this.VoucherGenerateCodesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.VoucherGenerateCodesCompleted(this, new VoucherGenerateCodesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class VoucherConsumeResult {
        
        private VoucherValueType valueTypeField;
        
        private bool isValidField;
        
        private decimal discountValueField;
        
        private int voucherBatchIdField;
        
        private decimal amountLeftField;
        
        private bool specialOfferField;
        
        /// <remarks/>
        public VoucherValueType ValueType {
            get {
                return this.valueTypeField;
            }
            set {
                this.valueTypeField = value;
            }
        }
        
        /// <remarks/>
        public bool IsValid {
            get {
                return this.isValidField;
            }
            set {
                this.isValidField = value;
            }
        }
        
        /// <remarks/>
        public decimal DiscountValue {
            get {
                return this.discountValueField;
            }
            set {
                this.discountValueField = value;
            }
        }
        
        /// <remarks/>
        public int VoucherBatchId {
            get {
                return this.voucherBatchIdField;
            }
            set {
                this.voucherBatchIdField = value;
            }
        }
        
        /// <remarks/>
        public decimal AmountLeft {
            get {
                return this.amountLeftField;
            }
            set {
                this.amountLeftField = value;
            }
        }
        
        /// <remarks/>
        public bool SpecialOffer {
            get {
                return this.specialOfferField;
            }
            set {
                this.specialOfferField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public enum VoucherValueType {
        
        /// <remarks/>
        Unknown,
        
        /// <remarks/>
        Percentage,
        
        /// <remarks/>
        Price,
        
        /// <remarks/>
        GiftCard,
        
        /// <remarks/>
        Code,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class SimpleVoucherBatch {
        
        private int idField;
        
        private int quantityField;
        
        /// <remarks/>
        public int Id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        public int Quantity {
            get {
                return this.quantityField;
            }
            set {
                this.quantityField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherCheckCompletedEventHandler(object sender, VoucherCheckCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherCheckCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherCheckCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public VoucherConsumeResult Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((VoucherConsumeResult)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherConsumeCompletedEventHandler(object sender, VoucherConsumeCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherConsumeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherConsumeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public VoucherConsumeResult Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((VoucherConsumeResult)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherReserveCompletedEventHandler(object sender, VoucherReserveCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherReserveCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherReserveCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherReleaseCompletedEventHandler(object sender, VoucherReleaseCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherReleaseCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherReleaseCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompletedEventHandler(object sender, GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GiftCardVoucherBatchIdGetByChannelCampaignDiscountValueCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public SimpleVoucherBatch Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((SimpleVoucherBatch)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherCountGetByVoucherInfoCompletedEventHandler(object sender, VoucherCountGetByVoucherInfoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherCountGetByVoucherInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherCountGetByVoucherInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherBatchCreateCompletedEventHandler(object sender, VoucherBatchCreateCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherBatchCreateCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherBatchCreateCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void VoucherGenerateCodesCompletedEventHandler(object sender, VoucherGenerateCodesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class VoucherGenerateCodesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal VoucherGenerateCodesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591