using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Order
{
	public static class OrderUrlHelper
	{
		private static string _cacheBasePath;
		private static DateTime _cacheLastUpdate = DateTime.UtcNow;
		private const int _cacheExpirationTime = 10;

		public static string BasePath
		{
			get
			{
				if (_cacheBasePath == null || CacheIsOld)
				{
					_cacheBasePath = GetBasePath();
					_cacheLastUpdate = DateTime.UtcNow;
				}

				return _cacheBasePath;
			}
		}

		private static string GetBasePath()
		{
			IOrderModuleChannel orderModuleChannel = IoC.Resolve<IOrderModuleChannelService>()
                .GetById(UserContext.Current);
			if (orderModuleChannel == null || !orderModuleChannel.OrderTemplateContentNodeId.HasValue) return null;
			int contentNodeId = orderModuleChannel.OrderTemplateContentNodeId.Value;

            IContentNodeTree contentNodeTree = IoC.Resolve<IContentNodeService>().GetAllAsTree(UserContext.Current);

			IContentNodeTreeItem treeItem = contentNodeTree.FindItemById(contentNodeId);
			if (treeItem == null) return null;

			return treeItem.PageUrl;
		}

		private static bool CacheIsOld
		{
			get { return DateTime.UtcNow > _cacheLastUpdate.AddMinutes(_cacheExpirationTime); }
		}
	}
}