using System;
using System.Web;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Customer;

namespace Litium.Scensum.Web
{
	public class ContentPage : LekmerContentPageHandler
	{
		protected override IContentPageFull GetContentPageCore()
		{
			var contentPage = (IContentPage) Items["ContentPage"];

			var contentPageService = IoC.Resolve<IContentPageService>();
			IContentPageFull contentPageFull = contentPageService.GetFullById(UserContext.Current, contentPage.Id);

			return contentPageFull;
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
			return (IContentNodeTreeItem) Items["ContentNodeTreeItem"];
		}

		protected override Core.Web.Master CreateMaster()
		{
			ApplyActionsToDisablePageCaching();

			return new Master();
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetSignInUrl());
		}

		protected override void AccessDeniedToSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetMyPagesUrl());
		}
 
		protected virtual void ApplyActionsToDisablePageCaching()
		{
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.Cache.SetNoServerCaching();
			Response.Cache.SetNoStore();
			Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
			Response.Expires = -1440;
			Response.CacheControl = "no-cache";
		}
	}
}