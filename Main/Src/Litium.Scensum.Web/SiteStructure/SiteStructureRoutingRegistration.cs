using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.SiteStructure
{
	public static class SiteStructureRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");
			routes.Add(new ContentPageRoute(@"{*url}", new RouteHandler("~/SiteStructure/ContentPage.ashx")));
		}

		public static void RegisterRedirectRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");
			routes.Add(new ContentPageRedirectRoute(@"{*url}", null));
		}
	}
}