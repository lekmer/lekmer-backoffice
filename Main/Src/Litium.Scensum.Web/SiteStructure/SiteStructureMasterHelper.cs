using System;
using System.Text.RegularExpressions;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.SiteStructure
{
	public static class SiteStructureMasterHelper
	{
		private const string _siteStructureUrlMatchPattern = @"\[SiteStructureUrl(Http|Https)?\(""(.*?)""\)\]";
		private const int _siteStructureUrlMatchProtocolPosition = 1;
		private const int _siteStructureUrlMatchCommonNamePosition = 2;

		public static void AddSiteStructureVariables(Fragment fragmentContent)
		{
			if (fragmentContent == null) throw new ArgumentNullException("fragmentContent");

			fragmentContent.AddRegexVariable(
				_siteStructureUrlMatchPattern,
				delegate(Match match)
					{
						string protocol = match.Groups[_siteStructureUrlMatchProtocolPosition].Value;
						string commonName = match.Groups[_siteStructureUrlMatchCommonNamePosition].Value;
						return GetSiteStructureUrl(protocol, commonName);
					},
					VariableEncoding.HtmlEncodeLight,
					RegexOptions.IgnoreCase);
		}

		private static string GetSiteStructureUrl(string protocol, string commonName)
		{
			if (protocol == null) throw new ArgumentNullException("protocol");
			if (commonName == null) throw new ArgumentNullException("commonName");

            IContentNodeTreeItem treeItem = IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, commonName);
			if (treeItem == null) return null;

			string url = treeItem.Url;
			if (string.IsNullOrEmpty(url)) return null;

			switch (protocol)
			{
				case "Http":
					return UrlHelper.ResolveUrlHttp(url);
				case "Https":
					return UrlHelper.ResolveUrlHttps(url);
				default:
					return UrlHelper.ResolveUrl(url);
			}
		}
	}
}