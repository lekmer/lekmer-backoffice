﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Encoder=Litium.Scensum.Foundation.Utilities.Encoder;

namespace Litium.Scensum.Web.Search
{
	public class AutoCompleteEsales : HandlerBase
	{
		private const int _defaultNumberOfItemsToShow = 10;
		private const int _maximumNumberOfItemsToShow = 100;

		protected int NumberOfItemsToShow
		{
			get
			{
				return Math.Min(Request.QueryString.GetInt32OrNull("show-items") ?? _defaultNumberOfItemsToShow, _maximumNumberOfItemsToShow);
			}
		}

		protected override void ProcessRequest()
		{
			var query = Request.QueryString["query"];
			if (string.IsNullOrEmpty(query)) return;

			var searchService = IoC.Resolve<IEsalesAutoCompleteService>();
			Collection<string> suggestions = searchService.FindSuggestions(UserContext.Current, query, NumberOfItemsToShow);

			var json = new StringBuilder();
			json.Append("{");
			json.Append(string.Format(CultureInfo.CurrentCulture, "query:'{0}',", Encoder.EncodeJavaScript(query)));
			json.Append("suggestions:[");
			foreach (var suggestion in suggestions)
			{
				json.Append(string.Format(CultureInfo.CurrentCulture, "'{0}',", Encoder.EncodeJavaScript(suggestion)));
			}
			json.Remove(json.Length - 1, 1);
			json.Append("]");
			json.Append("}");

			Response.ContentType = "text/plain";
			Response.Write(json.ToString());
		}
	}
}