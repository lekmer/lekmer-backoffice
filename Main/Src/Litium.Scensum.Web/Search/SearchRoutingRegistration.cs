using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Search
{
	public static class SearchRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");

			routes.Add(new Route(@"search/autocomplete", new RouteHandler("~/Search/AutoCompleteEsales.ashx")));
			routes.Add(new Route(@"search/autocomplete-old", new RouteHandler("~/Search/AutoComplete.ashx")));
			routes.Add(new Route(@"search/didyoumean", new RouteHandler("~/Search/DidYouMeanEsales.ashx")));
		}
	}
}