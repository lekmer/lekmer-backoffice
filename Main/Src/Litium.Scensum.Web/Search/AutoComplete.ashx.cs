﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Framework.Search;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Search;
using Encoder=Litium.Scensum.Foundation.Utilities.Encoder;

namespace Litium.Scensum.Web.Search
{
	public class AutoComplete : HandlerBase
	{
		protected override void ProcessRequest()
		{
			var query = Request.QueryString["query"];
			if (string.IsNullOrEmpty(query)) return;

			var searchService = IoC.Resolve<IAutoCompleteService>();
			Collection<Hit> hits = searchService.Search(UserContext.Current, query, 1, 10);
			var titles = new Collection<string>(new List<string>(hits.Select(o => o.Title)));

			var json = new StringBuilder();
			json.Append("{");
			json.Append(string.Format(CultureInfo.CurrentCulture, "query:'{0}',", Encoder.EncodeJavaScript(query)));
			json.Append("suggestions:[");
			foreach (var title in titles)
			{
				json.Append(string.Format(CultureInfo.CurrentCulture, "'{0}',", Encoder.EncodeJavaScript(title)));
			}
			json.Remove(json.Length - 1, 1);
			json.Append("]");
			json.Append("}");

			Response.ContentType = "text/plain";
			Response.Write(json.ToString());
		}
	}
}