using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Framework.Statistics;
using Litium.Lekmer.Cache.Setting;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product.Cache;
using Litium.Scensum.Statistics;
using Litium.Scensum.Statistics.TrackerEntity;
using Litium.Scensum.Web.Error;
using log4net;
using log4net.Config;
using Litium.Lekmer.Avail.Web;

namespace Litium.Scensum.Web
{
	public class Global : HttpApplication
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected Global()
		{
			BeginRequest += delegate
			{
				Response.CacheControl = "no-cache";
				Response.Expires = -10000;
			};
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Start(object sender, EventArgs e)
		{
			RoutingRegistration.RegisterRoutes(RouteTable.Routes);

			InitializeCache();

			StatisticsSetup.CompilerLocator = new CompilerLocator();
			StatisticsSetup.WorkerInterval = StatisticsSetting.Instance.WorkerInterval;

			GlobalContext.Properties["SiteName"] = System.Web.Hosting.HostingEnvironment.SiteName;
			XmlConfigurator.Configure();

			Initializer.InitializeAll();
		}

		private static void PopulateProductCache()
		{
			var cacheSetting = new CacheSetting(ProductCache.Instance.ManagerName);

			var populateCount = (int)(cacheSetting.MaximumElementsInCacheBeforeScavenging * 0.8m);
			TimeSpan waitTime = LekmerProductCacheSetting.Instance.ExpirationInMinutes;
			if (populateCount <= 0 || waitTime.Minutes <= 0) return;

			var random = new Random();
			TimeSpan additionalRandomTimeToWait =
				TimeSpan.FromMinutes(random.Next(0, (int)LekmerProductCacheSetting.Instance.RefreshIntervalTolerance.TotalMinutes));

			ProductCachePopulateWorker.StartIfNotStarted(
				UserContext.Current,
				populateCount,
				waitTime + additionalRandomTimeToWait,
				additionalRandomTimeToWait);
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_Start(object sender, EventArgs e)
		{
			PopulateProductCache();

			AlternateVersion.SetAlternate();
			if (!IsCrawler(Request))
			{
				var visitor = new Visitor
				{
					ChannelId = Channel.Current.Id,
					Date = DateTime.Now,
					Alternate = AlternateVersion.UseAlternate,
					Referrer = Request.UrlReferrer == null ? null : Request.UrlReferrer.ToString()
				};
				StatisticsTracker.Track(visitor);
			}

			ILekmerCartService lekmerCartService = (ILekmerCartService)IoC.Resolve<ICartService>();
			lekmerCartService.RetrivePreviousShoppingCard();

			ICustomerSession customerSession = IoC.Resolve<ICustomerSession>();
			CustomerIdentificationCookie.SetCustomerIdentificationId(customerSession);
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Error(object sender, EventArgs e)
		{
			HttpContext context = HttpContext.Current;

			// HttpExceptions with HttpCode 404 should render a PageNotFound.
			var httpException = context.Server.GetLastError() as HttpException;
			if (httpException != null && httpException.GetHttpCode() == 404)
			{
				context.Response.Clear();
				new PageNotFound().ProcessRequest(context);
				Server.ClearError();

				return;
			}

			if (ErrorSetting.Instance.ShowFriendlyError)
			{
				context.Response.Clear();
				new ServerError().ProcessRequest(context);
				Server.ClearError();
			}

			var exception = context.Server.GetLastError();
			if (exception != null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "\r\nServer error (alien).\r\nUrl: {0}", context.Request.Url.AbsoluteUri), exception);
			}
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_End(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_End(object sender, EventArgs e)
		{
		}

		private static bool IsCrawler(HttpRequest request)
		{
			if (request.Browser.Crawler)
			{
				return true;
			}

			string userAgent = request.UserAgent;

			if (userAgent.HasValue())
			{
				var crawlerDetector = IoC.Resolve<ICrawlerDetectorService>();

				return crawlerDetector.IsMatch(userAgent);
			}

			return false;
		}

		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private void InitializeCache()
		{
			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();

			CacheUpdateCleaner.WorkerCleanInterval = new LekmerCacheUpdateCleanerSetting().CacheUpdateIntervalSeconds * 1000;
#if DEBUG
			CacheUpdateCleaner.WorkerCleanInterval = 1 * 1000; // Clean interval lowered in debug mode.
#endif
			CacheUpdateCleaner.StartWorker();
		}
	}
}