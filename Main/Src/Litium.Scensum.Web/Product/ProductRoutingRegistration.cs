using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Product
{
	public static class ProductRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");
			routes.Add(new ProductRoute(@"{title}-{id}", null, new RouteValueDictionary {{"id", @"\d+"}},
										new RouteHandler("~/Product/ProductPage.ashx")));
			routes.Add(new ProductRoute(@"{title}-{id}/{sitestructurecommonname}", null, new RouteValueDictionary { { "id", @"\d+" } },
							  new RouteHandler("~/Product/ProductPage.ashx")));
		}
	}
}