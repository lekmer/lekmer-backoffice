using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using Channel = Litium.Scensum.Core.Web.Channel;

namespace Litium.Scensum.Web.Product
{
	public class ProductEntityMapper : Scensum.Product.Web.ProductEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IProduct item)
		{
			var lekmerProduct = (ILekmerProduct)item;

			ProductHelper.AddUrlVariables(fragment, lekmerProduct.LekmerUrl);

			base.AddEntityVariables(fragment, item);

			var formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>();
			ProductHelper.AddPriceVariables(fragment, item, formatter);

			ProductHelper.AddImageVariables(fragment, item.Image, item.DisplayTitle);

			IBrand brand = lekmerProduct.Brand;
			fragment.AddCondition("Product.HasBrand", brand != null);

			if (brand != null)
			{
				fragment.AddEntity(brand);
			}

			if (IsPriceAffectedByCampaign(item))
			{
				fragment.AddVariable("Product.DiscountInPercent", CalculateDiscountPercent(lekmerProduct.Price.PriceIncludingVat, lekmerProduct.CampaignInfo.Price.IncludingVat));
				fragment.AddVariable("Product.DiscountInAmount", CalculateDiscountAmount(lekmerProduct.Price.PriceIncludingVat, lekmerProduct.CampaignInfo.Price.IncludingVat));
			}
			fragment.AddCondition("Product.IsBookable", lekmerProduct.IsBookable);
			fragment.AddCondition("Product.IsMonitorable", lekmerProduct.IsMonitorable);
			fragment.AddCondition("Product.IsNew", lekmerProduct.IsNewProduct);
			fragment.AddCondition("Product.HasSizes", lekmerProduct.HasSizes);
			fragment.AddCondition("Product.ShowVariantRelations", lekmerProduct.ShowVariantRelations);

			fragment.AddVariable("Product.ErpId", lekmerProduct.ErpId);
			fragment.AddCondition("Product.HasLekmerErpId", lekmerProduct.LekmerErpId.HasValue());
			fragment.AddVariable("Product.LekmerErpId", lekmerProduct.LekmerErpId);

			fragment.AddVariable("Product.EsalesTicket", lekmerProduct.EsalesTicket);

			ProductHelper.AddCampaignAppliedConditions(fragment, item);
			ProductHelper.AddFlags(fragment, item);
			ProductHelper.AddRecommendedPriceData(fragment, lekmerProduct);
			ProductHelper.AddLekmerProductInfo(fragment, lekmerProduct);
			ProductHelper.AddStockRangeValue(fragment, lekmerProduct);
			ProductHelper.AddCampaignPriceType(fragment, lekmerProduct);
			ProductHelper.AddDeliveryTime(fragment, lekmerProduct);
		}

		private static string CalculateDiscountPercent(decimal originalPrice, decimal campaignPrice)
		{
			var discountInPercent = (originalPrice - campaignPrice) * 100 / originalPrice;
			discountInPercent = Math.Round(discountInPercent, 0, MidpointRounding.AwayFromZero);
			return discountInPercent + "%";
		}

		private static string CalculateDiscountAmount(decimal originalPrice, decimal campaignPrice)
		{
			var discount = originalPrice - campaignPrice;
			var formatter = IoC.Resolve<IFormatter>();
			string formatDiscount = formatter.FormatPrice(Channel.Current, discount);
			return formatDiscount;
		}
	}
}