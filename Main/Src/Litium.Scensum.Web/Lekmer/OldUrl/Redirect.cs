namespace Litium.Scensum.Web.Lekmer.OldUrl
{
	public class Redirect
	{
		public string Source { get; set; }

		public string Target { get; set; }
	}
}