using System.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Lekmer
{
	// This class is used to include master template variables (e.g. [Channel.MediaUrl]).
	// But instead of loading a template from the db, it will just be code defined template
	// that only contains [Page.Content].
	internal class EmptyMaster : Master
	{
		public override string Render()
		{
			HttpContext context = HttpContext.Current;
			if (!context.Trace.IsEnabled)
			{
				return RenderInternal();
			}

			const string category = "Master";
			context.Trace.Write(category, "Begin Render");

			string content = RenderInternal();

			context.Trace.Write(category, "End Render");

			return content;
		}

		protected override string RenderInternal()
		{
			Response.ContentType = "text/html";

			Page.AppendMetaTagsToHead();

			var fragmentContent = new Fragment("[Page.Content]");
			AddFragmentContentVariables(fragmentContent);
			return fragmentContent.Render();
		}
	}
}