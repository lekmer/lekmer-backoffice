﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using CartHelper = Litium.Scensum.Core.Web.CartHelper;

namespace Litium.Scensum.Web.Lekmer
{
	/// <summary>
	/// Add all products from wish list into the cart
	/// </summary>
	public class WishListToCart : HandlerBase
	{
		private const int _defaultProductQuantity = 1;
		private const string _referrerFieldName = "referrer";

		private readonly ICartService _cartService = IoC.Resolve<ICartService>();
		private readonly ILekmerProductService _productService = (ILekmerProductService) IoC.Resolve<IProductService>();
		private readonly IWishListService _wishListService = IoC.Resolve<IWishListService>();
		private readonly IWishListCookieService _wishListCookieService = IoC.Resolve<IWishListCookieService>();

		protected override void ProcessRequest()
		{
			var crawlerDetector = IoC.Resolve<ICrawlerDetectorService>();
			if (Request.Browser.Crawler || crawlerDetector.IsMatch(Request.UserAgent))
			{
				Response.Write(@"<!-- CD -->");
				Response.DisablePageCaching();
				return;
			}

			var wishListKey = GetWishListKey();
			if (wishListKey == Guid.Empty)
			{
				return;
			}

			var wishList = _wishListService.GetByKey(UserContext.Current, wishListKey);
			if (wishList == null || !wishList.GetItems().Any())
			{
				return;
			}

			var cart = GetCart();
			foreach (var item in wishList.GetItems())
			{
				AddItemToCart(item, cart, _defaultProductQuantity);
			}

			HandleEmptyCart(cart);

			if (AjaxHelper.IsXmlHttpRequest(Request))
			{
				Response.Write(AjaxHelper.RenderCart());
			}
			else
			{
				Response.Redirect(GetRedirectUrl());
			}
		}

		private void AddItemToCart(IWishListItem wishListItem, ICartFull cart, int quantity)
		{
			if (cart == null) throw new ArgumentNullException("cart");

			int? sizeId = wishListItem.SizeId;

			var product = (ILekmerProductView)GetProduct(wishListItem.ProductId);
			if (product == null)
				return;

			var cartItemPackageElements = new Collection<ICartItemPackageElement>();
			if (product.IsProduct())
			{
				if (product.HasSizes)
				{
					if (!sizeId.HasValue) return;
				}
			}
			else
			{
				ProductIdCollection productIds = _productService.GetIdAllByPackageMasterProduct(UserContext.Current, product.Id);
				ProductCollection packageItems = _productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
				foreach (IWishListPackageItem wishListPackageItem in wishListItem.PackageItems)
				{
					IProduct productView = packageItems.FirstOrDefault(i => i.Id == wishListPackageItem.ProductId);
					if (productView == null) return;

					var item = (ILekmerProductView) productView;

					if (!item.HasSizes) continue;
					if (!wishListPackageItem.SizeId.HasValue) return;

					var cartItemPackageElement = IoC.Resolve<ICartItemPackageElement>();
					cartItemPackageElement.ProductId = wishListPackageItem.ProductId;
					cartItemPackageElement.SizeId = wishListPackageItem.SizeId.Value;
					cartItemPackageElements.Add(cartItemPackageElement);
				}
			}

			((ILekmerCartFull)cart).ChangeItemQuantity(UserContext.Current, product, quantity, sizeId, cartItemPackageElements);
			cart.Id = _cartService.Save(UserContext.Current, cart);
			RefreshCart();
		}

		private void HandleEmptyCart(ICartFull cart)
		{
			if (cart == null || cart.Id != 0) return;

			if (cart.GetCartItems().Count > 0)
			{
				cart.Id = _cartService.Save(UserContext.Current, cart);
			}
			else
			{
				IoC.Resolve<ICartSession>().Cart = null;
			}
		}

		protected virtual IProductView GetProduct(int productId)
		{
			return _productService.GetViewById(UserContext.Current, productId);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		private ICartFull GetCart()
		{
			var session = IoC.Resolve<ICartSession>();
			if (session.Cart == null)
			{
				session.Cart = CreateCart();
			}
			return session.Cart;
		}

		private ICartFull CreateCart()
		{
			return ((ILekmerCartService) _cartService).Create(UserContext.Current, Request.UserHostAddress);
		}

		private static void RefreshCart()
		{
			CartHelper.RefreshSessionCart();
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		private Guid GetWishListKey()
		{
			var key = GetWishListKeyFromQuery();
			if (key == Guid.Empty)
			{
				key = GetWishListKeyFromCookie();
			}
			return key;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		private Guid GetWishListKeyFromQuery()
		{
			var key = Request.QueryString["wishlist"];
			if (string.IsNullOrEmpty(key))
				return Guid.Empty;

			return new Guid(key);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		private Guid GetWishListKeyFromCookie()
		{
			return _wishListCookieService.GetWishListKey();
		}

		private string GetRedirectUrl()
		{
			string referrer = Request.QueryString[_referrerFieldName];
			return string.IsNullOrEmpty(referrer) ? "~/" : "~" + referrer;
		}
	}
}