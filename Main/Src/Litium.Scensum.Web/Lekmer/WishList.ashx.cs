﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.Web.Lekmer
{
	public class WishList : HandlerBase
	{
		private WishListHelper _helper = new WishListHelper();

		protected override void ProcessRequest()
		{
			var crawlerDetector = IoC.Resolve<ICrawlerDetectorService>();
			if (Request.Browser.Crawler || crawlerDetector.IsMatch(Request.UserAgent))
			{
				Response.Write(@"<!-- CD -->");
				Response.DisablePageCaching();
				return;
			}

			int? productId = GetProductId();
			if (productId.HasValue)
			{
				var wishListCookieService = IoC.Resolve<IWishListCookieService>();
				var wishListService = IoC.Resolve<IWishListService>();
				var wishListPackageItemService = IoC.Resolve<IWishListPackageItemService>();
				
				var packageItems = PrepareWishListPackageItems(productId);

				IWishList wishList = null;

				Guid wishListKey = wishListCookieService.GetWishListKey();
				if (wishListKey == Guid.Empty)
				{
					wishListKey = wishListCookieService.CreateNewWishListKey();
				}
				else
				{
					wishList = wishListService.GetByKey(UserContext.Current, wishListKey);
				}

				if (wishList == null)
				{
					wishList = IoC.Resolve<IWishListService>().Create();
					wishList.IpAddress = Request.UserHostAddress;
					wishList.Key = wishListKey;
				}
				
				var wishListItem = PrepareWishListItem(wishList.Id, productId.Value, packageItems);
				bool isItemAdded = wishList.AddItem(wishListItem);
				wishListService.Save(UserContext.Current, wishList);
				if (isItemAdded)
				{
					foreach (var packageItem in wishListItem.PackageItems)
					{
						packageItem.WishListItemId = wishListItem.Id;
						wishListPackageItemService.Save(packageItem);
					}
				}
				wishListCookieService.SetWishListKey(wishListKey);
			}

			string url = GetReferrer();
			if (url.IsNullOrTrimmedEmpty())
			{
				url = "~/";
			}

			if (AjaxHelper.IsXmlHttpRequest(Request))
			{
				Response.Write(AjaxHelper.RenderWishList());
			}
			else
			{
				Response.DisablePageCaching();
				Response.PermanentRedirect(url);
			}
		}

		protected string GetReferrer()
		{
			return Request.QueryString[_helper.ReferrerName];
		}
		protected int? GetProductId()
		{
			return Request.QueryString.GetInt32OrNull(_helper.ProductIdName);
		}
		protected int? GetSizeId()
		{
			return Request.QueryString.GetInt32OrNull(_helper.SizetIdName);
		}
		protected List<string> GetPackageProductIds()
		{
			var productIds = Request.QueryString[_helper.PackageProductIdsName];
			return !productIds.IsNullOrTrimmedEmpty()
				? productIds.Split(',').ToList()
				: new List<string>();
		}
		protected List<string> GetPackageProductSizeIds()
		{
			var sizeIds = Request.QueryString[_helper.PackageProductSizetIdsName];
			return !sizeIds.IsNullOrTrimmedEmpty()
				? sizeIds.Split(',').ToList()
				: new List<string>();
		}
		protected Collection<IWishListPackageItem> PrepareWishListPackageItems(int? productId)
		{
			var productService = (ILekmerProductService)IoC.Resolve<IProductService>();

			var packageItems = new Collection<IWishListPackageItem>();

			// Get package items for product.
			var packageProducts = productService.GetIdAllByPackageMasterProduct(UserContext.Current, productId.Value);
			if (packageProducts != null && packageProducts.Count > 0)
			{
				// Product ids and Size ids from response.
				var packageProductIds = GetPackageProductIds();
				var packageProductSizeIds = GetPackageProductSizeIds();

				foreach (var id in packageProducts)
				{
					int? sizeId = null;

					// Find package item in response result.
					var index = packageProductIds.IndexOf(id.ToString(CultureInfo.InvariantCulture));
					if (index >= 0)
					{
						sizeId = int.Parse(packageProductSizeIds[index], CultureInfo.InvariantCulture);

						// Remove items from response result
						packageProductIds.RemoveAt(index);
						packageProductSizeIds.RemoveAt(index);
					}

					var packageItem = IoC.Resolve<IWishListPackageItem>();
					packageItem.ProductId = id;
					packageItem.SizeId = sizeId;

					packageItems.Add(packageItem);
				}
			}
			return packageItems;
		}

		protected IWishListItem PrepareWishListItem(int wishListId, int productId, Collection<IWishListPackageItem> packageItems)
		{
			var wishListItem = IoC.Resolve<IWishListItemService>().Create();
			wishListItem.WishListId = wishListId;
			wishListItem.ProductId = productId;
			wishListItem.SizeId = GetSizeId();
			wishListItem.PackageItems = packageItems;
			return wishListItem;
		}
	}
}