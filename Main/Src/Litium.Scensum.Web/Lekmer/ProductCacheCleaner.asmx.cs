﻿using System.ComponentModel;
using System.Web.Services;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Scensum.Product.Cache;

namespace Litium.Scensum.Web.Lekmer
{
	/// <summary>
	/// Handles cleaning of the product cache.
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class ProductCacheCleaner : WebService
	{
		/// <summary>
		/// Flushes the product cache.
		/// </summary>
		[WebMethod]
		public void Flush()
		{
			ProductSizeCache.Instance.Flush();
			ProductSizeCollectionCache.Instance.Flush();

			ProductCache.Instance.Flush();
			ProductViewCache.Instance.Flush();

			FilterProductCollectionCache.Instance.Flush();
		}
	}
}