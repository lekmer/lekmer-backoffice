using System.Web.Routing;

namespace Litium.Scensum.Web.Lekmer
{
	public static class LekmerOldUrlRoutingRegistration
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.Add(new OldUrlRoute(@"{*url}", new RouteHandler("~/Lekmer/OldUrlRedirect.ashx")));
		}
	}
}