﻿using System.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Lekmer
{
	public class OldBrandRedirect : HandlerBase
	{
		protected override void ProcessRequest()
		{
			var brandId = Items["BrandId"] as int?;

			if (!brandId.HasValue)
			{
				PageNotFound();
				return;
			}

			var brandService = IoC.Resolve<IBrandService>();
			IBrand brand = brandService.GetById(UserContext.Current, brandId.Value);

			if (brand == null || !brand.ContentNodeId.HasValue)
			{
				PageNotFound();
				return;
			}

			var contentNodeService = IoC.Resolve<IContentNodeService>();
			IContentNodeTreeItem contentNodeTreeItem = contentNodeService.GetTreeItemById(UserContext.Current, brand.ContentNodeId.Value);

			if (contentNodeTreeItem == null || contentNodeTreeItem.Url.IsNullOrEmpty())
			{
				PageNotFound();
				return;
			}

			Response.DisablePageCaching();
			Response.PermanentRedirect(contentNodeTreeItem.Url);
		}

		private static void PageNotFound()
		{
			throw new HttpException(404, "Not found");
		}
	}
}