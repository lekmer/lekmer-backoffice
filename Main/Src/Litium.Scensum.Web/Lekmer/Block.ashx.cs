using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Scensum.Web.Lekmer
{
	public class Block : ContentPageHandler, ILekmerContentPageHandler
	{
		private AreaBlockPair? _areaBlockPair;
		private AreaBlockPair? _esalesDataSourceAreaBlockPair;

		private IBlockControl _blockControl;
		private IBlockControl _esalesDataSourceBlockControl;

		protected override IContentPageFull GetContentPageCore()
		{
			var contentPage = (IContentPage) Items["ContentPage"];

			var contentPageService = IoC.Resolve<IContentPageService>();
			IContentPageFull contentPageFull = contentPageService.GetFullById(UserContext.Current, contentPage.Id);

			return contentPageFull;
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
			return (IContentNodeTreeItem) Items["ContentNodeTreeItem"];
		}

		protected override Core.Web.Master CreateMaster()
		{
			return new EmptyMaster();
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			RenderPageNotFound();
		}

		protected override void AccessDeniedToSignedIn()
		{
			RenderPageNotFound();
		}

		protected override void ProcessRequest()
		{
			if (ContentPage == null || !ContentPage.ShowPage)
			{
				RenderPageNotFound();
				return;
			}

			bool isSignedIn = IoC.Resolve<ICustomerSession>().IsSignedIn;
			var access = (AccessInfo) ContentPage.AccessId;

			if (!isSignedIn && access == AccessInfo.SignedIn)
			{
				AccessDeniedToNotSignedIn();
				return;
			}

			if (isSignedIn && access == AccessInfo.NotSignedIn)
			{
				AccessDeniedToSignedIn();
				return;
			}

			var blockId = Items["BlockId"] as int?;
			if (!blockId.HasValue)
			{
				RenderPageNotFound();
				return;
			}

			FindAreaBlockPair(blockId.Value);
			if (!_areaBlockPair.HasValue)
			{
				RenderPageNotFound();
				return;
			}

			// Use a master to get variables such as [Channel.MediaUrl].
			var master = new EmptyMaster();

			master.DynamicProperties["ActiveContentNodeId"] = ActiveContentNodeId;
			AppendBreadcrumbItems(master.BreadcrumbItems);
			AppendMetaTags(master.Page.MetaTags);
			master.Page.Title = PageTitle();

			InstantiateBlockControls();

			if (_blockControl == null)
			{
				RenderPageNotFound();
				return;
			}

			master.Page.Content = RenderBlock().Body;

			Response.Write(master.Render());
		}

		private BlockContent RenderBlock()
		{
			HttpContext context = HttpContext.Current;

			var headBuilder = new StringBuilder();
			var bodyBuilder = new StringBuilder();
			var footerBuilder = new StringBuilder();

			string traceCategory = "Block Id=" + _blockControl.BlockId;
			if (context.Trace.IsEnabled)
			{
				context.Trace.Write(traceCategory, "Begin Render");
			}

			if (_esalesDataSourceBlockControl != null)
			{
				BlockContent esalesContent = _esalesDataSourceBlockControl.Render();

				if (!string.IsNullOrEmpty(esalesContent.Head)) headBuilder.AppendLine(esalesContent.Head);
				if (!string.IsNullOrEmpty(esalesContent.Body)) bodyBuilder.AppendLine(esalesContent.Body);
				if (!string.IsNullOrEmpty(esalesContent.Footer)) footerBuilder.AppendLine(esalesContent.Footer);
			}

			BlockContent content = _blockControl.Render();

			if (!string.IsNullOrEmpty(content.Head)) headBuilder.AppendLine(content.Head);
			if (!string.IsNullOrEmpty(content.Body)) bodyBuilder.AppendLine(content.Body);
			if (!string.IsNullOrEmpty(content.Footer)) footerBuilder.AppendLine(content.Footer);

			if (context.Trace.IsEnabled)
			{
				context.Trace.Write(traceCategory, "End Render");
			}

			return new BlockContent(headBuilder.ToString(), bodyBuilder.ToString(), footerBuilder.ToString());
		}

		private void InstantiateBlockControls()
		{
			if (_areaBlockPair.HasValue)
			{
				_blockControl = CreateBlockControlInstance(_areaBlockPair.Value.ContentArea, _areaBlockPair.Value.Block);
			}

			if (_esalesDataSourceAreaBlockPair.HasValue)
			{
				_esalesDataSourceBlockControl = CreateBlockControlInstance(_esalesDataSourceAreaBlockPair.Value.ContentArea, _esalesDataSourceAreaBlockPair.Value.Block);
			}
		}

		private IBlockControl CreateBlockControlInstance(IContentArea contentArea, IBlock block)
		{
			if (ShowBlockBasic(block) == false)
			{
				return null;
			}

			var blockControl = IoC.Resolve<IBlockControl>(block.BlockType.CommonName);
			blockControl.ContentPage = ContentPage;
			blockControl.ContentNodeTreeItem = ContentNodeTreeItem;
			blockControl.ContentArea = contentArea;
			blockControl.ContentPageHandler = this;
			blockControl.BlockId = block.Id;

			return blockControl;
		}

		private void FindAreaBlockPair(int blockId)
		{
			var blocks = new Collection<IBlock>();

			foreach (IContentAreaFull area in ContentPage.ContentAreas)
			{
				blocks.AddRange(area.Blocks);
			}

			IContentPageFull masterContentPage = null;
			if (ContentPage.MasterPageId.HasValue)
			{
				masterContentPage = IoC.Resolve<IContentPageService>().GetFullById(UserContext.Current, ContentPage.MasterPageId.Value);
				foreach (IContentAreaFull area in masterContentPage.ContentAreas)
				{
					blocks.AddRange(area.Blocks);
				}
			}

			foreach (IBlock block in blocks)
			{
				if (block.Id == blockId)
				{
					IContentAreaFull contentArea = ContentPage.ContentAreas.SingleOrDefault(a => a.Id == block.ContentAreaId);
					if (contentArea == default(IContentAreaFull) && masterContentPage != null)
					{
						contentArea = masterContentPage.ContentAreas.SingleOrDefault(a => a.Id == block.ContentAreaId);
					}

					_areaBlockPair = new AreaBlockPair(contentArea, block);

					break;
				}
			}

			foreach (IBlock block in blocks)
			{
				if (block.BlockType.CommonName == "EsalesDataSource")
				{
					IContentAreaFull contentArea = ContentPage.ContentAreas.SingleOrDefault(a => a.Id == block.ContentAreaId);
					if (contentArea == default(IContentAreaFull) && masterContentPage != null)
					{
						contentArea = masterContentPage.ContentAreas.SingleOrDefault(a => a.Id == block.ContentAreaId);
					}

					_esalesDataSourceAreaBlockPair = new AreaBlockPair(contentArea, block);

					break;
				}
			}
		}

		/// <summary>
		/// Finds specified type of blocks rised on the page.
		/// </summary>
		public virtual Collection<TBlockControl> FindBlockControls<TBlockControl>() where TBlockControl : class
		{
			var blockControls = new Collection<TBlockControl>();

			if (_blockControl is TBlockControl)
			{
				blockControls.Add(_blockControl.Cast<TBlockControl>());
			}

			return blockControls;
		}

		protected virtual bool ShowBlockBasic(IBlock block)
		{
			if ((BlockStatusInfo)block.BlockStatusId != BlockStatusInfo.Online)
			{
				return false;
			}

			var lekmerBlock = block as ILekmerBlock;
			if (lekmerBlock != null && !lekmerBlock.IsInTimeLimit)
			{
				return false;
			}

			return true;
		}
	}
}