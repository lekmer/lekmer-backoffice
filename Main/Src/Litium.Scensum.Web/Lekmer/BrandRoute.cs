﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Lekmer.OldUrl;

namespace Litium.Scensum.Web.Lekmer
{
	public class BrandRoute : Route
	{
		private static readonly Regex _blockRouteRegex1 = new Regex(@"([A-Za-z0-9\-]+)/([A-Za-z0-9\-]+)_([A-Za-z0-9\-]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		private static readonly Regex _blockRouteRegex2 = new Regex(@"([A-Za-z0-9\-]+)_([A-Za-z0-9\-]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		private static readonly Regex _blockRouteRegex3 = new Regex(@"([A-Za-z0-9\-]+)/([A-Za-z0-9\-]+)/([A-Za-z0-9\-]+)_([A-Za-z0-9\-]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		public BrandRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			var data = base.GetRouteData(httpContext);
			if (data == null)
			{
				return null;
			}
			string routeUrl = (data.Values["url"] ?? "").ToString();
			if (routeUrl.EndsWith("/", StringComparison.Ordinal))
			{
				routeUrl = routeUrl.Remove(routeUrl.Length - 1);
			}

			var title = string.Empty;
			;
			var blockRouteMatch1 = _blockRouteRegex1.Match(routeUrl);

			if (blockRouteMatch1.Success)
			{
				title = blockRouteMatch1.Groups[3].Value;
			}
			else
			{
				var blockRouteMatch2 = _blockRouteRegex2.Match(routeUrl);
				if (blockRouteMatch2.Success)
				{
					title = blockRouteMatch2.Groups[2].Value;
				}
				else
				{
					var blockRouteMatch3 = _blockRouteRegex3.Match(routeUrl);
					if (blockRouteMatch3.Success)
					{
						title = blockRouteMatch3.Groups[4].Value;
					}
					else
					{
						return null;
					}
				}
			}
			if (string.IsNullOrEmpty(title)) return null;

			var contentNodeService = IoC.Resolve<ILekmerContentNodeService>();
			var contentNodeTree = contentNodeService.GetAllAsTree(UserContext.Current);
			int index = routeUrl.IndexOf('_');
			var url = routeUrl.Remove(index);
			url += "/";
			var item = contentNodeTree.FindItemByUrl(url);
			if (item == null) return null;

			var brandSiteStructureService = IoC.Resolve<IBrandService>();
			var brandNode = contentNodeTree.FindItemByUrl(string.Format(CultureInfo.CurrentCulture, "{0}/", title));
			if (null == brandNode)
			{

				httpContext.Response.PermanentRedirect("~/" + item.PageUrl);
			}
			var brand = brandSiteStructureService.GetByNode(brandNode.ContentNode.Id);


			if (null == brand)
			{				
				return null;
			}

			var brandId = brand.BrandId;

			var contentPage = item.ContentNode as IContentPage;
			if (contentPage == null) return null;

			httpContext.Items["BrandId"] = brandId;
			httpContext.Items["ContentPage"] = contentPage;
			httpContext.Items["ContentNodeTreeItem"] = item;

			return data;
		}





	}
}