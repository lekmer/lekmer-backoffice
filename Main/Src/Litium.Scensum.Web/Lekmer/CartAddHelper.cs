﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Lekmer
{
	public class CartAddHelper
	{
		private const string _idFieldName = "id";
		private const string _trackingcodeFieldName = "trackingcode";
		private const string _quantityFieldName = "quantity";
		private const string _addedtobasketFieldName = "addedtobasket";
		private const string _nodeIdFieldName = "nodeId";
		private const string _referrerFieldName = "referrer";
		private const string _sizeIdFieldName = "sizeid";
		private const string _availSessionIdFieldName = "AvailSessionId";
		private const string _packageProductIdsFieldName = "package-product-ids";
		private const string _packageProductSizeIdsFieldName = "package-product-size-ids";

		private readonly NameValueCollection _query;

		public CartAddHelper(NameValueCollection query)
		{
			_query = query;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int? GetProductId()
		{
			int productId;
			if (!int.TryParse(_query[_idFieldName], out productId))
			{
				return null;
			}
			IoC.Resolve<IAvailSession>().AvailProductId = productId;
			SetTrackingCodeToAvail();
			return productId;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public string GetTrackingCode()
		{
			return _query[_trackingcodeFieldName];
		}

		public void SetTrackingCodeToAvail()
		{
			string trackingCode = GetTrackingCode();
			if (!string.IsNullOrEmpty(trackingCode))
			{
				IoC.Resolve<IAvailSession>().TrackingCode = trackingCode;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int GetQuantity()
		{
			int quantity;
			if (!int.TryParse(_query[_quantityFieldName], out quantity))
			{
				quantity = 1;
			}
			return quantity;
		}

		public string GetRedirectUrl(int productId)
		{
			string addedtobasket = _query[_addedtobasketFieldName];
			if (!string.IsNullOrEmpty(addedtobasket))
			{
				string contentNodeId = _query[_nodeIdFieldName];

				var node = IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, addedtobasket);
				return node == null
					? "~/"
					: UrlHelper.ResolveUrlHttp(node.Url + "?id=" + productId + "&nodeId=" + contentNodeId);
			}

			string referrer = _query[_referrerFieldName];
			return string.IsNullOrEmpty(referrer) ? "~/" : "~" + referrer;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int? GetSizeId()
		{
			int sizeId;
			if (!int.TryParse(_query[_sizeIdFieldName], out sizeId))
			{
				return null;
			}
			return sizeId;
		}

		public void GetAvailSessionId()
		{
			var availSessionId = _query[_availSessionIdFieldName];
			if (!string.IsNullOrEmpty(availSessionId))
			{
				IoC.Resolve<IAvailSession>().AvailSessionId = availSessionId;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public Collection<ICartItemPackageElement> GetCartItemPackageElements()
		{
			var cartItemPackageElements = new Collection<ICartItemPackageElement>();

			if (_query[_packageProductIdsFieldName] == null || _query[_packageProductSizeIdsFieldName] == null)
			{
				return cartItemPackageElements;
			}

			List<string> packageProductIds = _query[_packageProductIdsFieldName].Split(',').ToList();
			List<string> packageProductSizeIds = _query[_packageProductSizeIdsFieldName].Split(',').ToList();

			for (int i = 0; i < packageProductIds.Count; i++)
			{
				int productId;
				if (!int.TryParse(packageProductIds[i], out productId)) continue;

				int sizeId;
				if (!int.TryParse(packageProductSizeIds[i], out sizeId)) continue;

				var cartItemPackageElement = IoC.Resolve<ICartItemPackageElement>();
				cartItemPackageElement.ProductId = productId;
				cartItemPackageElement.SizeId = sizeId;

				cartItemPackageElements.Add(cartItemPackageElement);
			}

			return cartItemPackageElements;
		}
	}
}