using System;
using System.Web;
using System.Web.Routing;
using Litium.Scensum.Core.Web;

namespace Litium.Scensum.Web
{
	public class RootRoute : Route
	{
		public RootRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			if (base.GetRouteData(httpContext) == null) return null;

			bool shouldRedirectToEndingSlash = ShouldRedirectToEndingSlash(httpContext);
			if (shouldRedirectToEndingSlash)
			{
				httpContext.Response.PermanentRedirect("~/");
			}

			return null;
		}

		private static bool ShouldRedirectToEndingSlash(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			// Get full path and remove querystring.
			string path = httpContext.Request.RawUrl;
			int questionMarkPosition = path.IndexOf('?');
			if (questionMarkPosition > -1)
			{
				path = path.Substring(0, questionMarkPosition);
			}

			// Application is run in the root and not in a virtual folder.
			if (path.Equals("/")) return false;

			// Take path and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath;
			string relativePath = path.Substring(appPath.Length);

			// End slash is missing so do redirect.
			return relativePath.Length == 0;
		}
	}
}