﻿using System;
using System.Web;

namespace Litium.Scensum.Web
{
	public static class ResponseExtension
	{
		public static void DisablePageCaching(this HttpResponseBase response)
		{
			response.Cache.SetCacheability(HttpCacheability.NoCache);
			response.Cache.SetNoServerCaching();
			response.Cache.SetNoStore();
			response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
			response.Expires = -1440;
			response.CacheControl = "no-cache";
		}

		public static void DisablePageCaching(this HttpResponse response)
		{
			response.Cache.SetCacheability(HttpCacheability.NoCache);
			response.Cache.SetNoServerCaching();
			response.Cache.SetNoStore();
			response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
			response.Expires = -1440;
			response.CacheControl = "no-cache";
		}
	}
}
