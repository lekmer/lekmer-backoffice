using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Media
{
	public static class MediaRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");
			routes.Add(new Route("media/{*pathInfo}", new StopRoutingHandler()));
		}
	}
}