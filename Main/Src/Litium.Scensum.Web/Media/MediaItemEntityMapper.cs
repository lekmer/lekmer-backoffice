using System.Globalization;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Media
{
	public class MediaItemEntityMapper : EntityMapper<IMediaItem>
	{
		public override void AddEntityVariables(Fragment fragment, IMediaItem item)
		{
			fragment.AddVariable("MediaItem.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("MediaItem.Title", item.Title);

			string mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(Channel.Current, item);
			IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatService>().GetById(item.MediaFormatId);

			fragment.AddVariable("MediaItem.Url", MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, item, mediaFormat));
		}
	}
}