using System;
using System.Globalization;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web
{
	public class ChannelEntityMapper : Core.Web.ChannelEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IChannel item)
		{
			base.AddEntityVariables(fragment, item);

			if (fragment == null)
			{
				throw new ArgumentNullException("fragment");
			}

			var mediaUrlService = IoC.Resolve<IMediaUrlService>();

			fragment.AddVariable("Channel.MediaUrl", mediaUrlService.ResolveStaticMediaUrl(item), VariableEncoding.None);
			fragment.AddVariable("Channel.MediaArchiveUrl", mediaUrlService.ResolveMediaArchiveUrl(item), VariableEncoding.None);

			AddDateTimeVariables(fragment, item);
		}

		protected virtual void AddDateTimeVariables(Fragment fragment, IChannel item)
		{
			if (fragment == null)
			{
				throw new ArgumentNullException("fragment");
			}
			
			var lekmerChannel = item as ILekmerChannel;
			if (lekmerChannel == null)
			{
				throw new ArgumentNullException("item");
			}

			IChannelFormatInfo formatInfo = lekmerChannel.FormatInfo;
			if (formatInfo == null)
			{
				return;
			}

			DateTime serverUtcDateTime = DateTime.UtcNow;
			DateTime channelDateTime = serverUtcDateTime.AddHours(formatInfo.TimeZoneDiff);

			string channelTimeValue = channelDateTime.ToString(formatInfo.TimeFormat, CultureInfo.CurrentCulture);
			string channelWeekDayValue = channelDateTime.ToString(formatInfo.WeekDayFormat, CultureInfo.CurrentCulture);
			string channelDayValue = channelDateTime.ToString(formatInfo.DayFormat, CultureInfo.CurrentCulture);
			string channelDateTimeValue = channelDateTime.ToString(formatInfo.DateTimeFormat, CultureInfo.CurrentCulture);

			fragment.AddVariable("Channel.Time", channelTimeValue, VariableEncoding.None);
			fragment.AddVariable("Channel.WeekDay", channelWeekDayValue, VariableEncoding.None);
			fragment.AddVariable("Channel.Day", channelDayValue, VariableEncoding.None);
			fragment.AddVariable("Channel.DateTime", channelDateTimeValue, VariableEncoding.None);
		}
	}
}