using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Customer
{
	public class SignOut : SignOutHandler
	{
		[SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
		protected override void Redirect()
		{
			var contentNodeService = IoC.Resolve<IContentNodeService>();

            IContentNodeTreeItem signInTreeItem = contentNodeService.GetTreeItemByCommonName(UserContext.Current, "SignIn");

			if (signInTreeItem == null)
			{
				throw new ApplicationException(
					"Failed to redirect to sign in page. No node with common name 'SignIn' was found.");
			}

			if (string.IsNullOrEmpty(signInTreeItem.Url))
			{
				throw new ApplicationException(
					"Failed to redirect to sign in page. Node with common name 'SignIn' didn't have a URL.");
			}

			Response.Redirect(signInTreeItem.Url);
		}
	}
}