using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Order
{
	public static class CustomerRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");

			routes.Add(new Route(@"signout", new RouteHandler("~/Customer/SignOut.ashx")));
		}
	}
}