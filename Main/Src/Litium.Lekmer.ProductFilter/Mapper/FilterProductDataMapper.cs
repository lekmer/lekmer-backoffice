using System;
using System.Data;
using Litium.Framework.DataMapper;

namespace Litium.Lekmer.ProductFilter.Mapper
{
	public class FilterProductDataMapper : DataMapperBase<FilterProduct>
	{
		public FilterProductDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override FilterProduct Create()
		{
			var filterProduct = new FilterProduct
			{
				Id = MapValue<int>("FilterProduct.ProductId"),
				BrandId = MapNullableValue<int>("FilterProduct.BrandId"),
				AgeFrom = MapValue<int>("FilterProduct.AgeFromMonth"),
				AgeTo = MapValue<int>("FilterProduct.AgeToMonth"),
				CategoryId = MapValue<int>("FilterProduct.CategoryId"),
				Price = MapValue<decimal>("Price.PriceIncludingVat"),
				Popularity = MapNullableValue<int?>("FilterProduct.Popularity"),
				IsNewFrom = MapNullableValue<DateTime?>("FilterProduct.IsNewFrom"),
				NumberInStock = MapValue<int>("FilterProduct.NumberInStock"),
				ProductTypeId = MapValue<int>("FilterProduct.ProductTypeId")
			};

			decimal? discountPrice = MapNullableValue<decimal?>("Price.DiscountPriceIncludingVat");

			filterProduct.DiscountPrice = discountPrice ?? filterProduct.Price;

			return filterProduct;
		}
	}
}