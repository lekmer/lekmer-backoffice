using System;
using Litium.Framework.Setting;

namespace Litium.Lekmer.ProductFilter.Setting
{
	public sealed class ProductFilterCacheSetting : SettingBase
	{
		//Singleton

		private static readonly ProductFilterCacheSetting _instance = new ProductFilterCacheSetting();

		private ProductFilterCacheSetting()
		{
		}

		public static ProductFilterCacheSetting Instance
		{
			get { return _instance; }
		}

		//End of Singleton

		private const string _groupName = "ProductFilterCache";

		protected override string StorageName
		{
			get { return "LekmerCacheManagement"; }
		}

		public TimeSpan ProductFilterCacheRefreshInterval
		{
			get { return TimeSpan.FromSeconds(GetInt32(_groupName, "CacheRenewRefreshIntervalInSeconds", 60)); }
		}
	}
}