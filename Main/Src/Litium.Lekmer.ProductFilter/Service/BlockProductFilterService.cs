using System;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public class BlockProductFilterService : IBlockProductFilterService
	{
		protected BlockProductFilterRepository Repository { get; private set; }
		protected IBlockRatingService BlockRatingService { get; private set; }
		protected IProductTypeService ProductTypeService { get; private set; }

		public BlockProductFilterService(
			BlockProductFilterRepository repository, 
			IBlockRatingService blockRatingService,
			IProductTypeService productTypeService)
		{
			Repository = repository;
			BlockRatingService = blockRatingService;
			ProductTypeService = productTypeService;
		}

		public virtual IBlockProductFilter GetById(IUserContext context, int blockId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return BlockProductFilterCache.Instance.TryGetItem(
				new BlockProductFilterKey(context.Channel.Id, blockId),
				() => GetByIdCore(context.Channel, blockId));
		}

		protected virtual IBlockProductFilter GetByIdCore(IChannel channel, int blockId)
		{
			var blockProductFilter = Repository.GetById(channel, blockId);

			if (blockProductFilter != null)
			{
				blockProductFilter.DefaultBrandIds = Repository.GetAllBrandsByBlock(blockId);
				blockProductFilter.DefaultTagIds = Repository.GetAllTagsByBlock(blockId);
				blockProductFilter.TargetProductTypeIds = ProductTypeService.GetIdAllByBlock(blockId);
				
				var ratings = BlockRatingService.GetAllByBlock(blockId);
				if (ratings != null && ratings.Count > 0)
				{
					blockProductFilter.BlockRating = ratings[0];
				}
			}

			return blockProductFilter;
		}
	}
}