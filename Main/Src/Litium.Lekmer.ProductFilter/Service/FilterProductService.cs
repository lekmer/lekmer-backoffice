using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterProductService : IFilterProductService
	{
		protected FilterProductRepository Repository { get; private set; }

		private readonly IProductService _productService;
		private readonly ICategoryService _categoryService;
		private readonly IProductTagService _productTagService;
		private readonly IProductSizeService _productSizeService;
		private readonly IFilterProcessor _filterProcessor;

		public FilterProductService(
			FilterProductRepository filterProductRepository,
			IProductService productService,
			ICategoryService categoryService,
			IProductTagService productTagService,
			IProductSizeService productSizeService,
			IFilterProcessor filterProcessor)
		{
			Repository = filterProductRepository;
			_productService = productService;
			_categoryService = categoryService;
			_productTagService = productTagService;
			_productSizeService = productSizeService;
			_filterProcessor = filterProcessor;
		}


		public virtual Collection<FilterProduct> GetAll(IUserContext context)
		{
			return FilterProductStickyCache.GetFilterProducts(context);
		}

		public virtual Collection<FilterProduct> GetAllOnline(IUserContext context, ProductIdCollection productIds)
		{
			var filterProducts = new Collection<FilterProduct>();

			if (productIds.Count == 0)
			{
				return filterProducts;
			}

			var onlineProducts = GetAll(context).ToDictionary(p => p.Id);

			foreach (int productId in productIds)
			{
				if (onlineProducts.ContainsKey(productId))
				{
					filterProducts.Add(onlineProducts[productId]);
				}
			}

			return filterProducts;
		}

		public virtual Collection<FilterProduct> GetAllNoCache(IUserContext context)
		{
			var filterProducts = Repository.GetAll(context.Channel.Id, context.Customer);

			CalculateDiscountInfo(context, filterProducts);
			CompleteProducts(context, filterProducts);

			return filterProducts;
		}

		public virtual ProductCollection GetAllByQuery(IUserContext context, FilterQuery query, int pageNumber, int pageSize, out FilteredProductRangeStatistics statistics)
		{
			var productIdCollection = GetProductIdCollectionByQuery(context, query, pageNumber, pageSize, out statistics);

			// Populate full IProducts.
			ProductCollection products = _productService.PopulateProducts(context, productIdCollection);

			return products;
		}

		public virtual ProductCollection GetAllViewByQuery(IUserContext context, FilterQuery query, int pageNumber, int pageSize, out FilteredProductRangeStatistics statistics)
		{
			var productIdCollection = GetProductIdCollectionByQuery(context, query, pageNumber, pageSize, out statistics);

			// Populate full IProducts.
			ProductCollection products = ((ILekmerProductService)_productService).PopulateViewProducts(context, productIdCollection);

			return products;
		}


		/// <summary>
		/// CompleteProducts
		/// </summary>
		/// <param name="context"></param>
		/// <param name="products"></param>
		/// <remarks>Rewritten by Dino Miralem, CDON</remarks>
		protected virtual void CompleteProducts(IUserContext context, Collection<FilterProduct> products)
		{
			if (products == null)
			{
				return;
			}

			var categoryTree = _categoryService.GetAllAsTree(context);
			var productTags = _productTagService.GetAll();
			var productSizes = _productSizeService.GetAll();

			var productTagsHash = new Dictionary<int, List<IProductTag>>(products.Count);
			var productSizesHash = new Dictionary<int, List<IProductSizeRelation>>(products.Count);

			foreach (IProductTag productTag in productTags)
			{
				if (!productTagsHash.ContainsKey(productTag.ProductId))
				{
					productTagsHash[productTag.ProductId] = new List<IProductTag>();
				}

				productTagsHash[productTag.ProductId].Add(productTag);
			}

			foreach (IProductSizeRelation productSizeRelation in productSizes)
			{
				if (!productSizesHash.ContainsKey(productSizeRelation.ProductId))
				{
					productSizesHash[productSizeRelation.ProductId] = new List<IProductSizeRelation>();
				}

				productSizesHash[productSizeRelation.ProductId].Add(productSizeRelation);
			}

			foreach (FilterProduct product in products)
			{
				product.AddCategory(product.CategoryId);
				var treeItem = categoryTree.FindItemById(product.CategoryId);

				foreach (ICategoryTreeItem parentTreeItem in treeItem.GetAncestors())
				{
					if (parentTreeItem.Parent == null) break;
					product.AddCategory(parentTreeItem.Id);
				}

				int productId = product.Id;
				if (productTagsHash.ContainsKey(productId))
				{
					var currentProductTags = productTagsHash[productId];
					foreach (IProductTag productTag in currentProductTags)
					{
						product.AddTag(productTag.TagId);
					}
				}

				if (productSizesHash.ContainsKey(productId))
				{
					var currentProductSizes = productSizesHash[productId];
					foreach (IProductSizeRelation productSize in currentProductSizes)
					{
						product.AddSize(productSize.SizeId);
					}
				}
			}
		}

		protected virtual void CalculateDiscountInfo(IUserContext context, Collection<FilterProduct> products)
		{
			if (products == null)
			{
				return;
			}

			// Set discount amount and %
			foreach (FilterProduct product in products)
			{
				decimal discountAmount = product.Price - product.DiscountPrice;
				product.DiscountPercent = discountAmount / product.Price;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "4#")]
		protected virtual ProductIdCollection GetProductIdCollectionByQuery(IUserContext context, FilterQuery query, int pageNumber, int pageSize, out FilteredProductRangeStatistics statistics)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (query == null)
			{
				throw new ArgumentNullException("query");
			}

			if (pageNumber < 1)
			{
				throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "Page number can't be less then 0.");
			}

			var allProducts = GetAll(context);

			var result = _filterProcessor.Process(context, allProducts, query);

			statistics = result.Statistics;

			// Paging.
			int totalCount = result.Products.Count();
			var productIds = result.Products
				.Skip((pageNumber - 1) * pageSize)
				.Take(pageSize)
				.Select(product => product.Id);

			return new ProductIdCollection(productIds) { TotalCount = totalCount };
		}
	}
}