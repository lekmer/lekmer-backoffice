﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public interface IBlockProductFilterSecureService
	{
		IBlockProductFilter Create();
		IBlockProductFilter GetByIdSecure(int blockId);
		void Delete(ISystemUserFull systemUserFull, int blockId);
		void DeleteTag(ISystemUserFull systemUserFull, int tagId);
		int Save(ISystemUserFull systemUserFull, IBlockProductFilter blockProductFilter);
	}
}
