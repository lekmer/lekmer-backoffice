using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public interface IFilterProcessor
	{
		FilterResult Process(IUserContext context, IEnumerable<FilterProduct> products, FilterQuery query);
	}
}