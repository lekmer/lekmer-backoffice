using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter
{
	public class FilteredProductRangeStatistics
	{
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IPriceIntervalService _priceIntervalService;

		private Collection<IAgeInterval> _ageIntervals;
		private Collection<IPriceInterval> _priceIntervals;

		private readonly Dictionary<int, int> _categoryCount = new Dictionary<int, int>();
		private readonly Dictionary<int, int> _brandCount = new Dictionary<int, int>();
		private readonly Dictionary<int, int> _tagCount = new Dictionary<int, int>();
		private readonly Dictionary<int, int> _sizeCount = new Dictionary<int, int>();

		public FilteredProductRangeStatistics()
		{
			_ageIntervalService = IoC.Resolve<IAgeIntervalService>();
			_priceIntervalService = IoC.Resolve<IPriceIntervalService>();
		}

		public Dictionary<int, int> CategoryCount
		{
			get { return _categoryCount; }
		}

		public Dictionary<int, int> BrandCount
		{
			get { return _brandCount; }
		}

		public IAgeIntervalBand AgeInterval { get; private set; }

		public IPriceIntervalBand PriceInterval { get; private set; }

		public Dictionary<int, int> TagCount
		{
			get { return _tagCount; }
		}

		public Dictionary<int, int> SizeCount
		{
			get { return _sizeCount; }
		}


		public void LoadAgeIntervals(Collection<IAgeInterval> ageIntervals)
		{
			_ageIntervals = ageIntervals;
		}

		public void LoadPriceIntervals(Collection<IPriceInterval> priceIntervals)
		{
			_priceIntervals = priceIntervals;
		}

		public void LoadProducts(IEnumerable<FilterProduct> products)
		{
			foreach (FilterProduct product in products)
			{
				LoadProduct(product);
			}

			LoadFilteredAgeInterval(products);
			LoadFilteredPriceInterval(products);
		}


		private void LoadProduct(FilterProduct product)
		{
			if (product.BrandId.HasValue)
			{
				IncrementItem(_brandCount, product.BrandId.Value);
			}

			foreach (KeyValuePair<int, int> categoryId in product.ParentCategoryIds)
			{
				IncrementItem(_categoryCount, categoryId.Key);
			}

			foreach (KeyValuePair<int, int> tag in product.Tags)
			{
				IncrementItem(_tagCount, tag.Key);
			}

			foreach (KeyValuePair<int, int> size in product.Sizes)
			{
				IncrementItem(_sizeCount, size.Key);
			}
		}

		private void LoadFilteredAgeInterval(IEnumerable<FilterProduct> products)
		{
			if (products == null || products.Count() == 0)
			{
				return;
			}

			int ageFromMin = products.Min(p => p.AgeFrom);
			int ageToMax = products.Max(p => p.AgeTo);

			if (_ageIntervals != null && _ageIntervals.Count > 0)
			{
				// Find 'From' border
				int? fromMonth = null;
				var intervalsFrom = _ageIntervals.Where(ai => ai.FromMonth <= ageFromMin).ToList();
				if (intervalsFrom.Count() > 0)
				{
					fromMonth = intervalsFrom.Max(ai => ai.FromMonth);
				}

				// Find 'To' border
				int? toMonth = null;
				var intervalsTo = _ageIntervals.Where(ai => ai.ToMonth >= ageToMax).ToList();
				if (intervalsTo.Count() > 0)
				{
					toMonth = intervalsTo.Min(ai => ai.ToMonth);
				}

				AgeInterval = _ageIntervalService.GetAgeInterval(fromMonth, toMonth);
			}
		}

		private void LoadFilteredPriceInterval(IEnumerable<FilterProduct> products)
		{
			if (products == null || products.Count() == 0)
			{
				return;
			}

			decimal priceMin = products.Min(p => p.DiscountPrice);
			decimal priceMax = products.Max(p => p.DiscountPrice);

			if (_priceIntervals != null && _priceIntervals.Count > 0)
			{
				// Find 'From' border
				decimal? fromPrice = null;
				var intervalsFrom = _priceIntervals.Where(pi => pi.From <= priceMin).ToList();
				if (intervalsFrom.Count() > 0)
				{
					fromPrice = intervalsFrom.Max(pi => pi.From);
				}

				// Find 'To' border
				decimal? toPrice = null;
				var intervalsTo = _priceIntervals.Where(pi => pi.To >= priceMax).ToList();
				if (intervalsTo.Count() > 0)
				{
					toPrice = intervalsTo.Min(pi => pi.To);
				}

				PriceInterval = _priceIntervalService.GetPriceInterval(fromPrice, toPrice);
			}
		}

		private static void IncrementItem<TKey>(IDictionary<TKey, int> itemCount, TKey key)
		{
			int count;
			if (itemCount.TryGetValue(key, out count))
			{
				itemCount[key] = count + 1;
			}
			else
			{
				itemCount.Add(key, 1);
			}
		}
	}
}