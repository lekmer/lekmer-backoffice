﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerSecureService : CustomerSecureService, ILekmerCustomerSecureService
	{
		protected LekmerCustomerRepository LekmerRepository { get; private set; }

		public LekmerCustomerSecureService(
			IAccessValidator accessValidator,
			CustomerRepository repository,
			ICustomerInformationSecureService customerInformationSecureService,
			IAddressSecureService addressSecureService,
			ICustomerCommentSecureService customerCommentSecureService,
			IUserSecureService userSecureService)
			: base(accessValidator, repository, customerInformationSecureService, addressSecureService, customerCommentSecureService, userSecureService)
		{
			LekmerRepository = (LekmerCustomerRepository) repository;
		}

		public void Delete(ISystemUserFull systemUserFull, int customerId)
		{
			AddressSecureService.EnsureNotNull();
			CustomerInformationSecureService.EnsureNotNull();
			LekmerRepository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			((LekmerCustomerInformationSecureService) CustomerInformationSecureService).DeleteByCustomer(systemUserFull, customerId);
			((LekmerAddressSecureService) AddressSecureService).DeleteByCustomer(systemUserFull, customerId);
			LekmerRepository.Delete(customerId);
		}
	}
}