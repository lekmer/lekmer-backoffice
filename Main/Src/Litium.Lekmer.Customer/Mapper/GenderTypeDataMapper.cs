﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Mapper
{
	public class GenderTypeDataMapper : DataMapperBase<IGenderType>
	{
		private int _customerStatusId;
		private int _erpId;
		private int _commonName;
		private int _title;

		public GenderTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_customerStatusId = OrdinalOf("GenderType.GenderTypeId");
			_erpId = OrdinalOf("GenderType.ErpId");
			_commonName = OrdinalOf("GenderType.CommonName");
			_title = OrdinalOf("GenderType.Title");
		}

		protected override IGenderType Create()
		{
			var genderType = IoC.Resolve<IGenderType>();

			genderType.Id = MapValue<int>(_customerStatusId);
			genderType.ErpId = MapValue<string>(_erpId);
			genderType.CommonName = MapValue<string>(_commonName);
			genderType.Title = MapValue<string>(_title);

			genderType.Status = BusinessObjectStatus.Untouched;

			return genderType;
		}
	}
}