﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Mapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Mapper
{
	public class LekmerCustomerDataMapper : CustomerDataMapper
	{
		private DataMapperBase<IFacebookUser> _facebookUserDataMapper;

		public LekmerCustomerDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_facebookUserDataMapper = DataMapperResolver.Resolve<IFacebookUser>(DataReader);
		}

		protected override ICustomer Create()
		{
			var customer = base.Create();
			var lekmerCustomer = customer as ILekmerCustomer;

			if (lekmerCustomer != null)
			{
				var facebookUserCustomerId = MapNullableValue<int?>("FacebookCustomerUser.CustomerId");
				if (facebookUserCustomerId.HasValue)
				{
					lekmerCustomer.FacebookUser = _facebookUserDataMapper.MapRow();
				}
			}

			customer.SetUntouched();
			return customer;
		}
	}
}
