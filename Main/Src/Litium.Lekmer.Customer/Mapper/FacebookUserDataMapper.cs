﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class FacebookUserDataMapper : DataMapperBase<IFacebookUser>
	{
		private int _customerId;
		private int _customerRegistryId;
		private int _facebookId;
		private int _name;
		private int _email;
		private int _createdDate;

		public FacebookUserDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_customerId = OrdinalOf("FacebookCustomerUser.CustomerId");
			_customerRegistryId = OrdinalOf("FacebookCustomerUser.CustomerRegistryId");
			_facebookId = OrdinalOf("FacebookCustomerUser.FacebookId");
			_name = OrdinalOf("FacebookCustomerUser.Name");
			_email = OrdinalOf("FacebookCustomerUser.Email");
			_createdDate = OrdinalOf("FacebookCustomerUser.CreatedDate");
		}
		protected override IFacebookUser Create()
		{
			var user = IoC.Resolve<IFacebookUser>();
			user.CustomerId = MapValue<int>(_customerId);
			user.CustomerRegistryId = MapValue<int>(_customerRegistryId);
			user.FacebookId = MapValue<string>(_facebookId);
			user.Name = MapValue<string>(_name);
			user.Email = MapNullableValue<string>(_email);
			user.CreatedDate = MapValue<DateTime>(_createdDate);
			user.Status = BusinessObjectStatus.Untouched;
			return user;
		}
	}
}
