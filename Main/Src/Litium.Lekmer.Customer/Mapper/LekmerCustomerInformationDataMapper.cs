﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Common;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Mapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Mapper
{
	public class LekmerCustomerInformationDataMapper : CustomerInformationDataMapper
	{
		private int _genderTypeId;
		private DataMapperBase<ILekmerAlternateAddress> _alternateAddressDataMapper;

		public LekmerCustomerInformationDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_genderTypeId = OrdinalOf("CustomerInformation.GenderTypeId");
			_alternateAddressDataMapper = DataMapperResolver.Resolve<ILekmerAlternateAddress>(DataReader);
		}

		protected override ICustomerInformation Create()
		{
			var customerInformation = base.Create();

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;

			if (lekmerCustomerInformation != null)
			{
				lekmerCustomerInformation.GenderTypeId = MapNullableValue<int>(_genderTypeId);

				if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.None)
				{
					lekmerCustomerInformation.GenderTypeId = (int)GenderTypeInfo.Unknown;
				}

				lekmerCustomerInformation.Status = BusinessObjectStatus.Untouched;

				lekmerCustomerInformation.IsCompany = MapNullableValue<bool?>("CustomerInformation.IsCompany") ?? false;
				int? alternateAddressId = MapNullableValue<int?>("CustomerInformation.AlternateAddressId");
				if (alternateAddressId.HasValue)
				{
					lekmerCustomerInformation.AlternateAddressId = alternateAddressId;
					lekmerCustomerInformation.AlternateAddress = _alternateAddressDataMapper.MapRow();
				}
			}

			customerInformation.Status = BusinessObjectStatus.Untouched;

			return customerInformation;
		}
	}
}
