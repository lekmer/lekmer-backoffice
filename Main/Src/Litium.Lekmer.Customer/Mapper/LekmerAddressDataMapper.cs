﻿using System.Data;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Mapper;

namespace Litium.Lekmer.Customer.Mapper
{
	public class LekmerAddressDataMapper<T> : AddressDataMapper<T>
		where T : class, IAddress
	{
		private int _houseNumber;
		private int _houseExtension;
		private int _reference;
		private int _doorCode;

		public LekmerAddressDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_houseNumber = OrdinalOf(Prefix + "Address.HouseNumber");
			_houseExtension = OrdinalOf(Prefix + "Address.HouseExtension");
			_reference = OrdinalOf(Prefix + "Address.Reference");
			_doorCode = OrdinalOf(Prefix + "Address.DoorCode");
		}

		protected override T Create()
		{
			var address = base.Create();
			var lekmerAddress = address as ILekmerAddress;

			if (lekmerAddress != null)
			{
				lekmerAddress.HouseNumber = MapNullableValue<string>(_houseNumber);
				lekmerAddress.HouseExtension = MapNullableValue<string>(_houseExtension);
				lekmerAddress.Reference = MapNullableValue<string>(_reference);
				lekmerAddress.DoorCode = MapNullableValue<string>(_doorCode);

				lekmerAddress.SetUntouched();
			}

			return address;
		}
	}
}
