﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class FacebookUserRepository
	{
		public virtual int Save(IFacebookUser user)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", user.CustomerId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerRegistryId", user.CustomerRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FacebookId", user.FacebookId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Name", user.Name, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Email", user.Email, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CreatedDate", user.CreatedDate, SqlDbType.DateTime)
				};
			var dbSettings = new DatabaseSetting("FacebookUserRepository.Save");
			user.CustomerId = new DataHandler().ExecuteCommandWithReturnValue("[customerlek].[pFacebookUserSave]", parameters, dbSettings);
			return user.CustomerId;
		}
	}
}
