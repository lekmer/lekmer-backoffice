﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class GenderTypeRepository
	{
		protected virtual DataMapperBase<IGenderType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IGenderType>(dataReader);
		}

		/// <summary>
		/// Gets GenderTypes
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IGenderType> GetAll()
		{
			var dbSettings = new DatabaseSetting("GenderTypeRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[customerlek].[pGenderTypeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}