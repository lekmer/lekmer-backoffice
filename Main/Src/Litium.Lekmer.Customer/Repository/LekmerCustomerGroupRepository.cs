﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class LekmerCustomerGroupRepository : CustomerGroupRepository
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "dataReader"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<ICustomerGroup> GetAll()
		{
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerCustomerGroupRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[customerlek].[pCustomerGroupGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}