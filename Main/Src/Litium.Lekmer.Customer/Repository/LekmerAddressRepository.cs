﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class LekmerAddressRepository : AddressRepository
	{
		public override int Save(IAddress address)
		{
			var lekmerAddress = (ILekmerAddress)address;

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("AddressId", address.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("CustomerId", address.CustomerId, SqlDbType.Int),
				ParameterHelper.CreateParameter("Addressee", address.Addressee, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("StreetAddress", address.StreetAddress, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("StreetAddress2", address.StreetAddress2, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("PostalCode", address.PostalCode, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("City", address.City, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("CountryId", address.CountryId, SqlDbType.Int),
				ParameterHelper.CreateParameter("PhoneNumber", address.PhoneNumber, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("AddressTypeId", address.AddressTypeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("HouseNumber", lekmerAddress.HouseNumber, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("HouseExtension", lekmerAddress.HouseExtension, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("Reference", lekmerAddress.Reference, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("DoorCode", lekmerAddress.DoorCode, SqlDbType.NVarChar)
			};

			var dbSettings = new DatabaseSetting("LekmerAddressRepository.Save");

			address.Id = new DataHandler().ExecuteCommandWithReturnValue("[customerlek].[pAddressSave]", parameters, dbSettings);

			return address.Id;
		}

		public override void Delete(int addressId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("AddressId", addressId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerAddressRepository.Delete");

			new DataHandler().ExecuteCommand("[customerlek].[pAddressDelete]", parameters, dbSettings);
		}

		public virtual void DeleteByCustomer(int customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerAddressRepository.DeleteByCustomer");
			new DataHandler().ExecuteCommand("[customerlek].[pAddressDeleteByCustomer]", parameters, dbSettings);
		}
	}
}