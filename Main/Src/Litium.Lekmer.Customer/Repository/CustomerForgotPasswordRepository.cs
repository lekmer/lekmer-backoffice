﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class CustomerForgotPasswordRepository
	{
		protected DataMapperBase<ICustomerForgotPassword> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICustomerForgotPassword>(dataReader);
		}

		public ICustomerForgotPassword GetByGuid(Guid id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Guid", id, SqlDbType.UniqueIdentifier)
				};
			var dbSettings = new DatabaseSetting("CustomerForgotPasswordRepository.GetByGuid");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCustomerForgotPasswordGetByGuid]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public void Save(ICustomerForgotPassword customerForgotPassword)
		{
			if (customerForgotPassword == null) throw new ArgumentNullException("customerForgotPassword");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerForgotPassword.CustomerId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Guid", customerForgotPassword.Guid, SqlDbType.UniqueIdentifier),
					ParameterHelper.CreateParameter("CreatedDate", customerForgotPassword.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("ValidToDate", customerForgotPassword.ValidToDate, SqlDbType.DateTime)
				};
			var dbSettings = new DatabaseSetting("CustomerForgotPasswordRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pCustomerForgotPasswordSave]", parameters, dbSettings);
		}

		public void Delete(int customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CustomerForgotPasswordRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pCustomerForgotPasswordDelete]", parameters, dbSettings);
		}
	}
}
