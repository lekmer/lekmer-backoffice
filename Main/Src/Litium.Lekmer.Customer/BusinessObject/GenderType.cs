﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class GenderType : BusinessObjectBase, IGenderType
	{
		private int _id;
		private string _erpId;
		private string _commonName;
		private string _title;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string ErpId
		{
			get { return _erpId; }
			set
			{
				CheckChanged(_erpId, value);
				_erpId = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
	}
}