﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class CustomerForgotPassword : BusinessObjectBase, ICustomerForgotPassword
	{
		private int _customerId;
		private Guid _guid;
		private DateTime _createdDate;
		private DateTime? _validToDate;

		public int CustomerId
		{
			get
			{
				return _customerId;
			}
			set 
			{ 
				CheckChanged(_customerId, value);
				_customerId = value;
			}
		}

		public Guid Guid
		{
			get
			{
				return _guid;
			}
			set
			{
				CheckChanged(_guid, value);
				_guid = value;
			}
		}

		public DateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public DateTime? ValidToDate
		{
			get
			{
				return _validToDate;
			}
			set
			{
				CheckChanged(_validToDate, value);
				_validToDate = value;
			}
		}
	}
}
