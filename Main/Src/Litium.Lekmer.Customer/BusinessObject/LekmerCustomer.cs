﻿using System;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class LekmerCustomer : Litium.Scensum.Customer.Customer, ILekmerCustomer
	{
		private IFacebookUser _facebookUser;
		public IFacebookUser FacebookUser
		{
			get { return _facebookUser; }
			set
			{
				CheckChanged(_facebookUser, value);
				_facebookUser = value;
			}
		}
	}
}