﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerGroupService : ILekmerCustomerGroupService
	{
		protected CustomerGroupRepository Repository { get; private set; }

		public LekmerCustomerGroupService(CustomerGroupRepository customerGroupRepository)
		{
			Repository = customerGroupRepository;
		}

		public virtual Collection<ICustomerGroup> GetAllByCustomer(int customerId)
		{
			return CustomerGroupCollectionCache.Instance.TryGetItem(
				new CustomerGroupCollectionCacheKey(customerId),
				() => GetAllByCustomerCore(customerId));
		}

		protected virtual Collection<ICustomerGroup> GetAllByCustomerCore(int customerId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCustomer(customerId);
		}
	}
}