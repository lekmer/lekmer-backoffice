﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerInformationService : CustomerInformationService
	{
		public LekmerCustomerInformationService(CustomerInformationRepository customerInformationRepository, IAddressTypeService addressTypeService, IAddressService addressService)
			: base(customerInformationRepository, addressTypeService, addressService)
		{
		}

		public override ICustomerInformation Create(IUserContext context)
		{
			ICustomerInformation customerInformation = base.Create(context);

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				// Set default gender type
				lekmerCustomerInformation.GenderTypeId = (int)GenderTypeInfo.Unknown;
			}

			customerInformation.Status = BusinessObjectStatus.New;
			return customerInformation;
		}

		public override void Save(IUserContext context, ICustomerInformation customerInformation)
		{
			if (customerInformation == null)
			{
				throw new ArgumentNullException("customerInformation");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(customerInformation);
				customerInformation.DefaultBillingAddressId = SaveAddress(context, customerInformation, customerInformation.DefaultBillingAddress, "BillingAddress");

				if (customerInformation.DefaultDeliveryAddress != null && customerInformation.DefaultDeliveryAddress.Id != customerInformation.DefaultBillingAddress.Id)
				{
					customerInformation.DefaultDeliveryAddressId = SaveAddress(context, customerInformation, customerInformation.DefaultDeliveryAddress, "DeliveryAddress");
				}
				else
				{
					customerInformation.DefaultDeliveryAddress = customerInformation.DefaultBillingAddress;
					customerInformation.DefaultDeliveryAddressId = customerInformation.DefaultBillingAddressId;
				}

				var information = (ILekmerCustomerInformation) customerInformation;
				information.AlternateAddressId = information.AlternateAddress != null 
					? SaveAddress(context, information, information.AlternateAddress, "AlternateAddress") 
					: null;

				// Saving customer information once again to save its addresses values.
				Repository.Save(customerInformation);

				transactedOperation.Complete();
			}
		}
	}
}
