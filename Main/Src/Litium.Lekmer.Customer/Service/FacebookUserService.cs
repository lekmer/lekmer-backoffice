﻿using System;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class FacebookUserService : IFacebookUserService
	{
		protected FacebookUserRepository Repository { get; private set; }

		public FacebookUserService(FacebookUserRepository userRepository)
		{
			Repository = userRepository;
		}

		public virtual IFacebookUser Create(IUserContext context)
		{
			var user = IoC.Resolve<IFacebookUser>();
			user.CreatedDate = DateTime.Now;
			user.Status = BusinessObjectStatus.New;
			return user;
		}

		public virtual void Save(IUserContext context, IFacebookUser user)
		{
			if (user == null) throw new ArgumentNullException("user");

			Repository.Save(user);
		}
	}
}
