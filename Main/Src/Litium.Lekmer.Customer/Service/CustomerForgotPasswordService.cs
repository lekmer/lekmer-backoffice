﻿using System;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class CustomerForgotPasswordService : ICustomerForgotPasswordService
	{
		protected CustomerForgotPasswordRepository Repository { get; private set; }

		public CustomerForgotPasswordService(CustomerForgotPasswordRepository repository)
		{
			Repository = repository;
		}

		public ICustomerForgotPassword Create(int customerId, int validPeriod)
		{
			var currentDate = DateTime.Now;

			var customerForgotPassword = IoC.Resolve<ICustomerForgotPassword>();
			customerForgotPassword.CustomerId = customerId;
			customerForgotPassword.Guid = Guid.NewGuid();
			customerForgotPassword.CreatedDate = currentDate;
			customerForgotPassword.ValidToDate = validPeriod > 0 ? currentDate.AddMinutes(validPeriod) : (DateTime?) null;
			return customerForgotPassword;
		}

		public ICustomerForgotPassword GetByGuid(Guid id)
		{
			return Repository.GetByGuid(id);
		}

		public void Save(ICustomerForgotPassword customerForgotPassword)
		{
			if (customerForgotPassword == null) throw new ArgumentNullException("customerForgotPassword");
			Repository.Save(customerForgotPassword);
		}

		public void Delete(int customerId)
		{
			Repository.Delete(customerId);
		}
	}
}