using Litium.Lekmer.Common;
using Litium.Lekmer.Core.Web;

namespace Litium.Lekmer.Voucher.Web
{
	public class VoucherSession : IVoucherSession
	{
		protected ISessionStateService Session { get; private set; }

		public VoucherSession(ISessionStateService session)
		{
			Session = session;
		}

		public bool WasUsed
		{
			get
			{
				if (Session.Available)
				{
					var wasUsed = Session["VoucherWasUsed"];
					if (wasUsed != null)
					{
						return (bool) wasUsed;
					}
				}

				return false;
			}
			set
			{
				if (Session.Available)
				{
					Session["VoucherWasUsed"] = value;
				}
			}
		}

		public string VoucherCode
		{
			get
			{
				if (Session.Available)
				{
					return Session["VoucherCode"] as string;
				}

				return null;
			}
			set
			{
				if (Session.Available)
				{
					Session["VoucherCode"] = value;
				}
			}
		}

		public IVoucherCheckResult Voucher
		{
			get
			{
				if (Session.Available)
				{
					return Session["VoucherCheckResult"] as IVoucherCheckResult;
				}

				return null;
			}
			set
			{
				if (Session.Available)
				{
					Session["VoucherCheckResult"] = value;
				}
			}
		}
	}
}