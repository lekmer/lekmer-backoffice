﻿using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using log4net;

namespace Litium.Lekmer.Payment.Qliro
{
	public class InvoiceItemsValidation : IInvoiceItemsValidation
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public virtual bool Validate(int orderId, invoiceItemType[] invoiceItems, decimal orderTotalAmount)
		{
			decimal itemsGrossAmount = invoiceItems.Sum(i => i.grossAmount);

			//decimal itemsNetAmount = invoiceItems.Sum(i => (i.netAmount * (1 + i.vatPercent / 100m)).Round());
			decimal itemsNetAmount = invoiceItems.Sum(i => (i.itemPrice * (1 + i.vatPercent / 100m)).Round() * i.quantity);

			if (itemsGrossAmount != orderTotalAmount)
			{
				_log.FatalFormat("{0} : Order's total amount ({1}) doesn't equel to Qliro items gross amount ({2})!!!", orderId, orderTotalAmount, itemsGrossAmount);
				return false;
			}

			if (itemsNetAmount != orderTotalAmount)
			{
				_log.FatalFormat("{0} : Order's total amount ({1}) doesn't equel to Qliro items net amount ({2})!!!", orderId, orderTotalAmount, itemsNetAmount);
				return false;
			}

			return true;
		}
	}
}
