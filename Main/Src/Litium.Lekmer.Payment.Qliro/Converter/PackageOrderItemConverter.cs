﻿using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class PackageOrderItemConverter : IPackageOrderItemConverter
	{
		public virtual invoiceItemType Convert(IPackageOrderItem packageOrderItem, bool taxFreeZone)
		{
			string rowDescription = packageOrderItem.Title;
			decimal vatPercent = packageOrderItem.GetOriginalVatPercent();
			decimal itemGrossAmount = packageOrderItem.OriginalPrice.IncludingVat;

			if (taxFreeZone) // ExclVat
			{
				vatPercent = 0m;
				itemGrossAmount = packageOrderItem.OriginalPrice.ExcludingVat;
			}

			string erpId = packageOrderItem.SizeId.HasValue ? packageOrderItem.SizeErpId : packageOrderItem.ErpId + "-000";

			var invoiceItem = new invoiceItemType
			{
				itemNo = erpId,
				description = rowDescription,
				itemPrice = packageOrderItem.OriginalPrice.ExcludingVat, // item price (excl. VAT)
				grossAmount = itemGrossAmount * packageOrderItem.Quantity, // item price (incl. VAT) * item count
				netAmount = packageOrderItem.OriginalPrice.ExcludingVat * packageOrderItem.Quantity, // item price (excl. VAT) * item count
				vatPercent = vatPercent,
				quantity = packageOrderItem.Quantity,
				unit = null,
				discountAmount = 0.0m,
				feeType = debtFeeType.None
			};

			return invoiceItem;
		}
	}
}
