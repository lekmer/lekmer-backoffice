﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IOrderItemsConverter
	{
		Collection<invoiceItemType> Convert(ILekmerOrderFull lekmerOrderFull, ILekmerChannel lekmerChannel);
	}
}
