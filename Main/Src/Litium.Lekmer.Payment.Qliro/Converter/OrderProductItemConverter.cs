﻿using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class OrderProductItemConverter : IOrderProductItemConverter
	{
		public virtual invoiceItemType Convert(ILekmerOrderItem orderItem, bool taxFreeZone)
		{
			string rowDescription = orderItem.Title;
			decimal vatPercent = orderItem.GetOriginalVatPercent();
			decimal dicountAmount = orderItem.RowDiscount(true);
			decimal itemGrossAmount = orderItem.ActualPrice.IncludingVat;

			if (taxFreeZone) // ExclVat
			{
				vatPercent = 0m;
				dicountAmount = orderItem.RowDiscount(false);
				itemGrossAmount = orderItem.ActualPrice.ExcludingVat;
			}

			string erpId = orderItem.OrderItemSize.SizeId.HasValue ? orderItem.OrderItemSize.ErpId : orderItem.ErpId + "-000";

			var invoiceItem = new invoiceItemType
			{
				itemNo = erpId,
				description = rowDescription,
				itemPrice = orderItem.ActualPrice.ExcludingVat, // item price (excl. VAT)
				grossAmount = itemGrossAmount * orderItem.Quantity, // item price (incl. VAT) * item count
				netAmount = orderItem.ActualPrice.ExcludingVat * orderItem.Quantity, // item price (excl. VAT) * item count
				vatPercent = vatPercent,
				quantity = orderItem.Quantity,
				unit = null,
				discountAmount = dicountAmount * orderItem.Quantity, // total discount
				feeType = debtFeeType.None
			};

			return invoiceItem;
		}
	}
}
