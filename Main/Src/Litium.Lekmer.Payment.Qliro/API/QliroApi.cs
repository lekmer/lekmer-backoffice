﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Web.Script.Serialization;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using Litium.Lekmer.Payment.Qliro.Setting;
using log4net;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroApi: IQliroApi
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected CheckoutSoapClient CheckoutClient { get; private set; }
		protected IQliroTransactionService QliroTransactionService { get; private set; }
		public IQliroSetting QliroSetting { get; set; }

		public QliroApi(IQliroTransactionService qliroTransactionService)
		{
			QliroTransactionService = qliroTransactionService;
		}

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in Sweden ?
		/// </summary>
		public virtual IQliroGetAddressResponse GetAddress(string civicNumber, juridicalType juridicalType, string ip, string languageCode)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Get addresses");
			}

			var duration = new Stopwatch();
			IQliroGetAddressResponse qliroResult = null;

			int transactionId = QliroTransactionService.CreateGetAddress(QliroSetting.ClientRef, civicNumber);

			try
			{
				CreateClient();

				duration.Start();
				getAddressReply addressReply = CheckoutClient.GetAddress(QliroSetting.ClientRef, civicNumber, juridicalType, ip, languageCode);
				duration.Stop();

				qliroResult = new QliroGetAddressResponse
				{
					StatusCode = QliroTransactionStatus.Ok,
					ReturnCode = addressReply.returnCode,
					CustomerMessage = addressReply.customerMessage,
					Message = addressReply.message,
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return BuildQliroGetAddressResponse(qliroResult, addressReply);
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Qliro call returned exception on get address for '{0}'.", civicNumber);

				LogException(logMessage, ex);

				qliroResult = new QliroGetAddressResponse
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleQliroException(ex, qliroResult);

				return qliroResult;
			}
			finally
			{
				QliroTransactionService.SaveResult(qliroResult);
			}
		}

		/// <summary>
		/// Gets payment types for specific language.
		/// </summary>
		public virtual IQliroGetPaymentTypesResponse GetPaymentTypes(string languageCode)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Get payment types");
			}

			var duration = new Stopwatch();
			IQliroGetPaymentTypesResponse qliroResult = null;

			int transactionId = QliroTransactionService.CreateGetPaymentTypes(QliroSetting.ClientRef);

			try
			{
				CreateClient();

				duration.Start();
				getPaymentTypesReply getPaymentTypesReply = CheckoutClient.GetPaymentTypes(QliroSetting.ClientRef, languageCode);
				duration.Stop();

				qliroResult = new QliroGetPaymentTypesResponse
				{

					StatusCode = QliroTransactionStatus.Ok,
					ReturnCode = getPaymentTypesReply.returnCode,
					Message = getPaymentTypesReply.message,
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return BuildQliroGetPaymentTypesResponse(qliroResult, getPaymentTypesReply);
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Qliro call returned exception on get payment types for '{0}'.", languageCode);

				LogException(logMessage, ex);

				qliroResult = new QliroGetPaymentTypesResponse
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleQliroException(ex, qliroResult);

				return qliroResult;
			}
			finally
			{
				QliroTransactionService.SaveResult(qliroResult);
			}
		}

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		public virtual IQliroReservationResult ReserveAmount(
			string languageCode,
			string civicNumber,
			pNoEncodingType pNoEncoding,
			genderType gender,
			juridicalType juridical,
			decimal amount,
			int orderId,
			address deliveryAddr,
			address billingAddr,
			string clientIp,
			string currencyCode,
			string countryCode,
			string preferredLanguage,
			string paymentType,
			string comment,
			string email,
			string cellularPhone,
			invoiceItemType[] invoiceItems
			)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Reserve amount");
			}

			productionStatusType productionStatus = GetProductionStatus();
			communicationChannel communicationChannel = GetCommunicationChannel();

			var duration = new Stopwatch();
			IQliroReservationResult qliroResult = null;

			int transactionId = QliroTransactionService.CreateReservation(
				QliroSetting.ClientRef,
				(int)productionStatus,
				civicNumber,
				orderId,
				amount,
				currencyCode,
				paymentType
			);

			try
			{
				CreateClient();

				duration.Start();
				reservationReply reservationReply = CheckoutClient.Reservation(
					languageCode,
					civicNumber,
					pNoEncoding,
					gender,
					juridical,
					amount,
					orderId.ToString(CultureInfo.InvariantCulture),
					deliveryAddr,
					billingAddr,
					clientIp,
					productionStatus,
					currencyCode,
					countryCode,
					preferredLanguage,
					QliroSetting.ClientRef,
					paymentType,
					comment,
					email,
					cellularPhone,
					communicationChannel,
					invoiceItems
					);
				duration.Stop();

				_log.InfoFormat("Qliro reservation made!. OrderId: {0}. ReferenceNumber: {1}", orderId, reservationReply.reservationNo);

				// Return reservation number.
				qliroResult = new QliroReservationResult
				{
					StatusCode = QliroTransactionStatus.Ok,
					ReturnCode = reservationReply.returnCode,
					ReservationId = reservationReply.reservationNo,
					InvoiceStatus = (QliroInvoiceStatus)reservationReply.invoiceStatus,
					CustomerMessage = reservationReply.customerMessage,
					Message = reservationReply.message,
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return qliroResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Qliro call returned exception on reserve amount. OrderId: {0}", orderId);

				LogException(logMessage, ex);

				qliroResult = new QliroReservationResult
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleQliroException(ex, qliroResult);

				return qliroResult;
			}
			finally
			{
				QliroTransactionService.SaveReservationResponse(qliroResult);
			}
		}

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		public virtual IQliroReservationResult ReserveAmountTimeout(
			string languageCode,
			string civicNumber,
			pNoEncodingType pNoEncoding,
			genderType gender,
			juridicalType juridical,
			decimal amount,
			int orderId,
			address deliveryAddr,
			address billingAddr,
			string clientIp,
			string currencyCode,
			string countryCode,
			string preferredLanguage,
			string paymentType,
			string comment,
			string email,
			string cellularPhone,
			invoiceItemType[] invoiceItems
			)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Reserve amount");
			}

			productionStatusType productionStatus = GetProductionStatus();
			communicationChannel communicationChannel = GetCommunicationChannel();

			var duration = new Stopwatch();
			IQliroReservationResult qliroResult = null;

			int transactionId = QliroTransactionService.CreateReservation(
				QliroSetting.ClientRef,
				(int)productionStatus,
				civicNumber,
				orderId,
				amount,
				currencyCode,
				paymentType
			);

			try
			{
				CreateClient();

				duration.Start();
				var qliroReservationRequest = new QliroReservationRequest
					{
						CheckoutClient = CheckoutClient,
						QliroSetting = QliroSetting,
						LanguageCode = languageCode,
						CivicNumber = civicNumber,
						PNoEncoding = pNoEncoding,
						Gender = gender,
						Juridical = juridical,
						Amount = amount,
						OrderId = orderId,
						DeliveryAddr = deliveryAddr,
						BillingAddr = billingAddr,
						ClientIp = clientIp,
						ProductionStatus = productionStatus,
						CurrencyCode = currencyCode,
						CountryCode = countryCode,
						PreferredLanguage = preferredLanguage,
						PaymentType = paymentType,
						Comment = comment,
						Email = email,
						CellularPhone = cellularPhone,
						CommunicationChannel = communicationChannel,
						InvoiceItems = invoiceItems
					};
				var qliroManager = new QliroManager(QliroTransactionService) { ReservationRequest = qliroReservationRequest };
				var qliroReservationResponse = qliroManager.ExecuteReservation();
				duration.Stop();

				if (qliroReservationResponse == null)
				{
					qliroResult = new QliroReservationResult
					{
						StatusCode = QliroTransactionStatus.TimeoutResponse,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};
				}
				else if (qliroReservationResponse.ReservationReply != null)
				{
					var reservationReply = qliroReservationResponse.ReservationReply;

					_log.InfoFormat("Qliro reservation made!. OrderId: {0}. ReferenceNumber: {1}", orderId, reservationReply.reservationNo);

					// Return reservation number.
					qliroResult = new QliroReservationResult
					{
						StatusCode = QliroTransactionStatus.Ok,
						ReturnCode = reservationReply.returnCode,
						ReservationId = reservationReply.reservationNo,
						InvoiceStatus = (QliroInvoiceStatus)reservationReply.invoiceStatus,
						CustomerMessage = reservationReply.customerMessage,
						Message = reservationReply.message,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};
				}
				else // Exception
				{
					string logMessage = string.Format("Qliro call returned exception on reserve amount. OrderId: {0}", orderId);

					Exception exception = qliroReservationResponse.ReservationException;
					LogException(logMessage, exception);

					qliroResult = new QliroReservationResult
					{
						StatusCode = QliroTransactionStatus.UnspecifiedError,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds,
						Message = exception.ToString()
					};
				}

				return qliroResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Qliro call returned exception on reserve amount. OrderId: {0}", orderId);

				LogException(logMessage, ex);

				qliroResult = new QliroReservationResult
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleQliroException(ex, qliroResult);

				return qliroResult;
			}
			finally
			{
				QliroTransactionService.SaveReservationResponse(qliroResult);
			}
		}

		/// <summary>
		/// Checks status on your order
		/// </summary>
		public virtual IQliroGetStatusResult CheckOrderStatus(int orderId, string reservationNumber, string languageCode)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Check order status");
			}

			var duration = new Stopwatch();
			IQliroGetStatusResult qliroResult = null;

			int transactionId = QliroTransactionService.CreateCheckOrderStatus(QliroSetting.ClientRef, orderId, reservationNumber);

			try
			{
				CreateClient();

				duration.Start();
				getStatusReply getStatusReply = CheckoutClient.GetStatus(
					QliroSetting.ClientRef,
					reservationNumber,
					orderId.ToString(CultureInfo.InvariantCulture),
					languageCode
				);
				duration.Stop();

				_log.InfoFormat("Qliro check order status result: reservation number: {0}, status: {1}", reservationNumber, getStatusReply.invoiceStatus);

				qliroResult = new QliroGetStatusResult
				{
					StatusCode = QliroTransactionStatus.Ok,
					ReturnCode = getStatusReply.returnCode,
					InvoiceStatus = (QliroInvoiceStatus)getStatusReply.invoiceStatus,
					Message = getStatusReply.message,
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return qliroResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Qliro call returned exception on check order status. Reservation number: {0}", reservationNumber);

				LogException(logMessage, ex);

				qliroResult = new QliroGetStatusResult
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleQliroException(ex, qliroResult);

				return qliroResult;
			}
			finally
			{
				QliroTransactionService.SaveCheckOrderStatusResponse(qliroResult);
			}
		}

		public virtual void Initialize(string channelCommonName)
		{
			QliroSetting.Initialize(channelCommonName);
		}

		protected virtual void CreateClient()
		{
			if (CheckoutClient != null) return;

			CheckoutClient = new CheckoutSoapClient();

			CheckoutClient.Endpoint.Address = new EndpointAddress(QliroSetting.Url);

			if (CheckoutClient.ClientCredentials != null)
			{
				CheckoutClient.ClientCredentials.UserName.UserName = QliroSetting.UserName;
				CheckoutClient.ClientCredentials.UserName.Password = QliroSetting.Password;
			}

			CheckoutClient.InnerChannel.OperationTimeout = TimeSpan.FromMilliseconds(QliroSetting.DefaultResponseTimeout);

			if (QliroSetting.IgnoreCertificateValidation)
			{
				ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
			}
		}

		protected virtual productionStatusType GetProductionStatus()
		{
			productionStatusType productionStatus = productionStatusType.Test;

			try
			{
				productionStatus = (productionStatusType)Enum.Parse(typeof(productionStatusType), QliroSetting.ProductionStatus, true);
			}
			catch (ArgumentException ex)
			{
				string message = string.Format("'{0}' is not a member of the productionStatusType enumeration.", QliroSetting.ProductionStatus);
				LogException(message, ex);
			}

			return productionStatus;
		}

		protected virtual communicationChannel GetCommunicationChannel()
		{
			communicationChannel communicationChannel = communicationChannel.Email;

			try
			{
				communicationChannel = (communicationChannel)Enum.Parse(typeof(communicationChannel), QliroSetting.CommunicationChannel, true);
			}
			catch (ArgumentException ex)
			{
				string message = string.Format("'{0}' is not a member of the communicationChannel enumeration.", QliroSetting.CommunicationChannel);
				LogException(message, ex);
			}

			return communicationChannel;
		}

		private void LogException(string message, Exception ex)
		{
			_log.Error(message, ex);
		}

		private void TryHandleQliroException<T>(Exception ex, T qliroResult) where T : IQliroResult
		{
			// Unspecified error
			qliroResult.Message = ex.ToString();
			qliroResult.StatusCode = QliroTransactionStatus.UnspecifiedError;
		}

		/// <summary>
		/// Builds a IQliroResponseItem from a Qliro call result.
		/// </summary>
		private static IQliroGetAddressResponse BuildQliroGetAddressResponse(IQliroGetAddressResponse qliroGetAddressResponse, getAddressReply getAddressReply)
		{
			try
			{
				if (getAddressReply.adresses != null && getAddressReply.adresses.Length > 0)
				{
					qliroGetAddressResponse.Addresses = getAddressReply.adresses;
				}
			}
			catch (Exception)
			{
				_log.Error("Qliro call returned an invalid format in BuildQliroGetAddressResponse.");
			}

			try
			{
				if (getAddressReply.adresses != null && getAddressReply.adresses.Length > 0)
				{
					var serializer = new JavaScriptSerializer();
					string json = serializer.Serialize(getAddressReply.adresses);
					qliroGetAddressResponse.ResponseContent = json;
				}
			}
			catch (Exception)
			{
				_log.Error("Json serialization fails in BuildQliroGetAddressResponse.");
			}

			return qliroGetAddressResponse;
		}

		private static IQliroGetPaymentTypesResponse BuildQliroGetPaymentTypesResponse(IQliroGetPaymentTypesResponse qliroGetPaymentTypesResponse, getPaymentTypesReply getPaymentTypesReply)
		{
			try
			{
				if (getPaymentTypesReply.paymentTypes != null && getPaymentTypesReply.paymentTypes.Length > 0)
				{
					qliroGetPaymentTypesResponse.PaymentTypes = getPaymentTypesReply.paymentTypes;
				}
			}
			catch (Exception)
			{
				_log.Error("Qliro call returned an invalid format in BuildQliroGetPaymentTypesResponse.");
			}

			try
			{
				if (getPaymentTypesReply.paymentTypes != null && getPaymentTypesReply.paymentTypes.Length > 0)
				{
					var serializer = new JavaScriptSerializer();
					string json = serializer.Serialize(getPaymentTypesReply.paymentTypes);
					qliroGetPaymentTypesResponse.ResponseContent = json;
				}
			}
			catch (Exception)
			{
				_log.Error("Json serialization fails in BuildQliroGetPaymentTypesResponse.");
			}

			return qliroGetPaymentTypesResponse;
		}

		private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			if (sslPolicyErrors == SslPolicyErrors.None)
			{
				return true;
			}

			return true;
		}
	}
}