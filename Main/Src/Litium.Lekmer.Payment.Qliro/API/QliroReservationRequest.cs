﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using Litium.Lekmer.Payment.Qliro.Setting;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroReservationRequest
	{
		public CheckoutSoapClient CheckoutClient { get; set; }
		public IQliroSetting QliroSetting { get; set; }
		public communicationChannel CommunicationChannel { get; set; }
		public productionStatusType ProductionStatus { get; set; }
		public string LanguageCode { get; set; }
		public string CivicNumber { get; set; }
		public pNoEncodingType PNoEncoding { get; set; }
		public genderType Gender { get; set; }
		public juridicalType Juridical { get; set; }
		public decimal Amount { get; set; }
		public int OrderId { get; set; }
		public address DeliveryAddr { get; set; }
		public address BillingAddr { get; set; }
		public string ClientIp { get; set; }
		public string CurrencyCode { get; set; }
		public string CountryCode { get; set; }
		public string PreferredLanguage { get; set; }
		public string PaymentType { get; set; }
		public string Comment { get; set; }
		public string Email { get; set; }
		public string CellularPhone { get; set; }
		public invoiceItemType[] InvoiceItems { get; set; }
	}
}
