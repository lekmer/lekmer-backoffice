﻿using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro
{
	public static class QliroFactory
	{
		public static IQliroSetting CreateQliroSetting()
		{
			return CreateQliroSetting(Channel.Current.CommonName);
		}

		public static IQliroSetting CreateQliroSetting(string channelCommonName)
		{
			var qliroSetting = IoC.Resolve<IQliroSetting>();

			qliroSetting.Initialize(channelCommonName);

			return qliroSetting;
		}

		public static IQliroClient CreateQliroClient(string channelCommonName)
		{
			var qliroClient = IoC.Resolve<IQliroClient>();

			qliroClient.QliroSetting = CreateQliroSetting(channelCommonName);
			qliroClient.Initialize(channelCommonName);

			return qliroClient;
		}
	}
}