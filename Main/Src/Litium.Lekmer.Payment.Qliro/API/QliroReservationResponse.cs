﻿using System;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroReservationResponse
	{
		public reservationReply ReservationReply { get; set; }
		public Exception ReservationException { get; set; }
		public bool IsCompleated { get; set; } 
	}
}
