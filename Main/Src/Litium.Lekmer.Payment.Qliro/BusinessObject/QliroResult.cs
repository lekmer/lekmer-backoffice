﻿namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroResult : IQliroResult
	{
		public int TransactionId { get; set; }
		public QliroTransactionStatus StatusCode { get; set; }
		public int ReturnCode { get; set; }
		public string Message { get; set; }
		public string CustomerMessage { get; set; }
		public long Duration { get; set; }
		public string ResponseContent { get; set; }
	}
}
