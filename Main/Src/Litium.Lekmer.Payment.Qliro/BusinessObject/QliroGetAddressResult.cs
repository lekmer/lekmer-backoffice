﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroGetAddressResult : QliroResult, IQliroGetAddressResult
	{
		public Collection<IQliroAddress> QliroAddresses { get; set; }
	}
}