﻿namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroGetStatusResult : QliroResult, IQliroGetStatusResult
	{
		public QliroInvoiceStatus InvoiceStatus { get; set; }
	}
}