﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Qliro.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroPendingOrderService : IQliroPendingOrderService
	{
		protected QliroPendingOrderRepository Repository { get; private set; }

		public QliroPendingOrderService(QliroPendingOrderRepository repository)
		{
			Repository = repository;
		}

		public virtual IQliroPendingOrder Create()
		{
			var qliroPendingOrder = IoC.Resolve<IQliroPendingOrder>();

			qliroPendingOrder.FirstAttempt = DateTime.Now;
			qliroPendingOrder.LastAttempt = DateTime.Now;

			return qliroPendingOrder;
		}

		public virtual int Insert(IQliroPendingOrder qliroPendingOrder)
		{
			Repository.EnsureNotNull();

			return Repository.Insert(qliroPendingOrder);
		}

		public virtual Collection<IQliroPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			Repository.EnsureNotNull();

			return Repository.GetPendingOrdersForStatusCheck(checkToDate);
		}

		public virtual void Update(IQliroPendingOrder qliroPendingOrder)
		{
			Repository.EnsureNotNull();

			Repository.Update(qliroPendingOrder);
		}

		public virtual void Delete(int qliroPendingOrderId)
		{
			Repository.EnsureNotNull();

			Repository.Delete(qliroPendingOrderId);
		}
	}
}
