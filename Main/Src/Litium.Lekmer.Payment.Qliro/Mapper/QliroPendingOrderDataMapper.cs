using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro.Mapper
{
	public class QliroPendingOrderDataMapper : DataMapperBase<IQliroPendingOrder>
	{
		public QliroPendingOrderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IQliroPendingOrder Create()
		{
			var klarnaPendingOrder = IoC.Resolve<IQliroPendingOrder>();

			klarnaPendingOrder.Id = MapValue<int>("QliroPendingOrder.QliroPendingOrderId");
			klarnaPendingOrder.ChannelId = MapValue<int>("QliroPendingOrder.ChannelId");
			klarnaPendingOrder.OrderId = MapValue<int>("QliroPendingOrder.OrderId");
			klarnaPendingOrder.FirstAttempt = MapValue<DateTime>("QliroPendingOrder.FirstAttempt");
			klarnaPendingOrder.LastAttempt = MapValue<DateTime>("QliroPendingOrder.LastAttempt");

			return klarnaPendingOrder;
		}
	}
}