﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Qliro.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroPaymentTypeSecureService : IQliroPaymentTypeSecureService
	{
		protected QliroPaymentTypeRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public QliroPaymentTypeSecureService(
			QliroPaymentTypeRepository repository,
			IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public Collection<IQliroPaymentType> GetAllByChannel(IChannel channel)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByChannel(channel.Id);
		}

		public void Delete(ISystemUserFull systemUserFull, int qliroPaymentTypeId)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.ToolsPayments);

			Repository.Delete(qliroPaymentTypeId);
		}

		public int Save(ISystemUserFull systemUserFull, IQliroPaymentType qliroPaymentType)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.ToolsPayments);

			return Repository.Save(qliroPaymentType);
		}
	}
}
