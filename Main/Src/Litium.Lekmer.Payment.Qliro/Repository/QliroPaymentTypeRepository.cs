﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro.Repository
{
	public class QliroPaymentTypeRepository
	{
		protected virtual DataMapperBase<IQliroPaymentType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IQliroPaymentType>(dataReader);
		}

		public virtual Collection<IQliroPaymentType> GetAllByChannel(int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("QliroPaymentTypeRepository.GetAllByChannel");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pQliroPaymentTypeGetAllByChannel]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(IQliroPaymentType qliroPaymentType)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@QliroPaymentTypeId", qliroPaymentType.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", qliroPaymentType.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Code", qliroPaymentType.Code, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Description", qliroPaymentType.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@RegistrationFee", qliroPaymentType.RegistrationFee, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@SettlementFee", qliroPaymentType.SettlementFee, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@InterestRate", qliroPaymentType.InterestRate, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@InterestType", qliroPaymentType.InterestType, SqlDbType.Int),
					ParameterHelper.CreateParameter("@InterestCalculation", qliroPaymentType.InterestCalculation, SqlDbType.Int),
					ParameterHelper.CreateParameter("@NoOfMonths", qliroPaymentType.NoOfMonths, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MinPurchaseAmount", qliroPaymentType.MinPurchaseAmount, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("QliroPaymentTypeRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pQliroPaymentTypeSave]", parameters, dbSettings);
		}

		public virtual void Delete(int qliroPaymentTypeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("QliroPaymentTypeId", qliroPaymentTypeId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("QliroPaymentTypeRepository.Delete");

			new DataHandler().ExecuteCommand("[orderlek].[pQliroPaymentTypeDelete]", parameters, dbSettings);
		}
	}
}
