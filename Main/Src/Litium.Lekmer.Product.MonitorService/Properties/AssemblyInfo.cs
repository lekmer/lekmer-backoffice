﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Litium.Lekmer.Product.MonitorService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: Guid("e071acc5-2c6e-4f1f-91d5-850d66dd822b")]
[assembly: CLSCompliant(true)]

// Configure log4net using the .config file
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
