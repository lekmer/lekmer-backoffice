﻿using System.ServiceProcess;

namespace Litium.Lekmer.Product.MonitorService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] servicesToRun = new ServiceBase[] 
			{ 
				new MonitorProductWinService() 
			};
			ServiceBase.Run(servicesToRun);
		}
	}
}
