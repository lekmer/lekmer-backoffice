﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.MonitorService
{
	public class MonitorProductJob : BaseJob
	{
		public override string Group
		{
			get { return "MonitorProduct"; }
		}

		public override string Name
		{
			get { return "MonitorProductJob"; }
		}

		protected override void ExecuteAction()
		{
			var monitorProductSetting = IoC.Resolve<IMonitorProductSetting>();
			var monitorProductService = IoC.Resolve<IMonitorProductService>();

			monitorProductService.Monitor(monitorProductSetting.PortionSize, monitorProductSetting.BreakDuration, monitorProductSetting.MinNumberInStockDefault);
		}
	}
}
