﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Product.MonitorService
{
	public class MonitorProductScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "MonitorProduct"; }
		}

		protected override string GroupName
		{
			get { return "MonitorProductJob"; }
		}
	}
}