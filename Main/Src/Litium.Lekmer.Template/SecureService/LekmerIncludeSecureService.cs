using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Template.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Repository;

namespace Litium.Lekmer.Template
{
	public class LekmerIncludeSecureService : IncludeSecureService
	{
		/// <summary>
		/// Constructor with data handler.
		/// </summary>
		public LekmerIncludeSecureService(IAccessValidator accessValidator, IncludeRepository includeRepository)
			: base(accessValidator, includeRepository)
		{
		}

		/// <summary>
		/// Saves a Include
		/// </summary>
		public override int Save(ISystemUserFull systemUserFull, IInclude include)
		{
			if (include == null) throw new ArgumentNullException("include");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.InterfaceInclude);

			include.Id = Repository.Save(include);

			// Clean up cached item
			IncludeCache.Instance.Remove(new IncludeKey(include.CommonName));

			new TemplateModificationNotifier().OnTemplateModified();

			return include.Id;
		}

		/// <summary>
		/// Deletes a Include
		/// </summary>
		public override void Delete(ISystemUserFull systemUserFull, int includeId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.InterfaceInclude);

			Repository.Delete(includeId);

			// Clean up all cached items
			IncludeCache.Instance.Flush();

			new TemplateModificationNotifier().OnTemplateModified();
		}

		/// <summary>
		/// Deletes all alias from alias folder
		/// </summary>
		/// <returns>rows affected</returns>
		public override void DeleteAllByAliasFolder(ISystemUserFull systemUserFull, int folderId)
		{
			if (folderId <= 0) throw new ArgumentOutOfRangeException("folderId");
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");

			Collection<IInclude> includes = Repository.GetAllByIncludeFolder(folderId);

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var item in includes)
				{
					Delete(systemUserFull, item.Id);
				}
				transactedOperation.Complete();
			}

			// Clean up cached items
			foreach (var item in includes)
			{
				IncludeCache.Instance.Remove(new IncludeKey(item.CommonName));
			}
		}
	}
}