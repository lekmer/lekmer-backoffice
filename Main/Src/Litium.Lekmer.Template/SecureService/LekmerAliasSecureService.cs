﻿using Litium.Scensum.Core;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Repository;

namespace Litium.Lekmer.Template
{
	public class LekmerAliasSecureService : AliasSecureService, ILekmerAliasSecureService
	{
		public LekmerAliasSecureService(IAccessValidator accessValidator, AliasRepository aliasRepository)
			: base(accessValidator, aliasRepository)
		{
		}

		public IAlias GetByCommonName(string commonName)
		{
			return Repository.GetByCommonNameSecure(commonName);
		}
	}
}