﻿using System;
using System.Reflection;
using Litium.Scensum.Core;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Cache;
using Litium.Scensum.Template.Repository;
using log4net;

namespace Litium.Lekmer.Template
{
	public class LekmerAliasSharedService : AliasSharedService
	{
		private const int DEFAULT_ALIAS_CACHE_KEY = -13;
		private const string INVALID_ALIAS_ERROR = "Cannot get Alias from database where ChannelId = {0} and CommonName = {1}";

		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public LekmerAliasSharedService(AliasRepository aliasRepository) : base(aliasRepository)
		{
		}

		public override IAlias GetByCommonName(IChannel channel, string commonName)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			AliasKey aliasKey = new AliasKey(channel.Id, commonName);
			IAlias alias = AliasCache.Instance.GetData(aliasKey);

			if (alias == null)
			{
				alias = GetByCommonNameCore(channel, commonName);

				// If (alias == null) it cannot be putted into cache and fake Alias object will be added there.
				if (alias == null)
				{
					alias = new Alias { Id = DEFAULT_ALIAS_CACHE_KEY };
					_log.WarnFormat(INVALID_ALIAS_ERROR, channel.Id, commonName);
				}

				AliasCache.Instance.Add(aliasKey, alias);
			}
			
			// Return null if Alias is a fake object. 
			return alias.Id == DEFAULT_ALIAS_CACHE_KEY ? null : alias;
		}
	}
}
