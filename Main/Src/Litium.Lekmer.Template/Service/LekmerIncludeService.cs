using System;
using Litium.Lekmer.Template.Cache;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Repository;

namespace Litium.Lekmer.Template
{
	[Obsolete("Obsolete since 2.1.1.")]
	public class LekmerIncludeService : IncludeService
	{
		public LekmerIncludeService(IncludeRepository includeRepository)
			: base(includeRepository)
		{
		}

		/// <summary>
		/// Gets include by common Name
		/// </summary>
		public override IInclude GetByCommonName(string commonName)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			return IncludeCache.Instance.TryGetItem(
				new IncludeKey(commonName),
				() => Repository.GetByCommonName(commonName));
		}
	}
}