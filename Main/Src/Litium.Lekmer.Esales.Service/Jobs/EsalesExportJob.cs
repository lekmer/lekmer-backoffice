﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Esales.Exporter;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Service
{
	public class EsalesExportJob : BaseJob
	{
		public override string Name
		{
			get { return "ExportProductToEsales"; }
		}

		protected override void ExecuteAction()
		{
			var esalesProductInfoExporter = IoC.Resolve<IExporter>("EsalesProductInfoExporter");
			esalesProductInfoExporter.Execute();
		}
	}
}
