﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Lekmer.Cache.Setting;
using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Esales.Service
{
	public partial class EsalesServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private object _syncToken = new object();
		private Collection<IJob> _jobs = new Collection<IJob>();

		public EsalesServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeCache();

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			try
			{
				foreach (IJob job in _jobs)
				{
					job.StopJob();
				}

				_log.Info("Service stopped successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Error occurred while stopping service.", ex);
			}
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var esalesDefragJob = new EsalesDefragJob {ScheduleSetting = new EsalesDefragScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken};
			_jobs.Add(esalesDefragJob);

			var esalesExportJob = new EsalesExportJob { ScheduleSetting = new EsalesExportJobScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(esalesExportJob);

			var esalesExportIncrementalJob = new EsalesExportIncrementalJob { ScheduleSetting = new EsalesExportIncrementalJobScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(esalesExportIncrementalJob);

			var productChangesCleanUpJob = new ProductChangesCleanUpJob { ScheduleSetting = new ProductChangesCleanUpJobScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(productChangesCleanUpJob);
		}

		private void InitializeCache()
		{
			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();

			CacheUpdateCleaner.WorkerCleanInterval = new LekmerCacheUpdateCleanerSetting().CacheUpdateIntervalSeconds * 1000;
#if DEBUG
			CacheUpdateCleaner.WorkerCleanInterval = 1 * 1000; // Clean interval lowered in debug mode.
#endif
			CacheUpdateCleaner.StartWorker();
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}
