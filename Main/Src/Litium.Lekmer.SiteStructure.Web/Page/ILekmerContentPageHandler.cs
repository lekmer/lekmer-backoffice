﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.SiteStructure.Web
{
	public interface ILekmerContentPageHandler
	{
		/// <summary>
		/// Finds specified type of blocks rised on the page.
		/// </summary>
		Collection<TBlockControl> FindBlockControls<TBlockControl>() where TBlockControl : class;
	}
}