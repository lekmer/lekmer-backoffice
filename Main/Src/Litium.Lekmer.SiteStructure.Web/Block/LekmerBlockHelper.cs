﻿using System.Globalization;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.SiteStructure.Web
{
	public static class LekmerBlockHelper
	{
		public static void AddLekmerBlockVariables(Fragment fragment, IBlock item)
		{
			fragment.AddVariable("Block.Title.Raw", item.Title, VariableEncoding.None);
			var lekmerBlockItem = item as ILekmerBlock;
			if (null != lekmerBlockItem)
			{
				var startDate = lekmerBlockItem.StartDate.HasValue
					? lekmerBlockItem.StartDate.Value.ToString("yyyy-MM-dd HH:mm:ss", DateTimeFormatInfo.InvariantInfo)
					: string.Empty;
				var endDate = lekmerBlockItem.EndDate.HasValue
					? lekmerBlockItem.EndDate.Value.ToString("yyyy-MM-dd HH:mm:ss", DateTimeFormatInfo.InvariantInfo)
					: string.Empty;
				var startDailyInterval = lekmerBlockItem.StartDailyInterval.HasValue
					? lekmerBlockItem.StartDailyInterval.Value.ToString()
					: string.Empty;
				var endDailyInterval = lekmerBlockItem.EndDailyInterval.HasValue
				   ? lekmerBlockItem.EndDailyInterval.Value.ToString()
				   : string.Empty;
				fragment.AddVariable("Block.StartDate", startDate, VariableEncoding.None);
				fragment.AddVariable("Block.EndDate", endDate, VariableEncoding.None);
				fragment.AddVariable("Block.StartDailyInterval", startDailyInterval, VariableEncoding.None);
				fragment.AddVariable("Block.EndDailyInterval", endDailyInterval, VariableEncoding.None);
			}
		}
	}
}
