﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.SiteStructure.Web
{
	public class LekmerBlockHelloWorldControl : HelloWorldControl
	{
		public LekmerBlockHelloWorldControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		protected override BlockContent RenderCore()
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}
	}
}