﻿using System.Globalization;
using Litium.Lekmer.Esales;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerCustomerSession : CustomerSession
	{
		protected IEsalesNotifyService EsalesNotifyService { get; private set; }

		public LekmerCustomerSession(IUserContextFactory userContextFactory, IEsalesNotifyService esalesNotifyService)
			: base(userContextFactory)
		{
			EsalesNotifyService = esalesNotifyService;
		}

		protected override void OnCustomerUpdated()
		{
			base.OnCustomerUpdated();

			ICustomer signedInCustomer = SignedInCustomer;
			if (signedInCustomer != null)
			{
				EsalesNotifyService.NotifyCustomerKey("customer_key_" + signedInCustomer.Id.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				EsalesNotifyService.NotifySessionEnd();
			}
		}
	}
}