using System;
using System.Collections.Specialized;
using System.Globalization;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Customer detail form.
	/// </summary>
	public class CustomerDetailForm : ControlBase
	{
		private readonly string _customerDetailPostUrl;
		private LekmerInformationForm _customerInformationForm;
		private AddressForm _billingAddressForm;
		private AddressForm _deliveryAddressForm;
		private FacebookUtility _facebookUtility;
		private ICustomerService _customerService;
		private IUserService _userService;
		private IAddressService _addressService;

		public virtual string PostUrl
		{
			get { return _customerDetailPostUrl; }
		}
		public LekmerInformationForm CustomerInformationForm
		{
			get { return _customerInformationForm ?? (_customerInformationForm = new LekmerInformationForm()); }
		}
		public AddressForm BillingAddressForm
		{
			get { return _billingAddressForm ?? (_billingAddressForm = new LekmerAddressForm("BillingAddress", "billingaddress")); }
		}
		public AddressForm DeliveryAddressForm
		{
			get { return _deliveryAddressForm ?? (_deliveryAddressForm = new LekmerAddressForm("DeliveryAddress", "deliveryaddress")); }
		}
		protected FacebookUtility FacebookUtility
		{
			get
			{
				return _facebookUtility ?? (_facebookUtility = new FacebookUtility(IoC.Resolve<IMyAccountSetting>()));
			}
		}
		public ICustomerService CustomerService
		{
			get { return _customerService ?? (_customerService = IoC.Resolve<ICustomerService>()); }
		}
		public IUserService UserService
		{
			get { return _userService ?? (_userService = IoC.Resolve<IUserService>()); }
		}
		public IAddressService AddressService
		{
			get { return _addressService ?? (_addressService = IoC.Resolve<IAddressService>()); }
		}

		public virtual string PostModeValue
		{
			get { return "customerdetail"; }
		}
		public virtual string CustomerIdFormName
		{
			get { return "customer-id"; }
		}
		public virtual string UserNameFormName
		{
			get { return "customer-username"; }
		}
		public virtual string OldPasswordFormName
		{
			get { return "customer-oldpassword"; }
		}
		public virtual string NewPasswordFormName
		{
			get { return "customer-newpassword"; }
		}
		public virtual string NewPasswordVerifyFormName
		{
			get { return "customer-newpasswordverify"; }
		}
		public virtual string UseBillingAddressAsDeliveryAddressFormName
		{
			get { return "customer-deliveryaddress-usebillingaddress"; }
		}
		public virtual string ChangeUserNameActionName
		{
			get { return "customer-changeusername-action"; }
		}
		public virtual string ChangePasswordActionName
		{
			get { return "customer-changepassword-action"; }
		}
		public virtual string UpdateCustomerInformationActionName
		{
			get { return "customer-update-action"; }
		}
		public virtual string UserName { get; set; }
		public virtual string CustomerId { get; set; }
		public virtual string OldPassword { get; set; }
		public virtual string NewPassword { get; set; }
		public virtual string NewPasswordVerify { get; set; }
		public virtual IFacebookUser FacebookUser { get; set; }
		public virtual bool UseBillingAddressAsDeliveryAddress { get; set; }
		public virtual bool IsChangePasswordActionPost
		{
			get { return Request.Form[ChangePasswordActionName] != null; }
		}
		public virtual bool IsChangeUsernameActionPost
		{
			get { return Request.Form[ChangeUserNameActionName] != null; }
		}
		public virtual bool IsUpdateCustomerInformationActionPost
		{
			get { return Request.Form[UpdateCustomerInformationActionName] != null; }
		}
		public virtual bool IsFacebookUser
		{
			get { return FacebookUser != null; }
		}

		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public CustomerDetailForm(string customerDetailPostUrl)
		{
			_customerDetailPostUrl = customerDetailPostUrl;
		}

		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.User.CustomerId.Name", CustomerIdFormName);
			fragment.AddVariable("Form.User.UserName.Name", UserNameFormName);
			fragment.AddVariable("Form.User.OldPassword.Name", OldPasswordFormName);
			fragment.AddVariable("Form.User.NewPassword.Name", NewPasswordFormName);
			fragment.AddVariable("Form.User.NewPasswordVerify.Name", NewPasswordVerifyFormName);
			fragment.AddVariable("Form.User.ChangeUserName.Action.Name", ChangeUserNameActionName);
			fragment.AddVariable("Form.User.ChangePassword.Action.Name", ChangePasswordActionName);
			fragment.AddVariable("Form.User.UpdateCustomerInformation.Action.Name", UpdateCustomerInformationActionName);
			fragment.AddVariable("Form.UseBillingAddressAsDeliveryAddress.Name", UseBillingAddressAsDeliveryAddressFormName);

			CustomerInformationForm.MapFieldNamesToFragment(fragment);
			BillingAddressForm.MapFieldNamesToFragment(fragment);
			DeliveryAddressForm.MapFieldNamesToFragment(fragment);
		}

		public virtual void MapFieldValuesToFragment(Fragment fragment, bool needExtendedInfo)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.User.CustomerId.Value", CustomerId);
			fragment.AddVariable("Form.User.UserName.Value", UserName);
			fragment.AddCondition("Form.User.IsFacebookUser", IsFacebookUser);
			fragment.AddCondition("Form.UseBillingAddressAsDeliveryAddress.Checked", UseBillingAddressAsDeliveryAddress);

			CustomerInformationForm.MapFieldValuesToFragment(fragment);
			BillingAddressForm.MapFieldValuesToFragment(fragment);
			DeliveryAddressForm.MapFieldValuesToFragment(fragment);

			if (needExtendedInfo)
			{
				FacebookUtility.AddUserPictureParameters(fragment, FacebookUser);
			}
		}

		public virtual void MapToForm(ICustomer customer)
		{
			if (customer == null) throw new ArgumentNullException("customer");

			CustomerId = customer.Id.ToString(CultureInfo.InvariantCulture);
			UserName = customer.User != null ? customer.User.UserName : ((ILekmerCustomer)customer).FacebookUser.Name;

			var customerInformation = (ILekmerCustomerInformation) customer.CustomerInformation;
			if (customerInformation != null)
			{
				CustomerInformationForm.MapFromCustomerInformationToForm(customerInformation);

				var billingAddress = customerInformation.DefaultBillingAddress;
				if (billingAddress != null)
				{
					BillingAddressForm.MapFromAddressToForm(billingAddress);
				}

				var deliveryAddress = customerInformation.DefaultDeliveryAddress;
				if (deliveryAddress != null)
				{
					DeliveryAddressForm.MapFromAddressToForm(deliveryAddress);
				}

				UseBillingAddressAsDeliveryAddress = deliveryAddress == null || customerInformation.DefaultBillingAddressId == customerInformation.DefaultDeliveryAddressId;
			}
		}

		public virtual void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			CustomerId = form[CustomerIdFormName];
			UserName = form[UserNameFormName];
			OldPassword = form[OldPasswordFormName];
			NewPassword = form[NewPasswordFormName];
			NewPasswordVerify = form[NewPasswordVerifyFormName];

			CustomerInformationForm.MapFromRequestToForm();
			BillingAddressForm.MapFromRequestToForm();
			DeliveryAddressForm.MapFromRequestToForm();

			bool useBillingAddressAsDeliveryAddress;
			Boolean.TryParse(form[UseBillingAddressAsDeliveryAddressFormName], out useBillingAddressAsDeliveryAddress);
			UseBillingAddressAsDeliveryAddress = useBillingAddressAsDeliveryAddress;
		}

		public virtual void MapToCustomer(ICustomer customer)
		{
			if (IsChangeUsernameActionPost)
			{
				customer.User.UserName = UserName;
			}
			else if (IsChangePasswordActionPost)
			{
				customer.User.ClearTextPassword = NewPassword;
			}
			else if (IsUpdateCustomerInformationActionPost)
			{
				MapToCustomerInformation(customer);
				MapToCustomerAddress(customer);
			}
		}
		protected virtual void MapToCustomerInformation(ICustomer customer)
		{
			if (customer.CustomerInformation == null)
			{
				var customerInformationService = IoC.Resolve<ICustomerInformationService>();
				customer.CustomerInformation = customerInformationService.Create(UserContext.Current);
			}

			CustomerInformationForm.MapFromFormToCustomerInformation(customer.CustomerInformation);
		}
		protected virtual void MapToCustomerAddress(ICustomer customer)
		{
			var customerInformation = customer.CustomerInformation;
			customerInformation.DefaultBillingAddress = MapToCustomerAddress(customerInformation.DefaultBillingAddress, BillingAddressForm);

			if (UseBillingAddressAsDeliveryAddress)
			{
				customerInformation.DefaultDeliveryAddress = customerInformation.DefaultBillingAddress;
				customerInformation.DefaultDeliveryAddressId = customerInformation.DefaultBillingAddressId;
			}
			else
			{
				if (customerInformation.DefaultDeliveryAddress != null && customerInformation.DefaultDeliveryAddress.Id == customerInformation.DefaultBillingAddress.Id)
				{
					customerInformation.DefaultDeliveryAddress = null;
				}
				customerInformation.DefaultDeliveryAddress = MapToCustomerAddress(customerInformation.DefaultDeliveryAddress, DeliveryAddressForm);
			}
		}
		protected virtual IAddress MapToCustomerAddress(IAddress address, AddressForm addressForm)
		{
			if (address == null)
			{
				address = AddressService.Create(UserContext.Current);
				address.CountryId = Channel.Current.Country.Id;
			}
			addressForm.MapFromFormToAddress(address);
			return address;
		}

		public virtual ValidationResult ValidateUserName(int customerId)
		{
			var validationResult = CustomerValidationUtil.ValidateUserName(UserName, "CustomerDetail");
			if (validationResult.IsValid && UserNameExists(UserName, customerId))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.CustomerDetail.Validation.UserNameAlreadyExists"));
			}
			return validationResult;
		}
		protected virtual bool UserNameExists(string userName, int customerId)
		{
			var customer = CustomerService.GetByUserName(UserContext.Current, userName);
			return customer != null && customer.Id != customerId;
		}

		public virtual ValidationResult ValidatePassword(ICustomer customer)
		{
			var validationResult = new ValidationResult();
			ValidateOldPassword(validationResult, customer);
			ValidateNewPassword(validationResult);
			return validationResult;
		}
		protected virtual void ValidateOldPassword(ValidationResult validationResult, ICustomer customer)
		{
			if (OldPassword.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.CustomerDetail.Validation.OldPasswordNotProvided"));
			}
			else
			{
				bool oldPasswordIsValid = UserService.Validate(UserContext.Current, customer.User, OldPassword);
				if (!oldPasswordIsValid)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.CustomerDetail.Validation.InvalidOldPassword"));
				}
			}
		}
		protected virtual void ValidateNewPassword(ValidationResult validationResult)
		{
			validationResult.Errors.Append(CustomerValidationUtil.ValidatePassword(NewPassword, NewPasswordVerify, "CustomerDetail").Errors);
		}

		public virtual ValidationResult ValidateCustomerInformation()
		{
			var validationResult = new ValidationResult();
			validationResult.Errors.Append(CustomerInformationForm.CivicNumber.IsNullOrTrimmedEmpty()
				? CustomerInformationForm.Validate().Errors
				: CustomerInformationForm.Validate(true).Errors);
			validationResult.Errors.Append(((LekmerAddressForm)BillingAddressForm).CustomerDetailAddressValidate().Errors);
			if (!UseBillingAddressAsDeliveryAddress)
			{
				validationResult.Errors.Append(((LekmerAddressForm)DeliveryAddressForm).CustomerDetailAddressValidate().Errors);
			}
			return validationResult;
		}
		
	}
}