using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Edit customer information control.
	/// </summary>
	public class BlockCustomerDetailControl : BlockControlBase
	{
		private readonly ICustomerService _customerService;
		private readonly ICustomerSession _customerSession;

		private CustomerDetailForm _customerDetailForm;
		private CustomerDetailForm CustomerDetailForm
		{
			get
			{
				return _customerDetailForm ?? (_customerDetailForm = new CustomerDetailForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		private bool WasUsernameChangedSuccessfully { get; set; }
		private bool WasPasswordChangedSuccessfully { get; set; }
		private bool WasCustomerInformationUpdatedSuccessfully { get; set; }

		public BlockCustomerDetailControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICustomerService customerService,
			ICustomerSession customerSession)
			: base(templateFactory, blockService)
		{
			_customerService = customerService;
			_customerSession = customerSession;
		}

		protected override BlockContent RenderCore()
		{
			if (!_customerSession.IsSignedIn)
			{
				return new BlockContent();
			}

			var signedInCustomer = _customerSession.SignedInCustomer;
			CustomerDetailForm.FacebookUser = ((ILekmerCustomer) signedInCustomer).FacebookUser;

			var validationResult = new ValidationResult();
			if (CustomerDetailForm.IsFormPostBack)
			{
				CustomerDetailForm.MapFromRequest();

				if (CustomerDetailForm.IsChangeUsernameActionPost)
				{
					validationResult = CustomerDetailForm.ValidateUserName(signedInCustomer.Id);
					if (validationResult.IsValid)
					{
						UpdateCustomer(signedInCustomer);
						WasUsernameChangedSuccessfully = true;
					}
				}
				else if (CustomerDetailForm.IsChangePasswordActionPost)
				{
					validationResult = CustomerDetailForm.ValidatePassword(signedInCustomer);
					if (validationResult.IsValid)
					{
						UpdateCustomer(signedInCustomer);
						WasPasswordChangedSuccessfully = true;
					}
				}
				else if (CustomerDetailForm.IsUpdateCustomerInformationActionPost)
				{
					validationResult = CustomerDetailForm.ValidateCustomerInformation();
					if (validationResult.IsValid)
					{
						UpdateCustomer(signedInCustomer);
						WasCustomerInformationUpdatedSuccessfully = true;
					}
				}
			}
			else
			{
				CustomerDetailForm.MapToForm(signedInCustomer);
			}

			return RenderCustomerDetailForm(validationResult);
		}

		protected virtual BlockContent RenderCustomerDetailForm(ValidationResult validationResult)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			CustomerDetailForm.MapFieldNamesToFragment(fragmentContent);
			CustomerDetailForm.MapFieldValuesToFragment(fragmentContent, true);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			RenderUpdateSucceedNotification(fragmentContent, validationResult);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}
		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			CustomerDetailForm.MapFieldNamesToFragment(fragment);
			CustomerDetailForm.MapFieldValuesToFragment(fragment, false);
			return fragment.Render();
		}
		protected virtual void RenderUpdateSucceedNotification(Fragment fragmentContent, ValidationResult validationResult)
		{
			fragmentContent.AddCondition("WasUsernameChangedSuccessfully", WasUsernameChangedSuccessfully);
			fragmentContent.AddVariable("UsernameChangedSuccessfully", WasUsernameChangedSuccessfully ? AliasHelper.GetAliasValue("Customer.CustomerDetail.UsernameChangedSuccessfully") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasPasswordChangedSuccessfully", WasPasswordChangedSuccessfully);
			fragmentContent.AddVariable("PasswordChangedSuccessfully", WasPasswordChangedSuccessfully ? AliasHelper.GetAliasValue("Customer.CustomerDetail.PasswordChangedSuccessfully") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasCustomerInformationUpdatedSuccessfully", WasCustomerInformationUpdatedSuccessfully);
			fragmentContent.AddVariable("CustomerInformationUpdatedSuccessfully", WasCustomerInformationUpdatedSuccessfully ? AliasHelper.GetAliasValue("Customer.CustomerDetail.CustomerInformationUpdatedSuccessfully") : string.Empty, VariableEncoding.None);
		}

		protected virtual void UpdateCustomer(ICustomer signedInCustomer)
		{
			var customer = _customerService.GetById(UserContext.Current, signedInCustomer.Id);
			CustomerDetailForm.MapToCustomer(customer);
			_customerService.Save(UserContext.Current, customer);
			_customerSession.SignedInCustomer = customer;
		}
	}
}