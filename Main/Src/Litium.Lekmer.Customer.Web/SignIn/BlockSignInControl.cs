﻿using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Sign in customer control.
	/// </summary>
	public class BlockSignInControl : BlockControlBase
	{
		private readonly ICustomerSession _customerSession;
		private readonly IMyAccountSetting _setting;
		private readonly ILekmerCustomerService _customerService;

		private FacebookUtility _facebookUtility;
		protected FacebookUtility FacebookUtility
		{
			get
			{
				return _facebookUtility ?? (_facebookUtility = new FacebookUtility(_setting));
			}
		}

		private SignInForm _signInForm;
		protected SignInForm SignInForm
		{
			get
			{
				return _signInForm ?? (_signInForm = new SignInForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}


		public BlockSignInControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICustomerSession customerSession,
			IMyAccountSetting setting,
			ICustomerService customerService)
			: base(templateFactory, blockService)
		{
			_customerSession = customerSession;
			_setting = setting;
			_customerService = (ILekmerCustomerService) customerService;
		}

		protected override BlockContent RenderCore()
		{
			var validationResult = new ValidationResult();
			if (SignInForm.IsFormPostBack)
			{
				if (SignInForm.SignInActionPost)
				{
					SignInForm.MapFromRequest();
					ICustomer customer;
					validationResult = SignInForm.Validate(out customer);
					if (validationResult.IsValid && customer != null)
					{
						_customerSession.SignedInCustomer = customer;
						return new BlockContent();
					}
				}
				else if (SignInForm.FacebookSignInActionPost)
				{
					return FacebookLogin();
				}
			}

			return RenderSignInForm(validationResult);
		}

		protected virtual BlockContent RenderSignInForm(ValidationResult validationResult)
		{
			var fragmentContent = Template.GetFragment("Content");
			SignInForm.MapFieldNamesToFragment(fragmentContent);
			SignInForm.MapFieldValuesToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", RenderValidationErrors(validationResult), VariableEncoding.None);
			fragmentContent.AddVariable("AppId", _setting.FacebookAppId, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			SignInForm.MapFieldNamesToFragment(fragment);
			SignInForm.MapFieldValuesToFragment(fragment);
			return fragment.Render();
		}

		private string RenderValidationErrors(ValidationResult validationResult)
		{
			if (validationResult == null || validationResult.IsValid) 
				return null;

			var validationControl = new ValidationControl(validationResult.Errors);
			return validationControl.Render();
		}

		protected virtual BlockContent FacebookLogin()
		{
			string userProfile;
			var validationResult = FacebookUtility.ValidateCookies(out userProfile);
			if (!validationResult.IsValid)
			{
				return RenderSignInForm(validationResult);
			}

			string id;
			string email;
			string name;
			string firstName;
			string lastName;
			string gender;
			validationResult = FacebookUtility.GetUserParameters(userProfile, out id, out email, out name, out firstName, out lastName, out gender);
			if (!validationResult.IsValid)
			{
				return RenderSignInForm(validationResult);
			}

			ProcessCustomer(id, email, name, firstName, lastName, gender, ref validationResult);
			if (!validationResult.IsValid)
			{
				return RenderSignInForm(validationResult);
			}

			return new BlockContent();
		}

		protected virtual ValidationResult ProcessCustomer(string id, string email, string name, string firstName, string lastName, string gender, ref ValidationResult validationResult)
		{
			var customer = (ILekmerCustomer) _customerService.GetByFacebookId(UserContext.Current, id);
			if (customer == null || customer.FacebookUser == null)
			{
				var hasInformation = !firstName.IsNullOrTrimmedEmpty() && !lastName.IsNullOrTrimmedEmpty() && !gender.IsNullOrTrimmedEmpty() && !email.IsNullOrTrimmedEmpty();

				customer = _customerService.Create(UserContext.Current, hasInformation, false, true);
				customer.FacebookUser.FacebookId = id;
				customer.FacebookUser.Name = name;
				customer.FacebookUser.Email = email;
				customer.CustomerStatusId = (int) CustomerStatusInfo.Online;
				if (hasInformation)
				{
					var customerInfo = customer.CustomerInformation;
					customerInfo.FirstName = firstName;
					customerInfo.LastName = lastName;
					customerInfo.Email = email;
					((ILekmerCustomerInformation) customerInfo).GenderTypeId = FacebookUtility.GetGenderTypeId(gender.ToLower());
				}

				_customerService.Save(UserContext.Current, customer);
				_customerSession.SignedInCustomer = customer;
				if (!string.IsNullOrEmpty(email))
				{
					new RegisterMessenger().Send(_customerSession.SignedInCustomer);
				}
			}
			else if (customer.CustomerStatusId != 0)
			{
				var customerStatus = (CustomerStatusInfo) customer.CustomerStatusId;
				if (customerStatus == CustomerStatusInfo.Offline || customerStatus == CustomerStatusInfo.Blocked)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.SignIn.Validation.CustomerIsBlockedOrOffline"));
				}
			}
			else
			{
				_customerSession.SignedInCustomer = customer;
			}

			return validationResult;
		}
	}
}
