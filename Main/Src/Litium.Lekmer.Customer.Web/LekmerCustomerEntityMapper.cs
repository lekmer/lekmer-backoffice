﻿using System.Globalization;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerCustomerEntityMapper : LekmerCustomerEntityMapper<ICustomer>
	{
		public LekmerCustomerEntityMapper(IFormatter formatter, ICountryService countryService, IMyAccountSetting setting)
			: base(formatter, countryService, setting)
		{
		}
	}

	public class LekmerCustomerEntityMapper<T> : CustomerEntityMapper<T>
		where T : class, ICustomer
	{
		private readonly IMyAccountSetting _setting;

		private FacebookUtility _facebookUtility;
		protected FacebookUtility FacebookUtility
		{
			get
			{
				return _facebookUtility ?? (_facebookUtility = new FacebookUtility(_setting));
			}
		}

		public LekmerCustomerEntityMapper(IFormatter formatter, ICountryService countryService, IMyAccountSetting setting)
			: base(formatter, countryService)
		{
			_setting = setting;
		}

		public override void AddEntityVariables(Fragment fragment, T item)
		{
			fragment.AddVariable("CustomerInformation.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("CustomerInformation.ErpId", item.ErpId);

			var user = item.User;
			var facebookUser = ((ILekmerCustomer) item).FacebookUser;
			var hasUser = user != null || facebookUser != null;
			fragment.AddCondition("Customer.HasUser", hasUser);
			if (hasUser)
			{
				string userName;
				string createdDate;
				if (user != null)
				{
					userName = user.UserName;
					createdDate = Formatter.FormatDateTime(Channel.Current, user.CreatedDate);
				}
				else
				{
					userName = facebookUser.Name;
					createdDate = Formatter.FormatDateTime(Channel.Current, facebookUser.CreatedDate);
				}
				fragment.AddVariable("User.UserName", userName);
				fragment.AddVariable("User.CreatedDate", createdDate);
				FacebookUtility.AddUserPictureParameters(fragment, facebookUser);
			}

			ICustomerInformation customerInformation = item.CustomerInformation;
			fragment.AddCondition("Customer.HasCustomerInformation", customerInformation != null);
			if (customerInformation == null) return;

			fragment.AddVariable("CustomerInformation.FirstName", customerInformation.FirstName);
			fragment.AddVariable("CustomerInformation.LastName", customerInformation.LastName);
			fragment.AddVariable("CustomerInformation.CivicNumber", customerInformation.CivicNumber);
			fragment.AddVariable("CustomerInformation.PhoneNumber", customerInformation.PhoneNumber);
			fragment.AddVariable("CustomerInformation.CellPhoneNumber", customerInformation.CellPhoneNumber);
			fragment.AddVariable("CustomerInformation.Email", customerInformation.Email);
			fragment.AddVariable("CustomerInformation.CreatedDate", Formatter.FormatDateTime(Channel.Current, customerInformation.CreatedDate));
			fragment.AddCondition("CustomerInformation.HasDeliveryAddress", customerInformation.DefaultDeliveryAddress != null);
			if (customerInformation.DefaultDeliveryAddress != null)
			{
				AddAddressVariables(fragment, "CustomerInformation.DeliveryAddress", customerInformation.DefaultDeliveryAddress);
			}
			fragment.AddCondition("CustomerInformation.HasBillingAddress", customerInformation.DefaultBillingAddress != null);
			if (customerInformation.DefaultBillingAddress != null)
			{
				AddAddressVariables(fragment, "CustomerInformation.BillingAddress", customerInformation.DefaultBillingAddress);
			}
			int? deliveryAddressId = customerInformation.DefaultDeliveryAddressId;
			int? billingAddressId = customerInformation.DefaultBillingAddressId;
			bool flag = deliveryAddressId.GetValueOrDefault() == billingAddressId.GetValueOrDefault() && deliveryAddressId.HasValue == billingAddressId.HasValue;
			fragment.AddCondition("CustomerInformation.DeliveryAddressIsEqualToBillingAddress", flag);
		}

		protected override void AddAddressVariables(Fragment fragment, string prefix, IAddress address)
		{
			base.AddAddressVariables(fragment, prefix, address);

			var lekmerAddress = address as ILekmerAddress;
			if (lekmerAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerAddress.HouseExtension);
			}
		}
	}
}