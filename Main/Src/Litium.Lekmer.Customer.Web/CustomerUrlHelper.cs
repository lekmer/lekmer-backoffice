using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Customer.Web
{
	public static class CustomerUrlHelper
	{
		public static string GetPageUrl(string pageCommonName)
		{
			var contentNodeService = IoC.Resolve<IContentNodeService>();
			var item = contentNodeService.GetTreeItemByCommonName(UserContext.Current, pageCommonName);
			if (item == null || string.IsNullOrEmpty(item.Url))
			{
				throw new ApplicationException("Url to '" + pageCommonName + "' could not be found.");
			}
			return item.Url;
		}
	}
}