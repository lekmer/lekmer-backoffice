﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Sign up customer control.
	/// </summary>
	public class BlockRegisterControl : BlockControlBase
	{
		private readonly ICustomerSession _customerSession;
		private readonly ICustomerService _customerService;
		private readonly ILekmerOrderService _orderService;
		private readonly IUserService _userService;

		private RegisterForm _registerForm;
		private RegisterForm RegisterForm
		{
			get
			{
				return _registerForm ?? (_registerForm = new RegisterForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		private bool WasCofirmationMailResent { get; set; }


		public BlockRegisterControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICustomerSession customerSession,
			ICustomerService customerService,
			IOrderService orderService,
			IUserService userService)
			: base(templateFactory, blockService)
		{
			_customerSession = customerSession;
			_customerService = customerService;
			_orderService = (ILekmerOrderService) orderService;
			_userService = userService;
		}

		protected override BlockContent RenderCore()
		{
			var validationResult = new ValidationResult();
			if (RegisterForm.IsFormPostBack)
			{
				RegisterForm.MapFromRequest();
				validationResult = RegisterForm.Validate();
				if (validationResult.IsValid)
				{
					if (RegisterForm.IsStepOneActionPost)
					{
						AccessDeniedToSignedIn();
						RegisterForm.Step = RegisterStep.StepTwo;
					}
					else if (RegisterForm.IsStepTwoActionPost)
					{
						AccessDeniedToSignedIn();
						RegisterForm.Step = RegisterStep.StepThree;

						Collection<IOrderFull> oldOrders;
						var customer = RegisterCustomer(out oldOrders);
						_customerSession.SignedInCustomer = customer;
						SendConfirmation();

						ReassignOldOrdersToCustomer(oldOrders);
					}
					else if (RegisterForm.IsStepThreeActionPost)
					{
						_customerSession.SignOut();
						Response.Redirect(CustomerUrlHelper.GetPageUrl("SignIn"));
					}
					else if (RegisterForm.IsStepThreeResendConfirmPost)
					{
						AccessDeniedToNotSignedIn();

						var id = SendConfirmation();
						WasCofirmationMailResent = !id.Equals(Guid.Empty);
					}
				}
			}
			else
			{
				AccessDeniedToSignedIn();
				RegisterForm.SetDefaultValues();
			}

			return RenderRegisterForm(validationResult);
		}

		protected virtual BlockContent RenderRegisterForm(ValidationResult validationResult)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			var fragmentContent = Template.GetFragment("Content");
			RegisterForm.MapFieldNamesToFragment(fragmentContent);
			RegisterForm.MapFieldValuesToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			fragmentContent.AddVariable("CofirmationMailResentSuccessfully", WasCofirmationMailResent ? AliasHelper.GetAliasValue("Customer.Register.CofirmationMailResentSuccessfully") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasCofirmationMailResent", WasCofirmationMailResent);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			RegisterForm.MapFieldNamesToFragment(fragment);
			RegisterForm.MapFieldValuesToFragment(fragment);
			return fragment.Render();
		}

		protected virtual void AccessDeniedToSignedIn()
		{
			if (_customerSession.IsSignedIn)
			{
				Response.Redirect(CustomerUrlHelper.GetPageUrl("MyPages"));
			}
		}

		protected virtual void AccessDeniedToNotSignedIn()
		{
			if (!_customerSession.IsSignedIn)
			{
				Response.Redirect(CustomerUrlHelper.GetPageUrl("SignIn"));
			}
		}

		protected virtual Guid SendConfirmation()
		{
			return new RegisterMessenger().Send(_customerSession.SignedInCustomer);
		}

		protected virtual ICustomer RegisterCustomer(out Collection<IOrderFull> oldOrders)
		{
			ICustomer customer = null;

			bool isOldCustomerRegistered = false;

			oldOrders = _orderService.GetOrdersByEmail(UserContext.Current, RegisterForm.UserName);
			if (oldOrders.Count > 0)
			{
				for (var i = oldOrders.Count - 1; i >= 0; i--)
				{
					var oldOrder = oldOrders[i];
					if (oldOrder == null || oldOrder.Customer == null) continue;

					customer = oldOrder.Customer;
					break;
				}

				if (customer != null)
				{
					customer.User = _userService.Create(UserContext.Current);
					RegisterForm.MapToCustomer(customer);
					customer.User.Id = customer.Id;
					_userService.Save(UserContext.Current, customer.User);

					isOldCustomerRegistered = true;
				}
			}

			if (!isOldCustomerRegistered)
			{
				customer = _customerService.Create(UserContext.Current, false, true);
				RegisterForm.MapToCustomer(customer);
				_customerService.Save(UserContext.Current, customer);
			}

			return customer;
		}

		protected virtual void ReassignOldOrdersToCustomer(Collection<IOrderFull> oldOrders)
		{
			foreach (var oldOrder in oldOrders)
			{
				oldOrder.CustomerId = _customerSession.SignedInCustomer.User.Id;
				_orderService.Save(UserContext.Current, oldOrder);
			}
		}
	}
}