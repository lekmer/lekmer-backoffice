﻿using System;
using System.Collections.Specialized;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class RegisterForm : ControlBase
	{
		private readonly string _registerPostUrl;

		public virtual string PostUrl
		{
			get { return _registerPostUrl; }
		}
		public virtual string PostModeValue
		{
			get { return "register"; }
		}
		public virtual string StepFormName
		{
			get { return "register-step"; }
		}
		public virtual string UserNameFormName
		{
			get { return "register-username"; }
		}
		public virtual string PasswordFormName
		{
			get { return "register-password"; }
		}
		public virtual string PasswordVerifyFormName
		{
			get { return "register-passwordverify"; }
		}
		public virtual string AcceptTermsFormName
		{
			get { return "register-accept-terms"; }
		}
		public virtual string RegisterStep1ActionName
		{
			get { return "register-step1-action"; }
		}
		public virtual string RegisterStep2ActionName
		{
			get { return "register-step2-action"; }
		}
		public virtual string RegisterStep3ActionName
		{
			get { return "register-step3-action"; }
		}
		public virtual string RegisterStep3ResendConfirmName
		{
			get { return "register-step3-resend-confirm"; }
		}
		public virtual RegisterStep Step { get; set; }
		public virtual string UserName { get; set; }
		public virtual string Password { get; set; }
		public virtual string PasswordVerify { get; set; }
		public virtual string AcceptTermsFormValue
		{
			get { return "agree"; }
		}
		public virtual bool AcceptTerms { get; set; }
		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}
		public virtual bool IsStepOneActionPost
		{
			get { return Request.Form[RegisterStep1ActionName] != null; }
		}
		public virtual bool IsStepTwoActionPost
		{
			get { return Request.Form[RegisterStep2ActionName] != null; }
		}
		public virtual bool IsStepThreeActionPost
		{
			get { return Request.Form[RegisterStep3ActionName] != null; }
		}
		public virtual bool IsStepThreeResendConfirmPost
		{
			get { return Request.Form[RegisterStep3ResendConfirmName] != null; }
		}


		public RegisterForm(string registerPostUrl)
		{
			_registerPostUrl = registerPostUrl;
		}


		public virtual void SetDefaultValues()
		{
			Step = RegisterStep.StepOne;
		}

		/// <summary>
		/// Adds field name variables to a certain fragment.
		/// </summary>
		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Step.Name", StepFormName);
			fragment.AddVariable("Form.UserName.Name", UserNameFormName);
			fragment.AddVariable("Form.Password.Name", PasswordFormName);
			fragment.AddVariable("Form.PasswordVerify.Name", PasswordVerifyFormName);
			fragment.AddVariable("Form.AcceptTerms.Name", AcceptTermsFormName);
			fragment.AddVariable("Form.Step1.Action.Name", RegisterStep1ActionName);
			fragment.AddVariable("Form.Step2.Action.Name", RegisterStep2ActionName);
			fragment.AddVariable("Form.Step3.Action.Name", RegisterStep3ActionName);
			fragment.AddVariable("Form.Step3.ResendConfirm.Name", RegisterStep3ResendConfirmName);
		}

		/// <summary>
		/// Adds field value variables to a certain fragment.
		/// </summary>
		public virtual void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Step.Value", Step.ToString());
			fragment.AddVariable("Form.UserName.Value", UserName);
			fragment.AddVariable("Form.Password.Value", Password);
			fragment.AddVariable("Form.PasswordVerify.Value", PasswordVerify);
			fragment.AddVariable("Form.AcceptTerms.Value", AcceptTermsFormValue);
			fragment.AddCondition("Form.AcceptTerms.Checked", AcceptTerms);
			fragment.AddCondition("Form.Step.IsFirst", Step == RegisterStep.StepOne);
			fragment.AddCondition("Form.Step.IsSecond", Step == RegisterStep.StepTwo);
			fragment.AddCondition("Form.Step.IsThird", Step == RegisterStep.StepThree);
		}

		/// <summary>
		/// Map fields from http request form.
		/// </summary>
		public virtual void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			Step = (RegisterStep) Enum.Parse(typeof(RegisterStep), form[StepFormName]);
			UserName = form[UserNameFormName];
			Password = form[PasswordFormName];
			PasswordVerify = form[PasswordVerifyFormName];
			var accept = Request.Form[AcceptTermsFormName];
			AcceptTerms = accept.HasValue() && (accept.ToLower() == "on" || accept.ToLower() == AcceptTermsFormValue);
		}

		/// <summary>
		/// Maps the form values to a customer.
		/// </summary>
		public virtual void MapToCustomer(ICustomer customer)
		{
			if (!Validate().IsValid) throw new ValidationException("Couldn't validate the user.");

			customer.User.UserName = UserName;
			customer.User.ClearTextPassword = Password;
			customer.CustomerStatusId = (int) CustomerStatusInfo.Online;
		}

		/// <summary>
		/// Validates the form.
		/// </summary>
		public virtual ValidationResult Validate()
		{
			var validationResult = new ValidationResult();

			switch (Step)
			{
				case RegisterStep.StepOne:
					ValidateUserName(validationResult);
					break;
				case RegisterStep.StepTwo:
					ValidateUserName(validationResult);
					ValidatePassword(validationResult);
					ValidateAcceptTerms(validationResult);
					break;
			}

			return validationResult;
		}

		/// <summary>
		/// Validates the user name.
		/// </summary>
		protected virtual void ValidateUserName(ValidationResult validationResult)
		{
			validationResult.Errors.Append(CustomerValidationUtil.ValidateUserName(UserName, "Register").Errors);
			if (validationResult.IsValid && UserNameExists(UserName))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.Register.Validation.UserNameAlreadyExists"));
			}
		}

		/// <summary>
		/// Validates the password.
		/// </summary>
		protected virtual void ValidatePassword(ValidationResult validationResult)
		{
			validationResult.Errors.Append(CustomerValidationUtil.ValidatePassword(Password, PasswordVerify, "Register").Errors);
		}

		/// <summary>
		/// Validates accept terms.
		/// </summary>
		protected virtual void ValidateAcceptTerms(ValidationResult validationResult)
		{
			if (!AcceptTerms)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.Register.Validation.TermsNotAccepted"));
			}
		}

		/// <summary>
		/// Checks if the user name exists.
		/// </summary>
		/// <param name="userName">The use name to check.</param>
		/// <returns>True if the user name exists, false if not.</returns>
		protected virtual bool UserNameExists(string userName)
		{
			var customerService = IoC.Resolve<ICustomerService>();
			return customerService.GetByUserName(UserContext.Current, userName) != null;
		}
	}
}
