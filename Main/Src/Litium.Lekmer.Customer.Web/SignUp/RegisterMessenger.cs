﻿using System;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class RegisterMessenger : MessengerBase<ICustomer>
	{
		protected RegisterMailTemplate Template { get; set; }

		protected override Message CreateMessage(ICustomer messageArgs)
		{
			if (messageArgs == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			var user = messageArgs.User;
			var fbUser = ((ILekmerCustomer)messageArgs).FacebookUser;
			if (user == null && fbUser == null)
			{
				throw new ArgumentException("Empty User");
			}

			string email = user != null ? user.UserName : fbUser.Email;
			if (email.IsNullOrTrimmedEmpty())
			{
				throw new ArgumentException("Empty Email/Username");
			}

			Template = new RegisterMailTemplate(UserContext.Current.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(user, fbUser),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Address = email,
				Name = email
			});
			return message;
		}

		private string RenderMessage(IUser user, IFacebookUser fbUser)
		{
			var isUserExists = user != null;
			var isFacebookUserExists = fbUser != null;
			var userName = isUserExists ? user.UserName : fbUser.Email;
			var createdDate = isUserExists ? user.CreatedDate : fbUser.CreatedDate;

			Fragment htmlFragment = Template.GetFragment("Content");
			htmlFragment.AddVariable("Customer.Username", userName);
			htmlFragment.AddVariable("Customer.CreatedDate", createdDate.ToString("yyyy-MM-dd HH:mm"));
			htmlFragment.AddCondition("Customer.IsFacebookUser", isFacebookUserExists);
			if (isFacebookUserExists)
			{
				htmlFragment.AddVariable("Customer.FacebookUsername", fbUser.Name);
			}

			htmlFragment.AddVariable("Customer.Channel", UserContext.Current.Channel.CommonName);
			htmlFragment.AddVariable("Customer.SignIn.Url", UrlHelper.ResolveUrlHttp(CustomerUrlHelper.GetPageUrl("SignIn")));
			htmlFragment.AddVariable("Customer.ForgotPassword.Url", UrlHelper.ResolveUrlHttp(CustomerUrlHelper.GetPageUrl("ForgotPassword")));
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		protected override string MessengerName
		{
			get { return "Register"; }
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}
