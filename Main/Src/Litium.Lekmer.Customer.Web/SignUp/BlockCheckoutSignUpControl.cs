﻿using System;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Checkout sign up control.
	/// </summary>
	public class BlockCheckoutSignUpControl : BlockControlBase
	{
		private readonly IMyAccountSetting _setting;
		private readonly ICustomerSession _customerSession;
		private readonly IOrderSession _orderSession;
		private readonly IUserService _userService;
		private readonly IFacebookUserService _facebookUserService;
		private readonly ILekmerCustomerService _customerService;
		private readonly ILekmerOrderService _orderService;
		private bool _isCustomerExists;
		private bool _isCustomerNew;
		private bool _isUser;
		private bool _isFacebookUser;
		private bool _isFacebookUserBlocked;

		private FacebookUtility _facebookUtility;
		protected FacebookUtility FacebookUtility
		{
			get
			{
				return _facebookUtility ?? (_facebookUtility = new FacebookUtility(_setting));
			}
		}

		private CheckoutSignUpForm _checkoutSignUpForm;
		protected CheckoutSignUpForm CheckoutSignUpForm
		{
			get
			{
				return _checkoutSignUpForm ?? (_checkoutSignUpForm = new CheckoutSignUpForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}


		public BlockCheckoutSignUpControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IMyAccountSetting setting,
			ICustomerSession customerSession,
			IOrderSession orderSession,
			IUserService userService,
			IFacebookUserService facebookUserService,
			ICustomerService customerService,
			IOrderService orderService)
			: base(templateFactory, blockService)
		{
			_setting = setting;
			_customerSession = customerSession;
			_orderSession = orderSession;
			_userService = userService;
			_facebookUserService = facebookUserService;
			_customerService = (ILekmerCustomerService) customerService;
			_orderService = (ILekmerOrderService) orderService;
		}

		protected override BlockContent RenderCore()
		{
			// return empty block if orderSession does not contain order or there are no order with Id from orderSession.
			IOrderFull order = FindOrder();
			if (order == null)
			{
				return new BlockContent();
			}

			CheckoutSignUpForm.MapFromOrder(order);

			var validationResult = new ValidationResult();
			if (CheckoutSignUpForm.IsFormPostBack)
			{
				if (CheckoutSignUpForm.CheckoutSignUpActionPost)
				{
					validationResult = CheckoutSignUpForm.Validate();
					if (validationResult.IsValid)
					{
						_isUser = true;

						var customer = _customerService.GetByUserName(UserContext.Current, CheckoutSignUpForm.UserName);
						if (customer != null && customer.User != null)
						{
							// User already exists.
							_isCustomerExists = true;
						}
						else
						{
							// Create user for order customer.
							customer = RegisterCustomer(order);
							new RegisterMessenger().Send(customer);
							new ForgotPasswordMessenger().Send(customer);

							_isCustomerNew = true;
						}

						ReassignOldOrdersToCustomer(customer.User.Id);
					}
				}
				else if (CheckoutSignUpForm.FacebookCheckoutSignUpActionPost)
				{
					FacebookLogin(order, out validationResult);
				}
			}

			return RenderCheckoutSignUpForm(validationResult);
		}

		protected virtual BlockContent RenderCheckoutSignUpForm(ValidationResult validationResult)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			var fragmentContent = Template.GetFragment("Content");
			CheckoutSignUpForm.MapFieldNamesToFragment(fragmentContent);
			CheckoutSignUpForm.MapFieldValuesToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			fragmentContent.AddVariable("AppId", _setting.FacebookAppId, VariableEncoding.None);
			fragmentContent.AddCondition("IsCustomerExists", _isCustomerExists);
			fragmentContent.AddCondition("IsCustomerNew", _isCustomerNew);
			fragmentContent.AddCondition("IsUser", _isUser);
			fragmentContent.AddCondition("IsFacebookUser", _isFacebookUser);
			fragmentContent.AddCondition("IsFacebookUserBlocked", _isFacebookUserBlocked);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			CheckoutSignUpForm.MapFieldNamesToFragment(fragment);
			CheckoutSignUpForm.MapFieldValuesToFragment(fragment);
			return fragment.Render();
		}

		protected virtual void FacebookLogin(IOrderFull order, out ValidationResult validationResult)
		{
			string userProfile;
			validationResult = FacebookUtility.ValidateCookies(out userProfile);
			if (!validationResult.IsValid)
			{
				return;
			}

			string id;
			string email;
			string name;
			string firstName;
			string lastName;
			string gender;
			validationResult = FacebookUtility.GetUserParameters(userProfile, out id, out email, out name, out firstName, out lastName, out gender);
			if (!validationResult.IsValid)
			{
				return;
			}

			ProcessFacebookUser(order, id, email, name);
		}

		protected virtual void ProcessFacebookUser(IOrderFull order, string id, string email, string name)
		{
			_isFacebookUser = true;

			var customer = (ILekmerCustomer)_customerService.GetByFacebookId(UserContext.Current, id);
			if (customer != null && customer.FacebookUser != null)
			{
				// Already exists
				if (customer.CustomerStatusId != 0)
				{
					var customerStatus = (CustomerStatusInfo) customer.CustomerStatusId;
					_isFacebookUserBlocked = customerStatus == CustomerStatusInfo.Offline || customerStatus == CustomerStatusInfo.Blocked;
				}
				else
				{
					_customerSession.SignedInCustomer = customer;
				}

				_isCustomerExists = true;
			}
			else
			{
				// Create user for order customer.
				customer = RegisterFacebookCustomer(order, id, email, name);
				new RegisterMessenger().Send(customer);

				_customerSession.SignedInCustomer = customer;
				_isCustomerNew = true;
			}

			ReassignOldOrdersToCustomer(customer.FacebookUser.CustomerId);
		}

		protected virtual IOrderFull FindOrder()
		{
			IOrderFull order = null;

			int? orderId = _orderSession.OrderId;
			if (orderId.HasValue)
			{
				order = _orderService.GetFullById(UserContext.Current, orderId.Value);
			}

			return order;
		}

		protected virtual ICustomer RegisterCustomer(IOrderFull order)
		{
			var customer = order.Customer;
			customer.User = _userService.Create(UserContext.Current);
			customer.User.Id = customer.Id;
			customer.User.UserName = CheckoutSignUpForm.UserName;
			customer.User.ClearTextPassword = Guid.NewGuid().ToString();
			customer.CustomerStatusId = (int)CustomerStatusInfo.Online;
			_userService.Save(UserContext.Current, customer.User);
			return customer;
		}
		protected virtual ILekmerCustomer RegisterFacebookCustomer(IOrderFull order, string id, string email, string name)
		{
			var customer = (ILekmerCustomer) order.Customer;
			customer.FacebookUser = _facebookUserService.Create(UserContext.Current);
			customer.FacebookUser.FacebookId = id;
			customer.FacebookUser.Name = name;
			customer.FacebookUser.Email = email;
			customer.FacebookUser.CustomerId = customer.Id;
			customer.FacebookUser.CustomerRegistryId = customer.CustomerRegistryId;
			customer.CustomerStatusId = (int)CustomerStatusInfo.Online;
			_facebookUserService.Save(UserContext.Current, customer.FacebookUser);
			return customer;
		}

		protected virtual void ReassignOldOrdersToCustomer(int customerId)
		{
			var oldOrders = _orderService.GetOrdersByEmail(UserContext.Current, CheckoutSignUpForm.UserName);
			foreach (var oldOrder in oldOrders)
			{
				oldOrder.CustomerId = customerId;
				_orderService.Save(UserContext.Current, oldOrder);
			}
		}
	}
}
