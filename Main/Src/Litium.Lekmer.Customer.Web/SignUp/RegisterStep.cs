﻿namespace Litium.Lekmer.Customer.Web
{
	public enum RegisterStep
	{
		StepOne = 1,
		StepTwo = 2,
		StepThree = 3
	}
}
