﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class AdChannelService : IAdChannelService
	{
		protected AdChannelRepository Repository { get; private set; }

		public AdChannelService(AdChannelRepository adChannelRepository)
		{
			Repository = adChannelRepository;
		}

		public virtual Collection<IAdChannel> GetAllByAd(int adId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByAd(adId);
		}
	}
}