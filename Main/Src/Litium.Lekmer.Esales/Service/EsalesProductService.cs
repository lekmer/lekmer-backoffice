﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public class EsalesProductService : IEsalesProductService
	{
		private Dictionary<int, ICategoryTree> _categoryTrees;  // channel_id -> category_tree
		private Dictionary<int, Dictionary<int, IBrand>> _brands; // brand_id -> channel_id -> brand
		private Dictionary<int, ICampaign> _campaigns; // campaign_id -> campaign

		private IProductTagHash _productTagHash;

		protected ILekmerProductService LekmerProductService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected ITagService TagService { get; private set; }
		protected IProductTagService ProductTagService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected ICampaignSecureService CampaignSecureService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }

		public EsalesProductService(IBrandService brandService, ICategoryService categoryService, ILekmerProductService lekmerProductService,
			ITagGroupService tagGroupService, ITagService tagService, IProductTagService productTagService, IProductSizeService productSizeService,
			ICampaignSecureService campaignSecureService, IEsalesProductKeyService esalesProductKeyService)
		{
			BrandService = brandService;
			CategoryService = categoryService;
			LekmerProductService = lekmerProductService;
			TagGroupService = tagGroupService;
			TagService = tagService;
			ProductTagService = productTagService;
			ProductSizeService = productSizeService;
			CampaignSecureService = campaignSecureService;
			EsalesProductKeyService = esalesProductKeyService;
		}


		public virtual IEsalesProductInfoList CreateNewEsalesProductInfoList()
		{
			return IoC.Resolve<IEsalesProductInfoList>();
		}


		public virtual IEsalesProductInfoList GetNextProductListPortion(IPageTracker pageTracker, IEnumerable<IUserContext> userContexts)
		{
			ProductIdCollection productIdCollection = LekmerProductService.GetIdAllWithoutAnyFilter(pageTracker.Current, pageTracker.PageSize, false);

			var productInfoList = IoC.Resolve<IEsalesProductInfoList>();
			productInfoList.TotalCount = productIdCollection.TotalCount;

			return GetProducts(userContexts, productInfoList, productIdCollection);
		}

		public virtual IEsalesProductInfoList GetNextProductListPortion(IEnumerable<IUserContext> userContexts, IEsalesProductInfoList productInfoList, ProductIdCollection productIdCollection)
		{
			productInfoList.TotalCount += productIdCollection.TotalCount;

			return GetProducts(userContexts, productInfoList, productIdCollection);
		}

		public virtual IEsalesTagInfoList GetTags(IEnumerable<IUserContext> userContexts)
		{
			PreloadAllTags(userContexts);

			var tagInfoList = IoC.Resolve<IEsalesTagInfoList>();

			Dictionary<int, Collection<ITag>> channelTags = _productTagHash.GetChannelTags();

			foreach (var userContext in userContexts)
			{
				if (channelTags.ContainsKey(userContext.Channel.Id))
				{
					tagInfoList.Add(userContext.Channel, channelTags[userContext.Channel.Id]);
				}
			}

			SupplementWithKeyInfo(tagInfoList);

			return tagInfoList;
		}


		protected virtual IEsalesProductInfoList GetProducts(IEnumerable<IUserContext> userContexts, IEsalesProductInfoList productInfoList, ProductIdCollection productIdCollection)
		{
			productInfoList.AddOrReplace(productIdCollection);

			foreach (IUserContext userContext in userContexts)
			{
				ProductCollection productCollection = LekmerProductService.PopulateViewProductsWithoutStatusFilter(userContext, productIdCollection, false);

				productInfoList.Add(userContext.Channel, productCollection);
			}

			SupplementWithBrandInfo(userContexts, productInfoList);
			SupplementWithCategoryInfo(userContexts, productInfoList);
			SupplementWithTagsInfo(userContexts, productInfoList);
			SupplementWithSizeInfo(productIdCollection, productInfoList);
			SupplementWithCampaignInfo(productInfoList);
			SupplementWithKeyInfo(productInfoList);

			return productInfoList;
		}


		protected virtual void SupplementWithBrandInfo(IEnumerable<IUserContext> userContexts, IEsalesProductInfoList esalesProductInfoList)
		{
			PreloadAllBrands(userContexts);

			Collection<IEsalesProductInfo> productInfoCollection = esalesProductInfoList.GetEsalesProductInfoCollection();

			foreach (IEsalesProductInfo esalesProductInfo in productInfoCollection)
			{
				foreach (IEsalesChannelProductInfo channelProductInfo in esalesProductInfo.ChannelProductInfoCollection)
				{
					if (channelProductInfo.Product.BrandId.HasValue)
					{
						channelProductInfo.Product.Brand = FindBrand(channelProductInfo.Channel.Id, channelProductInfo.Product.BrandId.Value);
					}
				}
			}
		}

		protected virtual void SupplementWithCategoryInfo(IEnumerable<IUserContext> userContexts, IEsalesProductInfoList esalesProductInfoList)
		{
			PreloadAllCategoryTrees(userContexts);

			Collection<IEsalesProductInfo> productInfoCollection = esalesProductInfoList.GetEsalesProductInfoCollection();

			foreach (IEsalesProductInfo esalesProductInfo in productInfoCollection)
			{
				foreach (IEsalesChannelProductInfo channelProductInfo in esalesProductInfo.ChannelProductInfoCollection)
				{
					ICategoryTree categoryTree = FindCategoryTree(channelProductInfo.Channel.Id);
					if (categoryTree != null)
					{
						ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(channelProductInfo.Product.CategoryId);
						if (categoryTreeItem != null)
						{
							ICategory category = categoryTreeItem.Category;
							channelProductInfo.Category = category;

							categoryTreeItem = categoryTreeItem.Parent;
							if (categoryTreeItem != null)
							{
								ICategory parentCategory = categoryTreeItem.Category;
								channelProductInfo.ParentCategory = parentCategory;

								categoryTreeItem = categoryTreeItem.Parent;
								if (categoryTreeItem != null)
								{
									ICategory mainCategory = categoryTreeItem.Category;
									channelProductInfo.MainCategory = mainCategory;
								}
							}
						}
					}
				}
			}
		}

		protected virtual void SupplementWithTagsInfo(IEnumerable<IUserContext> userContexts, IEsalesProductInfoList esalesProductInfoList)
		{
			PreloadAllTags(userContexts);

			Collection<IEsalesProductInfo> productInfoCollection = esalesProductInfoList.GetEsalesProductInfoCollection();

			foreach (IEsalesProductInfo esalesProductInfo in productInfoCollection)
			{
				foreach (IEsalesChannelProductInfo channelProductInfo in esalesProductInfo.ChannelProductInfoCollection)
				{
					channelProductInfo.Product.TagGroups = _productTagHash.GetProductTags(channelProductInfo.Channel.Id, channelProductInfo.Product.Id);
				}
			}
		}

		protected virtual void SupplementWithSizeInfo(ProductIdCollection productIds, IEsalesProductInfoList esalesProductInfoList)
		{
			Collection<IProductSize> productSizes = ProductSizeService.GetAllByProductIdList(productIds);
			Dictionary<int, Collection<IProductSize>> productSizesByProduct = productSizes.GroupBy(ps => ps.ProductId).ToDictionary(ps => ps.Key, ps => new Collection<IProductSize>(ps.ToArray()));

			Collection<IEsalesProductInfo> productInfoCollection = esalesProductInfoList.GetEsalesProductInfoCollection();

			foreach (IEsalesProductInfo esalesProductInfo in productInfoCollection)
			{
				if (productSizesByProduct.ContainsKey(esalesProductInfo.ProductId))
				{
					Collection<IProductSize> sizes = productSizesByProduct[esalesProductInfo.ProductId];

					foreach (IEsalesChannelProductInfo channelProductInfo in esalesProductInfo.ChannelProductInfoCollection)
					{
						channelProductInfo.Product.ProductSizes = sizes;
					}
				}
			}
		}

		protected virtual void SupplementWithCampaignInfo(IEsalesProductInfoList esalesProductInfoList)
		{
			PreloadAllCampaigns();

			Collection<IEsalesProductInfo> productInfoCollection = esalesProductInfoList.GetEsalesProductInfoCollection();

			foreach (IEsalesProductInfo esalesProductInfo in productInfoCollection)
			{
				foreach (IEsalesChannelProductInfo channelProductInfo in esalesProductInfo.ChannelProductInfoCollection)
				{
					var campaignInfo = (ILekmerProductCampaignInfo) channelProductInfo.Product.CampaignInfo;

					if (campaignInfo != null && campaignInfo.CampaignActionsApplied != null && campaignInfo.CampaignActionsApplied.Count > 0)
					{
						var campaignIds = campaignInfo.CampaignActionsApplied.GroupBy(a => a.ProductCampaignId).Select(c => c.First().ProductCampaignId);

						channelProductInfo.Campaigns = new Collection<ICampaign>();

						foreach (int campaignId in campaignIds)
						{
							if (_campaigns.ContainsKey(campaignId))
							{
								ICampaign campaign = _campaigns[campaignId];
								channelProductInfo.Campaigns.Add(campaign);
							}
						}
					}
				}
			}
		}

		protected virtual void SupplementWithKeyInfo(IEsalesProductInfoList esalesProductInfoList)
		{
			Collection<IEsalesProductInfo> productInfoCollection = esalesProductInfoList.GetEsalesProductInfoCollection();

			foreach (IEsalesProductInfo esalesProductInfo in productInfoCollection)
			{
				esalesProductInfo.EsalesKey = EsalesProductKeyService.ComposeProductKey(esalesProductInfo.ProductId);
			}
		}

		protected virtual void SupplementWithKeyInfo(IEsalesTagInfoList esalesTagInfoList)
		{
			Collection<IEsalesTagInfo> tagInfoCollection = esalesTagInfoList.GetEsalesTagInfoCollection();

			foreach (IEsalesTagInfo esalesTagInfo in tagInfoCollection)
			{
				esalesTagInfo.EsalesKey = EsalesProductKeyService.ComposeTagKey(esalesTagInfo.TagId);
			}
		}


		protected virtual void PreloadAllBrands(IEnumerable<IUserContext> userContexts)
		{
			if (_brands == null)
			{
				_brands = new Dictionary<int, Dictionary<int, IBrand>>();
			}
			else
			{
				return;
			}

			foreach (IUserContext userContext in userContexts)
			{
				BrandCollection brandCollection = BrandService.GetAll(userContext);

				foreach (IBrand brand in brandCollection)
				{
					if (!_brands.ContainsKey(brand.Id))
					{
						_brands[brand.Id] = new Dictionary<int, IBrand>();
					}

					Dictionary<int, IBrand> brandChannels = _brands[brand.Id];

					if (!brandChannels.ContainsKey(userContext.Channel.Id))
					{
						brandChannels[userContext.Channel.Id] = brand;
					}
				}
			}
		}

		protected virtual void PreloadAllCategoryTrees(IEnumerable<IUserContext> userContexts)
		{
			if (_categoryTrees == null)
			{
				_categoryTrees = new Dictionary<int, ICategoryTree>();
			}
			else
			{
				return;
			}

			foreach (IUserContext userContext in userContexts)
			{
				_categoryTrees[userContext.Channel.Id] = CategoryService.GetAllAsTree(userContext);
			}
		}

		protected virtual void PreloadAllTags(IEnumerable<IUserContext> userContexts)
		{
			if (_productTagHash == null)
			{
				_productTagHash = IoC.Resolve<IProductTagHash>();
			}
			else
			{
				return;
			}

			Collection<IProductTag> productTags = ProductTagService.GetAll();
			_productTagHash.Add(productTags);

			foreach (IUserContext userContext in userContexts)
			{
				Collection<ITagGroup> tagGroups = TagGroupService.GetAll(userContext);
				_productTagHash.Add(userContext.Channel.Id, tagGroups);
			}
		}

		protected virtual void PreloadAllCampaigns()
		{
			if (_campaigns != null)
			{
				return;
			}

			Collection<ICampaign> productCampaigns = CampaignSecureService.GetAllProductCampaigns();

			_campaigns = productCampaigns.ToDictionary(c => c.Id);
		}


		protected virtual IBrand FindBrand(int channelId, int brandId)
		{
			if (_brands.ContainsKey(brandId))
			{
				if (_brands[brandId].ContainsKey(channelId))
				{
					return _brands[brandId][channelId];
				}
			}

			return null;
		}

		protected virtual ICategoryTree FindCategoryTree(int channelId)
		{
			if (_categoryTrees.ContainsKey(channelId))
			{
				return _categoryTrees[channelId];
			}

			return null;
		}
	}
}