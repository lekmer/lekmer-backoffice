﻿using System.Globalization;

namespace Litium.Lekmer.Esales
{
	public class EsalesProductKeyService : IEsalesProductKeyService
	{
		public string ComposeProductKey(int productId)
		{
			return productId.ToString(CultureInfo.InvariantCulture);
		}

		public string ComposeTagKey(int tagId)
		{
			return "t-" + tagId.ToString(CultureInfo.InvariantCulture);
		}

		public int ParseProductKey(string key)
		{
			int id;
			int.TryParse(key, out id);
			return id;
		}

		public int ParseTagKey(string key)
		{
			int id;
			int.TryParse(key.Substring(2), out id);
			return id;
		}
	}
}