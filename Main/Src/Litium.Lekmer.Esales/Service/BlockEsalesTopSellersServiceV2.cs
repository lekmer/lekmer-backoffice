﻿using System;
using System.Linq;
using Litium.Lekmer.Esales.Repository;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesTopSellersServiceV2 : IBlockEsalesTopSellersServiceV2
	{
		protected BlockEsalesTopSellersRepositoryV2 Repository { get; private set; }
		protected IBlockEsalesTopSellersCategoryServiceV2 BlockEsalesTopSellersCategoryServiceV2 { get; private set; }
		protected IBlockEsalesTopSellersProductServiceV2 BlockEsalesTopSellersProductServiceV2 { get; private set; }
		protected IEsalesGeneralServiceV2 EsalesGeneralServiceV2;

		public BlockEsalesTopSellersServiceV2(
			BlockEsalesTopSellersRepositoryV2 repository,
			IBlockEsalesTopSellersCategoryServiceV2 blockEsalesTopSellersCategoryServiceV2,
			IBlockEsalesTopSellersProductServiceV2 blockEsalesTopSellersProductServiceV2,
			IEsalesGeneralServiceV2 esalesGeneralServiceV2)
		{
			Repository = repository;
			BlockEsalesTopSellersCategoryServiceV2 = blockEsalesTopSellersCategoryServiceV2;
			BlockEsalesTopSellersProductServiceV2 = blockEsalesTopSellersProductServiceV2;
			EsalesGeneralServiceV2 = esalesGeneralServiceV2;
		}

		public virtual IBlockEsalesTopSellersV2 GetById(IUserContext context, int id)
		{
			return BlockEsalesTopSellersCacheV2.Instance.TryGetItem(
				new BlockEsalesTopSellersKeyV2(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockEsalesTopSellersV2 GetByIdCore(IUserContext context, int id)
		{
			var blockEsalesTopSellersV2 = Repository.GetById(id, context.Channel.Language.Id);
			blockEsalesTopSellersV2.Categories = BlockEsalesTopSellersCategoryServiceV2.GetAllByBlock(context, id);
			blockEsalesTopSellersV2.Products = BlockEsalesTopSellersProductServiceV2.GetAllByBlock(context, id);
			return blockEsalesTopSellersV2;
		}

		public virtual ProductCollection GetTopSellersFromEsalesResponse(IUserContext context, IBlockEsalesTopSellersV2 block, IPanelContent esalesGeneralResponse)
		{
			var blockProducts = block.Products;
			var panelName = block.PanelName;
			var esalesResponse = esalesGeneralResponse.FindPanel(panelName);

			IStartPagePanelSettingV2 panelSetting = new StartPagePanelSettingV2(context.Channel.CommonName);

			var topSellersCount = block.Setting.ActualColumnCount * block.Setting.ActualRowCount;
			var topSellersProducts = esalesResponse == null
				? new ProductCollection()
				: EsalesGeneralServiceV2.FindProducts(context, esalesResponse, panelSetting.TopSellersPanelName, topSellersCount);

			var esalesPredefinedProducts = new ProductCollection();
			if (blockProducts != null && blockProducts.Count > 0)
			{
				var predefinedProductsCount = Math.Min(blockProducts.Count, topSellersCount);
				esalesPredefinedProducts = esalesResponse == null
					? new ProductCollection()
					: EsalesGeneralServiceV2.FindProducts(context, esalesResponse, panelSetting.PredefinedProductListPanelName, predefinedProductsCount);
			}

			var products = new ProductCollection();
			if (topSellersProducts.Count > 0)
			{
				products = topSellersProducts;
				if (blockProducts != null)
				{
					foreach (var predefinedProduct in blockProducts)
					{
						var product = esalesPredefinedProducts.FirstOrDefault(p => p.Id == predefinedProduct.Product.Id);
						if (product == null) continue;
						products.Insert(predefinedProduct.Position - 1, product);
					}
				}
			}
			else if (esalesPredefinedProducts.Count > 0)
			{
				if (blockProducts != null)
				{
					foreach (var predefinedProduct in blockProducts)
					{
						var product = esalesPredefinedProducts.FirstOrDefault(p => p.Id == predefinedProduct.Product.Id);
						if (product == null) continue;
						products.Add(product);
					}
				}
			}

			return new ProductCollection(products.Take(topSellersCount));
		}
	}
}