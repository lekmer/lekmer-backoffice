﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Esales
{
	public class EsalesCartService : IEsalesCartService
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }

		public EsalesCartService(IEsalesService esalesService, IProductService productService, IEsalesProductKeyService esalesProductKeyService)
		{
			EsalesService = esalesService;
			ProductService = productService;
			EsalesProductKeyService = esalesProductKeyService;
		}

		public ProductCollection FindRecommend(IUserContext context, string panelPath, string fallbackPanelPath, int productId, ProductIdCollection cartProductIds, int itemsToReturn)
		{
			if (context == null) throw new ArgumentNullException("context");
			EsalesService.EnsureNotNull();

			var recommendProducts = FindRecommendProducts(context, panelPath, cartProductIds, itemsToReturn);
			var recommendationsCount = recommendProducts.Count;
			if (recommendationsCount < itemsToReturn)
			{
				var excludeProductIds = recommendProducts.Select(p => p.Id).ToList();
				excludeProductIds.AddRange(cartProductIds);
				var fallbackProducts = FindFallbackProducts(context, fallbackPanelPath, productId, excludeProductIds, itemsToReturn - recommendationsCount);
				recommendProducts.AddRange(fallbackProducts);
			}
			return recommendProducts;
		}
		protected virtual ProductCollection FindRecommendProducts(IUserContext context, string panelPath, ProductIdCollection cartProductIds, int itemsToReturn)
		{
			var esalesRequest = IoC.Resolve<ICartEsalesRequest>();
			esalesRequest.PanelPath = panelPath;
			esalesRequest.CartProductIds = Convert.ToStringIdentifierList(cartProductIds).TrimEnd(',');
			esalesRequest.Paging = new Paging(1, itemsToReturn);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			return FindProducts(context, esalesResponse, itemsToReturn);
		}
		protected virtual ProductCollection FindFallbackProducts(IUserContext context, string panelPath, int productId, List<int> excludeProductIds, int itemsToReturn)
		{
			var esalesRequest = IoC.Resolve<ICartEsalesRequest>();
			esalesRequest.PanelPath = panelPath;
			if (productId > 0)
			{
				esalesRequest.ProductKey = productId.ToString(CultureInfo.InvariantCulture);
			}
			esalesRequest.ExcludeProductIds = excludeProductIds;
			esalesRequest.Paging = new Paging(1, itemsToReturn);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			return FindProducts(context, esalesResponse, itemsToReturn);
		}

		protected virtual ProductCollection FindProducts(IUserContext context, IEsalesResponse esalesResponse, int itemsToReturn)
		{
			Collection<IResult> productsResults = esalesResponse.FindResults(EsalesResultType.Products);

			if (productsResults == null)
			{
				return new ProductCollection();
			}

			var productHitsToReturn = new Collection<IProductHitInfo>();
			var productKeysDictionary = new Dictionary<string, string>();

			IEnumerable<IProductHitInfo> allProductHits = productsResults.Cast<IProductHits>().SelectMany(ph => ph.ProductsInfo);
			foreach (IProductHitInfo productHitInfo in allProductHits)
			{
				if (productHitsToReturn.Count >= itemsToReturn)
				{
					break;
				}

				if (!productKeysDictionary.ContainsKey(productHitInfo.Key))
				{
					productHitsToReturn.Add(productHitInfo);
					productKeysDictionary[productHitInfo.Key] = productHitInfo.Key;
				}
			}

			if (productHitsToReturn.Count > 0)
			{
				var productIds = new ProductIdCollection(productHitsToReturn.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)))
					{
						TotalCount = productHitsToReturn.Count
					};
				
				var tickets = productHitsToReturn.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

				var products = ProductService.PopulateProducts(context, productIds);
				foreach (ILekmerProduct product in products)
				{
					product.EsalesTicket = tickets[product.Id];
				}

				return products;
			}

			return new ProductCollection();
		}
	}
}
