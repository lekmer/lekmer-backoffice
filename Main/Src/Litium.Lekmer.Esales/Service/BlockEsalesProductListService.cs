﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesProductListService : IBlockEsalesProductListService
	{
		protected BlockEsalesProductListRepository Repository { get; private set; }

		public BlockEsalesProductListService(BlockEsalesProductListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockEsalesProductList GetById(IUserContext context, int id)
		{
			return BlockEsalesProductListCache.Instance.TryGetItem(
				new BlockEsalesProductListKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockEsalesProductList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}
