﻿using Litium.Lekmer.Esales.Cache;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesSettingService : IBlockEsalesSettingService
	{
		protected BlockEsalesSettingRepository Repository { get; private set; }

		public BlockEsalesSettingService(BlockEsalesSettingRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockEsalesSetting GetByBlock(int blockId)
		{
			return BlockEsalesSettingCache.Instance.TryGetItem(
				new BlockEsalesSettingKey(blockId),
				() => Repository.GetByBlock(blockId));
		}
	}
}
