﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class EsalesRecommendationTypeDataMapper : DataMapperBase<IEsalesRecommendationType>
	{
		private int _id;
		private int _commonName;
		private int _title;

		public EsalesRecommendationTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_id = OrdinalOf("EsalesRecommendationType.Id");
			_commonName = OrdinalOf("EsalesRecommendationType.CommonName");
			_title = OrdinalOf("EsalesRecommendationType.Title");
		}

		protected override IEsalesRecommendationType Create()
		{
			var recommendationType = IoC.Resolve<IEsalesRecommendationType>();

			recommendationType.Id = MapValue<int>(_id);
			recommendationType.CommonName = MapValue<string>(_commonName);
			recommendationType.Title = MapValue<string>(_title);

			recommendationType.Status = BusinessObjectStatus.Untouched;

			return recommendationType;
		}
	}
}