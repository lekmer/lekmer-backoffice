﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class EsalesModelComponentDataMapper : DataMapperBase<IEsalesModelComponent>
	{
		public EsalesModelComponentDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IEsalesModelComponent Create()
		{
			var esalesModelComponent = IoC.Resolve<IEsalesModelComponent>();

			esalesModelComponent.Id = MapValue<int>("EsalesModelComponent.ModelComponentId");
			esalesModelComponent.ModelId = MapValue<int>("EsalesModelComponent.ModelId");
			esalesModelComponent.Title = MapValue<string>("EsalesModelComponent.Title");
			esalesModelComponent.CommonName = MapValue<string>("EsalesModelComponent.CommonName");
			esalesModelComponent.Ordinal = MapValue<int>("EsalesModelComponent.Ordinal");

			return esalesModelComponent;
		}
	}
}
