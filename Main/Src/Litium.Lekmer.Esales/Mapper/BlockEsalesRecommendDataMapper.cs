﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesRecommendDataMapper : DataMapperBase<IBlockEsalesRecommend>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockEsalesRecommendDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockEsalesRecommend Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockEsalesRecommend = IoC.Resolve<IBlockEsalesRecommend>();

			block.ConvertTo(blockEsalesRecommend);

			blockEsalesRecommend.RecommendationType = MapNullableValue<int?>("BlockEsalesRecommend.EsalesRecommendationTypeId");
			blockEsalesRecommend.PanelPath = MapNullableValue<string>("BlockEsalesRecommend.PanelPath");
			blockEsalesRecommend.FallbackPanelPath = MapNullableValue<string>("BlockEsalesRecommend.FallbackPanelPath");
			blockEsalesRecommend.Setting = _blockSettingDataMapper.MapRow();

			blockEsalesRecommend.SetUntouched();

			return blockEsalesRecommend;
		}
	}
}