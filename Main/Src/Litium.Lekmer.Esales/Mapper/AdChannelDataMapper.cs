﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class AdChannelDataMapper : DataMapperBase<IAdChannel>
	{
		public AdChannelDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IAdChannel Create()
		{
			var adChannel = IoC.Resolve<IAdChannel>();

			adChannel.Id = MapValue<int>("AdChannel.AdChannelId");
			adChannel.AdId = MapValue<int>("AdChannel.AdId");
			adChannel.RegistryId = MapValue<int>("AdChannel.RegistryId");
			adChannel.Title = MapNullableValue<string>("AdChannel.Title");
			adChannel.ShowOnSite = MapValue<bool>("AdChannel.ShowOnSite");

			adChannel.LandingPageUrl = MapNullableValue<string>("AdChannel.LandingPageUrl");
			adChannel.LandingPageId = MapNullableValue<int?>("AdChannel.LandingPageId");

			adChannel.ImageUrl = MapNullableValue<string>("AdChannel.ImageUrl");
			adChannel.ImageId = MapNullableValue<int?>("AdChannel.ImageId");

			adChannel.SetUntouched();

			return adChannel;
		}
	}
}