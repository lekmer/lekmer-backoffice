﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesSettingDataMapper : DataMapperBase<IBlockEsalesSetting>
	{
		public BlockEsalesSettingDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBlockEsalesSetting Create()
		{
			var blockEsalesSetting = IoC.Resolve<IBlockEsalesSetting>();

			blockEsalesSetting.BlockId = MapValue<int>("BlockEsalesSetting.BlockId");
			blockEsalesSetting.EsalesModelComponentId = MapNullableValue<int?>("BlockEsalesSetting.EsalesModelComponentId");

			blockEsalesSetting.SetUntouched();

			return blockEsalesSetting;
		}
	}
}
