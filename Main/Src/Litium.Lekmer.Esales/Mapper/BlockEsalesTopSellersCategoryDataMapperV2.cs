﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesTopSellersCategoryDataMapperV2 : DataMapperBase<IBlockEsalesTopSellersCategoryV2>
	{
		private DataMapperBase<ICategory> _categoryDataMapper;

		public BlockEsalesTopSellersCategoryDataMapperV2(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_categoryDataMapper = DataMapperResolver.Resolve<ICategory>(DataReader);
		}

		protected override IBlockEsalesTopSellersCategoryV2 Create()
		{
			var blockCategory = IoC.Resolve<IBlockEsalesTopSellersCategoryV2>();
			blockCategory.BlockId = MapValue<int>("BlockEsalesTopSellersV2Category.BlockId");
			blockCategory.IncludeSubcategories = MapValue<bool>("BlockEsalesTopSellersV2Category.IncludeSubcategories");
			blockCategory.Category = _categoryDataMapper.MapRow();
			blockCategory.SetUntouched();
			return blockCategory;
		}
	}
}