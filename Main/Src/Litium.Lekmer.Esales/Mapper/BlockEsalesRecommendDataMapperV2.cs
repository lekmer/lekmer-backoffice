﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesRecommendDataMapperV2 : DataMapperBase<IBlockEsalesRecommendV2>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockEsalesRecommendDataMapperV2(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockEsalesRecommendV2 Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockEsalesRecommendV2 = IoC.Resolve<IBlockEsalesRecommendV2>();
			block.ConvertTo(blockEsalesRecommendV2);
			blockEsalesRecommendV2.PanelName = MapNullableValue<string>("BlockEsalesRecommendV2.PanelName");
			blockEsalesRecommendV2.WindowLastEsalesValue = MapValue<int>("BlockEsalesRecommendV2.WindowLastEsalesValue");
			blockEsalesRecommendV2.Setting = _blockSettingDataMapper.MapRow();
			blockEsalesRecommendV2.SetUntouched();
			return blockEsalesRecommendV2;
		}
	}
}