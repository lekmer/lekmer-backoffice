﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public class EsalesRegistrySecureService : IEsalesRegistrySecureService
	{
		protected EsalesRegistryRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public EsalesRegistrySecureService(EsalesRegistryRepository esalesRegistryRepository, IAccessValidator accessValidator)
		{
			Repository = esalesRegistryRepository;
			AccessValidator = accessValidator;
		}

		public virtual Collection<IEsalesRegistry> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}
	}
}