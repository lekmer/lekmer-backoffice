using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Esales.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesProductListSecureService : IBlockEsalesProductListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockEsalesProductListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IBlockEsalesSettingSecureService BlockEsalesSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockEsalesProductListSecureService(
			IAccessValidator accessValidator,
			BlockEsalesProductListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IBlockEsalesSettingSecureService blockEsalesSettingSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			BlockEsalesSettingSecureService = blockEsalesSettingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockEsalesProductList Create()
		{
			if (AccessSecureService == null)
			{
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			}

			var blockEsalesProductList = IoC.Resolve<IBlockEsalesProductList>();

			blockEsalesProductList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockEsalesProductList.Setting = BlockSettingSecureService.Create();
			blockEsalesProductList.EsalesSetting = BlockEsalesSettingSecureService.Create();
			blockEsalesProductList.Status = BusinessObjectStatus.New;

			return blockEsalesProductList;
		}

		public virtual IBlockEsalesProductList GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockEsalesProductList blockEsalesProductList = Create();

			blockEsalesProductList.ContentNodeId = contentNodeId;
			blockEsalesProductList.ContentAreaId = contentAreaId;
			blockEsalesProductList.BlockTypeId = blockTypeId;
			blockEsalesProductList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockEsalesProductList.Title = title;
			blockEsalesProductList.TemplateId = null;
			blockEsalesProductList.Id = Save(systemUserFull, blockEsalesProductList);

			return blockEsalesProductList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockEsalesProductList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (BlockSecureService == null)
			{
				throw new InvalidOperationException("BlockSecureService must be set before calling Save.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				block.EsalesSetting.BlockId = block.Id;
				BlockEsalesSettingSecureService.Save(block.EsalesSetting);

				transactedOperation.Complete();
			}

			BlockEsalesProductListCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());
			
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				BlockEsalesSettingSecureService.Delete(blockId);
				transactedOperation.Complete();
			}

			BlockEsalesProductListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}
