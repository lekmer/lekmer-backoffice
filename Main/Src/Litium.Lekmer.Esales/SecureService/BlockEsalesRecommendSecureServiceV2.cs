using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesRecommendSecureServiceV2 : IBlockEsalesRecommendSecureServiceV2, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockEsalesRecommendRepositoryV2 Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockEsalesRecommendSecureServiceV2(
			IAccessValidator accessValidator,
			BlockEsalesRecommendRepositoryV2 repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockEsalesRecommendV2 Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockEsalesRecommendV2 = IoC.Resolve<IBlockEsalesRecommendV2>();

			blockEsalesRecommendV2.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockEsalesRecommendV2.Setting = BlockSettingSecureService.Create();
			blockEsalesRecommendV2.Status = BusinessObjectStatus.New;

			return blockEsalesRecommendV2;
		}

		public virtual IBlockEsalesRecommendV2 GetById(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetByIdSecure(blockId);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockEsalesRecommendV2 blockEsalesRecommendV2 = Create();

			blockEsalesRecommendV2.ContentNodeId = contentNodeId;
			blockEsalesRecommendV2.ContentAreaId = contentAreaId;
			blockEsalesRecommendV2.BlockTypeId = blockTypeId;
			blockEsalesRecommendV2.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockEsalesRecommendV2.Title = title;
			blockEsalesRecommendV2.TemplateId = null;
			blockEsalesRecommendV2.Id = Save(systemUserFull, blockEsalesRecommendV2);

			return blockEsalesRecommendV2;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockEsalesRecommendV2 block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			BlockEsalesRecommendCacheV2.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockEsalesRecommendCacheV2.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}