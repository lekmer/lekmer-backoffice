﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Esales
{
	public class AdFolderSecureService : IAdFolderSecureService
	{
		protected AdFolderRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAdSecureService AdSecureService { get; private set; }

		public AdFolderSecureService(AdFolderRepository adFolderRepository, IAccessValidator accessValidator, IAdSecureService adSecureService)
		{
			Repository = adFolderRepository;
			AccessValidator = accessValidator;
			AdSecureService = adSecureService;
		}

		public virtual IAdFolder Create()
		{
			var adFolder = IoC.Resolve<IAdFolder>();
			adFolder.Status = BusinessObjectStatus.New;
			return adFolder;
		}

		public virtual IAdFolder Save(ISystemUserFull systemUserFull, IAdFolder adFolder)
		{
			if (adFolder == null)
			{
				throw new ArgumentNullException("adFolder");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.CampaignAds);

			using (var transaction = new TransactedOperation())
			{
				adFolder.Id = Repository.Save(adFolder);

				transaction.Complete();
			}

			return adFolder;
		}

		public virtual bool TryDelete(ISystemUserFull systemUserFull, int adFolderId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			AdSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.CampaignAds);

			using (var transaction = new TransactedOperation())
			{
				if (!TryDeleteCore(adFolderId))
				{
					return false;
				}

				transaction.Complete();

				return true;
			}
		}

		public virtual IAdFolder GetById(int adFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(adFolderId);
		}

		public virtual Collection<IAdFolder> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}

		public virtual Collection<IAdFolder> GetAllByParent(int parentFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByParent(parentFolderId);
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			Repository.EnsureNotNull();

			return Repository.GetTree(selectedId);
		}


		protected virtual bool TryDeleteCore(int adFolderId)
		{
			if (AdSecureService.GetAllByFolder(adFolderId).Count > 0)
			{
				return false;
			}

			if (!TryDeleteChildren(adFolderId))
			{
				return false;
			}

			Repository.Delete(adFolderId);

			return true;
		}

		protected virtual bool TryDeleteChildren(int parentFolderId)
		{
			foreach (IAdFolder adFolder in Repository.GetAllByParent(parentFolderId))
			{
				if (!TryDeleteCore(adFolder.Id))
				{
					return false;
				}
			}

			return true;
		}
	}
}