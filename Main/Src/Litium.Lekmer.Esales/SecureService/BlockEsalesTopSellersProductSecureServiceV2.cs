using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesTopSellersProductSecureServiceV2 : IBlockEsalesTopSellersProductSecureServiceV2
	{
		protected BlockEsalesTopSellersProductRepositoryV2 Repository { get; private set; }

		public BlockEsalesTopSellersProductSecureServiceV2(BlockEsalesTopSellersProductRepositoryV2 repository)
		{
			Repository = repository;
		}

		public virtual Collection<IBlockEsalesTopSellersProductV2> GetAllByBlock(int blockId, int channelId)
		{
			return Repository.GetAllByBlockSecure(blockId, channelId);
		}

		public virtual void Save(int blockId, Collection<IBlockEsalesTopSellersProductV2> blockProducts)
		{
			if (blockProducts == null) throw new ArgumentNullException("blockProducts");

			Repository.DeleteAll(blockId);
			foreach (var blockProduct in blockProducts)
			{
				blockProduct.BlockId = blockId;
				Repository.Save(blockProduct);
			}
		}
	}
}