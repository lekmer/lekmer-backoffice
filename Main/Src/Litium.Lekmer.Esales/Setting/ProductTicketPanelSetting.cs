﻿namespace Litium.Lekmer.Esales.Setting
{
	public class ProductTicketPanelSetting : EsalesPanelSetting, IProductTicketPanelSetting
	{
		protected override string GroupName
		{
			get { return "ProductTicketPanel-" + ChannelCommonName; }
		}

		public ProductTicketPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string ProductTicketPanelName
		{
			get { return GetString(GroupName, "ProductTicketPanelName"); }
		}
	}
}
