﻿namespace Litium.Lekmer.Esales.Setting
{
	public class DidYouMeanPanelSetting : EsalesPanelSetting, IDidYouMeanPanelSetting
	{
		protected override string GroupName
		{
			get { return "DidYouMeanPanel-" + ChannelCommonName; }
		}

		public DidYouMeanPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string DidYouMeanPanelName
		{
			get { return GetString(GroupName, "DidYouMeanPanelName"); }
		}
	}
}
