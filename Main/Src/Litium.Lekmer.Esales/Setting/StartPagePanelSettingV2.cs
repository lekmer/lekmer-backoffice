﻿namespace Litium.Lekmer.Esales.Setting
{
	public class StartPagePanelSettingV2 : EsalesPanelSettingV2, IStartPagePanelSettingV2
	{
		protected override string GroupName
		{
			get { return "StartPagePanel-" + ChannelCommonName; }
		}

		public StartPagePanelSettingV2(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string TopSellersPanelName
		{
			get { return GetString(GroupName, "TopSellersPanelName"); }
		}
		public string PredefinedProductListPanelName
		{
			get { return GetString(GroupName, "PredefinedProductListPanelName"); }
		}
	}
}
