﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class EsalesRegistryRepository
	{
		protected virtual DataMapperBase<IEsalesRegistry> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IEsalesRegistry>(dataReader);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IEsalesRegistry> GetAll()
		{
			var dbSettings = new DatabaseSetting("EsalesRegistryRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pEsalesRegistryGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}