﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class AdChannelRepository
	{
		protected virtual DataMapperBase<IAdChannel> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IAdChannel>(dataReader);
		}

		public virtual int Save(IAdChannel adChannel)
		{
			if (adChannel == null)
			{
				throw new ArgumentNullException("adChannel");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdChannelId", adChannel.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("AdId", adChannel.AdId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RegistryId", adChannel.RegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", adChannel.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("ShowOnSite", adChannel.ShowOnSite, SqlDbType.Bit),
					ParameterHelper.CreateParameter("LandingPageUrl", adChannel.LandingPageUrl, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("LandingPageId", adChannel.LandingPageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ImageUrl", adChannel.ImageUrl, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("ImageId", adChannel.ImageId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdChannelRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[esales].[pAdChannelSave]", parameters, dbSettings);
		}

		public virtual void DeleteAllByAd(int adId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdId", adId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdChannelRepository.DeleteAllByAd");

			new DataHandler().ExecuteCommand("[esales].[pAdChannelDeleteAllByAd]", parameters, dbSettings);
		}

		public virtual IAdChannel GetById(int adChannelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdChannelId", adChannelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdChannelRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdChannelGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IAdChannel> GetAll()
		{
			var dbSettings = new DatabaseSetting("AdChannelRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdChannelGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IAdChannel> GetAllByAd(int adId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdId", adId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdChannelRepository.GetAllByAd");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdChannelGetAllByAd]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}