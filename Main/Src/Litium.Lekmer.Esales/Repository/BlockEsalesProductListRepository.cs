﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesProductListRepository
	{
		protected virtual DataMapperBase<IBlockEsalesProductList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesProductList>(dataReader);
		}

		public virtual IBlockEsalesProductList GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("BlockEsalesProductListRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesProductListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockEsalesProductList GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
			};

			var dbSetting = new DatabaseSetting("BlockEsalesProductListRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesProductListGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}
