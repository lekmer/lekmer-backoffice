﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Esales.Repository
{
	public class AdFolderRepository
	{
		protected virtual DataMapperBase<IAdFolder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IAdFolder>(dataReader);
		}

		protected virtual DataMapperBase<INode> CreateDataMapperNode(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INode>(dataReader);
		}

		public virtual int Save(IAdFolder adFolder)
		{
			if (adFolder == null)
			{
				throw new ArgumentNullException("adFolder");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdFolderId", adFolder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ParentFolderId", adFolder.ParentFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", adFolder.Title, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("AdFolderRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[esales].[pAdFolderSave]", parameters, dbSettings);
		}

		public virtual void Delete(int adFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdFolderId", adFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdFolderRepository.Delete");

			new DataHandler().ExecuteCommand("[esales].[pAdFolderDelete]", parameters, dbSettings);
		}

		public virtual IAdFolder GetById(int adFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdFolderId", adFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdFolderRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdFolderGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IAdFolder> GetAll()
		{
			var dbSettings = new DatabaseSetting("AdFolderRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdFolderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SelectedId", selectedId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdFolderRepository.GetTree");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdFolderGetTree]", parameters, dbSettings))
			{
				return CreateDataMapperNode(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IAdFolder> GetAllByParent(int parentId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ParentId", parentId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdFolderRepository.GetAllByParent");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdFolderGetAllByParent]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}