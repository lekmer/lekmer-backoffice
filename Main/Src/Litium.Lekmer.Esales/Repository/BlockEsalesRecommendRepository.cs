﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesRecommendRepository
	{
		protected virtual DataMapperBase<IBlockEsalesRecommend> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesRecommend>(dataReader);
		}


		public virtual void Save(IBlockEsalesRecommend blockEsalesRecommend)
		{
			if (blockEsalesRecommend == null)
			{
				throw new ArgumentNullException("blockEsalesRecommend");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockEsalesRecommend.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("RecommendationType", blockEsalesRecommend.RecommendationType, SqlDbType.Int),
					ParameterHelper.CreateParameter("PanelPath", blockEsalesRecommend.PanelPath, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("FallbackPanelPath", blockEsalesRecommend.FallbackPanelPath, SqlDbType.NVarChar)
				};

			var dbSetting = new DatabaseSetting("BlockEsalesRecommendRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockEsalesRecommendSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockEsalesRecommendRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockEsalesRecommendDelete]", parameters, dbSettings);
		}


		public virtual IBlockEsalesRecommend GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockEsalesRecommendRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockEsalesRecommendGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockEsalesRecommend GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockEsalesRecommendRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockEsalesRecommendGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}