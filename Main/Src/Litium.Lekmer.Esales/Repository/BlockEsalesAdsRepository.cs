﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesAdsRepository
	{
		protected virtual DataMapperBase<IBlockEsalesAds> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesAds>(dataReader);
		}


		public virtual void Save(IBlockEsalesAds blockEsalesAds)
		{
			if (blockEsalesAds == null)
			{
				throw new ArgumentNullException("blockEsalesAds");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockEsalesAds.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("PanelPath", blockEsalesAds.PanelPath, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Format", blockEsalesAds.Format, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Gender", blockEsalesAds.Gender, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("ProductCategory", blockEsalesAds.ProductCategory, SqlDbType.NVarChar)
				};

			var dbSetting = new DatabaseSetting("BlockEsalesAdsRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockEsalesAdsSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockEsalesAdsRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockEsalesAdsDelete]", parameters, dbSettings);
		}


		public virtual IBlockEsalesAds GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockEsalesAdsRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockEsalesAdsGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockEsalesAds GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockEsalesAdsRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockEsalesAdsGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}