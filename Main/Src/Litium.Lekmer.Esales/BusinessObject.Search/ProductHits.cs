using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class ProductHits : Result, IProductHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Products; }
		}

		public Collection<IProductHitInfo> ProductsInfo { get; set; }
	}
}