using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="ISearchFilterRequest"/> to build a query and execute it.
	/// </summary>
	[Serializable]
	public class SearchFilterRequest : SearchRequest, ISearchFilterRequest
	{
		public IFilterQuery FilterQuery { get; set; }
		public IChannel Channel { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = base.ComposeArguments();

			argMap[EsalesArguments.SearchFilter] = BuildFilter();
			argMap[EsalesArguments.SortBy] = BuildSortOptions();

			return argMap;
		}

		/// <summary>
		/// Compose set of filters
		/// </summary>
		protected virtual string BuildFilter()
		{
			var filter = new StringBuilder();

			if (FilterQuery != null)
			{
				AppendFilter(FilterQuery.BrandIds, EsalesXmlNodes.NodeBrandId, ref filter);
				AppendFilter(FilterQuery.Level1CategoryIds, EsalesXmlNodes.NodeMainCategoryId, ref filter);
				AppendFilter(FilterQuery.Level2CategoryIds, EsalesXmlNodes.NodeParentCategoryId, ref filter);
				AppendFilter(FilterQuery.Level3CategoryIds, EsalesXmlNodes.NodeCategoryId, ref filter);
				AppendFilter(FilterQuery.SizeIds, EsalesXmlNodes.NodeSizeId, ref filter);

				if (FilterQuery.AgeInterval != null)
				{
					int ageFrom = FilterQuery.AgeInterval.FromMonth != null ? FilterQuery.AgeInterval.FromMonth.Value : 0;
					int ageTo = FilterQuery.AgeInterval.ToMonth != null ? FilterQuery.AgeInterval.ToMonth.Value : 1000000;

					if (filter.Length > 0)
					{
						filter.Append(" AND ");
					}
					filter.Append("(");
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:['{1}','{2}']", EsalesXmlNodes.NodeAgeFromMonth, ageFrom, ageTo));
					filter.Append(")");

					filter.Append(" AND ");
					filter.Append("(");
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:['{1}','{2}']", EsalesXmlNodes.NodeAgeToMonth, ageFrom, ageTo));
					filter.Append(")");
				}

				if (FilterQuery.PriceInterval != null)
				{
					decimal priceFrom = FilterQuery.PriceInterval.From != null ? FilterQuery.PriceInterval.From.Value : 1;
					decimal priceTo = FilterQuery.PriceInterval.To != null ? FilterQuery.PriceInterval.To.Value : 1000000;

					if (filter.Length > 0)
					{
						filter.Append(" AND ");
					}
					filter.Append("(");
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:['{1}','{2}']", GetElementName(Channel, EsalesXmlNodes.NodeProductCurrentPrice), priceFrom, priceTo));
					filter.Append(")");
				}

				if (FilterQuery.GroupedTagIds != null && FilterQuery.GroupedTagIds.Count > 0)
				{
					foreach (IEnumerable<int> tagGroup in FilterQuery.GroupedTagIds)
					{
						var tags = new Collection<int>(tagGroup.ToList());
						AppendFilter(tags, EsalesXmlNodes.NodeTagId, ref filter);
					}
				}
			}

			return filter.ToString();
		}

		/// <summary>
		/// Compose set of sort options
		/// </summary>
		protected virtual string BuildSortOptions()
		{
			var sortOptions = new StringBuilder();
			sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", EsalesXmlNodes.NodeProductInStock, EsalesXmlNodes.ValueDescending));

			if (FilterQuery != null)
			{
				switch (FilterQuery.SortOption)
				{
					case SortOption.PriceAsc:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductCurrentPrice), EsalesXmlNodes.ValueAscending));
							break;
						}
					case SortOption.PriceDesc:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductCurrentPrice), EsalesXmlNodes.ValueDescending));
							break;
						}
					case SortOption.TitleAsc:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductTitle), EsalesXmlNodes.ValueAscending));
							break;
						}
					case SortOption.TitleDesc:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductTitle), EsalesXmlNodes.ValueDescending));
							break;
						}
					case SortOption.IsNewFrom:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", EsalesXmlNodes.NodeIsNewFrom, EsalesXmlNodes.ValueDescending));
							break;
						}
					case SortOption.DiscountPercentDesc:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductDiscountPercent), EsalesXmlNodes.ValueDescending));
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", EsalesXmlNodes.ValueRelevance, EsalesXmlNodes.ValueDescending));
							break;
						}
					default:
						{
							sortOptions.Append(",");
							sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", EsalesXmlNodes.ValueRelevance, EsalesXmlNodes.ValueDescending));
							break;
						}
				}
			}

			return sortOptions.ToString();
		}

		protected virtual string GetElementName(IChannel channel, string elementName)
		{
			return string.Concat(elementName, "_", channel.Id.ToString(CultureInfo.InvariantCulture));
		}

		protected virtual void AppendFilter(Collection<int> ids, string nodeName, ref StringBuilder filter)
		{
			if (ids != null && ids.Count > 0)
			{
				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");

				foreach (int id in ids)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", nodeName, id));
				}

				filter.Length -= 4;
				filter.Append(")");
			}
		}
	}
}