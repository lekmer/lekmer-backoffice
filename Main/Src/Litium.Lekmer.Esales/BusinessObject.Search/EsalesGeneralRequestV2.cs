using System;
using System.Collections.Generic;
using Apptus.ESales.Connector;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesGeneralRequestV2 : EsalesRequest, IEsalesGeneralRequestV2
	{
		private const string DefaultWindowFirst = "1";

		/// <summary>
		/// Additional request parameters.
		/// </summary>
		public Dictionary<string, string> Parameters { get; set; }

		/// <summary>
		/// Compose set of arguments
		/// </summary>
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();
			argMap[EsalesArguments.WindowFirst] = DefaultWindowFirst;
			foreach (var parameter in Parameters)
			{
				argMap[parameter.Key] = parameter.Value;
			}
			return argMap;
		}
	}
}