using System;
using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it on one or more indexes. 
	/// </summary>
	[Serializable]
	public abstract class EsalesRequest : IEsalesRequest
	{
		/// <summary>
		/// Esales panel path
		/// </summary>
		public string PanelPath { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		/// <returns></returns>
		public abstract Dictionary<string, string> ComposeArguments();
	}
}