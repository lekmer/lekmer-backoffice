using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class SuggestionInfo : ISuggestionInfo
	{
		public int Rank { get; set; }
		public double Relevance { get; set; }
		public string Text { get; set; }
		public string Ticket { get; set; }
	}
}