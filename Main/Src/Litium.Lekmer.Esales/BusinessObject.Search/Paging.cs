using System;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Paging information used by consumers of <see cref="EsalesRequest"/>'s, 
	/// (primarily <see cref="IEsalesService"/> implementations) to determine how large 
	/// (and which part) of a result set should be compiled and returned in the 
	/// <see cref="EsalesRequest"/>.
	/// </summary>
	[Serializable]
	public class Paging : IPaging
	{
		private readonly int _pageNumber;
		private readonly int _pageSize;

		private readonly int _firstNumber;
		private readonly int _lastNumber;

		/// <summary>
		/// Creates a new instance of <see cref="Paging"/>.
		/// </summary>
		/// <param name="pageNumber">Then number of the page. Starts from 1.</param>
		/// <param name="pageSize">The maxmimum number of items per page.</param>
		public Paging(int pageNumber, int pageSize)
		{
			if (pageNumber <= 0)
			{
				throw new ArgumentOutOfRangeException("pageNumber", "pageNumber must be greater than or equal to one.");
			}
			if (pageSize <= 0)
			{
				throw new ArgumentOutOfRangeException("pageSize", "pageSize must be greater than or equal to one.");
			}

			_pageNumber = pageNumber;
			_pageSize = pageSize;

			_firstNumber = (pageNumber - 1) * pageSize + 1;
			_lastNumber = _firstNumber + pageSize - 1;
		}

		/// <summary>
		/// The number of the page.
		/// </summary>
		public int PageNumber
		{
			get { return _pageNumber; }
		}

		/// <summary>
		/// The maxmimum number of items per page.
		/// </summary>
		public int PageSize
		{
			get { return _pageSize; }
		}

		/// <summary>
		/// The number of the first item in sequence, on specified page.
		/// </summary>
		public int FirstItemNumber
		{
			get { return _firstNumber; }
		}

		/// <summary>
		/// The number of the last item in sequence, on specified page.
		/// </summary>
		public int LastItemNumber
		{
			get { return _lastNumber; }
		}

		/// <summary>
		/// Creates a paging object with PageNumber = 1, PageSize = 16.
		/// </summary>
		public static Paging Default
		{
			get { return new Paging(1, 16); }
		}
	}
}