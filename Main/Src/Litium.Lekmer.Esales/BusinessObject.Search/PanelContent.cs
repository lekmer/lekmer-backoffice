using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class PanelContent : IPanelContent
	{
		public string Name { get; set; }
		public string Description { get; set; }

		public bool HasRezult { get; set; }
		public bool IsZone { get; set; }

		public Collection<IPanelContent> SubPanels { get; set; }

		public IResult Result { get; set; }

		public virtual IPanelContent FindPanel(string panelName)
		{
			if (Name == panelName)
			{
				return this;
			}

			if (SubPanels == null)
			{
				return null;
			}

			return SubPanels.Select(subPanel => subPanel.FindPanel(panelName)).FirstOrDefault(panel => panel != null);
		}

		public virtual IResult FindResult(EsalesResultType resultType)
		{
			if (!IsZone && HasRezult && Result.Type == resultType)
			{
				return Result;
			}

			if (SubPanels == null)
			{
				return null;
			}

			return SubPanels.Select(subPanel => subPanel.FindResult(resultType)).FirstOrDefault(result => result != null);
		}

		public virtual Collection<IResult> FindResults(EsalesResultType resultType)
		{
			if (!IsZone && HasRezult && Result.Type == resultType)
			{
				return new Collection<IResult> { Result };
			}

			if (SubPanels == null)
			{
				return null;
			}

			return new Collection<IResult>(SubPanels.SelectMany(subPanel => subPanel.FindResults(resultType)).Where(result => result != null).ToArray());
		}
	}
}