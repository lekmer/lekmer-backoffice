using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class AdHits : Result, IAdHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Ads;}
		}

		public Collection<IAdInfo> AdsInfo { get; set; }
	}
}