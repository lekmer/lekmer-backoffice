﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Apptus.ESales.Connector;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Esales
{
	public class CartEsalesRequest : EsalesRequest, ICartEsalesRequest
	{
		public string ProductKey { get; set; }
		public string CartProductIds { get; set; }
		public List<int> ExcludeProductIds { get; set; }
		public IPaging Paging { get; set; }

		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();

			if (ProductKey.HasValue())
			{
				argMap[EsalesArguments.ProductKey] = ProductKey;
			}

			if (CartProductIds.HasValue())
			{
				argMap[EsalesArguments.Cart] = CartProductIds;
			}

			if (ExcludeProductIds != null && ExcludeProductIds.Count > 0)
			{
				argMap[EsalesArguments.FilterExcludeProducts] = BuildFilter();
			}

			if (Paging != null)
			{
				argMap.Add(EsalesArguments.WindowFirst, Paging.FirstItemNumber.ToString(CultureInfo.InvariantCulture));
				argMap.Add(EsalesArguments.WindowLast, Paging.LastItemNumber.ToString(CultureInfo.InvariantCulture));
			}

			return argMap;
		}

		protected virtual string BuildFilter()
		{
			var filter = new StringBuilder();

			filter.Append(" NOT (");
			foreach (int excludeProductId in ExcludeProductIds)
			{
				filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeProductKey, excludeProductId));
			}

			filter.Length -= 4;
			filter.Append(")");

			return filter.ToString();
		}
	}
}
