using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class ValueInfo : IValueInfo
	{
		public int Count { get; set; }
		public string Text { get; set; }
		public string Ticket { get; set; }
	}
}