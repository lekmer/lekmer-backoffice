using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Apptus.ESales.Connector;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	[Serializable]
	public class RecommendEsalesRequest : EsalesRequest, IRecommendEsalesRequest
	{
		public virtual string ChannelId { get; set; }

		public virtual string CustomerKey { get; set; }

		public virtual string ProductKey { get; set; }

		public virtual string CartProductIds { get; set; }

		public virtual List<int> ExcludeProductIds { get; set; }

		public virtual List<int> IncludeCategoryIds { get; set; }

		public virtual List<int> IncludeParentCategoryIds { get; set; }

		public virtual List<int> IncludeMainCategoryIds { get; set; }

		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		public virtual IPaging Paging { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		/// <returns></returns>
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();

			argMap[EsalesArguments.Filter] = BuildFilter();

			if (CustomerKey.HasValue())
			{
				argMap[EsalesArguments.CustomerKey] = CustomerKey;
			}

			if (ProductKey.HasValue())
			{
				argMap[EsalesArguments.ProductKey] = ProductKey;
			}

			if (CartProductIds.HasValue())
			{
				argMap[EsalesArguments.Cart] = CartProductIds;
			}

			if (Paging != null)
			{
				argMap.Add(EsalesArguments.WindowFirst, Paging.FirstItemNumber.ToString(CultureInfo.InvariantCulture));
				argMap.Add(EsalesArguments.WindowLast, Paging.LastItemNumber.ToString(CultureInfo.InvariantCulture));
			}

			return argMap;
		}


		/// <summary>
		/// Compose set of filters
		/// </summary>
		/// <returns></returns>
		protected virtual string BuildFilter()
		{
			var filter = new StringBuilder();

			filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}'", EsalesXmlNodes.NodeAvailableInChannels, ChannelId));

			if (IncludeCategoryIds != null && IncludeCategoryIds.Count > 0)
			{
				filter.Append(" AND (");
				foreach (int includeCategoryId in IncludeCategoryIds)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeCategoryId, includeCategoryId));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			if (IncludeParentCategoryIds != null && IncludeParentCategoryIds.Count > 0)
			{
				filter.Append(" AND (");
				foreach (int includeParentCategoryIds in IncludeParentCategoryIds)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeParentCategoryId, includeParentCategoryIds));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			if (IncludeMainCategoryIds != null && IncludeMainCategoryIds.Count > 0)
			{
				filter.Append(" AND (");
				foreach (int includeMainCategoryId in IncludeMainCategoryIds)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeMainCategoryId, includeMainCategoryId));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			if (ExcludeProductIds != null && ExcludeProductIds.Count > 0)
			{
				filter.Append(" AND NOT (");
				foreach (int excludeProductId in ExcludeProductIds)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeProductKey, excludeProductId));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			return filter.ToString();
		}
	}
}