using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class CompletionHits : Result, ICompletionHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Completions;}
		}

		public Collection<ICompletionInfo> CompletionsInfo { get; set; }
	}
}