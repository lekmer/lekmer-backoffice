﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales.Connector;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.Esales.XmlFile;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Esales.Exporter
{
	public class EsalesProductInfoExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IEnumerable<IUserContext> UserContexts;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected IEsalesProductService EsalesProductService { get; private set; }
		protected IEsalesSetting EsalesSetting { get; private set; }
		protected IEsalesConnector EsalesConnector { get; private set; }

		public EsalesProductInfoExporter(ILekmerChannelService lekmerChannelService, IEsalesProductService esalesProductService, IEsalesSetting esalesSetting, IEsalesConnector esalesConnector)
		{
			LekmerChannelService = lekmerChannelService;
			EsalesProductService = esalesProductService;
			EsalesSetting = esalesSetting;
			EsalesConnector = esalesConnector;
		}

		public virtual void Execute()
		{
			_log.Info("Execution is started.");

			UserContexts = GetUserContextForAllChannels();

			ExecuteEsalesExport();
			ExecuteEsalesImport();

			_log.Info("Execution is done.");
		}

		public virtual void ExecuteEsalesExport()
		{
			_log.Info("Product data exporting is started.");

			var esalesProductInfoXmlFile = new EsalesProductInfoXmlFile();

			esalesProductInfoXmlFile.Initialize(EsalesSetting, EsalesImportType.Full);

			ExecuteEsalesExportProductList(esalesProductInfoXmlFile);

			ExecuteEsalesExportTags(esalesProductInfoXmlFile);

			string filePath = GetFilePath();

			_log.InfoFormat("Product data - file saving... //{0}", filePath);

			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				esalesProductInfoXmlFile.Save(stream);
			}

			_log.Info("Product data exporting is completed.");
		}

		public virtual void ExecuteEsalesImport()
		{
			_log.Info("Sending product data to Esales is started.");

			string filePath = GetFilePath();

			if (File.Exists(filePath))
			{
				EsalesConnector.ImportProducts(filePath);
			}

			_log.Info("Sending product data to Esales is completed.");
		}

		protected virtual void ExecuteEsalesExportProductList(EsalesProductInfoXmlFile esalesProductInfoXmlFile)
		{
			var pageInfo = IoC.Resolve<IPageTracker>().Initialize(EsalesSetting.PageSize, 1);

			_log.Info("Reading product data  - all / all.");

			while (pageInfo.HasNext)
			{
				var productsInfo = EsalesProductService.GetNextProductListPortion(pageInfo, UserContexts);

				esalesProductInfoXmlFile.AddProducts(productsInfo.GetEsalesProductInfoCollection());

				pageInfo.MoveToNext(productsInfo.TotalCount);

				_log.InfoFormat("Reading product data  - {0} / {1}.", pageInfo.ItemsLeft, pageInfo.ItemsTotalCount);
			}

			_log.Info("Product data reading is done.");
		}

		protected virtual void ExecuteEsalesExportTags(EsalesProductInfoXmlFile esalesProductInfoXmlFile)
		{
			_log.Info("Reading tags data.");

			IEsalesTagInfoList esalesTagInfoList = EsalesProductService.GetTags(UserContexts);

			esalesProductInfoXmlFile.AddTags(esalesTagInfoList.GetEsalesTagInfoCollection());

			_log.Info("Product tags reading is done.");
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			var channels = LekmerChannelService.GetAll().Where(c => EsalesSetting.GetChannelNameReplacementExists(c.CommonName));

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
				)
				.ToList();
		}

		protected virtual string GetFilePath()
		{
			string dirr = EsalesSetting.DestinationDirectoryProduct;
			string name = EsalesSetting.XmlFileNameProduct;

			return Path.Combine(dirr, name);
		}
	}
}
