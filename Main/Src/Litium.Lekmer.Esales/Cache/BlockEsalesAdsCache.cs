﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales
{
	public sealed class BlockEsalesAdsCache : ScensumCacheBase<BlockEsalesAdsKey, IBlockEsalesAds>
	{
		private static readonly BlockEsalesAdsCache _instance = new BlockEsalesAdsCache();

		private BlockEsalesAdsCache()
		{
		}

		public static BlockEsalesAdsCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockEsalesAdsKey(channel.Id, blockId));
			}
		}
	}

	public class BlockEsalesAdsKey : ICacheKey
	{
		public BlockEsalesAdsKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}