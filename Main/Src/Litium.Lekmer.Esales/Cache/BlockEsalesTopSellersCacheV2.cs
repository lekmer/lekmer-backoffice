﻿using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales
{
	public sealed class BlockEsalesTopSellersCacheV2 : ScensumCacheBase<BlockEsalesTopSellersKeyV2, IBlockEsalesTopSellersV2>
	{
		private static readonly BlockEsalesTopSellersCacheV2 _instance = new BlockEsalesTopSellersCacheV2();

		private BlockEsalesTopSellersCacheV2()
		{
		}

		public static BlockEsalesTopSellersCacheV2 Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (var channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockEsalesTopSellersKeyV2(channel.Id, blockId));
			}
		}
	}

	public class BlockEsalesTopSellersKeyV2 : ICacheKey
	{
		public BlockEsalesTopSellersKeyV2(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return "BlockEsalesTopSellersV2-" + ChannelId + "-" + BlockId; }
		}
	}
}