﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales.Cache
{
	public sealed class BlockEsalesSettingCache : ScensumCacheBase<BlockEsalesSettingKey, IBlockEsalesSetting>
	{
		// Singleton

		private static readonly BlockEsalesSettingCache _instance = new BlockEsalesSettingCache();

		private BlockEsalesSettingCache()
		{
		}

		public static BlockEsalesSettingCache Instance
		{
			get { return _instance; }
		}
	}

	public class BlockEsalesSettingKey : ICacheKey
	{
		public BlockEsalesSettingKey(int blockId)
		{
			BlockId = blockId;
		}

		public int BlockId { get; set; }

		public string Key
		{
			get { return string.Format("block_{0}", BlockId); }
		}
	}
}