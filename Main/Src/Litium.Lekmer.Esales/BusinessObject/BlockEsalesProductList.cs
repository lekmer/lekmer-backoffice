﻿using System;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesProductList : LekmerBlockBase, IBlockEsalesProductList
	{
		private IBlockSetting _setting;
		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}

		private IBlockEsalesSetting _esalesSetting;
		public IBlockEsalesSetting EsalesSetting
		{
			get { return _esalesSetting; }
			set
			{
				CheckChanged(_esalesSetting, value);
				_esalesSetting = value;
			}
		}
	}
}
