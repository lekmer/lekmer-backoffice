using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesProductInfo : IEsalesProductInfo
	{
		private readonly Dictionary<int, int> _channelDict = new Dictionary<int, int>();
		private readonly Collection<IEsalesChannelProductInfo> _channelProductInfoCollection = new Collection<IEsalesChannelProductInfo>();

		public int ProductId { get; set; }

		public bool Available
		{
			get { return _channelProductInfoCollection.Count > 0; }
		}

		public string EsalesKey { get; set; }

		public IEsalesChannelProductInfo DefaultProductInfo
		{
			get { return _channelProductInfoCollection[0]; }
		}

		public Collection<IEsalesChannelProductInfo> ChannelProductInfoCollection
		{
			get { return _channelProductInfoCollection; }
		}

		public void Add(IChannel channel, ILekmerProductView product)
		{
			if (!_channelDict.ContainsKey(channel.Id))
			{
				var esalesChannelProductInfo = IoC.Resolve<IEsalesChannelProductInfo>();

				esalesChannelProductInfo.Channel = channel;
				esalesChannelProductInfo.Product = product;

				_channelDict[channel.Id] = channel.Id;
				_channelProductInfoCollection.Add(esalesChannelProductInfo);
			}
		}
	}
}