using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesTagInfo : IEsalesTagInfo
	{
		private readonly Dictionary<int, int> _channelDict = new Dictionary<int, int>();
		private readonly Collection<IEsalesChannelTagInfo> _channelTagInfoCollection = new Collection<IEsalesChannelTagInfo>();

		public int TagId { get; set; }

		public string EsalesKey { get; set; }

		public IEsalesChannelTagInfo DefaultTagInfo
		{
			get { return _channelTagInfoCollection[0]; }
		}

		public Collection<IEsalesChannelTagInfo> ChannelTagInfoCollection
		{
			get { return _channelTagInfoCollection; }
		}

		public void Add(IChannel channel, ITag tag)
		{
			if (!_channelDict.ContainsKey(channel.Id))
			{
				var esalesChannelTagInfo = IoC.Resolve<IEsalesChannelTagInfo>();

				esalesChannelTagInfo.Channel = channel;
				esalesChannelTagInfo.Tag = tag;

				_channelDict[channel.Id] = channel.Id;
				_channelTagInfoCollection.Add(esalesChannelTagInfo);
			}
		}
	}
}