﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesRecommendV2 : LekmerBlockBase, IBlockEsalesRecommendV2
	{
		private string _panelName;
		private int _windowLastEsalesValue;
		private IBlockSetting _setting;

		public string PanelName
		{
			get { return _panelName; }
			set
			{
				CheckChanged(_panelName, value);
				_panelName = value;
			}
		}

		public int WindowLastEsalesValue
		{
			get { return _windowLastEsalesValue; }
			set
			{
				CheckChanged(_windowLastEsalesValue, value);
				_windowLastEsalesValue = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}

		public void PopulateEsalesParameters(Dictionary<string, string> parameters)
		{
			var key = string.Format(EsalesConstants.WindowLastTemplate, PanelName);
			if (!parameters.ContainsKey(key))
			{
				parameters.Add(key, WindowLastEsalesValue.ToString(CultureInfo.InvariantCulture));
			}
		}
	}
}
