﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class Ad : BusinessObjectBase, IAd
	{
		private int _id;
		private int _folderId;
		private string _key;
		private string _title;
		private DateTime? _displayFrom;
		private DateTime? _displayTo;
		private string _productCategory;
		private string _gender;
		private string _format;
		private string _searchTags;
		private DateTime _createdDate;
		private DateTime _updatedDate;
		private Collection<IAdChannel> _adChannelList;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int FolderId
		{
			get { return _folderId; }
			set
			{
				CheckChanged(_folderId, value);
				_folderId = value;
			}
		}

		public string Key
		{
			get { return _key; }
			set
			{
				CheckChanged(_key, value);
				_key = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public DateTime? DisplayFrom
		{
			get { return _displayFrom; }
			set
			{
				CheckChanged(_displayFrom, value);
				_displayFrom = value;
			}
		}

		public DateTime? DisplayTo
		{
			get { return _displayTo; }
			set
			{
				CheckChanged(_displayTo, value);
				_displayTo = value;
			}
		}

		public string ProductCategory
		{
			get { return _productCategory; }
			set
			{
				CheckChanged(_productCategory, value);
				_productCategory = value;
			}
		}

		public string Gender
		{
			get { return _gender; }
			set
			{
				CheckChanged(_gender, value);
				_gender = value;
			}
		}

		public string Format
		{
			get { return _format; }
			set
			{
				CheckChanged(_format, value);
				_format = value;
			}
		}

		public string SearchTags
		{
			get { return _searchTags; }
			set
			{
				CheckChanged(_searchTags, value);
				_searchTags = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set { _createdDate = value; }
		}

		public DateTime UpdatedDate
		{
			get { return _updatedDate; }
			set { _updatedDate = value; }
		}

		public Collection<IAdChannel> AdChannelList
		{
			get { return _adChannelList; }
			set
			{
				CheckChanged(_adChannelList, value);
				_adChannelList = value;
			}
		}
	}
}