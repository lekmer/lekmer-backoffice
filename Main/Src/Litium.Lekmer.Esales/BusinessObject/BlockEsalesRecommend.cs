﻿using System;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesRecommend : LekmerBlockBase, IBlockEsalesRecommend
	{
		private int? _recommendationType;
		private string _panelPath;
		private string _fallbackPanelPath;
		private IBlockSetting _setting;

		public int? RecommendationType
		{
			get { return _recommendationType; }
			set
			{
				CheckChanged(_recommendationType, value);
				_recommendationType = value;
			}
		}

		public string PanelPath
		{
			get { return _panelPath; }
			set
			{
				CheckChanged(_panelPath, value);
				_panelPath = value;
			}
		}

		public string FallbackPanelPath
		{
			get { return _fallbackPanelPath; }
			set
			{
				CheckChanged(_fallbackPanelPath, value);
				_fallbackPanelPath = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}
