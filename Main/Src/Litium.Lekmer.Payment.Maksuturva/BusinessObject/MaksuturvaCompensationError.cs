﻿using System;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class MaksuturvaCompensationError : IMaksuturvaCompensationError
	{
		public int Id { get; set; }
		public string ChannelCommonName { get; set; }
		public DateTime FetchDate { get; set; }
		public DateTime? LastAttempt { get; set; }
	}
}