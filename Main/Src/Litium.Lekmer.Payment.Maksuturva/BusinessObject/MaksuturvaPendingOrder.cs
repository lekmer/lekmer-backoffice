﻿using System;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class MaksuturvaPendingOrder : IMaksuturvaPendingOrder
	{
		public int Id { get; set; }
		public int ChannelId { get; set; }
		public int OrderId { get; set; }
		public int StatusId { get; set; }
		public DateTime? FirstAttempt { get; set; }
		public DateTime? LastAttempt { get; set; }
		public DateTime? NextAttempt { get; set; }
	}
}