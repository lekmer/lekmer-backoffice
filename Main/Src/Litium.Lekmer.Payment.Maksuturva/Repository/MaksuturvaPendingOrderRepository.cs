﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Maksuturva.Repository
{
	public class MaksuturvaPendingOrderRepository
	{
		protected virtual DataMapperBase<IMaksuturvaPendingOrder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IMaksuturvaPendingOrder>(dataReader);
		}

		public virtual int Insert(IMaksuturvaPendingOrder maksuturvaPendingOrder)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaPendingOrderRepository.Insert");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", maksuturvaPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", maksuturvaPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusId", maksuturvaPendingOrder.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", maksuturvaPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", maksuturvaPendingOrder.LastAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("NextAttempt", maksuturvaPendingOrder.NextAttempt, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pMaksuturvaPendingOrderInsert]", parameters, dbSettings);
		}

		public virtual void Update(IMaksuturvaPendingOrder maksuturvaPendingOrder)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaPendingOrderRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", maksuturvaPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", maksuturvaPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusId", maksuturvaPendingOrder.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", maksuturvaPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", maksuturvaPendingOrder.LastAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("NextAttempt", maksuturvaPendingOrder.NextAttempt, SqlDbType.DateTime)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pMaksuturvaPendingOrderUpdate]", parameters, dbSettings);
		}

		public virtual void Delete(int orderId)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaPendingOrderRepository.Delete");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pMaksuturvaPendingOrderDelete]", parameters, dbSettings);
		}

		public virtual IMaksuturvaPendingOrder Get(int orderId)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaPendingOrderRepository.Get");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pMaksuturvaPendingOrderGet]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IMaksuturvaPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaPendingOrderRepository.GetPendingOrdersForStatusCheck");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CheckToDate", checkToDate, SqlDbType.DateTime)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pMaksuturvaPendingOrderGetPendingOrdersForStatusCheck]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}