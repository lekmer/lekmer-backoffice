﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Maksuturva.Repository
{
	public class MaksuturvaCompensationErrorRepository
	{
		protected virtual DataMapperBase<IMaksuturvaCompensationError> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IMaksuturvaCompensationError>(dataReader);
		}

		public virtual void Insert(IMaksuturvaCompensationError maksuturvaCompensationError)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaCompensationErrorRepository.Insert");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelCommonName", maksuturvaCompensationError.ChannelCommonName, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("FetchDate", maksuturvaCompensationError.FetchDate, SqlDbType.DateTime)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pMaksuturvaCompensationErrorInsert]", parameters, dbSettings);
		}

		public virtual void Update(IMaksuturvaCompensationError maksuturvaCompensationError)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaCompensationErrorRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("MaksuturvaCompensationErrorId", maksuturvaCompensationError.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("LastAttempt", maksuturvaCompensationError.LastAttempt, SqlDbType.DateTime)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pMaksuturvaCompensationErrorUpdate]", parameters, dbSettings);
		}

		public virtual void Delete(int id)
		{
			var dbSettings = new DatabaseSetting("MaksuturvaCompensationErrorRepository.Delete");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("MaksuturvaCompensationErrorId", id, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pMaksuturvaCompensationErrorDelete]", parameters, dbSettings);
		}

		public virtual Collection<IMaksuturvaCompensationError> Get()
		{
			var dbSettings = new DatabaseSetting("MaksuturvaCompensationErrorRepository.Get");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pMaksuturvaCompensationErrorGet]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}