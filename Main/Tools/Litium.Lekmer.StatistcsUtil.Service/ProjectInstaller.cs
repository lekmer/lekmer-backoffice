﻿using System.ComponentModel;
using System.Configuration.Install;


namespace Litium.Lekmer.StatisticsUtil.Service
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}
	}
}
