﻿using System;
using System.Reflection;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.StatisticsUtil.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main()
		{
			try
			{
				//System.Console.WriteLine("Statistics Util Console");
				//System.Console.WriteLine("-----------------------");
				//System.Console.WriteLine("Press any key to start");
				var lekmerProductSecureService = (ILekmerProductSecureService)IoC.Resolve<IProductSecureService>();
				var channels = IoC.Resolve<IChannelSecureService>().GetAll();
				lekmerProductSecureService.UpdatePopularity(channels, DateTime.Now.AddDays(-30), DateTime.Now); ;

				System.Console.WriteLine("Done!");
				System.Console.ReadLine();
			}
			catch (Exception ex)
			{
				_log.Error("StatisticsUtil.Console failed.", ex);

				System.Console.ReadLine();
			}
		}
	}
}
