CREATE TABLE [lekmer].[tAge]
(
[AgeId] [int] NOT NULL IDENTITY(1, 1),
[Months] [int] NOT NULL,
[Title] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tAge] ADD CONSTRAINT [PK_tAge] PRIMARY KEY CLUSTERED  ([AgeId]) ON [PRIMARY]
GO
