CREATE TABLE [product].[tBlockCategoryProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[ProductSortOrderId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockCategoryProductList] ADD CONSTRAINT [PK_tBlockCategoryProductList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockCategoryProductList] ADD CONSTRAINT [FK_tBlockCategoryProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [product].[tBlockCategoryProductList] ADD CONSTRAINT [FK_tBlockCategoryProductList_tProductSortOrder] FOREIGN KEY ([ProductSortOrderId]) REFERENCES [product].[tProductSortOrder] ([ProductSortOrderId])
GO
