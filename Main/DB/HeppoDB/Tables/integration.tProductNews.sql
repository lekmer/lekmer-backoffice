CREATE TABLE [integration].[tProductNews]
(
[ProductId] [int] NOT NULL,
[ErpId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductNews] ADD CONSTRAINT [PK__tProduct__B40CC6CD558D1123] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
