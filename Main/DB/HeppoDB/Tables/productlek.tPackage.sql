CREATE TABLE [productlek].[tPackage]
(
[PackageId] [int] NOT NULL IDENTITY(1, 1),
[MasterProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackage] ADD CONSTRAINT [PK_tPackage] PRIMARY KEY CLUSTERED  ([PackageId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackage] WITH NOCHECK ADD CONSTRAINT [FK_tPackage_tProduct] FOREIGN KEY ([MasterProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
ALTER TABLE [productlek].[tPackage] NOCHECK CONSTRAINT [FK_tPackage_tProduct]
GO
