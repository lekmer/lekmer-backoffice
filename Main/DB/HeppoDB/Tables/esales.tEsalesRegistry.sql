CREATE TABLE [esales].[tEsalesRegistry]
(
[EsalesRegistryId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesRegistry] ADD CONSTRAINT [PK_tEsalesRegistry] PRIMARY KEY CLUSTERED  ([EsalesRegistryId]) ON [PRIMARY]
GO
