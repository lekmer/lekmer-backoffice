CREATE TABLE [campaignlek].[tCartValueRangeConditionCurrency]
(
[ConditionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValueFrom] [decimal] (16, 2) NOT NULL,
[MonetaryValueTo] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartValueRangeConditionCurrency] ADD CONSTRAINT [PK_tCartValueRangeConditionCurrency] PRIMARY KEY CLUSTERED  ([ConditionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartValueRangeConditionCurrency] ADD CONSTRAINT [FK_tCartValueRangeConditionCurrency_tCartValueRangeCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCartValueRangeCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tCartValueRangeConditionCurrency] ADD CONSTRAINT [FK_tCartValueRangeConditionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
