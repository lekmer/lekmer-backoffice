CREATE TABLE [review].[tRatingGroupProduct]
(
[RatingGroupId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupProduct] ADD CONSTRAINT [PK_tRatingGroupProduct] PRIMARY KEY CLUSTERED  ([RatingGroupId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupProduct] ADD CONSTRAINT [FK_tRatingGroupProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [review].[tRatingGroupProduct] ADD CONSTRAINT [FK_tRatingGroupProduct_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId])
GO
