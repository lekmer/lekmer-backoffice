CREATE TABLE [campaignlek].[tCartValueRangeCondition]
(
[ConditionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartValueRangeCondition] ADD CONSTRAINT [PK_tCartValueRangeCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartValueRangeCondition] ADD CONSTRAINT [FK_tCartValueRangeCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
