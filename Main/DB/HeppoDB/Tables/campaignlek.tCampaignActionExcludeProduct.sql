CREATE TABLE [campaignlek].[tCampaignActionExcludeProduct]
(
[ConfigId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeProduct] ADD CONSTRAINT [PK_tCampaignActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ConfigId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeProduct] ADD CONSTRAINT [FK_tCampaignActionExcludeProduct_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeProduct] ADD CONSTRAINT [FK_tCampaignActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
