CREATE TABLE [lekmer].[tBlockBrandProductListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandProductListBrand] ADD CONSTRAINT [PK_tBlockBrandProductListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandProductListBrand] ADD CONSTRAINT [FK_tBlockBrandProductListBrand_tBlockBrandProductList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandProductList] ([BlockId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tBlockBrandProductListBrand] ADD CONSTRAINT [FK_tBlockBrandProductListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
