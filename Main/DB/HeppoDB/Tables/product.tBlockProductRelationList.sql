CREATE TABLE [product].[tBlockProductRelationList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[ProductSortOrderId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductRelationList] ADD CONSTRAINT [PK_tBlockProductRelationList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductRelationList_ProductSortOrderId] ON [product].[tBlockProductRelationList] ([ProductSortOrderId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductRelationList] ADD CONSTRAINT [FK_tBlockProductRelationList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [product].[tBlockProductRelationList] ADD CONSTRAINT [FK_tBlockProductRelationList_tProductSortOrder] FOREIGN KEY ([ProductSortOrderId]) REFERENCES [product].[tProductSortOrder] ([ProductSortOrderId])
GO
