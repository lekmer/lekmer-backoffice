CREATE TABLE [product].[tBlockProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductList] ADD CONSTRAINT [PK_tBlockProductList_1] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductList] ADD CONSTRAINT [FK_tBlockProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
