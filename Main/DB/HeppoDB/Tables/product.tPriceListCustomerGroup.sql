CREATE TABLE [product].[tPriceListCustomerGroup]
(
[PriceListId] [int] NOT NULL,
[CustomerGroupId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceListCustomerGroup] ADD CONSTRAINT [PK_tPriceListCustomerGroup] PRIMARY KEY CLUSTERED  ([PriceListId], [CustomerGroupId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceListCustomerGroup] ADD CONSTRAINT [FK_tPriceListCustomerGroup_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId])
GO
ALTER TABLE [product].[tPriceListCustomerGroup] ADD CONSTRAINT [FK_tPriceListCustomerGroup_tPriceList] FOREIGN KEY ([PriceListId]) REFERENCES [product].[tPriceList] ([PriceListId])
GO
