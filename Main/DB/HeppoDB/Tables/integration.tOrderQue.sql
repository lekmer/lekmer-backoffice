CREATE TABLE [integration].[tOrderQue]
(
[OrderId] [int] NOT NULL,
[NumberOfFailedAttempts] [int] NOT NULL,
[LastFailedAttempt] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tOrderQue] ADD 
CONSTRAINT [PK_tOrderQue] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
