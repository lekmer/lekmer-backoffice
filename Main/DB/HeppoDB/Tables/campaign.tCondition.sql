CREATE TABLE [campaign].[tCondition]
(
[ConditionId] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[ConditionTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCondition] ADD CONSTRAINT [PK_tCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCondition_ConditionTypeId] ON [campaign].[tCondition] ([ConditionTypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCondition] ADD CONSTRAINT [FK_tCondition_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
ALTER TABLE [campaign].[tCondition] WITH NOCHECK ADD CONSTRAINT [FK_tCondition_tConditionType] FOREIGN KEY ([ConditionTypeId]) REFERENCES [campaign].[tConditionType] ([ConditionTypeId])
GO
