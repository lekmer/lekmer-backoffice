CREATE TABLE [customer].[tCustomer]
(
[CustomerId] [int] NOT NULL IDENTITY(1, 1),
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CustomerStatusId] [int] NOT NULL,
[CustomerRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomer] ADD CONSTRAINT [PK_tCustomer] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomer_CustomerRegistryId] ON [customer].[tCustomer] ([CustomerRegistryId]) INCLUDE ([CustomerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomer_CustomerStatusId] ON [customer].[tCustomer] ([CustomerStatusId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomer] ADD CONSTRAINT [FK_tCustomer_tCustomerRegistry] FOREIGN KEY ([CustomerRegistryId]) REFERENCES [customer].[tCustomerRegistry] ([CustomerRegistryId])
GO
ALTER TABLE [customer].[tCustomer] ADD CONSTRAINT [FK_tCustomer_tCustomerStatus] FOREIGN KEY ([CustomerStatusId]) REFERENCES [customer].[tCustomerStatus] ([CustomerStatusId])
GO
