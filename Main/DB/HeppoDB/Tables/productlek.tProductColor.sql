CREATE TABLE [productlek].[tProductColor]
(
[ProductId] [int] NOT NULL,
[ColorId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [productlek].[tProductColor] ADD
CONSTRAINT [FK_tProductColor_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
ALTER TABLE [productlek].[tProductColor] ADD CONSTRAINT [PK_tProductColor] PRIMARY KEY CLUSTERED  ([ProductId], [ColorId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tProductColor] ADD CONSTRAINT [FK_tProductColor_tColor] FOREIGN KEY ([ColorId]) REFERENCES [productlek].[tColor] ([ColorId])
GO
