CREATE TABLE [addon].[tBlockTopList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[TotalProductCount] [int] NOT NULL,
[IncludeAllCategories] [bit] NOT NULL,
[OrderStatisticsDayCount] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopList] ADD CONSTRAINT [PK_tBlockTopList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopList] ADD CONSTRAINT [FK_tBlockTopList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
