CREATE TABLE [campaignlek].[tFixedDiscountActionExcludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tFixedDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tFixedDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tFixedDiscountActionExcludeCategory_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
