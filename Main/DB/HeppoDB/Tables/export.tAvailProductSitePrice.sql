CREATE TABLE [export].[tAvailProductSitePrice]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SitePrice] [decimal] (16, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tAvailProductSitePrice] ADD CONSTRAINT [PK_tAvailProductSitePrice] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId]) ON [PRIMARY]
GO
