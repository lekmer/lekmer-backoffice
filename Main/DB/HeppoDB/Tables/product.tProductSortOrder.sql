CREATE TABLE [product].[tProductSortOrder]
(
[ProductSortOrderId] [int] NOT NULL,
[Title] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SortByColumn] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SortDescending] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSortOrder] ADD CONSTRAINT [PK_tProductSortOrder] PRIMARY KEY CLUSTERED  ([ProductSortOrderId]) ON [PRIMARY]
GO
