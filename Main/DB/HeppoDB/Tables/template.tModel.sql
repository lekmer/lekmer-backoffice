CREATE TABLE [template].[tModel]
(
[ModelId] [int] NOT NULL IDENTITY(1, 1),
[ModelFolderId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DefaultTemplateId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tModel] ADD CONSTRAINT [PK_tModel] PRIMARY KEY CLUSTERED  ([ModelId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModel_CommonName] ON [template].[tModel] ([CommonName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tModel_DefaultTemplateId] ON [template].[tModel] ([DefaultTemplateId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tModel_ModelFolderId] ON [template].[tModel] ([ModelFolderId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])
GO
