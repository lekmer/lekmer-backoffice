CREATE TABLE [sitestructure].[tSiteStructureModuleChannel]
(
[ChannelId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tSiteStructureModuleChannel] ADD CONSTRAINT [PK_tSiteStructureModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tSiteStructureModuleChannel_SSRegistryId] ON [sitestructure].[tSiteStructureModuleChannel] ([SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tSiteStructureModuleChannel] ADD CONSTRAINT [FK_tSiteStructureModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [sitestructure].[tSiteStructureModuleChannel] ADD CONSTRAINT [FK_tSiteStructureModuleChannel_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
