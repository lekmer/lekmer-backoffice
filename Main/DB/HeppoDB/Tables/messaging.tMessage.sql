CREATE TABLE [messaging].[tMessage]
(
[MessageId] [uniqueidentifier] NOT NULL,
[BatchId] [uniqueidentifier] NULL,
[MessageStatusId] [int] NOT NULL,
[MessageTypeId] [int] NOT NULL,
[ProviderName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Body] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessage] ADD CONSTRAINT [PK_Message] PRIMARY KEY NONCLUSTERED  ([MessageId]) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessage] ADD CONSTRAINT [FK_tMessage_tBatch] FOREIGN KEY ([BatchId]) REFERENCES [messaging].[tBatch] ([BatchId])
GO
ALTER TABLE [messaging].[tMessage] ADD CONSTRAINT [FK_tMessage_tMessageStatus] FOREIGN KEY ([MessageStatusId]) REFERENCES [messaging].[tMessageStatus] ([MessageStatusId])
GO
ALTER TABLE [messaging].[tMessage] ADD CONSTRAINT [FK_tMessage_tMessageType] FOREIGN KEY ([MessageTypeId]) REFERENCES [messaging].[tMessageType] ([MessageTypeId])
GO
