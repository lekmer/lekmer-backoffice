CREATE TABLE [lekmer].[tAgeInterval]
(
[AgeIntervalId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[FromMonth] [int] NOT NULL,
[ToMonth] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tAgeInterval] ADD CONSTRAINT [PK_tAgeInterval] PRIMARY KEY CLUSTERED  ([AgeIntervalId]) ON [PRIMARY]
GO
