CREATE TABLE [integration].[OrderLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CustomerNo] [int] NOT NULL,
[OrderNo] [int] NOT NULL,
[OrderXmlIn] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[OrderXmlOut] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL,
[Messagge] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [integration].[OrderLog] ADD 
CONSTRAINT [PK_OrderLog] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
