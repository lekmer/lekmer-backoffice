CREATE TABLE [export].[tProductPrice]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[PriceIncludingVat] [decimal] (16, 2) NOT NULL,
[PriceExcludingVat] [decimal] (16, 2) NOT NULL,
[DiscountPriceIncludingVat] [decimal] (16, 2) NULL,
[DiscountPriceExcludingVat] [decimal] (16, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tProductPrice] ADD CONSTRAINT [PK_tProductPrice] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId]) ON [PRIMARY]
GO
