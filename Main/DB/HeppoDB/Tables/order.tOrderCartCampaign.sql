CREATE TABLE [order].[tOrderCartCampaign]
(
[OrderCartCampaignId] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderCartCampaign] ADD CONSTRAINT [PK_tOrderCartCampaign] PRIMARY KEY CLUSTERED  ([OrderCartCampaignId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderCartCampaign_OrderId] ON [order].[tOrderCartCampaign] ([OrderId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderCartCampaign] ADD CONSTRAINT [FK_tOrderCartCampaign_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
