CREATE TABLE [campaignlek].[tFixedDiscountActionExcludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tFixedDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tFixedDiscountActionExcludeProduct_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tFixedDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
