CREATE TABLE [integration].[tOrderDataStatistics]
(
[OrderId] [int] NOT NULL,
[Price] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NULL,
[CivicNumber] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[Email] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Channel] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tOrderDataStatistics] ADD CONSTRAINT [PK__tOrderDa__C3905BCF62A1E952] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
