CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountAction]
(
[CartActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountAction] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountAction] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
