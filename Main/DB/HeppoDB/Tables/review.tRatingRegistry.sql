CREATE TABLE [review].[tRatingRegistry]
(
[RatingRegistryId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRatingRegistry_CommonName] ON [review].[tRatingRegistry] ([CommonName]) ON [PRIMARY]

GO
ALTER TABLE [review].[tRatingRegistry] ADD CONSTRAINT [PK_tRatingRegistry] PRIMARY KEY CLUSTERED  ([RatingRegistryId]) ON [PRIMARY]
GO
