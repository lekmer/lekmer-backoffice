CREATE TABLE [customer].[tBlockRegister]
(
[BlockId] [int] NOT NULL,
[RedirectContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tBlockRegister] ADD CONSTRAINT [PK_tBlockRegister] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockRegister_RedirectContentNodeId] ON [customer].[tBlockRegister] ([RedirectContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tBlockRegister] ADD CONSTRAINT [FK_tBlockRegister_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [customer].[tBlockRegister] ADD CONSTRAINT [FK_tBlockRegister_tContentNode] FOREIGN KEY ([RedirectContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
