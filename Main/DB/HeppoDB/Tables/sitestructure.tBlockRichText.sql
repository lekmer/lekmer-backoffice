CREATE TABLE [sitestructure].[tBlockRichText]
(
[BlockId] [int] NOT NULL,
[Content] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockRichText] ADD CONSTRAINT [PK_tBlockRichText] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockRichText] ADD CONSTRAINT [FK_tBlockRichText_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
