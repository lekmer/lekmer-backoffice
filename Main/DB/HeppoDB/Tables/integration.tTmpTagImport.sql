CREATE TABLE [integration].[tTmpTagImport]
(
[TagId] [int] NOT NULL,
[Value] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tTmpTagImport] ADD CONSTRAINT [PK__tTmpTagI__657CF9AC015EC906] PRIMARY KEY CLUSTERED  ([TagId]) ON [PRIMARY]
GO
