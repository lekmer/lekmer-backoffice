CREATE TABLE [addon].[tCartItemPriceActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemPriceActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemPriceActionExcludeCategory_tProductPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId])
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemPriceActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
