CREATE TABLE [campaign].[tCampaignFolderType]
(
[TypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignFolderType] ADD CONSTRAINT [PK_tCampaignFolderType] PRIMARY KEY CLUSTERED  ([TypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignFolderType_CommonName] ON [campaign].[tCampaignFolderType] ([CommonName]) ON [PRIMARY]
GO
