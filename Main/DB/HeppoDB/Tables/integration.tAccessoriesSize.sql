CREATE TABLE [integration].[tAccessoriesSize]
(
[AccessoriesSizeId] [int] NOT NULL,
[AccessoriesSize] [nvarchar] (35) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AccessoriesSizeErpId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tAccessoriesSize] ADD 
CONSTRAINT [PK_tAccessoriesSize] PRIMARY KEY CLUSTERED  ([AccessoriesSizeId]) ON [PRIMARY]
GO
