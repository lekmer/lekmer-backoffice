CREATE TABLE [lekmer].[tProductRegistryRestrictionProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionProduct] ADD CONSTRAINT [PK_tProductRegistryRestrictionProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionProduct] ADD CONSTRAINT [FK_tProductRegistryRestrictionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionProduct] ADD CONSTRAINT [FK_tProductRegistryRestrictionProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
