CREATE TABLE [lekmer].[tProductRegistryRestrictionCategory]
(
[ProductRegistryId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionCategory] ADD CONSTRAINT [PK_tProductRegistryRestrictionCategory] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionCategory] ADD CONSTRAINT [FK_tProductRegistryRestrictionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionCategory] ADD CONSTRAINT [FK_tProductRegistryRestrictionCategory_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
