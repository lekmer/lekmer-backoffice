CREATE TABLE [product].[tStore]
(
[StoreId] [int] NOT NULL IDENTITY(1, 1),
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[StatusId] [int] NOT NULL,
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [product].[tStore] ADD CONSTRAINT [PK_tStore] PRIMARY KEY CLUSTERED  ([StoreId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tStore_StatusId] ON [product].[tStore] ([StatusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tStore_Title] ON [product].[tStore] ([Title]) ON [PRIMARY]
GO
ALTER TABLE [product].[tStore] ADD CONSTRAINT [FK_tStore_tStoreStatus] FOREIGN KEY ([StatusId]) REFERENCES [product].[tStoreStatus] ([StoreStatusId])
GO
