CREATE TABLE [campaign].[tProductCampaign]
(
[CampaignId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tProductCampaign] ADD CONSTRAINT [PK_tProductCampaign] PRIMARY KEY CLUSTERED  ([CampaignId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tProductCampaign] ADD CONSTRAINT [FK_tProductCampaign_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
