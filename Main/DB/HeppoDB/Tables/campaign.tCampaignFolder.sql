CREATE TABLE [campaign].[tCampaignFolder]
(
[FolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TypeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignFolder] ADD CONSTRAINT [PK_tCampaignFolder] PRIMARY KEY CLUSTERED  ([FolderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCampaignFolder_ParentFolderId] ON [campaign].[tCampaignFolder] ([ParentFolderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCampaignFolder_TypeId] ON [campaign].[tCampaignFolder] ([TypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignFolder] ADD CONSTRAINT [FK_tCampaignFolder_tCampaignFolder] FOREIGN KEY ([ParentFolderId]) REFERENCES [campaign].[tCampaignFolder] ([FolderId])
GO
ALTER TABLE [campaign].[tCampaignFolder] ADD CONSTRAINT [FK_tCampaignFolder_tCampaignFolderType] FOREIGN KEY ([TypeId]) REFERENCES [campaign].[tCampaignFolderType] ([TypeId])
GO
