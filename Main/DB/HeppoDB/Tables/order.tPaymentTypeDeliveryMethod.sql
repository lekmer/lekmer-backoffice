CREATE TABLE [order].[tPaymentTypeDeliveryMethod]
(
[PaymentTypeId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tPaymentTypeDeliveryMethod] ADD CONSTRAINT [PK_tPaymentTypeDeliveryMethod] PRIMARY KEY CLUSTERED  ([PaymentTypeId], [DeliveryMethodId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tPaymentTypeDeliveryMethod] ADD CONSTRAINT [FK_tPaymentTypeDeliveryMethod_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
ALTER TABLE [order].[tPaymentTypeDeliveryMethod] ADD CONSTRAINT [FK_tPaymentTypeDeliveryMethod_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
