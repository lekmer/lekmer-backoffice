CREATE TABLE [review].[tRatingReviewFeedbackLike]
(
[RatingReviewFeedbackLikeId] [int] NOT NULL IDENTITY(1, 1),
[RatingReviewFeedbackId] [int] NOT NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[RatingReviewUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD CONSTRAINT [PK_tRatingReviewFeedbackLike] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackLikeId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId])
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewUser] FOREIGN KEY ([RatingReviewUserId]) REFERENCES [review].[tRatingReviewUser] ([RatingReviewUserId])
GO
