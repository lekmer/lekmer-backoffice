CREATE TABLE [customerlek].[tAddress]
(
[AddressId] [int] NOT NULL,
[CustomerId] [int] NOT NULL,
[HouseNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[HouseExtension] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[Reference] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [customerlek].[tAddress] ADD CONSTRAINT [PK_tAddress] PRIMARY KEY CLUSTERED  ([CustomerId], [AddressId]) ON [PRIMARY]
GO
ALTER TABLE [customerlek].[tAddress] ADD CONSTRAINT [FK_tAddress(lek)_tAddress] FOREIGN KEY ([CustomerId], [AddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId])
GO
