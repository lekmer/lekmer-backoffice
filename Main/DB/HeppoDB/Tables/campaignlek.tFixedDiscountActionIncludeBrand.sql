CREATE TABLE [campaignlek].[tFixedDiscountActionIncludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tFixedDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeBrand] ADD CONSTRAINT [FK_tFixedDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeBrand] ADD CONSTRAINT [FK_tFixedDiscountActionIncludeBrand_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
