CREATE TABLE [lekmer].[tFlag]
(
[FlagId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Class] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFlag] ADD CONSTRAINT [PK_tFlag] PRIMARY KEY CLUSTERED  ([FlagId]) ON [PRIMARY]
GO
