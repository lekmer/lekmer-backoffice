CREATE TABLE [lekmer].[tCartItemFixedDiscountActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionCurrency] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionCurrency] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionCurrency_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionCurrency] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
