CREATE TABLE [lekmer].[tCustomerForgotPassword]
(
[CustomerId] [int] NOT NULL,
[Guid] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCustomerForgotPassword] ADD CONSTRAINT [PK_tCustomerForgotPassword] PRIMARY KEY CLUSTERED  ([Guid]) ON [PRIMARY]
GO
