CREATE TABLE [lekmer].[tWishList]
(
[WishListId] [int] NOT NULL IDENTITY(1, 1),
[WishListKey] [uniqueidentifier] NOT NULL,
[WishList] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tWishList_WishListKey] ON [lekmer].[tWishList] ([WishListKey]) ON [PRIMARY]

GO
ALTER TABLE [lekmer].[tWishList] ADD CONSTRAINT [PK_tWishList] PRIMARY KEY CLUSTERED  ([WishListId]) ON [PRIMARY]
GO
