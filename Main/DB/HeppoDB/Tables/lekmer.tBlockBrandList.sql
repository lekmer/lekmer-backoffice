CREATE TABLE [lekmer].[tBlockBrandList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[IncludeAllBrands] [bit] NOT NULL,
[LinkContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandList] ADD CONSTRAINT [PK_tBlockBrandList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandList] ADD CONSTRAINT [FK_tBlockBrandList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockBrandList] ADD CONSTRAINT [FK_tBlockBrandList_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
