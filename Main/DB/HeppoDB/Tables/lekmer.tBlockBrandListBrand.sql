CREATE TABLE [lekmer].[tBlockBrandListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tBlockBrandListBrand_Ordinal] ON [lekmer].[tBlockBrandListBrand] ([Ordinal]) ON [PRIMARY]

GO
ALTER TABLE [lekmer].[tBlockBrandListBrand] ADD CONSTRAINT [PK_tBlockBrandListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandListBrand] ADD CONSTRAINT [FK_tBlockBrandListBrand_tBlockBrandList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandList] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockBrandListBrand] ADD CONSTRAINT [FK_tBlockBrandListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
