CREATE TABLE [orderlek].[tPackageOrderItemProduct]
(
[PackageOrderItemId] [int] NOT NULL,
[OrderItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SizeId] [int] NULL,
[SizeErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD
CONSTRAINT [FK_tPackageOrderItemProduct_tPackageOrderItem] FOREIGN KEY ([PackageOrderItemId]) REFERENCES [orderlek].[tPackageOrderItemProduct] ([PackageOrderItemId])
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [PK_tPackageOrderItemProduct] PRIMARY KEY CLUSTERED  ([PackageOrderItemId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [FK_tPackageOrderItemProduct_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [FK_tPackageOrderItemProduct_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
