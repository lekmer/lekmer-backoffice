CREATE TABLE [media].[tImageTranslation]
(
[MediaId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[AlternativeText] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [media].[tImageTranslation] ADD CONSTRAINT [PK_tImageTranslation] PRIMARY KEY CLUSTERED  ([MediaId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [media].[tImageTranslation] ADD CONSTRAINT [FK_tImageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [media].[tImageTranslation] ADD CONSTRAINT [FK_tImageTranslation_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId])
GO
