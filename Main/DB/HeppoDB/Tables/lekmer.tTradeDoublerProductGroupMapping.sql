CREATE TABLE [lekmer].[tTradeDoublerProductGroupMapping]
(
[ProductId] [int] NOT NULL,
[ProductGroupId] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ChannelId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tTradeDoublerProductGroupMapping] ADD 
CONSTRAINT [PK_tTradeDoublerProductGroupMapping] PRIMARY KEY CLUSTERED  ([ProductId], [ChannelId]) ON [PRIMARY]
GO
