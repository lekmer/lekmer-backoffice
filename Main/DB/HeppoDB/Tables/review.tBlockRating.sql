CREATE TABLE [review].[tBlockRating]
(
[BlockId] [int] NOT NULL,
[RatingId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockRating] ADD CONSTRAINT [PK_tBlockRating] PRIMARY KEY CLUSTERED  ([BlockId], [RatingId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockRating] ADD CONSTRAINT [FK_tBlockRating_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [review].[tBlockRating] ADD CONSTRAINT [FK_tBlockRating_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
