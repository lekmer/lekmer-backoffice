CREATE TABLE [order].[tOrderAuditType]
(
[OrderAuditTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderAuditType] ADD CONSTRAINT [PK_tOrderAuditType] PRIMARY KEY CLUSTERED  ([OrderAuditTypeId]) ON [PRIMARY]
GO
