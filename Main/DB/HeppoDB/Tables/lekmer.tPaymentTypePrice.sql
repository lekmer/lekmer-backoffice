CREATE TABLE [lekmer].[tPaymentTypePrice]
(
[CountryId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[PaymentTypeId] [int] NOT NULL,
[PaymentCost] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPaymentTypePrice] ADD CONSTRAINT [PK_tPaymentTypePrice] PRIMARY KEY CLUSTERED  ([CountryId], [CurrencyId], [PaymentTypeId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPaymentTypePrice] ADD CONSTRAINT [FK_tPaymentTypePrice_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
GO
ALTER TABLE [lekmer].[tPaymentTypePrice] ADD CONSTRAINT [FK_tPaymentTypePrice_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [lekmer].[tPaymentTypePrice] ADD CONSTRAINT [FK_tPaymentTypePrice_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
