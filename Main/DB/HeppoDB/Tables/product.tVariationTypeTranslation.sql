CREATE TABLE [product].[tVariationTypeTranslation]
(
[VariationTypeId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationTypeTranslation] ADD CONSTRAINT [PK_tVariationTypeTranslation] PRIMARY KEY CLUSTERED  ([VariationTypeId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationTypeTranslation] ADD CONSTRAINT [FK_tVariationTypeTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [product].[tVariationTypeTranslation] ADD CONSTRAINT [FK_tVariationTypeTranslation_tVariationType] FOREIGN KEY ([VariationTypeId]) REFERENCES [product].[tVariationType] ([VariationTypeId])
GO
