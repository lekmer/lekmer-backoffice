CREATE TABLE [review].[tBlockProductFeedback]
(
[BlockId] [int] NOT NULL,
[AllowReview] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockProductFeedback] ADD CONSTRAINT [PK_tBlockProductFeedback] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockProductFeedback] ADD CONSTRAINT [FK_tBlockProductFeedback_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
