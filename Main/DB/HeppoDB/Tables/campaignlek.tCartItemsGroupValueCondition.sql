CREATE TABLE [campaignlek].[tCartItemsGroupValueCondition]
(
[ConditionId] [int] NOT NULL,
[ConfigId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueCondition] ADD CONSTRAINT [PK_tCartItemsGroupValueCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueCondition] ADD CONSTRAINT [FK_tCartItemsGroupValueCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueCondition] ADD CONSTRAINT [FK_tCartItemsGroupValueCondition_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
