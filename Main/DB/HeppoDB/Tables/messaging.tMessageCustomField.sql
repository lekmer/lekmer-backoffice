CREATE TABLE [messaging].[tMessageCustomField]
(
[MessageId] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessageCustomField] ADD CONSTRAINT [PK_tMessageCustomField] PRIMARY KEY NONCLUSTERED  ([MessageId], [Name]) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessageCustomField] ADD CONSTRAINT [FK_tMessageCustomField_tMessage] FOREIGN KEY ([MessageId]) REFERENCES [messaging].[tMessage] ([MessageId])
GO
