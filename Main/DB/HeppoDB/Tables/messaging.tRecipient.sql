CREATE TABLE [messaging].[tRecipient]
(
[RecipientId] [uniqueidentifier] NOT NULL,
[MessageId] [uniqueidentifier] NOT NULL,
[BatchId] [uniqueidentifier] NULL,
[Chunk] [int] NOT NULL,
[Address] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tRecipient] ADD CONSTRAINT [PK_tRecipient] PRIMARY KEY NONCLUSTERED  ([RecipientId]) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tRecipient] ADD CONSTRAINT [FK_tRecipient_tBatch] FOREIGN KEY ([BatchId]) REFERENCES [messaging].[tBatch] ([BatchId])
GO
ALTER TABLE [messaging].[tRecipient] ADD CONSTRAINT [FK_tRecipient_tMessage] FOREIGN KEY ([MessageId]) REFERENCES [messaging].[tMessage] ([MessageId])
GO
