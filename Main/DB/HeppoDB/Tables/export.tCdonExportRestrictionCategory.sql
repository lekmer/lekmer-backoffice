CREATE TABLE [export].[tCdonExportRestrictionCategory]
(
[ProductRegistryId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportRestrictionCategory] ADD CONSTRAINT [PK_tCdonExportRestrictionCategory] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportRestrictionCategory] ADD CONSTRAINT [FK_tCdonExportRestrictionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [export].[tCdonExportRestrictionCategory] ADD CONSTRAINT [FK_tCdonExportRestrictionCategory_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
