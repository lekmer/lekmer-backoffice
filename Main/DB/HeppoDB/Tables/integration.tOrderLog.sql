CREATE TABLE [integration].[tOrderLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[OrderXmlIn] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[OrderXmlOut] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [integration].[tOrderLog] ADD 
CONSTRAINT [PK_tOrderLog] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
