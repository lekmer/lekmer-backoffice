CREATE TABLE [lekmer].[tBlockBrandProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[ProductSortOrderId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandProductList] ADD CONSTRAINT [PK_tBlockBrandProductList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandProductList] ADD CONSTRAINT [FK_tBlockBrandProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockBrandProductList] ADD CONSTRAINT [FK_tBlockBrandProductList_tProductSortOrder] FOREIGN KEY ([ProductSortOrderId]) REFERENCES [product].[tProductSortOrder] ([ProductSortOrderId])
GO
