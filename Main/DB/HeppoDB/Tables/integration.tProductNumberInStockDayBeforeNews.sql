CREATE TABLE [integration].[tProductNumberInStockDayBeforeNews]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[NoInStock] [int] NULL,
[Date] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductNumberInStockDayBeforeNews] ADD CONSTRAINT [PK__tProduct__0C37165A07590AEF] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId]) ON [PRIMARY]
GO
