CREATE TABLE [messaging].[tMessageStatus]
(
[MessageStatusId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessageStatus] ADD CONSTRAINT [PK_tMessageStatus] PRIMARY KEY NONCLUSTERED  ([MessageStatusId]) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessageStatus] ADD CONSTRAINT [UQ_tMessageStatus_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
