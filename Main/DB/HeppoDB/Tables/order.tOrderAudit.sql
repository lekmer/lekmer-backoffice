CREATE TABLE [order].[tOrderAudit]
(
[OrderAuditId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[SystemUserId] [int] NOT NULL,
[OrderAuditTypeId] [int] NOT NULL,
[Information] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [text] COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderAudit] ADD CONSTRAINT [PK_tOrderAudit] PRIMARY KEY CLUSTERED  ([OrderAuditId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderAudit] ADD CONSTRAINT [FK_tOrderAudit_tOrderAuditType] FOREIGN KEY ([OrderAuditTypeId]) REFERENCES [order].[tOrderAuditType] ([OrderAuditTypeId])
GO
ALTER TABLE [order].[tOrderAudit] ADD CONSTRAINT [FK_tOrderAudit_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
ALTER TABLE [order].[tOrderAudit] ADD CONSTRAINT [FK_tOrderAudit_tSystemUser] FOREIGN KEY ([SystemUserId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
