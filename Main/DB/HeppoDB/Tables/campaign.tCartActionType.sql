CREATE TABLE [campaign].[tCartActionType]
(
[CartActionTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartActionType] ADD CONSTRAINT [PK_tCartActionType] PRIMARY KEY CLUSTERED  ([CartActionTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCartActionType_CommonName] ON [campaign].[tCartActionType] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCartActionType_Title] ON [campaign].[tCartActionType] ([Title]) ON [PRIMARY]
GO
