CREATE TABLE [sitestructure].[tContentPageTranslation]
(
[ContentNodeId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[UrlTitle] [varchar] (200) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageTranslation] ADD CONSTRAINT [PK_tContentPageTranslation] PRIMARY KEY CLUSTERED  ([ContentNodeId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageTranslation] ADD CONSTRAINT [FK_tContentPageTranslation_tContentPage] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tContentPageTranslation] ADD CONSTRAINT [FK_tContentPageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
