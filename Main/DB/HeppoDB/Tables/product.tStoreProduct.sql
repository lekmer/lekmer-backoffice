CREATE TABLE [product].[tStoreProduct]
(
[ProductId] [int] NOT NULL,
[StoreId] [int] NOT NULL,
[Quantity] [int] NOT NULL,
[Threshold] [int] NOT NULL CONSTRAINT [DF_tStoreProduct_Threshold] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [product].[tStoreProduct] ADD CONSTRAINT [PK_tStoreProduct] PRIMARY KEY CLUSTERED  ([ProductId], [StoreId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tStoreProduct] ADD CONSTRAINT [FK_tStoreProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tStoreProduct] ADD CONSTRAINT [FK_tStoreProduct_tStore] FOREIGN KEY ([StoreId]) REFERENCES [product].[tStore] ([StoreId])
GO
