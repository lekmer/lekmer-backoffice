CREATE TABLE [customer].[tAddress]
(
[AddressId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NOT NULL,
[Addressee] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StreetAddress] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StreetAddress2] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[PostalCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[City] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CountryId] [int] NOT NULL,
[PhoneNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[AddressTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tAddress] ADD CONSTRAINT [PK_tAddress] PRIMARY KEY CLUSTERED  ([CustomerId], [AddressId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_AddressId_Covered] ON [customer].[tAddress] ([AddressId]) INCLUDE ([Addressee], [AddressTypeId], [City], [CountryId], [CustomerId], [PhoneNumber], [PostalCode], [StreetAddress], [StreetAddress2]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_AddressTypeId] ON [customer].[tAddress] ([AddressTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_CountryId] ON [customer].[tAddress] ([CountryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_CustomerInformationId] ON [customer].[tAddress] ([CustomerId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tAddress] ADD CONSTRAINT [FK_tAddress_tAddressType] FOREIGN KEY ([AddressTypeId]) REFERENCES [customer].[tAddressType] ([AddressTypeId])
GO
ALTER TABLE [customer].[tAddress] ADD CONSTRAINT [FK_tAddress_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
GO
ALTER TABLE [customer].[tAddress] ADD CONSTRAINT [FK_tAddress_tCustomerInformation] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId]) ON DELETE CASCADE
GO
