CREATE TABLE [campaignlek].[tFixedPriceActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionCurrency] ADD CONSTRAINT [PK_tFixedPriceActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionCurrency] ADD CONSTRAINT [FK_tFixedPriceActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionCurrency] ADD CONSTRAINT [FK_tFixedPriceActionCurrency_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
