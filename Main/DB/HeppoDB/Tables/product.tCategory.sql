CREATE TABLE [product].[tCategory]
(
[CategoryId] [int] NOT NULL IDENTITY(1, 1),
[ParentCategoryId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tCategory] ADD CONSTRAINT [PK_tCategory] PRIMARY KEY CLUSTERED  ([CategoryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCategory_ParentCategoryId] ON [product].[tCategory] ([ParentCategoryId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tCategory] ADD CONSTRAINT [FK_tCategory_tCategory] FOREIGN KEY ([ParentCategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'A unique ID of the category created automatically by the database engine.', 'SCHEMA', N'product', 'TABLE', N'tCategory', 'COLUMN', N'CategoryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The title of the category in swedish. E.g "Musik"', 'SCHEMA', N'product', 'TABLE', N'tCategory', 'COLUMN', N'Title'
GO
