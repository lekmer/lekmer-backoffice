CREATE TABLE [customerlek].[tCustomerInformation]
(
[CustomerId] [int] NOT NULL,
[GenderTypeId] [int] NOT NULL,
[IsCompany] [bit] NULL,
[AlternateAddressId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [customerlek].[tCustomerInformation] ADD
CONSTRAINT [FK_tCustomerInformation_tAddress] FOREIGN KEY ([CustomerId], [AlternateAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId]) ON DELETE CASCADE
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD CONSTRAINT [PK_tCustomerInformation] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD CONSTRAINT [FK_tCustomerInformation_tCustomerInformation1] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId])
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD CONSTRAINT [FK_tCustomerInformation_tGenderType] FOREIGN KEY ([GenderTypeId]) REFERENCES [customerlek].[tGenderType] ([GenderTypeId])
GO
