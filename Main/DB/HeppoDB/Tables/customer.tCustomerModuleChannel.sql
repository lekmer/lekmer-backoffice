CREATE TABLE [customer].[tCustomerModuleChannel]
(
[ChannelId] [int] NOT NULL,
[CustomerRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerModuleChannel] ADD CONSTRAINT [PK_tCustomerModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomerModuleChannel_CustomerRegistryId] ON [customer].[tCustomerModuleChannel] ([CustomerRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerModuleChannel] ADD CONSTRAINT [FK_tCustomerModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [customer].[tCustomerModuleChannel] ADD CONSTRAINT [FK_tCustomerModuleChannel_tCustomerRegistry] FOREIGN KEY ([CustomerRegistryId]) REFERENCES [customer].[tCustomerRegistry] ([CustomerRegistryId])
GO
