CREATE TABLE [campaign].[tFreightValueActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tFreightValueActionCurrency] ADD CONSTRAINT [PK_tFreightValueActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tFreightValueActionCurrency] ON [campaign].[tFreightValueActionCurrency] ([CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tFreightValueActionCurrency] ADD CONSTRAINT [FK_tFreightValueActionCurrency_tFreightValueAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tFreightValueAction] ([CartActionId])
GO
ALTER TABLE [campaign].[tFreightValueActionCurrency] ADD CONSTRAINT [FK_tFreightValueActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
