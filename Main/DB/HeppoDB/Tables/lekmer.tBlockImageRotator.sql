CREATE TABLE [lekmer].[tBlockImageRotator]
(
[BlockId] [int] NOT NULL,
[SecondaryTemplateId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockImageRotator] ADD CONSTRAINT [PK_tBlockImageRotator] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockImageRotator] ADD CONSTRAINT [FK_tBlockImageRotator_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
