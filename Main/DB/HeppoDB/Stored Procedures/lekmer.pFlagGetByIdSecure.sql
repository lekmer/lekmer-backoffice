SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagGetByIdSecure]
@FlagId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT *
	FROM lekmer.vFlagSecure 
	WHERE [Flag.Id] = @FlagId
END

GO
