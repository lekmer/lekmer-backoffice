SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pBlockCheckoutGetById]
@LanguageId INT,
@BlockId int
as	
begin
	set nocount on
	select
		bc.*,
		b.*
	from
		[order].[vCustomBlockCheckout] as bc
		inner join [sitestructure].[vCustomBlock] as b on bc.[BlockCheckout.CheckoutBlockId] = b.[Block.BlockId]
	where
		bc.[BlockCheckout.CheckoutBlockId] = @BlockId 
		AND b.[Block.LanguageId] = @LanguageId
end

GO
