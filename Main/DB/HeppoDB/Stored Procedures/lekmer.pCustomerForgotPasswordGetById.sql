SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCustomerForgotPasswordGetById]
	@Guid	uniqueidentifier
AS 
BEGIN 
	SET nocount ON 
	SELECT 
		* 
	FROM 
		lekmer.vCustomerForgotPassword
	WHERE 
		[ForgotPassword.Guid] = @Guid
END
GO
