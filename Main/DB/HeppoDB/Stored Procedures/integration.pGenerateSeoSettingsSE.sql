
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSeoSettingsSE]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Produktnivå skor>		
		------------------------------------------------------------
		
		-- DEFAULT --
		INSERT INTO product.tProductSeoSetting(ProductId)
		SELECT
			p.ProductId
		FROM
			product.tProduct p
		WHERE
			p.ProductId not in (select ProductId 
									from product.tProductSeoSetting)		
									

		UPDATE
			pss
		SET
			pss.Title = b.Title  + ' ' + p.Title + ' skor - Heppo.se ' + LOWER(coalesce(t.Value,'')) + 'a ' 
			+ case c2.Title when 'Kvinna' then 'damskor'
								when 'Man' then 'herrskor'								
								end 
			+ ' online', 
			
			pss.[Description] = 'Heppo.se din skobutik på nätet. Köp ' + b.Title + ' ' + p.Title + ' på nätet. ' + 
			'Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp och prisgaranti!'
			
			-- WARNING !! kan finnas mer än en tag färg !
			--select LOWER(coalesce(t.Value,'')), c2.title, c3.title
		FROM 
			product.tProductSeoSetting pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				left join lekmer.tProductTag pt
					on pt.ProductId = p.ProductId
				left join lekmer.tTag t
					on t.TagId = pt.TagId
		WHERE
			c3.CategoryId = 1000059 -- skor
			and t.TagGroupId = 30 -- färg
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))

		
		
		------------------------------------------------------------
		-- <Produktnivå skovård>	
		------------------------------------------------------------
		
		UPDATE
			pss
		SET
			pss.Title = b.Title  + ' ' + p.Title + ' ' + c.Title + ' & skovårdsprodukter på nätet. Skor hos Heppo.se',
			
			pss.[Description] = 'Köp ' + b.Title  + ' ' + p.Title + ' ' + c.Title +
			'& skovårdsprodukter online på Heppo.se. Fri frakt & Fri retur. Vår skovårdsskola hjälper dig till rätt skovård. Välkommen!' 
			--select c.title, c2.title, c3.title
		FROM
				product.tProductSeoSetting pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				left join lekmer.tProductTag pt
					on pt.ProductId = p.ProductId
				left join lekmer.tTag t
					on t.TagId = pt.TagId
		WHERE
			c3.CategoryId = 1000071 -- skotillbehör		
			and t.TagGroupId != 48 -- Strumpor storlekar
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))
		
		
		
		------------------------------------------------------------
		-- <Underkategori>
		-- Title>
		-- Formel: Underkategori Huvudkategori & Skor online på nätet från Heppo.se
		-- Exempel: Stövlar Damskor & Skor online på nätet från Heppo.se 	
		------------------------------------------------------------
		
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n --3
		where 
			n.SiteStructureRegistryId = 2
			and n.ContentNodeTypeId = 3 -- detta är contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
										
		UPDATE
			cps
		SET
			cps.Title = cn.Title  + ' ' + cn3.Title + ' & Skor online. Heppo.se – köp skor på nätet.',
			
			cps.[Description] = 'Heppo.se din skobutik på internet - Köp ' + cn.Title + ' och ' + cn3.Title +
			'på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp när du köper ' + cn.Title + ' och ' + cn3.Title +
			' från Heppo'
			--select cps.title, cps.description, cn.title, cn2.title, cn3.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				--inner join sitestructure.tContentPage cp
					--on cp.ContentNodeId = cps.ContentNodeId	
				------
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId				
				inner join sitestructure.tContentNode cn2
				on cn.ParentContentNodeId = cn2.ContentNodeId
				inner join sitestructure.tContentNode cn3
				on cn.ParentContentNodeId = cn2.ContentNodeId
				------						
		WHERE
			cn.SiteStructureRegistryId = 1
			and ((cn3.ContentNodeId = 1003358 and cn2.ContentNodeId = 1000843) -- damskor, kategorier
					or (cn3.ContentNodeId = 1003359 and cn2.ContentNodeId = 1000844) -- herrskor, kategorier
					or (cn3.ContentNodeId = 1003728 and cn2.ContentNodeId = 1003732))
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
		
		
		
		------------------------------------------------------------
		-- <Varumärke>		
		------------------------------------------------------------
		INSERT INTO sitestructure.tContentPageSeoSetting(ContentNodeId)
		SELECT
			c.ContentNodeId
		FROM
			sitestructure.tContentPage c
		WHERE
			c.ContentNodeId not in (select ContentNodeId
									from sitestructure.tContentPageSeoSetting)		
									
									
		UPDATE
			cps
		SET
			cps.Title = 'Köp ' + cn.Title  + ' skor online – Heppo.se skor på nätet',
			
			cps.[Description] = 'Köp ' + cn.Title + ' skor på nätet. Prova hemma - Fri frakt & Fri retur.' +
			' 14 dagars öppet köp och prisgaranti! Heppo.se – din skobutik på nätet.' 
			--select cps.title, cps.description, cn.title, cn2.title, cn3.title
		FROM
				sitestructure.tContentPageSeoSetting cps
				------
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId				
				inner join sitestructure.tContentNode cn2
					on cn.ParentContentNodeId = cn2.ContentNodeId
				inner join sitestructure.tContentNode cn3
					on cn.ParentContentNodeId = cn2.ContentNodeId
				------						
		WHERE
			cn.SiteStructureRegistryId = 1
			and cn3.ContentNodeId = 1000080 -- skomärken
			and (cn2.ContentNodeId = 1000080 or cn2.ParentContentNodeId = 1000080)
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
