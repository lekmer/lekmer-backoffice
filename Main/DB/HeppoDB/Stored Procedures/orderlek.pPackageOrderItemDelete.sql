SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemDelete]
	@OrderItemId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tPackageOrderItemProduct]
	WHERE [OrderItemId] = @OrderItemId
	
	DELETE [orderlek].[tPackageOrderItem]
	WHERE [OrderItemId] = @OrderItemId
END
GO
