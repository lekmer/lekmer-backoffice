SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserStatusGetById]
	@SystemUserStatusId	int
as
begin
	set nocount on

	select
		*
	from
		security.[vCustomSystemUserStatus]
	where
		[SystemUserStatus.Id] = @SystemUserStatusId
end

GO
