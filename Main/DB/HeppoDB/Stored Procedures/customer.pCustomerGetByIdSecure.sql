SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetByIdSecure] 
	@CustomerId int
as	
begin
	set nocount on

	select
		*
	from
		[customer].[vCustomCustomerSecure]
	where
		[Customer.CustomerId] = @CustomerId
end

GO
