SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create procedure [product].[pBlockCategoryProductListDelete]
@BlockId		int
as
BEGIN
	delete
		[product].[tBlockCategoryProductList]
	where
		[BlockId] = @BlockId

end
GO
