SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman D.	Date: 22.12.2008	Time: 10:00
Description:	Created

*/

CREATE  procedure [sitestructure].[pContentNodeParentUpdate]
@ContentNodeId			int,
@ParentContentNodeId	int
as
begin
if (@ParentContentNodeId!=null)
begin
	if not exists (select 1 from [sitestructure].[tContentNode] where ContentNodeId = @ParentContentNodeId)
		return -1
end
update [sitestructure].[tContentNode] set ParentContentNodeId = @ParentContentNodeId where ContentNodeId = @ContentNodeId
	
end
GO
