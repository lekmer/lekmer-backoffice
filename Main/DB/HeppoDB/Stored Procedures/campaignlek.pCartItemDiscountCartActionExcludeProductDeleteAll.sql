SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemDiscountCartActionExcludeProduct]
	WHERE
		CartActionId = @CartActionId
END

GO
