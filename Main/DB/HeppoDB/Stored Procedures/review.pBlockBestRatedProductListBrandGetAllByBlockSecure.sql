SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockBestRatedProductListBrandSecure] b
	WHERE
		b.[BlockBestRatedProductListBrand.BlockId] = @BlockId
END
GO
