SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-25
-- Description:	Gets a media folder by parentId
-- =============================================
CREATE PROCEDURE [media].[pMediaFolderGetAllByParentId]
	@MediaFolderParentId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT	
		*
	FROM   [media].[vCustomMediaFolder] v
	WHERE (v.[MediaFolder.ParentId] = @MediaFolderParentId AND NOT @MediaFolderParentId IS NULL) OR 
		  (v.[MediaFolder.ParentId] IS NULL AND @MediaFolderParentId IS NULL)
			
END

GO
