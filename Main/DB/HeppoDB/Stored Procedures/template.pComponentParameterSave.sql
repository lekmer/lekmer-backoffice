SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pComponentParameterSave]
	@ComponentParameterId			INT,
	@ComponentId					INT,
	@Title							NVARCHAR(50),
	@DataType						INT,
	@Description					NVARCHAR(50)
AS
BEGIN
	UPDATE
		template.tComponentParameter
	SET				
		[ComponentId]				= @ComponentId,
		[Title]						= @Title,
		[DataType]					= @DataType,
		[Description]				= @Description
	WHERE
		ComponentParameterId = @ComponentParameterId
		
	IF @@ROWCOUNT = 0 
		BEGIN
			INSERT INTO
				template.tComponentParameter
				(
					[ComponentId],
					[Title],
					[DataType],
					[Description]
				)
			VALUES
				(
					@ComponentId, 
					@Title, 
					@DataType, 
					@Description
				)
				
			SET @ComponentId = SCOPE_IDENTITY()
		END	

	RETURN @ComponentId
END
	





GO
