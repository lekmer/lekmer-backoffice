SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tFixedDiscountAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tFixedDiscountAction] (
			ProductActionId,
			IncludeAllProducts
		)
		VALUES (
			@ProductActionId,
			@IncludeAllProducts
		)
	END
END
GO
