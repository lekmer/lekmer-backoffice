SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pBlockRegisterGetById]
@LanguageId INT,
@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		br.*,
		b.*
	FROM
		[customer].[vCustomBlockRegister] AS br
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON br.[BlockRegister.BlockId] = b.[Block.BlockId]
	WHERE
		br.[BlockRegister.BlockId] = @BlockId
		AND B.[Block.LanguageId] = @LanguageId 
END

GO
