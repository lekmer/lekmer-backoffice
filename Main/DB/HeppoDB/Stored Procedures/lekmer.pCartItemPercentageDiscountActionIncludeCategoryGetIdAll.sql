SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
