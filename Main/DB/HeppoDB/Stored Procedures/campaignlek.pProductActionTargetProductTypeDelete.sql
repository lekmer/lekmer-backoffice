SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pProductActionTargetProductTypeDelete]
	@ProductActionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE [campaignlek].[tProductActionTargetProductType]
	WHERE [ProductActionId] = @ProductActionId
END
GO
