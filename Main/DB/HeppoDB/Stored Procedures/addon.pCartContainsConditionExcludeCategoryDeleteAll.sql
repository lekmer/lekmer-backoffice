SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryDeleteAll]
	@ConditionId int
as
begin
	DELETE
		[addon].tCartContainsConditionExcludeCategory
	WHERE
		ConditionId = @ConditionId
end



GO
