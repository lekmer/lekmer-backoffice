SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pComponentGetByCommonName]
	@CommonName VARCHAR(50)
AS
BEGIN
	SELECT
		*
	FROM
		template.vCustomComponent
	WHERE
		[Component.CommonName] = @CommonName
END

GO
