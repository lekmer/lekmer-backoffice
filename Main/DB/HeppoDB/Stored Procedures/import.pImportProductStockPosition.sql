SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [import].[pImportProductStockPosition]

AS
BEGIN	

		if object_id('tempdb..#tImportProductStockPosition') is not null
		drop table #tImportProductStockPosition
		
		create table #tImportProductStockPosition
		(
			HYarticleId nvarchar(50) COLLATE Finnish_Swedish_CI_AS not null,
			Lagerplatstyp nvarchar(100) COLLATE Finnish_Swedish_CI_AS not null,
			LagerplatstypBenamning nvarchar(100) COLLATE Finnish_Swedish_CI_AS not null,
			Lagerplats nvarchar(100)COLLATE Finnish_Swedish_CI_AS not null
			constraint PK_#tImportProductStockPisotion primary key(HYarticleId, Lagerplats)
		)

		-- Insert all new rows into temp table
		insert into #tImportProductStockPosition(HYarticleId, Lagerplatstyp, LagerplatstypBenamning, Lagerplats)
		select
			i.HYErpIdSize,
			i.Lagerplatstyp,
			i.LagerplatstypBenamning,
			i.Lagerplats
			-- select *
		from -- hanterar inte det som är ändrat (dvs uppdatering)
			import.tProductStockPosition i
			left join integration.tProductStockPosition h
				on h.HYarticleId = i.HYErpIdSize
				and h.Lagerplats = i.Lagerplats				
		where
			h.HYarticleId is null
			or i.Lagerplats <> h.Lagerplats
			or i.Lagerplatstyp <> h.Lagerplatstyp
			or i.LagerplatstypBenamning <> h.LagerplatstypBenamning
		order by 
			i.HYErpIdSize
		
		
		
		declare @TotalAmountProducts int
		declare @RowSize int
		set @TotalAmountProducts = (select COUNT(*) from #tImportProductStockPosition)								
		set @RowSize = 300
		
		while @TotalAmountProducts > 0
		begin
			begin try
				begin transaction
				
				--------------------------------------------------------------------------
				-- Update lagerplats if it has been changes
				--------------------------------------------------------------------------
				update
					i
				set
					i.Lagerplats = h.Lagerplats,
					i.Lagerplatstyp = h.Lagerplatstyp,
					i.LagerplatstypBenamning = h.LagerplatstypBenamning
					-- select *
				from
					integration.tProductStockPosition i
					inner join #tImportProductStockPosition h
						on h.HYarticleId = i.HYarticleId
						and h.Lagerplats = i.Lagerplats			
				where
					i.Lagerplats <> h.Lagerplats
					or i.Lagerplatstyp <> h.Lagerplatstyp
					or i.LagerplatstypBenamning <> h.LagerplatstypBenamning
				
				
				
				--------------------------------------------------------------------------
				-- Insert if it does not exists, left join alot more effective than NOT IN
				--------------------------------------------------------------------------
				insert into integration.tProductStockPosition(HYarticleId, Lagerplatstyp,LagerplatstypBenamning,Lagerplats, ImportDate)
				select top (@RowSize)
					h.HYarticleId,
					h.Lagerplatstyp,
					h.LagerplatstypBenamning,
					h.Lagerplats,
					GETDATE()
					-- select *
				from
					#tImportProductStockPosition h
					left join integration.tProductStockPosition i
						on h.HYarticleId = i.HYarticleId
						and h.Lagerplats = i.Lagerplats			
				where
					i.HYarticleId is null
					--h.HYarticleId not in (select HYarticleId
											--from integration.tProductStockPosition)
			
			
				
				set @TotalAmountProducts = @TotalAmountProducts - @RowSize
				commit
				
				-- Delete rows affected
				delete top(@RowSize)from #tImportProductStockPosition
				
			end try
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG here
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			
		end
END

GO
