SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberUpdateFromOrders]
AS
BEGIN
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			DECLARE @CreatedDate DATETIME = GETDATE()

			INSERT INTO
				[lekmer].[tNewsletterSubscriber] (
					[ChannelId],
					[Email],
					[SubscriberTypeId],
					[SentStatus],
					[CreatedDate],
					[UpdatedDate]
				)
			SELECT DISTINCT
				o.[ChannelId],
				o.[Email],
				2, -- OrderBased,
				NULL,
				@CreatedDate,
				@CreatedDate
			FROM
				[order].[tOrder] o WITH (NOLOCK)
			WHERE
				[o].[OrderStatusId] = 4 -- In HY
				AND [o].[Email] LIKE '%@%'
				AND NOT EXISTS
				(
					SELECT 1
					FROM [lekmer].[tNewsletterSubscriber] n
					WHERE [n].[ChannelId] = [o].[ChannelId] AND [n].[Email] = [o].[Email]
				)
				AND NOT EXISTS
				(
					SELECT 1
					FROM [lekmer].[tNewsletterUnsubscriber] un
					WHERE [un].[ChannelId] = [o].[ChannelId] AND [un].[Email] = [o].[Email]
				)

			COMMIT
		END TRY

		BEGIN CATCH
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
			VALUES (NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		END CATCH
	END
END
GO
