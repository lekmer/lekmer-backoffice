SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartValueSave]
	@ConditionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN 
	INSERT 
		campaign.tCartValueConditionCurrency
	( 
		ConditionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES 
	(
		@ConditionId,
		@CurrencyId,
		@Value
	)
END
GO
