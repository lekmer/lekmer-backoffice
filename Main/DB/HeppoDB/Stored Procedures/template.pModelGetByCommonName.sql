SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelGetByCommonName]
    @CommonName VARCHAR(50)
AS 
BEGIN
    SET NOCOUNT ON

    SELECT  *
    FROM    [template].[vCustomModel]
    WHERE   [Model.CommonName] = @CommonName
END

GO
