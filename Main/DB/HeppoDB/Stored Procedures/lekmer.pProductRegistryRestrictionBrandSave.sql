SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandSave]
	@ProductRegistryId	INT,
	@BrandId			INT,
	@RestrictionReason	NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME
AS
BEGIN
	INSERT INTO [lekmer].[tProductRegistryRestrictionBrand] (
		ProductRegistryId,
		BrandId,
		RestrictionReason,
		UserId,
		CreatedDate
	)
	VALUES (
		@ProductRegistryId,
		@BrandId,
		@RestrictionReason,
		@UserId,
		@CreatedDate
	)
END
GO
