SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [lekmer].[tCartItemFixedDiscountAction]
	WHERE CartActionId = @CartActionId
END

GO
