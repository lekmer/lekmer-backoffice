SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAll]
AS
BEGIN
	SELECT DISTINCT src.CategoryId, rc.ProductRegistryId
	FROM [lekmer].[tProductRegistryRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] (rc.CategoryId) src
END
GO
