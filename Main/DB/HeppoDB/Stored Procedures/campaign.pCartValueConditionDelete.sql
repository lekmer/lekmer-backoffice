SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartValueConditionDelete]
	@ConditionId int
AS 
BEGIN
	DELETE FROM campaign.tCartValueConditionCurrency
	WHERE ConditionId = @ConditionId
	
	DELETE FROM campaign.tCartValueCondition
	WHERE ConditionId = @ConditionId
END
GO
