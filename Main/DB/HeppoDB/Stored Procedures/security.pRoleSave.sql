SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE procedure [security].[pRoleSave]
	@RoleId				int,
	@Title				nvarchar(50)
as
begin
	set nocount on

	if exists (
				select 
					1 
				from 
					[security].[tRole] 
				where 
					Title = @Title 
					and RoleId <> @RoleId
				)
	return -1
	ELSE
	BEGIN
		update
				[security].[tRole]
			set
				[Title] = @Title
			where
				[RoleId] = @RoleId
		if @@ROWCOUNT = 0
		begin
			insert into [security].[tRole]
			(
				[Title]
			)
			values
			(
				@Title
			)
			
			set @RoleId = scope_identity()
		end
			
		return @RoleId	
	END
END
	


GO
