SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [customer].[pAddressSave]
@AddressId		int,
@CustomerId		int,
@Addressee		nvarchar(100),
@StreetAddress	nvarchar(200),
@StreetAddress2 nvarchar(200),
@PostalCode		nvarchar(10),
@City			nvarchar(100),
@CountryId		int,
@PhoneNumber	nvarchar(20),
@AddressTypeId	int
AS	
BEGIN
	SET NOCOUNT ON
	
	--Check if customer exist
	IF NOT EXISTS ( 
					SELECT 
						1 
					FROM 
						[customer].[tCustomer] 
					WHERE 
						[tCustomer].[CustomerId] = @CustomerId 
					)
		RETURN -1
	
	UPDATE  [customer].[tAddress]
	SET 
		Addressee = @Addressee,
		StreetAddress = @StreetAddress,
		StreetAddress2 = @StreetAddress2,
		PostalCode = @PostalCode,
		City = @City,
		CountryId = @CountryId,
		PhoneNumber = @PhoneNumber,
		AddressTypeId = @AddressTypeId
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customer].[tAddress]
		(
			CustomerId,
			Addressee,
			StreetAddress,
			StreetAddress2,
			PostalCode,
			City,
			CountryId,
			PhoneNumber,
			AddressTypeId
		)
		VALUES
		(
			@CustomerId,
			@Addressee,
			@StreetAddress,
			@StreetAddress2,
			@PostalCode,
			@City,
			@CountryId,
			@PhoneNumber,
			@AddressTypeId
		)
		SET	@AddressId = SCOPE_IDENTITY()
	END

	RETURN @AddressId
END




GO
