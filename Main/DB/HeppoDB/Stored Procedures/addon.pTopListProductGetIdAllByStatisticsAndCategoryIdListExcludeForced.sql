
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [addon].[pTopListProductGetIdAllByStatisticsAndCategoryIdListExcludeForced]
	@ChannelId				INT,
	@BlockId				INT,
	@IncludeAllCategories	BIT,
	@CategoryIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@CountOrdersFrom		DATETIME,
	@From					INT,
	@To						INT
AS
BEGIN

	DECLARE @sql AS NVARCHAR(MAX)
	DECLARE @sqlCount AS NVARCHAR(MAX)
	DECLARE @sqlFragment AS NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT
			ROW_NUMBER() OVER (ORDER BY SUM(oi.[OrderItem.Quantity]) DESC) AS Number,
			p.[Product.Id] AS ProductId
		FROM
			product.vCustomProduct p
			INNER JOIN [order].vCustomOrderItem oi WITH(NOLOCK) ON oi.[OrderItem.ProductId] = p.[Product.Id]
			INNER JOIN [order].vCustomOrder o WITH(NOLOCK)
				ON oi.[OrderItem.OrderId] = o.[Order.OrderId]
				AND o.[Order.ChannelId] = @ChannelId
				AND o.[Order.CreatedDate] >= @CountOrdersFrom
			CROSS APPLY (
				SELECT SUM(ps.[NumberInStock]) AS ''NumberInStock'' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[Product.Id]
			) a1
		WHERE
			p.[Product.ChannelId] = @ChannelId
			AND ISNULL(a1.[NumberInStock], p.[Product.NumberInStock]) > 0'
			
	IF (@IncludeAllCategories = 0 AND @CategoryIds IS NOT NULL)
	SET @sqlFragment = @sqlFragment + '
			AND ( p.[Product.CategoryId] IN ( SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter) ) )'

	SET @sqlFragment = @sqlFragment + '		
			AND NOT EXISTS ( SELECT 1 FROM [addon].[tBlockTopListProduct] fp WHERE fp.[BlockId] = @BlockId AND fp.[ProductId] = p.[Product.Id] )
		GROUP BY
			p.[Product.Id]
	'

	SET @sqlCount = '
		SELECT COUNT(*)
		FROM
			(' + @sqlFragment + ') AS CountResults'

	SET @sql = '
		SELECT ProductId
		FROM
			(' + @sqlFragment + ') AS SearchResult
		WHERE
			Number BETWEEN @From AND @To'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME,
			@From					INT,
			@To						INT',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To
END
GO
