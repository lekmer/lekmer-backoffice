SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBlockReviewMyWishListGetByIdSecure]
	@BlockId int
as	
begin
	set nocount on
	select
		BRMWL.*,
		b.*
	from
		[lekmer].[tBlockReviewMyWishList] as BRMWL
		inner join [sitestructure].[vCustomBlockSecure] as b on BRMWL.[BlockId] = b.[Block.BlockId]
	where
		BRMWL.[BlockId] = @BlockId
end
GO
