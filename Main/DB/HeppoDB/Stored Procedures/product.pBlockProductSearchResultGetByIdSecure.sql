SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductSearchResultGetByIdSecure]
	@BlockId	INT
AS BEGIN
	SELECT
		ba.*,
		b.*
	FROM
		[product].[vCustomBlockProductSearchResult] AS ba
		inner join [sitestructure].[vCustomBlockSecure] AS b ON ba.[BlockProductSearchResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchResult.BlockId] = @BlockId
END

GO
