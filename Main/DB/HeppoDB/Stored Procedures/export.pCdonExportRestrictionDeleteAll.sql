SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionDeleteAll]
AS
BEGIN
	DELETE FROM [export].[tCdonExportIncludeProduct]
	DELETE FROM [export].[tCdonExportRestrictionProduct]
	
	DELETE FROM [export].[tCdonExportIncludeCategory]
	DELETE FROM [export].[tCdonExportRestrictionCategory]
	
	DELETE FROM [export].[tCdonExportIncludeBrand]
	DELETE FROM [export].[tCdonExportRestrictionBrand]
END
GO
