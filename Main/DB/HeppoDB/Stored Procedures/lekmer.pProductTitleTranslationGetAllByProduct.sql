SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTitleTranslationGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.Title] AS 'Value'
	FROM
		[lekmer].[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
END

GO
