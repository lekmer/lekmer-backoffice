SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryGetIdAll]
	@ConditionId		int
AS
BEGIN
	SELECT 
		c.CategoryId,
		A.IncludeSubcategories
	FROM 
		[addon].tCartContainsConditionExcludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.ConditionId = @ConditionId
END
GO
