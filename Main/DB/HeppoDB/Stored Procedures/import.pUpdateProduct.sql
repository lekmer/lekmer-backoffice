SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pUpdateProduct]

AS
BEGIN
		BEGIN TRANSACTION
		BEGIN TRY				
		
			-- Update tProduct
			UPDATE 
				p
			SET
				p.Title = m.ArticleTitle, 
				p.CategoryId = (select categoryId from product.tCategory 
								where ErpId = 'C_'+m.ArticleClassId+'-'+m.ArticleGroupId+'-'+m.ArticleCodeId)		
			FROM
				import.tProduct m
				inner join [lekmer].tLekmerProduct lp
						on lp.HYErpId = m.HYErpId
				inner join product.tProduct p
						on p.ProductId = lp.ProductId
				--inner join [product].tProductRegistryProduct pli
						--on pli.ProductId = lp.ProductId and
						--pli.ProductRegistryId = m.ChannelId
				LEFT JOIN product.tCategory c
					ON c.CategoryId = p.CategoryId
			WHERE
				m.ChannelId = 1
				AND p.Title <> m.ArticleTitle
				OR c.ErpId <> 'C_'+m.ArticleClassId+'-'+m.ArticleGroupId+'-'+m.ArticleCodeId
	
		COMMIT
		END TRY		
		BEGIN CATCH
			if @@TRANCOUNT > 0 rollback

				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				values(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

		END CATCH
	
END
GO
