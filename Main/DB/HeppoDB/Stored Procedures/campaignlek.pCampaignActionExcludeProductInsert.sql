SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductInsert]
	@ConfigId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeProduct] (
		ConfigId,
		ProductId
	)
	VALUES (
		@ConfigId,
		@ProductId
	)
END
GO
