SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman D.	Date: 21.05.2009	Time: 16:00
Description:	Created
*/

CREATE PROCEDURE 	[sitestructure].[pBlockRichTextDelete]
@BlockId	int
as
BEGIN
	DELETE
		 sitestructure.tBlockRichTextTranslation
	 WHERE 
		[BlockId] = @BlockId

	DELETE 
		[sitestructure].[tBlockRichText]
	where
		[BlockId] = @BlockId
end
GO
