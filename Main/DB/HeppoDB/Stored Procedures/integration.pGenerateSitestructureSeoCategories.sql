
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSitestructureSeoCategories]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@ChannelId			INT
AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Underkategori>
		-- Title>
		-- Formel: Underkategori Huvudkategori & Skor online på nätet från Heppo.se			
		------------------------------------------------------------
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		DECLARE @StartPageMenContentPageId INT
		DECLARE @StartPageWomenContentPageId INT
		DECLARE @StartPageChildrenContentPageId INT
		DECLARE @StartPageMenCategoriesContentPageId INT
		DECLARE @StartPageWomenCategoriesContentPageId INT
		DECLARE @StartPageChildrenCategoriesContentPageId INT
		
		DECLARE @StartPageAllContentPageId INT
		DECLARE @StartPageAllCategoriesContentPageId INT
		
		
		SET @SiteStructureRegistryId = (Select SiteStructureRegistryId
											from sitestructure.tSiteStructureModuleChannel																					
											where ChannelId = @ChannelId)
												
		SET @StartPageMenContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageMen')
									
		SET @StartPageWomenContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageWomen')
					
		SET @StartPageChildrenContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageChildren')
		
		SET @StartPageAllContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'allShoes')
									
		SET @StartPageMenCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'menCategories')
							
		SET @StartPageWomenCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'womenCategories')
							
		SET @StartPageChildrenCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'childrenCategories')
								
		SET @StartPageAllCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'allCategories')
									
		-- DEFAULT --
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n
		where 
			n.SiteStructureRegistryId = @SiteStructureRegistryId
			and n.ContentNodeTypeId = 3 -- contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
					

		UPDATE
			cps
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[Underkategori]', cn1.Title),
			@NewTitle = REPLACE(@NewTitle, '[Huvudkategori]', cn3.Title),
				
			cps.Title = @NewTitle,
			
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[Underkategori]', cn1.Title),
			@NewDescription = REPLACE(@NewDescription, '[Huvudkategori]', cn3.Title),
			
			cps.[Description] = @NewDescription
			--select cps.title, cps.description, cn1.title, cn2.title, cn3.title, cn1.contentnodeid
		FROM			
			sitestructure.tContentPageSeoSetting cps			
			--inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId
			--inner join sitestructure.tContentNode cn2 on cn2.ContentNodeId = cn1.ParentContentNodeId
			--inner join sitestructure.tContentNode cn3 on cn3.ContentNodeId = cn2.ParentContentNodeId
			
			inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId				
			inner join sitestructure.tContentNode cn2 on cn1.ParentContentNodeId = cn2.ContentNodeId
			inner join sitestructure.tContentNode cn3 on cn1.ParentContentNodeId = cn2.ContentNodeId						
		WHERE
			cn1.SiteStructureRegistryId = @SiteStructureRegistryId
			and ((cn3.ContentNodeId = @StartPageWomenContentPageId and cn2.ContentNodeId = @StartPageWomenCategoriesContentPageId) -- damskor, kategorier
			or   (cn3.ContentNodeId = @StartPageMenContentPageId and cn2.ContentNodeId = @StartPageMenCategoriesContentPageId) -- herrskor, kategorier
			or   (cn3.ContentNodeId = @StartPageChildrenContentPageId and cn2.ContentNodeId = @StartPageChildrenCategoriesContentPageId) -- Barnskor
			or   (cn3.ContentNodeId = @StartPageAllContentPageId and cn2.ContentNodeId = @StartPageAllCategoriesContentPageId)) -- Alla kategorier
			and
				((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
			
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
