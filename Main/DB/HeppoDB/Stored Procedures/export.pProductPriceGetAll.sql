SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pProductPriceGetAll]
	@ChannelId INT
AS 
BEGIN 
	SELECT
		*
	FROM
		[export].[vProductPrice] pp
	WHERE
		pp.[ProductPrice.ChannelId] = @ChannelId
END
GO
