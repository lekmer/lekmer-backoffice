SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [automatedtest].[pCampaignFolderGetByTitle]
	@Title nvarchar(50)
as
begin
	select
		*
	from
		[campaign].vCustomCampaignFolder
	where
		[CampaignFolder.Title] = @Title
end

GO
