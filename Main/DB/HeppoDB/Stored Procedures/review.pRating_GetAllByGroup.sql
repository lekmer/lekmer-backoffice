
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRating_GetAllByGroup]
	@RatingGroupId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingGroupRating] rgr
		INNER JOIN [review].[vRating] r ON r.[Rating.RatingId] = rgr.[RatingGroupRating.RatingId]
	WHERE
		rgr.[RatingGroupRating.RatingGroupId] = @RatingGroupId
		AND
		r.[Rating.Channel.Id] = @ChannelId
	ORDER BY
		rgr.[RatingGroupRating.Ordinal]
END
GO
