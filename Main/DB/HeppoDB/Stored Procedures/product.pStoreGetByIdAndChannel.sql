SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pStoreGetByIdAndChannel]
	@ChannelId	int,
	@StoreId	int
as
begin

	select
		s.*
	from
		[product].[vCustomStore] s
	where
		s.[Store.Id] = @StoreId
		and s.[Store.StatusId] = 0

end



GO
