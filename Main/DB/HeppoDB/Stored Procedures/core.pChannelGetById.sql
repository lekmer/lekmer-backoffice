SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pChannelGetById]
	@ChannelId int
as
begin
	set nocount on

	select
		*
	from
		[core].[vCustomChannel]
	where
		[Channel.Id] = @ChannelId
end

GO
