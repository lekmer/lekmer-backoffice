SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [import].[pPopulateTagInfoHeppo]
	@FileName NVARCHAR(MAX)
AS
BEGIN
	DELETE FROM import.tTagInfoHeppo
	
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'BULK INSERT import.tTagInfoHeppo FROM ''' + @FileName + ''' WITH (FIELDTERMINATOR = ''\t'', ROWTERMINATOR = ''\n'', CODEPAGE = ''ACP'')';
	
	EXEC(@sql);
END
GO
