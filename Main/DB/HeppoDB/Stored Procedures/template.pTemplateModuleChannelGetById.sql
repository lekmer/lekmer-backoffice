SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateModuleChannelGetById]
@ChannelId	int
AS
BEGIN
	SELECT	
		*
	FROM	
		template.vCustomTemplateModuleChannel
	WHERE 
		[TemplateModuleChannel.Id] = @ChannelId
END

GO
