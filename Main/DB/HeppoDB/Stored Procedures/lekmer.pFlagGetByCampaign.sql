SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagGetByCampaign]
@LanguageId int,
@CampaignId int
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @FlagId int
	SET @FlagId = (SELECT FlagId FROM lekmer.tCampaignFlag WHERE CampaignId = @CampaignId)
	IF(@FlagId IS NULL)
	BEGIN
		SET @FlagId = (SELECT cff.FlagId FROM lekmer.tCampaignFolderFlag cff INNER JOIN campaign.tCampaign c ON cff.FolderId = c.FolderId WHERE c.CampaignId = @CampaignId)
	END
	SELECT *
	FROM lekmer.vFlag
	WHERE [Flag.Id] = @FlagId and [Flag.LanguageId] = @LanguageId
END

GO
