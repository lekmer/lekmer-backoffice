SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionSave]
	@CartActionId			INT,
	@IncludeAllProducts		BIT
AS
BEGIN
	UPDATE
		[lekmer].[tCartItemGroupFixedDiscountAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tCartItemGroupFixedDiscountAction] (
			CartActionId,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@IncludeAllProducts
		)
	END
END

GO
