SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pProductAutoFreeCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vProductAutoFreeCartAction]
	WHERE
		[ProductAutoFreeCartAction.CartActionId] = @CartActionId
END
GO
