SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartActionDelete]
	@CartActionId int
AS 
BEGIN	
	DELETE FROM campaign.tCartAction
	WHERE CartActionId = @CartActionId
END
GO
