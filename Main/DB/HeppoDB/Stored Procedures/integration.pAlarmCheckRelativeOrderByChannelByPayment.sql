SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pAlarmCheckRelativeOrderByChannelByPayment]
AS
BEGIN
	SET NOCOUNT ON
	
	
    
declare @intervalLength int
set @intervalLength = 240

-- man jämför 0 till - 30 med -30 min till -60 min
-- 40% critical inom intervallet = critical
-- 30% warning

select 
	z.OrderCount as OrderCount2,
	m.Title as Channel,
	h.Title as PaymentType,
	isnull(x.OrderCount, 0) as OrderCount1,
	x.ChannelId,
	x.PaymentTypeId,
	cast(case when isnull(x.OrderCount, 0) = 0 then -100 else 100*(cast(x.OrderCount as decimal(12,2)) - (cast(z.OrderCount as decimal(12,2)))) / cast(z.OrderCount as decimal(12,2)) end as int) as 'Quota %'
from
		(select
			count(*) as 'OrderCount',
			o.ChannelId,
			p.PaymentTypeId
		from
			[order].tOrder o
			inner join [order].tOrderPayment p
				on p.OrderId = o.OrderId
		where
			o.CreatedDate between dateadd(minute, -(@intervalLength*2), getdate()) and dateadd(minute, -(@intervalLength), getdate()) and
			o.OrderStatusId = 4
		group by
			o.ChannelId, p.PaymentTypeId) z
	left join 
		(select
			count(*) as 'OrderCount',
			o.ChannelId,
			p.PaymentTypeId
		from
			[order].tOrder o
			inner join [order].tOrderPayment p
				on p.OrderId = o.OrderId
		where
			o.CreatedDate between dateadd(minute, -(@intervalLength), getdate()) and dateadd(minute, -0, getdate()) and
			o.OrderStatusId = 4
		group by
			o.ChannelId, p.PaymentTypeId) x
	on z.ChannelId = x.ChannelId
	and z.PaymentTypeId = x.PaymentTypeId
	
	inner join [order].tPaymentType h
	on h.PaymentTypeId = z.PaymentTypeId
	inner join core.tChannel m
	on m.ChannelId = z.ChannelId
   
END

GO
