SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductRelationListItemGetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT
		*
	FROM 
		product.vCustomBlockProductRelationListItem brli
	WHERE
		brli.[BlockProductRelationListItem.BlockId] = @BlockId
END

GO
