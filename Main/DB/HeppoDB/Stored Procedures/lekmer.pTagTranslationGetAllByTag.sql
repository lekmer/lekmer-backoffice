SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pTagTranslationGetAllByTag]
@TagId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [TagId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[Value] AS 'Value'
	FROM
	    lekmer.tTagTranslation where [TagId] = @TagId
END




GO
