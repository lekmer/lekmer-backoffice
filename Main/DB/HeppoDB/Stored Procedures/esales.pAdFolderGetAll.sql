SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pAdFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[esales].[vAdFolder] rf
END
GO
