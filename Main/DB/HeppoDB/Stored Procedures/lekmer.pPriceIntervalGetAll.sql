SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pPriceIntervalGetAll]
	@CurrencyId int,
	@LanguageId int
AS
begin
	set nocount on

	select
		*
	from
		lekmer.[vPriceInterval]
	where
		[Currency.Id] = @CurrencyId
		AND [PriceInterval.LanguageId] = @LanguageId
end

GO
