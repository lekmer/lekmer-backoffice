SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListGetById]
@PriceListId int
AS
BEGIN
	SELECT
		*
	FROM
		[product].[vCustomPriceList] pl
	WHERE pl.[PriceList.Id] = @PriceListId
END

GO
