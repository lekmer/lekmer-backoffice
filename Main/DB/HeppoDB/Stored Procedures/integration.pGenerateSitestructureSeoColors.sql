
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSitestructureSeoColors]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@ChannelId			INT
AS
begin
	set nocount on
	begin try
		begin transaction				
				
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		DECLARE @StartPageMenAccessoriesContentPageId INT
		DECLARE @StartPageWomenAccessoriesContentPageId INT
		DECLARE @StartPageChildrenAccessoriesContentPageId INT
		DECLARE @StartPageMenAccessoriesColorsContentPageId INT
		DECLARE @StartPageWomenAccessoriesColorsContentPageId INT
		DECLARE @StartPageAccessoriesChildrenColorsContentPageId INT		
		DECLARE @StartPageAllAccessoriesContentPageId INT
		DECLARE @StartPageAllAccessoriesColorsContentPageId INT
		
		DECLARE @StartPageMenContentPageId INT
		DECLARE @StartPageWomenContentPageId INT
		DECLARE @StartPageChildrenContentPageId INT
		DECLARE @StartPageMenColorsContentPageId INT
		DECLARE @StartPageWomenColorsContentPageId INT
		DECLARE @StartPageChildrenColorsContentPageId INT
		
		DECLARE @StartPageAllContentPageId INT
		DECLARE @StartPageAllColorsContentPageId INT
		
		SET @SiteStructureRegistryId = (Select SiteStructureRegistryId
											from sitestructure.tSiteStructureModuleChannel																					
											where ChannelId = @ChannelId)
						
												
		SET @StartPageMenAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessoriesMen')
									
		SET @StartPageWomenAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessoriesWomen')
					
		SET @StartPageChildrenAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessoriesChildren')
		
		SET @StartPageAllAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessories')

		SET @StartPageMenContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageMen')
									
		SET @StartPageWomenContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageWomen')
									
		SET @StartPageChildrenContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageChildren')
									
		SET @StartPageAllContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'allShoes')




		SET @StartPageMenColorsContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'menColors')
									
		SET @StartPageWomenColorsContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'womenColors')
									
		SET @StartPageChildrenColorsContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'childrenColors')
									
		SET @StartPageAllColorsContentPageId  = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'allColors')
									
						
		SET @StartPageMenAccessoriesColorsContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'menAccessoriesColors')
							
		SET @StartPageWomenAccessoriesColorsContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'womenAccessoriesColors')
							
		--SET @StartPageAccessoriesChildrenCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									--and CommonName = 'childrenCategories')
								
		SET @StartPageAllAccessoriesColorsContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'allAccessoriesColors')
									
		-- DEFAULT --
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n
		where 
			n.SiteStructureRegistryId = @SiteStructureRegistryId
			and n.ContentNodeTypeId = 3 -- contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
					

		UPDATE
			cps
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[Color]', cn1.Title),
				
			cps.Title = @NewTitle,
			
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[Color]', cn1.Title),
			
			cps.[Description] = @NewDescription
			--select cps.title, cps.description, cn1.title, cn2.title, cn3.title, cn1.contentnodeid
		FROM			
			sitestructure.tContentPageSeoSetting cps					
			inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId				
			inner join sitestructure.tContentNode cn2 on cn1.ParentContentNodeId = cn2.ContentNodeId
			inner join sitestructure.tContentNode cn3 on cn1.ParentContentNodeId = cn2.ContentNodeId						
		WHERE
			cn1.SiteStructureRegistryId = @SiteStructureRegistryId
			and ((cn3.ContentNodeId = @StartPageWomenAccessoriesContentPageId and cn2.ContentNodeId = @StartPageWomenAccessoriesColorsContentPageId) -- damskor accessories, färger
			or   (cn3.ContentNodeId = @StartPageMenAccessoriesContentPageId and cn2.ContentNodeId = @StartPageMenAccessoriesColorsContentPageId) -- herrskor accessories, färger			
			or   (cn3.ContentNodeId = @StartPageAllAccessoriesContentPageId and cn2.ContentNodeId = @StartPageAllAccessoriesColorsContentPageId) -- Alla accessories färger
			or   (cn3.ContentNodeId = @StartPageWomenContentPageId and cn2.ContentNodeId = @StartPageWomenColorsContentPageId) -- damskor färger
			or   (cn3.ContentNodeId = @StartPageMenContentPageId and cn2.ContentNodeId = @StartPageMenColorsContentPageId) -- herrskor, färger
			or   (cn3.ContentNodeId = @StartPageChildrenContentPageId and cn2.ContentNodeId = @StartPageChildrenColorsContentPageId) -- Barnskor färger
			or   (cn3.ContentNodeId = @StartPageAllContentPageId and cn2.ContentNodeId = @StartPageAllColorsContentPageId)) -- Alla färger
			
			--or   (cn3.ContentNodeId = @StartPageChildrenContentPageId and cn2.ContentNodeId = @StartPageChildrenCategoriesContentPageId) -- Barnskor
			and
				((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))						

			
	commit transaction
	end try
	begin catch		
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
