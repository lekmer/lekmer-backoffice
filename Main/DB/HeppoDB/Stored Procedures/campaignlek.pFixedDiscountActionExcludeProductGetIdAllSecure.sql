SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductGetIdAllSecure]
	@ProductActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tFixedDiscountActionExcludeProduct] fdaep
		INNER JOIN [product].[tProduct] p ON fdaep.[ProductId] = p.[ProductId]
	WHERE
		fdaep.[ProductActionId] = @ProductActionId
		AND p.[IsDeleted] = 0
END
GO
