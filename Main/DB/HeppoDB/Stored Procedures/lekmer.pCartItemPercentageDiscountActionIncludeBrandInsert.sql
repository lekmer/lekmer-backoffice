SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
