SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageProductDelete]
	@PackageId	INT
AS
BEGIN
	DELETE FROM [productlek].[tPackageProduct]
	WHERE [PackageId] = @PackageId
END
GO
