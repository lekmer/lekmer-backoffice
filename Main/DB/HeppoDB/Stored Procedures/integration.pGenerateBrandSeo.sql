
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateBrandSeo]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@ChannelId			INT
AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Varumärke>		
		------------------------------------------------------------
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		DECLARE @BrandContentPageId INT
		DECLARE @LanguageId int
		
		SET @LanguageId = (select LanguageId from core.tChannel where ChannelId = @ChannelId)
		
		-- DEFAULT --
		INSERT INTO sitestructure.tContentPageSeoSetting(ContentNodeId)
		SELECT
			c.ContentNodeId
		FROM
			sitestructure.tContentPage c
		WHERE
			c.ContentNodeId not in (select ContentNodeId
									from sitestructure.tContentPageSeoSetting)	
	
		
		INSERT INTO sitestructure.tContentPageSeoSettingTranslation(ContentNodeId, LanguageId)
		SELECT
			c.ContentNodeId,
			@languageId
		FROM
			sitestructure.tContentPage c
		WHERE
			not exists (SELECT 1
							FROM sitestructure.tContentPageSeoSettingTranslation n
							WHERE n.ContentNodeId = c.ContentNodeId and
							n.LanguageId = @LanguageId)
	
		SET @SiteStructureRegistryId = (Select SiteStructureRegistryId
											from sitestructure.tSiteStructureModuleChannel																					
											where ChannelId = @ChannelId)		
												
		SET @BrandContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageBrands')
		
																											
		UPDATE
			cps
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[BrandTitle]', cn1.Title),
				
			cps.Title = @NewTitle,
			
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[BrandTitle]', cn1.Title),
			
			cps.[Description] = @NewDescription
			--select cps.title, cps.description, cn1.title, cn2.title, cn3.title, cn1.contentnodeid
		FROM			
			sitestructure.tContentPageSeoSetting cps			
			--inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId
			--inner join sitestructure.tContentNode cn2 on cn2.ContentNodeId = cn1.ParentContentNodeId
			--inner join sitestructure.tContentNode cn3 on cn3.ContentNodeId = cn2.ParentContentNodeId
			
			inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId				
			inner join sitestructure.tContentNode cn2 on cn1.ParentContentNodeId = cn2.ContentNodeId
			inner join sitestructure.tContentNode cn3 on cn1.ParentContentNodeId = cn2.ContentNodeId						
		WHERE
			cn1.SiteStructureRegistryId = @SiteStructureRegistryId
			and cn3.ContentNodeId = @BrandContentPageId -- skomärken
			and (cn2.ContentNodeId = @BrandContentPageId or  cn2.ParentContentNodeId = @BrandContentPageId)
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
		
		
														

	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
