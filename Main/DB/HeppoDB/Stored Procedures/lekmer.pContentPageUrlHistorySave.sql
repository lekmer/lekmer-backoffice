
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistorySave]
	@ContentPageId				INT,
	@UrlTitleOld				NVARCHAR(256),
	@HistoryLifeIntervalTypeId	INT
AS 
BEGIN 
	INSERT INTO lekmer.tContentPageUrlHistory (
		ContentPageId,
		UrlTitleOld,
		HistoryLifeIntervalTypeId
	)
	VALUES (
		@ContentPageId,
		@UrlTitleOld,
		@HistoryLifeIntervalTypeId
	)
	
	RETURN @@IDENTITY
END
GO
