SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockProductRelationListSave]
	@BlockId			int,
	@ColumnCount		int,
	@RowCount			int,
	@ProductSortOrderId	int
as
begin
	
	update
		[product].[tBlockProductRelationList]
	set
		[ColumnCount]			= @ColumnCount,
		[RowCount]				= @RowCount,
		[ProductSortOrderId]	= @ProductSortOrderId
	where
		[BlockId]		= @BlockId
		
	if  @@ROWCOUNT = 0
	begin
		insert
			[product].[tBlockProductRelationList]
			(
				[BlockId],
				[ColumnCount],
				[RowCount],
				[ProductSortOrderId]
			)
		values
			(
				@BlockId,
				@ColumnCount,
				@RowCount,
				@ProductSortOrderId
			)
	end
end
GO
