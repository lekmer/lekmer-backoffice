SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockContactUsSave]
	@BlockId	INT,
	@Receiver	VARCHAR(320)
AS
BEGIN
	UPDATE
		[lekmer].[tBlockContactUs]
	SET
		[Receiver]	= @Receiver
	WHERE
		[BlockId] = @BlockId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBlockContactUs]
		( 
			[BlockId], 
			[Receiver] 
		)
		VALUES
		(
			@BlockId, 
			@Receiver 
		)
	END
END
GO
