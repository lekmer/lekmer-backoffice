SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [order].[pOrderAddressSave]
		@OrderAddressId int,
		@Addressee NVARCHAR(100),
		@StreetAddress NVARCHAR(200),
		@StreetAddress2 NVARCHAR(200),
		@PostalCode NVARCHAR(10),
		@City NVARCHAR(100),
		@CountryId INT,
		@PhoneNumber NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[order].[tOrderAddress]
	SET 
			Addressee = @Addressee,
			StreetAddress = @StreetAddress,
			StreetAddress2 = @StreetAddress2,
			PostalCode = @PostalCode,
			City = @City,
			CountryId = @CountryId,
			PhoneNumber = @PhoneNumber
	WHERE	
			OrderAddressId = @OrderAddressId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [order].[tOrderAddress]
		(
			Addressee,
			StreetAddress,
			StreetAddress2,
			PostalCode,
			City,
			CountryId,
			PhoneNumber
		)
		VALUES
		(
			@Addressee,
			@StreetAddress,
			@StreetAddress2,
			@PostalCode,
			@City,
			@CountryId,
			@PhoneNumber
		)
		SET	@OrderAddressId = SCOPE_IDENTITY();
	END
	RETURN  @OrderAddressId;	
END




GO
