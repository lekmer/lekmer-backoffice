SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerSearch]
	@ErpId          varchar(50),
	@FirstName      nvarchar(64),
	@LastName       nvarchar(64),
	@CivicNumber    varchar(50),
	@Email          nvarchar(64),
	@PhoneHome      varchar(50),
	@PhoneMobile    varchar(50),
	@CreatedFrom    datetime,
	@CreatedTo      datetime,
	@StatusId       int,
	@Separator      char(1),
	@Page           int = NULL,
	@PageSize       int
as   
begin
    set nocount on

	DECLARE @sqlGeneral nvarchar(max)
	declare @sql nvarchar(max)
	declare @sqlCount nvarchar(max)
	declare @sqlFragment nvarchar(max)
	
	set @sqlFragment = '
		select
			row_number() over (order by c.CustomerId desc) AS _Number,
			c.CustomerId
		from
			[customer].[tCustomer] c
			inner join [customer].[tCustomerInformation] ci
				on c.CustomerId = ci.CustomerId
		where
			1 = 1'
			+ case when (@ErpId is not null) then + ' and c.ErpId = @ErpId' else '' end
			+ case when (@StatusId is not null) then + ' and c.CustomerStatusId = @StatusId' else '' end
			+ case when (@FirstName is not null) then + ' and contains(ci.FirstName, @FirstName)' else '' end
			+ case when (@LastName is not null) then + ' and contains(ci.LastName, @LastName)' else '' end
			+ case when (@CivicNumber is not null) then + ' and ci.CivicNumber = @CivicNumber' else '' end
			+ case when (@Email is not null) then + ' and ci.Email = @Email' else '' end
			+ case when (@PhoneHome is not null) then + ' and ci.PhoneNumber = @PhoneHome' else '' end
			+ case when (@PhoneMobile is not null) then + ' and ci.CellPhoneNumber = @PhoneMobile' else '' end
			+ case when (@CreatedFrom is not null) then + ' and ci.InformationCreatedDate >= @CreatedFrom' else '' end
			+ case when (@CreatedTo is not null) then + ' and ci.InformationCreatedDate <= @CreatedTo' else '' end

	SET @sqlGeneral = '
		IF (@FirstName IS NOT NULL)
		BEGIN
			SET @FirstName = generic.fPrepareFulltextSearchParameter(@FirstName)
		END
		IF (@LastName IS NOT NULL)
		BEGIN
			SET @LastName = generic.fPrepareFulltextSearchParameter(@LastName)
		END'

	set @sqlCount = @sqlGeneral + '
		select count(*) from (' + @sqlFragment + ') as CountResults'

	set @sql = @sqlGeneral + '
		select
			c.*
		from
			(' + @sqlFragment + ') as result
			inner join [customer].[vCustomCustomerSecure] c
				on c.[Customer.CustomerId] = result.CustomerId
		where
			_Number > ' + cast((@Page - 1) * @PageSize AS varchar(10)) + '
			and _Number <= ' + cast(@Page * @PageSize AS varchar(10)) + '
		order by
			result.CustomerId desc'
	

	exec sp_executesql @sqlCount,
		N'	@ErpId          varchar(50),
			@FirstName      nvarchar(64),
			@LastName       nvarchar(64),
			@CivicNumber    varchar(50),
			@Email          nvarchar(64),
			@PhoneHome      varchar(50),
			@PhoneMobile    varchar(50),
			@CreatedFrom    datetime,
			@CreatedTo      datetime,
			@StatusId       int',
			@ErpId,          
			@FirstName, 
			@LastName,     
			@CivicNumber,
			@Email,  
			@PhoneHome, 
			@PhoneMobile,  
			@CreatedFrom,  
			@CreatedTo,
			@StatusId

	exec sp_executesql @sql, 
		N'	@ErpId          varchar(50),
			@FirstName      nvarchar(64),
			@LastName       nvarchar(64),
			@CivicNumber    varchar(50),
			@Email          nvarchar(64),
			@PhoneHome      varchar(50),
			@PhoneMobile    varchar(50),
			@CreatedFrom    datetime,
			@CreatedTo      datetime,
			@StatusId       int',
			@ErpId,          
			@FirstName, 
			@LastName,     
			@CivicNumber,
			@Email,  
			@PhoneHome, 
			@PhoneMobile,  
			@CreatedFrom,  
			@CreatedTo,
			@StatusId
end

GO
