SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceSave]
	@CartActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN
	UPDATE
		addon.tCartItemPriceActionCurrency
	SET 
		[Value] = @Value
	WHERE 
		CartActionId = @CartActionId AND 
		CurrencyId = @CurrencyId
		
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			addon.tCartItemPriceActionCurrency
		( 
			CartActionId,
			CurrencyId,
			[Value]
		)
		VALUES 
		(
			@CartActionId,
			@CurrencyId,
			@Value
		)
	END
END
GO
