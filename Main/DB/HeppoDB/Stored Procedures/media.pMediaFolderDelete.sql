SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-25
-- Description:	Deletes MediaFolder by Id
-- =============================================
CREATE PROCEDURE [media].[pMediaFolderDelete]
	@MediaFolderId INT
AS
BEGIN
	SET NOCOUNT ON
	DELETE 
	FROM   [media].[tMediaFolder]
	WHERE  [MediaFolderId] = @MediaFolderId
END

GO
