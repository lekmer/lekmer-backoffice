SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductGetAllByPackageSecure]
	@ChannelId		INT,
	@PackageId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId
	SELECT @ProductRegistryId = [ProductRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId

	SELECT
		[p].*,
		[pli].*
	FROM
		[product].[vCustomProductSecure] p
		INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[ProductId] = [p].[Product.Id]
		LEFT JOIN [product].[tProductRegistryProduct] prp ON [prp].[ProductId] = [p].[Product.Id] AND [prp].[ProductRegistryId] = @ProductRegistryId
		LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = @ChannelId
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Product.Id]
														  AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
																							@CurrencyId,
																							[p].[Product.Id],
																							[pmc].[PriceListRegistryId],
																							NULL)
	WHERE
		[pp].[PackageId] = @PackageId
		AND [p].[Product.IsDeleted] = 0
END
GO
