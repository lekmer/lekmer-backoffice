SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconTranslationDelete]
	@IconId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [lekmer].[tIconTranslation]
	WHERE [IconId] = @IconId
END
GO
