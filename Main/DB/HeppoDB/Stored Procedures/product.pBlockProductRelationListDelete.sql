SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockProductRelationListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM 
		product.tBlockProductRelationList
	WHERE
		BlockId = @BlockId
END
GO
