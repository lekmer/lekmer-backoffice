SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureRegistryGetAllByBrand]
	@BrandId	int
as
begin
	select
		*
	FROM
		lekmer.vBrandSiteStructureRegistry
	where 
		[BrandSiteStructureRegistry.BrandId] = @BrandId
end

GO
