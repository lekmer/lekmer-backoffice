SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeBrandDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionExcludeBrand]
	WHERE
		ConfigId = @ConfigId
END
GO
