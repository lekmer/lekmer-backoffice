SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [lekmer].[pLekmerCartUpdateCreatedDate]
	@CartGuid		UNIQUEIDENTIFIER,
	@CreatedDate	DATETIME
as
begin
	SET NOCOUNT ON

	update [order].tCart
	
	set 
		
		CreatedDate=@CreatedDate
	
	where CartGuid=@CartGuid
end

GO
