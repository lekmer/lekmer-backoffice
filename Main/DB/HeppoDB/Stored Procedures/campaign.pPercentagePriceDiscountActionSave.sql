
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@DiscountAmount		DECIMAL(16, 2),
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaign].[tPercentagePriceDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaign].[tPercentagePriceDiscountAction] (
			ProductActionId,
			DiscountAmount,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@DiscountAmount,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
