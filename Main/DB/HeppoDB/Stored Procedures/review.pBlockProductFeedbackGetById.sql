SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockProductFeedbackGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		bpf.*,
		b.*
	FROM 
		[review].[vBlockProductFeedback] AS bpf
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bpf.[BlockProductFeedback.BlockId] = b.[Block.BlockId]
	WHERE
		bpf.[BlockProductFeedback.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
