SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pAddressTypeGetById]
@AddressTypeId int
as	
begin
set nocount on
	select
		*
	from
		customer.vCustomAddressType
	where 
		[AddressType.AddressTypeId] = @AddressTypeId
end

GO
