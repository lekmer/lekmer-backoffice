SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListBrandDeleteAll]
	@BlockId	int
AS 
BEGIN 
	DELETE lekmer.tBlockBrandListBrand
	WHERE BlockId = @BlockId
END 
GO
