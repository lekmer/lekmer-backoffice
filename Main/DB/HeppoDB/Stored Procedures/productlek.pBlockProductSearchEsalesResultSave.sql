
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultSave]
	@BlockId INT,
	@SecondaryTemplateId INT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[productlek].[tBlockProductSearchEsalesResult]
	SET	
		[SecondaryTemplateId] = @SecondaryTemplateId
	WHERE
		[BlockId] = @BlockId
	
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[productlek].[tBlockProductSearchEsalesResult]
			( [BlockId],
			  [SecondaryTemplateId]
			)
		VALUES
			( @BlockId,
			  @SecondaryTemplateId
			)
	END
END
GO
