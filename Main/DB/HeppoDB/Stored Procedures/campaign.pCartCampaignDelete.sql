SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartCampaignDelete]
	@CampaignId int
AS 
BEGIN	
	DELETE FROM campaign.tCartCampaign
	WHERE CampaignId = @CampaignId
END
GO
