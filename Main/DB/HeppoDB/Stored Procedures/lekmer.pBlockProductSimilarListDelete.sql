SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockProductSimilarListDelete]
	@BlockId INT
AS
BEGIN
	DELETE lekmer.tBlockProductSimilarList
	WHERE BlockId = @BlockId
END
GO
