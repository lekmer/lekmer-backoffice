SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [corelek].[pCacheUpdateSave]
	@ManagerName	VARCHAR(100),
	@UpdateType		INT,
	@Key			VARCHAR(500),
	@CreationDate	DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT [corelek].[tCacheUpdate]
	(
		[ManagerName],
		[UpdateType],
		[Key],
		[CreationDate],
		[InsertionDate]
	)
	VALUES
	(
		@ManagerName,
		@UpdateType,
		@Key,
		@CreationDate,
		GETUTCDATE()
	)
END
GO
