SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [product].[pBlockProductSearchResultSave]
	@BlockId		int,
	@ColumnCount	int,
	@RowCount		int
AS
BEGIN
	
	UPDATE
		[product].[tBlockProductSearchResult]
	SET
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount
	WHERE
		[BlockId] = @BlockId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			[product].[tBlockProductSearchResult]
			(
				[BlockId],
				[ColumnCount],
				[RowCount]
			)
		VALUES
			(
				@BlockId,
				@ColumnCount,
				@RowCount
			)
	END
END




GO
