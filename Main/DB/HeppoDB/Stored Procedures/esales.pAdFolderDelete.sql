SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pAdFolderDelete]
	@AdFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [esales].[tAdFolder]
	WHERE [AdFolderId] = @AdFolderId
END
GO
