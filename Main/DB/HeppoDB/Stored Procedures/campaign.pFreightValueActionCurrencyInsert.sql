SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pFreightValueActionCurrencyInsert]
	@CartActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
as
begin
	INSERT
		campaign.tFreightValueActionCurrency
	(
		CartActionId,
		CurrencyId,
		Value
	)
	VALUES
	(
		@CartActionId,
		@CurrencyId,
		@Value
	)
end



GO
