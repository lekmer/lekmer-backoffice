SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pBackofficeProductInfoImportDeleteByDate]
	@Days int
AS
begin
	set nocount on
	
	-- Current date.
	declare @CurrentDate as datetime
	set @CurrentDate = GETDATE()
	
	-- Deletion date.
	declare @DeletionDate as datetime
	set @DeletionDate = DATEADD(day, -@Days, @CurrentDate)
	
	-- Delete old rows.
	delete from integration.tBackofficeProductInfoImport
	where [InsertedDate] < @DeletionDate	 
end
GO
