SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure  [lekmer].[pWishListSave]
	@WishListKey uniqueidentifier,
	@WishList		varchar(500)
as
begin
	update [lekmer].[tWishList] 
	set
		WishList = @WishList
	where
		WishListKey = @WishListKey
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert [lekmer].[tWishList] 
	(
		WishListKey,
		WishList
	)
	values
	(
		@WishListKey,
		@WishList
	)
end
GO
