SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentEntityDelete]
	@FragmentId INT,
	@EntityId	INT
AS
begin
	set nocount ON
	DELETE [template].[tModelFragmentEntity] WHERE EntityId = @EntityId	AND ModelFragmentId = @FragmentId
	RETURN SCOPE_IDENTITY()
end	




GO
