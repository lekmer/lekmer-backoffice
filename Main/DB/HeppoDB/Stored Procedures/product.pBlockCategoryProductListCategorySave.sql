SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockCategoryProductListCategorySave]
	@BlockId				int,
	@CategoryId				int,
	@IncludeSubcategories	bit
as
BEGIN
	update
		[product].[tBlockCategoryProductListCategory]
	set
		[IncludeSubcategories] = @IncludeSubcategories
	where
		[CategoryId] = @CategoryId
		and [BlockId] = @BlockId
		
	if  @@ROWCOUNT = 0
		begin
			insert
				[product].[tBlockCategoryProductListCategory]
				(
					[BlockId],
					[CategoryId],
					[IncludeSubcategories]
				)
			values
				(
					@BlockId,
					@CategoryId,
					@IncludeSubcategories
				)
		END
		
end



GO
