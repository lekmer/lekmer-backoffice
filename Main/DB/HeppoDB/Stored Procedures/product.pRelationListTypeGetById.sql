SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Roman A.		Date: 04.09.2008		Time: 13:00
Description: 
	Added CommonName
*/

CREATE PROCEDURE [product].[pRelationListTypeGetById]
	@RelationListTypeId		int
as
begin
	set nocount on

	select
		*
	from
		product.vCustomRelationListType rlt
	where
		rlt.[RelationListType.Id] = @RelationListTypeId
end

GO
