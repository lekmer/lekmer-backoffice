SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerGroupCustomerSave]
@CustomerGroupId	int,
@CustomerId			int
AS
BEGIN
	INSERT INTO 
		customer.tCustomerGroupCustomer
		(
			CustomerGroupId,
			CustomerId
		)
	VALUES
		(
			@CustomerGroupId,
			@CustomerId
		)
END













GO
