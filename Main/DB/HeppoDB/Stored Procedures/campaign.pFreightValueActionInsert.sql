SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure campaign.pFreightValueActionInsert
	@CartActionId int
as
begin
	insert
		campaign.tFreightValueAction
	(
		CartActionId
	)
	values
	(
		@CartActionId
	)
end
GO
