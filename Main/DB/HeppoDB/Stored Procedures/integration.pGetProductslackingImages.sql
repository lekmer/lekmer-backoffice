SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGetProductslackingImages]

AS
begin
	set nocount on
	
		SELECT 
			l.ProductId, l.HYErpId, pp.ProductId
        FROM
			lekmer.tLekmerProduct l WITH (NOLOCK)
			inner join product.tProduct p WITH (NOLOCK)
				on l.ProductId = p.ProductId
			left join product.tProductImage pp WITH (NOLOCK)
				on p.ProductId = pp.ProductId				
        WHERE 
			pp.ProductId is null
		
	
end
GO
