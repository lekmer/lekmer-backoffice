SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pStoreProductGetAllByProductAndChannel]
	@ChannelId int,
	@ProductId int
AS 
BEGIN 
	select 
		sp.*
	from
		product.vCustomStoreProduct sp
		inner join
			product.vCustomStore s
			on sp.[StoreProduct.StoreId] = s.[Store.Id]
	where
		sp.[StoreProduct.ProductId] = @ProductId
		and s.[Store.StatusId] = 0
END 



GO
