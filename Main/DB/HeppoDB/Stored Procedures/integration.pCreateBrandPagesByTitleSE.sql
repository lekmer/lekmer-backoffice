
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pCreateBrandPagesByTitleSE]
	@BrandTitle nvarchar(50)

AS
begin
	set nocount on
		
	begin try
		begin transaction
		
		
		--declare @BrandTitle nvarchar(50)
		--set @BrandTitle = 'TestBrabds5'
		
		DECLARE @tmp TABLE (ContentNodeId int not null)
		DECLARE @tmpBlockId TABLE (BlockId int not null)
		
		-- tContentNode
		insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
					Ordinal, CommonName, AccessId, SiteStructureRegistryId)
		output inserted.ContentNodeId INTO @tmp
		select
			1000080, -- Skomärken, lär vara olika beroende på sitestructure
			3, -- ?
			1, --Offline
			@BrandTitle,
			128,
			LOWER(@BrandTitle),
			1, --All
			1 -- se

		
		-- tContentPage
		insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
					UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
		select
			(select top 1 ContentNodeId from @tmp),
			1, -- se
			1000048, -- BrandsDetailPage
			1000508, -- Xinnan null  1000050
			@BrandTitle,
			REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-')), '&', 'and'),
			1, -- Content page
			0,
			1000057 -- Xinnan null
			
		-- tBlock
		-- Brand Profile
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			18, --BrandList
			0,
			1000018,
			'brand profile',
			1,
			1,
			1000049
			
					-- Brand profile extra content
					insert into lekmer.tBlockBrandList(BlockId, ColumnCount, [RowCount], IncludeAllBrands, LinkContentNodeId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						1,
						1,
						0,
						NULL
				
						-- Brand profile BrandlistBrand extra content
					insert into lekmer.tBlockBrandListBrand(BlockId, BrandId, Ordinal)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle), -- NY
						10

					-- töm listan
					delete from @tmpBlockId
			
		-- Image
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			1, 
			0,
			1000018,
			'Image',
			2,
			1,
			1000096
			
					-- Image extra content
					insert into sitestructure.tBlockRichText(BlockId, Content)
					select
						(select top 1 BlockId from @tmpBlockId),
						NULL
		
					-- töm listan
					delete from @tmpBlockId
					
					
		-- Brand Info
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			18, --BrandList
			0,
			1000018,
			'brand info',
			3,
			1,
			1000095
				
					-- Brand Info extra content
					insert into lekmer.tBlockBrandList(BlockId, ColumnCount, [RowCount], IncludeAllBrands, LinkContentNodeId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						1,
						1,
						0,
						NULL
					
					-- Brand Info BrandListBrand extra content
					insert into lekmer.tBlockBrandListBrand(BlockId, BrandId, Ordinal)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle), -- NY
						10
						
					-- töm listan
					delete from @tmpBlockId
		
		-- Heppo tipsar
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			17, 
			0,
			1000019,
			'Heppo tipsar', -- en case här beroende på channel
			1,
			1,
			1000134
			
					-- Heppo Tipsar extra content
					insert into lekmer.tBlockBrandProductList(BlockId, ColumnCount, [RowCount], ProductSortOrderId)
					select
						(select top 1 BlockId from @tmpBlockId),
						2,
						2,
						2
					
					-- Heppo Tipsar BrandProductListBrand extra content
					insert into lekmer.tBlockBrandProductListBrand(BlockId, BrandId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle) -- NY

					
					-- töm listan
					delete from @tmpBlockId
			
		-- produkter från brand
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			17, 
			0,
			1000021,
			'Märkesskor från ' + @BrandTitle, -- en case här beroende på channel
			1,
			1,
			1000133
			
					-- produkter från brand extra content
					insert into lekmer.tBlockBrandProductList(BlockId, ColumnCount, [RowCount], ProductSortOrderId)
					select
						(select top 1 BlockId from @tmpBlockId),
						4,
						3,
						3
					
					--produkter från brand BrandProductListBrand extra content
					insert into lekmer.tBlockBrandProductListBrand(BlockId, BrandId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle) -- NY
						
						
					-- töm listan
					delete from @tmpBlockId
	
					
				---------------------------
				-- CHILDREN - MAN
				---------------------------
				DECLARE @tmpChildrenMan TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenMan
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' för män',
					2,
					LOWER(@BrandTitle)+'ForMan',
					1, --All
					1 -- se

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenMan),
					1, -- se
					1000713,
					1000708,
					@BrandTitle,
					REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-man'), '&', 'and'),
					1, -- Content page
					0,
					1019559
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenMan),
							1000001,
							0,
							1000195,
							@BrandTitle + ' för män', --,'Brand/category filter',
							1,
							1,
							1000748
								
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, SecondaryTemplateId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000747,
										1000059
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
										
									-- Brand/category filterTags extra content
									insert into lekmer.tBlockProductFilterTag(BlockId, TagId)
									select
										(select top 1 BlockId from @tmpBlockId),
										1000009
									
									-- Rating
									DELETE FROM [review].[tBlockRating]
									WHERE [BlockId] = (select top 1 BlockId from @tmpBlockId)

									INSERT INTO [review].[tBlockRating] ([BlockId], [RatingId])
									select
										(select top 1 BlockId from @tmpBlockId),
										1
									
									-- töm listan
									delete from @tmpBlockId
						
				---------------------------
				-- CHILDREN - Woman
				---------------------------
				DECLARE @tmpChildrenWoman TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenWoman
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' för kvinnor',
					2,
					LOWER(@BrandTitle)+'ForKvinnor',
					1, --All
					1 -- se

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenWoman),
					1, -- se
					1000713,
					1000708,
					@BrandTitle,
					REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-kvinnor'), '&', 'and'),					
					1, -- Content page
					0,
					1019559
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenWoman),
							1000001,
							0,
							1000195,
							@BrandTitle + ' för kvinnor',
							1,
							1,
							1000748
							
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, SecondaryTemplateId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000747,
										1000059
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
									
									-- Brand/category filterTags extra content
									insert into lekmer.tBlockProductFilterTag(BlockId, TagId)
									select
										(select top 1 BlockId from @tmpBlockId),
										1000010
									
									-- Rating
									DELETE FROM [review].[tBlockRating]
									WHERE [BlockId] = (select top 1 BlockId from @tmpBlockId)

									INSERT INTO [review].[tBlockRating] ([BlockId], [RatingId])
									select
										(select top 1 BlockId from @tmpBlockId),
										1
										
									-- töm listan
									delete from @tmpBlockId
									
									
				---------------------------
				-- CHILDREN - CHILD
				---------------------------
				DECLARE @tmpChildrenChild TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenChild
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' för barn',
					2,
					LOWER(@BrandTitle)+'ForBarn',
					1, --All
					1 -- se

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenChild),
					1, -- se
					1000713,
					1000708,
					@BrandTitle,
					REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-barn'), '&', 'and'),										
					1, -- Content page
					0,
					1019559
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenChild),
							1000001,
							0,
							1000195,
							@BrandTitle + ' för barn',
							1,
							1,
							1000750
						
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, SecondaryTemplateId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000747,
										1000059
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
									
									-- Rating
									DELETE FROM [review].[tBlockRating]
									WHERE [BlockId] = (select top 1 BlockId from @tmpBlockId)

									INSERT INTO [review].[tBlockRating] ([BlockId], [RatingId])
									select
										(select top 1 BlockId from @tmpBlockId),
										1
																			
									-- töm listan
									delete from @tmpBlockId
									
							--rollback
			commit			
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
