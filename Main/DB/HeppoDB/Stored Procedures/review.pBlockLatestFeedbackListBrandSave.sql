SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandSave]
	@BlockId INT,
	@BrandId INT
AS
BEGIN
	INSERT [review].[tBlockLatestFeedbackListBrand] (
		[BlockId],
		[BrandId]
	)
	VALUES (
		@BlockId,
		@BrandId
	)
END
GO
