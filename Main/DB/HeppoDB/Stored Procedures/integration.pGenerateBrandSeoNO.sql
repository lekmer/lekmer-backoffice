SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateBrandSeoNO]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Varumärke>
		-- Title>
		-- Formel: Varumärke skor online - Märkesskor Damskor & Herrskor på nätet hos Heppo


		-- Meta Desc>
		-- Formel:	Heppo säljer Varumärke skor på nätet. Här hittar du alla våra märkesskor. 
		-- Gratis frakt på skor, även fri retur. 30 dagars öppet köp när du köper skor online från Heppo. 
		------------------------------------------------------------

		-- DEFAULT --
		INSERT INTO sitestructure.tContentPageSeoSetting(ContentNodeId)
		SELECT
			c.ContentNodeId
		FROM
			sitestructure.tContentPage c
		WHERE
			c.ContentNodeId not in (select ContentNodeId
									from sitestructure.tContentPageSeoSetting)	
									
		declare @LanguageId int
		set @LanguageId = 1000001 -- Norway
		
		--INSERT INTO sitestructure.tContentPageSeoSettingTranslation(ContentNodeId, LanguageId)
		--SELECT
		--	c.ContentNodeId,
		--	@languageId
		--FROM
		--	sitestructure.tContentPage c
		--WHERE
		--	not exists (SELECT 1
		--					FROM sitestructure.tContentPageSeoSettingTranslation n
		--					WHERE n.ContentNodeId = c.ContentNodeId and
		--					n.LanguageId = @LanguageId)
							
							
		UPDATE
			cps
		SET
			cps.Title = cn.Title  + ' sko online - Merkesko Damesko & Herresko på nett hos Heppo',
			
			cps.[Description] = 'Heppo selger ' + cn.Title + ' sko på nett. Her finner du alle våre merkesko. ' +
			'Gratis frakt på sko, også gratis retur. 30 dagers åpent kjøp når du kjøper sko online fra Heppo.' 
			--select cps.title, cps.description, cn.title, cn2.title, cn3.title, cn.contentnodeid
		FROM
				--sitestructure.tContentPageSeoSettingTranslation cps Används inte allt körs in i tContentPageSeoSetting
				sitestructure.tContentPageSeoSetting cps
				------
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId				
				inner join sitestructure.tContentNode cn2
				on cn.ParentContentNodeId = cn2.ContentNodeId
				inner join sitestructure.tContentNode cn3
				on cn.ParentContentNodeId = cn2.ContentNodeId
				------						
		WHERE
			cn.SiteStructureRegistryId = 2 -- NO
			and cn3.ContentNodeId = 1001324 -- skomärken
			and (cn2.ContentNodeId = 1001324 or cn2.ParentContentNodeId = 1001324)
			--and LanguageId = @LanguageId	
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))					
															


		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
