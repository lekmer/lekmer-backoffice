SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupProductGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgp.*
	FROM
		[review].[vRatingGroupProduct] rgp
	WHERE
		rgp.[RatingGroupProduct.ProductId] = @ProductId
END
GO
