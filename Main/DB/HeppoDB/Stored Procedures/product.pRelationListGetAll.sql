SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****************  Version 2  *****************
User: Yura P.		Date: 01.09.2008		Time: 17:50
Description:
	Added paging capabilities.
	Added sorting logic.
*****************  Version 3  *****************
User: Yura P.		Date: 01.09.2008		Time: 16:50
Description:
	Add RelationListTypeName
*/

CREATE PROCEDURE [product].[pRelationListGetAll]
    @Page INT = NULL,
    @PageSize INT,
    @SortBy VARCHAR(20) = NULL,
    @SortDescending BIT = NULL
AS 
    BEGIN
        SET NOCOUNT ON
	
        IF ( generic.fnValidateColumnName(@SortBy) = 0 ) 
        BEGIN
            RAISERROR ( N'Illegal characters in string (parameter @SortBy): %s' , 16 , 1 , @SortBy ) ;
            RETURN
        END

        DECLARE @sql NVARCHAR(MAX)
        DECLARE @sqlCount NVARCHAR(MAX)
        DECLARE @sqlFragment NVARCHAR(MAX)

        SET @sqlFragment = '
			SELECT ROW_NUMBER() OVER (ORDER BY vRL.[' 
				+ COALESCE(@SortBy, 'RelationList.Id') 
				+ ']'
				+ CASE WHEN ( @SortDescending = 1 ) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				vRL.*
			FROM [product].[vCustomRelationList] as vRL '
			
        SET @sql = 'SELECT * FROM
			(' + @sqlFragment + '
			)
			AS SearchResult'
			
        IF @Page != 0 AND @Page IS NOT NULL 
        BEGIN
            SET @sql = @sql + '
			WHERE Number > ' + CAST(( @Page - 1 ) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
        END
		
        SET @sqlCount = 'SELECT COUNT(1) FROM
			(
			' + @sqlFragment + '
			)
			AS CountResults'
			
        EXEC sp_executesql @sqlCount
        EXEC sp_executesql @sql
        
    END

GO
