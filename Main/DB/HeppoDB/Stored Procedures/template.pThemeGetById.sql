SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pThemeGetById]
@ThemeId	int
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomTheme]
	WHERE [Theme.Id] = @ThemeId
END

GO
