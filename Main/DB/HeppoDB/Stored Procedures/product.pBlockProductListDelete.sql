SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create procedure [product].[pBlockProductListDelete]
@BlockId		int
as
BEGIN
	delete
		[product].[tBlockProductList]
	where
		[BlockId] = @BlockId

end
GO
