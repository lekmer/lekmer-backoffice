SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 2  *****************
User: Victor E.		Date: 18.11.2008		Time: 15:40
Description:
			delete channel id 
************************************************

*****************  Version 1  *****************
User: Victor E.		Date: 08.10.2008		Time: 15:40
Description:
			Created 
*****************  Version 3  *****************
User: Yuriy P.		Date: 22.01.2009
Description:
			added Image
*/
CREATE PROCEDURE [template].[pTemplateGetById]
	@TemplateId	INT
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomTemplate]
	WHERE
		[Template.Id] = @TemplateId
END

GO
