SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-19
-- Description:	Gets a media item by id
-- =============================================
CREATE PROCEDURE [media].[pMediaItemGetById]
	@MediaId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT *
	FROM   [media].[vCustomMedia]
	WHERE  [Media.Id] = @MediaId
END

GO
