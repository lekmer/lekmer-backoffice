SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupCategoryGetAllByGroup]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgc.*
	FROM
		[review].[vRatingGroupCategory] rgc
	WHERE
		rgc.[RatingGroupCategory.RatingGroupId] = @RatingGroupId
END
GO
