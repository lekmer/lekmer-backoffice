SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductSortOrderGetById]
@ProductSortOrderId	int
AS
BEGIN
	SELECT
		*
	FROM
		[product].[vCustomProductSortOrder] o
	WHERE
		o.[ProductSortOrder.Id] = @ProductSortOrderId
END

GO
