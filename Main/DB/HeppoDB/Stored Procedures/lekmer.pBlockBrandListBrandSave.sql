SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListBrandSave]
	@BlockId	int,
	@BrandId	int,
	@Ordinal	int
AS 
BEGIN 
	UPDATE lekmer.tBlockBrandListBrand
	SET 
		Ordinal = @Ordinal
	WHERE 
		BlockId = @BlockId
		AND BrandId = @BrandId
		
	IF @@ROWCOUNT <> 0
		RETURN
		
	INSERT lekmer.tBlockBrandListBrand
	(
		BlockId,
		BrandId,
		Ordinal
	)
	VALUES
	(
		@BlockId,
		@BrandId,
		@Ordinal
	)
END 
GO
