SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedbackLikeInsert]
	@RatingReviewFeedbackId INT,
	@IPAddress VARCHAR(50),
	@CreatedDate DATETIME,
	@RatingReviewUserId INT
AS
BEGIN
	SET NOCOUNT ON
	
	IF EXISTS
	(
		SELECT 
			1 
		FROM 
			[review].[tRatingReviewFeedbackLike]
		WHERE 
			[RatingReviewFeedbackId] = @RatingReviewFeedbackId
			AND [RatingReviewUserId] = @RatingReviewUserId
	)
	
	RETURN -1
	
	EXEC [review].[pRatingReviewFeedback_IncreaseLikeHit] @RatingReviewFeedbackId
	
	INSERT INTO [review].[tRatingReviewFeedbackLike] (
		[RatingReviewFeedbackId],
		[IPAddress],
		[CreatedDate],
		[RatingReviewUserId]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@IPAddress,
		@CreatedDate,
		@RatingReviewUserId
	)
	
	RETURN SCOPE_IDENTITY()
END
GO
