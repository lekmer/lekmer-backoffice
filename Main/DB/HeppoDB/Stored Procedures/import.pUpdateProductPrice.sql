
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [import].[pUpdateProductPrice]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ProductsToUpdate INT
	DECLARE @RowSize INT
	DECLARE @Index INT

	IF OBJECT_ID('tempdb..#tProductPricesToBeUpdated') IS NOT NULL
		DROP TABLE #tProductPricesToBeUpdated

	CREATE TABLE #tProductPricesToBeUpdated
	(
		Id INT NULL,
		ProductId INT NOT NULL,
		PriceListId INT NOT NULL,
		PriceIncludingVat DECIMAL(16,2) NOT NULL,
		PriceExcludingVat DECIMAL(16,2) NOT NULL,
		VatPercentage DECIMAL(16,2) NOT NULL
		CONSTRAINT PK_#tProductPricesToBeUpdated PRIMARY KEY(ProductId, PriceListId) WITH (IGNORE_DUP_KEY = ON)
	)
	
	-----------------------------------------

	INSERT #tProductPricesToBeUpdated(
		ProductId,
		PriceListId,
		PriceIncludingVat,
		PriceExcludingVat,
		VatPercentage
	)
	SELECT
		pli.ProductId,
		pli.PriceListId,
		pp.Price * 0.01 AS 'PriceIncludingVat',
		pp.Price * 0.008 AS 'PriceExcludingVat',
		25.0
	FROM [import].tProductPrice pp
		INNER JOIN [lekmer].tLekmerProduct lp on lp.HYErpId = substring(pp.HYErpIdSize, 0,12)
		INNER JOIN [lekmer].tLekmerChannel lc on pp.ChannelId = lc.ErpId
		INNER JOIN [product].tProductModuleChannel pmc on pmc.ChannelId = lc.ChannelId
		INNER JOIN [product].tPriceList pl on pl.PriceListRegistryId = pmc.PriceListRegistryId
		INNER JOIN [product].tPriceListItem pli on pli.ProductId = lp.ProductId AND pli.PriceListId = pl.PriceListId
	WHERE
		pli.PriceIncludingVat <> (pp.Price * 0.01)

	SET @Index = 0
	UPDATE #tProductPricesToBeUpdated
	SET Id = @Index,
		@Index = @Index + 1

	SET @ProductsToUpdate = (SELECT COUNT(1) FROM #tProductPricesToBeUpdated)
	SET @RowSize = 500

	WHILE @ProductsToUpdate > 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			-- UPDATE
			UPDATE
				pli
			SET
				pli.PriceIncludingVat = t.PriceIncludingVat,
				pli.PriceExcludingVat = t.PriceExcludingVat,
				pli.VatPercentage = t.VatPercentage
			FROM #tProductPricesToBeUpdated t
				INNER JOIN [product].tPriceListItem pli
					ON pli.ProductId = t.ProductId
					AND pli.PriceListId = t.PriceListId
			WHERE
				t.Id > (@ProductsToUpdate - @RowSize)
			
			COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			-- LOG here
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
			VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		END CATCH
		
		DELETE FROM #tProductPricesToBeUpdated
		WHERE Id > (@ProductsToUpdate - @RowSize)
		
		SET @ProductsToUpdate = @ProductsToUpdate - @RowSize
	END
END
GO
