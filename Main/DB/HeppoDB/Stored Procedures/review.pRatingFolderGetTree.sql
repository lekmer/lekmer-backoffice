SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT rf.*
	FROM [review].[vRatingFolder] AS rf
	WHERE rf.[RatingFolder.ParentRatingFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT rf.*
		FROM [review].[vRatingFolder] AS rf
		WHERE rf.[RatingFolder.ParentRatingFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT rf.[RatingFolder.ParentRatingFolderId]
							 FROM [review].[vRatingFolder] AS rf
							 WHERE rf.[RatingFolder.RatingFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT rf.*
			FROM [review].[vRatingFolder] AS rf
			WHERE rf.[RatingFolder.ParentRatingFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [review].[vRatingFolder] AS rf
								WHERE rf.[RatingFolder.ParentRatingFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
