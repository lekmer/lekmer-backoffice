SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductStatusGetById]
@ProductStatusId	int
AS
BEGIN
	SELECT
		*
	FROM
		[product].[vCustomProductStatus]
	WHERE
		[ProductStatus.Id] = @ProductStatusId
END

GO
