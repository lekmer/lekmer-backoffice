
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemGetAllSecure]
	@ActionId		int
AS
BEGIN
	SELECT 
		A.*
	FROM 
		lekmer.vProductDiscountActionItem A
		INNER JOIN product.tProduct P ON A.[ProductDiscountActionItem.ProductId] = P.ProductId
	WHERE 
		A.[ProductDiscountActionItem.ActionId] = @ActionId AND 
		P.IsDeleted = 0
	ORDER BY
		[P].[ErpId]
END
GO
