SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRatingItemSave]
	@BlockId INT,
	@RatingItemId INT
AS
BEGIN
	INSERT [review].[tBlockRatingItem] (
		[BlockId],
		[RatingItemId]
	)
	VALUES (
		@BlockId,
		@RatingItemId
	)
END
GO
