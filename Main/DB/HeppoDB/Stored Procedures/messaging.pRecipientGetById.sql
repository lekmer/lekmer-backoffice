SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [messaging].[pRecipientGetById]
@RecipientId	uniqueidentifier
as
begin

	select
		RecipientId,
		MessageId,
		BatchId,
		Chunk,
		[Address]
	from
		[messaging].[tRecipient]
	where
		RecipientId = @RecipientId

	select
		RecipientId,
		[Name],
		[Value]
	from
		[messaging].[tRecipientCustomField]
	where
		RecipientId = @RecipientId

end
GO
