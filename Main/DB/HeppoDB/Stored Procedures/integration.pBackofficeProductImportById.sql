
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId			NVARCHAR(20),
	@DescriptionSE		NVARCHAR(MAX),
	@DescriptionDK		NVARCHAR(MAX),
	@DescriptionNO		NVARCHAR(MAX),
	@DescriptionFI		NVARCHAR(MAX),
	@TagIds				VARCHAR(MAX),
	@SizeDeviationId	INT,
	@UserName			NVARCHAR(100)
AS
begin
	set nocount on
	
	BEGIN TRY
		BEGIN TRANSACTION
	
			DECLARE @HasDescriptionSE BIT, @HasDescriptionDK BIT, @HasDescriptionNO BIT, @HasDescriptionFI BIT
			SET @HasDescriptionSE = 0
			SET @HasDescriptionDK = 0
			SET @HasDescriptionNO = 0
			SET @HasDescriptionFI = 0
	
			-- insert into tProductTranslation
			DECLARE @LanguageId INT
			DECLARE @ISO CHAR(2)
				
			DECLARE cur_language CURSOR FAST_FORWARD FOR
				SELECT DISTINCT l.LanguageId, c.ISO
				FROM core.tLanguage l
				INNER JOIN core.tChannel ch on l.LanguageId = ch.LanguageId
				INNER JOIN core.tCountry c on ch.CountryId = c.CountryId
				WHERE c.ISO IN ('SE', 'DK', 'NO', 'FI')
				
			OPEN cur_language
			FETCH NEXT FROM cur_language INTO @LanguageId, @ISO
				
			WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO product.tProductTranslation(ProductId, LanguageId)
					SELECT p.ProductId, @LanguageId FROM product.tProduct p
					WHERE NOT EXISTS (SELECT 1 FROM product.tProductTranslation pt
									  WHERE pt.ProductId = p.ProductId 
											AND pt.LanguageId = @LanguageId)
				
					-- Update product Description.
					IF @ISO = 'SE'
					BEGIN
						UPDATE p
						SET p.[Description] = @DescriptionSE
						FROM 
							lekmer.tLekmerProduct lp
							INNER JOIN product.tProduct p ON lp.ProductId = p.ProductId	
						WHERE
							lp.HYErpId = @HYErpId
							AND (@DescriptionSE IS NOT NULL AND @DescriptionSE != '')	
							AND (p.[Description] != @DescriptionSE OR p.[Description] IS NULL)
							
						IF EXISTS (SELECT 1 FROM lekmer.tLekmerProduct lp
								   INNER JOIN product.tProduct p ON lp.ProductId = p.ProductId
								   WHERE
										lp.HYErpId = @HYErpId
										AND p.[Description] IS NOT NULL
										AND p.[Description] <> '')
						BEGIN
							SET @HasDescriptionSE = 1
						END
					END
					
					ELSE IF @ISO = 'DK'
					BEGIN
						UPDATE pt
						SET pt.[Description] = @DescriptionDK
						FROM 
							lekmer.tLekmerProduct lp
							INNER JOIN product.tProductTranslation pt ON lp.ProductId = pt.ProductId	
						WHERE
							lp.HYErpId = @HYErpId
							AND pt.LanguageId = @LanguageId
							AND (@DescriptionDK IS NOT NULL AND @DescriptionDK != '')
							AND (pt.[Description] != @DescriptionDK OR pt.[Description] IS NULL)
						
						IF EXISTS (SELECT 1 FROM lekmer.tLekmerProduct lp
								   INNER JOIN product.tProductTranslation pt ON lp.ProductId = pt.ProductId
								   WHERE
										lp.HYErpId = @HYErpId
										AND pt.LanguageId = @LanguageId
										AND pt.[Description] IS NOT NULL
										AND pt.[Description] <> '')
						BEGIN
							SET @HasDescriptionDK = 1
						END
					END
					
					ELSE IF @ISO = 'NO'
					BEGIN
						UPDATE pt
						SET pt.[Description] = @DescriptionNO
						FROM 
							lekmer.tLekmerProduct lp
							INNER JOIN product.tProductTranslation pt ON lp.ProductId = pt.ProductId	
						WHERE
							lp.HYErpId = @HYErpId
							AND pt.LanguageId = @LanguageId
							AND (@DescriptionNO IS NOT NULL AND @DescriptionNO != '')
							AND (pt.[Description] != @DescriptionNO OR pt.[Description] IS NULL)
							
						IF EXISTS (SELECT 1 FROM lekmer.tLekmerProduct lp
								   INNER JOIN product.tProductTranslation pt ON lp.ProductId = pt.ProductId
								   WHERE
										lp.HYErpId = @HYErpId
										AND pt.LanguageId = @LanguageId
										AND pt.[Description] IS NOT NULL
										AND pt.[Description] <> '')
						BEGIN
							SET @HasDescriptionNO = 1
						END
					END
					
					ELSE IF @ISO = 'FI'
					BEGIN
						UPDATE pt
						SET pt.[Description] = @DescriptionFI
						FROM 
							lekmer.tLekmerProduct lp
							INNER JOIN product.tProductTranslation pt ON lp.ProductId = pt.ProductId	
						WHERE
							lp.HYErpId = @HYErpId
							AND pt.LanguageId = @LanguageId
							AND (@DescriptionFI IS NOT NULL AND @DescriptionFI != '')
							AND (pt.[Description] != @DescriptionFI OR pt.[Description] IS NULL)
							
						IF EXISTS (SELECT 1 FROM lekmer.tLekmerProduct lp
								   INNER JOIN product.tProductTranslation pt ON lp.ProductId = pt.ProductId
								   WHERE
										lp.HYErpId = @HYErpId
										AND pt.LanguageId = @LanguageId
										AND pt.[Description] IS NOT NULL
										AND pt.[Description] <> '')
						BEGIN
							SET @HasDescriptionFI = 1
						END
					END
					
					FETCH NEXT FROM cur_language INTO @LanguageId, @ISO
				END
			CLOSE cur_language
			DEALLOCATE cur_language	 
			
			-- Update product SizeDeviation.
			IF  EXISTS (SELECT 1 FROM lekmer.tSizeDeviation sd
						WHERE sd.SizeDeviationId = @SizeDeviationId)
			BEGIN
				UPDATE lp
				SET lp.[SizeDeviationId] = @SizeDeviationId
				FROM 
					lekmer.tLekmerProduct lp
				WHERE
					lp.HYErpId = @HYErpId
					AND (@SizeDeviationId IS NOT NULL AND @SizeDeviationId > 0)	
					AND (lp.[SizeDeviationId] != @SizeDeviationId OR lp.[SizeDeviationId] IS NULL)
			END
			
			-- Update/Insert product tags.
			DECLARE @TagId INT
			DECLARE cur_tag CURSOR FAST_FORWARD FOR
				SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
				
			OPEN cur_tag
			FETCH NEXT FROM cur_tag INTO @TagId
				
			WHILE @@FETCH_STATUS = 0
				BEGIN
					-- Update/Insert product tags.
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
							   INNER JOIN product.tProduct p ON pt.ProductId = p.ProductId	
							   INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
							   WHERE 
									lp.HYErpId = @HYErpId
									AND pt.TagId = @TagId)
						BEGIN
							INSERT INTO lekmer.tProductTag (ProductId, TagId)
							VALUES ((SELECT TOP(1) p.ProductId FROM product.tProduct p 
									INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
									WHERE lp.HYErpId = @HYErpId), @TagId)
						END
					
					FETCH NEXT FROM cur_tag INTO @TagId
				END
			CLOSE cur_tag
			DEALLOCATE cur_tag
			
			-- Set Product Status = 1 (Offline) for products that has descriptions in all channels.
			--IF (@HasDescriptionSE = 1 AND @HasDescriptionDK = 1 AND @HasDescriptionNO = 1 AND @HasDescriptionFI = 1)
			--BEGIN
			--	UPDATE p
			--	SET p.ProductStatusId = 1
			--	FROM product.tProduct p
			--	INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
			--	WHERE lp.HYErpId = @HYErpId
			--END
			
			-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
			INSERT INTO integration.tBackofficeProductInfoImport
			(
				[HYErpId]
				,[DescriptionSE]
				,[DescriptionDK]
				,[DescriptionNO]
				,[DescriptionFI]
				,[TagIdCollection]
				,[SizeDeviationId]
				,[UserName]
				,[InsertedDate]
			)
			VALUES 
			(
				@HYErpId
				,@DescriptionSE
				,@DescriptionDK
				,@DescriptionNO
				,@DescriptionFI
				,@TagIds
				,@SizeDeviationId
				,@UserName
				,CAST(GETDATE() AS SMALLDATETIME)
			)
			
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog]
		(
			[Data], 
			[Message], 
			[Date], 
			[OcuredInProcedure]
		)
		VALUES
		(
			'', 
			ERROR_MESSAGE(), 
			GETDATE(), 
			ERROR_PROCEDURE()
		)
	END CATCH
END
GO
