SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pCdonExportUpdate]
@CdonExportId		INT,
@StatusCommonName	VARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @StatusId INT
	SET @StatusId = (SELECT CdonExportStatusId FROM [integration].[tCdonExportStatus] WHERE CommonName = @StatusCommonName)
	
	UPDATE
		[integration].[tCdonExport]
	SET
		[Status] = @StatusId
		,EndDate = GETDATE()
	WHERE 
		[CdonExportId] = @CdonExportId
END
GO
