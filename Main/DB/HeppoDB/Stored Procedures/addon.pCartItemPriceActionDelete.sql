SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartItemPriceActionDelete]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionIncludeProduct
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionExcludeProduct
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionIncludeCategory
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionExcludeCategory
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionCurrency
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceAction
	where CartActionId = @CartActionId
end
GO
