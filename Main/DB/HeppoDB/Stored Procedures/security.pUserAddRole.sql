SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [security].[pUserAddRole]
	@UserId int,
	@RoleId int
as
begin
	insert
		security.tUserRole
	(
		SystemUserId,
		RoleId
	)
	values
	(
		@UserId,
		@RoleId
	)
end
GO
