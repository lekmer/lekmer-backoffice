SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pPriceIntervalGetAllSecure]
AS
begin
	set nocount on

	select
		*
	from
		lekmer.[vPriceIntervalSecure]
end

GO
