SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pRoleGetAllBySystemUser]
	@SystemUserId	int
as
begin
	set nocount on

	select
		R.*
	from
		[security].[vCustomRole] as R
		inner join [security].[tUserRole] as U on U.[RoleId] = R.[Role.Id]
	where
		U.[SystemUserId] = @SystemUserId
end

GO
