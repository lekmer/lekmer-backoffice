
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pLekmerChannelSave]
	@ChannelId				INT,
	@TimeFormat				NVARCHAR(50),
	@WeekDayFormat			NVARCHAR(50),
	@DayFormat				NVARCHAR(50),
	@DateTimeFormat			NVARCHAR(50),
	@TimeZoneDiff			INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tLekmerChannel]
	SET
		[TimeFormat] = @TimeFormat,
		[WeekDayFormat] = @WeekDayFormat,
		[DayFormat] = @DayFormat,
		[DateTimeFormat] = @DateTimeFormat,
		[TimeZoneDiff] = @TimeZoneDiff
	WHERE
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	
	INSERT INTO [lekmer].[tLekmerChannel] (
		[ChannelId],
		[TimeFormat],
		[WeekDayFormat],
		[DayFormat],
		[DateTimeFormat],
		[TimeZoneDiff])
	VALUES (
		@ChannelId,
		@TimeFormat,
		@WeekDayFormat,
		@DayFormat,
		@DateTimeFormat,
		@TimeZoneDiff
	)
		
	RETURN @ChannelId
END
GO
