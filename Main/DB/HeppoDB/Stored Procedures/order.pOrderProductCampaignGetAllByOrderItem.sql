SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderProductCampaignGetAllByOrderItem]
	@OrderItemId	INT
AS
BEGIN
	SELECT
		*
	FROM 
		[order].[vCustomOrderProductCampaign]
	WHERE 
		[OrderCampaign.OrderItemId] = @OrderItemId
END

GO
