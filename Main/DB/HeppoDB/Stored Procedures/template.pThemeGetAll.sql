SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pThemeGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomTheme]
END

GO
