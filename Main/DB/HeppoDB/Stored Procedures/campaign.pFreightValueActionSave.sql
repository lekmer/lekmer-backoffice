SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pFreightValueActionSave]
	@CartActionId int
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM campaign.tFreightValueAction WHERE CartActionId = @CartActionId)
			RETURN
		
		INSERT campaign.tFreightValueAction
		( 
			CartActionId
		)
		VALUES 
		(
			@CartActionId
		)
END
GO
