SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreSave]
	@StoreId		int,
	@StatusId		int,
	@Title			varchar(100),
	@ErpId			varchar(50),
	@Description	nvarchar(max)
AS
BEGIN
	IF EXISTS (SELECT 1 FROM product.tStore WHERE Title = @Title AND StoreId <> @StoreId)
		RETURN -1
	
	UPDATE 
		product.tStore
	SET 
		StatusId = @StatusId, 
		ErpId = @ErpId,
		[Description] = @Description,
		Title = @Title
	WHERE 
		StoreId = @StoreId
		
	IF @@ROWCOUNT = 0 
	BEGIN 
		INSERT 
			product.tStore
		( 
			StatusId, 
			ErpId, 
			[Description], 
			Title
		)
		VALUES 
		(
			@StatusId,
			@ErpId,
			@Description,
			@Title
		)
		SET @StoreId = scope_identity()						
	END 
	RETURN @StoreId
END 
GO
