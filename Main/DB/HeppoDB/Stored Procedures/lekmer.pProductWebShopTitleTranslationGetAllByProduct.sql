SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductWebShopTitleTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.WebShopTitle] AS 'Value'
	FROM
		lekmer.[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
END

GO
