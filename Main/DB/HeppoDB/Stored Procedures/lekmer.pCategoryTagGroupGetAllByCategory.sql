SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryTagGroupGetAllByCategory]
	@CategoryId INT
AS
BEGIN
	SELECT * FROM [lekmer].[vCategoryTagGroup] ctg
	WHERE ctg.[CategoryTagGroup.CategoryId] = @CategoryId
END
GO
