SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerGroupDelete]
@CustomerGroupId	int
AS
BEGIN
	DELETE FROM 
		product.tPriceListCustomerGroup
	WHERE 
		CustomerGroupId = @CustomerGroupId
	
	DELETE FROM 
		customer.tCustomerGroupCustomer
	WHERE 
		CustomerGroupId = @CustomerGroupId
	
	DELETE FROM 
		customer.tCustomerGroup
	WHERE 
		CustomerGroupId = @CustomerGroupId
END
GO
