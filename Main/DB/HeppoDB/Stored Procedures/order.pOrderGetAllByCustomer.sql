SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderGetAllByCustomer]
	@CustomerId		INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CustomerId] = @CustomerId
		AND [OrderStatus.CommonName] <> 'RejectedByPaymentProvider'
END

GO
