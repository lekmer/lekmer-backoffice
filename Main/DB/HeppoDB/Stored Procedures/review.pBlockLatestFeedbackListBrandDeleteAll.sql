SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockLatestFeedbackListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
