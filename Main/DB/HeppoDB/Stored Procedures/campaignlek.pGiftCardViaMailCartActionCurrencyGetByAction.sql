SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailCartActionCurrency]
	WHERE 
		[GiftCardViaMailCartActionCurrency.CartActionId] = @CartActionId
END
GO
