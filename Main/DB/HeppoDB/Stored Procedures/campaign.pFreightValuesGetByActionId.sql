SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pFreightValuesGetByActionId]
	@ActionId int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomFreightValueActionCurrency
	WHERE 
		[FreightValueAction.ActionId] = @ActionId
end

GO
