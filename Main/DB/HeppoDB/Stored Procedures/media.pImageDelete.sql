SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-25
-- Description:	Deletes an image by Id
-- =============================================
CREATE PROCEDURE [media].[pImageDelete]
	@MediaId INT
AS
BEGIN

	SET NOCOUNT ON
	DELETE 
	FROM   [media].[tImageTranslation]
	WHERE  [MediaId] = @MediaId
	
	DELETE 
	FROM   [media].[tImage]
	WHERE  [MediaId] = @MediaId
END
GO
