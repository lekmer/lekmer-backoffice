SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bbrpl.*,
		b.*
	FROM 
		[review].[vBlockBestRatedProductList] AS bbrpl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bbrpl.[BlockBestRatedProductList.BlockId] = b.[Block.BlockId]
	WHERE
		bbrpl.[BlockBestRatedProductList.BlockId] = @BlockId
END
GO
