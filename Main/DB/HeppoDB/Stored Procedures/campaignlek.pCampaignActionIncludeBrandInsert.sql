SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeBrandInsert]
	@ConfigId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeBrand] (
		ConfigId,
		BrandId
	)
	VALUES (
		@ConfigId,
		@BrandId
	)
END
GO
