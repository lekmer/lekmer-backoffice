SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pPriceListCustomerGroupSave]
@CustomerGroupId	int,
@PriceListId		int
AS
BEGIN
	INSERT INTO 
		product.tPriceListCustomerGroup
		(
			PriceListId,
			CustomerGroupId
		)
	VALUES
		(
			@PriceListId,
			@CustomerGroupId
		)
END













GO
