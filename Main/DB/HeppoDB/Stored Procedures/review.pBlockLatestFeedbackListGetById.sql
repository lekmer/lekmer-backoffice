SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		blfl.*,
		b.*
	FROM 
		[review].[vBlockLatestFeedbackList] AS blfl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON blfl.[BlockLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		blfl.[BlockLatestFeedbackList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
