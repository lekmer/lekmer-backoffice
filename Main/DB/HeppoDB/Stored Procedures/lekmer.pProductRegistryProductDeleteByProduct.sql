
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductDeleteByProduct]
	@ProductId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE 
		[product].[tProductRegistryProduct]
	WHERE 
		[ProductId] = @ProductId
		AND [ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
		
	EXEC [productlek].[pPackageRestrictionsUpdate] @ProductId, NULL, NULL, @ProductRegistryIds, @Delimiter
END
GO
