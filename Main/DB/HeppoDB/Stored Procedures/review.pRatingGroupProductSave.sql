SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupProductSave]
	@RatingGroupId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingGroupProduct] (
		[RatingGroupId],
		[ProductId]
	)
	VALUES (
		@RatingGroupId,
		@ProductId
	)
END
GO
