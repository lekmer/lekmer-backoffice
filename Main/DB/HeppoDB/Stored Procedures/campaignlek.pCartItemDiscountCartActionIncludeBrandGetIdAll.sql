SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tCartItemDiscountCartActionIncludeBrand]
	WHERE 
		CartActionId = @CartActionId
END

GO
