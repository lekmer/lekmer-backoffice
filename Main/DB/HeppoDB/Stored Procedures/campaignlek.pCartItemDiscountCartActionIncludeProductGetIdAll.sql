SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[campaignlek].[tCartItemDiscountCartActionIncludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END

GO
