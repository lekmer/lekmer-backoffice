
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 13.01.2009
Description:
			Created 
*/

CREATE PROCEDURE [core].[pCacheUpdateSave]
	@ManagerName	VARCHAR(100),
	@UpdateType		INT,
	@Key			VARCHAR(500),
	@CreationDate	DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT [core].[tCacheUpdate] WITH (TABLOCKX)
	(
		[ManagerName],
		[UpdateType],
		[Key],
		[CreationDate]
	)
	VALUES
	(
		@ManagerName,
		@UpdateType,
		@Key,
		@CreationDate
	)
END
GO
