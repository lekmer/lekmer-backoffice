SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE procedure [product].[pVariationGroupSave]
	@VariationGroupId	int,
	@Title				nvarchar(256),
	@DefaultProductId	int,
	@VariationGroupStatusId	int
as
begin
	set nocount off

	update	
		[product].[tVariationGroup]
	Set
		[Title] = @Title,
		[DefaultProductId] = @DefaultProductId,
		[VariationGroupStatusId] = @VariationGroupStatusId
	where
		[VariationGroupId] = @VariationGroupId
			
	if  @@ROWCOUNT = 0
	begin
		insert into [product].[tVariationGroup]
		(
			[Title],
			[DefaultProductId],
			[VariationGroupStatusId]
		)
		values
		(
			@Title,
			@DefaultProductId,
			@VariationGroupStatusId
		)
		set @VariationGroupId = scope_identity();
	end
	return @VariationGroupId
end






GO
