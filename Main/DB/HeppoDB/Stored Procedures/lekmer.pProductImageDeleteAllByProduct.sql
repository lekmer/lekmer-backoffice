SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductImageDeleteAllByProduct]
	@ProductId		INT,
	@NewImagesCount	INT
AS
BEGIN
	IF @NewImagesCount = 0 AND EXISTS (SELECT TOP 1 MediaId FROM product.tProductImage WHERE ProductId = @ProductId)
		RETURN -1
	
	DELETE
		[product].[tProductImage]
	WHERE
		ProductId = @productId
		
	RETURN 1
END
GO
