SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionExcludeDeleteAll]
	@ConditionId INT
AS 
BEGIN 
	DELETE
		[campaignlek].[tCustomerGroupConditionExclude]
	WHERE
		ConditionId = @ConditionId
END
GO
