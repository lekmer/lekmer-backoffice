SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderAuditGetAllByOrder]
	@OrderId		INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrderAudit]
	WHERE
		[OrderAudit.OrderId] = @OrderId

END


GO
