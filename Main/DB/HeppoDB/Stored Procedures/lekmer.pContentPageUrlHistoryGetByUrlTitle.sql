
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryGetByUrlTitle]
	@ChannelId INT,
	@UrlTitleOld NVARCHAR(256)
AS 
BEGIN 
	SELECT 
		CCPUH.*
	FROM 
		[lekmer].[vCustomContentPageUrlHistory] CCPUH
		LEFT OUTER JOIN [sitestructure].[tContentPage] AS CP ON CP.ContentNodeId = CCPUH.[ContentPageUrlHistory.ContentPageId]
		LEFT OUTER JOIN [sitestructure].[tSiteStructureModuleChannel] AS MCH ON MCH.SiteStructureRegistryId = CP.SiteStructureRegistryId
		LEFT OUTER JOIN [core].[tChannel] AS CH ON MCH.ChannelId = CH.ChannelId
	WHERE
		CH.ChannelId = @ChannelId
		AND
		CCPUH.[ContentPageUrlHistory.UrlTitleOld] = @UrlTitleOld
END
GO
