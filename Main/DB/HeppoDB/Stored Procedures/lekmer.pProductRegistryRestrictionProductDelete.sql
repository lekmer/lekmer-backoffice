SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductDelete]
	@ProductId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductRegistryRestrictionProduct]
	WHERE 
		[ProductId] = @ProductId
END 
GO
