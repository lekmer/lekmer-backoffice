
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pTrackProductChangesHeppo]
AS 
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductChanges') IS NOT NULL 
		DROP TABLE #tProductChanges
	
	CREATE TABLE #tProductChanges (
		[ProductId] INT NOT NULL
		CONSTRAINT PK_#tProductChanges PRIMARY KEY ( [ProductId] )
	)

	INSERT	#tProductChanges ( [ProductId] )
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[import].[tProduct] p
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = [p].[HYErpId]
		UNION
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[import].[tProductPrice] pp
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([pp].[HYErpIdSize], 0, 12)
	
	IF (SELECT COUNT(*) FROM [#tProductChanges]) < 10000
	BEGIN
		INSERT [productlek].[tProductChangeEvent] ( [ProductId], [EventStatusId], [CdonExportEventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] )
		SELECT [ProductId], 0, 0, GETDATE(), NULL, 'HY import' FROM [#tProductChanges]
	END
	
	DROP TABLE #tProductChanges
END
GO
