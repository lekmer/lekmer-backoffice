
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentPageSeoSettingSave]
	@ContentNodeId	INT,
	@Title			NVARCHAR(500),
	@Description	NVARCHAR(500),
	@Keywords		NVARCHAR(500)
AS
BEGIN
	UPDATE
		[sitestructure].[tContentPageSeoSetting]
	SET
		[Title]			= @Title,
		[Description]	= @Description,
		[Keywords]		= @Keywords
	WHERE
		[ContentNodeId] = @ContentNodeId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [sitestructure].[tContentPageSeoSetting]
		(
			[ContentNodeId],
			[Title],
			[Description],
			[Keywords]
		)
		VALUES
		(
			@ContentNodeId,
			@Title,
			@Description,
			@Keywords
		)
	END
END
GO
