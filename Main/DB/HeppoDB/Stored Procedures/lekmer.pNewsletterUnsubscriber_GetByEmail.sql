SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_GetByEmail]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriber]
	WHERE
		[NewsletterUnsubscriber.ChannelId] = @ChannelId
		AND
		[NewsletterUnsubscriber.Email] = @Email
END

GO
