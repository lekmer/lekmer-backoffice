SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignGetAllProductCampaigns]
AS
BEGIN
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C 
		INNER JOIN campaign.tProductCampaign PC ON PC.CampaignId = C.[Campaign.Id]
	ORDER BY 
		C.[Campaign.Priority] ASC
END

GO
