SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemTranslationDeleteAll]
	@RatingId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE rit
	FROM 
		[review].[tRatingItemTranslation] rit
		INNER JOIN [review].[tRatingItem] ri ON ri.[RatingItemId] = rit.[RatingItemId]
	WHERE 
		ri.[RatingId] = @RatingId
END
GO
