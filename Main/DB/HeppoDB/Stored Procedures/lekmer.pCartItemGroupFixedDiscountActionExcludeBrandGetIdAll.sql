SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
