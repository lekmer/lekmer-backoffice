SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockTypeGetAll]
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomBlockType]
	order by
		[BlockType.Ordinal]
end

GO
