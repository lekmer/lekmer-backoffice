SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pFreightValueActionDelete]
	@CartActionId int
AS 
BEGIN
	DELETE FROM campaign.tFreightValueActionCurrency
	WHERE CartActionId = @CartActionId
	
	DELETE FROM campaign.tFreightValueAction
	WHERE CartActionId = @CartActionId
END
GO
