SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pBlockTopListCategoryGetAllByBlockSecure]
	@BlockId int
as
begin
	select
		*
	from
		addon.vCustomBlockTopListCategorySecure
	where
		[BlockTopListCategory.BlockId] = @BlockId
end

GO
