SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 16.04.2010		Time: 18:40
Description: Created 
*/
CREATE PROCEDURE [order].[pOrderCommentGetAllByOrder]
	@OrderId	int
AS
BEGIN
	SELECT
		*
	FROM
		[order].vCustomOrderComment
	WHERE
		[OrderComment.OrderId] = @OrderId
		order by [OrderComment.CreationDate] ASC
END


GO
