SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 2  *****************
User: Artymyshyn M.		Date: 13.02.2009		Time: 16:15
Description:
			Remove:
			TemplateId,
			TypePath					
*/
/*
*****************  Version 1  *****************
User: Victor E.		Date: 18.11.2008		Time: 13:40
Description:
			Rename:
					ModuleName on Name
					ModuleCN on CommonName
					ModuleTypePath on TypePath					
*/

CREATE PROCEDURE [template].[pComponentGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomComponent]
END

GO
