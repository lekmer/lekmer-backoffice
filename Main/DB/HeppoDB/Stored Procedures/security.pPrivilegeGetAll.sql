SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pPrivilegeGetAll]
as
begin
	set nocount on

	select
		*
	from
		[security].[vCustomPrivilege]
	order by
		[Privilege.Title]
end

GO
