SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListBrandGetAllByBlockSecure]
	@BlockId	int
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[lekmer].[vBlockBrandListBrand]
	WHERE 
		[BlockBrandListBrand.BlockId] = @BlockId
END 
GO
