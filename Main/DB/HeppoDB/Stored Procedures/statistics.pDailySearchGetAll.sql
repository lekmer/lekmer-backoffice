SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pDailySearchGetAll]
	@ChannelId			int,
	@DateTimeFrom		smalldatetime,
	@DateTimeTo 		smalldatetime,
	@FoundTopCount		int,
	@ZeroHitsTopCount	int
AS
BEGIN
	DECLARE @Search TABLE (Query nvarchar(400), HitCount int, SearchCount int)
	INSERT INTO @Search
	(
		Query,
		HitCount,
		SearchCount
	)
	SELECT 
		[Query] AS 'Query', 
		max([HitCount]) AS 'HitCount', 
		sum([SearchCount]) AS 'SearchCount'
	FROM
		[statistics].[tDailySearch] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [Date] BETWEEN @DateTimeFrom AND @DateTimeTo
	GROUP BY 
		[Query]
	ORDER BY 
		[SearchCount] DESC

	SELECT TOP(@FoundTopCount) 
		@ChannelId as 'DailySearch.ChannelId',
		Query as 'DailySearch.Query',
		@DateTimeFrom as 'DailySearch.Date',
		HitCount as 'DailySearch.HitCount',
		SearchCount as 'DailySearch.SearchCount'
	FROM 
		@Search
	WHERE 
		HitCount > 0

	UNION ALL
	
	SELECT TOP(@ZeroHitsTopCount) 
		@ChannelId as 'DailySearch.ChannelId',
		Query as 'DailySearch.Query',
		@DateTimeFrom as 'DailySearch.Date',
		HitCount as 'DailySearch.HitCount',
		SearchCount as 'DailySearch.SearchCount'
	FROM 
		@Search
	WHERE 
		HitCount = 0
		
END
GO
