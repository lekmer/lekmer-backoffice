
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pCreateProductRelationVariant]
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @RelationListTypeId INT
	SELECT @RelationListTypeId = RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant'

	DECLARE @VariantsCount INT,
			@RelationListId INT,
			@ProductId INT,
			@ProductErpId VARCHAR(50),
			@VariantErpId VARCHAR(50)

	-- Data to insert into relation tables
	IF OBJECT_ID('tempdb..#tTmpProducts') IS NOT NULL
		DROP TABLE #tTmpProducts
		
	CREATE TABLE #tTmpProducts (			
		ProductId INT,
		ProductErpId VARCHAR(50),
		VariantErpId VARCHAR(50)
	)
	
	INSERT INTO #tTmpProducts
	SELECT ProductId, HYErpId, SUBSTRING(HYErpId, 1, CHARINDEX('-', HYErpId) - 1) FROM [lekmer].[tLekmerProduct] ORDER BY HYErpId


	-- Relation Lists to delete
	IF OBJECT_ID('tempdb..#tTmpRelationListsToDelete') IS NOT NULL
		DROP TABLE #tTmpRelationListsToDelete
		
	CREATE TABLE #tTmpRelationListsToDelete (			
		RelationListId INT
	)
	
	INSERT INTO #tTmpRelationListsToDelete
	SELECT [rl].[RelationListId] FROM product.tRelationList rl
	LEFT JOIN #tTmpProducts tmpP ON [tmpP].[VariantErpId] = [rl].[Title]
	WHERE [rl].[RelationListTypeId] = @RelationListTypeId AND [tmpP].[VariantErpId] IS NULL
	

	-- Delete redundant Relation Lists
	-- Delete all Product - Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tProductRelationList
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List - Product where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationListProduct
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationList 
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)
	
	
	-- Delete redundant Relations
	DELETE rlp
	FROM [product].[tRelationListProduct] rlp
	INNER JOIN [product].[tRelationList] rl ON [rl].[RelationListId] = [rlp].[RelationListId]
	LEFT JOIN #tTmpProducts tmpP ON [tmpP].[VariantErpId] = [rl].[Title] AND [tmpP].[ProductId] = [rlp].[ProductId]
	WHERE [tmpP].[VariantErpId] IS NULL AND [tmpP].[ProductId] IS NULL AND rl.[RelationListTypeId] = @RelationListTypeId
	
	DELETE prl
	FROM [product].[tProductRelationList] prl
	INNER JOIN [product].[tRelationList] rl ON [rl].[RelationListId] = [prl].[RelationListId]
	LEFT JOIN #tTmpProducts tmpP ON [tmpP].[VariantErpId] = [rl].[Title] AND [tmpP].[ProductId] = [prl].[ProductId]
	WHERE [tmpP].[VariantErpId] IS NULL AND [tmpP].[ProductId] IS NULL AND rl.[RelationListTypeId] = @RelationListTypeId
	

	-- Add new relation lists and relations
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			[ProductId],
			[ProductErpId],
			[VariantErpId]
		FROM
			#tTmpProducts

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@ProductId,
			@ProductErpId,
			@VariantErpId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			-- Check if product has variants			
			SET @VariantsCount = (SELECT COUNT(ProductId) FROM lekmer.tLekmerProduct WHERE HYErpId LIKE @VariantErpId + '%')
			IF @VariantsCount > 1
			BEGIN
				BEGIN TRANSACTION
					SET @RelationListId = ( SELECT RelationListId FROM product.tRelationList rl
											WHERE rl.Title = @VariantErpId AND rl.RelationListTypeId = @RelationListTypeId )

					IF @RelationListId IS NULL
					BEGIN
						EXEC @RelationListId = [product].[pRelationListSave] NULL, @VariantErpId, @RelationListTypeId
					END

					IF NOT EXISTS (SELECT 1 FROM [product].[tProductRelationList] WHERE [ProductId] = @ProductId AND [RelationListId] = @RelationListId)
					BEGIN 
						EXEC [product].[pProductRelationListSave] @ProductId, @RelationListId
					END
						
					IF NOT EXISTS (SELECT 1 FROM [product].[tRelationListProduct] WHERE [ProductId] = @ProductId AND [RelationListId] = @RelationListId)
					BEGIN
						EXEC [product].[pRelationListProductSave] @RelationListId, @ProductId
					END
				COMMIT
			END
		END TRY		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				NULL,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@ProductId,
				@ProductErpId,
				@VariantErpId
	END

	CLOSE cur_product
	DEALLOCATE cur_product
	
	
	-- Delete Relation Lists with <= 1 relations
	DELETE FROM #tTmpRelationListsToDelete
	
	INSERT INTO #tTmpRelationListsToDelete
	SELECT [rl].[RelationListId] FROM [product].[tRelationList] rl
	INNER JOIN [product].[tRelationListProduct] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
	WHERE [rl].[RelationListTypeId] = @RelationListTypeId
	GROUP BY [rl].[RelationListId]
	HAVING COUNT([rlp].[ProductId]) <= 1
	
	INSERT INTO #tTmpRelationListsToDelete
	SELECT [rl].[RelationListId] FROM [product].[tRelationList] rl
	INNER JOIN [product].[tProductRelationList] prl ON [prl].[RelationListId] = [rl].[RelationListId]
	WHERE [rl].[RelationListTypeId] = @RelationListTypeId
	GROUP BY [rl].[RelationListId]
	HAVING COUNT([prl].[ProductId]) <= 1
	
	
	-- Delete redundant Relation Lists
	-- Delete all Product - Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tProductRelationList
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List - Product where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationListProduct
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationList 
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)
END
GO
