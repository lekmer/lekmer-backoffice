SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionIncludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END

GO
