SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCartItemPackageElementSave]
	@CartItemPackageElementId INT,
	@CartItemId INT,
	@ProductId INT,
	@SizeId INT,
	@ErpId VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[orderlek].[tCartItemPackageElement]
	SET	
		[CartItemId] = @CartItemId,
		[ProductId] = @ProductId,
		[SizeId] = @SizeId,
		[ErpId] = @ErpId
	WHERE
		[CartItemPackageElementId] = @CartItemPackageElementId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	INTO [orderlek].[tCartItemPackageElement]
				( [CartItemId],
				  [ProductId],
				  [SizeId],
				  [ErpId]
           		)
		VALUES
				( @CartItemId,
				  @ProductId,
				  @SizeId,
				  @ErpId
           		)

		SET @CartItemPackageElementId = SCOPE_IDENTITY()
	END
	
	RETURN @CartItemPackageElementId
END
GO
