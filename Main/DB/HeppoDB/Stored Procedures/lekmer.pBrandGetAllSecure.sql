SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pBrandGetAllSecure] 
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	     *
	FROM
	    lekmer.vBrandSecure 
	ORDER BY
		cast(lower([Brand.Title]) AS binary)
END



GO
