SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionIncludeInsert]
	@ConditionId		INT,
	@CustomerGroupId	INT
AS
BEGIN
	INSERT [campaignlek].[tCustomerGroupConditionInclude] (
		ConditionId,
		CustomerGroupId
	)
	VALUES (
		@ConditionId,
		@CustomerGroupId
	)
END
GO
