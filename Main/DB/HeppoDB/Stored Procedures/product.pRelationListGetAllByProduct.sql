SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****************  Version 2  *****************
User: Yura P.		Date: 04.09.2008		Time: 16:35
Description:
	Add RelationListTypeName
*/

CREATE PROCEDURE [product].[pRelationListGetAllByProduct]
	@ProductId int		
as
begin
	
	Select
		rl.*
	From
		[product].[vCustomRelationList] as rl 
		inner join [product].[tProductRelationList] as prl on rl.[RelationList.Id] = prl.[RelationListId]
	Where 
		prl.ProductId = @ProductId
end

GO
