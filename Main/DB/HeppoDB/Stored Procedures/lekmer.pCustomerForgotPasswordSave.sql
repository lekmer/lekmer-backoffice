SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pCustomerForgotPasswordSave]
	@CustomerId	int,
	@Guid uniqueidentifier,
	@CreatedDate datetime
as	
begin
	set nocount on
	insert 
		[lekmer].[tCustomerForgotPassword]
	values(
		@CustomerId,
		@Guid,
		@CreatedDate)
end
GO
