SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Anders Brandtsfridh
-- Create date: 2010-05-20
-- Description:	
-- =============================================
CREATE PROCEDURE [lekmer].[pBrandGetAll]
	@ChannelId int 
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	     *
	FROM
	    lekmer.vBrand
	WHERE 
		ChannelId = @ChannelId
	ORDER BY
		cast(lower([Brand.Title]) AS binary)
END



GO
