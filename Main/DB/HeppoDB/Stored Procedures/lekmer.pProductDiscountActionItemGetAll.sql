
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemGetAll]
	@ChannelId		int,
	@CustomerId		int,
	@ActionId		int
AS
BEGIN
	SELECT
		A.*
	FROM
		lekmer.vProductDiscountActionItem A
		INNER JOIN product.vCustomProduct P ON A.[ProductDiscountActionItem.ProductId] = P.[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		A.[ProductDiscountActionItem.ActionId] = @ActionId
		and P.[Product.ChannelId] = @ChannelId
END
GO
