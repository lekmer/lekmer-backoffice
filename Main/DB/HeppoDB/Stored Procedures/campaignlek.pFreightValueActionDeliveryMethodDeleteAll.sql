SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFreightValueActionDeliveryMethodDeleteAll]
	@ActionId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[campaignlek].[tFreightValueActionDeliveryMethod]
	WHERE
		[CartActionId] = @ActionId
END
GO
