SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconDelete]
	@IconId	INT
AS
BEGIN
	DELETE FROM [lekmer].[tLekmerProductIcon]
	WHERE [IconId] = @IconId
	
	DELETE FROM [lekmer].[tBrandIcon]
	WHERE [IconId] = @IconId
	
	DELETE FROM [lekmer].[tCategoryIcon]
	WHERE [IconId] = @IconId
		
	EXEC [lekmer].[pIconTranslationDelete] @IconId	
		
	DELETE FROM [lekmer].[tIcon]
	WHERE [IconId] = @IconId
END
GO
