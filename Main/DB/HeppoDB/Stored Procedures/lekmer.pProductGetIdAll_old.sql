SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [lekmer].[pProductGetIdAll_old]
	@ChannelId INT,
	@CustomerId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	
	DECLARE @CustomerIdString VARCHAR(10)
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))

	SET @sqlFilter = '
		(
			SELECT ROW_NUMBER() OVER (ORDER BY p.[Product.Id]) AS Number,
			p.[Product.Id]
			FROM [product].[vCustomProduct] p
			INNER JOIN product.vCustomPriceListItem AS pli
				ON pli.[Price.ProductId] = P.[Product.Id]
				AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], ' + @CustomerIdString + ')
			WHERE
				p.[Product.ChannelId] = ' + CAST(@ChannelId AS VARCHAR(10)) + '
		)'

	SET @sql = '
		SELECT * FROM
		' + @sqlFilter + '
		AS SearchResult'
	SET @sqlCount = '
		SELECT COUNT(1) FROM
		' + @sqlFilter + '
		AS SearchResultsCount'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
			AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END	

	EXEC (@sqlCount)
	EXEC (@sql)
END


GO
