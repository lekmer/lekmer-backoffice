SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupFolderGetById]
@CustomerGroupFolderId	int
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerGroupFolder]
	WHERE
		[CustomerGroupFolder.Id] = @CustomerGroupFolderId
END

GO
