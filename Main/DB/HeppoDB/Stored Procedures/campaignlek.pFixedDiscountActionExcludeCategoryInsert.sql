SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionExcludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END
GO
