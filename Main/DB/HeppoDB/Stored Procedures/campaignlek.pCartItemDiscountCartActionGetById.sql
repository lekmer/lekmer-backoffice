SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemDiscountCartAction]
	WHERE
		[CartItemDiscountCartAction.CartActionId] = @CartActionId
END
GO
