SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pBlockTopListProductGetIdAllByCategoryIdList]
	@ChannelId		int,
	@IncludeAllCategories	bit,
	@CategoryIds	varchar(max),
	@Delimiter		char(1),
	@TotalProductCount	int
as
begin
	SELECT
		Number,
		ProductId
	FROM
	(
		SELECT TOP (@TotalProductCount)
			ROW_NUMBER() OVER (ORDER BY sum(oi.[OrderItem.Quantity]) desc) AS Number,
			[p].[Product.Id] as ProductId
		FROM
			product.vCustomProduct p
			inner join [order].vCustomOrderItem oi WITH(NOLOCK) on oi.[OrderItem.ProductId] = p.[Product.Id]
			inner join [order].vCustomOrder o WITH(NOLOCK) on oi.[OrderItem.OrderId] = o.[Order.OrderId]
		where
			p.[Product.ChannelId] = @ChannelId
			and o.[Order.ChannelId] = @ChannelId
			and (
				@IncludeAllCategories = 1
				or (
					p.[Product.CategoryId] in (
						SELECT
							ID
						FROM
							[generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)
					)
				)
			)
		group by
			p.[Product.Id]
	) as Products
end


GO
