SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@Value			DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailCartActionCurrency] (
		CartActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@Value
	)
END
GO
