SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pVoucherActionValuesGetByActionId]
	@ActionId int
as
begin
	SELECT
		*
	FROM
		lekmer.vCustomVoucherActionCurrency
	WHERE 
		[VoucherAction.ActionId] = @ActionId
end


GO
