SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tCartItemDiscountCartActionIncludeProduct] c
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = c.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END

GO
