SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pSqlJobProductsOutOfStock]

AS
begin
	set nocount on		
	begin try
		begin transaction
		
		
		insert into integration.[tProductOutOfStock](ProductId, SizeId, [OutOfStockDate])
		select 		
			p.ProductId,
			coalesce(ps.SizeId, 0),
			GETDATE()
			-- select p.productid, ps.sizeid, ps.numberinstock, p.numberinstock
		from
			product.tProduct p
			left join lekmer.tProductSize ps
				on p.ProductId = ps.ProductId
		where
			p.ProductStatusId = 0 -- Bevaka produkter
			and (p.NumberInStock = 0 or ps.NumberInStock = 0)
			and not exists (select 1
							from integration.[tProductOutOfStock] 
							where ProductId = p.ProductId
							and SizeId = coalesce(ps.SizeId, 0))
			
		
		delete from 
			integration.[tProductOutOfStock] 
		where
			ProductId in (select distinct ProductId from lekmer.tProductSize
							where NumberInStock > 0)
		
		
		delete from 
			integration.[tProductOutOfStock] 
		where
			ProductId in (select distinct ProductId from product.tProduct
							where NumberInStock > 0
							and ProductStatusId = 0
							and ProductId not in (select distinct ProductId from lekmer.tProductSize))
							
					
			
		commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
