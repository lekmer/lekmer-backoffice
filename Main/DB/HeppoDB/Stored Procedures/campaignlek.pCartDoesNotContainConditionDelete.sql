SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartDoesNotContainConditionDelete]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tCartDoesNotContainCondition] WHERE [ConditionId] = @ConditionId)
	
	DELETE FROM
		[campaignlek].[tCartDoesNotContainCondition]
	WHERE
		[ConditionId] = @ConditionId
		
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
