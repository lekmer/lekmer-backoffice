SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
***********************  Version 1  **********************
User: Roman G.		Date: 12/23/2008		Time: 17:17:26
Description: Created 

***********************  Version 2  **********************
User: Roman A.		Date: 12/24/2008
Description: Renamed 
*/

create PROCEDURE [sitestructure].[pContentNodeChangeOrder]
	@ContentNodeId	INT,
	@Ordinal		INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[sitestructure].[tContentNode]
	SET
		[Ordinal] = @Ordinal
	WHERE
		[ContentNodeId] = @ContentNodeId
END
GO
