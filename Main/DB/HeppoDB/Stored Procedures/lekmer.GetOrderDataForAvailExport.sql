
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[GetOrderDataForAvailExport] 
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		COALESCE(lo.CustomerIdentificationKey, CAST(o.CustomerId AS VARCHAR(50))) AS CustomerId,
		p.ProductId,
		o.OrderId
	FROM
		[order].[tOrder] o
		INNER JOIN [lekmer].[tLekmerOrder] lo ON o.OrderId = lo.OrderId
		INNER JOIN [order].[tOrderItem] oi ON o.OrderId = oi.OrderId
		INNER JOIN [order].[tOrderItemProduct] oip ON oi.OrderItemId = oip.OrderItemId
		INNER JOIN [product].[tProduct] p ON oip.ErpId = p.ErpId
END
GO
