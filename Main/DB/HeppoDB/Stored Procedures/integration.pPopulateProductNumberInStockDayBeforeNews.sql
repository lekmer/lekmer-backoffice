SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pPopulateProductNumberInStockDayBeforeNews]

AS
begin
	set nocount on
	begin try
		begin transaction
			
			-- Empty the table
			DELETE FROM integration.tProductNumberInStockDayBeforeNews
			
			-- Insert noSize products
			INSERT INTO integration.tProductNumberInStockDayBeforeNews
				(
					ProductId,
					SizeId,
					NoInStock,
					[Date]
				)
			SELECT
				p.ProductId,
				0 AS SizeId,
				p.NumberInStock,
				GETDATE()
			FROM
				product.tProduct p WITH (NOLOCK)
				LEFT JOIN lekmer.tProductSize s  WITH (NOLOCK)
					ON p.ProductId = s.ProductId
			WHERE
				s.ProductId IS NULL
			
			
			-- Insert Size products
			INSERT INTO integration.tProductNumberInStockDayBeforeNews
				(
					ProductId,
					SizeId,
					NoInStock,
					[Date]
				)
			SELECT
				p.ProductId,
				s.SizeId,
				s.NumberInStock,
				GETDATE()
			FROM
				product.tProduct p WITH (NOLOCK)
				LEFT JOIN lekmer.tProductSize s WITH (NOLOCK)
					ON p.ProductId = s.ProductId
			WHERE
				s.ProductId IS NOT NULL

		commit transaction
	end try
	begin catch

		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	
	end catch		 
end
GO
