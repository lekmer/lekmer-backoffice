SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemFixedDiscountActionIncludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
