SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageValidate]
@ContentNodeId				int,
@CommonName					nvarchar(100),
@ParentContentNodeId		int,
@SiteStructureRegistryId	int,
@UrlTitle					varchar(200)				
as
begin
	declare @error int
	set @error = 0	
	
	if  exists (select 1
				from
				[sitestructure].[tContentNode] as CN
				inner join  [sitestructure].[tContentPage] as CP
				on CP.[ContentNodeId] = CN.[ContentNodeId]
				where				
				((@ParentContentNodeId IS NULL And CN.[ParentContentNodeId] IS NULL) OR CN.[ParentContentNodeId] = @ParentContentNodeId)
				and CP.[ContentNodeId] <> @ContentNodeId
				AND CN.SiteStructureRegistryId = @SiteStructureRegistryId
				and CP.[UrlTitle] = @UrlTitle
				)
	set @error = @error + 2

	if (@error > 0)
		return 0 - @error
end

GO
