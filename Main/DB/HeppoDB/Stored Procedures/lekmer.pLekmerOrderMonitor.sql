SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerOrderMonitor]
	@Date				DATETIME,
	@MinAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder] 
	WHERE
		[Order.CreatedDate] >= DATEADD(MINUTE, -@MinAfterPurchase, @Date)
		AND [Order.CreatedDate] < @Date
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
