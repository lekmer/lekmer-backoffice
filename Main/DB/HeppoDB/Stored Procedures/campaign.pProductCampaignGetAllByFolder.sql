SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductCampaignGetAllByFolder]
	@FolderId int,
	@Page int,
	@PageSize int,
    @SortBy VARCHAR(20) = NULL,
    @SortDescending BIT = NULL
as
begin
        SET NOCOUNT ON
	
        IF ( generic.fnValidateColumnName(@SortBy) = 0 ) 
        BEGIN
            RAISERROR ( N'Illegal characters in string (parameter @SortBy): %s' , 16 , 1 , @SortBy ) ;
            RETURN
        END

        DECLARE @sql NVARCHAR(MAX)
        DECLARE @sqlCount NVARCHAR(MAX)
        DECLARE @sqlFragment NVARCHAR(MAX)

        SET @sqlFragment = '
			SELECT
				ROW_NUMBER() OVER (
					ORDER BY c.[Campaign.Title] ASC
				) AS Number,
				*
			FROM
				campaign.vCustomProductCampaign c
			WHERE
				[Campaign.FolderId] = ' + CAST(@FolderId AS VARCHAR(10))
			
        SET @sql = 'SELECT * FROM
			(' + @sqlFragment + '
			)
			AS SearchResult'
			
        IF @Page != 0 AND @Page IS NOT NULL 
        BEGIN
            SET @sql = @sql + '
			WHERE Number > ' + CAST(( @Page - 1 ) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
        END
		
        SET @sqlCount = 'SELECT COUNT(1) FROM
			(
			' + @sqlFragment + '
			)
			AS CountResults'
			
        EXEC sp_executesql @sqlCount
        EXEC sp_executesql @sql
end

GO
