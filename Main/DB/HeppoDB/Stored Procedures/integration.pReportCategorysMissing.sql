
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportCategorysMissing]

AS
begin
	set nocount on
	
	select 
		HYErpId, 
		ArticleClassTitle, isnull(ArticleClassId, 'NULL') as ArticleClassId, 
		ArticleGroupTitle, isnull(ArticleGroupId, 'NULL') as ArticleGroupId,
		ArticleCodeTitle, isnull(ArticleCodeId , 'NULL') as ArticleCodeId
	from 
		import.tProduct
	where
		ChannelId in (1,2,3,4)
		and
			(
				ArticleClassTitle = '***Missing***'
				or ArticleGroupTitle = '***Missing***'
				or ArticleCodeTitle = '***Missing***'
			)
	


end
GO
