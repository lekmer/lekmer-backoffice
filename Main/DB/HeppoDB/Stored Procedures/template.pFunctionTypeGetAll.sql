SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pFunctionTypeGetAll]
AS
BEGIN
	SET NOCOUNT ON
	SELECT *
	FROM   [template].[vCustomFunctionType]
END

GO
