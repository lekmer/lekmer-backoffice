SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [messaging].[pRecipientDelete]
@RecipientId	uniqueidentifier
as
begin

	delete from 
		[messaging].[tRecipientCustomField]
	where
		RecipientId = @RecipientId

	delete from 
		[messaging].[tRecipient]
	where
		RecipientId = @RecipientId

end
GO
