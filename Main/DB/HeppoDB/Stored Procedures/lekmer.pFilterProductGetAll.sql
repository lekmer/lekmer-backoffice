
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pFilterProductGetAll]
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		/*Select only necessary columns (see data mapper)*/
		[FilterProduct.ProductId],
		[FilterProduct.CategoryId],
		--[FilterProduct.Title] ,
		[FilterProduct.BrandId] ,
		[FilterProduct.AgeFromMonth] ,
		[FilterProduct.AgeToMonth] ,
		[FilterProduct.IsNewFrom] ,
		--[FilterProduct.ChannelId] ,
		--[FilterProduct.CurrencyId] ,
		--[FilterProduct.PriceListRegistryId] ,
		[FilterProduct.Popularity] ,
		--[Price.PriceListId] ,
		--[Price.ProductId] ,
		[pp].[DiscountPriceIncludingVat] AS 'Price.DiscountPriceIncludingVat',
		[Price.PriceIncludingVat],
		--[Price.PriceExcludingVat] ,
		--[Price.VatPercentage]
		[FilterProduct.NumberInStock],
		[FilterProduct.ProductTypeId]
	FROM
		lekmer.vFilterProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[FilterProduct.ProductId]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[FilterProduct.CurrencyId],
				p.[FilterProduct.ProductId],
				p.[FilterProduct.PriceListRegistryId],
				@CustomerId
			)
		LEFT JOIN [export].[tProductPrice] pp
			ON p.[FilterProduct.ProductId] = pp.[ProductId]
			AND p.[FilterProduct.ChannelId] = pp.[ChannelId]
	WHERE
		[FilterProduct.ChannelId] = @ChannelId
	ORDER BY
		[FilterProduct.Title]
END
GO
