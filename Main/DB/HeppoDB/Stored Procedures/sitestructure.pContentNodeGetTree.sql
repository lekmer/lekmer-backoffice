SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeGetTree]
@SelectedId	int, /* @SelectedId < 0 means registry node, @SelectedId > 0 means content node */
@GetAllRegistries bit
AS
BEGIN
	DECLARE @RegistryId	int
	SET @RegistryId = CASE WHEN (@SelectedId < 0) THEN (-1) * @SelectedId 
		ELSE (SELECT c.[ContentNode.SiteStructureRegistryId] 
		      FROM sitestructure.vCustomContentNodeSecure AS c 
		      WHERE c.[ContentNode.ContentNodeId] = @SelectedId)
		END
	
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max), StatusId int, TypeId int, Ordinal int)
	
	INSERT INTO @tTree
		SELECT
			v.[SiteStructureRegistry.Id] * (-1),
			NULL,
			v.[SiteStructureRegistry.Title],
			(SELECT s.[ContentNodeStatus.Id] FROM sitestructure.vCustomContentNodeStatus AS s WHERE s.[ContentNodeStatus.CommonName] = 'Online'),
			(SELECT t.[ContentNodeType.ContentNodeTypeId] FROM sitestructure.vCustomContentNodeType AS t WHERE t.[ContentNodeType.CommonName] = 'Folder'),
			0
		FROM sitestructure.vCustomSiteStructureRegistry AS v
		WHERE v.[SiteStructureRegistry.Id] = @RegistryId OR @GetAllRegistries = 1
		
	INSERT INTO @tTree
		SELECT
			v.[ContentNode.ContentNodeId],
			v.[ContentNode.SiteStructureRegistryId] * (-1),
			v.[ContentNode.Title],
			v.[ContentNode.ContentNodeStatusId],
			v.[ContentNodeType.ContentNodeTypeId],
			v.[ContentNode.Ordinal]
		FROM sitestructure.vCustomContentNodeSecure AS v
		WHERE v.[ContentNode.SiteStructureRegistryId] = @RegistryId 
			AND v.[ContentNode.ParentContentNodeId] IS NULL
				
	IF (@SelectedId > 0)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[ContentNode.ContentNodeId],
					v.[ContentNode.ParentContentNodeId],
					v.[ContentNode.Title],
					v.[ContentNode.ContentNodeStatusId],
					v.[ContentNodeType.ContentNodeTypeId],
					v.[ContentNode.Ordinal]
				FROM sitestructure.vCustomContentNodeSecure AS v
				WHERE v.[ContentNode.ParentContentNodeId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[ContentNode.ParentContentNodeId]
									 FROM sitestructure.vCustomContentNodeSecure AS v
									 WHERE v.[ContentNode.ContentNodeId] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT
							v.[ContentNode.ContentNodeId],
							v.[ContentNode.ParentContentNodeId],
							v.[ContentNode.Title],
							v.[ContentNode.ContentNodeStatusId],
							v.[ContentNodeType.ContentNodeTypeId],
							v.[ContentNode.Ordinal]
						FROM sitestructure.vCustomContentNodeSecure AS v
						WHERE v.[ContentNode.ParentContentNodeId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		DECLARE @PageTypeId int
		SET @PageTypeId = (SELECT ContentNodeTypeId
		                     FROM sitestructure.tContentNodeType WHERE CommonName = 'Page')
		
		SELECT 
			Id,	ParentId, Title, StatusId, TypeId,
			CAST((CASE WHEN (Id < 0) THEN 
				(CASE WHEN EXISTS (SELECT 1 FROM sitestructure.vCustomContentNodeSecure v
									WHERE v.[ContentNode.SiteStructureRegistryId] = -1 * Id)
				THEN 1 ELSE 0 END) ELSE 
				(CASE WHEN EXISTS (SELECT 1 FROM sitestructure.vCustomContentNodeSecure v
								WHERE v.[ContentNode.ParentContentNodeId] = Id)
				THEN 1 ELSE 0 END) END) AS bit) AS 'HasChildren',
			CAST((CASE WHEN (TypeId <> @PageTypeId) THEN 0 ELSE 
				(SELECT cp.IsMaster FROM sitestructure.tContentPage cp 
				 WHERE cp.ContentNodeId = Id) END) AS bit) AS 'IsMasterPage',
			CAST((CASE WHEN (TypeId <> @PageTypeId) THEN 0 ELSE 
				(CASE WHEN EXISTS (SELECT 1 FROM sitestructure.tContentPage cp 
				 WHERE cp.ContentNodeId = Id AND cp.MasterPageId IS NOT NULL) THEN 1 ELSE 0 END) 
					END) AS bit) AS 'HasMasterPage'
		FROM @tTree
		ORDER BY Ordinal ASC
END

GO
