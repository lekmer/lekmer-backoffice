SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCampaignSetType]
@CampaignId		int,
@Exclusive		bit
AS
BEGIN
	UPDATE [campaign].[tCampaign]
	SET Exclusive = @Exclusive
	WHERE CampaignId = @CampaignId
END
GO
