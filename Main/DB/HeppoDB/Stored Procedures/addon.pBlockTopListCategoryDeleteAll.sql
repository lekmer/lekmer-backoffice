SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure addon.pBlockTopListCategoryDeleteAll
	@BlockId int
as
begin
	delete addon.tBlockTopListCategory
	where BlockId = @BlockId
end

GO
