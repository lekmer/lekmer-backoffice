SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListCategory]
	WHERE  [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandTopListBrand]
	WHERE  [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandTopList]
	WHERE  [BlockId] = @BlockId
END
GO
