
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSitestructureSeoAccessoriesCategories]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@CHannelId			INT
AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Underkategori accessories>		
		------------------------------------------------------------
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		DECLARE @StartPageMenAccessoriesContentPageId INT
		DECLARE @StartPageWomenAccessoriesContentPageId INT
		DECLARE @StartPageChildrenAccessoriesContentPageId INT
		DECLARE @StartPageMenAccessoriesCategoriesContentPageId INT
		DECLARE @StartPageWomenAccessoriesCategoriesContentPageId INT
		DECLARE @StartPageAccessoriesChildrenCategoriesContentPageId INT
		
		DECLARE @StartPageAllAccessoriesContentPageId INT
		DECLARE @StartPageAllAccessoriesCategoriesContentPageId INT
		
		SET @SiteStructureRegistryId = (Select SiteStructureRegistryId
											from sitestructure.tSiteStructureModuleChannel																					
											where ChannelId = @ChannelId)
												
		SET @StartPageMenAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessoriesMen')
									
		SET @StartPageWomenAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessoriesWomen')
					
		SET @StartPageChildrenAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessoriesChildren')
		
		SET @StartPageAllAccessoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'startPageAccessories')
									
		SET @StartPageMenAccessoriesCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'menAccessoriesCategories')
							
		SET @StartPageWomenAccessoriesCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'womenAccessoriesCategories')
							
		--SET @StartPageAccessoriesChildrenCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									--and CommonName = 'childrenCategories')
								
		SET @StartPageAllAccessoriesCategoriesContentPageId = (Select ContentNodeId from sitestructure.tContentNode where SiteStructureRegistryId = @SiteStructureRegistryId
									and CommonName = 'allAccessoriesCategories')
									
		-- DEFAULT --
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n
		where 
			n.SiteStructureRegistryId = @SiteStructureRegistryId
			and n.ContentNodeTypeId = 3 -- contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
					

		UPDATE
			cps
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[Underkategori]', cn1.Title),
			@NewTitle = REPLACE(@NewTitle, '[Huvudkategori]', cn3.Title),
				
			cps.Title = @NewTitle,
			
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[Underkategori]', cn1.Title),
			@NewDescription = REPLACE(@NewDescription, '[Huvudkategori]', cn3.Title),
			
			cps.[Description] = @NewDescription
			--select cps.title, cps.description, cn1.title, cn2.title, cn3.title, cn1.contentnodeid
		FROM			
			sitestructure.tContentPageSeoSetting cps			
			--inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId
			--inner join sitestructure.tContentNode cn2 on cn2.ContentNodeId = cn1.ParentContentNodeId
			--inner join sitestructure.tContentNode cn3 on cn3.ContentNodeId = cn2.ParentContentNodeId
			
			inner join sitestructure.tContentNode cn1 on cn1.ContentNodeId = cps.ContentNodeId				
			inner join sitestructure.tContentNode cn2 on cn1.ParentContentNodeId = cn2.ContentNodeId
			inner join sitestructure.tContentNode cn3 on cn1.ParentContentNodeId = cn2.ContentNodeId						
		WHERE
			cn1.SiteStructureRegistryId = @SiteStructureRegistryId
			and ((cn3.ContentNodeId = @StartPageWomenAccessoriesContentPageId and cn2.ContentNodeId = @StartPageWomenAccessoriesCategoriesContentPageId) -- damskor, kategorier
			or   (cn3.ContentNodeId = @StartPageMenAccessoriesContentPageId and cn2.ContentNodeId = @StartPageMenAccessoriesCategoriesContentPageId) -- herrskor, kategorier
			--or   (cn3.ContentNodeId = @StartPageChildrenContentPageId and cn2.ContentNodeId = @StartPageChildrenCategoriesContentPageId) -- Barnskor
			or   (cn3.ContentNodeId = @StartPageAllAccessoriesContentPageId and cn2.ContentNodeId = @StartPageAllAccessoriesCategoriesContentPageId)) -- Alla kategorier
			and
				((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))						

			
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
