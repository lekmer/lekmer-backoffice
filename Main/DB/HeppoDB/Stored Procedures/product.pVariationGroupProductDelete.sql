SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [product].[pVariationGroupProductDelete]
	@VariationGroupId	int
as
begin
		delete from [product].[tVariationGroupProduct] where [VariationGroupId] = @VariationGroupId		
end
GO
