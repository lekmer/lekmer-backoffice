
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoGetAllByOrder]
	@OrderId INT
AS 
BEGIN
	SELECT
		gcvmi.*
	FROM
		[campaignlek].[vGiftCardViaMailInfo] gcvmi
	WHERE
		gcvmi.[GiftCardViaMailInfo.OrderId] = @OrderId
END
GO
