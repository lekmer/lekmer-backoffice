SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetByAreaId]
@ContentAreaId	int
AS
BEGIN
	SELECT 
		*
	FROM 
		[sitestructure].[vCustomContentPageSecure] 
	WHERE
		[ContentPage.TemplateId] IN 
		(
			SELECT 
				[ContentArea.TemplateId] 
			FROM 
				sitestructure.vCustomContentArea 
			WHERE 
				[ContentArea.Id] = @ContentAreaId
		)
END

GO
