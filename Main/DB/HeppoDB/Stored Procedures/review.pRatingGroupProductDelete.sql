SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupProductDelete]
	@RatingGroupId	INT,
	@ProductId		INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingGroupId IS NULL AND @ProductId IS NULL)
	RETURN

	DELETE FROM [review].[tRatingGroupProduct]
	WHERE
		([RatingGroupId] = @RatingGroupId OR @RatingGroupId IS NULL)
		AND
		([ProductId] = @ProductId OR @ProductId IS NULL)
END
GO
