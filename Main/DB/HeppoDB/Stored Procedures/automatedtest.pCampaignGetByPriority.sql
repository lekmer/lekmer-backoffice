SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [automatedtest].[pCampaignGetByPriority]
	@Priority int
as
begin
	select
		C.*
	FROM campaign.vCustomCampaign C 
		INNER JOIN campaign.tCartCampaign PC ON ((PC.CampaignId = C.[Campaign.Id])AND ([Campaign.Priority] = @Priority))
end

GO
