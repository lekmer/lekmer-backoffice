SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherActionSave]
	@ActionId	int,
	@Fixed bit,
	@Percentage bit,
	@DiscountValue decimal(16,2)
AS 
BEGIN 
	UPDATE
		lekmer.tVoucherAction
	SET
		Fixed = @Fixed,
		Percentage=@Percentage,
		DiscountValue=@DiscountValue
	WHERE
		CartActionId = @ActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT 
			lekmer.tVoucherAction
		(
			CartActionId,
			Fixed,
			Percentage,
			DiscountValue
		)
		VALUES
		(
			@ActionId,
			@Fixed,
			@Percentage,
			@DiscountValue
		)
	END
END
GO
