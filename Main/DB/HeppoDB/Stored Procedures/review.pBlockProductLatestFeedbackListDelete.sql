SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockProductLatestFeedbackList]
	WHERE BlockId = @BlockId
END
GO
