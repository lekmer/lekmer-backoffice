SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupSave]
@CustomerGroupId		int,
@FolderId				int,
@Title					nvarchar(50),
@StatusId				int,
@ErpId					varchar(50)
AS
BEGIN
	IF EXISTS
		(
			SELECT 1 FROM customer.tCustomerGroup 
			WHERE Title = @Title 
				AND CustomerGroupId <> @CustomerGroupId
				AND (CustomerGroupFolderId = @FolderId 
					OR (@FolderId IS NULL AND CustomerGroupFolderId IS NULL))
		)
		RETURN -1
	
	UPDATE 
		customer.tCustomerGroup
	SET
		CustomerGroupFolderId = @FolderId,
		Title = @Title,
		StatusId = @StatusId,
		ErpId = @ErpId
	WHERE 
		CustomerGroupId = @CustomerGroupId
		
	if @@ROWCOUNT = 0
		BEGIN
			INSERT INTO 
				customer.tCustomerGroup 
				(
					CustomerGroupFolderId,
					Title, 
					StatusId, 
					ErpId
				)
			VALUES
				(
					@FolderId,
					@Title, 
					@StatusId, 
					@ErpId
				)
			SET @CustomerGroupId = SCOPE_IDENTITY()
		END
	
	RETURN @CustomerGroupId
END

GO
