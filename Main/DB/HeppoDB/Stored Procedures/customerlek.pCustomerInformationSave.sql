
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customerlek].[pCustomerInformationSave]
	@CustomerId INT,
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@CivicNumber NVARCHAR(50),
	@PhoneNumber NVARCHAR(50),
	@CellPhoneNumber NVARCHAR(50),
	@Email VARCHAR(320),
	@CreatedDate DATETIME,
	@DefaultBillingAddressId INT = NULL,
	@DefaultDeliveryAddressId INT = NULL,
	@GenderTypeId INT,
	@IsCompany BIT,
	@AlternateAddressId INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[customer].[tCustomerInformation]
	SET    
		FirstName = @FirstName, 
		LastName = @LastName,
		CivicNumber = @CivicNumber,
		PhoneNumber = @PhoneNumber,
		CellPhoneNumber = @CellPhoneNumber,
		Email = @Email,
		DefaultBillingAddressId = @DefaultBillingAddressId,
		DefaultDeliveryAddressId = @DefaultDeliveryAddressId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customer].[tCustomerInformation]
		(
			CustomerId,
			FirstName, 
			LastName,
			CivicNumber,
			PhoneNumber,
			CellPhoneNumber,
			Email,
			CreatedDate,
			DefaultBillingAddressId,
			DefaultDeliveryAddressId
		)
		VALUES
		(
			@CustomerId,
			@FirstName, 
			@LastName,
			@CivicNumber,
			@PhoneNumber,
			@CellPhoneNumber,
			@Email,
			@CreatedDate,
			@DefaultBillingAddressId,
			@DefaultDeliveryAddressId
		)
	END
	
	-- Save Lekmer part
	
	UPDATE
		[customerlek].[tCustomerInformation]
	SET    
		GenderTypeId = @GenderTypeId,
		IsCompany = @IsCompany,
		AlternateAddressId = @AlternateAddressId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customerlek].[tCustomerInformation]
		(
			CustomerId,
			GenderTypeId,
			IsCompany,
			AlternateAddressId
		)
		VALUES
		(
			@CustomerId,
			@GenderTypeId,
			@IsCompany,
			@AlternateAddressId
		)
	END

	RETURN @CustomerId
END
GO
