SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductSeoSettingGetById]
	@LanguageId	INT,
	@ProductId	int
AS
BEGIN
	  SELECT
			PSS.*
	  FROM
			[product].[vCustomProductSeoSetting] AS PSS
	  WHERE
			PSS.[SeoSetting.ProductId] = @ProductId
            AND PSS.[SeoSetting.LanguageId] = @LanguageId
	 		
END

GO
