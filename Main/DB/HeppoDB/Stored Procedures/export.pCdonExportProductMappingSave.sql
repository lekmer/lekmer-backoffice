SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportProductMappingSave]
	@CdonProductId	INT,
	@CdonArticleId	INT
AS	
BEGIN
	SET NOCOUNT ON
	
	IF NOT EXISTS (SELECT 1 FROM [export].[tCdonExportProductMapping] WHERE [CdonArticleId] = @CdonArticleId)
		BEGIN
			INSERT INTO	[export].[tCdonExportProductMapping]
				(
					[CdonProductId],
					[CdonArticleId]
				)
			VALUES
				(
					@CdonProductId,
					@CdonArticleId
				)
			
			SELECT @CdonProductId
		END
	ELSE 
		BEGIN
			SELECT [CdonProductId] 
			FROM [export].[tCdonExportProductMapping] 
			WHERE [CdonArticleId] = @CdonArticleId
		END
END
GO
