SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationGetAllByVariationType]
	@VariationTypeId	int
as
begin
	set nocount off
	SELECT 
		V.*
	FROM 
		[product].[vCustomVariationSecure] V
	where
		V.[Variation.TypeId] = @VariationTypeId
	order by V.[Variation.Ordinal]
end

GO
