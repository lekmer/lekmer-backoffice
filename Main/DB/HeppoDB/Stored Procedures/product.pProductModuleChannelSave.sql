SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [product].[pProductModuleChannelSave]
@ChannelId	int,
@ProductTemplateContentNodeId int
as
begin

	UPDATE 
		product.tProductModuleChannel
	SET 
		ProductTemplateContentNodeId = @ProductTemplateContentNodeId
	WHERE 
		ChannelId = @ChannelId
	
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO product.tProductModuleChannel
		(
			ChannelId, 
			ProductTemplateContentNodeId
		)
		VALUES 
		(
			@ChannelId, 
			@ProductTemplateContentNodeId
		)
	END
end
GO
