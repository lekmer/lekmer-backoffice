SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [review].[pRatingReviewFeedback_SetStatus]
	@RatingReviewFeedbackId	INT,
	@StatusId	INT
AS    
BEGIN
	UPDATE 
		[review].[tRatingReviewFeedback]
	SET 
		RatingReviewStatusId = @StatusId
	WHERE 
		RatingReviewFeedbackId = @RatingReviewFeedbackId
END

GO
