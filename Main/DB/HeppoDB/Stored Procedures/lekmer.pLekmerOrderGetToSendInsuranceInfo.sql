SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerOrderGetToSendInsuranceInfo]
	@ChannelId				INT,
	@MaxDaysAfterPurchase	INT,
	@MinDaysAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentDate DATETIME
	DECLARE @MaxDate DATETIME
	DECLARE @MinDate DATETIME

	SET @CurrentDate = CONVERT (DATE, GETDATE())
	SET @MaxDate = (SELECT DATEADD(DAY, @MaxDaysAfterPurchase * -1, @CurrentDate))
	SET @MinDate = (SELECT DATEADD(DAY, @MinDaysAfterPurchase * -1, @CurrentDate))

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.ChannelId] = @ChannelId
		AND [Lekmer.NeedSendInsuranceInfo] = 1
		AND [OrderStatus.CommonName] = 'OrderInHY'
		AND [Order.CreatedDate] >= @MaxDate
		AND [Order.CreatedDate] < @MinDate
END
GO
