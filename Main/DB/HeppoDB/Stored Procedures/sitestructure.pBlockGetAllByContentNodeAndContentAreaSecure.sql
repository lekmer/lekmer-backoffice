SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetAllByContentNodeAndContentAreaSecure]
	@ContentNodeId	int,
	@ContentAreaId	int
as
begin
	select 
		B.*
	from 
		[sitestructure].[vCustomBlockSecure] as B
	where
		[Block.ContentNodeId] = @ContentNodeId and 
		[Block.ContentAreaId] = @ContentAreaId
	order by [Block.Ordinal]
end

GO
