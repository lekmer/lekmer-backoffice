SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetAllByMaster]
@MasterId int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageSecure]
	WHERE 
		[ContentPage.MasterPageId] = @MasterId
end

GO
