SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartCampaignInsert]
	@CampaignId int
as
begin
	insert
		campaign.tCartCampaign
	(
		CampaignId
	)
	values
	(
		@CampaignId
	)
end
GO
