
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemDeleteAll]
	@RatingId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	EXEC [review].[pRatingItemTranslationDeleteAll] @RatingId
	
	DELETE FROM [review].[tRatingItem]
	WHERE [RatingId] = @RatingId
END
GO
