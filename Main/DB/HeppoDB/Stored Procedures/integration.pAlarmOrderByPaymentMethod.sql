SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pAlarmOrderByPaymentMethod]
    @ChannelId	INT,
    @Period     INT = 60,
    @FromHour   INT = 7,
    @ToHour     INT = 22
AS
BEGIN
    SET NOCOUNT ON;
   
    DECLARE @CurrentHour INT
    SET @CurrentHour = DATEPART(hh, GETDATE())
    
    IF (@CurrentHour >= @ToHour or @CurrentHour < @FromHour)
		BEGIN
		   SELECT
				@Period AS 'Period',
				1 AS 'OrderCount',
				ch.CommonName AS 'Channel',
				'All' AS 'Description'
		   FROM
				core.tChannel ch
		   WHERE
				ch.ChannelId = @ChannelId
		END
    ELSE
		BEGIN
			CREATE TABLE #tOrders
			(
				OrderId  INT,
				ChannelId  INT
			)
			INSERT INTO #tOrders
				SELECT
					o.OrderId,
					@ChannelId
				FROM
					[order].tOrder o
				WHERE
					o.CreatedDate > DATEADD(MINUTE, -@Period, GETDATE())
					AND o.OrderStatusId IN (4)
					AND o.ChannelId = @ChannelId
	      
			SELECT
				@Period AS 'Period',
				COUNT(o.OrderId) AS 'OrderCount',
				ch.CommonName AS 'Channel',
				pt.Title AS 'Description'
			FROM
				[order].tPaymentType pt
				LEFT JOIN [order].tOrderPayment op ON pt.PaymentTypeId = op.PaymentTypeId
				INNER JOIN #tOrders o ON op.OrderId = o.OrderId
				INNER JOIN core.tChannel ch ON o.ChannelId = ch.ChannelId
			WHERE pt.PaymentTypeId in (SELECT PaymentTypeId FROM [order].tChannelPaymentType WHERE ChannelId = @ChannelId)
			GROUP BY pt.Title, ch.CommonName
						
			DROP TABLE #tOrders
		END
END

-- payment types
/*
PaymentTypeId	CommonName			Title
1				CreditCard			Kreditkort
1000002			Invoice				Faktura
1000003			KlarnaPartpayment	Delbetala (Klarna) 
*/

-- Example
/*
[integration].[pAlarmOrderByPaymentMethod] @ChannelId = 1, @Period = 100000, @FromHour = 7, @ToHour = 22
*/
	
--Resultset
/*
100000	129	Sweden	Delbetala (Klarna)
100000	1382	Sweden	Faktura 
100000	353	Sweden	Kreditkort
*/
GO
