SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelSettingTemplateSettingExist]
    @ModelSettingId INT
AS 
BEGIN
    SET NOCOUNT ON
    
    IF EXISTS ( SELECT  *
                FROM    [template].[tTemplateSetting]
                WHERE   [ModelSettingId] = @ModelSettingId ) 
        RETURN 1      
    RETURN 0

END
GO
