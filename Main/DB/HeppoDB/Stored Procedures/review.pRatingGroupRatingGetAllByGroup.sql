SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupRatingGetAllByGroup]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgr.*
	FROM
		[review].[vRatingGroupRating] rgr
	WHERE
		rgr.[RatingGroupRating.RatingGroupId] = @RatingGroupId
END
GO
