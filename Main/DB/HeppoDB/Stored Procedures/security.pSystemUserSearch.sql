SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserSearch]
	@UserName			nvarchar(100),
	@Name				nvarchar(100),
	@StatusId			int,
	@ActivationDate		smalldatetime,
	@ExpirationDate		smalldatetime,
	@Page				int = NULL,
	@PageSize			int,
	@SortBy				varchar(20) = NULL,
	@SortDescending		bit = NULL
as
begin
   set nocount on

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	IF (@UserName IS NOT NULL)
	BEGIN
		SET @UserName = generic.fPrepareSearchParameter(@UserName)
	END
	IF (@Name IS NOT NULL)
	BEGIN
		SET @Name = generic.fPrepareSearchParameter(@Name)
	END

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' 
		+ COALESCE(@SortBy, 'SystemUser.Id')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			*
		from
			[security].[vCustomSystemUser]
		where 1=1'

		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
		AND [SystemUserStatus.Id] = @StatusId' ELSE '' END

		+ CASE WHEN (@Name IS NOT NULL) THEN + '
		AND [SystemUser.Name] LIKE @Name' ELSE '' END

		+ CASE WHEN (@UserName IS NOT NULL) THEN + '
		AND [SystemUser.UserName] LIKE @UserName' ELSE '' END

		+ CASE WHEN (@ActivationDate IS NOT NULL) THEN + '
		AND [SystemUser.ActivationDate] >= @ActivationDate ' ELSE '' END

		+ CASE WHEN (@ExpirationDate IS NOT NULL) THEN + '
		AND [SystemUser.ExpirationDate] <= @ExpirationDate ' ELSE '' END

	SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@UserName				nvarchar(100),
			@Name					nvarchar(100),
			@StatusId				int,
			@ActivationDate			smalldatetime,
			@ExpirationDate			SMALLDATETIME',
			@UserName,          
			@Name, 
			@StatusId,     
			@ActivationDate,
			@ExpirationDate
			
			     
	EXEC sp_executesql @sql, 
		N'	@UserName				nvarchar(100),
			@Name					nvarchar(100),
			@StatusId				int,
			@ActivationDate			smalldatetime,
			@ExpirationDate			SMALLDATETIME',
			@UserName,          
			@Name, 
			@StatusId,     
			@ActivationDate,
			@ExpirationDate
	end

GO
