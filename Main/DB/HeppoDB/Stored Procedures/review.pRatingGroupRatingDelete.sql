SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupRatingDelete]
	@RatingGroupId	INT,
	@RatingId		INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingGroupId IS NULL AND @RatingId IS NULL)
	RETURN

	DELETE FROM [review].[tRatingGroupRating]
	WHERE
		([RatingGroupId] = @RatingGroupId OR @RatingGroupId IS NULL)
		AND
		([RatingId] = @RatingId OR @RatingId IS NULL)
END
GO
