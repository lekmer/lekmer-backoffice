SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountCartActionCurrency]
	WHERE
		CartActionId = @CartActionId
END
GO
