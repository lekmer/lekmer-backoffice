
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 13.01.2009
Description:
			Created 
*/

CREATE PROCEDURE [core].[pCacheUpdateDeleteOld]
	@ToDate		DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DELETE
	FROM
		[core].[tCacheUpdate] WITH (TABLOCKX)
	WHERE
		[CreationDate] < @ToDate
END
GO
