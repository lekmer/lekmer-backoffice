SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeGetAllByProductIdList]
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ps.*
	FROM 
		[lekmer].[vProductSize] ps
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl 
			ON pl.Id = ps.[ProductSize.ProductId]
END
GO
