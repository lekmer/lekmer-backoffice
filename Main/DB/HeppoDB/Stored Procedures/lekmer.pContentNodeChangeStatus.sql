SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentNodeChangeStatus]
	@IsSetOnline	BIT,
	@ContentPageIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DECLARE @StatusId INT
	
	IF @IsSetOnline = 1
		SET @StatusId = (SELECT ContentNodeStatusId FROM [sitestructure].[tContentNodeStatus] WHERE CommonName = 'Online')
	ELSE
		SET @StatusId = (SELECT ContentNodeStatusId FROM [sitestructure].[tContentNodeStatus] WHERE CommonName = 'Offline')
		
	UPDATE cn
	SET cn.[ContentNodeStatusId] = @StatusId
	FROM [sitestructure].[tContentNode] cn
	WHERE cn.ContentNodeId IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ContentPageIds, @Delimiter))
END
GO
