SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pAlarmCheckRelativeOrderByChannel]
AS
BEGIN
	SET NOCOUNT ON
	
	
    declare @hour int
    select @hour = datepart(hh, getdate())
   
    if @hour >= 8 and @hour < 22
    begin
		 		 
		select
			case when prev30.OrderCount = 0 and prev15.OrderCount = 0 then 0
				when prev30.OrderCount > 0 then
					(cast((100.00 * (cast(prev15.OrderCount as decimal(12,2)) - cast(prev30.OrderCount as decimal(12,2))) / cast(prev30.OrderCount as decimal(12,2))) as int))
				else 100 end as 'Quota %',
			 prev15.OrderCount as 'OrderCount60',
			 prev30.OrderCount as 'OrderCount120',
			 'Heppo' as 'Site'
		  from
			 (
			 select
				count(*) as 'OrderCount',
				0 as 'ChannelId'
			 from
				[order].tOrder o
			 where
				o.CreatedDate between dateadd(minute, -120, getdate()) and dateadd(minute, -60, getdate()) and
				o.OrderStatusId = 4
			 ) prev30
			 inner join
			 (
			 select
				count(*) as 'OrderCount',
				0 as 'ChannelId'
			 from
				[order].tOrder o
			 where
				o.CreatedDate > dateadd(minute, -60, getdate()) and
				o.OrderStatusId = 4
			 ) prev15
				on prev30.ChannelId = prev15.ChannelId
		end
		else
		begin
		  select
			 0,
			 0,
			 0,
			 'heppo'
		end
		
   
END

GO
