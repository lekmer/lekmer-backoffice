
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--set statistics io on
	--set statistics time on
	
	SET NOCOUNT ON;

	DECLARE
		@Channel_SE INT,
		@Channel_NO INT,
		@Channel_DA INT,
		@Channel_FI INT,
		@Channel_FR INT,
		@Channel_NL INT,
		@PriceListId_FI INT,
		@PriceListId_FR INT,
		@PriceListId_NL INT,
		@ProductActionTypeId_Discount INT,
		@ProductActionTypeId_Percentage INT
		
	SELECT @Channel_SE = ChannelId FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId FROM core.tChannel WHERE CommonName = 'Finland'
	SELECT @Channel_FR = ChannelId FROM core.tChannel WHERE CommonName = 'France'
	SELECT @Channel_NL = ChannelId FROM core.tChannel WHERE CommonName = 'Netherlands'
	
	SELECT @ProductActionTypeId_Discount = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'ProductDiscount'
	
	SELECT @ProductActionTypeId_Percentage = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'PercentagePriceDiscount'
	
	SELECT @PriceListId_FI = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Finland'
	
	SELECT @PriceListId_FR = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'France'
	
	SELECT @PriceListId_NL = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Netherlands'
	
	---- #Campaign
	
	IF OBJECT_ID('tempdb..#Campaign') IS NOT NULL
		DROP TABLE #Campaign

	CREATE TABLE #Campaign
	(
	   CampaignId INT PRIMARY KEY,
	   PriceListId INT
	)

	INSERT INTO #Campaign (
		CampaignId,
		PriceListId
	)
	SELECT
		c.CampaignId,
		pl.PriceListId
	FROM
		campaign.tCampaign c
		LEFT OUTER JOIN campaign.tCampaignModuleChannel cmc ON cmc.CampaignRegistryId = c.CampaignRegistryId
		LEFT OUTER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = cmc.ChannelId
		LEFT OUTER JOIN product.tPriceList pl ON pl.PriceListRegistryId = pmc.PriceListRegistryId
	WHERE
		c.CampaignStatusId = 0
		AND (c.StartDate IS NULL OR c.StartDate <= GETDATE())
		AND (c.EndDate IS NULL OR GETDATE() <= c.EndDate)
		AND cmc.ChannelId in (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI, @Channel_FR, @Channel_NL)

	---- #CampaignAction
	
	IF OBJECT_ID('tempdb..#CampaignAction') IS NOT NULL
		DROP TABLE #CampaignAction

	CREATE TABLE #CampaignAction (
	   CampaignId INT,
	   SortIndex INT,   
	   ProductActionId INT,
	   Ordinal INT
	)

	DECLARE @Index INT
	SET @Index = 1

	INSERT INTO #CampaignAction (
		CampaignId,
		SortIndex,
		ProductActionId,
		Ordinal
	)
	SELECT
		pa.CampaignId,
		@index,
		pa.ProductActionId,
		pa.Ordinal
	FROM
		#Campaign c
		INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId
		LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal < pa.Ordinal --WTF?
	WHERE
		pa2.ProductActionId IS NULL

	WHILE (@@ROWCOUNT > 0) 
	BEGIN
		SET @Index = @Index + 1
  
		INSERT INTO #CampaignAction (
			CampaignId,
			SortIndex,
			ProductActionId,
			Ordinal
		)
		SELECT
			pa.CampaignId,
			@index,
			pa.ProductActionId,
			pa.Ordinal
		FROM
			#Campaign c
			INNER JOIN #CampaignAction tca ON tca.CampaignId = c.CampaignId AND tca.SortIndex = @Index - 1
			INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId AND pa.Ordinal > tca.Ordinal
			LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal > tca.Ordinal AND pa2.Ordinal < pa.Ordinal
		WHERE
			pa2.ProductActionId IS NULL
		OPTION (FORCE ORDER)
	END


	---- #CampaignProduct

	IF OBJECT_ID('tempdb..#CampaignProduct') IS NOT NULL
		DROP TABLE #CampaignProduct

	CREATE TABLE #CampaignProduct (
		CampaignId INT,
		PriceListId INT,
		ProductId INT,
		Price DECIMAL(16, 2) PRIMARY KEY (CampaignId, ProductId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		ip.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ppda].[ProductActionId]) ip

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		pdai.ProductId,
		NULL
	from
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = c.PriceListId AND pl.CurrencyId = pdai.CurrencyId

	-- Set product prices

	UPDATE cp
	SET Price = pli.PriceIncludingVat
	FROM
		#Campaign c
		INNER JOIN #CampaignProduct cp ON cp.CampaignId = c.CampaignId
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = c.PriceListId AND pli.ProductId = cp.ProductId

	DECLARE @LastIndex INT
	SET @Index = 1
	SELECT @LastIndex = MAX(SortIndex) FROM #CampaignAction

	WHILE (@Index <= @LastIndex)
	BEGIN
		UPDATE cp
		SET Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Percentage
			INNER JOIN campaign.tPercentagePriceDiscountAction ppda ON ppda.ProductActionId = pa.ProductActionId
			CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ppda].[ProductActionId]) ip
			INNER JOIN #CampaignProduct cp ON cp.ProductId = ip.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index

		UPDATE cp
		SET Price = pdai.DiscountPrice
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Discount
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = pdai.ProductId AND cp.CampaignId = ca.CampaignId
			INNER JOIN product.tPriceList pl ON pl.PriceListId = cp.PriceListId AND pl.CurrencyId = pdai.CurrencyId
		WHERE
			ca.SortIndex = @Index
			AND (cp.Price > pdai.DiscountPrice OR cp.Price IS NULL)

		SET @Index = @Index + 1
	END

	DELETE cp FROM #CampaignProduct cp WHERE Price IS NULL

	UPDATE cp
	SET price = CASE
					WHEN (c.PriceListId = @PriceListId_FI) or (c.PriceListId = @PriceListId_FR) or (c.PriceListId = @PriceListId_NL)
					THEN Price 
					ELSE ROUND(Price, 0)					
				END
	FROM
		#CampaignProduct cp
		INNER JOIN #Campaign c ON c.CampaignId = cp.CampaignId

	---- #BestCampaignPrice

	IF OBJECT_ID('tempdb..#BestCampaignPrice') IS NOT NULL
		DROP TABLE #BestCampaignPrice

	CREATE TABLE #BestCampaignPrice (
		ProductId INT,
		PriceListId INT,
		CampaignId INT,
		Price DECIMAL(16, 2)
	)

	INSERT INTO #BestCampaignPrice (
		ProductId,
		PriceListId,
		CampaignId,
		Price) -- Fel
	SELECT
		cp.ProductId,
		cp.PriceListId,
		cp.CampaignId,
		cp.Price
	FROM
		#CampaignProduct cp
		LEFT OUTER JOIN #CampaignProduct cp2 
			ON cp2.PriceListId = cp.PriceListId
				AND cp2.ProductId = cp.ProductId
				AND (cp2.Price < cp.Price OR cp2.Price = cp.Price AND cp2.CampaignId > cp.CampaignId)      
	WHERE cp2.ProductId IS NULL

	DROP TABLE #Campaign
	DROP TABLE #CampaignAction
	DROP TABLE #CampaignProduct

	---- @PriceList
	
	DECLARE @PriceList TABLE (
		PriceListId INT PRIMARY KEY,
		ChannelId INT
	)

	INSERT INTO @PriceList 
	SELECT
		pl.PriceListId,
		c.ChannelId
	FROM
		core.tChannel c
		INNER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = c.ChannelId
		INNER JOIN product.tProductRegistry pr ON pr.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = pr.ProductRegistryId
	WHERE
		c.ChannelId IN (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- Result

	SELECT 
		ch.CommonName,
		ll.HYErpId,
		t.Title as Product,
		pli.PriceIncludingVat as ListPrice,
		rr.Title as Campaign,
		CASE 
			WHEN (bcp.Price IS NULL OR bcp.Price > pli.PriceIncludingVat) THEN pli.PriceIncludingVat
			ELSE bcp.Price 
		END AS SitePrice,
		CAST(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) AS DECIMAL (18,2)) AS 'Total discount %',
		prs.Title as ProductStatus		
	FROM
		@PriceList pl
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = pl.PriceListId
		INNER JOIN product.tProduct t ON t.ProductId = pli.ProductId
		LEFT OUTER JOIN #BestCampaignPrice bcp ON bcp.PriceListId = pl.PriceListId AND bcp.ProductId = t.ProductId
		LEFT OUTER JOIN campaign.tCampaign c ON c.CampaignId = bcp.CampaignId
		INNER JOIN lekmer.tLekmerProduct ll ON ll.ProductId = pli.ProductId
		INNER JOIN product.tPriceList ch on ch.PriceListId = pli.PriceListId
		LEFT JOIN campaign.tCampaign rr on rr.CampaignId = bcp.CampaignId
		INNER JOIN product.tProductStatus prs on prs.ProductStatusId = t.ProductStatusId
	WHERE
		c.CampaignId IS NOT NULL
	ORDER BY
		pl.ChannelId
		
   DROP TABLE #BestCampaignPrice
   
END
GO
