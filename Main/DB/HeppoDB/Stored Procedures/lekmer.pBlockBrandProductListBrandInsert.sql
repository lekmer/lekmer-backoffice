SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [lekmer].[pBlockBrandProductListBrandInsert]
	@BlockId				int,
	@BrandId				int
as
BEGIN

			insert
				[lekmer].[tBlockBrandProductListBrand]
				(
					[BlockId],
					[BrandId]
				)
			values
				(
					@BlockId,
					@BrandId
				)
		
		
end




GO
