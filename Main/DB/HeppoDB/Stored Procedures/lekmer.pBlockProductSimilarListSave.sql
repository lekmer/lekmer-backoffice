
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockProductSimilarListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalProductCount INT,
	@PriceRangeType INT
AS
BEGIN
	UPDATE [lekmer].[tBlockProductSimilarList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[TotalProductCount] = @TotalProductCount,
		[PriceRangeType] = @PriceRangeType
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockProductSimilarList] (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalProductCount],
		[PriceRangeType]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalProductCount,
		@PriceRangeType
	)
END
GO
