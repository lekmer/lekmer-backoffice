SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tFixedDiscountActionIncludeBrand]
	WHERE 
		ProductActionId = @ProductActionId
END
GO
