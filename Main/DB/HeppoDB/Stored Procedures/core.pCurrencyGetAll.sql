SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pCurrencyGetAll]
as
begin
	set nocount on
	
	select
		*
	from
		[core].[vCustomCurrency]
end

GO
