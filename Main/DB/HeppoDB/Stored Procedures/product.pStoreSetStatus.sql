SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreSetStatus]
	@StoreId	int,
	@StatusId	int
as
begin
	UPDATE 
		product.tStore
	SET 
		StatusId = @StatusId
	WHERE
		StoreId = @StoreId
end

GO
