SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemDeleteAll]
	@ActionId int
AS 
BEGIN 
	DELETE
		lekmer.tProductDiscountActionItem
	WHERE
		ProductActionId = @ActionId
END 




GO
