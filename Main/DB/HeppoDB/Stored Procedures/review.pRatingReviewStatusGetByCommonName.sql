SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewStatusGetByCommonName]
	@CommonName VARCHAR(50)
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewStatus] rrs
	WHERE
		rrs.[RatingReviewStatus.CommonName] = @CommonName
END
GO
