SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE 	[lekmer].[pBlockAvailCombineDelete]
@BlockId	int
as
BEGIN
	
	

	DELETE 
		lekmer.tBlockAvailCombine
	where
		[BlockId] = @BlockId
end
GO
