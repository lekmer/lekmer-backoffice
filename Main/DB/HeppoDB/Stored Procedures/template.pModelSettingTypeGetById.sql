SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelSettingTypeGetById]
    @ModelSettingTypeId INT
AS 
BEGIN
    SET NOCOUNT ON

    SELECT  *
    FROM    [template].[vCustomModelSettingType]
    WHERE   [ModelSettingType.Id] = @ModelSettingTypeId

END

GO
