SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [customer].[pCustomerInsert]
@CustomerStatusId	INT,
@ERPId				VARCHAR(50),
@CustomerRegistryId	INT
AS	
BEGIN
	SET NOCOUNT ON	
			INSERT INTO	[customer].[tCustomer]
				(
					[CustomerStatusId],
					[ErpId],
					[CustomerRegistryId]
				)
			VALUES
				(
					@CustomerStatusId,
					@ErpId,
					@CustomerRegistryId
				)		
	RETURN  SCOPE_IDENTITY()	
END
GO
