SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pProductDiscountActionDelete]
	@ActionId int
AS 
BEGIN
	DELETE FROM 
		lekmer.tProductDiscountAction
	WHERE 
		ProductActionId = @ActionId
END
GO
