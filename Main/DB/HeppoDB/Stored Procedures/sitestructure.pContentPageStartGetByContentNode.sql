SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [sitestructure].[pContentPageStartGetByContentNode]
@ContentNodeId	INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @StartContentPageIdOfCurrentRegistry INT
	set @StartContentPageIdOfCurrentRegistry = 
		(SELECT 
			r.[StartContentNodeId]
		FROM
			[sitestructure].[tContentNode] AS n
			INNER JOIN [sitestructure].[tSiteStructureRegistry] AS r ON n.SiteStructureRegistryId=r.SiteStructureRegistryId
		WHERE 
			n.ContentNodeId=@ContentNodeId
		)
		
	
	DECLARE @tContentNode TABLE (tContentNodeId INT, tParentContentNodeId INT);

	WITH ContentNode (ContentNodeId, ParentContentNodeId) AS 
	(
		SELECT ContentNodeId, ParentContentNodeId
		FROM  sitestructure.tContentNode
		WHERE ParentContentNodeId = @ContentNodeId

		UNION ALL

		SELECT C.ContentNodeId, C.ParentContentNodeId
		FROM sitestructure.tContentNode C 
		JOIN ContentNode OuterC ON OuterC.ContentNodeId = C.ParentContentNodeId
	)
	INSERT INTO @tContentNode (tContentNodeId, tParentContentNodeId)
	SELECT ContentNodeId, ParentContentNodeId FROM ContentNode
	UNION ALL	
	SELECT ContentNodeId, ParentContentNodeId
	FROM  sitestructure.tContentNode
	WHERE ContentNodeId = @ContentNodeId
	
	IF(EXISTS(
		SELECT 1		
		FROM @tContentNode
		WHERE tContentNodeId = @StartContentPageIdOfCurrentRegistry
		))
	BEGIN
		RETURN @StartContentPageIdOfCurrentRegistry
	END	
	RETURN	
		
	
END

GO
