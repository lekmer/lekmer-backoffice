SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure test.pMediaFolderGetIdByTitle
	@Title nvarchar(150)
as
begin
	declare @id int
	select
		@id = MediaFolderId
	from
		media.tMediaFolder
	where
		Title = @Title
	return @id
end
GO
