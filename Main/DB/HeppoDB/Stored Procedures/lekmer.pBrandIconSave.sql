SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandIconSave]
	@BrandId	INT,
	@IconId		INT
AS 
BEGIN 
	INSERT [lekmer].[tBrandIcon]
	VALUES (@BrandId, @IconId)
END
GO
