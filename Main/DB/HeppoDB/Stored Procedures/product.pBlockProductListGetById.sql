SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 02.12.2008	Time: 13:00
Description:	Created

*****************  Version 2  *****************
User: Yura P.	Date: 08.12.2008	Time: 11:00
Description:	Edited (add join on Block)
*/

CREATE PROCEDURE [product].[pBlockProductListGetById]
	@LanguageId INT,
	@BlockId	int
as
begin
	select 
		ba.*,
		b.*
	from 
		[product].[vCustomBlockProductList] as ba
		inner join [sitestructure].[vCustomBlock] as b on ba.[BlockProductList.BlockId] = b.[Block.BlockId]
	where
		ba.[BlockProductList.BlockId] = @BlockId 
		AND b.[Block.LanguageId] = @LanguageId
end

GO
