
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		caic.CategoryId,
		caic.IncludeSubCategories
	FROM 
		[campaignlek].[tCampaignActionIncludeCategory] caic
		INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [caic].[CategoryId]
	WHERE 
		caic.ConfigId = @ConfigId
END
GO
