SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pCampaignFlagSave]
	@CampaignId	INT,
	@FlagId		INT
AS
BEGIN
	
	IF @FlagId IS NULL
	BEGIN
		DELETE [lekmer].[tCampaignFlag]
		WHERE [CampaignId] = @CampaignId
		RETURN
	END
	
	UPDATE [lekmer].[tCampaignFlag]
	SET [FlagId] = @FlagId
	WHERE [CampaignId] = @CampaignId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tCampaignFlag]
		( 
			[CampaignId],
			[FlagId]
		)
		VALUES
		(
			@CampaignId,
			@FlagId
		)					
	END 
END

GO
