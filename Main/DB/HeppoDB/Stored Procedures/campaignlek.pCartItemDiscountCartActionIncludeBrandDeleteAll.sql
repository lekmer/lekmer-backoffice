SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemDiscountCartActionIncludeBrand]
	WHERE
		CartActionId = @CartActionId
END

GO
