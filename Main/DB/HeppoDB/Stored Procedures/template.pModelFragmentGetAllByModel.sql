SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentGetAllByModel]
    @ModelId INT
AS 
BEGIN
    SET NOCOUNT ON

    SELECT  *
    FROM    [template].[vCustomModelFragment]
    WHERE   [ModelFragment.ModelId] = @ModelId
END

GO
