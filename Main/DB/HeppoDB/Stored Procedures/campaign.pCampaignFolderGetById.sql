SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignFolderGetById]
@FolderId	int
AS
BEGIN
	SELECT * 
	FROM campaign.vCustomCampaignFolder
	WHERE [CampaignFolder.Id] = @FolderId
END

GO
