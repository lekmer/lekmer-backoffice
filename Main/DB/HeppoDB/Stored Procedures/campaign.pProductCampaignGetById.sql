SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductCampaignGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomProductCampaign
	WHERE
		[Campaign.Id] = @Id
end

GO
