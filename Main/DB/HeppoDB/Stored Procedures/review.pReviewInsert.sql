
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pReviewInsert]
	@RatingReviewFeedbackId INT,
	@AuthorName NVARCHAR(50),
	@Title NVARCHAR(50),
	@Message NVARCHAR(3000),
	@Email VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tReview] (
		[RatingReviewFeedbackId],
		[AuthorName],
		[Title],
		[Message],
		[Email]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@AuthorName,
		@Title,
		@Message,
		@Email
	)

	RETURN SCOPE_IDENTITY()
END
GO
