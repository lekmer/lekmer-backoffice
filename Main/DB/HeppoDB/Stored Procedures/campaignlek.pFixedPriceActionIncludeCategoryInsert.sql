SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionIncludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END

GO
