SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockBestRatedProductListBrand] b
	WHERE
		b.[BlockBestRatedProductListBrand.BlockId] = @BlockId
		AND b.[ChannelId] = @ChannelId
END
GO
