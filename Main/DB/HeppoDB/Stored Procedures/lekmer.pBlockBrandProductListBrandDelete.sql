SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [lekmer].[pBlockBrandProductListBrandDelete]
@BlockId				int,
@BrandId				int
as
begin
	delete from 
		[lekmer].[tBlockBrandProductListBrand]
	where
		[BrandId] = @BrandId
		and [BlockId] = @BlockId
end




GO
