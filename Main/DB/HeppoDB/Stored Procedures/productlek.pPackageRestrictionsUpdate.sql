SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageRestrictionsUpdate]
	@ProductId			INT,
	@BrandId			INT,
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	-- Get Product Ids that need add to package restirctions.
	DECLARE @ProductRegIds [lekmer].[ProductRegistryProducts]

	IF @ProductId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				WHERE [pp].[ProductId] = @ProductId) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @BrandId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [pp].[ProductId]
				WHERE [lp].[BrandId] = @BrandId) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				INNER JOIN [product].[tProduct] lp ON [lp].[ProductId] = [pp].[ProductId]
				WHERE [lp].[CategoryId] IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	-- Add package restrictions
	INSERT INTO [lekmer].[tProductRegistryRestrictionProduct] (
		[ProductRegistryId] ,
		[ProductId] ,
		[RestrictionReason] ,
		[UserId] ,
		[CreatedDate]
	)
	SELECT
		[ProductRegistryId],
		[ProductId],
		'Package has restricted include product/product category/product brand',
		NULL,
		GETDATE()
	FROM
		@ProductRegIds
	
	
	DELETE prp FROM product.tProductRegistryProduct prp
	INNER JOIN @ProductRegIds prIds ON prIds.ProductId = [prp].[ProductId] AND [prIds].[ProductRegistryId] = [prp].[ProductRegistryId]
END
GO
