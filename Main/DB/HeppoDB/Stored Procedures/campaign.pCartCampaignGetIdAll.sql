
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartCampaignGetIdAll]
	@ChannelId INT
AS
BEGIN
	SELECT
		[c].[Campaign.Id]
	FROM
		[campaign].[vCustomCartCampaign] c
		INNER JOIN [campaign].[tCampaignModuleChannel] m ON [m].[CampaignRegistryId] = [c].[Campaign.CampaignRegistryId]
	WHERE
		[m].[ChannelId] = @ChannelId
		AND [c].[Campaign.CampaignStatusId] = 0
	ORDER BY
		[c].[Campaign.LevelPriority],
		[c].[Campaign.Priority]
END
GO
