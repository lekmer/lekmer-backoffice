
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select
			o.OrderId,
			o.FreightCost, 
			o.ChannelId as Channel,   
			z.OrderPaymentId, 
			v.ErpId as HyPaymentMethodId,
			z.Price,
			--u.VoucherDiscount as VoucherAmount
			op.ItemsActualPriceIncludingVat - z.Price as VoucherAmount,
			lci.IsCompany -- NEW
		from
			[order].tPaymentType v
			inner join [order].tOrderPayment z
				on z.PaymentTypeId = v.PaymentTypeId
			inner join [order].tOrder o
				on z.OrderId = o.OrderId
			inner join customerlek.tCustomerInformation lci
				on lci.CustomerId = o.CustomerId
			left join lekmer.tlekmerOrder u
				on o.OrderId = u.OrderId	
			cross apply (select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat' from [order].tOrderItem oi where oi.OrderId = o.OrderId) as op
		where
			o.OrderStatusId = 2 and
			o.OrderId = @OrderId
				
		
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId, --p.ErpId
			coalesce(ps.NumberInStock, pp.NumberInStock) as QuantityLeft				
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			left join product.tProduct pp
				on pp.ProductId = p.ProductId
			left join lekmer.tProductSize ps
				on ps.ProductId = p.ProductId
				and ps.SizeId = ois.SizeId	
		where
			o.OrderId = @OrderId 
			
		-- Customer
		select 		
			c.CustomerId, 
			c.FirstName, 
			c.LastName,
			c.CivicNumber, 
			c.PhoneNumber,
			c.CellPhoneNumber as MobilePhoneNumber, 
			o.Email,			
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.Addressee, 		         
			a.StreetAddress as Adress, 
			a.StreetAddress2 as CoAdress, 
			a.City, 
			a.PostalCode, 
			l.ISO as CountryIso,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseAdditionalInformation,
			la.Reference as Reference
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.Addressee, 		         
			a.StreetAddress as Adress, 
			a.StreetAddress2 as CoAdress, 
			a.City, 
			a.PostalCode, 
			l.ISO as CountryIso,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseAdditionalInformation,
			la.Reference as Reference -- NEW
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		
		-- AlternativeAddress
		select
			a.Addressee, 		         
			a.StreetAddress as Adress, 
			a.StreetAddress2 as CoAdress, 
			a.City, 
			a.PostalCode, 
			l.ISO as CountryIso,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseAdditionalInformation,
			la.Reference as Reference -- NEW
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- FreightFee
		select o.FreightCost as Amount
		from [order].tOrder o 			
		where o.OrderId = @OrderId
		
		-- InvoiceFee
		select 
			y.PaymentCost as Amount
		from 
			[order].tOrder o 
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tOrderPayment t 
				on o.OrderId = t.OrderId
			inner join lekmer.tPaymentTypePrice y
				on t.PaymentTypeId = y.PaymentTypeId
				and c.CountryId = y.CountryId
				and c.CurrencyId = y.CurrencyId
		where o.OrderId = @OrderId
		
		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
		where
			o.OrderId = @OrderId
			
		
		-- PaymentMethod info
		select 				
			v.ErpId as HyPaymentMethodId,
			z.ReferenceId, 
			z.Price as AuthorizedAmount
		from
			[order].tPaymentType v
			inner join [order].tOrderPayment z
				on z.PaymentTypeId = v.PaymentTypeId					
		where
			z.OrderId = @OrderId
		
	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
