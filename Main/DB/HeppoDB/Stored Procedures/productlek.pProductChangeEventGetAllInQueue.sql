SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductChangeEventGetAllInQueue]
	@NumberOfItems INT,
	@EventStatusId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT TOP (@NumberOfItems)
		[pce].*
	FROM
		[productlek].[vProductChangeEvent] pce
	WHERE
		[pce].[ProductChangeEvent.EventStatusId] = @EventStatusId
END
GO
