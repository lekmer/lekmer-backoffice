SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tFixedPriceActionExcludeBrand]
	WHERE 
		ProductActionId = @ProductActionId
END

GO
