SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create procedure [lekmer].[pBlockBrandProductListDelete]
@BlockId		int
as
BEGIN
	delete
		[lekmer].[tBlockBrandProductList]
	where
		[BlockId] = @BlockId

end

GO
