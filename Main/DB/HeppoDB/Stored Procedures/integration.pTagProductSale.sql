
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pTagProductSale]

AS
BEGIN
	DECLARE @SalesTagId INT
		SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')

	DECLARE @ActiveCampaigns TABLE 
	(
		CampaignId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)
	
	DECLARE @ProductWithDiscountPrices TABLE
	(
		ChannelId INT,
		ProductId INT,
		SitePrice DECIMAL (16,2),
		PRIMARY KEY(ChannelId, ProductId)
	)	
	INSERT INTO @ProductWithDiscountPrices
	EXEC [lekmer].[pAvailGetProductDiscountPriceOnSite]
	
	
	INSERT INTO @ActiveCampaigns (CampaignId)
	SELECT
		c.CampaignId
	FROM 
		campaign.tCampaign c
	WHERE
		c.EndDate > GETDATE()
		AND c.StartDate < GETDATE()
		AND CampaignStatusId = 0

	DECLARE @ProductsToBeTaged TABLE 
	(
		ProductId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)
	
	DECLARE @IncludeAllProducts BIT
	
	/*--------------------------------------------*/
	/*CartContainsCondition*/

	DECLARE @CartContainsConditionId INT
	
	DECLARE CartContainsCondition_cursor CURSOR FOR 
	SELECT
		con.ConditionId,
		ccc.IncludeAllProducts
	FROM
		addon.tCartContainsCondition ccc
		INNER JOIN campaign.tCondition con ON con.ConditionId = ccc.ConditionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = con.CampaignId

	OPEN CartContainsCondition_cursor

	FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartContainsConditionAffectedProducts (@CartContainsConditionId)
	
		FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts
	END

	CLOSE CartContainsCondition_cursor;
	DEALLOCATE CartContainsCondition_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*CartItemPriceAction*/

	DECLARE @CartItemPriceActionId INT
	
	DECLARE CartItemPriceAction_cursor CURSOR FOR 
	SELECT
		ca.CartActionId,
		cipa.IncludeAllProducts
	FROM
		addon.tCartItemPriceAction cipa
		INNER JOIN campaign.tCartAction ca ON ca.CartActionId = cipa.CartActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = ca.CampaignId

	OPEN CartItemPriceAction_cursor

	FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartItemPriceActionAffectedProducts (@CartItemPriceActionId)
	
		FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts
	END

	CLOSE CartItemPriceAction_cursor;
	DEALLOCATE CartItemPriceAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*PercentagePriceDiscountAction*/

	DECLARE @PercentagePriceDiscountActionId INT
	
	DECLARE PercentagePriceDiscountAction_cursor CURSOR FOR 
	SELECT
		pa.ProductActionId,
		cac.IncludeAllProducts
	FROM
		campaign.tPercentagePriceDiscountAction ppda
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON [cac].[CampaignActionConfiguratorId] = [ppda].[ConfigId]
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ppda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	OPEN PercentagePriceDiscountAction_cursor

	FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetPercentagePriceDiscountActionAffectedProducts (@PercentagePriceDiscountActionId)
	
		FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts
	END

	CLOSE PercentagePriceDiscountAction_cursor;
	DEALLOCATE PercentagePriceDiscountAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*ProductDiscountAction*/
	
	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		ProductId
	FROM 
		lekmer.tProductDiscountActionItem pdai
		INNER JOIN lekmer.tProductDiscountAction pda ON pda.ProductActionId = pdai.ProductActionId
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = pda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	/*--------------------------------------------*/

UpdateTags:
	
	BEGIN TRY
	BEGIN TRANSACTION

		DELETE FROM lekmer.tProductTag WHERE TagId = @SalesTagId

		INSERT INTO lekmer.tProductTag(ProductId, TagId)
		SELECT
			p.ProductId,
			@SalesTagId
		FROM
			@ProductsToBeTaged p												
			left join @ProductWithDiscountPrices d
				on p.ProductId = d.ProductId
				and d.ChannelId = 1			
			inner join product.tPriceListItem i
				on d.ProductId = i.ProductId
				and i.PriceListId = 1
		WHERE
			d.SitePrice < i.PriceIncludingVat -- Only products with lower price than original price get tagged, lekmer uses campigns diffrently...
		OR
			d.ProductId is null
		
		
	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

	RETURN

	/*--------------------------------------------*/

AllProductsAffected:

	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT ProductId FROM product.tProduct

	GOTO UpdateTags

END
GO
