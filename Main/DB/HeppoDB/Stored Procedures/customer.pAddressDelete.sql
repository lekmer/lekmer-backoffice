SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [customer].[pAddressDelete]
@AddressId	int
as	
begin
	set nocount on
	delete [customer].[tAddress]
	where AddressId = @AddressId
end
GO
