SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****************  Version 2  *****************
User: Yura P.		Date: 04.09.2008		Time: 16:35
Description:
	Add RelationListTypeName
*/

CREATE PROCEDURE [product].[pRelationListGetById]
	@RelationListId int
as
begin
	Select
		*
	From
		[product].[vCustomRelationList] v
    where
		v.[RelationList.Id] = @RelationListId
end

GO
