SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCartValueCondition]
as
	select
		*
	from
		[campaign].[vCartValueCondition]
GO
