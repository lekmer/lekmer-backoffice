
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view [product].[vCustomCategorySecure]
as
	select
		c.*,
		l.AllowMultipleSizesPurchase as [LekmerCategory.AllowMultipleSizesPurchase]
	from
		[product].[vCategorySecure] c
		left join [lekmer].[tLekmerCategory] l on c.[Category.Id] = l.CategoryId
		
GO
