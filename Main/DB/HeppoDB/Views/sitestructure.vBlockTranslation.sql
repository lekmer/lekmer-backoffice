SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vBlockTranslation]
AS
SELECT
	bt.BlockId AS 'BlockTranslation.BlockId',
	bt.LanguageId AS 'BlockTranslation.LanguageId',
	bt.Title AS 'BlockTranslation.Title'
FROM         	
      [sitestructure].[tBlockTranslation] AS bt
GO
