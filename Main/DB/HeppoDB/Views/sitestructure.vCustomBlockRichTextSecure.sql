SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlockRichTextSecure]
as
	select
		*
	from
		[sitestructure].[vBlockRichTextSecure]
GO
