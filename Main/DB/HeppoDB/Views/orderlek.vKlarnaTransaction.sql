
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [orderlek].[vKlarnaTransaction]
AS
SELECT
	[kt].[KlarnaTransactionId],
	[kt].[KlarnaShopId],
	[kt].[KlarnaMode],
	
	CASE [kt].[KlarnaMode]
		WHEN 0 THEN 'NO_FLAG'
		WHEN 2 THEN 'KRED_TEST_MODE'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaMode])
	END AS 'KlarnaMode.Name',
	
	[kt].[KlarnaTransactionTypeId],
	
	CASE [kt].[KlarnaTransactionTypeId]
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'ReserveAmount'
		WHEN 3 THEN 'ReserveAmountCompany'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaTransactionTypeId])
	END AS 'KlarnaTransactionType.Name',
	
	[kt].[Created],
	[kt].[CivicNumber],
	[kt].[OrderId],
	[kt].[Amount],
	[kt].[CurrencyId],
	
	CASE [kt].[CurrencyId]
		WHEN 0 THEN 'SEK'
		WHEN 1 THEN 'NOK'
		WHEN 2 THEN 'EUR'
		WHEN 3 THEN 'DKK'
		ELSE CONVERT(VARCHAR(50), [kt].[CurrencyId])
	END AS 'Currency.Code',
	
	[kt].[PClassId],
	[kt].[YearlySalary],
	[kt].[Duration],
	[kt].[ReservationNumber],
	[kt].[ResponseCodeId],
	
	CASE [kt].[ResponseCodeId]
		WHEN 1 THEN 'OK'
		WHEN 2 THEN 'NoRisk'
		WHEN 3 THEN 'TimeoutResponse'
		WHEN 4 THEN 'SpecifiedError'
		WHEN 5 THEN 'UnspecifiedError'
		ELSE CONVERT(VARCHAR(50), [kt].[ResponseCodeId])
	END AS 'ResponseCode.Name',
	
	[kt].[KlarnaFaultCode],
	[kt].[KlarnaFaultReason]
FROM
	[orderlek].[tKlarnaTransaction] kt

GO
