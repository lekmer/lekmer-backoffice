
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vBrand]
AS
	SELECT     
		B.BrandId AS 'Brand.BrandId',
		COALESCE (bt.[BrandTranslation.Title], B.Title) AS 'Brand.Title',
		COALESCE (bt.[BrandTranslation.Description], B.[Description]) AS 'Brand.Description',
		B.ExternalUrl AS 'Brand.ExternalUrl',
		B.MediaId AS 'Brand.MediaId',
		B.ErpId AS 'Brand.ErpId',
		Ch.ChannelId,
		I.*,
		Bssr.ContentNodeId AS 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId
GO
