SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentArea]
as
	select
		*
	from
		[sitestructure].[vContentArea]
GO
