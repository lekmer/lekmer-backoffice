SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vProductAttribute]
AS
 select
		[ProductId] [ProductAttribute.ProductId],
		[LanguageId] [ProductAttribute.LanguageId],
		[Title] [ProductAttribute.Title],
		[Value] [ProductAttribute.Value],
		[Ordinal] [ProductAttribute.Ordinal]
from
		product.tProductAttribute
GO
