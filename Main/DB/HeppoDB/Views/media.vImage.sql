SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- View

CREATE VIEW [media].[vImage]
AS
SELECT     
	i.Width AS 'Image.Width',
	i.Height AS 'Image.Height', 
	COALESCE (it.AlternativeText, i.AlternativeText) AS 'Image.AlternativeText', 
	i.MediaId AS 'Image.MediaId', 
	m.*, 
	mf.Extension AS 'MediaFormat.Extension', 
	mf.CommonName AS 'MediaFormat.CommonName',
	l.LanguageId AS 'Image.LanguageId'
FROM
	media.tImage i CROSS JOIN
	core.tLanguage l INNER JOIN
	media.vCustomMedia m ON i.MediaId = m.[Media.Id] INNER JOIN
	media.tMediaFormat mf ON m.[Media.FormatId] = mf.MediaFormatId  LEFT JOIN 
	media.tImageTranslation it ON it.MediaId = i.MediaId AND it.LanguageId = l.LanguageId

GO
