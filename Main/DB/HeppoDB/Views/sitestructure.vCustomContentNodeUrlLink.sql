SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeUrlLink]
as
	select
		*
	from
		[sitestructure].[vContentNodeUrlLink]
GO
