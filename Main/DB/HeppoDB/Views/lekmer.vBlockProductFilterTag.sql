SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [lekmer].[vBlockProductFilterTag]
as
	select
		BlockId 'BlockProductFilterTag.BlockId',
		TagId 'BlockProductFilterTag.TagId'
	from
		lekmer.tBlockProductFilterTag

GO
