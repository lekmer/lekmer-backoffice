
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRating]
AS
	SELECT
		r.[RatingId]		'Rating.RatingId',
		r.[RatingFolderId]	'Rating.RatingFolderId',
		r.[RatingRegistryId]'Rating.RatingRegistryId',
		r.[CommonName]		'Rating.CommonName',
		COALESCE(rt.[Title], r.[Title])	'Rating.Title',
		COALESCE(rt.[Description], r.[Description])	'Rating.Description',
		r.[CommonForVariants]	'Rating.CommonForVariants',
		c.[ChannelId]		'Rating.Channel.Id'
	FROM 
		[review].[tRating] r
		INNER JOIN [review].[tRatingModuleChannel] rmc ON rmc.[RatingRegistryId] = r.[RatingRegistryId]
		INNER JOIN [core].[tChannel] c ON c.[ChannelId] = rmc.[ChannelId]
		LEFT OUTER JOIN [review].[tRatingTranslation] rt ON rt.[RatingId] = r.[RatingId] AND rt.[LanguageId] = c.[LanguageId]
GO
