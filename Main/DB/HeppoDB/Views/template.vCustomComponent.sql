SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomComponent]
as
	select
		*
	from
		[template].[vComponent]
GO
