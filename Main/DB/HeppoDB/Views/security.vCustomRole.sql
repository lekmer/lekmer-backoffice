SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [security].[vCustomRole]
as
	select
		*
	from
		[security].[vRole]
GO
