SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vBlockRatingItem]
AS
	SELECT
		[BlockId] AS 'BlockRatingItem.BlockId',
		[RatingItemId] AS 'BlockRatingItem.RatingItemId'
	FROM
		[review].[tBlockRatingItem]

GO
