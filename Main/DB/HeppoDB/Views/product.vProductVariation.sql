SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vProductVariation]
AS
SELECT 
		PV.VariationGroupId 'ProductVariation.VariationGroupId',
		PV.VariationId 'ProductVariation.VariationId',
		PV.ProductId 'ProductVariation.ProductId',
		PV.VariationTypeId 'ProductVariation.VariationTypeId',
		VT.Title AS 'ProductVariation.VariationTypeName',
		V.Value	AS 'ProductVariation.VariationValue'
	FROM 
		[product].[tProductVariation] PV 
		JOIN [product].[tVariationType] VT ON PV.VariationTypeId = VT.VariationTypeId
		JOIN [product].tVariation V ON V.VariationId = PV.VariationId 
GO
