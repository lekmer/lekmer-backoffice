SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vProductAutoFreeCartAction]
AS
	SELECT
		pafca.[CartActionId] AS 'ProductAutoFreeCartAction.CartActionId',
		pafca.[ProductId] AS 'ProductAutoFreeCartAction.ProductId',
		pafca.[Quantity] AS 'ProductAutoFreeCartAction.Quantity',
		ca.*
	FROM
		[campaignlek].[tProductAutoFreeCartAction] pafca
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = pafca.[CartActionId]


GO
