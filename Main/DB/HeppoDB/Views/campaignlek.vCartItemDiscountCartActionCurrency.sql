SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vCartItemDiscountCartActionCurrency]
AS
	SELECT
		pdcac.[CartActionId]	AS 'CartItemDiscountCartActionCurrency.CartActionId',
		pdcac.[CurrencyId]		AS 'CartItemDiscountCartActionCurrency.CurrencyId',
		pdcac.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tCartItemDiscountCartActionCurrency] pdcac
		INNER JOIN [core].[vCustomCurrency] c ON pdcac.[CurrencyId] = c.[Currency.Id]

GO
