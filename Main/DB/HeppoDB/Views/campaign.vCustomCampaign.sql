SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCampaign]
as
	select
		*
	from
		[campaign].[vCampaign]
GO
