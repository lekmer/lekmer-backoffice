SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view campaign.vCampaignStatus
as
	select
		CampaignStatusId as 'CampaignStatus.Id',
		Title as 'CampaignStatus.Title',
		CommonName as 'CampaignStatus.CommonName'
	from
		campaign.tCampaignStatus
GO
