SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockCategoryProductList]
as
	select
		*
	from
		[product].[vBlockCategoryProductList]
GO
