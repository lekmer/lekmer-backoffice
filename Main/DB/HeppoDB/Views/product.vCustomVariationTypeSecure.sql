SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomVariationTypeSecure]
as
	select
		*
	from
		[product].[vVariationTypeSecure]
GO
