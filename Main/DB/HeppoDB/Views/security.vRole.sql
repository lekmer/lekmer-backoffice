SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [security].[vRole]
AS
select
	[RoleId] 'Role.Id',
	[Title] 'Role.Title'
from
	[security].[tRole]
GO
