SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomCategoryView]
as
	select
		*
	from
		[product].[vCategoryView]
GO
