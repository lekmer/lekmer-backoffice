SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [lekmer].[vCustomProductUrlHistory]
AS
	SELECT
		*
	FROM
		[lekmer].[vProductUrlHistory]
GO
