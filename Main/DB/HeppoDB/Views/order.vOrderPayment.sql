SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vOrderPayment]
AS 
SELECT
	   [OrderPaymentId] AS 'OrderPayment.OrderPaymentId'
      ,[OrderId]  AS 'OrderPayment.OrderId'
      ,[PaymentTypeId] AS 'OrderPayment.PaymentTypeId'
      ,[Price] AS 'OrderPayment.Price'
      ,[Vat] AS 'OrderPayment.Vat'
      ,[ReferenceId] AS 'OrderPayment.ReferenceId'
  FROM 
	  [order].[tOrderPayment]


GO
