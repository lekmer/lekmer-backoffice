SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vCartItemOption]
AS
SELECT 
		cio.CartItemOptionId AS 'CartItemOption.CartItemOptionId',
		cio.CartId AS 'CartItemOption.CartId',
		cio.SizeId AS 'CartItemOption.SizeId',
		cio.ErpId AS 'CartItemOption.ErpId',
		cio.Quantity AS 'CartItemOption.Quantity', 
		cio.CreatedDate AS 'CartItemOption.CreatedDate',
		p.*		
FROM
	[campaignlek].[tCartItemOption] cio
	INNER JOIN [product].[vCustomProduct] p on p.[Product.Id] = cio.[ProductId]


GO
