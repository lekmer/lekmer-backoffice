SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [campaign].[vCampaignFolder]
as
	select
		FolderId as 'CampaignFolder.Id',
		ParentFolderId as 'CampaignFolder.ParentId',
		Title as 'CampaignFolder.Title',
		TypeId AS 'CampaignFolder.TypeId'
	from
		campaign.tCampaignFolder
GO
