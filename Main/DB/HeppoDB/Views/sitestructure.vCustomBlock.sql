
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [sitestructure].[vCustomBlock]
as
 select
  b.*,
  l.StartDate AS 'Block.StartDate',
  l.EndDate AS 'Block.EndDate',
  l.StartDailyIntervalMinutes AS 'Block.StartDailyIntervalMinutes',
  l.EndDailyIntervalMinutes AS 'Block.EndDailyIntervalMinutes'
 from
  [sitestructure].[vBlock] b left join [lekmer].[tLekmerBlock] l 
   on b.[Block.BlockId] = l.[BlockId]

GO
