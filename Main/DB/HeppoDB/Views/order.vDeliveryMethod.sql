SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vDeliveryMethod]
AS
SELECT 
		dm.DeliveryMethodId AS 'DeliveryMethod.DeliveryMethodId',
		dm.CommonName AS 'DeliveryMethod.CommonName',
		dm.Title AS 'DeliveryMethod.Title',
		dm.ErpId AS 'DeliveryMethod.ErpId',
		dm.TrackingUrl AS 'DeliveryMethod.TrackingUrl',
		dmp.*,
		ch.ChannelId AS 'DeliveryMethod.ChannelId'
FROM 
	[order].[tDeliveryMethod] dm
	INNER JOIN [order].[tChannelDeliveryMethod] cdm
		ON cdm.DeliveryMethodId = dm.DeliveryMethodId
	INNER JOIN [core].[tChannel] ch
		ON ch.ChannelId = cdm.ChannelId
	INNER JOIN [order].[vCustomDeliveryMethodPrice] dmp
		ON dmp.[DeliveryMethodPrice.DeliveryMethodId] = dm.DeliveryMethodId
		AND ch.CountryId = dmp.[DeliveryMethodPrice.CountryId]
		AND ch.CurrencyId = dmp.[DeliveryMethodPrice.CurrencyId]




GO
