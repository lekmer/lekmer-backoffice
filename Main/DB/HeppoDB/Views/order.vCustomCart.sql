SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomCart]
as
	select
		*
	from
		[order].[vCart]
GO
