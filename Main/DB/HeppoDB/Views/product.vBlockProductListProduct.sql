SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vBlockProductListProduct]
as
SELECT 
	[BlockId] 'BlockProductListProduct.BlockId',
	[ProductId] 'BlockProductListProduct.ProductId',
	[Ordinal] 'BlockProductListProduct.Ordinal'
FROM 
	[product].[tBlockProductListProduct]
GO
