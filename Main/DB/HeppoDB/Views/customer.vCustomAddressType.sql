SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomAddressType]
as
	select
		*
	from
		[customer].[vAddressType]
GO
