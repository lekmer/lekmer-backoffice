SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCartActionType]
as
	select
		*
	from
		[campaign].[vCartActionType]
GO
