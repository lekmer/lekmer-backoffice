SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [export].[vProductPrice]
AS
	SELECT
		pp.[ChannelId] 'ProductPrice.ChannelId',
		pp.[ProductId] 'ProductPrice.ProductId',
		pp.[PriceIncludingVat] 'ProductPrice.PriceIncludingVat',
		pp.[PriceExcludingVat] 'ProductPrice.PriceExcludingVat',
		pp.[DiscountPriceIncludingVat] 'ProductPrice.DiscountPriceIncludingVat',
		pp.[DiscountPriceExcludingVat] 'ProductPrice.DiscountPriceExcludingVat'
	FROM 
		export.tProductPrice  AS pp
GO
