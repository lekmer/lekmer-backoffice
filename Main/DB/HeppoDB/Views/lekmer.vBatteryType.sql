SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBatteryType]
AS
SELECT     
      BatteryTypeId AS 'BatteryType.Id', 
      Title AS 'BatteryType.Title'
FROM
      [lekmer].[tBatteryType]
GO
