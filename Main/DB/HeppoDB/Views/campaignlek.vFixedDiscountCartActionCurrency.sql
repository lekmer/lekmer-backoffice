SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vFixedDiscountCartActionCurrency]
AS
	SELECT
		fdcac.[CartActionId]	AS 'FixedDiscountCartActionCurrency.CartActionId',
		fdcac.[CurrencyId]		AS 'FixedDiscountCartActionCurrency.CurrencyId',
		fdcac.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tFixedDiscountCartActionCurrency] fdcac
		INNER JOIN [core].[vCustomCurrency] c ON fdcac.[CurrencyId] = c.[Currency.Id]
GO
