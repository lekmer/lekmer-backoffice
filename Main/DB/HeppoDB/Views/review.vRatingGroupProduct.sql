SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingGroupProduct]
AS
	SELECT
		rgp.[RatingGroupId]	'RatingGroupProduct.RatingGroupId',
		rgp.[ProductId]		'RatingGroupProduct.ProductId'
	FROM 
		[review].[tRatingGroupProduct] rgp
GO
