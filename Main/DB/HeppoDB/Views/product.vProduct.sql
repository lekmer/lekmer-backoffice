SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- View

CREATE VIEW [product].[vProduct]
AS
SELECT     
	P.ProductId 'Product.Id',  
	P.ItemsInPackage 'Product.ItemsInPackage', 
	P.ErpId 'Product.ErpId', 
	P.EanCode 'Product.EanCode', 
	P.NumberInStock 'Product.NumberInStock', 
    P.CategoryId 'Product.CategoryId', 
    COALESCE(PT.WebShopTitle, P.WebShopTitle) 'Product.WebShopTitle',
	COALESCE(PT.Title, P.Title) 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId', 
    COALESCE(PT.ShortDescription, P.ShortDescription) 'Product.ShortDescription',
    Ch.ChannelId 'Product.ChannelId',
    Ch.CurrencyId 'Product.CurrencyId',
    PMC.PriceListRegistryId 'Product.PriceListRegistryId',
    p.MediaId 'Product.MediaId'
FROM
	product.tProduct AS P

	/* filetrring */
	INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
	INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
	INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId

	/* extra info */
	/*LEFT JOIN media.vCustomImage AS I
		ON I.[Image.MediaId] = P.MediaId
		AND I.[Image.LanguageId] = Ch.LanguageId*/

	LEFT JOIN product.tProductTranslation AS PT
		ON P.ProductId = PT.ProductId
		AND Ch.LanguageId = PT.LanguageId
WHERE
	P.IsDeleted = 0
	AND P.ProductStatusId = 0





GO
