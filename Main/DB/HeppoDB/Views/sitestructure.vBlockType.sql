SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vBlockType]
AS
SELECT   
       [BlockTypeId] AS 'BlockType.BlockTypeId'
      ,[Title] AS 'BlockType.Title'
	  ,[CommonName] AS 'BlockType.CommonName'
	  ,[Ordinal] AS 'BlockType.Ordinal'
	  ,[AvailableForAllPageTypes] AS 'BlockType.AvailableForAllPageTypes'
FROM         	
      [sitestructure].[tBlockType]
GO
