SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomModel]
as
	select
		*
	from
		[template].[vModel]
GO
