SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vReviewStatus]
AS
 SELECT 
         rs.StatusId 'ReviewStatus.Id', 
         rs.CommonName 'ReviewStatus.CommonName',
         rs.Title 'ReviewStatus.Title'
 FROM 
         addon.tReviewStatus rs

GO
