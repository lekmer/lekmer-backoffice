
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [orderlek].[vDeliveryMethod]
AS
	SELECT
		[DeliveryMethodId] AS 'DeliveryMethod.DeliveryMethodId',
		[Priority] AS 'DeliveryMethod.Priority',
		[IsCompany] AS 'DeliveryMethod.IsCompany'
	FROM
		[orderlek].[tDeliveryMethod]


GO
