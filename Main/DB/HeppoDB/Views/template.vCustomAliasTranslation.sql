SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomAliasTranslation]
as
	select
		*
	from
		[template].[vAliasTranslation]
GO
