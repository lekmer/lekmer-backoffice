SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingReviewUser]
AS
	SELECT
		rru.[RatingReviewUserId]	'RatingReviewUser.RatingReviewUserId',
		rru.[CustomerId]			'RatingReviewUser.CustomerId',
		rru.[Token]					'RatingReviewUser.Token',
		rru.[CreatedDate]			'RatingReviewUser.CreatedDate'
	FROM 
		[review].[tRatingReviewUser] rru
GO
