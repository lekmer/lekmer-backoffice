SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomAliasType]
as
	select
		*
	from
		[template].[vAliasType]
GO
