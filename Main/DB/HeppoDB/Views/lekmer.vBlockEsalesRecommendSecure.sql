
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vBlockEsalesRecommendSecure]
AS
	SELECT
		b.*,
		ber.[ColumnCount] AS 'BlockEsalesRecommend.ColumnCount',
		ber.[RowCount] AS 'BlockEsalesRecommend.RowCount',
		ber.[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		ber.[PanelPath] AS 'BlockEsalesRecommend.PanelPath',
		ber.[FallbackPanelPath] AS 'BlockEsalesRecommend.FallbackPanelPath'
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = ber.[BlockId]
GO
