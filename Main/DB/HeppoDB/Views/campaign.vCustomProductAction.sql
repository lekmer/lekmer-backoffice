SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomProductAction]
as
	select
		*
	from
		[campaign].[vProductAction]
GO
