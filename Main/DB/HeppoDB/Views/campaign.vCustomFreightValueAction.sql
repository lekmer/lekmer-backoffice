SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomFreightValueAction]
as
	select
		*
	from
		[campaign].[vFreightValueAction]
GO
