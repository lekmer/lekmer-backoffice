SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomBlockSignIn]
as
	select
		*
	from
		[customer].[vBlockSignIn]
GO
