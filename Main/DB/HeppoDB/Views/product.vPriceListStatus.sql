SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vPriceListStatus]
AS
SELECT
		PriceListStatusId 'PriceListStatus.Id',
		Title 'PriceListStatus.Title',
		CommonName 'PriceListStatus.CommonName'
	from
		[product].[tPriceListStatus]
GO
