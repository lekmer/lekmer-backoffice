SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vFreeFreightFlagActionCurrency]
as
	select
		f.ProductActionId AS 'FreeFreightFlagActionCurrency.ActionId',
		f.CurrencyId AS 'FreeFreightFlagActionCurrency.CurrencyId',
		f.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		lekmer.tFreeFreightFlagActionCurrency f
		inner join core.vCustomCurrency c on f.CurrencyId = c.[Currency.Id]

GO
