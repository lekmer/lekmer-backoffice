SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [core].[vCustomLanguage]
as
	select
		*
	from
		[core].[vLanguage]
GO
