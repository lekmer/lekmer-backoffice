SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [core].[vCustomCulture]
as
	select
		*
	from
		[core].[vCulture]
GO
