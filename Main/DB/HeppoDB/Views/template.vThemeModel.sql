SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [template].[vThemeModel]
AS
SELECT     
	ThemeId AS [ThemeModel.ThemeId], 
	ModelId AS [ThemeModel.ModelId],
	TemplateId AS [ThemeModel.TemplateId]
FROM
	template.tThemeModel
GO
