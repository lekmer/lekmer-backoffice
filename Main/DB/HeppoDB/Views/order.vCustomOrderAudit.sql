SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomOrderAudit]
as
	select
		*
	from
		[order].[vOrderAudit]

GO
