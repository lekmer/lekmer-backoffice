SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomAliasFolder]
as
	select
		*
	from
		[template].[vAliasFolder]
GO
