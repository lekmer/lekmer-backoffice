SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vDeliveryMethodPrice]
AS 
SELECT
	 [CountryId] AS 'DeliveryMethodPrice.CountryId'
	,[CurrencyId] AS 'DeliveryMethodPrice.CurrencyId'
	,[DeliveryMethodId] AS 'DeliveryMethodPrice.DeliveryMethodId'
	,[FreightCost] AS 'DeliveryMethodPrice.FreightCost'
FROM
	[order].[tDeliveryMethodPrice]
GO
