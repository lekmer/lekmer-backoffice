SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentPageSeoSetting]
as
	select
		*
	from
		[sitestructure].[vContentPageSeoSetting]
GO
