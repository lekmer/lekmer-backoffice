SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vProductCampaign]
as
	select
		c.*
	from
		campaign.vCustomCampaign c
		inner join campaign.tProductCampaign pc on c.[Campaign.Id] = pc.CampaignId

GO
