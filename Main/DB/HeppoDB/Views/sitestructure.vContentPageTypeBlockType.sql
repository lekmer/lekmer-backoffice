SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vContentPageTypeBlockType]
AS
	select 
		ContentPageTypeId 'ContentPageTypeBlockType.ContentPageTypeId',
		BlockTypeId 'ContentPageTypeBlockType.BlockTypeId'
	from 
		[sitestructure].tContentPageTypeBlockType

GO
