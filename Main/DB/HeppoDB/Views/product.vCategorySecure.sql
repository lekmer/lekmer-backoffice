SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vCategorySecure]
AS
SELECT     
	[CategoryId] 'Category.Id',
	[ParentCategoryId] 'Category.ParentCategoryId',
	[Title] 'Category.Title',
	[ErpId] 'Category.ErpId'
FROM         
    [product].[tCategory]
GO
