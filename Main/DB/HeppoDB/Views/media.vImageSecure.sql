SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [media].[vImageSecure]
AS
SELECT
	i.Width AS 'Image.Width',
	i.Height AS 'Image.Height',
	i.AlternativeText AS 'Image.AlternativeText', 
	i.MediaId AS 'Image.MediaId',
	m.*,
	media.tMediaFormat.Extension AS 'MediaFormat.Extension', 
	media.tMediaFormat.CommonName AS 'MediaFormat.CommonName'
FROM
	media.tImage i INNER JOIN
	media.vCustomMedia m ON i.MediaId = m.[Media.Id] INNER JOIN
	media.tMediaFormat ON m.[Media.FormatId] = media.tMediaFormat.MediaFormatId

GO
