SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNode]
as
	select
		*
	from
		[sitestructure].[vContentNode]
GO
