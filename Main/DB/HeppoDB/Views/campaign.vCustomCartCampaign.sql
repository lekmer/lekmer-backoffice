SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCartCampaign]
as
	select
		*
	from
		[campaign].[vCartCampaign]
GO
