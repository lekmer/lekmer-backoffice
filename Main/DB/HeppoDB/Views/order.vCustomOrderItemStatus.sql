SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vCustomOrderItemStatus]
AS 
SELECT 
	*
FROM 	  
	[order].[vOrderItemStatus]


GO
