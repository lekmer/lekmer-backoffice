SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlockStatus]
as
	select
		*
	from
		[sitestructure].[vBlockStatus]
GO
