SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vBlockPackageProductList]
AS
	SELECT
		[BlockId] AS 'BlockPackageProductList.BlockId',
		[ColumnCount] AS 'BlockPackageProductList.ColumnCount',
		[RowCount] AS 'BlockPackageProductList.RowCount'
	FROM
		[productlek].[tBlockPackageProductList]
GO
