SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vCartItemsGroupValueConditionCurrency]
AS
	SELECT
		cigvcc.[ConditionId] AS 'CartItemsGroupValueConditionCurrency.ConditionId',
		cigvcc.[CurrencyId] AS 'CartItemsGroupValueConditionCurrency.CurrencyId',
		cigvcc.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tCartItemsGroupValueConditionCurrency] cigvcc
		INNER JOIN [core].[vCustomCurrency] c ON cigvcc.[CurrencyId] = c.[Currency.Id]


GO
