
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vProductPopularity]
AS
SELECT 
	pp.[ProductId] AS 'ProductPopularity.ProductId',
	pp.[ChannelId] AS 'ProductPopularity.ChannelId',
	pp.[Popularity] AS 'ProductPopularity.Popularity',
	pp.[SalesAmount] AS 'ProductPopularity.SalesAmount'
FROM 
	lekmer.[tProductPopularity] AS pp
GO
