SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentNodeShortcutLinkSecure]
AS
	select 
		cn.*,
		cnsl.[LinkContentNodeId] 'ContentNodeShortcutLink.LinkContentNodeId'
	from 
		[sitestructure].[tContentNodeShortcutLink] as cnsl
		inner join [sitestructure].[vCustomContentNodeSecure] as cn on cnsl.[ContentNodeId] = cn.[ContentNode.ContentNodeId]

GO
