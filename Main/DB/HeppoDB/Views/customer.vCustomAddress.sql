
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [customer].[vCustomAddress]
AS
	SELECT
		a.*,
		la.[Address.HouseNumber],
		la.[Address.HouseExtension],
		la.[Address.Reference]
	FROM
		[customer].[vAddress] a
		LEFT OUTER JOIN [customerlek].[vAddress] la 
			ON la.[Address.AddressId] = a.[Address.AddressId]
			AND la.[Address.CustomerId] = a.[Address.CustomerId]

GO
