SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [order].[vCustomOrderItem]
as
	select
		i.*,
		s.OrderItemId as 'OrderItemSize.OrderItemId',
		s.SizeId as 'OrderItemSize.SizeId',
		s.ErpId as 'OrderItemSize.ErpId',
		ps.*,
		ci.*,
		lp.BrandId
	from
		[order].[vOrderItem] i
		left join lekmer.tOrderItemSize s on i.[OrderItem.OrderItemId] = s.OrderItemId
		LEFT JOIN lekmer.vProductSize ps ON i.[OrderItem.ProductId] = ps.[ProductSize.ProductId] AND s.SizeId = ps.[ProductSize.SizeId]
		LEFT JOIN product.tProduct p ON i.[OrderItem.ProductId] = p.ProductId
		LEFT JOIN media.vCustomImageSecure ci ON ci.[Image.MediaId] = p.MediaId
		LEFT JOIN lekmer.tLekmerProduct lp ON i.[OrderItem.ProductId] = lp.ProductId

GO
