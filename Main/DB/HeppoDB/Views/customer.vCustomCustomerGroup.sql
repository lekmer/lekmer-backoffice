SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomerGroup]
as
	select
		*
	from
		[customer].[vCustomerGroup]
GO
