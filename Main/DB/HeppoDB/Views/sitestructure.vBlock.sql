SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vBlock]
AS
SELECT   
	   t.[BlockId] AS 'Block.BlockId'
      ,t.[ContentNodeId] AS 'Block.ContentNodeId'
      ,t.[BlockStatusId] AS 'Block.BlockStatusId'
      ,t.[ContentAreaId] AS 'Block.ContentAreaId'
      ,t.[Ordinal] AS 'Block.Ordinal'
      ,t.[AccessId] AS 'Block.AccessId'
      ,COALESCE (transl.Title, t.Title) AS 'Block.Title'
      ,t.[TemplateId] AS 'Block.TemplateId'
	  ,l.LanguageId AS 'Block.LanguageId'
	  ,bt.*
FROM 
	[sitestructure].[tBlock] AS t
	CROSS JOIN core.tLanguage AS l
	LEFT JOIN sitestructure.tBlockTranslation AS transl ON transl.BlockId = t.BlockId 
		AND transl.LanguageId = l.LanguageId
	inner join sitestructure.[vCustomBlockType] as bt on bt.[BlockType.BlockTypeId] = t.[BlockTypeId]

GO
