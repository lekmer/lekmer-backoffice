SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBlockBrandList]
AS
SELECT 
	[BlockId] as 'BlockBrandList.BlockId',
	[ColumnCount] as 'BlockBrandList.ColumnCount',
	[RowCount] as 'BlockBrandList.RowCount',
	[IncludeAllBrands] as 'BlockBrandList.IncludeAllBrands',
	[LinkContentNodeId] as 'BlockBrandList.LinkContentNodeId'
FROM 
	[lekmer].[tBlockBrandList]
GO
