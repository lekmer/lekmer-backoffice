SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomCategorySiteStructureRegistry]
as
	select
		*
	from
		[product].[vCategorySiteStructureRegistry]
GO
