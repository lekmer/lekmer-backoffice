
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [campaignlek].[vGiftCardViaMailCartAction]
AS
	SELECT
		gcca.[CartActionId] AS 'GiftCardViaMailCartAction.CartActionId',
		gcca.[SendingInterval] AS 'GiftCardViaMailCartAction.SendingInterval',
		gcca.[TemplateId] AS 'GiftCardViaMailCartAction.TemplateId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailCartAction] gcca
		INNER JOIN [campaign].[vCustomCartAction] pa ON pa.[CartAction.Id] = gcca.[CartActionId]


GO
