
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBlockProductSimilarList]
AS
	SELECT
		[BlockId] as 'BlockProductSimilarList.BlockId',
		[ColumnCount] as 'BlockProductSimilarList.ColumnCount',
		[RowCount] as 'BlockProductSimilarList.RowCount',
		[TotalProductCount] as 'BlockProductSimilarList.TotalProductCount',
		[PriceRangeType] as 'BlockProductSimilarList.PriceRangeType'
	FROM
		[lekmer].[tBlockProductSimilarList]
GO
