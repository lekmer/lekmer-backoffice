IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'dino.miralem')
CREATE LOGIN [dino.miralem] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [dino.miralem] FOR LOGIN [dino.miralem]
GO
GRANT CONNECT TO [dino.miralem]
