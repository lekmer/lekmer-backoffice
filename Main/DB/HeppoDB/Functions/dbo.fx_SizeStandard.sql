SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>

-- takes i a string and returns a string
-- =============================================
CREATE FUNCTION [dbo].[fx_SizeStandard]
( @SizeStandardname nvarchar (50))
RETURNS nvarchar (50)
AS
BEGIN
	DECLARE @returnString nvarchar (50)
	
	if @SizeStandardname = 'USM'
	begin
		SET @returnString = 'USMale'
	end
	
	else if @SizeStandardname = 'USW'
	begin
		SET @returnString = 'USFemale'
	end
	
	else if @SizeStandardname = 'UKM'
	begin
		SET @returnString = 'UKMale'
	end	
	
	else if @SizeStandardname = 'UKW'
	begin
		SET @returnString = 'UKFemale'
	end			
	
	else if @SizeStandardname = 'EU'
	begin
		SET @returnString = 'EU'
	end		
	
	else
	begin
		SET @returnString = @SizeStandardname
	end
 
	RETURN @returnString
END
GO
