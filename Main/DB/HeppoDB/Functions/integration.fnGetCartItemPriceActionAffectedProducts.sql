SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [integration].[fnGetCartItemPriceActionAffectedProducts](
	@CartActionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN
	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories
	
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartItemPriceActionIncludeCategory ic ON ic.CategoryId = p.CategoryId
		WHERE
			ic.CartActionId = @CartActionId

	UNION -- include products
	
		SELECT
			ProductId
		FROM
			addon.tCartItemPriceActionIncludeProduct ip
		WHERE
			ip.CartActionId = @CartActionId

	EXCEPT -- exclude products from categories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartItemPriceActionExcludeCategory ec ON ec.CategoryId = p.CategoryId
		WHERE
			ec.CartActionId = @CartActionId

	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			addon.tCartItemPriceActionExcludeProduct ep
		WHERE
			ep.CartActionId = @CartActionId

	RETURN
END
GO
