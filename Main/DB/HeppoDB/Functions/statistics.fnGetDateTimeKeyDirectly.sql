SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [statistics].fnGetDateTimeKeyDirectly
(
	@DateTime	smalldatetime,
	@Mode		varchar(10)
)
RETURNS smalldatetime
AS 
BEGIN 
	RETURN  (cast(CONVERT(varchar(4), Year(@DateTime))+'-'
		+CONVERT(varchar(2), Month(@DateTime))+'-'
		+CONVERT(varchar(2), (CASE WHEN @Mode <> 'Year' THEN Day(@DateTime) ELSE 1 END))+' '
		+CONVERT(varchar(2), (CASE WHEN @Mode = 'Day' THEN datepart(hour, @DateTime) ELSE 0 END))
		+':00:00' 
		AS smalldatetime))
END
GO
