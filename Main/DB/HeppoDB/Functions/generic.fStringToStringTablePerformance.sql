SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [generic].[fStringToStringTablePerformance](
        @SepString nvarchar(max),
        @Sep nvarchar(10)
) returns @t table(SepString nvarchar(max))
as
begin
  declare @Tmp nvarchar(max), @Pos int
  -- add ending separator
  if right(@SepString, len(@Sep)) != @Sep begin
    set @Tmp = @SepString + @Sep
  end else begin
    set @Tmp = @SepString
  end
  -- extract elements
  set @Pos = charindex(@Sep, @Tmp)
  while @Pos > 0 begin
    insert into @t values(left(@Tmp, @Pos-1))
    set @Tmp = substring(@Tmp, @Pos+1, len(@Tmp) - @Pos)
  set @Pos = charindex(@Sep, @Tmp)
  end
  return
end
GO
