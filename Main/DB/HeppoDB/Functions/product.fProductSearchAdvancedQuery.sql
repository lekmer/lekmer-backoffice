SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE FUNCTION [product].[fProductSearchAdvancedQuery]
(
	@ChannelId INT,
	@CategoryId INT,
	@Title NVARCHAR(256),
	@PriceFrom DECIMAL(16,2),
	@PriceTo DECIMAL(16,2),
	@ERPId VARCHAR(50),
	@ProductStatusId INT,
	@EANCode VARCHAR(13)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @sqlQuery NVARCHAR(MAX)
	SET @sqlQuery =
	'WHERE
		[p].[Product.ChannelId] = '+CONVERT(NVARCHAR, @ChannelId)
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN '
		AND [p].[Product.CategoryId] IN (SELECT * FROM product.fnGetSubCategories('+CONVERT(NVARCHAR, @CategoryId)+'))' ELSE '' END 
					
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
		AND ([p].[Product.WebShopTitle] LIKE ''%' + Replace(@Title, '''', '''''') + '%'' OR [p].[Product.Title] LIKE ''%' + Replace(@Title, '''', '''''') + '%'')' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL)THEN '
		AND [p].[Price.PriceIncludingVat] >= '+CONVERT(NVARCHAR, @PriceFrom) ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN '
		AND [p].[Price.PriceIncludingVat] <= '+CONVERT(NVARCHAR, @PriceTo) ELSE '' END
		
		+ CASE WHEN (@ERPId IS NOT NULL) THEN '
		AND [p].[Product.ERPId] = '''+@ERPId+'''' ELSE '' END
		
		+ CASE WHEN (@ProductStatusId IS NOT NULL)	THEN '
		AND [p].[Product.ProductStatusId] = '+CONVERT(NVARCHAR, @ProductStatusId) ELSE '' END 
		
		+ CASE WHEN (@EANCode IS NOT NULL) THEN '
		AND [p].[Product.EANCode] = '''+@EANCode+'''' ELSE '' END 
	RETURN @sqlQuery
END

















GO
