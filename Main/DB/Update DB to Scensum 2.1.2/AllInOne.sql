create function generic.fPrepareFulltextSearchParameter
(
	@Value VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	SET @Value = REPLACE(@Value, '"', '""')
	SET @Value = REPLACE(@Value, ' ', '*" and "')
	SET @Value = '"' + @Value + '*"'
	RETURN @Value
END
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [campaign].[pCampaignSearchProductCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignSearchProductCampaigns]
@Title	nvarchar(MAX)
AS
BEGIN
	SET @Title = [generic].[fPrepareSearchParameter](@Title)
	
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tProductCampaign PC ON PC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignSearchCartCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignSearchCartCampaigns]
@Title	nvarchar(MAX)
AS
BEGIN
	SET @Title = [generic].[fPrepareSearchParameter](@Title)
	
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignGetAllCartCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignGetAllCartCampaigns]
AS
BEGIN
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C 
		INNER JOIN campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	ORDER BY 
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignGetAllByFolder]'
GO
ALTER PROCEDURE [campaign].[pCampaignGetAllByFolder]
@FolderId	int
AS
BEGIN
	SELECT 
		*
	FROM 
		[campaign].[vCustomCampaign]
	WHERE 
		[Campaign.FolderId] = @FolderId
	ORDER BY 
		[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignGetAllProductCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignGetAllProductCampaigns]
AS
BEGIN
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C 
		INNER JOIN campaign.tProductCampaign PC ON PC.CampaignId = C.[Campaign.Id]
	ORDER BY 
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


EXEC sp_fulltext_database 'enable'
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [customer].[tAddress]'
GO
ALTER TABLE [customer].[tAddress] DROP
CONSTRAINT [FK_tAddress_tCustomerInformation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] DROP
CONSTRAINT [FK_tCustomerInformation_tAddress_Billing],
CONSTRAINT [FK_tCustomerInformation_tAddress_Delivery],
CONSTRAINT [FK_tCustomerInformation_tCustomer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [order].[tOrder]'
GO
ALTER TABLE [order].[tOrder] DROP
CONSTRAINT [FK_tOrder_tCustomer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] DROP CONSTRAINT [PK_tCustomerInformation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[pCustomerSearch]'
GO

ALTER PROCEDURE [customer].[pCustomerSearch]
	@ErpId          varchar(50),
	@FirstName      nvarchar(64),
	@LastName       nvarchar(64),
	@CivicNumber    varchar(50),
	@Email          nvarchar(64),
	@PhoneHome      varchar(50),
	@PhoneMobile    varchar(50),
	@CreatedFrom    datetime,
	@CreatedTo      datetime,
	@StatusId       int,
	@Separator      char(1),
	@Page           int = NULL,
	@PageSize       int
as   
begin
    set nocount on

	DECLARE @sqlGeneral nvarchar(max)
	declare @sql nvarchar(max)
	declare @sqlCount nvarchar(max)
	declare @sqlFragment nvarchar(max)
	
	set @sqlFragment = '
		select
			row_number() over (order by c.CustomerId desc) AS _Number,
			c.CustomerId
		from
			[customer].[tCustomer] c
			inner join [customer].[tCustomerInformation] ci
				on c.CustomerId = ci.CustomerId
		where
			1 = 1'
			+ case when (@ErpId is not null) then + ' and c.ErpId = @ErpId' else '' end
			+ case when (@StatusId is not null) then + ' and c.CustomerStatusId = @StatusId' else '' end
			+ case when (@FirstName is not null) then + ' and contains(ci.FirstName, @FirstName)' else '' end
			+ case when (@LastName is not null) then + ' and contains(ci.LastName, @LastName)' else '' end
			+ case when (@CivicNumber is not null) then + ' and ci.CivicNumber = @CivicNumber' else '' end
			+ case when (@Email is not null) then + ' and ci.Email = @Email' else '' end
			+ case when (@PhoneHome is not null) then + ' and ci.PhoneNumber = @PhoneHome' else '' end
			+ case when (@PhoneMobile is not null) then + ' and ci.CellPhoneNumber = @PhoneMobile' else '' end
			+ case when (@CreatedFrom is not null) then + ' and ci.InformationCreatedDate >= @CreatedFrom' else '' end
			+ case when (@CreatedTo is not null) then + ' and ci.InformationCreatedDate <= @CreatedTo' else '' end

	SET @sqlGeneral = '
		IF (@FirstName IS NOT NULL)
		BEGIN
			SET @FirstName = generic.fPrepareFulltextSearchParameter(@FirstName)
		END
		IF (@LastName IS NOT NULL)
		BEGIN
			SET @LastName = generic.fPrepareFulltextSearchParameter(@LastName)
		END'

	set @sqlCount = @sqlGeneral + '
		select count(*) from (' + @sqlFragment + ') as CountResults'

	set @sql = @sqlGeneral + '
		select
			c.*
		from
			(' + @sqlFragment + ') as result
			inner join [customer].[vCustomCustomerSecure] c
				on c.[Customer.CustomerId] = result.CustomerId
		where
			_Number > ' + cast((@Page - 1) * @PageSize AS varchar(10)) + '
			and _Number <= ' + cast(@Page * @PageSize AS varchar(10)) + '
		order by
			result.CustomerId desc'
	

	exec sp_executesql @sqlCount,
		N'	@ErpId          varchar(50),
			@FirstName      nvarchar(64),
			@LastName       nvarchar(64),
			@CivicNumber    varchar(50),
			@Email          nvarchar(64),
			@PhoneHome      varchar(50),
			@PhoneMobile    varchar(50),
			@CreatedFrom    datetime,
			@CreatedTo      datetime,
			@StatusId       int',
			@ErpId,          
			@FirstName, 
			@LastName,     
			@CivicNumber,
			@Email,  
			@PhoneHome, 
			@PhoneMobile,  
			@CreatedFrom,  
			@CreatedTo,
			@StatusId

	exec sp_executesql @sql, 
		N'	@ErpId          varchar(50),
			@FirstName      nvarchar(64),
			@LastName       nvarchar(64),
			@CivicNumber    varchar(50),
			@Email          nvarchar(64),
			@PhoneHome      varchar(50),
			@PhoneMobile    varchar(50),
			@CreatedFrom    datetime,
			@CreatedTo      datetime,
			@StatusId       int',
			@ErpId,          
			@FirstName, 
			@LastName,     
			@CivicNumber,
			@Email,  
			@PhoneHome, 
			@PhoneMobile,  
			@CreatedFrom,  
			@CreatedTo,
			@StatusId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
PRINT N'Altering [order].[pOrderSearch]'
GO

alter procedure [order].[pOrderSearch]
	@FirstName		nvarchar(64),
	@LastName		nvarchar(64),
	@CustomerEmail  nvarchar(64),
	@CivicNumber	varchar(50),
	@Page           int = null,
	@PageSize       int

as
begin

    set nocount on

	DECLARE @sqlGeneral nvarchar(max)
	declare @sqlFragment nvarchar(max)
	declare @sqlCount nvarchar(max)
	declare @sql nvarchar(max)

    SET @sqlFragment = '
		select
			row_number() over (order by OrderId desc) as _Number,
			OrderId
		from
			[order].[tOrder] o
			inner join [customer].[tCustomerInformation] ci on o.CustomerId = ci.CustomerId
		where
			1 = 1'
			+ case when (@FirstName is not null) then + ' and contains(ci.FirstName, @FirstName)' else '' end
			+ case when (@LastName is not null) then + ' and contains(ci.LastName, @LastName)' else '' end
			+ case when (@CivicNumber is not null) then + 'and ci.CivicNumber = @CivicNumber' else '' end
			+ case when (@CustomerEmail is not null) then + 'and ci.Email = @CustomerEmail' else '' end

	SET @sqlGeneral = '
		IF (@FirstName IS NOT NULL)
		BEGIN
			SET @FirstName = generic.fPrepareFulltextSearchParameter(@FirstName)
		END

		IF (@LastName IS NOT NULL)
		BEGIN
			SET @LastName = generic.fPrepareFulltextSearchParameter(@LastName)
		END'

	set @sqlCount = @sqlGeneral + '

		select count(*) from (' + @sqlFragment + ') as CountResults'

	set @sql = @sqlGeneral + '
		select
			o.*
		from
			(' + @sqlFragment + ') as result
			inner join [order].[vCustomOrder] o on o.[Order.OrderId] = result.OrderId
		where
			_Number > ' + cast((@Page - 1) * @PageSize as varchar(10)) + '
			and _Number <= ' + cast(@Page * @PageSize as varchar(10)) + '
		order by
			result.OrderId desc'


	exec sp_executesql @sqlCount,
		N'	@FirstName		nvarchar(64),
			@LastName		nvarchar(64),
			@CustomerEmail  nvarchar(64),
			@CivicNumber	varchar(50)',
			@FirstName,
			@LastName,
			@CustomerEmail,
			@CivicNumber

	exec sp_executesql @sql, 
		N'	@FirstName		nvarchar(64),
			@LastName		nvarchar(64),
			@CustomerEmail  nvarchar(64),
			@CivicNumber	varchar(50)',
			@FirstName,
			@LastName,
			@CustomerEmail,
			@CivicNumber
end

GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

*/

PRINT N'Creating primary key [PK_tCustomerInformation] on [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] ADD CONSTRAINT [PK_tCustomerInformation] PRIMARY KEY CLUSTERED  ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tAddress_AddressId_Covered] on [customer].[tAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_AddressId_Covered] ON [customer].[tAddress] ([AddressId]) INCLUDE ([Addressee], [AddressTypeId], [City], [CountryId], [CustomerId], [PhoneNumber], [PostalCode], [StreetAddress], [StreetAddress2])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tAddress_CustomerInformationId] on [customer].[tAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_CustomerInformationId] ON [customer].[tAddress] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tAddress]'
GO
ALTER TABLE [customer].[tAddress] ADD
CONSTRAINT [FK_tAddress_tCustomerInformation] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] ADD
CONSTRAINT [FK_tCustomerInformation_tAddress_Billing] FOREIGN KEY ([CustomerId], [DefaultBillingAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId]),
CONSTRAINT [FK_tCustomerInformation_tAddress_Delivery] FOREIGN KEY ([CustomerId], [DefaultDeliveryAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId]),
CONSTRAINT [FK_tCustomerInformation_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tOrder]'
GO
ALTER TABLE [order].[tOrder] ADD
CONSTRAINT [FK_tOrder_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Creating full text catalogs'
GO
CREATE FULLTEXT CATALOG [fulltextcatalog_tcustomerinformation_default]
ON FILEGROUP [PRIMARY]
--IN PATH 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\FTData'
WITH ACCENT_SENSITIVITY = ON
AUTHORIZATION [dbo]
GO
PRINT N'Adding full text indexing to tables'
GO
CREATE FULLTEXT INDEX ON [customer].[tCustomerInformation] KEY INDEX [PK_tCustomerInformation] ON [fulltextcatalog_tcustomerinformation_default]
GO
PRINT N'Adding full text indexing to columns'
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ADD (FirstName LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ADD (LastName LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ENABLE
GO


SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] ALTER COLUMN [EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderItemSave]'
GO
ALTER PROCEDURE [order].[pOrderItemSave]
	@OrderItemId INT,
	@OrderId INT,
	@ProductId INT,
	@Quantity INT,
	@ActualPriceIncludingVat DECIMAL(16,2),
	@OriginalPriceIncludingVat DECIMAL(16,2),
	@VAT DECIMAL(16,2),
	@ErpId VARCHAR(50),
	@EanCode VARCHAR(20),
	@Title NVARCHAR(50),
	@OrderItemStatusId int = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderItem]
	SET
		OrderId = @OrderId,
		Quantity = @Quantity,
		ActualPriceIncludingVat = @ActualPriceIncludingVat,
		OriginalPriceIncludingVat = @OriginalPriceIncludingVat,
		VAT = @VAT,
		OrderItemStatusId = @OrderItemStatusId
	WHERE
		OrderItemId = @OrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderItem]
		(
			OrderId,
			Quantity,
			ActualPriceIncludingVat,
			OriginalPriceIncludingVat,
			VAT,
			OrderItemStatusId
		)
		VALUES
		(
			@OrderId,
			@Quantity,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @OrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [order].[tOrderItemProduct]
		(
			OrderItemId,
			ProductId,
			ErpId,
			EanCode,
			Title
		)
		VALUES
		(
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title
		)
	END
	
	RETURN @OrderItemId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tProduct]'
GO
ALTER TABLE [product].[tProduct] ALTER COLUMN [EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [product].[pProductSave]'
GO
alter procedure [product].[pProductSave]
	@ProductId			int,
	@ItemsInPackage		int,
	@ErpId				varchar(50),
	@EANCode			varchar(20),
	@Title				nvarchar(256),
	@WebShopTitle		nvarchar(256),
	@ProductStatusId	int,
	@Description		nvarchar(max),
	@ShortDescription	nvarchar(max),
	@CategoryId			int,
	@MediaId			int	
as
begin
	set nocount on

	update
		[product].[tProduct]
	set
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	where
		[ProductId] = @ProductId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO


IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Adding full text indexing to tables'
GO


CREATE PROCEDURE [order].[pPaymentTypeGetAllSecure]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	ORDER BY
		[PaymentType.Title]
END
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

----dlelete keys from [lekmer].[tLekmerCartItem]
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP
CONSTRAINT [FK_tLekmerCartItem_tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT [PK_tLekmerCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

---- end dlelete keys from [lekmer].[tLekmerCartItem]


----dlelete keys from [lekmer].[order].[tCartItem]'
GO
PRINT N'Dropping foreign keys from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP
CONSTRAINT [FK_tCartItem_tCart],
CONSTRAINT [FK_tCartItem_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP CONSTRAINT [PK_tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

----end dlelete keys from [lekmer].[order].[tCartItem]'

GO
PRINT N'Altering [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD
[CartItemId] [int] NOT NULL IDENTITY(1, 1)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
PRINT N'Creating primary key [PK_tCartItem] on [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [PK_tCartItem] PRIMARY KEY CLUSTERED  ([CartItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

---- Altering[lekmer].tLekmerCartItem]
PRINT N'Altering [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
[CartItemId] [int]  NULL 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
PRINT N'Insert date from [order].tCartItem to primary key [PK_tLekmerCartItem] on [lekmer].[tLekmerCartItem]'
GO
UPDATE LCI 
SET LCI.CartItemId = CI.[CartItemId]
FROM [order].[tCartItem] AS CI,[lekmer].[tLekmerCartItem] AS LCI
WHERE LCI.CartId = CI.[CartId] AND LCI.ProductId = CI.[ProductId]

GO
ALTER TABLE [lekmer].[tLekmerCartItem] ALTER COLUMN 
[CartItemId] [int] NOT NULL 

GO
PRINT N'Creating primary key [PK_tLekmerCartItem] on [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [PK_tLekmerCartItem] PRIMARY KEY CLUSTERED  ([CartItemId])
GO
PRINT N'Delete CartId  column from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP COLUMN [CartId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

----end Altering[lekmer].tLekmerCartItem]

PRINT N'Altering [order].[pCartItemSave]'
GO
ALTER PROCEDURE [order].[pCartItemSave]
	@CartItemId int,
    @CartId INT,
    @ProductId INT,
    @Quantity INT,
    @CreatedDate datetime
AS 
BEGIN
      SET NOCOUNT ON

      UPDATE
            [order].tCartItem
      SET
            Quantity = @Quantity
      WHERE
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [order].tCartItem
            (
                  CartId,
                  ProductId,
                  Quantity,
                  CreatedDate            
            )
            VALUES
            (
                  @CartId,
                  @ProductId,
                  @Quantity,
                  @CreatedDate
            )
      END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pCartItemDelete]'
GO

ALTER PROCEDURE [order].[pCartItemDelete] 
	@CartItemId int
as
begin
	set nocount on
	
	DELETE FROM 
		[order].tCartItem
	WHERE
		CartItemId = @CartItemId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCartItem]'
GO

ALTER VIEW [order].[vCartItem]
AS
SELECT 
		C.CartItemId AS 'CartItem.CartItemId',
		C.CartId AS 'CartItem.CartId',
		C.Quantity AS 'CartItem.Quantity', 
		C.CreatedDate AS 'CartItem.CreatedDate',
		P.*		
from
	[order].tCartItem C WITH (NOLOCK)
	inner join product.vCustomProduct P on C.ProductId = P.[Product.Id]
GO
PRINT N'Altering [order].[vCustomCartItem]'
GO

/****** Object:  View [order].[vCustomCartItem]    Script Date: 12/29/2010 17:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER view [order].[vCustomCartItem]
as
	select
		c.*,
		l.SizeId as [LekmerCartItem.SizeId],
		l.ErpId as [LekmerCartItem.ErpId]
	from
		[order].[vCartItem] c
		inner join [lekmer].tLekmerCartItem l on c.[CartItem.CartItemId] = l.CartItemId
GO



IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD
CONSTRAINT [FK_tCartItem_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId]),
CONSTRAINT [FK_tCartItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
PRINT N'Adding foreign keys to [lekmer].[tLekmerCartItem]'
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
CONSTRAINT [FK_tLekmerCartItem_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId])
--CONSTRAINT [FK_tLekmerCartItem_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId]),
--CONSTRAINT [FK_tLekmerCartItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

alter PROCEDURE [product].[pCategoryGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[Category.Id],
			v.[Category.ParentCategoryId],
			v.[Category.Title]
		FROM product.vCustomCategorySecure AS v
		WHERE v.[Category.ParentCategoryId] IS NULL

	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[Category.Id],
					v.[Category.ParentCategoryId],
					v.[Category.Title]
				FROM product.vCustomCategorySecure AS v
				WHERE 
					v.[Category.ParentCategoryId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[Category.ParentCategoryId]
									 FROM product.vCustomCategorySecure AS v
									 WHERE v.[Category.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT 
							v.[Category.Id],
							v.[Category.ParentCategoryId],
							v.[Category.Title]
						FROM product.vCustomCategorySecure AS v
						WHERE v.[Category.ParentCategoryId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END

		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM product.vCustomCategorySecure AS v
									WHERE v.[Category.ParentCategoryId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY Title ASC
END


go

ALTER VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO

ALTER view [product].[vCustomProductSecure]
as
	select
		*
	from
		[product].[vProductSecure]

GO


ALTER VIEW [product].[vProductRelationList]
AS
SELECT      
       prl.RelationListId 'ProductRelationList.RelationListId',
       p.*
       
FROM       
       [product].[tProductRelationList] prl       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = prl.ProductId


GO

ALTER VIEW [product].[vRelationListProduct]
AS
SELECT      
       rlp.RelationListId 'RelationListProduct.RelationListId',
       p.*
       
FROM       
       [product].[tRelationListProduct] rlp       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = rlp.ProductId


GO

exec dev.pRefreshAllViews
go

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [product].[pProductGetAllUsingRelationList ]'
GO
DROP PROCEDURE [product].[pProductGetAllUsingRelationList ]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListSearch]'
GO

ALTER PROCEDURE [product].[pPriceListSearch]
	@SearchParameter NVARCHAR(50),
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = NULL,
	@SortDescending	BIT = NULL
AS
BEGIN
	
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY pl.[' 
			+ COALESCE(@SortBy, 'PriceList.Id')
			+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			pl.*
		FROM
			[product].[vCustomPriceList] pl
		WHERE
			(
				pl.[PriceList.Title] LIKE ''%'' + @SearchParameter + ''%''
				OR EXISTS (
					SELECT
						1
					FROM
						product.tProduct p
						inner join product.tPriceListItem pli on pli.PriceListId = pl.[PriceList.Id] and pli.ProductId = p.ProductId
					WHERE
						p.ErpId = @SearchParameter
				)
			)
		'

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@SearchParameter nvarchar(50)',
			@SearchParameter

	EXEC sp_executesql @sql, 
		N'	@SearchParameter nvarchar(50)',
			@SearchParameter
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListGetAllByFolderWithPaging]'
GO

ALTER PROCEDURE [product].[pPriceListGetAllByFolderWithPaging]
	@PriceListFolderId NVARCHAR(50),
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = NULL,
	@SortDescending	BIT = NULL
AS
BEGIN
	
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY pl.[' 
			+ COALESCE(@SortBy, 'PriceList.Id')
			+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			pl.*
		FROM
			[product].[vCustomPriceList] pl
		WHERE
		   	pl.[PriceList.FolderId] = @PriceListFolderId'

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@PriceListFolderId nvarchar(50)',
			@PriceListFolderId

	EXEC sp_executesql @sql, 
		N'	@PriceListFolderId nvarchar(50)',
			@PriceListFolderId
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[fnGetCurrencyAndPriceListRegistry]'
GO
CREATE function [product].[fnGetCurrencyAndPriceListRegistry](
	@ProductId int,
	@ChannelId int
)
RETURNS @tPriceListData TABLE (CurrencyId int, PriceListRegistryId int)
AS 
BEGIN
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	INSERT INTO @tPriceListData (CurrencyId, PriceListRegistryId)
	SELECT @CurrencyId, PMC.PriceListRegistryId 
	FROM product.tProductRegistryProduct AS PRP INNER JOIN product.tProductModuleChannel AS PMC ON 
			PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId AND PRP.ProductId = @ProductId	
	RETURN 
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListGetAllByFolder]'
GO
ALTER PROCEDURE [product].[pPriceListGetAllByFolder]
	@PriceListFolderId int
AS
BEGIN
	SELECT
		pl.*
	FROM
		[product].[vCustomPriceList] pl
	WHERE
		pl.[PriceList.FolderId] = @PriceListFolderId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListItemGetAllByProduct]'
GO

ALTER PROCEDURE [product].[pPriceListItemGetAllByProduct]
	@ProductId	int
as
begin
	SET NOCOUNT ON;

	SELECT 
		v.*
	FROM 
		product.vCustomPriceListItem v
	where
		v.[Price.ProductId] = @ProductId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[pProductGetAllUsingRelationList]'
GO
CREATE PROCEDURE [product].[pProductGetAllUsingRelationList]
		@ChannelId int,
		@RelationListId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY prl.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		prl.*,
		[pli].*
		FROM [product].[vCustomProductRelationList] prl
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = prl.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = prl.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', prl.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			prl.[ProductRelationList.RelationListId] = '+  CAST(@RelationListId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END
	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetAllByCategory]'
GO
ALTER PROCEDURE [product].[pProductGetAllByCategory]
		@ChannelId int,
		@CategoryId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END
	
DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		[p].*,
		[pli].*
		FROM [product].[vCustomProductSecure] p
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', p.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			p.[Product.CategoryId] = '+  CAST(@CategoryId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END	

	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetAllByRelationList]'
GO
ALTER PROCEDURE [product].[pProductGetAllByRelationList]
		@ChannelId int,
		@RelationListId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY rlp.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		rlp.*,
		[pli].*
		FROM [product].[vCustomRelationListProduct]  rlp
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = rlp.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = rlp.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', rlp.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			rlp.[RelationListProduct.RelationListId] = '+  CAST(@RelationListId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END
	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vProductRecord]'
GO


ALTER VIEW [product].[vProductRecord]
AS
SELECT     
	P.ProductId 'Product.Id',
	P.ItemsInPackage 'Product.ItemsInPackage',
	P.ErpId 'Product.ErpId',
	P.EanCode 'Product.EanCode',
	P.NumberInStock 'Product.NumberInStock',
    P.CategoryId 'Product.CategoryId',
    P.WebShopTitle 'Product.WebShopTitle',
    P.Title 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.Description 'Product.Description',
    P.ShortDescription 'Product.ShortDescription',
    P.IsDeleted 'Product.IsDeleted',
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vProductSecure]'
GO

ALTER VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO


ALTER VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO

ALTER view [product].[vCustomProductSecure]
as
	select
		*
	from
		[product].[vProductSecure]

GO

ALTER VIEW [product].[vRelationListProduct]
AS
SELECT      
       rlp.RelationListId 'RelationListProduct.RelationListId',
       p.*
       
FROM       
       [product].[tRelationListProduct] rlp       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = rlp.ProductId


GO

exec dev.pRefreshAllViews
go

PRINT N'Altering [product].[pProductGetRecordById]'
GO
ALTER PROCEDURE [product].[pProductGetRecordById]
	@ProductId	int,
	@ChannelId	int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CurrencyId int, @PriceListRegistryId int, @PriceListId int
	SELECT @CurrencyId = CurrencyId, @PriceListRegistryId = PriceListRegistryId
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT *		
	FROM 
		[product].[vCustomProductRecord] as p
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = @PriceListId
	WHERE
		p.[Product.Id] = @ProductId
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCategoryView]'
GO

ALTER VIEW [product].[vCategoryView]
AS
	SELECT     
		c.*,
		ch.[Channel.Id],
		r.[CategoryRegistry.ProductParentContentNodeId],
		r.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		[product].[vCustomCategory] AS c
		CROSS JOIN core.vCustomChannel ch
		LEFT JOIN sitestructure.tSiteStructureModuleChannel mch ON mch.ChannelId = ch.[Channel.Id]
		LEFT JOIN product.vCustomCategorySiteStructureRegistry r
			ON mch.SiteStructureRegistryId = r.[CategoryRegistry.SiteStructureRegistryId]
			AND c.[Category.Id] = r.[CategoryRegistry.CategoryId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

exec dev.pRefreshAllViews
go

PRINT N'Altering [product].[pProductGetByIdSecure]'
GO
ALTER PROCEDURE [product].[pProductGetByIdSecure]
	@ProductId	int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;	
	
	DECLARE @CurrencyId int, @PriceListRegistryId int, @PriceListId int
	SELECT @CurrencyId = CurrencyId, @PriceListRegistryId = PriceListRegistryId
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT *		
	FROM 
		[product].[vCustomProductSecure] as p
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = @PriceListId
	WHERE
		p.[Product.Id] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewById]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewById]
	@CategoryId	int,
	@ChannelId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Category.Id] = @CategoryId
		and c.[Channel.Id] = @ChannelId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewAll]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewAll]
	@ChannelId	int,
	@LanguageId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Channel.Id] = @ChannelId
		and c.[Category.LanguageId] = @LanguageId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetAllByVariationGroup]'
GO
ALTER PROCEDURE [product].[pProductGetAllByVariationGroup]
	@VariationGroupId	int,
	@ChannelId			int
as
begin
	SET NOCOUNT ON;
	
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	SELECT
		P.*,
		pli.*
	FROM 
		[product].[vCustomProductSecure] AS P
		INNER JOIN [product].[tVariationGroupProduct] AS VGP on P.[Product.Id] = VGP.[ProductId] AND VGP.VariationGroupId = @VariationGroupId    
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = P.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, P.[Product.Id], PMC.PriceListRegistryId, NULL)
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pBlockProductListProductGetAllByBlockSecure]'
GO

ALTER PROCEDURE [product].[pBlockProductListProductGetAllByBlockSecure]
	@ChannelId int,
	@BlockId int
as
begin
	
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	SELECT 
		bp.*,
		p.*,
		pli.*
	from 
		[product].[vCustomBlockProductListProduct] as bp
		inner join [product].[vCustomProductSecure] as p on bp.[BlockProductListProduct.ProductId] = p.[Product.Id] AND bp.[BlockProductListProduct.BlockId] = @BlockId
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = P.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, P.[Product.Id], PMC.PriceListRegistryId, NULL)
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


alter procedure [product].[pProductImageDeleteAllByProduct]
@ProductId INT
AS
BEGIN
	DELETE
		[product].[tProductImage]
	WHERE
		ProductId = @ProductId
END
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating primary key [PK_tBlockProductListProduct] on [product].[tBlockProductListProduct]'
GO
ALTER TABLE [product].[tBlockProductListProduct] ADD CONSTRAINT [PK_tBlockProductListProduct] PRIMARY KEY CLUSTERED  ([BlockId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPriceListItem] on [product].[tPriceListItem]'
GO
ALTER TABLE [product].[tPriceListItem] ADD CONSTRAINT [PK_tPriceListItem] PRIMARY KEY CLUSTERED  ([PriceListId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tCategory_ParentCategoryId] on [product].[tCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_tCategory_ParentCategoryId] ON [product].[tCategory] ([ParentCategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

alter PROCEDURE [product].[pProductIndexItemGetAll]
	@ChannelId int,
	@Page int = NULL,
	@PageSize int
AS
BEGIN

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)

SET @sqlFilter = '
	(
		SELECT 
			ROW_NUMBER() OVER (ORDER BY [Product.Id]) AS Number,
			[Product.Id], 
			(CASE WHEN (([Product.WebShopTitle] IS NOT NULL) AND ([Product.WebShopTitle] <> '''')) THEN [Product.WebShopTitle] ELSE [Product.Title] END) as [Product.Title], 
			[Product.Description], 
			[Product.ShortDescription],
			[Lekmer.LekmerErpId],
			0 as [Product.OrderCount]
		FROM 
			product.vCustomProductView
		WHERE
			[Product.ChannelId] = ' +  CAST(@ChannelId AS varchar(10)) + '
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END	

	EXEC (@sqlCount)
	EXEC (@sql)

END
GO

EXEC sp_fulltext_database 'enable'
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [product].[pProductSearch]'
GO

ALTER PROCEDURE [product].[pProductSearch]
	@CategoryId int,
	@Title nvarchar(256),
	@PriceFrom DECIMAL(16,2),
	@PriceTo DECIMAL(16,2),
	@ErpId varchar(50),
	@StatusId int,
	@EanCode varchar(20),
	@ChannelId	int,
	@Page int = NULL,
	@PageSize int,
	@SortBy varchar(20) = NULL,
	@SortDescending bit = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sqlGeneral nvarchar(max)
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	SET @sqlFragment = '	
				SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					p.ProductId,
					pli.*
				FROM 
					product.tProduct AS p
					-- get price for current channel
					LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[ProductId] 
					LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
					LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[ProductId]
						AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', p.[ProductId], PMC.PriceListRegistryId, NULL)
				WHERE
					p.IsDeleted = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND p.[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS(p.*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND p.[ErpId] = @ErpId' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND p.[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@EanCode IS NOT NULL) THEN + '
					AND p.[EanCode] = @EanCode' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN product.vCustomProductSecure vp
				on p.ProductId = vp.[Product.Id]'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(50),
			@StatusId int,
			@EanCode varchar(20)',
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode

	EXEC sp_executesql @sql, 
		N'	@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(50),
			@StatusId int,
			@EanCode varchar(20)',
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSearchAdvanced]'
GO

ALTER PROCEDURE [product].[pProductSearchAdvanced]
	@ChannelId	INT,
	@SearchCriteriaInclusiveList XML,
	@SearchCriteriaExclusiveList XML,
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = 'ErpId',
	@SortDescending BIT = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @ParsedCategoryId INT
	DECLARE @ParsedTitle NVARCHAR(256)
	DECLARE @ParsedPriceFrom DECIMAL(16,2)
	DECLARE @ParsedPriceTo DECIMAL(16,2)
	DECLARE @ParsedErpId VARCHAR(50)
	DECLARE @ParsedStatusId INT
	DECLARE @ParsedEanCode VARCHAR(20)
	
	DECLARE CriteriaInclusiveCursor CURSOR
		FOR SELECT  T.a.value('(@categoryId)', 'INT') AS 'categoryId',
					T.a.value('(@title)', 'NVARCHAR(256)') AS 'title',
					T.a.value('(@priceFrom)', 'DECIMAL(16,2)') AS 'priceFrom',
					T.a.value('(@priceTo)', 'DECIMAL(16,2)') AS 'priceTo',
					T.a.value('(@erpId)', 'VARCHAR(50)') AS 'erpId',
					T.a.value('(@statusId)', 'INT') AS 'statusId',
					T.a.value('(@eanCode)', 'VARCHAR(20)') AS 'eanCode'
			FROM    @SearchCriteriaInclusiveList.nodes('criterias/criteria') AS T ( a )
	
	OPEN CriteriaInclusiveCursor
	FETCH NEXT FROM CriteriaInclusiveCursor INTO @ParsedCategoryId, @ParsedTitle, @ParsedPriceFrom, @ParsedPriceTo, @ParsedERPId, @ParsedStatusId, @ParsedEANCode
	CLOSE CriteriaInclusiveCursor
	DEALLOCATE CriteriaInclusiveCursor

	IF ( @@FETCH_STATUS = 0 )
	BEGIN
		exec [product].[pProductSearch]
			@CategoryId = @ParsedCategoryId,
			@Title = @ParsedTitle,
			@PriceFrom = @ParsedPriceFrom,
			@PriceTo = @ParsedPriceTo,
			@ErpId = @ParsedERPId,
			@StatusId  = @ParsedStatusId,
			@EanCode  = @ParsedEANCode,
			@ChannelId	 = @ChannelId,
			@Page = @Page,
			@PageSize  = @PageSize,
			@SortBy = @SortBy,
			@SortDescending = @SortDescending
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Creating full text catalogs'
GO
CREATE FULLTEXT CATALOG [fulltextcatalog_tproduct_default]
ON FILEGROUP [PRIMARY]
--IN PATH 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\FTData'
WITH ACCENT_SENSITIVITY = ON
AUTHORIZATION [dbo]
GO
PRINT N'Adding full text indexing to tables'
GO
CREATE FULLTEXT INDEX ON [product].[tProduct] KEY INDEX [PK_tProduct] ON [fulltextcatalog_tproduct_default]
GO
PRINT N'Adding full text indexing to columns'
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ADD (Title LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ADD (WebShopTitle LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ENABLE
GO


SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [product].[tProductSeoSetting]'
GO
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSeoSettingSave]'
GO




/*
*****************  Version 1  *****************
User: Roman G.		Date: 15.09.2008		Time: 17:00
Description:
      Save/Update SEOSettings for Product.
/***********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 14:35
Description: delete try - cath  block and error logging 
**********************************************************/
*/
ALTER PROCEDURE [product].[pProductSeoSettingSave]
	@ProductId		INT,
	@Title			NVARCHAR(250),
	@Description	NVARCHAR(250),
	@Keywords		NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[product].[tProductSeoSetting]
	SET
		[Title] = @Title,
		[Description] = @Description,
		[Keywords] = @Keywords
	WHERE
		[ProductId] = @ProductId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [product].[tProductSeoSetting]
		(				
			[ProductId],
			[Title],
			[Description],
			[Keywords]
		)
		VALUES
		(
			@ProductId,
			@Title,
			@Description,
			@Keywords
		)
	END
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tProductSeoSettingTranslation]'
GO
ALTER TABLE [product].[tProductSeoSettingTranslation] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [product].[tProductSeoSettingTranslation] ALTER COLUMN [Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [security].[pSystemUserSearch]'
GO
ALTER PROCEDURE [security].[pSystemUserSearch]
	@UserName			nvarchar(100),
	@Name				nvarchar(100),
	@StatusId			int,
	@ActivationDate		smalldatetime,
	@ExpirationDate		smalldatetime,
	@Page				int = NULL,
	@PageSize			int,
	@SortBy				varchar(20) = NULL,
	@SortDescending		bit = NULL
as
begin
   set nocount on

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	IF (@UserName IS NOT NULL)
	BEGIN
		SET @UserName = generic.fPrepareSearchParameter(@UserName)
	END
	IF (@Name IS NOT NULL)
	BEGIN
		SET @Name = generic.fPrepareSearchParameter(@Name)
	END

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' 
		+ COALESCE(@SortBy, 'SystemUser.Id')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			*
		from
			[security].[vCustomSystemUser]
		where 1=1'

		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
		AND [SystemUserStatus.Id] = @StatusId' ELSE '' END

		+ CASE WHEN (@Name IS NOT NULL) THEN + '
		AND [SystemUser.Name] LIKE @Name' ELSE '' END

		+ CASE WHEN (@UserName IS NOT NULL) THEN + '
		AND [SystemUser.UserName] LIKE @UserName' ELSE '' END

		+ CASE WHEN (@ActivationDate IS NOT NULL) THEN + '
		AND [SystemUser.ActivationDate] >= @ActivationDate ' ELSE '' END

		+ CASE WHEN (@ExpirationDate IS NOT NULL) THEN + '
		AND [SystemUser.ExpirationDate] <= @ExpirationDate ' ELSE '' END

	SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@UserName				nvarchar(100),
			@Name					nvarchar(100),
			@StatusId				int,
			@ActivationDate			smalldatetime,
			@ExpirationDate			SMALLDATETIME',
			@UserName,          
			@Name, 
			@StatusId,     
			@ActivationDate,
			@ExpirationDate
			
			     
	EXEC sp_executesql @sql, 
		N'	@UserName				nvarchar(100),
			@Name					nvarchar(100),
			@StatusId				int,
			@ActivationDate			smalldatetime,
			@ExpirationDate			SMALLDATETIME',
			@UserName,          
			@Name, 
			@StatusId,     
			@ActivationDate,
			@ExpirationDate
	end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [security].[pRoleSearch]'
GO
ALTER PROCEDURE [security].[pRoleSearch]
	@Title			nvarchar(50),
	@Page           int = NULL,
	@PageSize       int,
	@SortBy         varchar(20) = NULL,
	@SortDescending bit = NULL
AS 
BEGIN 
   SET nocount ON

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	IF (@Title IS NOT NULL)
	BEGIN
		SET @Title = generic.fPrepareSearchParameter(@Title)
	END

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' 
		+ COALESCE(@SortBy, 'Role.Id')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			*
		from
		   [security].[vCustomRole]'
		   + CASE WHEN (@Title IS NOT NULL) THEN + '
		WHERE [Role.Title] LIKE @Title' ELSE '' END
		
	SET @sql = 
		'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END

	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'@Title nvarchar(100)',
		@Title          
			     
	EXEC sp_executesql @sql, 
		N'@Title nvarchar(100)',
		@Title          
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [sitestructure].[tContentPageSeoSettingTranslation]'
GO
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[tContentPageSeoSetting]'
GO
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[pContentPageSeoSettingSave]'
GO


/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

ALTER procedure [sitestructure].[pContentPageSeoSettingSave]
	@ContentNodeId	int,
	@Title			nvarchar(250),
	@Description	nvarchar(250),
	@Keywords		nvarchar(250)
as
BEGIN
	update
		[sitestructure].[tContentPageSeoSetting]
	set
		[Title]			= @Title,
		[Description]	= @Description,
		[Keywords]		= @Keywords
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		INSERT [sitestructure].[tContentPageSeoSetting]
		(
			[ContentNodeId],
			[Title],
			[Description],
			[Keywords]
		)
		values
		(
			@ContentNodeId,
			@Title,
			@Description,
			@Keywords
		)
	end
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [statistics].[pOrderGetSummary]'
GO
ALTER procedure [statistics].[pOrderGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@Alternate		bit,
	@StatusIds		varchar(max)
AS 
BEGIN	
	SELECT 
		isnull(count([OrderId]), 0)
	FROM
		[order].[tOrder] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [UsedAlternate] = @Alternate
		AND [CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pSaleGetSummary]'
GO
ALTER procedure [statistics].[pSaleGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN	
	SELECT 
		isnull(sum(oi.[ActualPriceIncludingVat] * oi.[Quantity]), 0)
	FROM
		[order].tOrderItem oi WITH(NOLOCK)
		INNER JOIN [order].tOrder o WITH(NOLOCK) ON oi.OrderId = o.OrderId
	WHERE
		o.[ChannelId] = @ChannelId
		AND o.[CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pOrderGetGroupedData]'
GO
ALTER procedure [statistics].[pOrderGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		
	DECLARE @tOrder TABLE ([DateTime] smalldatetime)
	INSERT INTO 
		@tOrder
	(
		[DateTime]
	)
	SELECT
		[statistics].[fnGetDateTimeKeyDirectly]([CreatedDate], @Mode) AS 'DateTime'
	FROM
		[order].[tOrder] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
			
	SELECT 
		d.[DateTime], 
		isnull(count(o.DateTime), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tOrder o ON o.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pSaleGetGroupedData]'
GO
ALTER procedure [statistics].[pSaleGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		 
	DECLARE @tSale TABLE ([DateTime] smalldatetime, Amount decimal(16, 2))
	INSERT INTO 
		@tSale
	(
		[DateTime],
		Amount
	)
	SELECT
		[statistics].[fnGetDateTimeKeyDirectly](o.[CreatedDate], @Mode) AS 'DateTime',
		oi.[ActualPriceIncludingVat] * oi.[Quantity] AS 'Amount'
	FROM
		[order].tOrderItem oi WITH(NOLOCK)
		INNER JOIN [order].tOrder o WITH(NOLOCK) ON oi.OrderId = o.OrderId
	WHERE
		o.[ChannelId] = @ChannelId
		AND o.[CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
			
	SELECT 
		d.[DateTime], 
		isnull(sum(s.Amount), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tSale s ON s.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pTopSellerGetAll]'
GO
ALTER procedure [statistics].[pTopSellerGetAll]
	@ChannelId int,
	@DateTimeFrom datetime,
	@DateTimeTo datetime,
	@TopCount int,
	@StatusIds		varchar(max)
as
begin
	declare @ProductIds table(ProductId int, Quantity int)
	
	insert
		@ProductIds
	(
		ProductId,
		Quantity
	)
	select top(@TopCount)
		p.ProductId,
		sum(oi.Quantity)
	from
		product.tProduct p
		inner join [order].tOrderItemProduct oip WITH(NOLOCK) on p.ProductId = oip.ProductId
		inner join [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		inner join [order].tOrder o WITH(NOLOCK) on oi.OrderId = o.OrderId
	where
		o.ChannelId = @ChannelId
		and o.CreatedDate between isnull(@DateTimeFrom, o.CreatedDate) and isnull(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
	group by
		p.ProductId
	order by
		sum(oi.Quantity) desc

	select
		p.*,
		pli.*,
		i.Quantity as 'TopSeller.Quantity'
	from
		@ProductIds as i 
		inner join [product].[vCustomProduct] as p on p.[Product.Id] = i.ProductId
		inner join [product].[vCustomPriceListItem] as pli
			on pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[Product.ChannelId] = @ChannelId
	order by
		i.Quantity DESC
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

alter PROCEDURE [template].[pModelGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[ModelFolder.Id] * (-1),
			NULL,
			v.[ModelFolder.Title]
		FROM template.vCustomModelFolder AS v

	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[Model.Id],
					v.[Model.ModelFolderId]  * (-1),
					v.[Model.Title]
				FROM template.vCustomModel AS v
				WHERE v.[Model.ModelFolderId] = @SelectedId
		END

		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN Id > 0 THEN 0 ELSE (CASE WHEN EXISTS (SELECT 1 FROM template.vCustomModel AS v
				WHERE v.[Model.ModelFolderId] = -1 * Id) THEN 1 ELSE 0 END) END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY Title ASC
END

GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [template].[tModelFragmentFunction]'
GO
ALTER TABLE [template].[tModelFragmentFunction] ALTER COLUMN [CommonName] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pModelFragmentFunctionSave]'
GO
ALTER PROCEDURE [template].[pModelFragmentFunctionSave] 
	@FunctionId INT,
	@ModelFragmentId INT,
	@FunctionTypeId INT,
	@CommonName NVARCHAR(500),
	@Description NVARCHAR(MAX)
AS 
BEGIN
    SET NOCOUNT ON
    	
	UPDATE  
		[template].[tModelFragmentFunction]
	SET     
		[ModelFragmentId] = @ModelFragmentId,
		[FunctionTypeId] = @FunctionTypeId,
		[CommonName] = @CommonName,
		[Description] = @Description
	WHERE
		[FunctionId] = @FunctionId
		
	IF  @@ROWCOUNT = 0 
	BEGIN
		INSERT  [template].[tModelFragmentFunction] 
        (
			[ModelFragmentId],
			[FunctionTypeId],
			[CommonName],
			[Description]
        )
        VALUES
        (
        	@ModelFragmentId,
			@FunctionTypeId, 
			@CommonName,
			@Description
		)

        SET @FunctionId = SCOPE_IDENTITY()
   END
   RETURN @FunctionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pModelFragmentFunctionGetByCommonName]'
GO
ALTER PROCEDURE [template].[pModelFragmentFunctionGetByCommonName]
	@CommonName	varchar(500)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomModelFragmentFunction]
	where
		[ModelFragmentFunction.CommonName] = @CommonName
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[tEntityFunction]'
GO
ALTER TABLE [template].[tEntityFunction] ALTER COLUMN [CommonName] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pEntityFunctionGetByCommonName]'
GO
ALTER PROCEDURE [template].[pEntityFunctionGetByCommonName]
	@EntityFunctionCommonName	varchar(500)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomEntityFunction] 
	where
		[EntityFunction.CommonName] = @EntityFunctionCommonName
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

Alter VIEW [template].[vTemplateSetting]
AS

SELECT  T.[TemplateId] AS [TemplateSetting.TemplateId],
		MS.[ModelSetting.Id] AS [TemplateSetting.ModelSettingId],
		TS.[Value] AS [TemplateSetting.Value], 
        TS.[AlternateValue] AS [TemplateSetting.AlternateValue], 
        MS.[ModelSetting.CommonName],
		MS.[ModelSetting.Title],
		MS.[ModelSetting.TypeId],
		MST.[ModelSettingType.Title]

FROM	[template].[tTemplate] AS T
		INNER JOIN [template].[vCustomModelSetting] AS MS ON T.[ModelId] = MS.[ModelSetting.ModelId]
		INNER JOIN [template].[vCustomModelSettingType] AS MST ON MST.[ModelSettingType.Id] = MS.[ModelSetting.TypeId]
		LEFT JOIN [template].[tTemplateSetting] AS TS ON MS.[ModelSetting.Id] = TS.[ModelSettingId] AND T.TemplateId = TS.TemplateId

GO


SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

-- Drop constraints from [template].[tTemplateFragment]
ALTER TABLE [template].[tTemplateFragment] DROP CONSTRAINT [FK_tTemplateFragment_tModelFragment]
ALTER TABLE [template].[tTemplateFragment] DROP CONSTRAINT [FK_tTemplateFragment_tTemplate]

-- Drop constraints from [template].[tTemplate]
ALTER TABLE [template].[tTemplate] DROP CONSTRAINT [FK_tTemplate_tModel]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Drop constraints from [template].[tAlias]
ALTER TABLE [template].[tAlias] DROP CONSTRAINT [FK_tAlias_tAliasFolder]
ALTER TABLE [template].[tAlias] DROP CONSTRAINT [FK_tAlias_tAliasType]

-- Drop constraint FK_tAliasTranslation_tAlias from [template].[tAliasTranslation]
ALTER TABLE [template].[tAliasTranslation] DROP CONSTRAINT [FK_tAliasTranslation_tAlias]

-- Drop constraints from [template].[tAliasFolder]
ALTER TABLE [template].[tAliasFolder] DROP CONSTRAINT [FK_tAliasFolder_tAliasFolder]


-- Delete 5 rows from [template].[tModelFragmentRegion]
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=34
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=35
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=47
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=48
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=116

-- Delete 3 rows from [template].[tModelFragmentEntity]
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=139 AND [EntityId]=6
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=139 AND [EntityId]=7
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=140 AND [EntityId]=8

-- Delete 1 row from [template].[tEntityFunction]
DELETE FROM [template].[tEntityFunction] WHERE [FunctionId]=35

-- Add 61 rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (142, 39, 1, N'Store.Id', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (143, 39, 1, N'Store.Title', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (144, 39, 1, N'Store.Description', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (145, 40, 1, N'StoreProduct.StockQuantity', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (146, 40, 2, N'StoreProduct.IsInStock', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (100, 40, 2, N'StoreProduct.StockIsUnknown', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (101, 40, 2, N'StoreProduct.HasLimitedStock', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (102, 40, 2, N'StoreProduct.IsOutOfStock', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (147, 10, 1, N'CustomerInformation.FirstName', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (148, 10, 1, N'CustomerInformation.LastName', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (149, 10, 1, N'CustomerInformation.CivicNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (150, 10, 1, N'CustomerInformation.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (151, 10, 1, N'CustomerInformation.CellPhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (152, 10, 1, N'CustomerInformation.CreatedDate', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (153, 10, 1, N'CustomerInformation.Email', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (154, 10, 1, N'Customer.Id', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (155, 10, 1, N'Customer.ErpId', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (156, 10, 2, N'Customer.HasCustomerInformation', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (157, 10, 2, N'CustomerInformation.HasDeliveryAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (158, 10, 2, N'CustomerInformation.HasBillingAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (159, 10, 1, N'CustomerInformation.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (160, 10, 1, N'CustomerInformation.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (161, 10, 1, N'CustomerInformation.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (162, 10, 1, N'CustomerInformation.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (163, 10, 1, N'CustomerInformation.DeliveryAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (164, 10, 1, N'CustomerInformation.DeliveryAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (165, 10, 1, N'CustomerInformation.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (166, 10, 1, N'CustomerInformation.BillingAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (167, 10, 1, N'CustomerInformation.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (168, 10, 1, N'CustomerInformation.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (169, 10, 1, N'CustomerInformation.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (170, 10, 1, N'CustomerInformation.BillingAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (171, 10, 1, N'CustomerInformation.BillingAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (172, 10, 1, N'CustomerInformation.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (173, 10, 1, N'User.CreatedDate', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (174, 6, 1, N'Order.IP', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (175, 7, 1, N'Order.FreightCost', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (176, 7, 1, N'Order.FreightCostVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (177, 7, 1, N'Order.ItemPriceSummaryIncludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (178, 7, 1, N'Order.ItemPriceSummaryExcludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (179, 7, 1, N'Order.TotalPriceIncludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (180, 7, 1, N'Order.TotalPriceExcludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (181, 7, 1, N'Order.TotalVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (182, 7, 1, N'Order.TotalQuantity', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (183, 7, 1, N'Order.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (184, 7, 1, N'Order.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (185, 7, 1, N'Order.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (186, 7, 1, N'Order.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (187, 7, 1, N'Order.DeliveryAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (188, 7, 1, N'Order.DeliveryAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (189, 7, 1, N'Order.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (190, 7, 1, N'Order.BillingAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (191, 7, 1, N'Order.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (192, 7, 1, N'Order.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (193, 7, 1, N'Order.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (194, 7, 1, N'Order.BillingAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (195, 7, 1, N'Order.BillingAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (196, 7, 1, N'Order.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (197, 8, 1, N'OrderItem.PriceSummary', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (198, 10, 2, N'CustomerInformation.DeliveryAddressIsEqualToBillingAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (199, 7, 2, N'Order.DeliveryAddressIsEqualToBillingAddress', N'')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF



-- Add 33 rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (92, 7)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (92, 10)


-- Add 76 rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (295, 139, 1, N'Order.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (296, 139, 1, N'Order.Number', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (297, 139, 1, N'Order.Email', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (298, 139, 1, N'Order.CreatedDate', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (299, 139, 1, N'Order.Url', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (300, 139, 1, N'Order.IP', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (301, 139, 1, N'Order.FreightCost', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (302, 139, 1, N'Order.FreightCostVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (303, 139, 1, N'Order.ItemPriceSummaryIncludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (304, 139, 1, N'Order.ItemPriceSummaryExcludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (305, 139, 1, N'Order.TotalPriceIncludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (306, 139, 1, N'Order.TotalPriceExcludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (307, 139, 1, N'Order.TotalVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (308, 139, 1, N'Order.TotalQuantity', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (309, 139, 1, N'Order.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (310, 139, 1, N'Order.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (311, 139, 1, N'Order.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (312, 139, 1, N'Order.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (313, 139, 1, N'Order.DeliveryAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (314, 139, 1, N'Order.DeliveryAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (315, 139, 1, N'Order.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (316, 139, 1, N'Order.BillingAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (317, 139, 1, N'Order.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (318, 139, 1, N'Order.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (319, 139, 1, N'Order.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (320, 139, 1, N'Order.BillingAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (321, 139, 1, N'Order.BillingAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (322, 139, 1, N'Order.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (323, 139, 2, N'Order.DeliveryAddressIsEqualToBillingAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (324, 139, 2, N'Customer.HasUser', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (325, 139, 1, N'User.UserName', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (326, 139, 1, N'CustomerInformation.FirstName', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (327, 139, 1, N'CustomerInformation.LastName', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (328, 139, 1, N'CustomerInformation.CivicNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (329, 139, 1, N'CustomerInformation.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (330, 139, 1, N'CustomerInformation.CellPhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (331, 139, 1, N'CustomerInformation.CreatedDate', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (332, 139, 1, N'CustomerInformation.Email', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (333, 139, 1, N'Customer.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (334, 139, 1, N'Customer.ErpId', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (335, 139, 2, N'Customer.HasCustomerInformation', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (336, 139, 2, N'CustomerInformation.HasDeliveryAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (337, 139, 2, N'CustomerInformation.HasBillingAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (338, 139, 1, N'CustomerInformation.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (339, 139, 1, N'CustomerInformation.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (340, 139, 1, N'CustomerInformation.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (341, 139, 1, N'CustomerInformation.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (342, 139, 1, N'CustomerInformation.DeliveryAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (343, 139, 1, N'CustomerInformation.DeliveryAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (344, 139, 1, N'CustomerInformation.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (345, 139, 1, N'CustomerInformation.BillingAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (346, 139, 1, N'CustomerInformation.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (347, 139, 1, N'CustomerInformation.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (348, 139, 1, N'CustomerInformation.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (349, 139, 1, N'CustomerInformation.BillingAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (350, 139, 1, N'CustomerInformation.BillingAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (351, 139, 1, N'CustomerInformation.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (352, 139, 1, N'User.CreatedDate', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (353, 139, 2, N'CustomerInformation.DeliveryAddressIsEqualToBillingAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (354, 140, 1, N'OrderItem.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (355, 140, 1, N'OrderItem.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (356, 140, 1, N'OrderItem.Price', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (357, 140, 1, N'OrderItem.Vat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (358, 140, 1, N'OrderItem.Quantity', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (359, 140, 1, N'OrderItem.PriceSummary', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF







-- Add constraints to [template].[tTemplateFragment]
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [FK_tTemplateFragment_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [FK_tTemplateFragment_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])

-- Add constraints to [template].[tTemplate]
ALTER TABLE [template].[tTemplate] ADD CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])

-- Add constraints to [template].[tAlias]
ALTER TABLE [template].[tAlias] ADD CONSTRAINT [FK_tAlias_tAliasFolder] FOREIGN KEY ([AliasFolderId]) REFERENCES [template].[tAliasFolder] ([AliasFolderId])
ALTER TABLE [template].[tAlias] ADD CONSTRAINT [FK_tAlias_tAliasType] FOREIGN KEY ([AliasTypeId]) REFERENCES [template].[tAliasType] ([AliasTypeId])

-- Add constraint FK_tAliasTranslation_tAlias to [template].[tAliasTranslation]
ALTER TABLE [template].[tAliasTranslation] ADD CONSTRAINT [FK_tAliasTranslation_tAlias] FOREIGN KEY ([AliasId]) REFERENCES [template].[tAlias] ([AliasId])

-- Add constraints to [template].[tAliasFolder]
ALTER TABLE [template].[tAliasFolder] ADD CONSTRAINT [FK_tAliasFolder_tAliasFolder] FOREIGN KEY ([ParentAliasFolderId]) REFERENCES [template].[tAliasFolder] ([AliasFolderId])
COMMIT TRANSACTION
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [security].[tRolePrivilege]'
GO
ALTER TABLE [security].[tRolePrivilege] DROP
CONSTRAINT [FK_tRolePrivilege_tPrivilege],
CONSTRAINT [FK_tRolePrivilege_tRole]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [security].[tUserRole]'
GO
ALTER TABLE [security].[tUserRole] DROP
CONSTRAINT [FK_tUserRole_tRole],
CONSTRAINT [FK_tUserRole_tSystemUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [sitestructure].[tContentPage]'
GO
ALTER TABLE [sitestructure].[tContentPage] DROP
CONSTRAINT [FK_tContentPage_tContentNode]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] DROP
CONSTRAINT [FK_tTemplate_tModel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tThemeModel]'
GO
ALTER TABLE [template].[tThemeModel] DROP
CONSTRAINT [FK_tThemeModel_tModel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [security].[tRolePrivilege]'
GO
ALTER TABLE [security].[tRolePrivilege] ADD
CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId]),
CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [security].[tUserRole]'
GO
ALTER TABLE [security].[tUserRole] ADD
CONSTRAINT [FK_tUserRole_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId]),
CONSTRAINT [FK_tUserRole_tSystemUser] FOREIGN KEY ([SystemUserId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [sitestructure].[tContentPage]'
GO
ALTER TABLE [sitestructure].[tContentPage] WITH NOCHECK ADD 
CONSTRAINT [FK_tContentPage_tContentNode] FOREIGN KEY ([ContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] ADD
CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tThemeModel]'
GO
ALTER TABLE [template].[tThemeModel] ADD
CONSTRAINT [FK_tThemeModel_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tAliasTranslation]'
GO
ALTER TABLE [template].[tAliasTranslation] DROP
CONSTRAINT [FK_tAliasTranslation_tAlias]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] DROP
CONSTRAINT [FK_tTemplate_tModel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tAliasTranslation]'
GO
ALTER TABLE [template].[tAliasTranslation] ADD
CONSTRAINT [FK_tAliasTranslation_tAlias] FOREIGN KEY ([AliasId]) REFERENCES [template].[tAlias] ([AliasId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] ADD
CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


declare @sql as varchar(max)
set @sql = ''

select
@sql = @sql + '
declare @'+table_name+'Seed int
select @'+table_name+'Seed = max(['+column_name+']) from ['+table_schema+'].['+table_name+']

if (@'+table_name+'Seed IS NULL OR @'+table_name+'Seed < 1000000) set @'+table_name+'Seed = 1000000

DBCC CHECKIDENT (''['+table_schema+'].['+table_name+']'', RESEED, @'+table_name+'Seed)

'

from information_schema.columns
where columnproperty(object_id(table_schema + '.' + table_name), column_name, 'IsIdentity') = 1
and table_name like 't%'
order by table_schema, table_name

--print @sql
exec (@sql)




GO
/****** Object:  StoredProcedure [order].[pCartItemSave]    Script Date: 12/29/2010 17:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [order].[pCartItemSave]
	@CartItemId int,
    @CartId INT,
    @ProductId INT,
    @Quantity INT,
    @CreatedDate datetime
AS 
BEGIN
      SET NOCOUNT ON

      UPDATE
            [order].tCartItem
      SET
            Quantity = @Quantity
      WHERE
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [order].tCartItem
            (
                  CartId,
                  ProductId,
                  Quantity,
                  CreatedDate            
            )
            VALUES
            (
                  @CartId,
                  @ProductId,
                  @Quantity,
                  @CreatedDate
            )
      set @CartItemId = scope_identity()
	end
	return @CartItemId
END
GO

/****** Object:  StoredProcedure [lekmer].[pLekmerCartItemSave]    Script Date: 12/29/2010 17:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [lekmer].[pLekmerCartItemSave]
	@CartItemId int,
	@ProductId int,
	@SizeId int,
	@ErpId varchar(50)
as
begin
      SET NOCOUNT ON

      UPDATE
            lekmer.tLekmerCartItem
      SET
            SizeId = @SizeId,
            ErpId = @ErpId
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO lekmer.tLekmerCartItem
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  ErpId
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @ErpId
            )
      END
end
GO
/****** Object:  StoredProcedure [lekmer].[pLekmerCartItemDelete]    Script Date: 12/29/2010 17:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [lekmer].[pLekmerCartItemDelete]
	@CartItemId int
as
begin
	delete
		lekmer.tLekmerCartItem
	where
		 CartItemId = @CartItemId
end
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		

GO


exec dev.pRefreshAllViews