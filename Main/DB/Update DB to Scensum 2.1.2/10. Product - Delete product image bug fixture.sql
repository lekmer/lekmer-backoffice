alter procedure [product].[pProductImageDeleteAllByProduct]
@ProductId INT
AS
BEGIN
	DELETE
		[product].[tProductImage]
	WHERE
		ProductId = @ProductId
END
GO