SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [product].[tProductSeoSetting]'
GO
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSeoSettingSave]'
GO




/*
*****************  Version 1  *****************
User: Roman G.		Date: 15.09.2008		Time: 17:00
Description:
      Save/Update SEOSettings for Product.
/***********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 14:35
Description: delete try - cath  block and error logging 
**********************************************************/
*/
ALTER PROCEDURE [product].[pProductSeoSettingSave]
	@ProductId		INT,
	@Title			NVARCHAR(250),
	@Description	NVARCHAR(250),
	@Keywords		NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[product].[tProductSeoSetting]
	SET
		[Title] = @Title,
		[Description] = @Description,
		[Keywords] = @Keywords
	WHERE
		[ProductId] = @ProductId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [product].[tProductSeoSetting]
		(				
			[ProductId],
			[Title],
			[Description],
			[Keywords]
		)
		VALUES
		(
			@ProductId,
			@Title,
			@Description,
			@Keywords
		)
	END
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tProductSeoSettingTranslation]'
GO
ALTER TABLE [product].[tProductSeoSettingTranslation] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [product].[tProductSeoSettingTranslation] ALTER COLUMN [Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO