SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

----dlelete keys from [lekmer].[tLekmerCartItem]
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP
CONSTRAINT [FK_tLekmerCartItem_tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT [PK_tLekmerCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

---- end dlelete keys from [lekmer].[tLekmerCartItem]


----dlelete keys from [lekmer].[order].[tCartItem]'
GO
PRINT N'Dropping foreign keys from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP
CONSTRAINT [FK_tCartItem_tCart],
CONSTRAINT [FK_tCartItem_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP CONSTRAINT [PK_tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

----end dlelete keys from [lekmer].[order].[tCartItem]'

GO
PRINT N'Altering [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD
[CartItemId] [int] NOT NULL IDENTITY(1, 1)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
PRINT N'Creating primary key [PK_tCartItem] on [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [PK_tCartItem] PRIMARY KEY CLUSTERED  ([CartItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

---- Altering[lekmer].tLekmerCartItem]
PRINT N'Altering [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
[CartItemId] [int]  NULL 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
PRINT N'Insert date from [order].tCartItem to primary key [PK_tLekmerCartItem] on [lekmer].[tLekmerCartItem]'
GO
UPDATE LCI 
SET LCI.CartItemId = CI.[CartItemId]
FROM [order].[tCartItem] AS CI,[lekmer].[tLekmerCartItem] AS LCI
WHERE LCI.CartId = CI.[CartId] AND LCI.ProductId = CI.[ProductId]

GO
ALTER TABLE [lekmer].[tLekmerCartItem] ALTER COLUMN 
[CartItemId] [int] NOT NULL 

GO
PRINT N'Creating primary key [PK_tLekmerCartItem] on [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [PK_tLekmerCartItem] PRIMARY KEY CLUSTERED  ([CartItemId])
GO
PRINT N'Delete CartId  column from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP COLUMN   [CartId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

----end Altering[lekmer].tLekmerCartItem]

PRINT N'Altering [order].[pCartItemSave]'
GO
ALTER PROCEDURE [order].[pCartItemSave]
	@CartItemId int,
    @CartId INT,
    @ProductId INT,
    @Quantity INT,
    @CreatedDate datetime
AS 
BEGIN
      SET NOCOUNT ON

      UPDATE
            [order].tCartItem
      SET
            Quantity = @Quantity
      WHERE
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [order].tCartItem
            (
                  CartId,
                  ProductId,
                  Quantity,
                  CreatedDate            
            )
            VALUES
            (
                  @CartId,
                  @ProductId,
                  @Quantity,
                  @CreatedDate
            )
      END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pCartItemDelete]'
GO

ALTER PROCEDURE [order].[pCartItemDelete] 
	@CartItemId int
as
begin
	set nocount on
	
	DELETE FROM 
		[order].tCartItem
	WHERE
		CartItemId = @CartItemId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCartItem]'
GO

ALTER VIEW [order].[vCartItem]
AS
SELECT 
		C.CartItemId AS 'CartItem.CartItemId',
		C.CartId AS 'CartItem.CartId',
		C.Quantity AS 'CartItem.Quantity', 
		C.CreatedDate AS 'CartItem.CreatedDate',
		P.*		
from
	[order].tCartItem C WITH (NOLOCK)
	inner join product.vCustomProduct P on C.ProductId = P.[Product.Id]
GO
PRINT N'Altering [order].[vCustomCartItem]'
GO

/****** Object:  View [order].[vCustomCartItem]    Script Date: 12/29/2010 17:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER view [order].[vCustomCartItem]
as
	select
		c.*,
		l.SizeId as [LekmerCartItem.SizeId],
		l.ErpId as [LekmerCartItem.ErpId]
	from
		[order].[vCartItem] c
		inner join [lekmer].tLekmerCartItem l on c.[CartItem.CartItemId] = l.CartItemId
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD
CONSTRAINT [FK_tCartItem_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId]),
CONSTRAINT [FK_tCartItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
PRINT N'Adding foreign keys to [lekmer].[tLekmerCartItem]'
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
CONSTRAINT [FK_tLekmerCartItem_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId]),
--CONSTRAINT [FK_tLekmerCartItem_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId]),
--CONSTRAINT [FK_tLekmerCartItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO