

GO
/****** Object:  StoredProcedure [order].[pCartItemSave]    Script Date: 12/29/2010 17:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [order].[pCartItemSave]
	@CartItemId int,
    @CartId INT,
    @ProductId INT,
    @Quantity INT,
    @CreatedDate datetime
AS 
BEGIN
      SET NOCOUNT ON

      UPDATE
            [order].tCartItem
      SET
            Quantity = @Quantity
      WHERE
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [order].tCartItem
            (
                  CartId,
                  ProductId,
                  Quantity,
                  CreatedDate            
            )
            VALUES
            (
                  @CartId,
                  @ProductId,
                  @Quantity,
                  @CreatedDate
            )
      set @CartItemId = scope_identity()
	end
	return @CartItemId
END
GO

/****** Object:  StoredProcedure [lekmer].[pLekmerCartItemSave]    Script Date: 12/29/2010 17:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [lekmer].[pLekmerCartItemSave]
	@CartItemId int,
	@ProductId int,
	@SizeId int,
	@ErpId varchar(50)
as
begin
      SET NOCOUNT ON

      UPDATE
            lekmer.tLekmerCartItem
      SET
            SizeId = @SizeId,
            ErpId = @ErpId
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO lekmer.tLekmerCartItem
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  ErpId
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @ErpId
            )
      END
end
GO
/****** Object:  StoredProcedure [lekmer].[pLekmerCartItemDelete]    Script Date: 12/29/2010 17:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [lekmer].[pLekmerCartItemDelete]
	@CartItemId int
as
begin
	delete
		lekmer.tLekmerCartItem
	where
		 CartItemId = @CartItemId
end
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		

GO