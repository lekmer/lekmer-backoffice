SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [statistics].[pOrderGetSummary]'
GO
ALTER procedure [statistics].[pOrderGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@Alternate		bit,
	@StatusIds		varchar(max)
AS 
BEGIN	
	SELECT 
		isnull(count([OrderId]), 0)
	FROM
		[order].[tOrder] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [UsedAlternate] = @Alternate
		AND [CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pSaleGetSummary]'
GO
ALTER procedure [statistics].[pSaleGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN	
	SELECT 
		isnull(sum(oi.[ActualPriceIncludingVat] * oi.[Quantity]), 0)
	FROM
		[order].tOrderItem oi WITH(NOLOCK)
		INNER JOIN [order].tOrder o WITH(NOLOCK) ON oi.OrderId = o.OrderId
	WHERE
		o.[ChannelId] = @ChannelId
		AND o.[CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pOrderGetGroupedData]'
GO
ALTER procedure [statistics].[pOrderGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		
	DECLARE @tOrder TABLE ([DateTime] smalldatetime)
	INSERT INTO 
		@tOrder
	(
		[DateTime]
	)
	SELECT
		[statistics].[fnGetDateTimeKeyDirectly]([CreatedDate], @Mode) AS 'DateTime'
	FROM
		[order].[tOrder] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
			
	SELECT 
		d.[DateTime], 
		isnull(count(o.DateTime), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tOrder o ON o.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pSaleGetGroupedData]'
GO
ALTER procedure [statistics].[pSaleGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		 
	DECLARE @tSale TABLE ([DateTime] smalldatetime, Amount decimal(16, 2))
	INSERT INTO 
		@tSale
	(
		[DateTime],
		Amount
	)
	SELECT
		[statistics].[fnGetDateTimeKeyDirectly](o.[CreatedDate], @Mode) AS 'DateTime',
		oi.[ActualPriceIncludingVat] * oi.[Quantity] AS 'Amount'
	FROM
		[order].tOrderItem oi WITH(NOLOCK)
		INNER JOIN [order].tOrder o WITH(NOLOCK) ON oi.OrderId = o.OrderId
	WHERE
		o.[ChannelId] = @ChannelId
		AND o.[CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
			
	SELECT 
		d.[DateTime], 
		isnull(sum(s.Amount), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tSale s ON s.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pTopSellerGetAll]'
GO
ALTER procedure [statistics].[pTopSellerGetAll]
	@ChannelId int,
	@DateTimeFrom datetime,
	@DateTimeTo datetime,
	@TopCount int,
	@StatusIds		varchar(max)
as
begin
	declare @ProductIds table(ProductId int, Quantity int)
	
	insert
		@ProductIds
	(
		ProductId,
		Quantity
	)
	select top(@TopCount)
		p.ProductId,
		sum(oi.Quantity)
	from
		product.tProduct p
		inner join [order].tOrderItemProduct oip WITH(NOLOCK) on p.ProductId = oip.ProductId
		inner join [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		inner join [order].tOrder o WITH(NOLOCK) on oi.OrderId = o.OrderId
	where
		o.ChannelId = @ChannelId
		and o.CreatedDate between isnull(@DateTimeFrom, o.CreatedDate) and isnull(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
	group by
		p.ProductId
	order by
		sum(oi.Quantity) desc

	select
		p.*,
		pli.*,
		i.Quantity as 'TopSeller.Quantity'
	from
		@ProductIds as i 
		inner join [product].[vCustomProduct] as p on p.[Product.Id] = i.ProductId
		inner join [product].[vCustomPriceListItem] as pli
			on pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[Product.ChannelId] = @ChannelId
	order by
		i.Quantity DESC
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO