CREATE PROCEDURE [order].[pPaymentTypeGetAllSecure]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	ORDER BY
		[PaymentType.Title]
END
GO