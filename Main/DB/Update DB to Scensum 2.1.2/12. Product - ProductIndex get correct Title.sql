alter PROCEDURE [product].[pProductIndexItemGetAll]
	@ChannelId int,
	@Page int = NULL,
	@PageSize int
AS
BEGIN

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)

SET @sqlFilter = '
	(
		SELECT 
			ROW_NUMBER() OVER (ORDER BY [Product.Id]) AS Number,
			[Product.Id], 
			(CASE WHEN (([Product.WebShopTitle] IS NOT NULL) AND ([Product.WebShopTitle] <> '''')) THEN [Product.WebShopTitle] ELSE [Product.Title] END) as [Product.Title], 
			[Product.Description], 
			[Product.ShortDescription],
			[Lekmer.LekmerErpId],
			0 as [Product.OrderCount]
		FROM 
			product.vCustomProductView
		WHERE
			[Product.ChannelId] = ' +  CAST(@ChannelId AS varchar(10)) + '
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END	

	EXEC (@sqlCount)
	EXEC (@sql)

END
GO