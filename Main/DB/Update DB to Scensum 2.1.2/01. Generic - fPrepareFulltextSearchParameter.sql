create function generic.fPrepareFulltextSearchParameter
(
	@Value VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	SET @Value = REPLACE(@Value, '"', '""')
	SET @Value = REPLACE(@Value, ' ', '*" and "')
	SET @Value = '"' + @Value + '*"'
	RETURN @Value
END
GO