SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [security].[tRolePrivilege]'
GO
ALTER TABLE [security].[tRolePrivilege] DROP
CONSTRAINT [FK_tRolePrivilege_tPrivilege],
CONSTRAINT [FK_tRolePrivilege_tRole]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [security].[tUserRole]'
GO
ALTER TABLE [security].[tUserRole] DROP
CONSTRAINT [FK_tUserRole_tRole],
CONSTRAINT [FK_tUserRole_tSystemUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [sitestructure].[tContentPage]'
GO
ALTER TABLE [sitestructure].[tContentPage] DROP
CONSTRAINT [FK_tContentPage_tContentNode]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] DROP
CONSTRAINT [FK_tTemplate_tModel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tThemeModel]'
GO
ALTER TABLE [template].[tThemeModel] DROP
CONSTRAINT [FK_tThemeModel_tModel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [security].[tRolePrivilege]'
GO
ALTER TABLE [security].[tRolePrivilege] ADD
CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId]),
CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [security].[tUserRole]'
GO
ALTER TABLE [security].[tUserRole] ADD
CONSTRAINT [FK_tUserRole_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId]),
CONSTRAINT [FK_tUserRole_tSystemUser] FOREIGN KEY ([SystemUserId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [sitestructure].[tContentPage]'
GO
ALTER TABLE [sitestructure].[tContentPage] WITH NOCHECK ADD 
CONSTRAINT [FK_tContentPage_tContentNode] FOREIGN KEY ([ContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] ADD
CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tThemeModel]'
GO
ALTER TABLE [template].[tThemeModel] ADD
CONSTRAINT [FK_tThemeModel_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tAliasTranslation]'
GO
ALTER TABLE [template].[tAliasTranslation] DROP
CONSTRAINT [FK_tAliasTranslation_tAlias]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] DROP
CONSTRAINT [FK_tTemplate_tModel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tAliasTranslation]'
GO
ALTER TABLE [template].[tAliasTranslation] ADD
CONSTRAINT [FK_tAliasTranslation_tAlias] FOREIGN KEY ([AliasId]) REFERENCES [template].[tAlias] ([AliasId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [template].[tTemplate]'
GO
ALTER TABLE [template].[tTemplate] ADD
CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO