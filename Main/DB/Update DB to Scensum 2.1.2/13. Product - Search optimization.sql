EXEC sp_fulltext_database 'enable'
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [product].[pProductSearch]'
GO

ALTER PROCEDURE [product].[pProductSearch]
	@CategoryId int,
	@Title nvarchar(256),
	@PriceFrom DECIMAL(16,2),
	@PriceTo DECIMAL(16,2),
	@ErpId varchar(50),
	@StatusId int,
	@EanCode varchar(20),
	@ChannelId	int,
	@Page int = NULL,
	@PageSize int,
	@SortBy varchar(20) = NULL,
	@SortDescending bit = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sqlGeneral nvarchar(max)
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	SET @sqlFragment = '	
				SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					p.ProductId,
					pli.*
				FROM 
					product.tProduct AS p
					-- get price for current channel
					LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[ProductId] 
					LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
					LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[ProductId]
						AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', p.[ProductId], PMC.PriceListRegistryId, NULL)
				WHERE
					p.IsDeleted = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND p.[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS(p.*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND p.[ErpId] = @ErpId' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND p.[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@EanCode IS NOT NULL) THEN + '
					AND p.[EanCode] = @EanCode' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN product.vCustomProductSecure vp
				on p.ProductId = vp.[Product.Id]'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(50),
			@StatusId int,
			@EanCode varchar(20)',
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode

	EXEC sp_executesql @sql, 
		N'	@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(50),
			@StatusId int,
			@EanCode varchar(20)',
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSearchAdvanced]'
GO

ALTER PROCEDURE [product].[pProductSearchAdvanced]
	@ChannelId	INT,
	@SearchCriteriaInclusiveList XML,
	@SearchCriteriaExclusiveList XML,
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = 'ErpId',
	@SortDescending BIT = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @ParsedCategoryId INT
	DECLARE @ParsedTitle NVARCHAR(256)
	DECLARE @ParsedPriceFrom DECIMAL(16,2)
	DECLARE @ParsedPriceTo DECIMAL(16,2)
	DECLARE @ParsedErpId VARCHAR(50)
	DECLARE @ParsedStatusId INT
	DECLARE @ParsedEanCode VARCHAR(20)
	
	DECLARE CriteriaInclusiveCursor CURSOR
		FOR SELECT  T.a.value('(@categoryId)', 'INT') AS 'categoryId',
					T.a.value('(@title)', 'NVARCHAR(256)') AS 'title',
					T.a.value('(@priceFrom)', 'DECIMAL(16,2)') AS 'priceFrom',
					T.a.value('(@priceTo)', 'DECIMAL(16,2)') AS 'priceTo',
					T.a.value('(@erpId)', 'VARCHAR(50)') AS 'erpId',
					T.a.value('(@statusId)', 'INT') AS 'statusId',
					T.a.value('(@eanCode)', 'VARCHAR(20)') AS 'eanCode'
			FROM    @SearchCriteriaInclusiveList.nodes('criterias/criteria') AS T ( a )
	
	OPEN CriteriaInclusiveCursor
	FETCH NEXT FROM CriteriaInclusiveCursor INTO @ParsedCategoryId, @ParsedTitle, @ParsedPriceFrom, @ParsedPriceTo, @ParsedERPId, @ParsedStatusId, @ParsedEANCode
	CLOSE CriteriaInclusiveCursor
	DEALLOCATE CriteriaInclusiveCursor

	IF ( @@FETCH_STATUS = 0 )
	BEGIN
		exec [product].[pProductSearch]
			@CategoryId = @ParsedCategoryId,
			@Title = @ParsedTitle,
			@PriceFrom = @ParsedPriceFrom,
			@PriceTo = @ParsedPriceTo,
			@ErpId = @ParsedERPId,
			@StatusId  = @ParsedStatusId,
			@EanCode  = @ParsedEANCode,
			@ChannelId	 = @ChannelId,
			@Page = @Page,
			@PageSize  = @PageSize,
			@SortBy = @SortBy,
			@SortDescending = @SortDescending
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Creating full text catalogs'
GO
CREATE FULLTEXT CATALOG [fulltextcatalog_tproduct_default]
ON FILEGROUP [PRIMARY]
--IN PATH 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\FTData'
WITH ACCENT_SENSITIVITY = ON
AUTHORIZATION [dbo]
GO
PRINT N'Adding full text indexing to tables'
GO
CREATE FULLTEXT INDEX ON [product].[tProduct] KEY INDEX [PK_tProduct] ON [fulltextcatalog_tproduct_default]
GO
PRINT N'Adding full text indexing to columns'
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ADD (Title LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ADD (WebShopTitle LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ENABLE
GO