SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pBlockProductFilterTagGetAllByBlock]
	@BlockId int
as
begin
	select
		[BlockProductFilterTag.TagId]
	from
		lekmer.vBlockProductFilterTag
	where
		[BlockProductFilterTag.BlockId] = @BlockId
end
GO
