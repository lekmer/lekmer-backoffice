SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateSearch]
	@SearchString	varchar(MAX)
AS
BEGIN
	SET @SearchString = [generic].[fPrepareSearchParameter](@SearchString)

	select
		t.*
	from
		[template].vCustomTemplate t
	WHERE
		t.[Template.Title] like @SearchString ESCAPE '\' 
		OR t.[Template.Id] IN
		(
			SELECT 
				vf.[TemplateFragment.TemplateId]
			FROM  
				template.vCustomTemplateFragment vf
			WHERE 
				vf.[TemplateFragment.Content] like @SearchString ESCAPE '\'
				OR vf.[TemplateFragment.AlternateContent] like @SearchString ESCAPE '\'
		)
	ORDER BY
		t.[Template.ModelId], t.[Template.Title]
end

GO
