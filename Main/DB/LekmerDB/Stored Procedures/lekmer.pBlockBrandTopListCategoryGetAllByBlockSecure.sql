SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vBlockBrandTopListCategorySecure]
	WHERE
		[BlockBrandTopListCategory.BlockId] = @BlockId
END
GO
