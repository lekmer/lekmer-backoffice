SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeShortcutLinkGetAll]
@ChannelId int
as
begin
	select 
		cns.*
	from 
		[sitestructure].[vCustomContentNodeShortcutLink] as cns
	WHERE cns.[ContentNode.ChannelId] = @ChannelId
end

GO
