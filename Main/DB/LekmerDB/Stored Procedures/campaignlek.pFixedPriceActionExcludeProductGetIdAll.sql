
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[aep].[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
