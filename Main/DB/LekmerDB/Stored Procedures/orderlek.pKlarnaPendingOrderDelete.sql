SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderDelete]
	@KlarnaPendingOrderId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE [orderlek].[tKlarnaPendingOrder]
	WHERE [KlarnaPendingOrderId] = @KlarnaPendingOrderId
END
GO
