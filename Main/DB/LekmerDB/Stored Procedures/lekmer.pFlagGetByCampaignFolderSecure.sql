SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagGetByCampaignFolderSecure]
@FolderId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT f.*
	FROM lekmer.vFlagSecure f inner join lekmer.tCampaignFolderFlag cff on f.[Flag.Id] = cff.FlagId
	WHERE cff.FolderId = @FolderId
END


GO
