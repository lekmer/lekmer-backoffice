
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeProductGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[cip].[ProductId]
	FROM 
		[addon].[tCartContainsConditionIncludeProduct] cip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [cip].[ProductId]
	WHERE 
		[cip].[ConditionId] = @ConditionId
		AND [p].[IsDeleted] = 0
END
GO
