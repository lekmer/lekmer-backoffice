SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pProductTypeSaveByBlock]
	@BlockId INT,
	@ProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductTypeIds, @Delimiter)
	
	DELETE [productlek].[tBlockProductType]
	WHERE
		[BlockId] = @BlockId
	
	INSERT [productlek].[tBlockProductType] ( [BlockId], [ProductTypeId] )
	SELECT
		@BlockId,
		Id
	FROM
		@IdList
END
GO
