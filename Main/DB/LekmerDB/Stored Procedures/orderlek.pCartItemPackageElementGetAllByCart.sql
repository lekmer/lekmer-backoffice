
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCartItemPackageElementGetAllByCart]
	@CartId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT
		[cipe].*
	FROM
		[orderlek].[vCartItemPackageElement] cipe
		INNER JOIN [order].[tCartItem] ci ON ci.[CartItemId] = [cipe].[CartItemPackageElement.CartItemId]
	WHERE
		[ci].[CartId] = @CartId
END
GO
