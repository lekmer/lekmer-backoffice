SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [lekmer].[pBlockImageRotatorGetById]
	@BlockId	int,
	@LanguageId	int
as	
begin
	set nocount on
	select
		BIR.*,
		b.*
	from
		[lekmer].[tBlockImageRotator] as BIR
		inner join [sitestructure].[vCustomBlock] as b on BIR.[BlockId] = b.[Block.BlockId]
	where
		BIR.[BlockId] = @BlockId
		and b.[Block.LanguageId] = @LanguageId 
end
GO
