SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dev].[pCustomViewCreateAll]
as
begin
	declare @scripts TABLE(script nvarchar(max))

	insert
		@scripts
	(
		script
	)
	select
		'
create view [' + TABLE_SCHEMA + '].[vCustom' + substring(TABLE_NAME, 2, LEN(TABLE_NAME)-1) + ']
as
	select
		*
	from
		[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'
	from
		INFORMATION_SCHEMA.Views
		
	declare scripts_cursor cursor forward_only fast_forward for select script from @scripts
	declare @create_script nvarchar(max)
	open scripts_cursor
	fetch next from scripts_cursor INTO @create_script
	while @@fetch_status = 0
	begin
		exec(@create_script)
		fetch next from scripts_cursor INTO @create_script
	end
	close scripts_cursor
	deallocate scripts_cursor
end
GO
