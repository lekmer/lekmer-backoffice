SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Mykhaylo Artymyshyn	Date: 13.08.2009	Time: 12:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentNodeTypeGetAll]
as
begin
	select 
		cnt.*
	from 
		[sitestructure].[vCustomContentNodeType] AS cnt
end

GO
