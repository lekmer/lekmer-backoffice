
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pProductActionTargetProductTypeGetAllByCampaign]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[at].*
	FROM
		[campaignlek].[vProductActionTargetProductType] at
		INNER JOIN [campaign].[tProductAction] pa ON [pa].[ProductActionId] = [at].[ProductActionTargetProductType.ProductActionId]
	WHERE
		[pa].[CampaignId] = @CampaignId
END
GO
