SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[bpil].*,
		[b].*
	FROM 
		[productlek].[vBlockProductImageList] bpil
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bpil].[BlockProductImageList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bpil].[BlockProductImageList.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
