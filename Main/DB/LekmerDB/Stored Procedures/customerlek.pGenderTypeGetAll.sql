SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customerlek].[pGenderTypeGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[customerlek].[vGenderType]
END
GO
