SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionIncludeCategory]
	WHERE
		ProductActionId = @ProductActionId
END

GO
