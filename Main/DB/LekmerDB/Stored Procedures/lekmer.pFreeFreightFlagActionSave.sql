SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pFreeFreightFlagActionSave]
	@ActionId int
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM [lekmer].[tFreeFreightFlagAction] WHERE ProductActionId = @ActionId)
			RETURN
		
		INSERT [lekmer].[tFreeFreightFlagAction]
		( 
			ProductActionId
		)
		VALUES 
		(
			@ActionId
		)
END
GO
