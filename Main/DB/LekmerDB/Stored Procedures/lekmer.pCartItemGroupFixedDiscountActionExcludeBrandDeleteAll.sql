SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END



GO
