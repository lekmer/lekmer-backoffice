
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistorySave]
	@ProductId					INT,
	@LanguageId					INT,
	@UrlTitleOld				NVARCHAR(256),
	@HistoryLifeIntervalTypeId	INT
AS 
BEGIN 
	INSERT INTO lekmer.tProductUrlHistory (
		ProductId,
		LanguageId,
		UrlTitleOld,
		HistoryLifeIntervalTypeId
	)
	VALUES (
		@ProductId,
		@LanguageId,
		@UrlTitleOld,
		@HistoryLifeIntervalTypeId
	)
	
	RETURN @@IDENTITY
END 
GO
