SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pAddressTypeGetByCommonName]
@CommonName varchar(100)
as	
begin
set nocount on
	select
		*
	from
		customer.vCustomAddressType
	where 
		[AddressType.CommonName] = @CommonName
end

GO
