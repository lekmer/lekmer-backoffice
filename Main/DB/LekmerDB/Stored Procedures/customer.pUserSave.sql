
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pUserSave]
	@CustomerId			INT,
	@CustomerRegistryId INT,
	@UserName			NVARCHAR(320),
	@Password			NVARCHAR(50),
	@PasswordSalt		NVARCHAR(50),
	@CreatedDate		DATETIME
AS	
BEGIN
	SET NOCOUNT ON	
	UPDATE
		[customer].[tCustomerUser]
	SET    
		[UserName]				= @UserName,
		[CustomerRegistryId]	= @CustomerRegistryId, 
		[Password]				= @Password,
		[PasswordSalt]			= @PasswordSalt
	WHERE 
		[CustomerId]			= @CustomerId
		
	IF @@ROWCOUNT = 0	
	BEGIN
		INSERT INTO	[customer].[tCustomerUser] (
			[CustomerId],
			[CustomerRegistryId],
			[UserName],
			[Password],
			[PasswordSalt],
			[CreatedDate]
		)
		VALUES (
			@CustomerId,
			@CustomerRegistryId,
			@UserName,
			@Password,
			@PasswordSalt,
			@CreatedDate
		)
	END
	RETURN @CustomerId
END
GO
