SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderCommentDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].[tOrderComment]
	WHERE [OrderId] = @OrderId
END
GO
