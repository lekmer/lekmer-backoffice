SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryGetByPageId]
	@ContentPageId	INT
AS 
BEGIN 
	SELECT 
		CCPUH.*
	FROM 
		[lekmer].[vCustomContentPageUrlHistory] CCPUH
	WHERE
		CCPUH.[ContentPageUrlHistory.ContentPageId] = @ContentPageId
END
GO
