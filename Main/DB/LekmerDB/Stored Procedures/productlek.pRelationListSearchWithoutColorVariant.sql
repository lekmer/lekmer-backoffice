SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pRelationListSearchWithoutColorVariant]
	@Title				NVARCHAR(50),
	@RelationListTypeId INT,
	@Page				INT = NULL,
	@PageSize			INT,
	@SortBy				VARCHAR(20) = NULL,
	@SortDescending		BIT = NULL
AS
BEGIN

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @ColorVariantRelationListId NVARCHAR(10)
	SET @ColorVariantRelationListId = (SELECT [RelationListTypeId] FROM [product].[tRelationListType] WHERE [CommonName] = 'ColorVariant')

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY vRL.[' + COALESCE(@SortBy, 'RelationList.Id')+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			vRL.*
		FROM [product].[vCustomRelationList] AS vRL
		WHERE
			vRL.[RelationList.TypeId] != ' + @ColorVariantRelationListId
		
			+ CASE WHEN (@Title IS NOT NULL) THEN + '
			AND vRL.[RelationList.Title] LIKE ''%'' + @Title + ''%''' ELSE '' END 
			
			+ CASE WHEN (@RelationListTypeId IS NOT NULL) THEN + '
			AND vRL.[RelationList.TypeId] = @RelationListTypeId' ELSE '' END

	SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM (' + @sqlFragment + ') AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@Title NVARCHAR(50),
			@RelationListTypeId INT',
			@Title,
			@RelationListTypeId

	EXEC sp_executesql @sql, 
		N'	@Title NVARCHAR(50),
			@RelationListTypeId INT',
			@Title,
			@RelationListTypeId
END
GO
