SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pSupplierSearch]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		[s].*
	FROM 
		[productlek].[vSupplier] s
	WHERE
		[s].[Supplier.Name] LIKE @SearchCriteria ESCAPE '\'
END
GO
