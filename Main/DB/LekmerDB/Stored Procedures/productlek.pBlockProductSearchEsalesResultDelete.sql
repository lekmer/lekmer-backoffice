SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultDelete]
	@BlockId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE
		[productlek].[tBlockProductSearchEsalesResult]
	WHERE
		[BlockId] = @BlockId
END

GO
