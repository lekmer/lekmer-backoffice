SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetById]
	@Id INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		[t].*
	FROM 
		[lekmer].[vTagSecure] t
	WHERE
		[t].[Tag.TagId] = @Id
END
GO
