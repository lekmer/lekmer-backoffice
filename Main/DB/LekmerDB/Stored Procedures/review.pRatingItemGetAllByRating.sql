SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemGetAllByRating]
	@RatingId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ri.*
	FROM
		[review].[vRatingItem] ri
	WHERE
		ri.[RatingItem.RatingId] = @RatingId
		AND
		ri.[RatingItem.Channel.Id] = @ChannelId
END
GO
