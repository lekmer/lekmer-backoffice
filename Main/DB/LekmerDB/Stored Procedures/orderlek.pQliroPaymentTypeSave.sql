SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pQliroPaymentTypeSave]
	@QliroPaymentTypeId INT,
	@ChannelId INT,
	@Code VARCHAR(50),
	@Description NVARCHAR(100),
	@RegistrationFee DECIMAL(16,2),
	@SettlementFee DECIMAL(16,2),
	@InterestRate DECIMAL(16,2),
	@InterestType INT,
	@InterestCalculation INT,
	@NoOfMonths INT,
	@MinPurchaseAmount DECIMAL(16,2)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id INT
	
	SET @Id = @QliroPaymentTypeId
	
	UPDATE
		[orderlek].[tQliroPaymentType]
	SET
		[Description] = @Description,
		[RegistrationFee] = @RegistrationFee,
		[SettlementFee] = @SettlementFee,
		[InterestRate] = @InterestRate,
		[InterestType] = @InterestType,
		[InterestCalculation] = @InterestCalculation,
		[NoOfMonths] = @NoOfMonths,
		[MinPurchaseAmount]= @MinPurchaseAmount
	WHERE
		[ChannelId] = @ChannelId
		AND
		[Code] = @Code
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [orderlek].[tQliroPaymentType]
				( [ChannelId],
				  [Code],
				  [Description],
				  [RegistrationFee],
				  [SettlementFee],
				  [InterestRate],
				  [InterestType],
				  [InterestCalculation],
				  [NoOfMonths],
				  [MinPurchaseAmount]
				)
		VALUES
				( @ChannelId,
				  @Code,
				  @Description,
				  @RegistrationFee,
				  @SettlementFee,
				  @InterestRate,
				  @InterestType,
				  @InterestCalculation,
				  @NoOfMonths,
				  @MinPurchaseAmount
				)

		SET @Id = SCOPE_IDENTITY()
	END

	RETURN @Id
END
GO
