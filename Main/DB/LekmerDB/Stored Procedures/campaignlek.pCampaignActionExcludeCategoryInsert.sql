
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryInsert]
	@ConfigId				INT,
	@CategoryId				INT,
	@IncludeSubCategories	BIT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeCategory] (
		ConfigId,
		CategoryId,
		IncludeSubCategories
	)
	VALUES (
		@ConfigId,
		@CategoryId,
		@IncludeSubCategories
	)
END
GO
