SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageGetByCampaignIdSecure]
	@CampaignId INT
AS 
BEGIN

	DECLARE @CampaignLandingpageId INT
	SET @CampaignLandingpageId = (SELECT [CampaignLandingPage.CampaignLandingPageId]
								  FROM [campaignlek].[vCampaignLandingPageSecure] clp
								  WHERE [clp].[CampaignLandingPage.CampaignId] = @CampaignId)
	
	SELECT
		[clp].*
	FROM
		[campaignlek].[vCampaignLandingPageSecure] clp
	WHERE
		[clp].[CampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
		
	SELECT
		[crclp].*
	FROM
		[campaignlek].[vCampaignRegistryCampaignLandingPage] crclp
	WHERE
		[crclp].[CampaignRegistryCampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
	
END
GO
