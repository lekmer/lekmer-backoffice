SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pPrivilegeGetAllByRole]
	@RoleId		int
as
begin
	set nocount on

	select
		P.*
	from
		[security].[vCustomPrivilege] as P
		inner join [security].[tRolePrivilege] as R on R.[PrivilegeId] = P.[Privilege.Id]
	where
		R.[RoleId] = @RoleId
	order by
		P.[Privilege.CommonName]
end

GO
