SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemDiscountCartActionCurrency]
	WHERE 
		[CartItemDiscountCartActionCurrency.CartActionId] = @CartActionId
END
GO
