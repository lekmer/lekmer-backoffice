
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_UpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 

	DECLARE 
	@NewFromDate DATETIME,
	@NewToDate DATETIME

	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

	--update new products depending on stock 
	UPDATE 
		lp
	SET 
		lp.IsNewFrom = @NewFromDate,
		lp.IsNewTo  = @NewToDate,
		IsNew = 0
	FROM 
		[integration].tempProduct tp
		INNER JOIN [lekmer].tLekmerProduct lp ON lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		INNER JOIN product.tProduct p ON p.ProductId = lp.ProductId
	WHERE 
		SUBSTRING(tp.HYarticleId, 3,1) = 1 
		AND (
			lp.IsNew = 1
		)
		AND (
			p.NumberInStock = 0
		)
		AND (
			tp.NoInStock >0
		)

	UPDATE 
		p
	SET 
		EanCode = tp.EanCode, 
		NumberInStock = tp.NoInStock,
		CategoryId = (SELECT categoryId 
					  FROM product.tCategory 
					  WHERE ErpId = 'C_' + ISNULL(tp.ArticleClassId, '') + '-' + ISNULL(tp.ArticleGroupId, '') + '-' + ISNULL(tp.ArticleCodeId, ''))
	FROM 
		[integration].tempProduct tp
		INNER JOIN [lekmer].tLekmerProduct lp ON lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		INNER JOIN product.tProduct p ON p.ProductId = lp.ProductId
		INNER JOIN [product].tPriceListItem pli ON pli.ProductId = lp.ProductId AND pli.PriceListId = 1
	WHERE 
		SUBSTRING(tp.HYarticleId, 3,1) = 1 
		AND (
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		)
		AND (
			tp.ArticleClassId != '***Missing***'
			AND
			tp.ArticleCodeId != '***Missing***'
			AND
			tp.ArticleGroupId != '***Missing***'
		)
END
GO
