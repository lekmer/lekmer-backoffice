SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagInsert]
	@CampaignId INT,
	@ProcessingDate DATETIME = NULL
AS
BEGIN
	INSERT [campaignlek].[tCampaignToTag] (
		[CampaignId],
		[ProcessingDate]
	)
	VALUES (
		@CampaignId,
		@ProcessingDate
	)
END
GO
