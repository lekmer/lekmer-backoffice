SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemPercentageDiscountAction]
	WHERE
		[CartItemPercentageDiscountAction.CartActionId] = @CartActionId
END

GO
