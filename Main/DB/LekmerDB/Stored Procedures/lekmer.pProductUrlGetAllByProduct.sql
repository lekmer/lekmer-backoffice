SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlGetAllByProduct]
	@ProductId	int
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[lekmer].[vProductUrl]
	WHERE
		[ProductUrl.ProductId] = @ProductId
END
GO
