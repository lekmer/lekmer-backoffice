SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [campaignlek].[tGiftCardViaMailInfo]
	WHERE [OrderId] = @OrderId
END
GO
