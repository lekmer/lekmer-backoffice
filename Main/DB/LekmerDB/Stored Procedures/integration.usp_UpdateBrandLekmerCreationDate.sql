
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateBrandLekmerCreationDate]
AS
BEGIN
	
	BEGIN TRY
	BEGIN TRANSACTION
	
	-- Add created date in integration.[tBrandCreation]
	INSERT INTO 
		integration.[tBrandCreation] (BrandId, CreatedDate)
	SELECT
		b.[BrandId],
		GETDATE()
	FROM
		[lekmer].[tBrand] b
	WHERE
		b.[BrandId] NOT IN (SELECT [BrandId] FROM [integration].[tBrandCreation])

	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

END

GO
