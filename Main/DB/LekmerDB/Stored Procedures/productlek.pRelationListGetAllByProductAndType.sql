SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pRelationListGetAllByProductAndType]
	@ProductId	INT,
	@CommonName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		[rl].*
	FROM
		[product].[vCustomRelationList] rl 
		INNER JOIN [product].[tProductRelationList] prl ON [rl].[RelationList.Id] = [prl].[RelationListId]
	WHERE
		[prl].[ProductId] = @ProductId
		AND [rl].[RelationList.CommonName] = @CommonName
END
GO
