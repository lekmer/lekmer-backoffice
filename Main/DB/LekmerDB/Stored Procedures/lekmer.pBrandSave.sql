
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSave]
	@BrandId				INT,
	@Title					VARCHAR(500),
	@MediaId				INT = NULL,
	@Description 			VARCHAR(MAX) = NULL,
	@ExternalUrl 			VARCHAR(500) = NULL,
	@PackageInfo 			NVARCHAR(MAX) = NULL,
	@MonitorThreshold		INT = NULL,
	@MaxQuantityPerOrder	INT = NULL,
	@DeliveryTimeId			INT = NULL,
	@IsOffline				BIT = 0
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tBrand WHERE Title = @Title AND BrandId <> @BrandId)
		RETURN -1

	UPDATE
		[lekmer].[tBrand]
	SET
		[Title] = @Title,	
		[MediaId] = @MediaId,
		[Description] = @Description,
		[ExternalUrl] = @ExternalUrl,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold,
		[MaxQuantityPerOrder] = @MaxQuantityPerOrder,
		[DeliveryTimeId] = @DeliveryTimeId,
		[IsOffline] = @IsOffline
	WHERE
		[BrandId] = @BrandId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBrand]
		( 
			[Title],
			[MediaId],
			[Description],
			[ExternalUrl],
			[PackageInfo],
			[MonitorThreshold],
			[MaxQuantityPerOrder],
			[DeliveryTimeId],
			[IsOffline]
		)
		VALUES
		(
			@Title,
			@MediaId ,
			@Description,
			@ExternalUrl,
			@PackageInfo,
			@MonitorThreshold,
			@MaxQuantityPerOrder,
			@DeliveryTimeId,
			@IsOffline
		)
		SET @BrandId = scope_identity()						
	END 
	
	RETURN @BrandId
END
GO
