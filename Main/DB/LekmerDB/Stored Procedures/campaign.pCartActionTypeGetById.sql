SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartActionTypeGetById]
	@CartActionTypeId	int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCartActionType
	WHERE 
		[CartActionType.Id] = @CartActionTypeId
end

GO
