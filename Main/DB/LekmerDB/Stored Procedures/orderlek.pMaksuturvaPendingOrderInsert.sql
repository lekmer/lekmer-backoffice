SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderInsert]
	@ChannelId		INT,
	@OrderId		INT,
	@StatusId		INT,
	@FirstAttempt	DATETIME = NULL,
	@LastAttempt	DATETIME = NULL,
	@NextAttempt	DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tMaksuturvaPendingOrder] (
		[ChannelId],
		[OrderId],
		[StatusId],
		[FirstAttempt],
		[LastAttempt],
		[NextAttempt]
	)
	VALUES (
		@ChannelId,
		@OrderId,
		@StatusId,
		@FirstAttempt,
		@LastAttempt,
		@NextAttempt
	)
			
	RETURN SCOPE_IDENTITY()
END
GO
