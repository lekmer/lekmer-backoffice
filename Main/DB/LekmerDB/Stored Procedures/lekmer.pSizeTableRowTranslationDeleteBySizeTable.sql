SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowTranslationDeleteBySizeTable]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [strt]
	FROM [lekmer].[tSizeTableRowTranslation] strt
	INNER JOIN [lekmer].[tSizeTableRow] st ON [st].[SizeTableRowId] = [strt].[SizeTableRowId]
	WHERE [st].[SizeTableId] = @SizeTableId
END
GO
