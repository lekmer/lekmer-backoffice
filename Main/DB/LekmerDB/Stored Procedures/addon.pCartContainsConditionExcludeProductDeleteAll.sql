SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionExcludeProductDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].tCartContainsConditionExcludeProduct
	WHERE
		ConditionId = @ConditionId
END 
GO
