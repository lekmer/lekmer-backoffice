SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListGetById]
	@LanguageId INT,
	@BlockId INT
AS
BEGIN
	SELECT 
		btl.*,
		b.*
	FROM 
		[lekmer].[vBlockBrandTopList] AS btl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON btl.[BlockBrandTopList.BlockId] = b.[Block.BlockId]
	WHERE
		btl.[BlockBrandTopList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
