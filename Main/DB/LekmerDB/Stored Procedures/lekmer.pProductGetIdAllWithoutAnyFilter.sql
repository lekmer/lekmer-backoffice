
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilter]
	@Page				INT = NULL,
	@PageSize			INT,
	@SellOnlyInPackage	BIT = NULL
AS
BEGIN
	DECLARE @sqlCount NVARCHAR(MAX)	
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sqlCount = '
		SELECT COUNT(1)
		FROM
			[product].[tProduct] AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		WHERE
			[p].[IsDeleted] = 0'
	IF (@SellOnlyInPackage IS NOT NULL)
	BEGIN
		SET @sqlCount = @sqlCount + ' AND [lp].[SellOnlyInPackage] = ' + CAST(@SellOnlyInPackage AS VARCHAR(10))
	END

	SET @sql = '
		SELECT * FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY [p].[ProductId]) AS Number,
				[p].[ProductId]
			FROM
				[product].[tProduct] AS p
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			WHERE
				[p].[IsDeleted] = 0
				' + CASE WHEN @SellOnlyInPackage IS NOT NULL  THEN + ' AND [lp].[SellOnlyInPackage] = ' + CAST(@SellOnlyInPackage AS VARCHAR(10)) ELSE '' END + '
		) AS Result'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + '
			AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END

	EXEC (@sqlCount)
	EXEC (@sql)
END
GO
