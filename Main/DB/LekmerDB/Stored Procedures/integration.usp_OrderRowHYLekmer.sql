SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_OrderRowHYLekmer]
	@OrderNo	int
AS
begin
	set nocount on
	begin try
		
		
		
		select
			o.OrderItemId, p.Title, o.Quantity, p.ProductId,
			o.ActualPriceIncludingVat, o.OriginalPriceIncludingVat, coalesce(ois.ErpId, p.ErpId) ErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
		where
			o.OrderId = @OrderNo
		
	
	
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
