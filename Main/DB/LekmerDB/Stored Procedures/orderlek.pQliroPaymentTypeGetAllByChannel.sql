SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pQliroPaymentTypeGetAllByChannel]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vQliroPaymentType]
	WHERE
		[QliroPaymentType.ChannelId] = @ChannelId
	ORDER BY
		[QliroPaymentType.Id]
END
GO
