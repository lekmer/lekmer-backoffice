SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityGetAllByModelFragmentCommonName]
	@ModelFragmentCommonName	varchar(50)
AS
begin
	set nocount on
	select
		E.*
	from
		[template].[vCustomEntity] E
		INNER JOIN [template].tModelFragmentEntity ME ON ME.EntityId = E.[Entity.Id]
		INNER JOIN [template].tModelFragment M ON ME.ModelFragmentId = M.ModelFragmentId
	where
		M.CommonName = @ModelFragmentCommonName
	ORDER BY E.[Entity.Title]
end

GO
