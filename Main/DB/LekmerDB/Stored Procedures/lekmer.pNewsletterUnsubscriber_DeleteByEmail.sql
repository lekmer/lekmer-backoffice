SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_DeleteByEmail]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Remove Options
	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	FROM
		[lekmer].[tNewsletterUnsubscriber] nu
		INNER JOIN [lekmer].[tNewsletterUnsubscriberOption] nuo ON [nuo].[UnsubscriberId] = [nu].[UnsubscriberId]
	WHERE
		nu.[Email] = @Email AND
		nu.[ChannelId] = @ChannelId
	
	DELETE
		[lekmer].[tNewsletterUnsubscriber]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	RETURN @@ROWCOUNT
END
GO
