SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRatingGroupDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockRatingGroup]
	WHERE BlockId = @BlockId
END
GO
