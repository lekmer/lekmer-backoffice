
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
