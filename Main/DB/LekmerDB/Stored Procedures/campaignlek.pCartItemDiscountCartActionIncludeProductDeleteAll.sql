SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemDiscountCartActionIncludeProduct]
	WHERE
		CartActionId = @CartActionId
END

GO
