SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageGetByCampaignId]
	@CampaignId INT,
	@ChannelId	INT
AS 
BEGIN

	DECLARE @CampaignLandingpageId INT
	SET @CampaignLandingpageId = (SELECT [CampaignLandingPage.CampaignLandingPageId]
								  FROM [campaignlek].[vCampaignLandingPageSecure] clp
								  WHERE [clp].[CampaignLandingPage.CampaignId] = @CampaignId)
	
	SELECT
		[clp].*
	FROM
		[campaignlek].[vCampaignLandingPage] clp
		INNER JOIN [core].[tChannel] c ON [c].[LanguageId] = [clp].[CampaignLandingPage.LanguageId]
	WHERE
		[clp].[CampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
		AND [c].[ChannelId] = @ChannelId
		
	SELECT
		[crclp].*
	FROM
		[campaignlek].[vCampaignRegistryCampaignLandingPage] crclp
		INNER JOIN [campaign].[tCampaignModuleChannel] cmc ON [cmc].[CampaignRegistryId] = [crclp].[CampaignRegistryCampaignLandingPage.CampaignRegistryId]
	WHERE
		[crclp].[CampaignRegistryCampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
		AND [cmc].[ChannelId] = @ChannelId
	
END
GO
