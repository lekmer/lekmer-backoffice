SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pVariationGroupSaveProducts]
	@VariationGroupId	int,	
	@ProductId			int
as
begin
	
	insert into product.[tVariationGroupProduct]
	(
		[VariationGroupId],
		[ProductId]
	)
	values
	( 
		@VariationGroupId,
		@ProductId
	)
	return scope_identity();

end









GO
