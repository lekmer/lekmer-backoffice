
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tFixedDiscountActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [aep].[ProductId] = [p].[ProductId]
	WHERE
		[aep].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
