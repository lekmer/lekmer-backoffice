SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-19
-- Description:	Gets a media items by FolderId
-- =============================================
CREATE PROCEDURE [media].[pMediaItemGetAllByFolderId]
	@MediaFolderId INT
AS
BEGIN
	SET NOCOUNT ON ;
	SELECT *
	FROM   [media].[vCustomMedia]
	WHERE  [Media.FolderId] = @MediaFolderId
END

GO
