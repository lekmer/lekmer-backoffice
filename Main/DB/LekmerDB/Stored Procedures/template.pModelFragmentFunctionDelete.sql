SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pModelFragmentFunctionDelete]
	@FunctionId INT
AS
BEGIN
	SET NOCOUNT ON	
	
	DELETE [template].[tModelFragmentFunction]
	WHERE  [FunctionId] = @FunctionId
	
	RETURN 0
END	









	




GO
