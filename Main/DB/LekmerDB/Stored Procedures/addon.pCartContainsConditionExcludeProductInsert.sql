SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionExcludeProductInsert]
	@ConditionId	int,
	@ProductId		int
AS 
BEGIN 
	INSERT
		[addon].tCartContainsConditionExcludeProduct
	(
		ConditionId,
		ProductId
	)
	VALUES
	(
		@ConditionId,
		@ProductId
	)
END 



GO
