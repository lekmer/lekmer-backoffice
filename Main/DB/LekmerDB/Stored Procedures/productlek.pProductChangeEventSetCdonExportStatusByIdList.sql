SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pProductChangeEventSetCdonExportStatusByIdList]
	@ProductChangeEventIds VARCHAR(MAX) ,
	@Delimiter CHAR(1) ,
	@CdonExportEventStatusId INT ,
	@ActionAppliedDate DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductChangeEventIds, @Delimiter)

	IF ( @ActionAppliedDate IS NULL ) 
	BEGIN
		UPDATE
			pce
		SET	
			[CdonExportEventStatusId] = @CdonExportEventStatusId
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[CdonExportEventStatusId] != @CdonExportEventStatusId
	END
	ELSE
	BEGIN
		UPDATE
			pce
		SET	
			[CdonExportEventStatusId] = @CdonExportEventStatusId,
			[ActionAppliedDate] = @ActionAppliedDate
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[CdonExportEventStatusId] != @CdonExportEventStatusId
	END
END
GO
