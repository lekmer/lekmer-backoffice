SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Volodymdyr Y.		Date: 20/01/2009                            >Creation*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelFragmentRegionSave] 
	@ModelFragmentRegionId INT,
	@ModelId INT,
	@Title NVARCHAR(50),
	@Ordinal INT
AS 
BEGIN
    SET NOCOUNT ON
    	
    UPDATE  [template].[tModelFragmentRegion]
    SET     [ModelId] = @ModelId,
			[Title] = @Title,
			[Ordinal] = @Ordinal
    WHERE   [ModelFragmentRegionId] = @ModelFragmentRegionId
    IF  @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tModelFragmentRegion] (
			[ModelId],
			[Title],
			[Ordinal]
		) VALUES ( 
			@ModelId,
			@Title, 
			@Ordinal
		)

        SET @ModelFragmentRegionId = SCOPE_IDENTITY()
    END
    
    RETURN @ModelFragmentRegionId
END
GO
