SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [messaging].[pMessageResetStatusOnOldWithStatusInProgress]
@MessageTypeId					int,
@MaxEndDate						datetime,
@InProgressMessageStatusId		int,
@ReadyToSendMessageStatusId		int
as
begin


	update
		[messaging].[tMessage]
	set
		MessageStatusId = @ReadyToSendMessageStatusId,
		BatchId = null
	where
		MessageTypeId = @MessageTypeId and
		MessageStatusId in (@InProgressMessageStatusId, @ReadyToSendMessageStatusId) and
		BatchId in (select BatchId from [messaging].[tBatch] where EndDate <= @MaxEndDate)


end


GO
