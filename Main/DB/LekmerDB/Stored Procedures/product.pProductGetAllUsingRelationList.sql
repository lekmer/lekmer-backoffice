SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetAllUsingRelationList]
		@ChannelId int,
		@RelationListId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int,@ProductRegistryId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY prl.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		prl.*,
		[pli].*
		FROM [product].[vCustomProductRelationList] prl
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = prl.[Product.Id] AND PRP.ProductRegistryId = ' + CAST(@ProductRegistryId AS varchar(10)) + '
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = prl.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', prl.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			prl.[ProductRelationList.RelationListId] = '+  CAST(@RelationListId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END
	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
