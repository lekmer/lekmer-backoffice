
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludesSave]
	@SizeTableId	INT,
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	-- Delete existing includes for size table.
	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryBrandDelete] @SizeTableId	

	-- Insert size table Product includes.
	INSERT [lekmer].[tSizeTableIncludeProduct] ([SizeTableId], [ProductId])
	SELECT @SizeTableId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
	
	-- Size table Category includes.
	DECLARE @tCategory TABLE (CategoryId INT, Ordinal INT)
	INSERT INTO @tCategory ([CategoryId], [Ordinal])
	SELECT [Id], [Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)

	-- Size table Brand includes.
	DECLARE @tBrand TABLE (BrandId INT, Ordinal INT)
	INSERT INTO @tBrand ([BrandId], [Ordinal])
	SELECT (CASE WHEN [Id] > 0 THEN [Id] ELSE NULL END), [Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal] (@BrandIds, @Delimiter)

	INSERT INTO [lekmer].[tSizeTableIncludeCategoryBrand] ([SizeTableId], [CategoryId], [BrandId])
	SELECT @SizeTableId, [c].[CategoryId], [b].[BrandId]
	FROM @tCategory c
	INNER JOIN @tBrand b ON [b].[Ordinal] = [c].[Ordinal]
END
GO
