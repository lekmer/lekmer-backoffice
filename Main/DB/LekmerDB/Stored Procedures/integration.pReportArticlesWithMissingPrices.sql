SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportArticlesWithMissingPrices]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		p.[ProductId],
		lp.[HYErpId],
		[pl_se].[PriceIncludingVat] AS 'SE',
		[pl_no].[PriceIncludingVat] AS 'NO',
		[pl_dk].[PriceIncludingVat] AS 'DK',
		[pl_fi].[PriceIncludingVat] AS 'FI',
		[pl_nl].[PriceIncludingVat] AS 'NL'
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = p.[ProductId]
		LEFT OUTER JOIN [product].[tPriceListItem] pl_se ON [pl_se].[ProductId] = p.[ProductId] AND [pl_se].[PriceListId] = 1
		LEFT OUTER JOIN [product].[tPriceListItem] pl_no ON [pl_no].[ProductId] = p.[ProductId] AND [pl_no].[PriceListId] = 2
		LEFT OUTER JOIN [product].[tPriceListItem] pl_dk ON [pl_dk].[ProductId] = p.[ProductId] AND [pl_dk].[PriceListId] = 3
		LEFT OUTER JOIN [product].[tPriceListItem] pl_fi ON [pl_fi].[ProductId] = p.[ProductId] AND [pl_fi].[PriceListId] = 4
		LEFT OUTER JOIN [product].[tPriceListItem] pl_nl ON [pl_nl].[ProductId] = p.[ProductId] AND [pl_nl].[PriceListId] = 1000005
	WHERE
		[pl_se].[PriceIncludingVat] IS NULL
		OR
		[pl_no].[PriceIncludingVat] IS NULL
		OR
		[pl_dk].[PriceIncludingVat] IS NULL
		OR
		[pl_fi].[PriceIncludingVat] IS NULL
		OR
		[pl_nl].[PriceIncludingVat] IS NULL
END
GO
