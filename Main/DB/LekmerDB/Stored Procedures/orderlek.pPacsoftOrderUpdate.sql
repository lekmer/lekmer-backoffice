SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPacsoftOrderUpdate]
	@OrderId	INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [orderlek].[tPacsoftOrder]
	SET [NeedSendPacsoftInfo] = 0
	WHERE [OrderId] = @OrderId
END
GO
