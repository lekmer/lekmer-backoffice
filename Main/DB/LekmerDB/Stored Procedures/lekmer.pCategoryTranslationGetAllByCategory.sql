
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCategoryTranslationGetAllByCategory]
@CategoryId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [CategoryId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[Title] AS 'Value'
	FROM
	    product.tCategoryTranslation where [CategoryId] = @CategoryId
	ORDER BY
		[LanguageId]
END
GO
