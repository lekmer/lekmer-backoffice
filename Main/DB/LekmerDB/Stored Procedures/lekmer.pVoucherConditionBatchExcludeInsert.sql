SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionBatchExcludeInsert]
	@ConditionId INT,
	@BatchId INT
AS 
BEGIN 
	SET NOCOUNT ON

	INSERT lekmer.tVoucherBatchesExclude
	( 
		ConditionId,
		BatchId
	)
	VALUES 
	(
		@ConditionId,
		@BatchId
	)
END
GO
