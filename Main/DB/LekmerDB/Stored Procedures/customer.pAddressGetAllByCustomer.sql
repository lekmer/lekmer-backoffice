SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pAddressGetAllByCustomer]
	@CustomerId int
as	
begin
	set nocount on

	select
		*
	from
		customer.vCustomAddress
	where
		[Address.CustomerId] = @CustomerId
end

GO
