SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupFolderSave]
	@RatingGroupFolderId INT,
	@ParentRatingGroupFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRatingGroupFolder]
		WHERE [Title] = @Title
			AND [RatingGroupFolderId] <> @RatingGroupFolderId
			AND	((@ParentRatingGroupFolderId IS NULL AND [ParentRatingGroupFolderId] IS NULL)
				OR [ParentRatingGroupFolderId] = @ParentRatingGroupFolderId)
	)
	RETURN -1
	
	UPDATE 
		[review].[tRatingGroupFolder]
	SET 
		[ParentRatingGroupFolderId] = @ParentRatingGroupFolderId,
		[Title] = @Title
	WHERE
		[RatingGroupFolderId] = @RatingGroupFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingGroupFolder] (
			[ParentRatingGroupFolderId],
			[Title]
		)
		VALUES (
			@ParentRatingGroupFolderId,
			@Title
		)

		SET @RatingGroupFolderId = SCOPE_IDENTITY()
	END

	RETURN @RatingGroupFolderId
END
GO
