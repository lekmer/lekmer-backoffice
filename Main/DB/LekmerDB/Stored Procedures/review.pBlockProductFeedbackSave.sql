SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockProductFeedbackSave]
	@BlockId INT,
	@AllowReview BIT
AS
BEGIN
	UPDATE [review].[tBlockProductFeedback]
	SET
		[AllowReview] = @AllowReview
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockProductFeedback] (
		[BlockId],
		[AllowReview]
	)
	VALUES (
		@BlockId,
		@AllowReview
	)
END
GO
