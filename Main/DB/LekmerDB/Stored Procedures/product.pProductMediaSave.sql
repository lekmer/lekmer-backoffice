SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pProductMediaSave]
	@ProductId int,
	@MediaId int,
	@ProductImageGroupId int,
	@Ordinal int
AS
BEGIN
	UPDATE	[product].[tProductMedia] 
	SET
		ProductImageGroupId = @ProductImageGroupId,
		Ordinal = @Ordinal
	WHERE
		(ProductId = @ProductId)And(MediaId=@MediaId)
IF @@ROWCOUNT = 0
	INSERT [product].[tProductMedia]
	(
		ProductId,
		MediaId,
		ProductImageGroupId,
		Ordinal
	)
	VALUES
	(
		@ProductId,
		@MediaId,
		@ProductImageGroupId,
		@Ordinal
	)
END
GO
