SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelGetById]
	@ModelId	int
AS
begin
	select
		*
	from
		[template].[vCustomModel]
	where
		[Model.Id] = @ModelId
end

GO
