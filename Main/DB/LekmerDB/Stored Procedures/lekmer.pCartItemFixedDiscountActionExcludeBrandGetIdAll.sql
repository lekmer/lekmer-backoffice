
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemFixedDiscountActionExcludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
