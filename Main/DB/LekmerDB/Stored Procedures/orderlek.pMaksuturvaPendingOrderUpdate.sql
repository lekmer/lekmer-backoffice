SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderUpdate]
	@ChannelId					INT,
	@OrderId					INT,
	@StatusId					INT,
	@FirstAttempt				DATETIME = NULL,
	@LastAttempt				DATETIME = NULL,
	@NextAttempt				DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tMaksuturvaPendingOrder]
	SET	
		[ChannelId] = @ChannelId,
		[StatusId] = @StatusId,
		[FirstAttempt] = @FirstAttempt,
		[LastAttempt] = @LastAttempt,
		[NextAttempt] = @NextAttempt
	WHERE
		[OrderId] = @OrderId
END
GO
