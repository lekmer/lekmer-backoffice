SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 3  *****************
User: Roman G.		Date: 23.12.2008		Time: 8:10
Description:
			Remove TemplateFragmentId
*****************  Version 2  *****************
User: Roman D.		Date: 22.12.2008		Time: 18:00
Description:
			added order
*****************  Version 1  *****************
User: Victor E.		Date: 14.10.2008		Time: 17:30
Description:
			Created 
*/


CREATE PROCEDURE [template].[pTemplateFragmentGetAllByModel]
	@ModelId	int
AS
begin
	set nocount on

	select
		null as 'TemplateFragment.TemplateId',
		null as 'TemplateFragment.Content',
		null as 'TemplateFragment.AlternateContent',
		MF.*
	from
		[template].[vCustomModelFragmentRegion] AS MFR
		join [template].[vCustomModelFragment] as MF 
		on MFR.[ModelFragmentRegion.Id] = MF.[ModelFragment.RegionId]  
	where
		MFR.[ModelFragmentRegion.ModelId] = @ModelId
    ORDER BY MF.[ModelFragment.Ordinal]	
end

GO
