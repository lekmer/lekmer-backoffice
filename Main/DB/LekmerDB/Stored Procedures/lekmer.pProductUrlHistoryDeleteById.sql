SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteById]
	@ProductUrlHistoryId INT
AS
BEGIN
	DELETE [lekmer].[tProductUrlHistory]
	WHERE ProductUrlHistoryId = @ProductUrlHistoryId
END
GO
