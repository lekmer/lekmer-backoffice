SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [lekmer].[pBlockProductFilterGetById]
	@BlockId	int,
	@LanguageId	int
as	
begin
	set nocount on
	select
		f.*,
		b.*
	from
		[lekmer].[vBlockProductFilter] as f
		inner join [sitestructure].[vCustomBlock] as b on f.[BlockProductFilter.BlockId] = b.[Block.BlockId]
	where
		f.[BlockProductFilter.BlockId] = @BlockId
		and b.[Block.LanguageId] = @LanguageId 
end

GO
