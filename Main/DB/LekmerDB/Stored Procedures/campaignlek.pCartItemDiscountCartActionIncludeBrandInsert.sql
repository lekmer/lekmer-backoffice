SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tCartItemDiscountCartActionIncludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END

GO
