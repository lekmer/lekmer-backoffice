SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionExcludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END

GO
