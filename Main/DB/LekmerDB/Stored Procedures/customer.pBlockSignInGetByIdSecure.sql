SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pBlockSignInGetByIdSecure]
@BlockId int
as	
begin
	set nocount on
	select
		s.*,
		b.*
	from
		[customer].[vCustomBlockSignIn] as s
		inner join [sitestructure].[vCustomBlockSecure] as b on s.[BlockSignIn.BlockId] = b.[Block.BlockId]
	where
		s.[BlockSignIn.BlockId]= @BlockId
end

GO
