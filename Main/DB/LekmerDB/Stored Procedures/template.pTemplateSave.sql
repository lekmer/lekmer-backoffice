SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
*****************  Version 3  *****************
User: Roman D.		Date: 04.12.2008		Time: 17:40
Description:
		 added reaction for  unique name
*****************  Version 2  *****************
User: Victor E.		Date: 18.18.2008		Time: 15:40
Description:
			delete channel id 
************************************************

*****************  Version 1  *****************
User: Victor E.		Date: 08.10.2008		Time: 17:00
Description:
			Created 

*****************  Version 4  *****************
User: Yuriy P.		Date: 22.01.2009
Description:
			added Image 
*/


CREATE PROCEDURE [template].[pTemplateSave]
	@TemplateId			int,
	@ModelId			int,
	@Title				varchar(50),
	@UseAlternate		bit,
	@ImageFileName		nvarchar(50)
AS
begin
	SET NOCOUNT ON

	IF @TemplateId != (
						SELECT
							TemplateId
						FROM
							[template].[tTemplate]
						WHERE
							[Title] =  @Title
							AND [ModelId] = @ModelId
					)
		RETURN -1
			
	UPDATE
		[template].[tTemplate]
	SET
		[Title] = @Title,
		[UseAlternate] = @UseAlternate,
		[ImageFileName] = @ImageFileName
	WHERE
		[TemplateId] = @TemplateId			

	IF  @@ROWCOUNT = 0
	BEGIN		
		INSERT INTO [template].[tTemplate]
		(
			[ModelId],
			[Title],
			[UseAlternate],
			[ImageFileName]
		)
		VALUES
		(
			@ModelId,
			@Title,
			@UseAlternate,
			@ImageFileName
		)
		SET @TemplateId = scope_identity()
	END
	
	RETURN @TemplateId
END
	





GO
