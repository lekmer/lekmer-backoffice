SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pAdIsUniqueKey]
	@AdId INT,
	@Key NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	-- Same keys are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [esales].[tAd]
		WHERE
			[Key] = @Key
			AND
			[AdId] <> @AdId
	)
	RETURN -1

	RETURN 0
END
GO
