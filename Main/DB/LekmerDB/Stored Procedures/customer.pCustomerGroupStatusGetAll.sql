SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupStatusGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerGroupStatus]
END

GO
