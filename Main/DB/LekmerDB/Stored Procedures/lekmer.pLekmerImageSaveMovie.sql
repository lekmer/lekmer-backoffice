SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerImageSaveMovie]
	@MediaId INT,
	@Link nvarchar(256),
	@Parameter nvarchar(50),
	@TumbnailImageUrl nvarchar(256),
	@HasImage NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON

	   update [lekmer].[tLekmerImage]
	      set
	        [Link]=@Link,
	        [Parameter]=@Parameter,
	        [TumbnailImageUrl]=@TumbnailImageUrl,
	        [HasImage]=@HasImage
	     where
			MediaId = @MediaId
	      
	      if @@ROWCOUNT = 0 
	begin
		insert into [lekmer].[tLekmerImage]
		(
			[MediaId],
			[Link],
			[Parameter],
			[TumbnailImageUrl],
			[HasImage]		
		)
		values
		(
			@MediaId,
			@Link,
			@Parameter,
			@TumbnailImageUrl,
			@HasImage
		) 
	END
END
GO
