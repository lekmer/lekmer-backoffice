SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2CategoryDeleteAll]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[esales].[tBlockEsalesTopSellersV2Category]
	WHERE
		[BlockId] = @BlockId
END
GO
