
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartItemPriceActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[aep].[ProductId]
	FROM 
		[addon].[tCartItemPriceActionExcludeProduct] aep
		INNER JOIN  [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE 
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
