SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageSetOfflineByIncludeProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @PackageMasterProductIds TABLE(ProductId INT)
	
	INSERT INTO @PackageMasterProductIds ([ProductId])
	SELECT DISTINCT [pak].[MasterProductId]
	FROM [productlek].[tPackage] pak
		 INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
	WHERE [pp].[ProductId] = @ProductId
	
	UPDATE
		[product].[tProduct]
	SET	
		[ProductStatusId] = 1
	WHERE
		[ProductId] IN (SELECT [ProductId] FROM @PackageMasterProductIds)
	
	SELECT [ProductId] FROM @PackageMasterProductIds
END
GO
