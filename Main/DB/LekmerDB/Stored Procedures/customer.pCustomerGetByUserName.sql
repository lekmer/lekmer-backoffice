
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetByUserName]
	@UserName	NVARCHAR(320),
	@ChannelId	INT
AS	
BEGIN
	SET NOCOUNT ON

	SELECT
		*	
	FROM
		[customer].[vCustomCustomer]
	WHERE
	    [CustomerUser.UserName] = @UserName
	    AND [Customer.CustomerRegistryId] IN
	    (
			SELECT CustomerRegistryId FROM [customer].[tCustomerModuleChannel]
			WHERE ChannelId = @ChannelId
	    )
END
GO
