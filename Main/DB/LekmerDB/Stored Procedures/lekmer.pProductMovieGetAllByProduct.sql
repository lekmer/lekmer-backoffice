SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductMovieGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SELECT
		[m].*,
		[li].Link AS 'LekmerImage.Link',
		[li].HasImage AS 'LekmerImage.HasImage',
		[li].TumbnailImageUrl AS 'LekmerImage.TumbnailImageUrl',
		[li].Parameter AS 'LekmerImage.Parameter'
	FROM 
		[lekmer].[tLekmerImage] li
	INNER JOIN 
		[product].[vCustomProductMedia] m
	ON
		li.[MediaId] = m.[Media.Id]
	WHERE
		m.[ProductMedia.ProductId] = @ProductId
	ORDER BY 
		[li].[MediaId]
END

GO
