SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure integration.usp_DapperTest
as
begin
   select top (10)
            OrderId as OrderNumber
           ,Number
           ,CustomerId
           ,BillingAddressId
           ,DeliveryAddressId
           ,CreatedDate
           ,FreightCost
           ,Email
           ,DeliveryMethodId
           ,ChannelId
           ,OrderStatusId
           ,UsedAlternate
           ,IP
           ,DeliveryTrackingId
   from     [order].tOrder
end
GO
