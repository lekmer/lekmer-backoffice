SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetAllByCustomerGroup] 
	@CustomerGroupId int
as	
begin
	set nocount on
	select
		C.*	
	from
		[customer].[vCustomCustomer] AS C
		INNER JOIN customer.tCustomerGroupCustomer AS CGC ON C.[Customer.CustomerId] = CGC.CustomerId
	where
		CGC.CustomerGroupId = @CustomerGroupId
end

GO
