SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionCreateReservation]
	@KlarnaShopId INT,
	@KlarnaMode INT,
	@KlarnaTransactionTypeId INT,
	@CreatedDate DATETIME,
	@CivicNumber VARCHAR(50),
	@OrderId INT,
	@Amount DECIMAL(16,2),
	@CurrencyId INT,
	@PClassId INT,
	@YearlySalary INT
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaTransaction]
			( [KlarnaShopId],
			  [KlarnaMode],
			  [KlarnaTransactionTypeId],
			  [Created],
			  [CivicNumber],
			  [OrderId],
			  [Amount],
			  [CurrencyId],
			  [PClassId],
			  [YearlySalary]
			)
	VALUES
			( @KlarnaShopId,
			  @KlarnaMode,
			  @KlarnaTransactionTypeId,
			  @CreatedDate,
			  @CivicNumber,
			  @OrderId,
			  @Amount,
			  @CurrencyId,
			  @PClassId,
			  @YearlySalary
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
