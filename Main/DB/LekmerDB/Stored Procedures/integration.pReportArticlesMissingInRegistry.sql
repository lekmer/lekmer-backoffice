SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportArticlesMissingInRegistry]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE	@RestrictedProducts TABLE
	(
	  RegistryId INT,
	  ProductId INT,
	  PRIMARY KEY ( RegistryId, ProductId ) WITH ( IGNORE_DUP_KEY = ON )
	)

	INSERT	INTO @RestrictedProducts ( [RegistryId], [ProductId] )
	SELECT [ProductRegistryId], [ProductId] FROM [lekmer].[tProductRegistryRestrictionProduct]

	INSERT	INTO @RestrictedProducts ( [RegistryId], [ProductId] )
	SELECT [ProductRegistryId], [ProductId] FROM [lekmer].[tProductRegistryRestrictionBrand] rb INNER JOIN [lekmer].[tLekmerProduct] p ON p.[BrandId] = rb.[BrandId]

	INSERT	INTO @RestrictedProducts ( [RegistryId], [ProductId] )
	SELECT [ProductRegistryId], [ProductId] FROM [lekmer].[tProductRegistryRestrictionCategory] rc INNER JOIN [product].[tProduct] p ON p.[CategoryId] = rc.[CategoryId]

	INSERT	INTO @RestrictedProducts ( [RegistryId], [ProductId] )
	SELECT [ProductRegistryId], [ProductId] FROM [lekmer].[tProductRegistryRestrictionCategory] rc 
	INNER JOIN [product].[tCategory] c3 ON c3.[ParentCategoryId] = rc.[CategoryId]
	INNER JOIN [product].[tProduct] p ON p.[CategoryId] = c3.[CategoryId]

	INSERT	INTO @RestrictedProducts ( [RegistryId], [ProductId] )
	SELECT [ProductRegistryId], [ProductId] FROM [lekmer].[tProductRegistryRestrictionCategory] rc 
	INNER JOIN [product].[tCategory] c2 ON c2.[ParentCategoryId] = rc.[CategoryId]
	INNER JOIN [product].[tCategory] c3 ON c3.[ParentCategoryId] = c2.[CategoryId]
	INNER JOIN [product].[tProduct] p ON p.[CategoryId] = c3.[CategoryId]

	-- Now we have list of restricted products
	-- Lets find missed products

	SELECT
		p.[ProductId],
		lp.[HYErpId],
		CASE
			WHEN [r_se].[ProductId] IS NOT NULL AND [pr_se].[ProductId] IS NULL THEN 'Ok' 
			WHEN [r_se].[ProductId] IS NULL     AND [pr_se].[ProductId] IS NULL THEN 'Missed'
			WHEN [r_se].[ProductId] IS NULL     AND [pr_se].[ProductId] IS NOT NULL THEN 'Ok (restricted)'
			WHEN [r_se].[ProductId] IS NOT NULL AND [pr_se].[ProductId] IS NOT NULL THEN 'Error'
		END 'SE',
		CASE
			WHEN [r_no].[ProductId] IS NOT NULL AND [pr_no].[ProductId] IS NULL THEN 'Ok' 
			WHEN [r_no].[ProductId] IS NULL     AND [pr_no].[ProductId] IS NULL THEN 'Missed'
			WHEN [r_no].[ProductId] IS NULL     AND [pr_no].[ProductId] IS NOT NULL THEN 'Ok (restricted)'
			WHEN [r_no].[ProductId] IS NOT NULL AND [pr_no].[ProductId] IS NOT NULL THEN 'Error'
		END 'NO',
		CASE
			WHEN [r_dk].[ProductId] IS NOT NULL AND [pr_dk].[ProductId] IS NULL THEN 'Ok' 
			WHEN [r_dk].[ProductId] IS NULL     AND [pr_dk].[ProductId] IS NULL THEN 'Missed'
			WHEN [r_dk].[ProductId] IS NULL     AND [pr_dk].[ProductId] IS NOT NULL THEN 'Ok (restricted)'
			WHEN [r_dk].[ProductId] IS NOT NULL AND [pr_dk].[ProductId] IS NOT NULL THEN 'Error'
		END 'DK',
		CASE
			WHEN [r_fi].[ProductId] IS NOT NULL AND [pr_fi].[ProductId] IS NULL THEN 'Ok' 
			WHEN [r_fi].[ProductId] IS NULL     AND [pr_fi].[ProductId] IS NULL THEN 'Missed'
			WHEN [r_fi].[ProductId] IS NULL     AND [pr_fi].[ProductId] IS NOT NULL THEN 'Ok (restricted)'
			WHEN [r_fi].[ProductId] IS NOT NULL AND [pr_fi].[ProductId] IS NOT NULL THEN 'Error'
		END 'FI',
		CASE
			WHEN [r_nl].[ProductId] IS NOT NULL AND [pr_nl].[ProductId] IS NULL THEN 'Ok' 
			WHEN [r_nl].[ProductId] IS NULL     AND [pr_nl].[ProductId] IS NULL THEN 'Missed'
			WHEN [r_nl].[ProductId] IS NULL     AND [pr_nl].[ProductId] IS NOT NULL THEN 'Ok (restricted)'
			WHEN [r_nl].[ProductId] IS NOT NULL AND [pr_nl].[ProductId] IS NOT NULL THEN 'Error'
		END 'NL'
	FROM
		[product].[tProduct] p
		LEFT OUTER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = p.[ProductId]
		LEFT OUTER JOIN [product].[tProductRegistryProduct] r_se ON [r_se].[ProductId] = p.[ProductId] AND [r_se].[ProductRegistryId] = 1
		LEFT OUTER JOIN [product].[tProductRegistryProduct] r_no ON [r_no].[ProductId] = p.[ProductId] AND [r_no].[ProductRegistryId] = 2
		LEFT OUTER JOIN [product].[tProductRegistryProduct] r_dk ON [r_dk].[ProductId] = p.[ProductId] AND [r_dk].[ProductRegistryId] = 3
		LEFT OUTER JOIN [product].[tProductRegistryProduct] r_fi ON [r_fi].[ProductId] = p.[ProductId] AND [r_fi].[ProductRegistryId] = 4
		LEFT OUTER JOIN [product].[tProductRegistryProduct] r_nl ON [r_nl].[ProductId] = p.[ProductId] AND [r_nl].[ProductRegistryId] = 1000005
		LEFT OUTER JOIN @RestrictedProducts pr_se ON [pr_se].[ProductId] = p.[ProductId] AND [pr_se].[RegistryId] = 1
		LEFT OUTER JOIN @RestrictedProducts pr_no ON [pr_no].[ProductId] = p.[ProductId] AND [pr_no].[RegistryId] = 2
		LEFT OUTER JOIN @RestrictedProducts pr_dk ON [pr_dk].[ProductId] = p.[ProductId] AND [pr_dk].[RegistryId] = 3
		LEFT OUTER JOIN @RestrictedProducts pr_fi ON [pr_fi].[ProductId] = p.[ProductId] AND [pr_fi].[RegistryId] = 4
		LEFT OUTER JOIN @RestrictedProducts pr_nl ON [pr_nl].[ProductId] = p.[ProductId] AND [pr_nl].[RegistryId] = 1000005
	WHERE
		[r_se].[ProductId] IS NULL AND [pr_se].[ProductId] IS NULL
		OR
		[r_no].[ProductId] IS NULL AND [pr_no].[ProductId] IS NULL
		OR
		[r_dk].[ProductId] IS NULL AND [pr_dk].[ProductId] IS NULL
		OR
		[r_fi].[ProductId] IS NULL AND [pr_fi].[ProductId] IS NULL
		OR
		[r_nl].[ProductId] IS NULL AND [pr_nl].[ProductId] IS NULL

END
GO
