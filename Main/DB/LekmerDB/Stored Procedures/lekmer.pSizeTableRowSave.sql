SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowSave]
	@SizeTableId		INT,
	@SizeId				INT,
	@Column1Value		NVARCHAR(50),
	@Column2Value		NVARCHAR(50),
	@Ordinal			INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [lekmer].[tSizeTableRow] (
		[SizeTableId],
		[SizeId],
		[Column1Value],
		[Column2Value],
		[Ordinal]
	)
	VALUES (
		@SizeTableId,
		@SizeId,
		@Column1Value,
		@Column2Value,
		@Ordinal
	)

	RETURN SCOPE_IDENTITY()
END
GO
