SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCartGetByIdAndCartGuid] 
	@CartId		INT,
	@CartGuid	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON
	SELECT	
		*
	FROM
		[order].[vCustomCart]
	WHERE
		[Cart.CartId] = @CartId
		AND [Cart.CartGuid] = @CartGuid
END
GO
