SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateFragmentGetAllByTemplate]
	@TemplateId			int
AS
begin
	SELECT
		tf.*,
		mf.*
	from
		[template].[vCustomTemplate] t
		inner join [template].[vCustomModelFragmentRegion] mfr on mfr.[ModelFragmentRegion.ModelId] = t.[Template.ModelId]
		inner join [template].[vCustomModelFragment] mf on mf.[ModelFragment.RegionId] = mfr.[ModelFragmentRegion.Id]
		left join [template].[vCustomTemplateFragment] tf on tf.[TemplateFragment.ModelFragmentId] = mf.[ModelFragment.Id] and tf.[TemplateFragment.TemplateId] = @TemplateId
	where
		t.[Template.Id] = @TemplateId
	ORDER BY
		MF.[ModelFragment.Ordinal]
end

GO
