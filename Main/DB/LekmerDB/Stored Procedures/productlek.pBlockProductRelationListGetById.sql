SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[bprl].*,
		[b].*
	FROM 
		[productlek].[vBlockProductRelationList] bprl
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bprl].[BlockProductRelationList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bprl].[BlockProductRelationList.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
