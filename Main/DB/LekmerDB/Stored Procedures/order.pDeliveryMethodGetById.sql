SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pDeliveryMethodGetById]
	@DeliveryMethodId	INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomDeliveryMethod]		
	WHERE
		[DeliveryMethod.DeliveryMethodId] = @DeliveryMethodId
		AND [DeliveryMethod.ChannelId] = @ChannelId
END

GO
