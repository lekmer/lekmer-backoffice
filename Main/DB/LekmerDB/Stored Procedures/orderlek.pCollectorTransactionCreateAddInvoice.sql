SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionCreateAddInvoice]
	@StoreId INT,
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@OrderId INT,
	@ProductCode VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tCollectorTransaction]
			( [StoreId],
			  [TransactionTypeId],
			  [CivicNumber],
			  [OrderId],
			  [ProductCode],
			  [Created]
			)
	VALUES
			( @StoreId,
			  @TransactionTypeId,			  
			  @CivicNumber,
			  @OrderId,
			  @ProductCode,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
