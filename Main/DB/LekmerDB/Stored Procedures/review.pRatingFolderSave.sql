SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingFolderSave]
	@RatingFolderId INT,
	@ParentRatingFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRatingFolder]
		WHERE [Title] = @Title
			AND [RatingFolderId] <> @RatingFolderId
			AND	((@ParentRatingFolderId IS NULL AND [ParentRatingFolderId] IS NULL)
				OR [ParentRatingFolderId] = @ParentRatingFolderId)
	)
	RETURN -1
	
	UPDATE 
		[review].[tRatingFolder]
	SET 
		[ParentRatingFolderId] = @ParentRatingFolderId,
		[Title] = @Title
	WHERE
		[RatingFolderId] = @RatingFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingFolder] (
			[ParentRatingFolderId],
			[Title]
		)
		VALUES (
			@ParentRatingFolderId,
			@Title
		)

		SET @RatingFolderId = SCOPE_IDENTITY()
	END

	RETURN @RatingFolderId
END
GO
