
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [report].[pCustomersFromStockholmArea]
	@DateFrom	DATETIME,
	@DateTo		DATETIME
AS
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#customersFromStockholmArea') IS NOT NULL
		DROP TABLE #customersFromStockholmArea
	CREATE TABLE #customersFromStockholmArea (
		Email VARCHAR(320),
		PostalCode NVARCHAR(50),
		CellPhoneNumber NVARCHAR(50),
		PRIMARY KEY (Email) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO [#customersFromStockholmArea]
	SELECT
		LTRIM(RTRIM([o].[Email])),
		[oa].[PostalCode],
		[ci].[CellPhoneNumber]
	FROM
		[order].[tOrder] o
		INNER JOIN [customer].[tCustomerInformation] ci ON [ci].[CustomerId] = [o].[CustomerId]
		INNER JOIN [order].[tOrderAddress] oa ON [oa].[OrderAddressId] = [o].[DeliveryAddressId]
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[ChannelId] = 1 -- SE
		AND [o].[CreatedDate] >= @DateFrom
		AND [o].[CreatedDate] < @DateTo
		AND SUBSTRING([oa].[PostalCode], 1, 2) IN ('10','11','12','13','14','15','16','17','18','19','63','70','72','74','75','76')

	SELECT
		[c].[Email],
		[c].[PostalCode],
		[c].[CellPhoneNumber]
	FROM
		[#customersFromStockholmArea] c
	WHERE
		NOT EXISTS ( -- exlude unsubscribers
			SELECT 1
			FROM [lekmer].[tNewsletterUnsubscriber] nu
			WHERE LTRIM(RTRIM([nu].[Email])) = [c].[Email]
		)
	ORDER BY
		[c].[Email]

	DROP TABLE #customersFromStockholmArea   
END
GO
