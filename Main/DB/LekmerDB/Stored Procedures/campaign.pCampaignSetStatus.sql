SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCampaignSetStatus]
@CampaignId		int,
@StatusId		int
AS
BEGIN
	UPDATE [campaign].[tCampaign]
	SET CampaignStatusId = @StatusId
	WHERE CampaignId = @CampaignId
END
GO
