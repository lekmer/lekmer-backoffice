SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [integration].[pSirBatchGetNew]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ChangesFromDate DATETIME
	
	SET @ChangesFromDate = (
		SELECT TOP 1 [b].[CompletedDate] 
		FROM [integration].[tSirBatch] b 
		WHERE [b].[CompletedDate] IS NOT NULL
		ORDER BY [b].[SirBatchId] DESC
	)
	
	INSERT INTO [integration].[tSirBatch]
			( [ChangesFromDate],
			  [StartedDate],
			  [CompletedDate]
			)
	VALUES
			( @ChangesFromDate, -- ChangesFromDate - datetime
			  GETDATE(), -- StartedDate - datetime
			  NULL  -- CompletedDate - datetime
			)
			
	SELECT *
	FROM [integration].[tSirBatch] b
	WHERE b.[SirBatchId] = @@IDENTITY
	
END
GO
