SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagSave]
	@CampaignId INT
AS
BEGIN
	EXEC [campaignlek].[pCampaignToTagDeleteByCampaignId] @CampaignId

	-- campaign deleted
	IF NOT EXISTS (SELECT 1 FROM [campaign].[tCampaign] WHERE [CampaignId] = @CampaignId)
	BEGIN
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, NULL
		RETURN
	END
	--------------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@CurrentDate DATETIME = GETDATE(),
		@StartDate DATETIME,
		@EndDate DATETIME,
		@StatusId INT	
	SELECT @StartDate = [StartDate], @EndDate = [EndDate], @StatusId = [CampaignStatusId] FROM [campaign].[tCampaign] WHERE [CampaignId] = @CampaignId
	
	-- campaign offline or online but end by date
	IF (@StatusId = 1 OR (@EndDate IS NOT NULL AND @EndDate < @CurrentDate))
	BEGIN
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, NULL
		RETURN
	END
	--------------------------------------------------------------------------------------------------------------------------------------------------
	IF (@StartDate IS NULL OR @StartDate < @CurrentDate)
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, NULL
	ELSE
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, @StartDate
	
	IF (@EndDate IS NOT NULL)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM [campaignlek].[tCampaignToTag] WHERE [CampaignId] = @CampaignId AND [ProcessingDate] = @EndDate)
		BEGIN
			EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, @EndDate
		END
	END
END
GO
