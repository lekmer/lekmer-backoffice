
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductGetIdAllByPackageMasterProduct]
		@ChannelId		INT,
		@CustomerId		INT,
		@ProductId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PackageId INT
	SET @PackageId = (SELECT [PackageId] FROM [productlek].[tPackage] WHERE [MasterProductId] = @ProductId)

	SELECT p.[Product.Id]
	FROM
		[productlek].[tPackageProduct] pp
		INNER JOIN [product].[vCustomProductInclPackageProducts] p ON pp.ProductId = [p].[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		pp.[PackageId] = @PackageId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
