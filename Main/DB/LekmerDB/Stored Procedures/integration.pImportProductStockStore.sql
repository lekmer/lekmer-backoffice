
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pImportProductStockStore]

AS
BEGIN	
		if object_id('tempdb..#tImportProductStockStore') is not null
		drop table #tImportProductStockStore
		
		create table #tImportProductStockStore
		(
			ProductId int null,
			SizeId int  null,
			LagerProductTitle nvarchar(200) COLLATE Finnish_Swedish_CI_AS not null,
			LagerAmmount int not null
		)
		delete integration.tempProductStockStore
		where ErpId = '' or ErpId is null

		-- Insert all new rows into temp table
		insert into #tImportProductStockStore(ProductId, SizeId, LagerProductTitle, LagerAmmount)
		select
			p.ProductId,
			s.[Size.Id],
			i.LagerProductTitle,
			i.LagerAmmount
		from 
			integration.tempProductStockStore i
		left join  [product].[tProduct] p
		on
			p.ErpId = SUBSTRING(i.ErpId, 1, 12)
		left join [lekmer].[vSize] s
		on
			s.[Size.ErpId] = SUBSTRING(i.[ErpId], 14, 16) 
		where ISNUMERIC(i.LagerAmmount) = 1

		truncate table [product].[tStoreProductSize]

		insert into [product].[tStoreProductSize] (ProductId, SizeId, Quantity)
		select 
			s.ProductId, s.SizeId, s.LagerAmmount
		from
			#tImportProductStockStore s
		where (s.ProductId is not null)and(s.SizeId is not null)

			

END
GO
