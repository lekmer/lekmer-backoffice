SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE [product].[tBlockCategoryProductListCategory]
	WHERE [BlockId] = @BlockId

	DELETE [productlek].[tBlockCategoryProductList]
	WHERE [BlockId] = @BlockId
END
GO
