SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductTypeGetAll]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[productlek].[vProductType]
END
GO
