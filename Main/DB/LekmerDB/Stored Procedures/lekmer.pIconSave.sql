SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconSave]
	@IconId			INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255),
	@MediaId		INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[lekmer].[tIcon]
	SET 
		[Title] = @Title,
		[Description] = @Description,
		[MediaId] = @MediaId
	WHERE
		[IconId] = @IconId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tIcon] (
			[Title],
			[Description],
			[MediaId]
		)
		VALUES (
			@Title,
			@Description,
			@MediaId
		)

		SET @IconId = SCOPE_IDENTITY()
	END

	RETURN @IconId
END
GO
