SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionCurrencyGetByCondition]
	@ConditionId	INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartValueRangeConditionCurrency] cvrcc
	WHERE 
		cvrcc.[CartValueRangeConditionCurrency.ConditionId] = @ConditionId
END
GO
