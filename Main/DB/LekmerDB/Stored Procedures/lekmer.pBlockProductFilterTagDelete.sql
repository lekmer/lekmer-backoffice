SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockProductFilterTagDelete]
	@TagId INT
AS 
BEGIN
	SET NOCOUNT ON

    DELETE
        [lekmer].[tBlockProductFilterTag]
    WHERE
        [TagId] = @TagId
END
GO
