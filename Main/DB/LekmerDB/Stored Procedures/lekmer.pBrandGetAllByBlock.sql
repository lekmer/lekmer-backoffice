
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandGetAllByBlock]
	@ChannelId		INT,
	@BlockId		INT,
	@Page			INT = NULL,
	@PageSize		INT
AS
BEGIN
	SET NOCOUNT ON
		
	DECLARE @sql		 NVARCHAR(MAX)
	DECLARE @sqlCount	 NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
	
	DECLARE @blockIncludeAllBrands BIT
	SELECT 
		@blockIncludeAllBrands = IncludeAllBrands 
	FROM 
		lekmer.tBlockBrandList
	WHERE 
		BlockId = @BlockId
	
	IF (@blockIncludeAllBrands = 1)
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY cast(lower([b].[Brand.Title]) AS binary) ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[vBrand] b
				WHERE 
					[b].[Brand.BrandId] NOT IN (
													SELECT [prrb].[BrandId] 
													FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
													INNER JOIN [product].[tProductModuleChannel] pmc 
														ON pmc.ProductRegistryId = prrb.ProductRegistryId
														   AND pmc.ChannelId = @ChannelId
												)
					AND [b].[ChannelId] = @ChannelId
					AND [b].[Brand.IsOffline] = 0
			)'
		END
	ELSE
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY [bb].[Ordinal] ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[tBlockBrandListBrand] bb
					INNER JOIN [lekmer].[vBrand] b ON [bb].[BrandId] = [b].[Brand.BrandId]
				WHERE
					[b].[Brand.BrandId] NOT IN (
													SELECT [prrb].[BrandId] 
													FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
													INNER JOIN [product].[tProductModuleChannel] pmc 
														ON pmc.ProductRegistryId = prrb.ProductRegistryId
														   AND pmc.ChannelId = @ChannelId
												)
					AND [bb].[BlockId] = @BlockId
					AND [b].[ChannelId] = @ChannelId
					AND [b].[Brand.IsOffline] = 0
			)'
		END

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
	WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT, 
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@BlockId,
			@Page,
			@PageSize

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@BlockId,
			@Page,
			@PageSize
END
GO
