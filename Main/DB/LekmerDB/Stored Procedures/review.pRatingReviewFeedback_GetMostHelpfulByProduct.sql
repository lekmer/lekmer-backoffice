
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetMostHelpfulByProduct]
	@ChannelId				INT,
	@ProductId				INT,
	@RatingId				INT,
	@RatingItemIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@RatingReviewStatusId	INT,
	@IsInappropriate		BIT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TOP(1) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [review].[vRatingItemProductVote] ripv 
			ON ripv.[RatingItemProductVote.RatingReviewFeedbackId] = rrf.[RatingReviewFeedback.RatingReviewFeedbackId]
		INNER JOIN [generic].[fnConvertIDListToTable](@RatingItemIds, @Delimiter) ri 
			ON ri.[ID] = ripv.[RatingItemProductVote.RatingItemId]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.ProductId] = @ProductId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Inappropriate] = @IsInappropriate
		AND ripv.[RatingItemProductVote.RatingId] = @RatingId
	ORDER BY
		rrf.[RatingReviewFeedback.LikeHit] DESC
END
GO
