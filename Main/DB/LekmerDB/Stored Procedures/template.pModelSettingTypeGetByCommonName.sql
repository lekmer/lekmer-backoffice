SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelSettingTypeGetByCommonName]
    @CommonName VARCHAR(50)
AS 
BEGIN
    SET NOCOUNT ON

    SELECT  *
    FROM    [template].[vCustomModelSettingType]
    WHERE   [ModelSettingType.CommonName] = @CommonName

END

GO
