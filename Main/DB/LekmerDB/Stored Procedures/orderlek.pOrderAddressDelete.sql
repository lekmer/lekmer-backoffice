SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderAddressDelete]
	@OrderAddressId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tOrderAddress]
	WHERE [OrderAddressId] = @OrderAddressId
	
	DELETE [order].[tOrderAddress]
	WHERE [OrderAddressId] = @OrderAddressId
END
GO
