SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBlockLatestProductAddedToBasketGetByIdSecure]
	@BlockId int
as	
begin
	set nocount on
	select
		BLPATB.*,
		b.*
	from
		[lekmer].[tBlockLatestProductAddedToBasket] as BLPATB
		inner join [sitestructure].[vCustomBlockSecure] as b on BLPATB.[BlockId] = b.[Block.BlockId]
	where
		BLPATB.[BlockId] = @BlockId
end
GO
