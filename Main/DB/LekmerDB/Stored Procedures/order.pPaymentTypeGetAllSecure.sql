SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [order].[pPaymentTypeGetAllSecure]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	ORDER BY
		[PaymentType.Title]
END
GO
