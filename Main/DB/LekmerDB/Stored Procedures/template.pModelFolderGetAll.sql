SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 06.10.2008		Time: 17:00
Description:
			Created 
*/

CREATE PROCEDURE [template].[pModelFolderGetAll]
AS
begin
	set nocount on

	select
		*	
	from
		[template].[vCustomModelFolder]
	order by
		[ModelFolder.Title]
end

GO
