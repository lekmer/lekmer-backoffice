SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationTypeGetById]
	@VariationTypeId	int
as
begin
	set nocount off
	SELECT 
		*
	FROM 
		[product].[vCustomVariationType] vt
	where
		vt.[VariationType.Id] = @VariationTypeId
	order by vt.[VariationType.Ordinal]
end

GO
