
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductGetIdAllByCampaignId]
	@CampaignId		INT
AS
BEGIN
	SET NOCOUNT ON

---The logic also exists in lekmer.pProductTagAllForCampaigns, keep them identical!!

--PercentagePriceDiscountAction - inlclude brand
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--PercentagePriceDiscountAction - inlclude product
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId = @CampaignId
union
 --PercentagePriceDiscountAction - include category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --PercentagePriceDiscountAction - include category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
 
 
 union
 
 
 
 -- ProductDiscount - include product
 select ProductId from campaign.tProductAction pa
inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pa.ProductActionId
where CampaignId = @CampaignId


union


-- FixedDiscount - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedDiscount - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedDiscount - include category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId

union


-- FixedPrice - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedPrice - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedPrice- include category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId


union

-- GiftVardViaMailProductAction - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--GiftVardViaMailProductAction - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId = @CampaignId
union 
 --GiftVardViaMailProductAction - include category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --GiftVardViaMailProductAction - include category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1



 
except




--PercentagePriceDiscountAction - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--PercentagePriceDiscountAction - exclude product
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId = @CampaignId
union
 --PercentagePriceDiscountAction - exclude category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --PercentagePriceDiscountAction - exclude category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
 
 
 union
 
 
-- FixedDiscount - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedDiscount - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedDiscount - exclude category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId

union 

-- FixedPrice - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedPrice - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedPrice - exclude category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId


union

-- GiftVardViaMailProductAction - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--GiftVardViaMailProductAction - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId = @CampaignId
union 
 --GiftVardViaMailProductAction - exclude category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --GiftVardViaMailProductAction - exclude category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
END

GO
