SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreGetAll]
    @Page INT = NULL,
    @PageSize INT,
    @SortBy VARCHAR(20) = NULL,
    @SortDescending BIT = NULL
AS 
    BEGIN
        SET NOCOUNT ON
	
        IF ( generic.fnValidateColumnName(@SortBy) = 0 ) 
        BEGIN
            RAISERROR ( N'Illegal characters in string (parameter @SortBy): %s' , 16 , 1 , @SortBy ) ;
            RETURN
        END

        DECLARE @sql NVARCHAR(MAX)
        DECLARE @sqlCount NVARCHAR(MAX)
        DECLARE @sqlFragment NVARCHAR(MAX)

        SET @sqlFragment = '
			SELECT ROW_NUMBER() OVER (ORDER BY s.[' 
				+ COALESCE(@SortBy, 'Store.Title') 
				+ ']'
				+ CASE WHEN ( @SortDescending = 1 ) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				*
			FROM [product].[vCustomStore] s'
			
        SET @sql = 'SELECT * FROM
			(' + @sqlFragment + '
			)
			AS SearchResult'
			
        IF @Page != 0 AND @Page IS NOT NULL 
        BEGIN
            SET @sql = @sql + '
			WHERE Number > ' + CAST(( @Page - 1 ) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
        END
		
        SET @sqlCount = 'SELECT COUNT(1) FROM
			(
			' + @sqlFragment + '
			)
			AS CountResults'
			
        EXEC sp_executesql @sqlCount
        EXEC sp_executesql @sql
    END

GO
