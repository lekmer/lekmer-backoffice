
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreDecrement]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ProductId INT
	DECLARE @ChannelId INT
	
	SELECT
		@ProductId = rrf.ProductId,
		@ChannelId = rrf.ChannelId
	FROM [review].[tRatingReviewFeedback] rrf
	WHERE rrf.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	
	DECLARE @RelationListVariantTypeId INT
	SET @RelationListVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant')
	DECLARE @RelationListColorVariantTypeId INT
	SET @RelationListColorVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'ColorVariant')

	IF OBJECT_ID('tempdb..#VariantIds') IS NOT NULL
		DROP TABLE #VariantIds
	CREATE TABLE #VariantIds(VariantId INT);
	INSERT INTO #VariantIds EXEC [lekmer].[pProductVariantGetIdAllByProduct] @ProductId, @RelationListVariantTypeId
	INSERT INTO #VariantIds EXEC [lekmer].[pProductVariantGetIdAllByProduct] @ProductId, @RelationListColorVariantTypeId
	
	-- Add current product
	INSERT INTO #VariantIds (VariantId) VALUES (@ProductId)
	
	-- Remove duplicates
	ALTER TABLE #VariantIds ADD Id INT IDENTITY(1,1)
	DELETE FROM #VariantIds 
	WHERE Id IN (SELECT Id FROM (SELECT *, RANK() OVER (PARTITION BY [VariantId] ORDER BY Id DESC) rank FROM #VariantIds) T WHERE rank > 1)
	ALTER TABLE #VariantIds DROP COLUMN Id

	UPDATE ps
	SET ps.[HitCount] = ps.[HitCount] - 1
	FROM [review].[tRatingItemProductScore] ps
		INNER JOIN [review].[tRatingItemProductVote] pv ON 
			pv.[RatingId] = ps.[RatingId] 
			AND pv.[RatingItemId] = ps.[RatingItemId]
		INNER JOIN #VariantIds vid ON
			vid.[VariantId] = ps.[ProductId]
	WHERE
		ps.[ChannelId] = @ChannelId
		AND pv.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	
	DELETE ps
	FROM [review].[tRatingItemProductScore] ps
		INNER JOIN [review].[tRatingItemProductVote] pv ON 
			pv.[RatingId] = ps.[RatingId] 
			AND pv.[RatingItemId] = ps.[RatingItemId]
		INNER JOIN #VariantIds vid ON
			vid.[VariantId] = ps.[ProductId]
	WHERE
		ps.[ChannelId] = @ChannelId
		AND pv.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
		AND ps.[HitCount] <= 0
	
	IF OBJECT_ID('tempdb..#VariantIds') IS NOT NULL
		DROP TABLE #VariantIds
END
GO
