SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionSaveReservationResponse]
	@KlarnaTransactionId INT,
	@ReservationNumber VARCHAR(50),
	@ResponseCodeId INT,
	@KlarnaFaultCode INT,
	@KlarnaFaultReason NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tKlarnaTransaction]
	SET
		[ReservationNumber] = @ReservationNumber,
		[ResponseCodeId] = @ResponseCodeId,
		[KlarnaFaultCode] = @KlarnaFaultCode,
		[KlarnaFaultReason] = @KlarnaFaultReason,
		[Duration] = @Duration
	WHERE
		[KlarnaTransactionId] = @KlarnaTransactionId
END
GO
