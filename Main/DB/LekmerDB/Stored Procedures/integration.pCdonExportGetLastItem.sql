SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pCdonExportGetLastItem]
AS
BEGIN
	SELECT 
		TOP (1) ImportId
	FROM
		[integration].[tCdonExport]
	ORDER BY CdonExportId DESC 
END
GO
