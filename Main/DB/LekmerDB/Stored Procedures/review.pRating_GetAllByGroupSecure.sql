SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRating_GetAllByGroupSecure]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingGroupRating] rgr
		INNER JOIN [review].[vRatingSecure] r ON r.[Rating.RatingId] = rgr.[RatingGroupRating.RatingId]
	WHERE
		rgr.[RatingGroupRating.RatingGroupId] = @RatingGroupId
	ORDER BY
		rgr.[RatingGroupRating.Ordinal]
END
GO
