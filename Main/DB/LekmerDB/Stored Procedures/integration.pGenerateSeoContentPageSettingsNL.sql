
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsNL]
AS 
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION				

		-- Insert new contentnodeIds in tContentPageSeoSetting
		INSERT INTO [sitestructure].[tContentPageSeoSetting] (ContentNodeId)
		SELECT
			[n].[ContentNodeId]
		FROM
			[sitestructure].[tContentNode] n --3
		WHERE
			[n].[SiteStructureRegistryId] = 1000005
			AND [n].[ContentNodeTypeId] = 3 -- detta är contentpages
			AND [n].[ContentNodeId] NOT IN (SELECT ContentNodeId FROM sitestructure.tContentPageSeoSetting)


		------------------------------------------------------------
		-- <Speelgoed Categorie Actiefiguren- niveau 2>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Speelgoed van Lekmer.nl – Speelgoed online.',
			cps.[Description] = 'Koop ' + cn.Title + ' online. '
			+ 'Wij bieden een grote keuze kinderspeelgoed aan van bekende merken en leveren deze binnen 2-5 werkdagen bij je thuis - Lekmer.nl jouw speelgoedwinkel.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1012305 -- Speelgoed
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Speelgoed OndercategorieStarwars - niveau 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = x.Title + ' ' + cn.Title + ' Speelgoed online van Lekmer.nl – Speelgoed online.',
			cps.[Description] = 'Koop ' + x.Title + ' ' + cn.Title + ' online. '
			+ 'Wij bieden een grote keuze kinderspeelgoed aan van bekende merken en leveren deze binnen 2-5 werkdagen bij je thuis - Lekmer.nl jouw speelgoedwinkel.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1012305) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Kind & Baby Categorie Overcategorie - niveau 2>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = 'Kind en baby ' + cn.Title + ' producten online van Lekmer.nl – Koop kinderartikelen online.',
			cps.[Description] = 'Koop Kinder-, en Babyproducten online. Krijg je kinder-, en babyproducten binnen 2-5 werkdagen thuis geleverd van Lekmer.nl.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1012735 -- Peuter & baby
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Kind & Baby Ondercatergorie - niveau 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Kinder-, en babyproducten online van Lekmer.nl – Koop kinderartikelen online.',
			cps.[Description] = 'Koop ' + cn.Title + ' Kinder-, en Babyprodukter online. '
			+ 'Ontvang jouw kinder- en babyproducten thuisgeleverd binnen 2-5 werkdagen van Lekmer.nl.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1012735) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Kinderkleding Categorie Overcategorie - niveau 2>
		------------------------------------------------------------										
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' Kinderkleding online van Lekmer.nl – Kinder-, en babykleding online.',
			cps.[Description] = 'Koop ' + cn.Title + ' online. '
			+ 'Grote keuze uit leuke kleding voor kinderen en baby''s – Koop je kinderkleding online en krijg ze thuisbezorgd binnen 2-5 werkdagen van Lekmer.nl.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1012871 -- Kinderkleding
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Kinderkleding Ondercategorie - niveau 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Kinderkleding online van Lekmer.nl – Kinder-, en babykleding online.',
			cps.[Description] = 'Koop ' + cn.Title + ' en ' + x.Title + ' online. Krijg jouw kinderkleding thuisbezorgd binnen 2-5 werkdagen van Lekmer.nl.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1012871) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inrichting Categorie Overcategorie - niveau 2>
		------------------------------------------------------------										
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' Inrichting online van Lekmer.nl. Kinderkamerinrichting op het net.',
			cps.[Description] = 'Koop ' + cn.Title + ' online. Krijg jouw kinderkamer thuisgeleverd binnen 2-5 werkdagen van Lekmer.nl.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1013114 -- Kinderkamers
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inrichting Ondercategorie - niveau 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Inrichting online van Lekmer.nl – Kinderkamer op het net.',
			cps.[Description] = 'Koop ' + cn.Title + ' en ' + x.Title + ' online. Krijg jouw kinderkamer binnen 2-5 werkdagan thuisgeleverd van Lekmer.nl.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1013114) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
