SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [product].[pProductVariationDeleteAllByVariationGroup]
	@VariationGroupId	int
as
begin
		delete from [product].[tProductVariation] where [VariationGroupId] = @VariationGroupId	
end


GO
