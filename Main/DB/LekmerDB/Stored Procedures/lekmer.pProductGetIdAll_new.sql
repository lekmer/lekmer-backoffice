SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductGetIdAll_new]
	@ChannelId INT,
	@CustomerId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)
	
	DECLARE @CustomerIdString VARCHAR(10)
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))

	SET @sqlFilter = '
		(
			SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
				p.[ProductId]
			FROM
				product.tProduct AS p
				INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
				INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
				INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
				
		INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]
		INNER JOIN lekmer.[tProductUrl] AS pu ON lp.[ProductId] = pu.[ProductId] AND ch.[LanguageId] = pu.[LanguageId]
				
				INNER JOIN product.tPriceListItem AS pli
					ON pli.[ProductId] = P.ProductId
					AND pli.[PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(Ch.CurrencyId, P.ProductId, PMC.PriceListRegistryId, ' + @CustomerIdString + ')
			WHERE
				Ch.ChannelId = ' + CAST(@ChannelId AS VARCHAR(10)) + '
				and P.IsDeleted = 0
				AND P.ProductStatusId = 0
		)'
		
	SET @sqlCountFilter = '
		(
			SELECT p.[ProductId]
			FROM
				product.tProduct AS p
				INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
				INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
				INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
				
		INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]
		INNER JOIN lekmer.[tProductUrl] AS pu ON lp.[ProductId] = pu.[ProductId] AND ch.[LanguageId] = pu.[LanguageId]
				
				INNER JOIN product.tPriceListItem AS pli
					ON pli.[ProductId] = P.ProductId
					AND pli.[PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(Ch.CurrencyId, P.ProductId, PMC.PriceListRegistryId, ' + @CustomerIdString + ')
			WHERE
				Ch.ChannelId = ' + CAST(@ChannelId AS VARCHAR(10)) + '
				and P.IsDeleted = 0
				AND P.ProductStatusId = 0
		)
	'

	SET @sql = '
		SELECT * FROM
		' + @sqlFilter + '
		AS SearchResult'
	SET @sqlCount = '
		SELECT COUNT(1) FROM
		' + @sqlCountFilter + '
		AS SearchResultsCount'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
			AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END	

	EXEC (@sqlCount)
	EXEC (@sql)
END
GO
