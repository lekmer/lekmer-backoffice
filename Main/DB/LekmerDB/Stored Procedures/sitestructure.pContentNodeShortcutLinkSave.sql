SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentNodeShortcutLinkSave]
	@ContentNodeId			int,
	@LinkContentNodeId		int
as
BEGIN
	update
		[sitestructure].[tContentNodeShortcutLink]
	set
		[LinkContentNodeId]	= @LinkContentNodeId
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		insert
			[sitestructure].[tContentNodeShortcutLink]
		(
			[ContentNodeId],
			[LinkContentNodeId]
		)
		values
		(
			@ContentNodeId,
			@LinkContentNodeId
		)
	end
end
GO
