SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pCustomersLapsedCount]
	@ChannelId	INT
AS
BEGIN
	-- Number of lapsed customers (<12 months)
	
	DECLARE @CurrentDate DATETIME
	DECLARE @12MonthsAgo DATETIME
	SET @CurrentDate = GETDATE()
	SET @12MonthsAgo = DATEADD(MONTH, -12, @CurrentDate)

	DECLARE @tBeforePeriod TABLE (Email VARCHAR(320) primary key with (ignore_dup_key = on))
	INSERT INTO @tBeforePeriod
	SELECT LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] < @12MonthsAgo
		AND [o].[ChannelId] = @ChannelId
		
	DECLARE @tAfterPeriod TABLE (Email VARCHAR(320) primary key with (ignore_dup_key = on))
	INSERT INTO @tAfterPeriod
	SELECT LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > @12MonthsAgo
		AND [o].[ChannelId] = @ChannelId

	SELECT COUNT(DISTINCT [a].[Email]) 'Customers' FROM (
		SELECT [Email] FROM @tBeforePeriod
		EXCEPT
		SELECT [Email] FROM @tAfterPeriod
	) a
END
GO
