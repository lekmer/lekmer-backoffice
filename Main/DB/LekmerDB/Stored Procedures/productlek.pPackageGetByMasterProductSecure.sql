SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageGetByMasterProductSecure]
	@MasterProductId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @CurrencyId INT, @PriceListRegistryId INT, @PriceListId INT

	SELECT @CurrencyId = [CurrencyId], @PriceListRegistryId = [PriceListRegistryId]
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@MasterProductId, @ChannelId)
	
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @MasterProductId, @PriceListRegistryId, NULL))
	
	SELECT
		*
	FROM
		[productlek].[vPackageSecure] p 
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Package.MasterProductId]
														  AND [pli].[Price.PriceListId] = @PriceListId
	WHERE
		[p].[Package.MasterProductId] = @MasterProductId
END
GO
