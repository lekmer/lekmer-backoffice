SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignGetByCampaignId]
	@CampaignId INT
AS
BEGIN
	SELECT
		[crc].[CampaignRegistryId] 'CampaignRegistryCampaign.CampaignRegistryId',
		[crc].[CampaignId] 'CampaignRegistryCampaign.CampaignId',
		[c].[ChannelId] 'CampaignRegistryCampaign.ChannelId',
		[co].[ISO] 'CampaignRegistryCampaign.CountryIso'
	FROM
		[campaignlek].[tCampaignRegistryCampaign] crc
		INNER JOIN [campaign].[tCampaignModuleChannel] cmc ON [cmc].[CampaignRegistryId] = [crc].[CampaignRegistryId]
		INNER JOIN [core].[tChannel] c ON [c].[ChannelId] = [cmc].[ChannelId]
		INNER JOIN [core].[tCountry] co ON [co].[CountryId] = [c].[CountryId]
	WHERE
		[crc].[CampaignId] = @CampaignId
END
GO
