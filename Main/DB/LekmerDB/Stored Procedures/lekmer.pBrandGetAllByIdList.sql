SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandGetAllByIdList]
	@ChannelId	INT,
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		b.*
	FROM
		[lekmer].[vBrand] b
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS bl ON bl.[ID] = b.[Brand.BrandId]
	WHERE 
		b.[ChannelId] = @ChannelId
END
GO
