SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pConditionTargetProductTypeDelete]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE [campaignlek].[tConditionTargetProductType]
	WHERE [ConditionId] = @ConditionId
END
GO
