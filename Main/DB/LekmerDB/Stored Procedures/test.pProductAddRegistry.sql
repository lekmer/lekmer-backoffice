SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure test.pProductAddRegistry
	@ProductId int
as
begin
	insert product.tProductRegistryProduct
	(
		ProductRegistryId,
		ProductId
	)
	select
		ProductRegistryId,
		@ProductId
	from
		product.tProductRegistry
end
GO
