SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pBatchGetById]
@BatchId	uniqueidentifier
as
begin

	select
		BatchId,
		StartDate,
		EndDate,
		MachineName,
		ProcessId
	from 
		[messaging].[tBatch]
	where
		[BatchId] = @BatchId

end

GO
