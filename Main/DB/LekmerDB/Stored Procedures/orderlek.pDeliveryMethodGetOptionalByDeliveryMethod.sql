SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDeliveryMethodGetOptionalByDeliveryMethod]
	@ChannelId INT,
	@DeliveryMethodId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		dm.*
	FROM 
		[order].[vCustomDeliveryMethod] dm
		INNER JOIN [orderlek].[tDeliveryMethodDependency] dmd ON dmd.[OptionalDeliveryMethodId] = dm.[DeliveryMethod.DeliveryMethodId]
	WHERE
		dmd.[BaseDeliveryMethodId] = @DeliveryMethodId
		AND dm.[DeliveryMethod.ChannelId] = @ChannelId
		AND dm.[DeliveryMethod.DeliveryMethodTypeId] = 2 -- Optional
	ORDER BY
		dm.[DeliveryMethod.Priority]
END
GO
