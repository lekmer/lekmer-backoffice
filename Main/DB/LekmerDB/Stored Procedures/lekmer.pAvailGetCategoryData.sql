SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pAvailGetCategoryData] 
AS
BEGIN
	SET NOCOUNT ON;

	--set statistics io on
	--set statistics time on

	declare @ProductCategoryLevel1 table
			(
				ProductId int,
				CategoryId int
			)

	declare @ProductCategoryLevel2 table
			(
				ProductId int,
				CategoryId int
			)
			
	declare @ProductCategoryLevel3 table
			(
				ProductId int,
				CategoryId int
			)
			
	-- Level 1
	Insert into @ProductCategoryLevel1 (ProductId, CategoryId)
	select 
		ProductId, 
		CategoryId
	from [product].tProduct with (nolock)

	-- Level 2
	insert into @ProductCategoryLevel2 (ProductId, CategoryId)
	select
		t.ProductId,
		c.ParentCategoryId
	from
		@ProductCategoryLevel1 t inner join product.tCategory c with (nolock) on t.CategoryId = c.CategoryId

	-- Level 3 
	insert into @ProductCategoryLevel3 (ProductId, CategoryId)
	select
		t.ProductId,
		c.ParentCategoryId
	from
		@ProductCategoryLevel2 t inner join product.tCategory c with (nolock) on t.CategoryId = c.CategoryId

	select * from @ProductCategoryLevel1
	UNION ALL
	select * from @ProductCategoryLevel2
	UNION ALL
	select * from @ProductCategoryLevel3

END
GO
