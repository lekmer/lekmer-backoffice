SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeBrandDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].[tCartContainsConditionExcludeBrand]
	WHERE
		ConditionId = @ConditionId
END
GO
