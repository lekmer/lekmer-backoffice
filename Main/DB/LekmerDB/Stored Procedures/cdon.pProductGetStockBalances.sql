
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [cdon].[pProductGetStockBalances]
	@ArticleNumbers VARCHAR(4000)
AS 
BEGIN
	SELECT
		a.SepString AS ArticleNumber,
		COALESCE(ps.NumberInStock, p.NumberInStock) AS CurrentStockBalance
	FROM
		generic.fStringToStringTable(@ArticleNumbers, ',', 1) a
		LEFT JOIN lekmer.tProductSize ps ON a.SepString = ps.ErpId
		LEFT JOIN lekmer.tLekmerProduct lp ON SUBSTRING(a.SepString, 1, 12) = lp.HyErpId  -- [0008706-1017]-223  // SELECT SUBSTRING('0008706-1017-223', 1, 12)
		LEFT JOIN product.tProduct p ON lp.ProductId = p.ProductId
END
GO
