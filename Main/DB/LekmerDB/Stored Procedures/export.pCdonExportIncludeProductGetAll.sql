SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeProductGetAll]
AS
BEGIN
	SELECT
		[ip].CreatedDate 'CreatedDate',
		[ip].ProductId 'ItemId',
		[ip].ProductRegistryId 'ProductRegistryId',
		[ip].Reason 'Reason',
		[ip].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeProduct] ip
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ip].[ProductRegistryId]
END
GO
