SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorUpdate]
	@MaksuturvaCompensationErrorId	INT,
	@LastAttempt					DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tMaksuturvaCompensationError]
	SET	
		[LastAttempt] = @LastAttempt
	WHERE
		[MaksuturvaCompensationErrorId] = @MaksuturvaCompensationErrorId
END
GO
