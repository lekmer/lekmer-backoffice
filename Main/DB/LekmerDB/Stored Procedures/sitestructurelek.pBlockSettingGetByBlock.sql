SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSettingGetByBlock]
	@BlockId INT
AS
BEGIN
	SELECT 
		*		
	FROM
		[sitestructurelek].[vBlockSetting]
	WHERE
		[BlockSetting.BlockId] = @BlockId
END
GO
