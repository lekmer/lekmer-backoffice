SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistrySave]
	@SizeTableMediaProductRegistryId INT,
	@SizeTableMediaId INT,
	@ProductRegistryId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[lekmer].[tSizeTableMediaProductRegistry]
	SET 
		[SizeTableMediaId] = @SizeTableMediaId,
		[ProductRegistryId] = @ProductRegistryId
	WHERE
		[SizeTableMediaProductRegistryId] = @SizeTableMediaProductRegistryId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTableMediaProductRegistry](
			[SizeTableMediaId],
			[ProductRegistryId]
		)
		VALUES (
			@SizeTableMediaId,
			@ProductRegistryId
		)

		SET @SizeTableMediaProductRegistryId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableMediaProductRegistryId
END
GO
