SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pSupplierGetById]
	@Id INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		[s].*
	FROM 
		[productlek].[vSupplier] s
	WHERE
		[s].[Supplier.Id] = @Id
END
GO
