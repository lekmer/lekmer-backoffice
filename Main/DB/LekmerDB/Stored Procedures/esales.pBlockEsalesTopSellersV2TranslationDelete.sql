SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2TranslationDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[esales].[tBlockEsalesTopSellersV2Translation]
	WHERE
		[BlockId] = @BlockId
END
GO
