
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pOrderSearch]
	@OrderId		INT,
	@Page           INT = NULL,
	@PageSize       INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortAcsending	BIT = 0
AS
BEGIN

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN;
	END
		
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY ' 
		+ COALESCE(@SortBy, 'vO.[Order.CreatedDate]')
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS _Number,
		vO.*
		FROM [order].[vCustomOrder] vO'
		+ CASE WHEN (@OrderId IS NOT NULL) THEN + '
		WHERE vO.[Order.OrderId] LIKE ''%'' + CAST(@OrderId AS VARCHAR(10)) + ''%'' ' ELSE '' END 

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE _Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND _Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END

	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@OrderId		INT',
			@OrderId

	EXEC sp_executesql @sql, 
		N'	@OrderId		INT',
			@OrderId
END
GO
