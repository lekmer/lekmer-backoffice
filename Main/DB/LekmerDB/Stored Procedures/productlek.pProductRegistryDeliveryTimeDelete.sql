SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductRegistryDeliveryTimeDelete]
	@Id INT
AS 
BEGIN
	DELETE FROM [productlek].[tProductRegistryDeliveryTime]
	WHERE [DeliveryTimeId] = @Id
END
GO
