
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pCategoryExportToCdon]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
		
	--insert into @Tree values (1000445, 0, 1)	-- Barn och Baby	
	--insert into @Tree values (1000494, 0, 1)	-- Inredning
	--insert into @Tree values (1000533, 0, 1)	-- Leksaker
	--insert into @Tree values (1000856, 0, 1)	-- Underhållning
	
	insert into @Tree (CategoryId, Depth, HasChildren)
	select CategoryId, 0, 1
	from product.tCategory	
	where ParentCategoryId is null
	
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	declare @ProductCategory table
	(
		CategoryId int not null primary key,
		ProductsOnline int not null,
		ProductsOffline int not null,
		Products int not null
	)	

	insert into @ProductCategory (CategoryId, ProductsOnline, ProductsOffline, Products)	
	select CategoryId, SUM(case when ProductStatusId = 0 then 1 else 0 end), SUM(case when ProductStatusId = 0 then 0 else 1 end), COUNT(*)
	from product.tProduct p
	group by CategoryId	
	
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.ErpId, 
	   t.Depth,
	   isnull(pc.ProductsOnline, 0) as ProductsOnline,
	   isnull(pc.ProductsOffline, 0) as ProductsOffline,
	   isnull(pc.Products, 0) as Products,
	   c.Title, 
	   st.Title as SeTitle,
	   dt.Title as DkTitle,
	   nt.Title as NoTitle,
	   ft.Title as FiTitle
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		left outer join @ProductCategory pc on pc.CategoryId = t.CategoryId		
		left outer join product.tCategoryTranslation st on st.CategoryId = t.CategoryId
			and st.LanguageId = 1
		left outer join product.tCategoryTranslation dt on dt.CategoryId = t.CategoryId
			and dt.LanguageId = 1000002
		left outer join product.tCategoryTranslation nt on nt.CategoryId = t.CategoryId
			and nt.LanguageId = 1000001
		left outer join product.tCategoryTranslation ft on ft.CategoryId = t.CategoryId
			and ft.LanguageId = 1000003
end
GO
