SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductRecordGetAllByIdList]
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PriceListId INT
	SET @PriceListId = (SELECT TOP(1) [PriceListId] FROM [product].[tPriceList])

	SELECT
		[p].*,
		[pli].*
	FROM
		[product].[vCustomProductRecord] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl ON [pl].[Id] = [p].[Product.Id]
		LEFT JOIN [product].[vCustomPriceListItem] AS pli ON [pli].[Price.ProductId] = [p].[Product.Id] AND [pli].[Price.PriceListId] = @PriceListId
END
GO
