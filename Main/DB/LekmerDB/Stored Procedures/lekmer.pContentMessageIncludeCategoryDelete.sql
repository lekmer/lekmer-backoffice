SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeCategoryDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeCategory]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
