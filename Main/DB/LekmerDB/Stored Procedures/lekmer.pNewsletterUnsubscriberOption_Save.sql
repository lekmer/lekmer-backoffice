
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_Save]
	@UnsubscriberId INT,
	@NewsletterTypeId INT,
	@CreatedDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [lekmer].[tNewsletterUnsubscriberOption]
	(
		[UnsubscriberId],
		[NewsletterTypeId],
		[CreatedDate]
	)
	VALUES
	(
		@UnsubscriberId,
		@NewsletterTypeId,
		@CreatedDate
	)

	RETURN SCOPE_IDENTITY()
END
GO
