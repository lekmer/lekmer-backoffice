SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagsGetAllByCampaign]
	@CampaignId	INT
AS
BEGIN
	SELECT [pt].[ProductTagId]
	FROM [lekmer].[tProductTag] pt
		  INNER JOIN [lekmer].[tProductTagUsage] ptu ON [ptu].[ProductTagId] = [pt].[ProductTagId]
		  WHERE [ptu].[ObjectId] = @CampaignId
			    AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTagUsage] ptu1
								WHERE [ptu1].[ProductTagId] = [pt].[ProductTagId]
								HAVING COUNT(1) > 1)
END
GO
