
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [report].[pGiftCardVoucherCreatedStatistic]
	@DateFrom	DATETIME = NULL,
	@DateTo		DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @TempVouchersInfo TABLE (
		Campaign NVARCHAR(MAX),
		CampaignId INT,
		Channel VARCHAR(50),
		ChannelId INT,
		VouchersCountDay INT,
		VouchersSumDay DECIMAL(16,2)
	)

	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	IF @DateFrom IS NULL
		SET @DateFrom = CONVERT(DATE, DATEADD(DAY, -1, @CurrentDate))
	ELSE
		SET @DateFrom = CONVERT(DATE, @DateFrom)

	IF @DateTo IS NULL
		SET @DateTo = CONVERT(DATE, @CurrentDate)
	ELSE
		SET @DateTo = CONVERT(DATE, @DateTo)

	INSERT INTO @TempVouchersInfo
	SELECT
		ISNULL([c].[Title], 'CAMPAIGN HAS BEEN REMOVED. Discount Value:' + CAST(MAX([gc].[DiscountValue]) AS VARCHAR)),
		[gc].[CampaignId],
		[ch].[CommonName],
		[ch].[ChannelId],
		COUNT([gc].[VoucherCode]),
		SUM([gc].[DiscountValue])
	FROM
		[campaignlek].[tGiftCardViaMailInfo] gc
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [gc].[OrderId]
		INNER JOIN [core].[tChannel] ch ON [ch].[ChannelId] = [o].[ChannelId]
		LEFT JOIN [campaign].[tCampaign] c ON [c].[CampaignId] = [gc].[CampaignId]
	WHERE
		[gc].[VoucherInfoId] IS NOT NULL
		AND [gc].[VoucherCode] IS NOT NULL
		AND [gc].[DateToSend] >= @DateFrom
		AND [gc].[DateToSend] < @DateTo
	GROUP BY
		[c].[Title], [gc].[CampaignId], [ch].[CommonName], [ch].[ChannelId]
	ORDER BY
		[c].[Title], [ch].[CommonName]

	SELECT
		[tvi].[Campaign] 'Campaign',
		[tvi].[Channel] 'Channel',
		MAX([tvi].[VouchersCountDay]) 'Count of created vouchers (DAY)',
		MAX([tvi].[VouchersSumDay]) 'Sum of created vouchers (DAY)',
		COUNT([gc].[VoucherCode]) 'Count of created vouchers (TOTAL)',
		SUM([gc].[DiscountValue]) 'Sum of created vouchers (TOTAL)'
	FROM
		@TempVouchersInfo tvi
		INNER JOIN [campaignlek].[tGiftCardViaMailInfo] gc ON [gc].[CampaignId] = [tvi].[CampaignId]
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [gc].[OrderId] AND [o].[ChannelId] = [tvi].[ChannelId]
	WHERE
		[gc].[VoucherInfoId] IS NOT NULL
		AND [gc].[VoucherCode] IS NOT NULL
	GROUP BY
		[tvi].[Campaign], [tvi].[Channel]
	ORDER BY
		[tvi].[Campaign], [tvi].[Channel]
END
GO
