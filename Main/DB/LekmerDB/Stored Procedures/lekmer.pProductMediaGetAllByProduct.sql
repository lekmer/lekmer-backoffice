SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductMediaGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SELECT
		[i].*
	FROM 
		[product].[vCustomProductMedia] i
	WHERE
		i.[ProductMedia.ProductId] = @ProductId
	ORDER BY 
		[i].[ProductMedia.GroupId],
		[i].[ProductMedia.Ordinal]
END
GO
