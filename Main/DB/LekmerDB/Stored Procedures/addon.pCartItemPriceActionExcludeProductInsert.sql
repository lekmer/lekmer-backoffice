SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceActionExcludeProductInsert]
	@CartActionId			int,
	@ProductId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionExcludeProduct
	(
		CartActionId,
		ProductId
	)
	VALUES
	(
		@CartActionId,
		@ProductId
	)
END 



GO
