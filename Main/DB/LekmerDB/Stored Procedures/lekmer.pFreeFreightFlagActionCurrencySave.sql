SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pFreeFreightFlagActionCurrencySave]
	@ActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN 
	INSERT 
		[lekmer].[tFreeFreightFlagActionCurrency]
	( 
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES 
	(
		@ActionId,
		@CurrencyId,
		@Value
	)
END

GO
