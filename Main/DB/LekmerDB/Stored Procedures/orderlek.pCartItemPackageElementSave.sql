
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCartItemPackageElementSave]
	@CartItemPackageElementId INT,
	@CartItemId INT,
	@ProductId INT,
	@SizeId INT
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[orderlek].[tCartItemPackageElement]
	SET	
		[CartItemId] = @CartItemId,
		[ProductId] = @ProductId,
		[SizeId] = @SizeId
	WHERE
		[CartItemPackageElementId] = @CartItemPackageElementId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	INTO [orderlek].[tCartItemPackageElement]
				( [CartItemId],
				  [ProductId],
				  [SizeId]
           		)
		VALUES
				( @CartItemId,
				  @ProductId,
				  @SizeId
           		)

		SET @CartItemPackageElementId = SCOPE_IDENTITY()
	END
	
	RETURN @CartItemPackageElementId
END
GO
