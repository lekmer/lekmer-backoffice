SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pHourlyVisitorSave]
	@ChannelId int,
	@Alternate bit,
	@Year smallint,
	@Month tinyint,
	@Day tinyint,
	@Hour tinyint,
	@VisitCount int
as
begin
	update
		[statistics].[tHourlyVisitor]
	set
		[VisitCount] = [VisitCount] + @VisitCount
	where
		[ChannelId] = @ChannelId
		and [Alternate] = @Alternate
		and [Year] = @Year
		and [Month] = @Month
		and [Day] = @Day
		and [Hour] = @Hour

	if  @@rowcount = 0
	begin
		insert
			[statistics].[tHourlyVisitor]
		(
			[ChannelId],
			[Alternate],
			[Year],
			[Month],
			[Day],
			[Hour],
			[VisitCount]
		)
		values
		(
			@ChannelId,
			@Alternate,
			@Year,
			@Month,
			@Day,
			@Hour,
			@VisitCount
		)
	end
end

GO
