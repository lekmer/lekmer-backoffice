SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageTranslationDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [lekmer].[tContentMessageTranslation]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
