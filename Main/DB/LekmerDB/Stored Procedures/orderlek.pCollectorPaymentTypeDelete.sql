SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorPaymentTypeDelete]
	@CollectorPaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tCollectorPaymentType]
	WHERE [CollectorPaymentTypeId] = @CollectorPaymentTypeId
END

GO
