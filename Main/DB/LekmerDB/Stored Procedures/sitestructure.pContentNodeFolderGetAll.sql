SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeFolderGetAll]
@ChannelId int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentNode]
	where 
		[ContentNodeType.CommonName] = 'Folder'
		AND [ContentNode.ChannelId] = @ChannelId
	order by [ContentNode.Ordinal] asc
end

GO
