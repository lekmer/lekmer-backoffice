SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [messaging].[pRecipientCustomFieldDeleteAllByRecipientId]
@RecipientId			uniqueidentifier
as
begin

	delete from
		[messaging].[tRecipientCustomField]
	where
		RecipientId = @RecipientId

end





GO
