
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingTranslationTitleGetAll]
	@RatingId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rt.[RatingTranslation.RatingId] AS 'Id',
		rt.[RatingTranslation.LanguageId] AS 'LanguageId',
		rt.[RatingTranslation.Title] AS 'Value'
	FROM
	    [review].[vRatingTranslation] rt
	WHERE 
		rt.[RatingTranslation.RatingId] = @RatingId
	ORDER BY
		rt.[RatingTranslation.LanguageId]
END
GO
