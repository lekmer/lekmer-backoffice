SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pFlagTranslationSave]
@FlagId		int,
@LanguageId	int,
@Title		nvarchar(50)
AS
BEGIN
	UPDATE
		lekmer.tFlagTranslation
	SET
		[Title] = @Title
	WHERE
		[FlagId] = @FlagId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tFlagTranslation
		(
			[FlagId],
			[LanguageId],
			[Title]				
		)
		VALUES
		(
			@FlagId,
			@LanguageId,
			@Title
		)
	END
END

GO
