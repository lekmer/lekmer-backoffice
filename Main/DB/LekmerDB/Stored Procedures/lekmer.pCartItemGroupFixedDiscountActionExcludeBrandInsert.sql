SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
