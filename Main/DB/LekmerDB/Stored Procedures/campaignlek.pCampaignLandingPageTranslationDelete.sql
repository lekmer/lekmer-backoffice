SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationDelete]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [campaignlek].[tCampaignLandingPageTranslation]
	WHERE [CampaignLandingPageId] = @CampaignLandingPageId
END
GO
