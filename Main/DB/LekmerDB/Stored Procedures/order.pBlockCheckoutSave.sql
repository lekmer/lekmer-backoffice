SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [order].[pBlockCheckoutSave]
@BlockId				int,
@RedirectContentNodeId	int
AS	
BEGIN
		
	UPDATE
		[order].[tBlockCheckout]
	SET
		[RedirectContentNodeId]	= @RedirectContentNodeId
	WHERE
		[BlockId] = @BlockId
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			[order].[tBlockCheckout]
			(
				[BlockId],
				[RedirectContentNodeId]
			)
		VALUES
			(
				@BlockId,
				@RedirectContentNodeId
			)
	END
END
GO
