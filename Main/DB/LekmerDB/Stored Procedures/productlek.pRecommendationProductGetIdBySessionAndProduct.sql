SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pRecommendationProductGetIdBySessionAndProduct]
	@SessionId VARCHAR(50),
	@ProductId INT,
	@ItemsToReturn INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT TOP (@ItemsToReturn)
		rp.[RecommendationProduct.ProductId]
	FROM
		[productlek].[vRecommenderList] rl
		INNER JOIN [productlek].[vRecommenderItem] ri ON ri.[RecommenderItem.RecommenderListId] = rl.[RecommenderList.RecommenderListId]
		INNER JOIN [productlek].[vRecommendationProduct] rp ON rp.[RecommendationProduct.RecommenderItemId] = ri.[RecommenderItem.RecommenderItemId]
	WHERE
		rl.[RecommenderList.SessionId] = @SessionId
		AND
		ri.[RecommenderItem.ProductId] = @ProductId
	ORDER BY
		rp.[RecommendationProduct.Ordinal]
		
	IF @@ROWCOUNT > 0 RETURN;
	
	SELECT TOP (@ItemsToReturn)
		rp.[RecommendationProduct.ProductId]
	FROM
		[productlek].[vRecommenderList] rl
		INNER JOIN [productlek].[vRecommenderItem] ri ON ri.[RecommenderItem.RecommenderListId] = rl.[RecommenderList.RecommenderListId]
		INNER JOIN [productlek].[vRecommendationProduct] rp ON rp.[RecommendationProduct.RecommenderItemId] = ri.[RecommenderItem.RecommenderItemId]
	WHERE
		rl.[RecommenderList.SessionId] = @SessionId
		AND
		ri.[RecommenderItem.ProductId] IS NULL
	ORDER BY
		rp.[RecommendationProduct.Ordinal]
		
	IF @@ROWCOUNT > 0 RETURN;
	
	SELECT TOP (@ItemsToReturn)
		rp.[RecommendationProduct.ProductId]
	FROM
		[productlek].[vRecommenderList] rl
		INNER JOIN [productlek].[vRecommenderItem] ri ON ri.[RecommenderItem.RecommenderListId] = rl.[RecommenderList.RecommenderListId]
		INNER JOIN [productlek].[vRecommendationProduct] rp ON rp.[RecommendationProduct.RecommenderItemId] = ri.[RecommenderItem.RecommenderItemId]
	WHERE
		rl.[RecommenderList.SessionId] IS NULL
		AND
		ri.[RecommenderItem.ProductId] = @ProductId
	ORDER BY
		rp.[RecommendationProduct.Ordinal]
		
	IF @@ROWCOUNT > 0 RETURN;
	
	SELECT TOP (@ItemsToReturn)
		rp.[RecommendationProduct.ProductId]
	FROM
		[productlek].[vRecommenderList] rl
		INNER JOIN [productlek].[vRecommenderItem] ri ON ri.[RecommenderItem.RecommenderListId] = rl.[RecommenderList.RecommenderListId]
		INNER JOIN [productlek].[vRecommendationProduct] rp ON rp.[RecommendationProduct.RecommenderItemId] = ri.[RecommenderItem.RecommenderItemId]
	WHERE
		rl.[RecommenderList.SessionId] IS NULL
		AND
		ri.[RecommenderItem.ProductId] IS NULL
	ORDER BY
		rp.[RecommendationProduct.Ordinal]

END
GO
