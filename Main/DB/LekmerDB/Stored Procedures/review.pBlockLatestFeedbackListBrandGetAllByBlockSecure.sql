SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockLatestFeedbackListBrandSecure] b
	WHERE
		b.[BlockLatestFeedbackListBrand.BlockId] = @BlockId
END
GO
