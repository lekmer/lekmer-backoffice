SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeGetById]
	@SizeId	INT
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		*
	FROM 
		[lekmer].[vSize]
	WHERE
		[Size.Id] = @SizeId
END
GO
