SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pRecommendationProductSave]
	@RecommendationProductId INT,
	@RecommenderItemId INT,
	@ProductId INT,
	@Ordinal INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE 
		[productlek].[tRecommendationProduct]
	SET 
		[RecommenderItemId] = @RecommenderItemId,
		[ProductId] = @ProductId,
		[Ordinal] = @Ordinal
	WHERE
		[RecommendationProductId] = @RecommendationProductId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [productlek].[tRecommendationProduct] (
			[RecommenderItemId],
			[ProductId],
			[Ordinal]
		)
		VALUES (
			@RecommenderItemId,
			@ProductId,
			@Ordinal
		)

		SET @RecommendationProductId = SCOPE_IDENTITY()
	END

	RETURN @RecommendationProductId
END
GO
