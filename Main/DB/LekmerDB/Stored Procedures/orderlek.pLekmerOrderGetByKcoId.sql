SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pLekmerOrderGetByKcoId]
	@KcoId VARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Lekmer.KcoId] = @KcoId
		AND [OrderStatus.CommonName] = 'KcoPaymentPending'
END
GO
