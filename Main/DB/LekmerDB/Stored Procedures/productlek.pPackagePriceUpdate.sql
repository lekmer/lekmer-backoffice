SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackagePriceUpdate]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PackageProductPrice TABLE (
		PriceListId INT NOT NULL,
		PriceIncludingVat DECIMAL(16,2) NULL,
		PriceExcludingVat DECIMAL(16,2) NULL
	)

	-- cursor by packages
	DECLARE @PackageId INT, @MasterProductId INT
	
	DECLARE cur_package CURSOR FAST_FORWARD FOR
		SELECT [PackageId], [MasterProductId]
		FROM [productlek].[tPackage] p
	OPEN cur_package
	FETCH NEXT FROM cur_package INTO @PackageId, @MasterProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM @PackageProductPrice
		
		-- Fill table with product prices in all price lists
		INSERT INTO @PackageProductPrice ([PriceListId], [PriceIncludingVat], [PriceExcludingVat])
		SELECT [pl].[PriceListId], [pli].[PriceIncludingVat], [pli].[PriceExcludingVat]
		FROM
			[productlek].[tPackageProduct] pp
			CROSS JOIN [product].[tPriceList] pl
			LEFT JOIN [product].[tPriceListItem] pli ON [pli].[PriceListId] = [pl].[PriceListId] AND [pli].[ProductId] = [pp].[ProductId]
		WHERE
			[pp].[PackageId] = @PackageId

		-- Delete price lists where price is missing
		DELETE FROM @PackageProductPrice
		WHERE [PriceListId] IN (
			SELECT [tpp].[PriceListId] 
			FROM @PackageProductPrice tpp 
			WHERE [tpp].[PriceIncludingVat] IS NULL OR [tpp].[PriceExcludingVat] IS NULL)
			
		-- tPriceListItem
		IF EXISTS (SELECT 1 FROM @PackageProductPrice)
		BEGIN
			MERGE [product].[tPriceListItem] AS pli
			USING (SELECT 
						[PriceListId] 'PriceListId',
						@MasterProductId 'MasterProductId',
						SUM([PriceIncludingVat]) 'PriceIncludingVat',
						SUM([PriceExcludingVat]) 'PriceExcludingVat',
						CAST(((SUM([PriceIncludingVat])/SUM([PriceExcludingVat])) - 1.0)*100.0 AS DECIMAL(16,2)) 'VatPercentage'
					FROM @PackageProductPrice
					GROUP BY [PriceListId]) src 
			ON ([pli].[ProductId] = [src].[MasterProductId] AND [pli].[PriceListId] = [src].[PriceListId])
			
			WHEN MATCHED AND ([pli].[PriceIncludingVat] <> [src].[PriceIncludingVat] 
						 OR [pli].[PriceExcludingVat] <> [src].[PriceExcludingVat]
						 OR [pli].[VatPercentage] <> [src].[VatPercentage]) THEN
			UPDATE SET 
				[pli].[PriceIncludingVat] = [src].[PriceIncludingVat],
				[pli].[PriceExcludingVat] = [src].[PriceExcludingVat],
				[pli].[VatPercentage] = [src].[VatPercentage]
			
			WHEN NOT MATCHED BY TARGET THEN 
			INSERT ([PriceListId], [ProductId], [PriceIncludingVat], [PriceExcludingVat], [VatPercentage]) 
			VALUES ([src].[PriceListId], [src].[MasterProductId], [src].[PriceIncludingVat], [src].[PriceExcludingVat], [src].[VatPercentage])
			
			WHEN NOT MATCHED BY SOURCE AND [pli].[ProductId] = @MasterProductId THEN 
			DELETE
			
			--OUTPUT 
			--$ACTION,
			--ISNULL(INSERTED.[PriceListId], DELETED.[PriceListId]) AS PriceListId,
			--ISNULL(INSERTED.[ProductId], DELETED.[ProductId]) AS ProductId
			;
		END

		FETCH NEXT FROM cur_package INTO @PackageId, @MasterProductId
	END

	CLOSE cur_package
	DEALLOCATE cur_package
END
GO
