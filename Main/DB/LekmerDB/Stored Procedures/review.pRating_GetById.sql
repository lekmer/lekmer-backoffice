
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRating_GetById]
	@RatingId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRating] r
	WHERE
		r.[Rating.RatingId] = @RatingId
		AND
		r.[Rating.Channel.Id] = @ChannelId
END
GO
