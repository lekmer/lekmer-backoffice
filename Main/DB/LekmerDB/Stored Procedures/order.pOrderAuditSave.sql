SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderAuditSave]
	@OrderId int,
	@SystemUserId int,
	@OrderAuditTypeId int,
	@Information varchar(50),
	@Message text,
	@CreatedDate datetime
AS	
BEGIN
	SET NOCOUNT ON;
			
	INSERT INTO [order].[tOrderAudit] 
	(
		[OrderId]
       ,[SystemUserId]
       ,[OrderAuditTypeId]
       ,[Information]
       ,[Message]
       ,[CreatedDate]
    )
	VALUES
	(
		@OrderId,
		@SystemUserId,
		@OrderAuditTypeId,
		@Information,
		@Message,
		@CreatedDate
	)

	RETURN SCOPE_IDENTITY();
END
GO
