
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBrandPackageInfoTranslationGetAllByBrand]
	@BrandId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.PackageInfo] AS 'Value'
	FROM
	    [lekmer].[vBrandTranslation]
	WHERE
		[BrandTranslation.BrandId] = @BrandId
	ORDER BY
		[BrandTranslation.LanguageId]
END
GO
