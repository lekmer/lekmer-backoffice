SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateCategoryTitleLekmer]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE 
		@Seperator NVARCHAR(1),	
		@ErpCategoryPrefix NVARCHAR(2),
		
		@ErpClassId NVARCHAR(50),
		@ErpClassGroupId NVARCHAR(50),
		@ErpClassGroupCodeId NVARCHAR(50),
		@ClassGroupCodeId NVARCHAR(50),
		
		@ArticleClassId NVARCHAR(10),
		@ArticleClassTitle NVARCHAR(40),
		@ArticleGroupId NVARCHAR(10),
		@ArticleGroupTitle NVARCHAR(40),
		@ArticleCodeId NVARCHAR(10),
		@ArticleCodeTitle NVARCHAR(40)
	
	SET @Seperator = '-'
	SET @ErpCategoryPrefix = 'C_'
		
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT DISTINCT
			tp.ArticleClassId,
			tp.ArticleClassTitle,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle
		FROM
			[integration].tempProduct tp
		WHERE
			SUBSTRING(tp.HYarticleId, 1, 3) = 1 -- only se channel

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@ArticleClassId,
			@ArticleClassTitle,
			@ArticleGroupId,
			@ArticleGroupTitle,
			@ArticleCodeId,
			@ArticleCodeTitle

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @ClassGroupCodeId = @ArticleClassId + @Seperator + @ArticleGroupId + @Seperator + @ArticleCodeId

			BEGIN TRY
				BEGIN TRANSACTION
					SET @ErpClassId          = @ErpCategoryPrefix + @ArticleClassId
					SET @ErpClassGroupId     = @ErpCategoryPrefix + @ArticleClassId + @Seperator + @ArticleGroupId
					SET @ErpClassGroupCodeId = @ErpCategoryPrefix + @ArticleClassId + @Seperator + @ArticleGroupId + @Seperator + @ArticleCodeId

					-- varuklass
					IF EXISTS (SELECT 1 FROM [product].[tCategory] c WHERE [c].[ErpId] = @ErpClassId AND [c].[Title] != @ArticleClassTitle)
						UPDATE	[product].[tCategory]
						SET		[Title] = @ArticleClassTitle
						WHERE	[ErpId] = @ErpClassId
								AND
								[Title] != @ArticleClassTitle

					-- Varugrupp
					IF EXISTS (SELECT 1 FROM [product].[tCategory] c WHERE [c].[ErpId] = @ErpClassGroupId AND [c].[Title] != @ArticleGroupTitle)
						UPDATE	[product].[tCategory]
						SET		[Title] = @ArticleGroupTitle
						WHERE	[ErpId] = @ErpClassGroupId
								AND
								[Title] != @ArticleGroupTitle

					-- Varukod
					IF (EXISTS (SELECT 1 FROM [product].[tCategory] c WHERE [c].[ErpId] = @ErpClassGroupCodeId AND [c].[Title] != @ArticleCodeTitle)
						AND @ArticleCodeTitle <> '***Missing***' )
						UPDATE	[product].[tCategory]
						SET		[Title] = @ArticleCodeTitle
						WHERE	[ErpId] = @ErpClassGroupCodeId
								AND
								[Title] != @ArticleCodeTitle

				COMMIT
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0 ROLLBACK
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				VALUES(@ClassGroupCodeId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

				CLOSE cur_product
				DEALLOCATE cur_product
			END CATCH

			FETCH NEXT FROM cur_product
				INTO
					@ArticleClassId,
					@ArticleClassTitle,
					@ArticleGroupId,
					@ArticleGroupTitle,
					@ArticleCodeId,
					@ArticleCodeTitle
		END
	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
