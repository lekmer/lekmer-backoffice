SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCustomerOrderAlertByCivicNumber]
	@DateFrom	DATETIME = NULL,
	@DateTo		DATETIME = NULL
AS
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tTmpResult') IS NOT NULL
	DROP TABLE #tTmpResult
	CREATE TABLE #tTmpResult (
		[Date] DATE NOT NULL,
		[CivicNumber] NVARCHAR(50) NULL,
		[OrdersCount] INT NOT NULL,
		[Channel] NVARCHAR (50)
	)

	INSERT INTO #tTmpResult([Date], [CivicNumber], [OrdersCount], [Channel])
	SELECT
		CAST([a].[Date] AS DATE) 'Date',
		REPLACE(REPLACE([a].[CivicNumber], '-', ''),' ', '') 'CivicNumber',
		COUNT([a].[OrderId])	'OrdersCount',
		[a].[Channel]			'Channel'
	FROM (
		SELECT
			CAST(YEAR([o].[CreatedDate]) AS VARCHAR(4)) + '-' + CAST(MONTH([o].[CreatedDate]) AS VARCHAR(2)) + '-' + CAST(DAY([o].[CreatedDate]) AS VARCHAR(2)) 'Date',
			[c].[CivicNumber] 'CivicNumber',
			[o].[OrderId] 'OrderId',
			[ch].[CommonName] 'Channel'
		FROM 
		   [order].[tOrder] o
		   INNER JOIN [customer].[tCustomerInformation] c ON [c].[CustomerId] = [o].[CustomerId]
		   INNER JOIN [core].[tChannel] ch ON [ch].[ChannelId] = [o].[ChannelId]
		WHERE 
			[o].[OrderStatusId] = 4
			AND [o].[CreatedDate] > @DateFrom
			AND [o].[CreatedDate] < @DateTo
	) a
	GROUP BY
		[a].[Date], [a].[CivicNumber], [a].[Channel]
	ORDER BY
		[a].[Channel], [a].[Date]

	SELECT
		*
	FROM
		#tTmpResult r
	WHERE
		[r].[OrdersCount] > 3
		AND [r].[CivicNumber] IS NOT NULL
	ORDER BY [r].[Channel], [r].[Date]

	DROP TABLE #tTmpResult   
END
GO
