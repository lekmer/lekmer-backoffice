
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [statistics].[pTopSellerGetAll]
	@ChannelId int,
	@DateTimeFrom datetime,
	@DateTimeTo datetime,
	@TopCount int,
	@StatusIds		varchar(max)
as
begin
	declare @ProductIds table(ProductId int, Quantity int)
	
	insert
		@ProductIds
	(
		ProductId,
		Quantity
	)
	select top(@TopCount)
		p.ProductId,
		sum(oi.Quantity)
	from
		product.tProduct p
		inner join [order].tOrderItemProduct oip WITH(NOLOCK) on p.ProductId = oip.ProductId
		inner join [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		inner join [order].tOrder o WITH(NOLOCK) on oi.OrderId = o.OrderId
	where
		o.ChannelId = @ChannelId
		and o.CreatedDate between isnull(@DateTimeFrom, o.CreatedDate) and isnull(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
	group by
		p.ProductId
	order by
		sum(oi.Quantity) desc

	select
		p.*,
		pli.*,
		i.Quantity as 'TopSeller.Quantity'
	from
		@ProductIds as i 
		--inner join [product].[vCustomProduct] as p on p.[Product.Id] = i.ProductId
		--Need to include offline products
		inner join [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts] as p on p.[Product.Id] = i.ProductId
		inner join [product].[vCustomPriceListItem] as pli
			on pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[Product.ChannelId] = @ChannelId
	order by
		i.Quantity DESC
end
GO
