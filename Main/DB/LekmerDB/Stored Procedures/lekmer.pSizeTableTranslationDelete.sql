SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableTranslationDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [lekmer].[tSizeTableTranslation]
	WHERE [SizeTableId] = @SizeTableId
END
GO
