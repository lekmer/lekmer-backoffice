
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pTrackProductChangesLekmer]
AS 
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductChanges') IS NOT NULL 
		DROP TABLE #tProductChanges
	
	CREATE TABLE #tProductChanges (
		[ProductId] INT NOT NULL
		CONSTRAINT PK_#tProductChanges PRIMARY KEY ( [ProductId] )
	)

	INSERT	#tProductChanges ( [ProductId] )
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[integration].[tempProduct] tp
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
		UNION
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[integration].[tempProductPrice] tpp
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tpp].[HYarticleId], 5, 12)
	
	IF (SELECT COUNT(*) FROM [#tProductChanges]) < 10000
	BEGIN
		INSERT [productlek].[tProductChangeEvent] ( [ProductId], [EventStatusId], [CdonExportEventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] )
		SELECT [ProductId], 0, 0, GETDATE(), NULL, 'HY import' FROM [#tProductChanges]
	END
	
	DROP TABLE #tProductChanges
END
GO
