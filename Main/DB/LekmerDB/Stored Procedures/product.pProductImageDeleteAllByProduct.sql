SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [product].[pProductImageDeleteAllByProduct]
@ProductId INT
AS
BEGIN
	DELETE
		[product].[tProductImage]
	WHERE
		ProductId = @ProductId
END
GO
