SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_SearchByEmail]
	@ChannelId		INT,
	@Email			NVARCHAR(MAX),
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortAcsending	BIT = 1
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY ' 
		+ COALESCE(@SortBy, '[ns].[NewsletterUnsubscriber.Email]')
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS _Number,
		[ns].*
		FROM [lekmer].[vNewsletterUnsubscriber] ns
		WHERE [ns].[NewsletterUnsubscriber.Email] LIKE ''%'' + @Email + ''%''
		AND [ns].[NewsletterUnsubscriber.ChannelId] = @ChannelId'
		
	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE _Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND _Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT,
			@Email		NVARCHAR(MAX)',
			@ChannelId,
			@Email

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT,
			@Email		NVARCHAR(MAX)',
			@ChannelId,
			@Email
END
GO
