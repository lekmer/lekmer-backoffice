SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludeCategoryBrandDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tSizeTableIncludeCategoryBrand]
	WHERE [SizeTableId] = @SizeTableId
END
GO
