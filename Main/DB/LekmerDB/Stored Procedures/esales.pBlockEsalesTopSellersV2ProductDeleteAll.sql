SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2ProductDeleteAll]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[esales].[tBlockEsalesTopSellersV2Product]
	WHERE
		[BlockId] = @BlockId
END
GO
