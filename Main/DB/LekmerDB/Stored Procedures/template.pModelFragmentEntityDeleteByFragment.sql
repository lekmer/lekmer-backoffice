SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentEntityDeleteByFragment]
	@FragmentId INT
AS
begin
	set nocount ON
	DELETE [template].[tModelFragmentEntity] WHERE ModelFragmentId = @FragmentId
	RETURN SCOPE_IDENTITY()
end	




GO
