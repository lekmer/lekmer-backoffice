SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_OrderRowHYHeppo]
	@OrderNo	int
AS
begin
	set nocount on
	begin try
		begin transaction
		
		--declare @status int
		--set @status = 0
		
		select
			o.OrderItemId, p.Title, o.Quantity, p.ProductId,
			o.ActualPriceIncludingVat, o.OriginalPriceIncludingVat, coalesce(ois.ErpId, p.ErpId) ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
		where
			o.OrderId = @OrderNo
		
		/*
		-- IF the orderItem exists in tOrderItemSize the product is a Size product!
		if exists (select * 
						from lekmer.tOrderItemSize ois
							inner join [order].tOrderItem oi
								on ois.OrderItemId = oi.OrderItemId
							inner join [order].tOrder o
								on o.OrderId = oi.OrderId
							where o.OrderId = 1000325)--@OrderNo)  
		begin
			
			SELECT DISTINCT 
			o.OrderItemId, p.Title, o.Quantity, p.ProductId,
			o.ActualPriceIncludingVat, o.OriginalPriceIncludingVat, ois.ErpId
			FROM 
			[order].tOrderItem o
			inner join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			inner join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			WHERE 
			o.OrderId = 1000325--@OrderNo
			
            set @status = 1                  
		end
		
		-- if status is 0, which means that the product does not have Size
		-- than read Erpid from tOrderItemProduct
		else if @status = 0
		begin
			
			SELECT DISTINCT 
			o.OrderItemId, p.Title, o.Quantity, p.ProductId,
			o.ActualPriceIncludingVat, o.OriginalPriceIncludingVat, p.ErpId 
			FROM 
			[order].tOrderItem o
			inner join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			WHERE 
			o.OrderId = 1000325 --@OrderNo
				
		end
		*/
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
