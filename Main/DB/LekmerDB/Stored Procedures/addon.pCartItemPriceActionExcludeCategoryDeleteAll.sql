SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [addon].[pCartItemPriceActionExcludeCategoryDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionExcludeCategory
	where CartActionId = @CartActionId
end
GO
