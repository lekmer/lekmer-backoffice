SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pMissedSeoTexts]
AS 
BEGIN 
	DECLARE @tResult TABLE (
		[Id] INT,
		[ObjectType] NVARCHAR(50),
		[Site] NVARCHAR(50),
		[ObjectId] INT,
		[ObjectTitle] NVARCHAR(MAX),
		[Status] NVARCHAR(50),
		[ObjectErpIdUrl] NVARCHAR(MAX))
	DECLARE @RowsCount INT

	/*****************************/
	/*Page Footer SEO preparation*/
	/*****************************/
	
	-- 1006996 -- Categories se
	-- 1003531 -- Categories no
	-- 1005873 -- Categories dk
	-- 1004968 -- Categories fi
	-- 1012873 -- Categories nl

	-- 1000001 -- Home se
	-- 1000072 -- Home no
	-- 1005844 -- Home dk
	-- 1004966 -- Home fi
	-- 1012741 -- Home nl

	DECLARE @CategoriesCNId INT, @HomePageId INT
	DECLARE @tCategoriesCNId TABLE (Id INT, HomePageId INT)
	INSERT INTO @tCategoriesCNId ([Id], [HomePageId])
	VALUES  (1006996, 1000001), (1003531, 1000072), (1005873, 1005844), (1004968, 1004966), (1012873, 1012741)

	DECLARE @tUsedCNFirstLevelId TABLE (Id INT)
	DECLARE @ContentNodes TABLE (ContentNodeId INT, Title NVARCHAR(200), ContentNodeStatusId INT, SiteStructureRegistryId INT)

	DECLARE @CNFirstLevelId INT, @CNSecondLevelId INT


	-- Cursor by Categories CN in different channels
	DECLARE cnCategories_cursor CURSOR FOR 
		SELECT [Id], [HomePageId] FROM @tCategoriesCNId
			
	OPEN cnCategories_cursor
	FETCH NEXT FROM cnCategories_cursor INTO @CategoriesCNId, @HomePageId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Get first level CN
		SELECT TOP(1) @CNFirstLevelId = [cn].[ContentNodeId]
		FROM [sitestructure].[tContentNode] cn
		WHERE [cn].[ParentContentNodeId] = @CategoriesCNId
		ORDER BY [cn].[ContentNodeId]

		WHILE @CNFirstLevelId IS NOT NULL
		BEGIN
			-- Home page
			INSERT INTO @ContentNodes ([ContentNodeId], [Title], [ContentNodeStatusId], [SiteStructureRegistryId])
			SELECT [n].[ContentNodeId], [n].[Title], [n].[ContentNodeStatusId], [n].[SiteStructureRegistryId] 
			FROM [sitestructure].[tContentNode] n WHERE [n].[ContentNodeId] = @HomePageId

			-- First Level page
			INSERT INTO @ContentNodes ([ContentNodeId], [Title], [ContentNodeStatusId], [SiteStructureRegistryId])
			SELECT [n].[ContentNodeId], [n].[Title], [n].[ContentNodeStatusId], [n].[SiteStructureRegistryId] 
			FROM [sitestructure].[tContentNode] n WHERE [n].[ContentNodeId] = @CNFirstLevelId
			
			-- Cursor by second level CN
			DECLARE cn_cursor CURSOR FOR 
				SELECT [cn].[ContentNodeId]
				FROM [sitestructure].[tContentNode] cn
				WHERE [cn].[ParentContentNodeId] = @CNFirstLevelId
				ORDER BY [cn].[ContentNodeId]
					
			OPEN cn_cursor
			FETCH NEXT FROM cn_cursor INTO @CNSecondLevelId

			WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Second Level page
				INSERT INTO @ContentNodes ([ContentNodeId], [Title], [ContentNodeStatusId], [SiteStructureRegistryId])
				SELECT [n].[ContentNodeId], [n].[Title], [n].[ContentNodeStatusId], [n].[SiteStructureRegistryId] 
				FROM [sitestructure].[tContentNode] n WHERE [n].[ContentNodeId] = @CNSecondLevelId
				
				-- Fill @ContentNodes table with Third Level page
				INSERT INTO @ContentNodes ([ContentNodeId], [Title], [ContentNodeStatusId], [SiteStructureRegistryId])
				SELECT [n].[ContentNodeId], [n].[Title], [n].[ContentNodeStatusId], [n].[SiteStructureRegistryId] 
				FROM [sitestructure].[tContentNode] n WHERE [n].[ParentContentNodeId] = @CNSecondLevelId
				ORDER BY [n].[ContentNodeId]
				
				FETCH NEXT FROM cn_cursor INTO @CNSecondLevelId
			END

			CLOSE cn_cursor
			DEALLOCATE cn_cursor

			-- Get next first level category
			INSERT INTO @tUsedCNFirstLevelId ([Id]) VALUES (@CNFirstLevelId)
			SET @CNFirstLevelId = NULL
			
			SELECT TOP(1) @CNFirstLevelId = [cn].[ContentNodeId]
			FROM [sitestructure].[tContentNode] cn
			WHERE [cn].[ParentContentNodeId] = @CategoriesCNId AND [cn].[ContentNodeId] NOT IN (SELECT Id FROM @tUsedCNFirstLevelId)
			ORDER BY [cn].[ContentNodeId]
		END
		
		FETCH NEXT FROM cnCategories_cursor INTO @CategoriesCNId, @HomePageId
	END

	CLOSE cnCategories_cursor
	DEALLOCATE cnCategories_cursor



	/*************************/
	/*Product SEO preparation*/
	/*************************/

	-- 1 -- Language se
	-- 1000001 -- Language no
	-- 1000002 -- Language dk
	-- 1000003 -- Language fi
	-- 1000005 -- Language nl

	DECLARE @LanguageId INT
	DECLARE @tlanguageId TABLE (Id INT)
	INSERT INTO @tlanguageId ([Id])
	VALUES  (1),(1000001),(1000002),(1000003),(1000005)

	DECLARE @ProductIds TABLE (LanguageId INT, ProductId INT)

	-- Cursor by Languages
	DECLARE lang_cursor CURSOR FOR 
		SELECT [Id] FROM @tlanguageId
			
	OPEN lang_cursor
	FETCH NEXT FROM lang_cursor INTO @LanguageId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO @ProductIds
		SELECT @LanguageId, [p].[ProductId]
		FROM [product].[tProduct] p
		LEFT JOIN [product].[tProductSeoSettingTranslation] ss ON [ss].[ProductId] = [p].[ProductId] AND [ss].[LanguageId] = @LanguageId
		WHERE ([ss].[Title] IS NULL OR [ss].[Title] = '') OR ([ss].[Description] IS NULL OR [ss].[Description] = '')

		FETCH NEXT FROM lang_cursor INTO @LanguageId
	END

	CLOSE lang_cursor
	DEALLOCATE lang_cursor

---------------------------------------------------------------------------------------------

	/*****************/
	/*Page Footer SEO*/
	/*****************/
	INSERT INTO @tResult
	SELECT
			ROW_NUMBER() OVER(ORDER BY (SELECT NULL))
			,'Page Footer SEO'
			,[sr].[Title]
			,[cn].[ContentNodeId]
			,[cn].[Title]
			,[ns].[Title]
			,[generic].[fGetPageUrl]([cn].[ContentNodeId])
			--,[brt].[Content]								'Footer SEO text'
	FROM
		@ContentNodes cn
		INNER JOIN [sitestructure].[tContentPage] cp ON [cp].[ContentNodeId] = [cn].[ContentNodeId]
		INNER JOIN [sitestructure].[tContentNodeStatus] ns ON [ns].[ContentNodeStatusId] = [cn].[ContentNodeStatusId]
		INNER JOIN [sitestructure].[tSiteStructureRegistry] sr ON [sr].[SiteStructureRegistryId] = [cn].[SiteStructureRegistryId]
		OUTER APPLY
			(
				SELECT [b].[BlockId] 'BlockId'
				FROM [sitestructure].[tBlock] b
				WHERE [b].[BlockTypeId] = 1 AND [b].[ContentNodeId] = [cn].[ContentNodeId]
			) a0
		LEFT JOIN [sitestructure].[tBlockRichText] brt ON [brt].[BlockId] = [a0].[BlockId]
	WHERE
		([brt].[Content] IS NULL OR [brt].[Content] = '')
	ORDER BY
		[sr].[SiteStructureRegistryId],
		[sr].[StartContentNodeId]

	SET @RowsCount = (SELECT COUNT(1) FROM @tResult)
	/**********/
	/*Page SEO*/
	/**********/
	INSERT INTO @tResult
	SELECT
		@RowsCount + ROW_NUMBER() OVER(ORDER BY [sr].[SiteStructureRegistryId])
		,'Page SEO'
		,[sr].[Title]
		,[cn].[ContentNodeId]
		,[cn].[Title]
		,[s].[Title]
		,[generic].[fGetPageUrl]([cn].[ContentNodeId])
		--,ISNULL(ss.[Title], '')						'SEO Title'
		--,ISNULL(ss.[Description], '')					'SEO Description'
	FROM
		[sitestructure].[tContentNode] cn
		INNER JOIN [sitestructure].[tContentPage] cp ON [cp].[ContentNodeId] = [cn].[ContentNodeId]
		INNER JOIN [sitestructure].[tContentNodeStatus] s ON [s].[ContentNodeStatusId] = [cn].[ContentNodeStatusId]
		INNER JOIN [sitestructure].[tSiteStructureRegistry] sr ON [sr].[SiteStructureRegistryId] = [cn].[SiteStructureRegistryId]
		LEFT  JOIN [sitestructure].[tContentPageSeoSetting] ss ON [ss].[ContentNodeId] = [cn].[ContentNodeId]
	WHERE
		([ss].[Title] IS NULL OR [ss].[Title] = '') OR ([ss].[Description] IS NULL OR [ss].[Description] = '')
	ORDER BY
		[sr].[SiteStructureRegistryId],
		[sr].[StartContentNodeId]
		
	SET @RowsCount = (SELECT COUNT(1) FROM @tResult)
	/*************/
	/*Product SEO*/
	/*************/
	INSERT INTO @tResult
	SELECT
			@RowsCount + ROW_NUMBER() OVER(ORDER BY (SELECT NULL))
			,'Product SEO'
			,[c].[ApplicationName]
			,[p].[ProductId]
			,[p].[Title]
			,[s].[Title]
			,[p].[ErpId]
			--,[ss].[Title]			'SEO.Title'
			--,[ss].[Description]	'SEO.Description'
	FROM
		@ProductIds tp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [tp].[ProductId]
		INNER JOIN [product].[tProductStatus] s ON [s].[ProductStatusId] = [p].[ProductStatusId]
		INNER JOIN [core].[tChannel] c ON [c].[LanguageId] = [tp].[LanguageId]
		LEFT JOIN [product].[tProductSeoSettingTranslation] ss ON [ss].[ProductId] = [p].[ProductId] AND [ss].[LanguageId] = [tp].[LanguageId]
	ORDER BY
		[c].[ChannelId],
		[tp].[ProductId]
	
	SELECT * FROM @tResult ORDER BY Id
END
GO
