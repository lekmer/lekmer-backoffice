
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories] 
	FROM [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
