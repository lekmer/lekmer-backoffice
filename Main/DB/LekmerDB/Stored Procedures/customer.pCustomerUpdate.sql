SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [customer].[pCustomerUpdate]
@CustomerId			INT,
@CustomerStatusId	INT,
@CustomerRegistryId	INT
AS	
BEGIN
	SET NOCOUNT ON	
		BEGIN
			UPDATE
				[customer].[tCustomer]
			SET    
				[CustomerStatusId] = @CustomerStatusId,
				[CustomerRegistryId] = @CustomerRegistryId
			WHERE 
				[CustomerId] = @CustomerId
		END
	RETURN @CustomerId		
END

GO
