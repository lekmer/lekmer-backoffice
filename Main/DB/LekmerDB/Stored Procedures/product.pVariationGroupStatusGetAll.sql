SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationGroupStatusGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[product].[vCustomVariationGroupStatus]
END

GO
