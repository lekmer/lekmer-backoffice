SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderDelete]
	@SizeTableFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].[tSizeTableFolder]
	WHERE [SizeTableFolderId] = @SizeTableFolderId
END
GO
