SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pEsalesRegistryGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		er.*
	FROM
		[esales].[vEsalesRegistry] er
END
GO
