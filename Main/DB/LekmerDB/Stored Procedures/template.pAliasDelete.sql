SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [template].[pAliasDelete]
	@AliasId	int
AS
begin
	set nocount ON
	delete
		[template].[tAliasTranslation] 
	where
		[AliasId] = @AliasId

	delete
		[template].[tAlias] 
	where
		[AliasId] = @AliasId
end	





GO
