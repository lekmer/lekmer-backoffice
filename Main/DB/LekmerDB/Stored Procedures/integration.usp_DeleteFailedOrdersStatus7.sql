SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_DeleteFailedOrdersStatus7]

AS
begin
	set nocount on
	
	if object_id('tempdb..#tFailedOrdersStatus7') is not null
		drop table #tFailedOrdersStatus7
		
	
	create table #tFailedOrdersStatus7
	(
		OrderId int not null,
		OrderItemId int not null
		constraint PK_#tFailedOrdersStatus7 primary key(OrderId, OrderItemId)
	)
	
	begin try
		begin transaction
		
		insert into #tFailedOrdersStatus7(OrderId, OrderItemId)
		select distinct
			o.OrderId,
			oi.OrderItemId
		from
			(select * from [order].torder
			where Createddate < DATEADD(Day, -7, GETDATE())
			AND OrderstatusId = 7) o
				inner join [order].tOrderItem oi
					on o.OrderId = oi.OrderId
				inner join [order].tOrderItemProduct oip
					on oip.OrderItemId = oi.OrderItemId
				inner join lekmer.tOrderItemSize ois
					on ois.OrderItemId = oip.OrderItemId
				inner join [order].tOrderPayment op
					on op.OrderId = o.OrderId
				inner join lekmer.tLekmerOrder lo
					on lo.OrderId = o.OrderId

		--select * from #tFailedOrdersStatus7
		
		delete from lekmer.tLekmerOrder
		where OrderId in (select OrderId
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table lekmer.tLekmerOrder'
		
		delete from [order].tOrderPayment
		where OrderId in (select OrderId
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table [order].tOrderPayment'
							
		delete from lekmer.tOrderItemSize
		where OrderItemId in (select OrderItemId 
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table lekmer.tOrderItemSize'
		
		delete from [order].tOrderItemProduct
		where OrderItemId in (select OrderItemId 
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table [order].tOrderItemProduct'
		
		delete from [order].tOrderItem 
		where OrderId in (select OrderId
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table [order].tOrderItem '
		
		delete from [order].tOrderCartCampaign
		where OrderId in (select OrderId
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table [order].tOrderCartCampaign '
		
		delete from [order].tOrder
		where OrderId in (select OrderId
							from #tFailedOrdersStatus7)
		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table [order].tOrder'
		
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
