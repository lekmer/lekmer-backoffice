SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetAllByContentNodeAndContentArea]
	@LanguageId int,
	@ContentNodeId	int,
	@ContentAreaId	int
as
begin
	select 
		B.*		
	from 
		[sitestructure].[vCustomBlock] as B
	where
		[Block.ContentNodeId] = @ContentNodeId AND 
		[Block.ContentAreaId] = @ContentAreaId AND 
		[Block.LanguageId] = @LanguageId
	order by [Block.Ordinal]
end

GO
