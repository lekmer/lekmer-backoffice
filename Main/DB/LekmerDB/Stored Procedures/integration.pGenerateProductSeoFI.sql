
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateProductSeoFI]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO [product].[tProductSeoSetting] (ProductId)
		SELECT
			[ProductId]
		FROM
			[product].[tProduct]
		WHERE
			[ProductId] NOT IN (SELECT [ProductId] FROM [product].[tProductSeoSetting])
				
		-- DEFAULT --
		DECLARE @LanguageId INT
		SET @LanguageId = 1000003 -- Finland
		
		INSERT INTO [product].[tProductSeoSettingTranslation] (ProductId, LanguageId)
		SELECT
			[p].[ProductId],
			@languageId
		FROM
			[product].[tProduct] p
		WHERE
			NOT EXISTS (SELECT 1 FROM [product].[tProductSeoSettingTranslation] n WHERE [n].[ProductId] = [p].[ProductId] AND [n].[LanguageId] = @LanguageId)


		------------------------------------------------------------
		-- <Produktnivå: Start Leksaker>
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = 'Osta ' + ISNULL(pt.Title, p.Title) + ' – Lekmer.fi - verkkokaupasta',
			pss.[Description] = 'Osta ' + ISNULL(ct.Title, c.Title) + ' toimintahahmo netistä. '
			+ 'Löydät myös muut lelut ja lastentarvikkeet valmistajalta '+ ISNULL(bt.Title, b.Title) + ' Lekmer.fi - verkkokaupasta.' 
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			------------------------------------------------------------------
			INNER JOIN product.tCategoryTranslation ct ON c.CategoryId = ct.CategoryId
		WHERE
			c3.CategoryId = 1000533 -- Leksaker
			AND pss.LanguageId = @LanguageId
			AND ct.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Barn & Baby>
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = ISNULL(pt.Title, p.Title) + ' - Lekmer.fi – Osta lastentarvikkeet netistä',
			pss.[Description] = 'Osta netistä ' + ISNULL(pt.Title, p.Title) + ' ' + ISNULL(bt.Title, b.Title)
			+ ' Löydät muut ' + ISNULL(bt.Title, b.Title) + ' lasten- ja vauvantuotteet verkkokaupasta Lekmer.fi' 
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId	
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
		WHERE
			c3.CategoryId = 1000445 -- Barn och Baby
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Barnkläder>  
		------------------------------------------------------------
		UPDATE
			pss
		SET   
			pss.Title = ISNULL(pt.Title, p.Title) + ' ' + ISNULL(ct.Title, c.Title) + ' ' + ISNULL(ct2.Title, c2.Title) 
			+ ' - Lekmer.fi – Lasten- ja vauvanvaatteet netistä. Lekmer.fi - Lasten- ja vauvanvaatteet netistä.',
			pss.[Description] = 'Osta ' + ISNULL(pt.Title, p.Title) + ' ' + ISNULL(ct2.Title, c2.Title) + ' ja ' + ISNULL(ct.Title, c.Title) + ' ' + 'netistä. '
			+ 'Löydät myös Lekmer.fi- verkkopaupasta muita lastenvaatteita samalta tuottajalta, ' + ISNULL(bt.Title, b.Title)
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			------------------------------------------------------------------
			INNER JOIN product.tCategoryTranslation ct ON c.CategoryId = ct.CategoryId
			INNER JOIN product.tCategoryTranslation ct2 ON c2.CategoryId = ct2.CategoryId
		WHERE
			c3.CategoryId = 1001310 -- Barnkläder
			AND pss.LanguageId = @LanguageId
			AND ct.LanguageId = @LanguageId
			AND ct2.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Inredning>  
		------------------------------------------------------------
		UPDATE
			pss
		SET  
			pss.Title = ISNULL(pt.Title, p.Title) + ' netistä. Lekmer.fi - verkkokaupasta',
			pss.[Description] = 'Osta ' + ISNULL(pt.Title, p.Title) + ' netistä. '
			+ 'Löydät myös muut ' + ISNULL(bt.Title, b.Title) + ' ' + 'sisustustuotteet ja lastenvaatteet Lekmer.fi - kaupasta.' 
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
		WHERE
			c3.CategoryId = 1000494 -- inredning
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
