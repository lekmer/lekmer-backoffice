SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryDelete]
	@SizeTableMediaProductRegistryId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[lekmer].[tSizeTableMediaProductRegistry]
	WHERE
		[SizeTableMediaProductRegistryId] = @SizeTableMediaProductRegistryId
END
GO
