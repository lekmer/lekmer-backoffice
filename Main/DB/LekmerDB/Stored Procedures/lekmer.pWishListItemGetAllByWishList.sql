SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pWishListItemGetAllByWishList]
	@WishListId int
as
begin

	select *
	from
		[lekmer].[tWishListItem] 
	where
		WishListId = @WishListId		
end

GO
