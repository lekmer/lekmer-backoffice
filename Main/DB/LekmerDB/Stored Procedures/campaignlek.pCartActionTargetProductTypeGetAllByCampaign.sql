SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartActionTargetProductTypeGetAllByCampaign]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[at].*
	FROM
		[campaignlek].[vCartActionTargetProductType] at
		INNER JOIN [campaign].[tCartAction] pa ON [pa].[CartActionId] = [at].[CartActionTargetProductType.CartActionId]
	WHERE
		[pa].[CampaignId] = @CampaignId
END
GO
