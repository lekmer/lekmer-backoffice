SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2GetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		*
	FROM
		[esales].[vBlockEsalesTopSellersV2Secure]
	WHERE
		[BlockEsalesTopSellersV2.BlockId] = @BlockId
END
GO
