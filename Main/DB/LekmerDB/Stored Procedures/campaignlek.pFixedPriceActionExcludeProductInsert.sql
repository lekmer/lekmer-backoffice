SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionExcludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END

GO
