SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 19.09.2008		Time: 17:00
Description:
			Created 

*****************  Version 2  *****************
User: Yuriy P.		Date: 06.09.2008		Time: 10:00
Description:
			Change logic (Modified Preorder Tree Traversal)
*/
CREATE procedure [template].[pIncludeFolderDelete]
	@IncludeFolderId	int
AS
begin
	set nocount on

	DELETE FROM
		template.tIncludeFolder
	WHERE
		IncludeFolderId = @IncludeFolderId
end	



GO
