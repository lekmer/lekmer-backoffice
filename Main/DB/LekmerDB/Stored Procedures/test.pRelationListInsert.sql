SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE test.pRelationListInsert
	@Title nvarchar(50),
	@RelationListTypeId int
as
begin
	insert
		product.tRelationList
	(
		Title,
		RelationListTypeId
	)
	values
	(
		@Title,
		@RelationListTypeId
	)

	return scope_identity()
end
GO
