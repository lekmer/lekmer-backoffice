SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [order].[pOrderItemStatusGetByCommonName]
	@CommonName	varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM
		[order].[vCustomOrderItemStatus]
	WHERE
		[OrderItemStatus.CommonName] = @CommonName
END


GO
