
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductGetIdAll]
	@ConfigId	INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCampaignActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [aep].[ProductId] = [p].[ProductId]
	WHERE
		[aep].[ConfigId] = @ConfigId
		AND [p].[IsDeleted] = 0
END
GO
