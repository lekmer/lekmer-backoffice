SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [messaging].[pRecipientCustomFieldInsert]
@RecipientId			uniqueidentifier,
@Name				nvarchar(200),
@Value				nvarchar(MAX)
as
begin

	insert into
		[messaging].[tRecipientCustomField]
		(
			RecipientId,
			[Name],
			[Value]
		)
	values
		(
			@RecipientId,
			@Name,
			@Value
		)

end





GO
