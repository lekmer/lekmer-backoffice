SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagGet]
AS
BEGIN
	SELECT TOP(1)
		[Id],
		[CampaignId],
		[ProcessingDate]
	FROM
		[campaignlek].[tCampaignToTag]
	WHERE
		[ProcessingDate] IS NULL
		OR [ProcessingDate] < GETDATE()
	ORDER BY [Id]
END
GO
