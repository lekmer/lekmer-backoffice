SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateGetDefaultByModelAndTheme]
@ModelCommonName	varchar(50),
@ThemeId			int
AS
BEGIN
	SELECT
		T.*
	FROM
		[template].[vCustomTemplate] T
		INNER JOIN [template].[vCustomModel] M ON M.[Model.Id] = T.[Template.ModelId]
		INNER JOIN [template].[vCustomThemeModel] TM ON T.[Template.Id] = TM.[ThemeModel.TemplateId] AND M.[Model.Id] = TM.[ThemeModel.ModelId]
	WHERE
        M.[Model.CommonName] = @ModelCommonName
        AND TM.[ThemeModel.ThemeId] = @ThemeId
END

GO
