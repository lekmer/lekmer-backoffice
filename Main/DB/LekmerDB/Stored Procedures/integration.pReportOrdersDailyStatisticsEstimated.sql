
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportOrdersDailyStatisticsEstimated]
	@CurrentDateTime	datetime = null,
	@ChannelId			int = null,
	@EstimatedEnabled	bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	declare @Summary int
	declare @SummaryTimeTo datetime

	if @EstimatedEnabled = 0
	begin
		select 0 as 'NumberOfOrders'
		return
	end


	create table #tChannel
	(
		ChannelId	int not null
	)
	
	if @ChannelId is not null and @ChannelId > 0
	begin
		--print 'single channel'
		insert into #tChannel (ChannelId) values (@ChannelId)
	end
	else if @ChannelId = 0
	begin
		--print 'lekmer'
		insert into #tChannel (ChannelId) values (1)
		insert into #tChannel (ChannelId) values (2)
		insert into #tChannel (ChannelId) values (3)
		insert into #tChannel (ChannelId) values (4)
		insert into #tChannel (ChannelId) values (1000005)
	end	


	if @CurrentDateTime is null
		set @CurrentDateTime = getdate()
	
	declare @zerotime datetime
	set @zerotime = cast(convert(varchar(10), @CurrentDateTime, 120) + ' 00:00:00' as datetime)

	if cast(convert(varchar(10), getdate(), 120) as datetime) <> cast(convert(varchar(10), @CurrentDateTime, 120) as datetime)
	begin
		select @Summary =
			count(*)
		from
			[order].tOrder o
		where
			o.ChannelId in (select ChannelId from #tChannel) and
			o.CreatedDate >= @zerotime and
			o.CreatedDate < dateadd(day, 1, @zerotime) and
			o.OrderStatusId in (2, 4)

		set @SummaryTimeTo = dateadd(minute, 1439, @zerotime)
	end	
	else begin
		set @CurrentDateTime = getdate()

		declare @0DaysAgo datetime
		set @0DaysAgo = cast(convert(varchar(10), @CurrentDateTime, 120) + ' 00:00:00' as datetime)
		declare @7DaysAgo datetime
		set @7DaysAgo = cast(convert(varchar(10), dateadd(week, -1, @CurrentDateTime), 120) + ' 00:00:00' as datetime)
		declare @14DaysAgo datetime
		set @14DaysAgo = cast(convert(varchar(10), dateadd(week, -2, @CurrentDateTime), 120) + ' 00:00:00' as datetime)


		-- one week ago (full 0)
		declare @7DaysAgoOrdersFull0 int
		select
			@7DaysAgoOrdersFull0 = count(*)
		from
			[Order].tOrder
		where
			ChannelId in (select ChannelId from #tChannel) and -- JE
			CreatedDate >= @7DaysAgo and CreatedDate < dateadd(day, 1, @7DaysAgo) and
			OrderStatusId in (2, 4)


		-- two weeks ago (full 1)
		declare @14DaysAgoOrdersFull1 int
		select
			@14DaysAgoOrdersFull1 = count(*)
		from
			[order].tOrder
		where
			ChannelId in (select ChannelId from #tChannel) and -- JE
			CreatedDate >= @14DaysAgo and CreatedDate < dateadd(day, 1, @14DaysAgo) and
			OrderStatusId in (2, 4)


		-- number of orders from current time for one week ago
		declare @7DaysAgoOrdersPart0 int
		select
			@7DaysAgoOrdersPart0 = count(*)
		from
			[order].tOrder
		where
			ChannelId in (select ChannelId from #tChannel) and -- JE
			CreatedDate >= @7DaysAgo and CreatedDate < dateadd(week, -1, @CurrentDateTime) and
			OrderStatusId in (2, 4)


		-- number of orders from current time for two weeks ago
		declare @14DaysAgoOrdersPart1 int
		select
			@14DaysAgoOrdersPart1 = count(*)
		from
			[order].tOrder
		where
			ChannelId in (select ChannelId from #tChannel) and -- JE
			CreatedDate >= @14DaysAgo and CreatedDate < dateadd(week, -2, @CurrentDateTime) and
			OrderStatusId in (2, 4)

		-- number of orders today until now
		declare @NumberOfOrdersToday int
		select
			@NumberOfOrdersToday = count(*)
		from
			[order].tOrder
		where
			ChannelId in (select ChannelId from #tChannel) and -- JE
			CreatedDate >= @0DaysAgo and CreatedDate < dateadd(day, 1, @0DaysAgo) and
			OrderStatusId in (2, 4)

		-- summary
		if ((@7DaysAgoOrdersPart0 + @14DaysAgoOrdersPart1) <> 0)
			select @Summary = cast(@NumberOfOrdersToday / (cast(@7DaysAgoOrdersPart0 + @14DaysAgoOrdersPart1 as decimal(18,2)) / cast(@7DaysAgoOrdersFull0 + @14DaysAgoOrdersFull1 as decimal(18,2))) as int)
		else
			select @Summary = 0


		set @SummaryTimeTo = @CurrentDateTime
	end

	-- growth between now and one year behind
	declare @OneYearBehindFrom datetime
	declare @OneYearBehindTo datetime
	declare @SummaryOneYear int

	set @OneYearBehindFrom = replace(cast(convert(varchar, dateadd(week, -52, @CurrentDateTime), 102) as varchar), '.', '-') + ' 00:00:00'
	set @OneYearBehindTo = dateadd(minute, 1439, @OneYearBehindFrom)
	
	select @SummaryOneYear =
		count(*)
	from
		[order].tOrder o
	where
		o.ChannelId in (select ChannelId from #tChannel) and
		o.CreatedDate between @OneYearBehindFrom and @OneYearBehindTo and
		o.OrderStatusId in (2, 4)

	---------------
	declare @SummaryOneYearTime int
	declare @SummaryCurrentTime int

	select @SummaryOneYearTime =
			count(*)
		from
			[order].tOrder o
		where
			o.ChannelId in (select ChannelId from #tChannel) and
			o.CreatedDate >= dateadd(week, -52, @zerotime) and
			o.CreatedDate <= dateadd(week, -52, @SummaryTimeTo) and
			o.OrderStatusId in (2, 4)

	select @SummaryCurrentTime =
			count(*)
		from
			[order].tOrder o
		where
			o.ChannelId in (select ChannelId from #tChannel) and
			o.CreatedDate >= @zerotime and
			o.CreatedDate <= @SummaryTimeTo and
			o.OrderStatusId in (2, 4)
	---------------

	select
		@Summary as 'NumberOfOrders',
		case when @SummaryOneYear > 0 then cast((((cast(@Summary as decimal(12,2)) / cast(@SummaryOneYear as decimal(12,2))) - 1.00)) as decimal(12,4)) else 0 end as 'GrowthEstimated',
		@SummaryOneYear as 'LastYearNumberOfOrdersEstimated',
		case when @SummaryOneYearTime > 0 then cast((((cast(@SummaryCurrentTime as decimal(12,2)) / cast(@SummaryOneYearTime as decimal(12,2))) - 1.00)) as decimal(12,4)) else 0 end as 'Growth'

END
GO
