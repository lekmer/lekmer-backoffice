SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pPriceListCustomerGroupDeleteAll]
@PriceListId		int
AS
BEGIN
	DELETE FROM 
		product.tPriceListCustomerGroup
	WHERE
		PriceListId = @PriceListId
END
GO
