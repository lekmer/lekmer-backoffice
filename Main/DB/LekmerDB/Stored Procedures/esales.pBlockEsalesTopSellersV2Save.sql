SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2Save]
	@BlockId				INT,
	@PanelName				NVARCHAR(MAX),
	@WindowLastEsalesValue	INT,
	@IncludeAllCategories	BIT,
	@LinkContentNodeId		INT = NULL,
	@CustomUrl				NVARCHAR(MAX) = NULL,
	@UrlTitle				NVARCHAR(MAX) = NULL
AS
BEGIN
	UPDATE
		[esales].[tBlockEsalesTopSellersV2]
	SET
		[PanelName]				= @PanelName,
		[WindowLastEsalesValue] = @WindowLastEsalesValue,
		[IncludeAllCategories]	= @IncludeAllCategories,
		[LinkContentNodeId]		= @LinkContentNodeId,
		[CustomUrl]				= @CustomUrl,
		[UrlTitle]				= @UrlTitle
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [esales].[tBlockEsalesTopSellersV2] (
		[BlockId],
		[PanelName],
		[WindowLastEsalesValue],
		[IncludeAllCategories],
		[LinkContentNodeId],
		[CustomUrl],
		[UrlTitle]
	)
	VALUES (
		@BlockId,
		@PanelName,
		@WindowLastEsalesValue,
		@IncludeAllCategories,
		@LinkContentNodeId,
		@CustomUrl,
		@UrlTitle
	)
END
GO
