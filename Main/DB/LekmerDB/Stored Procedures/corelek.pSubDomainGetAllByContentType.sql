SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [corelek].[pSubDomainGetAllByContentType]
	@ContentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[corelek].[vSubDomain] sd
	WHERE
		[sd].[SubDomain.ContentTypeId] = @ContentTypeId
END
GO
