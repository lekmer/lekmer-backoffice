SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2CategoryGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[esales].[vBlockEsalesTopSellersV2CategorySecure]
	WHERE
		[BlockEsalesTopSellersV2Category.BlockId] = @BlockId
END
GO
