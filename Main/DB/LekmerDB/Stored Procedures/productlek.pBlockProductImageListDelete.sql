SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM [productlek].[tBlockProductImageList]
	WHERE BlockId = @BlockId
END
GO
