SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pDealOfTheDayByHyId]
	@HyId		nvarchar(100),
	@BlockId	int
	--@ProductXml		int --xml
AS
begin
	set nocount on
	
	/*if object_id('tempdb..#tAdventskalender') is not null
		drop table #tAdventskalender

	create table #tAdventskalender
	(
		ProductId int not null,
		--Title nvarchar (250) COLLATE Finnish_Swedish_CI_AS not null,
		constraint PK_#tProductUrls primary key(ProductId)
	)*/
	
	begin try
		--begin transaction
		
		/*insert #tAdventskalender(ProductId)		
		select 
		T.c.value('@id[1]', 'int')
		--T.c.value('@title[1]', 'nvarchar(250)'),
		--T.c.value('@languageid[1]', 'int')
		from @ProductXml.nodes('/products/product') T(c)*/
		
		declare @ProductId int
		set @ProductId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HyId)
		
		if @ProductId is not null
		-- kolla att den inte e dealof the day redan
		and @ProductId <> (select ProductId from product.tBlockProductListProduct where BlockId = @BlockId)
		begin
			update
				b
			set
				ProductId = @ProductId
			from
				product.tBlockProductListProduct b
			where
				BlockId = @BlockId
		end
		
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
