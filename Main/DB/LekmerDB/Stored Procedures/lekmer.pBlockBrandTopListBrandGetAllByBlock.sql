SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListBrand] b
	WHERE
		b.[BlockBrandTopListBrand.BlockId] = @BlockId
	ORDER BY
		[BlockBrandTopListBrand.Position]
END
GO
