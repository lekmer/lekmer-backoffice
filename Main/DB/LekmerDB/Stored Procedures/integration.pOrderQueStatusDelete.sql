SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pOrderQueStatusDelete]
	@OrderId int
	
AS
begin
	--set nocount on
	begin try
		begin transaction
				
			delete from
				[integration].[tOrderQue]				
			where
				OrderId = @OrderId
			
		commit transaction	
	end try
	begin catch
		if @@trancount > 0 rollback transaction

		insert into [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
