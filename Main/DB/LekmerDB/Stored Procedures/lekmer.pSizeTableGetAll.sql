
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[st].*
	FROM
		[lekmer].[vSizeTableSecure] st
END
GO
