
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pProductPriceGetAllIncorrectPackagePrice]
AS 
BEGIN 
	DECLARE @tPackage TABLE (
		PackageHYID VARCHAR(50),
		ChannelId INT,
		PackagePrice DECIMAL(16,2),
		SumProductsPrice DECIMAL(16,2),
		OriginalPackagePrice DECIMAL(16,2),
		SumOriginalProductsPrice DECIMAL(16,2))
	DECLARE @tPackageOriginal TABLE (
		PackageHYID VARCHAR(50),
		ChannelId INT,
		OriginalPackagePrice DECIMAL(16,2),
		SumOriginalProductsPrice DECIMAL(16,2))

	DECLARE @PackageId INT, @MasterProductId INT, @ChannelId INT
	DECLARE @PackageHYID VARCHAR(50)
	DECLARE @PackagePrice DECIMAL(16,2), @SumProductsPrice DECIMAL(16,2), @OriginalPackagePrice DECIMAL(16,2), @SumOriginalProductsPrice DECIMAL(16,2)

	DECLARE channel_cursor CURSOR FOR SELECT [ChannelId] FROM [core].[tChannel]
	OPEN channel_cursor
	FETCH NEXT FROM channel_cursor INTO @ChannelId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE package_cursor CURSOR FOR 
			SELECT [pac].[PackageId], [pac].[MasterProductId], [p].[ErpId]
			FROM [productlek].[tPackage] pac
			INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [pac].[MasterProductId]
			WHERE [p].[ProductStatusId] = 0
			ORDER BY [pac].[PackageId]

		OPEN package_cursor
		FETCH NEXT FROM package_cursor INTO @PackageId, @MasterProductId, @PackageHYID

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @PackagePrice = NULL
			SET @PackagePrice = (	SELECT ISNULL([pp].[DiscountPriceIncludingVat], [pp].[PriceIncludingVat])
									FROM [export].[tProductPrice] pp
									WHERE [ProductId] = @MasterProductId AND [pp].[ChannelId] = @ChannelId)

			SET @SumProductsPrice = NULL
			SET @SumProductsPrice = (	SELECT SUM(ISNULL([pp].[DiscountPriceIncludingVat], [pp].[PriceIncludingVat])) 
										FROM [export].[tProductPrice] pp
										INNER JOIN [productlek].[tPackageProduct] p ON [p].[ProductId] = [pp].[ProductId]
										WHERE [p].[PackageId] = @PackageId AND [pp].[ChannelId] = @ChannelId)

			SET @OriginalPackagePrice = NULL
			SET @OriginalPackagePrice = (	SELECT [pp].[PriceIncludingVat]
											FROM [export].[tProductPrice] pp
											WHERE [ProductId] = @MasterProductId AND [pp].[ChannelId] = @ChannelId)

			SET @SumOriginalProductsPrice = NULL
			SET @SumOriginalProductsPrice = (	SELECT SUM([pp].[PriceIncludingVat]) 
												FROM [export].[tProductPrice] pp
												INNER JOIN [productlek].[tPackageProduct] p ON [p].[ProductId] = [pp].[ProductId]
												WHERE [p].[PackageId] = @PackageId AND [pp].[ChannelId] = @ChannelId)

			IF (@PackagePrice IS NOT NULL 
				AND @SumProductsPrice IS NOT NULL
				AND @PackagePrice >= @SumProductsPrice)
			BEGIN
				INSERT INTO @tPackage ([PackageHYID],[ChannelId],[PackagePrice],[SumProductsPrice],[OriginalPackagePrice],[SumOriginalProductsPrice])
				VALUES (@PackageHYID, @ChannelId, @PackagePrice, @SumProductsPrice, @OriginalPackagePrice, @SumOriginalProductsPrice)
			END


			IF (@OriginalPackagePrice IS NOT NULL 
				AND @SumOriginalProductsPrice IS NOT NULL
				AND @OriginalPackagePrice >= @SumOriginalProductsPrice)
			BEGIN
				INSERT INTO @tPackageOriginal ([PackageHYID],[ChannelId],[OriginalPackagePrice],[SumOriginalProductsPrice])
				VALUES (@PackageHYID, @ChannelId, @OriginalPackagePrice, @SumOriginalProductsPrice)
			END

			FETCH NEXT FROM package_cursor INTO @PackageId, @MasterProductId, @PackageHYID
		END

		CLOSE package_cursor
		DEALLOCATE package_cursor

		FETCH NEXT FROM channel_cursor INTO @ChannelId
	END
	CLOSE channel_cursor
	DEALLOCATE channel_cursor

	SELECT
		[c].[CommonName] 'Channel',
		[p].[PackageHYID] 'HYErpId',
		[p].[PackagePrice] 'WebPackagePrice',
		[p].[SumProductsPrice] 'WebSumPackageProductPrices',
		[p].[OriginalPackagePrice] 'OriginalPackagePrice',
		[p].[SumOriginalProductsPrice] 'OriginalSumPackageProductPrices',
		[c1].[Title] 'Category 1 level',
		[c2].[Title] 'Category 2 level',
		[c3].[Title] 'Category 3 level'
	FROM
		@tPackage p
		INNER JOIN [core].[tChannel] c ON [c].[ChannelId] = [p].[ChannelId]
		INNER JOIN [product].[tProduct] pr ON [pr].[ErpId] = [p].[PackageHYID]
		INNER JOIN [product].[tCategory] c3 ON [c3].[CategoryId] = [pr].[CategoryId]
		INNER JOIN [product].[tCategory] c2 ON [c2].[CategoryId] = [c3].[ParentCategoryId]
		INNER JOIN [product].[tCategory] c1 ON [c1].[CategoryId] = [c2].[ParentCategoryId]
	WHERE
		[PackagePrice] >= [SumProductsPrice]
	--SELECT * FROM @tPackageOriginal WHERE [OriginalPackagePrice] > [SumOriginalProductsPrice]
END
GO
