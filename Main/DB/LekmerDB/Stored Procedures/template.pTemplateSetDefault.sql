SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
***********************  Version 4  **********************
User: Roman D.		Date: 16/01/2009		Time: 15:00
Description: delete try - cath  block and error logging 
**********************************************************
*****************  Version 3  *****************
User: Roman A.		Date: 04.12.2008
Description:
			removed transaction
			removed return value
************************************************

*****************  Version 3  *****************
User: Roman D..		Date: 26.11.2008		Time: 18:30
Description:
			refactoring default template
************************************************

*****************  Version 2  *****************
User: Victor E.		Date: 18.18.2008		Time: 15:40
Description:
			delete channel id 
************************************************

*****************  Version 1  *****************
User: Victor Egorov.		Date: 20.10.2008		Time: 11:00
Description:
	Created
*/

CREATE procedure [template].[pTemplateSetDefault]
	@ModelId	int,
	@TemplateId	int
as
begin
		update
			[template].[tModel]
		set
			[DefaultTemplateId] = (CASE WHEN @TemplateId=-1 then NULL ELSE @TemplateId end)
		where
			[ModelId] = @ModelId			
end














GO
