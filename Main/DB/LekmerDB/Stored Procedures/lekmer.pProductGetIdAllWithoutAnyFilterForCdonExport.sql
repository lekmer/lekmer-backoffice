
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilterForCdonExport]
	@Page INT = NULL,
	@PageSize INT,
	@MonthPurchasedAgo INT
AS
BEGIN
	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId]) EXEC [export].[pProductGetIdAllAvaliableForCdonExport] @MonthPurchasedAgo

	SELECT COUNT(DISTINCT ([up].[ProductId]))
	FROM (SELECT * FROM #ProductIds) up

	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY [up].[ProductId]) AS Number,
			[up].[ProductId]
		FROM (SELECT * FROM #ProductIds) up
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
		
	DROP TABLE #ProductIds
END
GO
