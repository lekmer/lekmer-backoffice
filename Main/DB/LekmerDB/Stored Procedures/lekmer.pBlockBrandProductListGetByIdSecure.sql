
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandProductListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SELECT 
		[ba].*,
		[b].*,
		[bs].*,
		[ps].*
	FROM 
		[lekmer].[tBlockBrandProductList] ba
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON ba.[BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
		INNER JOIN product.vCustomProductSortOrder ps ON ps.[ProductSortOrder.Id] = ba.[ProductSortOrderId]
	WHERE
		ba.[BlockId] = @BlockId
END
GO
