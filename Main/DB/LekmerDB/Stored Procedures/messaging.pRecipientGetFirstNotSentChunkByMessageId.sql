SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [messaging].[pRecipientGetFirstNotSentChunkByMessageId]
@MessageId	uniqueidentifier
as
begin

	declare @Chunk as int
	set @Chunk = (
				select
					min(Chunk)
				from
					[messaging].[tRecipient]
				where
					MessageId = @MessageId and
					BatchId is null
	)

	select
		@Chunk as Chunk

	select 
		RecipientId,
		MessageId,
		BatchId,
		Chunk,
		[Address]
	from 
		[messaging].[tRecipient]
	where
		MessageId = @MessageId and
		BatchId is null and
		Chunk = @Chunk

	select 
		RecipientId,
		[Name],
		[Value]
	from 
		[messaging].[tRecipientCustomField]
	where
		RecipientId in (
			select 
				RecipientId
			from 
				[messaging].[tRecipient]
			where
				MessageId = @MessageId and
				BatchId is null and
				Chunk = @Chunk
		)

end
GO
