SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pBlockProductFilterSave]
	@BlockId int,
	@SecondaryTemplateId int = null,
	@DefaultCategoryId int = null,
	@DefaultPriceIntervalId int = null,
	@DefaultAgeIntervalId int = NULL,
	@DefaultSizeId int = NULL,
	@ProductListCookie NVARCHAR(50) = NULL,
	@DefaultSort NVARCHAR(50) = null
as	
begin
	
	update
		lekmer.[tBlockProductFilter]
	set
		SecondaryTemplateId	= @SecondaryTemplateId,
		DefaultCategoryId = @DefaultCategoryId,
		DefaultPriceIntervalId = @DefaultPriceIntervalId,
		DefaultAgeIntervalId = @DefaultAgeIntervalId,
		DefaultSizeId = @DefaultSizeId,
		ProductListCookie =@ProductListCookie,
		DefaultSort =@DefaultSort
	where
		[BlockId] = @BlockId
	
	if @@ROWCOUNT = 0
	begin
		insert
			lekmer.[tBlockProductFilter]
		(
			[BlockId],
			SecondaryTemplateId,
			DefaultCategoryId,
			DefaultPriceIntervalId,
			DefaultAgeIntervalId,
			DefaultSizeId,
			ProductListCookie,
			DefaultSort
		)
		values
		(
			@BlockId,
			@SecondaryTemplateId,
			@DefaultCategoryId,
			@DefaultPriceIntervalId,
			@DefaultAgeIntervalId,
			@DefaultSizeId,
			@ProductListCookie,
			@DefaultSort
		)
	end
end
GO
