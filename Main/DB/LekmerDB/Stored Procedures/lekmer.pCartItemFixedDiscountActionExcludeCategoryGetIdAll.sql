
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemFixedDiscountActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
