SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSettingTypeSave]
    @ModelSettingTypeId INT,
    @Title NVARCHAR(50),
    @CommonName VARCHAR(50),
    @Ordinal INT
AS 
BEGIN
    SET NOCOUNT ON
    
    UPDATE
		[template].[tModelSettingType]
    SET
		[Title] = @Title,
		[CommonName] = @CommonName,
		[Ordinal] = @Ordinal
    WHERE
		[ModelSettingTypeId] = @ModelSettingTypeId
    
    IF @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tModelSettingType]
        (
          [ModelSettingTypeId],
          [Title],
          [CommonName],
          [Ordinal]
	    )
        VALUES  
        (
          @ModelSettingTypeId,
          @Title,
          @CommonName,
          @Ordinal
        )

        SET @ModelSettingTypeId = SCOPE_IDENTITY()
    END
    
    RETURN @ModelSettingTypeId
END
GO
