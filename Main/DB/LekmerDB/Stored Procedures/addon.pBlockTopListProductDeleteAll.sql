SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure addon.pBlockTopListProductDeleteAll
	@BlockId int
as
begin
	delete addon.tBlockTopListProduct
	where BlockId = @BlockId
end
GO
