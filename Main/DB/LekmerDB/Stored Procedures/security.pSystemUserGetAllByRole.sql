SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserGetAllByRole]
@RoleId int
as
begin
	set nocount on

	select
		su.*
	from
		[security].[vCustomSystemUser] AS su
		INNER JOIN [security].[tUserRole] ur ON su.[SystemUser.Id] = ur.SystemUserId
	where 
		ur.RoleId = @RoleId
end

GO
