SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandSave]
	@BlockId INT,
	@BrandId INT,
	@Position INT
AS
BEGIN
	UPDATE [lekmer].[tBlockBrandTopListBrand]
	SET
		[Position] = @Position
	WHERE
		[BlockId] = @BlockId
		AND [BrandId] = @BrandId
		
	IF @@ROWCOUNT <> 0 RETURN
		
	INSERT [lekmer].[tBlockBrandTopListBrand] (
		[BlockId],
		[BrandId],
		[Position]
	)
	VALUES (
		@BlockId,
		@BrandId,
		@Position
	)
END
GO
