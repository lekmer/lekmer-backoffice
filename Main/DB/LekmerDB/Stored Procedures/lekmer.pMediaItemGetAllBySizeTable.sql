
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMediaItemGetAllBySizeTable]
	@ChannelId INT,
	@SizeTableId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		m.*
	FROM
		[media].[vCustomMedia] m
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[MediaId] = [m].[Media.Id]
		INNER JOIN [lekmer].[tSizeTableMediaProductRegistry] stmr ON stmr.[SizeTableMediaId] = stm.[SizeTableMediaId]
		INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.[ProductRegistryId] = stmr.[ProductRegistryId]
	WHERE
		stm.[SizeTableId] = @SizeTableId
		AND
		pmc.[ChannelId] = @ChannelId
END
GO
