SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSettingDelete]
	@BlockId INT
AS
BEGIN
	DELETE [sitestructurelek].[tBlockSetting]
	WHERE [BlockId] = @BlockId
END
GO
