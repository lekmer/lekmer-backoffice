SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupFolderGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerGroupFolder]
END

GO
