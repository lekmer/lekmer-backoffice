
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pContentMessageIncludeBrandDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeCategoryDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeProductDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeSupplierDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageTranslationDelete] @ContentMessageId
		
	DELETE FROM [lekmer].[tContentMessage]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
