SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionIncludeGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT 
		[CustomerGroupId]
	FROM 
		[campaignlek].[tCustomerGroupConditionInclude] 
	WHERE 
		[ConditionId] = @ConditionId 
END
GO
