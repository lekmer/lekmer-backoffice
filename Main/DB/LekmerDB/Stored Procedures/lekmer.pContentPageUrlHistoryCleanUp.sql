
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryCleanUp]
	@MaxCount INT
AS
BEGIN
	DECLARE 
		@ContentPageId INT,
		@Difference INT,
		@UrlHistoryCount INT

	DECLARE cur_urlHistory CURSOR FAST_FORWARD FOR
		SELECT
			cpuh.ContentPageId
		FROM
			[lekmer].[tContentPageUrlHistory] cpuh
		GROUP BY ContentPageId

	OPEN cur_urlHistory
	FETCH NEXT FROM cur_urlHistory
		INTO
			@ContentPageId
			
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @UrlHistoryCount = (SELECT COUNT(1) FROM [lekmer].[tContentPageUrlHistory]
								WHERE ContentPageId = @ContentPageId)
									  
		SET @Difference = @UrlHistoryCount - @MaxCount
		
		IF @Difference > 0
		BEGIN
			DELETE UrlHistory FROM 
			(SELECT TOP(@Difference) * 
			 FROM [lekmer].[tContentPageUrlHistory] 
			 WHERE ContentPageId = @ContentPageId
			 ORDER BY LastUsageDate ASC) UrlHistory
		END
	
		FETCH NEXT FROM cur_urlHistory
		INTO
			@ContentPageId
	END
	
	CLOSE cur_urlHistory
	DEALLOCATE cur_urlHistory
END
GO
