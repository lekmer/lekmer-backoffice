SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pSupplierGetByNo]
	@SupplierNo VARCHAR(50)
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		[s].*
	FROM 
		[productlek].[vSupplier] s
	WHERE
		[s].[Supplier.SupplierNo] = @SupplierNo
END
GO
