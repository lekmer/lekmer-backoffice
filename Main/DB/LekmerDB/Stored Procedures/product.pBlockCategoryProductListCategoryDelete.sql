SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockCategoryProductListCategoryDelete]
@BlockId				int,
@CategoryId				int
as
begin
	delete from 
		[product].[tBlockCategoryProductListCategory]
	where
		[CategoryId] = @CategoryId
		and [BlockId] = @BlockId
end



GO
