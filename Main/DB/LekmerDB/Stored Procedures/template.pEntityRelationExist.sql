SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityRelationExist] 
	@EntityId INT
AS 
BEGIN
    SET NOCOUNT ON	

    IF (
		EXISTS ( SELECT  *
                FROM    [template].[tEntityFunction]
                WHERE   [EntityId] = @EntityId )
        OR
        
        EXISTS ( SELECT  *
                FROM    [template].[tModelFragmentEntity]
                WHERE   [EntityId] = @EntityId))
        RETURN 1

    RETURN 0
END
GO
