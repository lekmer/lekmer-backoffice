SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeSupplierDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeSupplier]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
