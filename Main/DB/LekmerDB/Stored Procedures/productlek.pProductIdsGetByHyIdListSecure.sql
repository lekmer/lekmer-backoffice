SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductIdsGetByHyIdListSecure]
	@ProductHYIds	VARCHAR(MAX),
	@Delimiter		CHAR(1) = ','
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT
		[p].[Product.Id],
		[p].[Product.ErpId]
	FROM
		[product].[vCustomProductRecord] p
		INNER JOIN [generic].[fnConvertIDListToTableString](@ProductHYIds, @Delimiter) AS pl ON [pl].[ID] = [p].[Product.ErpId]
END
GO
