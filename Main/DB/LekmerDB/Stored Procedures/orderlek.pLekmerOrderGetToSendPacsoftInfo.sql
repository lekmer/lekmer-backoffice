
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pLekmerOrderGetToSendPacsoftInfo]
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[o].*
	FROM
		[order].[vCustomOrder] o
		INNER JOIN [orderlek].[tPacsoftOrder] po ON [po].[OrderId] = [o].[Order.OrderId]
	WHERE
		[o].[Order.ChannelId] = @ChannelId
		AND [po].[NeedSendPacsoftInfo] = 1
END
GO
