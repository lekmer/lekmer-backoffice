SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pAliasSave]
	@AliasId		int,
	@AliasFolderId	int,	
	@Description	nvarchar(max),
	@CommonName		varchar(100),
	@Value			nvarchar(max),
	@AliasTypeId	int
	
as
begin
	set nocount ON
		
	update
		[template].[tAlias]
	set
		[AliasFolderId] = @AliasFolderId,
		[Description] = @Description,
		[CommonName] = @CommonName,
		[AliasTypeId] = @AliasTypeId,
		[Value] = @Value	
	where
		[AliasId] = @AliasId
		
	if @@ROWCOUNT = 0 
	begin
		declare @insertedAliasId int
		insert into [template].[tAlias]
		(
			[AliasFolderId],
			[Description],
			[CommonName],
			[AliasTypeId],
			[Value]				
		)
		values
		(
			@AliasFolderId,
			@Description,
			@CommonName,
			@AliasTypeId,
			@Value
		)
		
		set @AliasId = scope_identity()
	end
	return @AliasId
end

GO
