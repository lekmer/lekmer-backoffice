SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pSubPaymentTypeGetByPayment]
	@PaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[tSubPaymentType]
	WHERE
		[PaymentTypeId] = @PaymentTypeId
END
GO
