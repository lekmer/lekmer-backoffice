SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerCartUpdate]
	@CartGuid			UNIQUEIDENTIFIER,
	@UpdatedDate		DATETIME,
	@RemovedItemsCount	INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[order].tCart
	SET
		[UpdatedDate] = @UpdatedDate,
		[RemovedItemsCount] = [RemovedItemsCount] + @RemovedItemsCount
	WHERE
		[CartGuid] = @CartGuid
END
GO
