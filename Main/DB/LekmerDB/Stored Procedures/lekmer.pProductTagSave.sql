
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagSave]
	@ProductId	INT,
	@TagId		INT,
	@ObjectId	INT
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @TagId)
		EXEC [lekmer].[pProductTagInsert] @ProductId, @TagId

	EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, @ObjectId	
END 
GO
