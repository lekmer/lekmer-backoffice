SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTableMedia]
	@SizeTableMediaId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[lekmer].[tSizeTableMediaProductRegistry]
	WHERE
		[SizeTableMediaId] = @SizeTableMediaId
END
GO
