SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListStatusGetById]
@PriceListStatusId int
as
begin
	select
		*
	from
		[product].[vCustomPriceListStatus] pls
	where 
		pls.[PriceListStatus.Id] = @PriceListStatusId
end

GO
