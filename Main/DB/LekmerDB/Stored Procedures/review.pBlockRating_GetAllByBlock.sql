SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRating_GetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT 
		br.*
	FROM 
		[review].[vBlockRating] AS br
	WHERE
		br.[BlockRating.BlockId] = @BlockId
END
GO
