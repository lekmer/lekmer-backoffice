
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductInsert]
	@ProductId			INT,
	@BrandId			INT,
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	-- Get Product Ids that need add to product.tProductRegistryProduct.
	DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

	IF @ProductId IS NOT NULL
	BEGIN
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT @ProductId, ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter)
	END

	IF @BrandId IS NOT NULL
	BEGIN
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
			SELECT p.ProductId, pr.ID
			FROM (SELECT ProductId FROM lekmer.tLekmerProduct WHERE BrandId = @BrandId) p,
				 [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
			SELECT p.ProductId, pr.ID
			FROM (SELECT ProductId FROM product.tProduct WHERE CategoryId IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))) p,
				 [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END


	EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
END
GO
