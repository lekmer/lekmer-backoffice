SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pBlockTopListCategorySave]
	@BlockId int,
	@CategoryId int,
	@IncludeSubcategories bit
as
begin
	update addon.tBlockTopListCategory
	set
		IncludeSubcategories = @IncludeSubcategories
	where
		BlockId = @BlockId
		and CategoryId = @CategoryId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert addon.tBlockTopListCategory
	(
		BlockId,
		CategoryId,
		IncludeSubcategories
	)
	values
	(
		@BlockId,
		@CategoryId,
		@IncludeSubcategories
	)
end
GO
