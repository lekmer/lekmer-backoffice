SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pBlockProductFeedbackDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockProductFeedback]
	WHERE BlockId = @BlockId
END
GO
