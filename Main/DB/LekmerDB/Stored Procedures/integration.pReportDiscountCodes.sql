
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportDiscountCodes]
	@ChannelId INT,
	@StartDate DATETIME,
	@EndDate DATETIME,
	@BatchId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @DiscountCode TABLE
	(
		BatchId INT,
		BatchDescription NVARCHAR(250),
		VoucherCode NVARCHAR(100),
		OrderId INT,
		ChannelId INT,
		OrderValueBeforeVoucher DECIMAL(16, 2),
		OrderValueAfterVoucher DECIMAL(16, 2),
		VoucherTypeId INT,
		VoucherValue DECIMAL(16, 2),
		VoucherValueOriginal DECIMAL(16, 2)
	)
	
	IF @StartDate IS NULL SET @StartDate = '2010-01-01'
	IF @EndDate IS NULL SET @EndDate = '2020-01-01'
	
	INSERT INTO @DiscountCode
	SELECT
		vi.[VoucherInfoId],
		vi.[Description],		
		v.[VoucherCode],
		o.[OrderId],
		o.[ChannelId],
		NULL, --[OrderValueBeforeVoucher]
		op.[Price] + o.[FreightCost] + lo.[PaymentCost],
		vi.[DiscountTypeId],
		ovi.[AmountUsed],
		vi.[DiscountValue]
	FROM
		[order].[tOrder] o
		INNER JOIN [lekmer].[tLekmerOrder] lo ON lo.[OrderId] = o.[OrderId]
		INNER JOIN [order].[tOrderPayment] op ON [op].[OrderId] = [o].[OrderId]
		INNER JOIN [orderlek].[tOrderVoucherInfo] ovi ON [ovi].[OrderId] = [o].[OrderId]
		INNER JOIN [Voucher_live].[product].[tVoucher] v ON v.[VoucherCode] = ovi.[VoucherCode]
		INNER JOIN [Voucher_live].[product].[tVoucherInfo] vi ON vi.[VoucherInfoId] = v.[VoucherInfoId]
	WHERE
		(o.[ChannelId] = @ChannelId OR @ChannelId IS NULL)
		AND
		o.[CreatedDate] BETWEEN @StartDate AND @EndDate
		AND
		(
			vi.[VoucherInfoId] = @BatchId OR @BatchId IS NULL
		)
		AND
		o.[OrderStatusId] = 4 -- in HY


	DECLARE	@OrderBatchId INT, @OrderId INT, @VoucherTypeId INT
	DECLARE	@CampaignId INT, @MonetaryValue DECIMAL
	
	DECLARE cur_order CURSOR FAST_FORWARD FOR
		SELECT [BatchId] , [OrderId], [VoucherTypeId] FROM @DiscountCode
		
	OPEN cur_order
	FETCH NEXT FROM cur_order INTO @OrderBatchId , @OrderId, @VoucherTypeId
		
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		-- Campaign doesn't exist
		IF NOT EXISTS (
			SELECT 1
			FROM
				[order].[tOrderCartCampaign] occ
				INNER JOIN [campaign].[tCondition] cond ON [cond].[CampaignId] = [occ].[CampaignId]
				INNER JOIN [lekmer].[tVoucherCondition] vc ON [cond].[ConditionId] = [vc].[ConditionId]
			WHERE
				occ.[OrderId] = @OrderId
		)		
		BEGIN
			GOTO WHILE_END
		END

		-- 1. Regular voucher campaign

		IF EXISTS(
			SELECT
				1
			FROM
				[order].[tOrderCartCampaign] occ
				INNER JOIN [campaign].[tCondition] cond ON [cond].[CampaignId] = [occ].[CampaignId]
				INNER JOIN [lekmer].[tVoucherCondition] vc ON [cond].[ConditionId] = [vc].[ConditionId]
			WHERE
				vc.[IncludeAllBatchIds] = 1
				AND
				occ.[OrderId] = @OrderId
		)
		BEGIN
			IF @VoucherTypeId IN (2,3) -- fixed and gift card
			BEGIN
				UPDATE @DiscountCode
				SET
					[OrderValueBeforeVoucher] = [OrderValueAfterVoucher] + [VoucherValue]
				WHERE
					[OrderId] = @OrderId
			END
			ELSE
			IF @VoucherTypeId = 1 -- percentage
			BEGIN
				UPDATE @DiscountCode
				SET
					[VoucherValue] = [VoucherValueOriginal] -- %
				WHERE
					[OrderId] = @OrderId
			END
			
			GOTO WHILE_END
		END


		-- 2. Custom voucher campaign
		
		SET @CampaignId = (
			SELECT TOP 1
				occ.[CampaignId]
			FROM
				[order].[tOrderCartCampaign] occ
				INNER JOIN [campaign].[tCondition] cond ON [cond].[CampaignId] = [occ].[CampaignId]
				INNER JOIN [lekmer].[tVoucherCondition] vc ON [cond].[ConditionId] = [vc].[ConditionId]
				INNER JOIN [lekmer].[tVoucherBatchesInclude] vbi ON [vc].[ConditionId] = [vbi].[ConditionId]
			WHERE
				occ.[OrderId] = @OrderId
				AND
				vbi.[BatchId] = @OrderBatchId
		)
		
		IF @CampaignId IS NULL
		BEGIN
			GOTO WHILE_END
		END
		
		-- Get action 'Fixed discount'
		SET @MonetaryValue = (
			SELECT
				cur.[MonetaryValue]
			FROM
				[campaign].[tCartAction] ca
				INNER JOIN [campaignlek].[tFixedDiscountCartActionCurrency] cur ON cur.[CartActionId] = ca.[CartActionId]
				INNER JOIN [core].[tChannel] c ON c.[CurrencyId] = cur.[CurrencyId]
				INNER JOIN [order].[tOrder] o ON o.[ChannelId] = c.[ChannelId]
			WHERE
				ca.[CampaignId] = @CampaignId
				AND
				o.[OrderId] = @OrderId
		)
		
		IF @MonetaryValue IS NOT NULL
		BEGIN
			UPDATE @DiscountCode
			SET
				[OrderValueBeforeVoucher] = [OrderValueAfterVoucher] + @MonetaryValue,
				[VoucherValueOriginal] = @MonetaryValue,
				[VoucherValue] = @MonetaryValue
			WHERE
				[OrderId] = @OrderId
			
			GOTO WHILE_END
		END
		
		-- Get action 'Cart item group fixed discount'
		SET @MonetaryValue = (
			SELECT
				cur.[MonetaryValue]
			FROM
				[campaign].[tCartAction] ca
				INNER JOIN [lekmer].[tCartItemGroupFixedDiscountActionCurrency] cur ON cur.[CartActionId] = ca.[CartActionId]
				INNER JOIN [core].[tChannel] c ON c.[CurrencyId] = cur.[CurrencyId]
				INNER JOIN [order].[tOrder] o ON o.[ChannelId] = c.[ChannelId]
			WHERE
				ca.[CampaignId] = @CampaignId
				AND
				o.[OrderId] = @OrderId
		)
		
		IF @MonetaryValue IS NOT NULL
		BEGIN
			UPDATE @DiscountCode
			SET
				[OrderValueBeforeVoucher] = [OrderValueAfterVoucher] + @MonetaryValue,
				[VoucherValueOriginal] = @MonetaryValue,
				[VoucherValue] = @MonetaryValue
			WHERE
				[OrderId] = @OrderId

			GOTO WHILE_END
		END
		
		
		-- Get action 'Percentage discount'
		SET @MonetaryValue = (
			SELECT
				pd.[DiscountAmount]
			FROM
				[campaign].[tCartAction] ca
				INNER JOIN [campaignlek].[tPercentageDiscountCartAction] pd ON pd.[CartActionId] = ca.[CartActionId]
			WHERE
				ca.[CampaignId] = @CampaignId
		)
		
		IF @MonetaryValue IS NOT NULL
		BEGIN
			UPDATE @DiscountCode
			SET
				[OrderValueBeforeVoucher] = NULL,
				[VoucherValue] = @MonetaryValue, -- %
				[VoucherValueOriginal] = @MonetaryValue -- %
			WHERE
				[OrderId] = @OrderId

			GOTO WHILE_END
		END
		
		-- Get action 'Cart item percentage discount'
		SET @MonetaryValue = (
			SELECT
				pd.[DiscountAmount]
			FROM
				[campaign].[tCartAction] ca
				INNER JOIN [lekmer].[tCartItemPercentageDiscountAction] pd ON pd.[CartActionId] = ca.[CartActionId]
			WHERE
				ca.[CampaignId] = @CampaignId
		)
		
		IF @MonetaryValue IS NOT NULL
		BEGIN
			UPDATE @DiscountCode
			SET
				[OrderValueBeforeVoucher] = NULL,
				[VoucherValue] = @MonetaryValue, -- %
				[VoucherValueOriginal] = @MonetaryValue -- %
			WHERE
				[OrderId] = @OrderId

			GOTO WHILE_END
		END


		WHILE_END:
		
		FETCH NEXT FROM cur_order INTO @OrderBatchId, @OrderId, @VoucherTypeId
	END
		
	CLOSE cur_order
	DEALLOCATE cur_order
	
	SELECT
		[VoucherCode],
		MIN(BatchId) AS 'BatchId',
		MIN(BatchDescription) AS 'Description',
		COUNT([OrderId]) AS 'OrdersCount',
		SUM(OrderValueBeforeVoucher) AS 'TotalOrderValueBeforeUsingCodes',
		SUM(OrderValueAfterVoucher) AS 'TotalOrderValueAfterUsingCodes',
		CASE MIN(dc.[VoucherTypeId]) WHEN 1 THEN MIN(VoucherValue) ELSE SUM(VoucherValue) END AS 'ValueOfCodes',
		CASE MIN(dc.[VoucherTypeId]) WHEN 1 THEN '%' ELSE '' END,
		CONVERT(DECIMAL(16, 2), SUM(OrderValueAfterVoucher) / COUNT([OrderId])) AS 'AverageOrderValue',
		MIN(cur.[ISO])
	FROM
		@DiscountCode dc
		INNER JOIN [core].[tChannel] c ON c.[ChannelId] = dc.[ChannelId]
		INNER JOIN [core].[tCurrency] cur ON cur.[CurrencyId] = c.[CurrencyId]
	GROUP BY
		dc.[ChannelId], [VoucherCode]
	ORDER BY
		MIN(BatchId), [VoucherCode], dc.[ChannelId]

END
GO
