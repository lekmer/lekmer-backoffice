
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageProductSave]
	@PackageId	INT,
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	EXEC [productlek].[pPackageProductDelete] @PackageId

	INSERT [productlek].[tPackageProduct] (
		[PackageId],
		[ProductId]
	)
	SELECT
		@PackageId,
		[p].[ID]
	FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
	
	
	DECLARE @AgeFromMonth INT, @AgeToMonth INT
	SET @AgeFromMonth = 0 
	SET @AgeToMonth = 0 
	
	SELECT 
		@AgeFromMonth = MIN([lp].[AgeFromMonth]),
		@AgeToMonth = MAX([lp].[AgeToMonth])
	FROM [lekmer].[tLekmerProduct] lp
	INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p ON p.[ID] = [lp].[ProductId]
	
	UPDATE [lekmer].[tLekmerProduct]
	SET
		[AgeFromMonth] = (CASE WHEN @AgeFromMonth IS NULL THEN 0 ELSE @AgeFromMonth END),
		[AgeToMonth] = CASE WHEN @AgeToMonth IS NULL THEN 0 ELSE @AgeToMonth END
	WHERE [ProductId] = (SELECT [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	
END
GO
