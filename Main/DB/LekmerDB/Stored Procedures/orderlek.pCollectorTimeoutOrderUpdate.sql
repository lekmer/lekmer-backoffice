SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTimeoutOrderUpdate]
	@TransactionId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tCollectorTransaction]
	SET	
		[TimeoutProcessed] = 1
	WHERE
		[TransactionId] = @TransactionId
END
GO
