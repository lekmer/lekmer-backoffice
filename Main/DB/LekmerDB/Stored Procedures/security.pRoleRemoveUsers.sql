
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pRoleRemoveUsers]
	@RoleId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[security].[tUserRole]
	WHERE
		[RoleId] = @RoleId
END
GO
