SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceActionIncludeProductInsert]
	@CartActionId			int,
	@ProductId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionIncludeProduct
	(
		CartActionId,
		ProductId
	)
	VALUES
	(
		@CartActionId,
		@ProductId
	)
END 



GO
