SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductVoteDelete]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [review].[tRatingItemProductVote]
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
