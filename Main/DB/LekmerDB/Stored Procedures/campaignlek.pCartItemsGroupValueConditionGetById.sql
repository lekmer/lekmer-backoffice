SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemsGroupValueCondition]
	WHERE
		[CartItemsGroupValueCondition.ConditionId] = @ConditionId
END
GO
