
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@ChannelNameISO	NVARCHAR(10),
	@TagIds			VARCHAR(MAX),
	@UserName		NVARCHAR(100)
AS
BEGIN
	/*NOT IN USE*/
	RAISERROR(N'NOT IN USE, not supported', 16, 1);
	RETURN

	--SET NOCOUNT ON

	---- Get language id by Channel
	--DECLARE @LanguageId INT
	--DECLARE @ChannelId INT
	--DECLARE @TitleFixed NVARCHAR(256)

	--SELECT 
	--	@LanguageId = l.LanguageId,
	--	@ChannelId = h.ChannelId
	--FROM [core].[tLanguage] l
	--	 INNER JOIN [core].[tChannel] h ON l.LanguageId = h.LanguageId
	--	 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
	--WHERE
	--	c.ISO = @ChannelNameISO

	---- Get product id by HYErpId
	--DECLARE @ProductId INT
	--SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	---- insert into tProductTransaltion empty rows
	--INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	--SELECT
	--	p.ProductId,
	--	@LanguageId
	--FROM
	--	[product].[tProduct] p
	--WHERE
	--	NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
	--				WHERE n.ProductId = p.ProductId 
	--				AND n.LanguageId = @LanguageId)

	--IF @Title = ''
	--	SET @Title = NULL

	--SET @TitleFixed = [generic].[fStripIllegalSymbols](@Title)

	--BEGIN TRY
	--	BEGIN TRANSACTION				

	--	IF @LanguageId != 1
	--		BEGIN
	--			UPDATE [product].[tProductTranslation]
	--			SET Title = @TitleFixed
	--			WHERE
	--				ProductId = @ProductId
	--				AND LanguageId = @LanguageId
	--				AND (Title != @TitleFixed OR Title IS NULL)


	--			/*BEGIN*/
	--			-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
	--			-- Get product id by HYErpId
	--			DECLARE @ProductRegistryId INT
	--			SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)

	--			-- Insert product to tProductRegistryproduct
	--			IF @TitleFixed IS NOT NULL
	--			BEGIN
	--				DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
	--				WHERE
	--					ProductRegistryId = @ProductRegistryId
	--					AND ProductId = @ProductId
	--					AND RestrictionReason = 'No Translation'
	--					AND UserId IS NULL

	--				DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
	--				INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
	--				VALUES (@ProductId, @ProductRegistryId)

	--				EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
	--			END
	--			/*END*/


	--			UPDATE [product].[tProductTranslation]
	--			SET [Description] = @Description
	--			WHERE
	--				ProductId = @ProductId
	--				AND LanguageId = @LanguageId
	--				AND (@Description IS NOT NULL AND @Description != '')
	--				AND ([Description] != @Description OR [Description] IS NULL)
	--		END
	--	ELSE
	--		BEGIN
	--			IF @TitleFixed IS NOT NULL
	--			BEGIN
	--				UPDATE [product].[tProduct]
	--				SET Title = @TitleFixed
	--				WHERE
	--					ProductId = @ProductId
	--			END

	--			UPDATE [product].[tProduct]
	--			SET [Description] = @Description
	--			WHERE
	--				ProductId = @ProductId
	--				AND (@Description IS NOT NULL AND @Description != '')
	--				AND ([Description] != @Description OR [Description] IS NULL)
	--		END

	--	-- Update/Insert product tags.
	--	DECLARE @TagId INT
	--	DECLARE cur_tag CURSOR FAST_FORWARD FOR
	--		SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')

	--	OPEN cur_tag
	--	FETCH NEXT FROM cur_tag INTO @TagId

	--	WHILE @@FETCH_STATUS = 0
	--		BEGIN
	--			-- Update/Insert product tags.
	--			IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
	--					  	   WHERE [pt].[ProductId] = @ProductId
	--								 AND [pt].[TagId] = @TagId)
	--				BEGIN
	--					INSERT INTO lekmer.tProductTag (ProductId, TagId)
	--					VALUES (@ProductId, @TagId)
	--				END

	--			EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, -1						

	--			FETCH NEXT FROM cur_tag INTO @TagId
	--		END
	--	CLOSE cur_tag
	--	DEALLOCATE cur_tag

	--	-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
	--	INSERT INTO integration.tBackofficeProductInfoImport
	--	(
	--		[HYErpId]
	--		,[Title]
	--		,[Description]
	--		,[ChannelNameISO]
	--		,[TagIdCollection]
	--		,[UserName]
	--		,[InsertedDate]
	--	)
	--	VALUES 
	--	(
	--		@HYErpId
	--		,@Title
	--		,@Description
	--		,@ChannelNameISO
	--		,@TagIds
	--		,@UserName
	--		,CAST(GETDATE() AS SMALLDATETIME)
	--	)

	--	COMMIT TRANSACTION
	--END TRY
	--BEGIN CATCH
	--	-- If transaction is active, roll it back.
	--	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

	--	INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
	--				values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	--END CATCH
END
GO
