SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetById]
	@LanguageId	int,
	@BlockId int
as
begin
	select 
		B.*		
	from 
		[sitestructure].[vCustomBlock] as B
	where
		[Block.BlockId] = @BlockId
		AND B.[Block.LanguageId] = @LanguageId
end

GO
