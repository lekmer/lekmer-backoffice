SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductGetInclPackageProductsByIdWithoutStatusFilter]
	@ProductId	INT,
	@ChannelId	INT,
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		*
	FROM
		[lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts] AS p
		INNER JOIN [product].[vCustomPriceListItem] AS pli ON pli.[Price.ProductId] = p.[Product.Id]
	WHERE
		[Product.Id] = @ProductId
		AND [Product.ChannelId] = @ChannelId
		AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice]
			(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
END
GO
