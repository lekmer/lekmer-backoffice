
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionSave]
	@ConditionId INT,
	@IncludeAllBatchIds BIT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tVoucherCondition]
	SET	
		[IncludeAllBatchIds] = @IncludeAllBatchIds
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tVoucherCondition]
		( 
			[ConditionId],
			[IncludeAllBatchIds]
		)
		VALUES 
		(
			@ConditionId,
			@IncludeAllBatchIds
		)
	END
END
GO
