SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRatingGroupGetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT 
		brg.*
	FROM 
		[review].[vBlockRatingGroup] AS brg
	WHERE
		brg.[BlockRatingGroup.BlockId] = @BlockId
END
GO
