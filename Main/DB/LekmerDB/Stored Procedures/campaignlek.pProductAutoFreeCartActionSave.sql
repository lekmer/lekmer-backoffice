SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pProductAutoFreeCartActionSave]
	@CartActionId	INT,
	@ProductId		INT,
	@Quantity		INT
AS
BEGIN
	UPDATE
		[campaignlek].[tProductAutoFreeCartAction]
	SET
		ProductId = @ProductId,
		Quantity = @Quantity
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tProductAutoFreeCartAction] (
			CartActionId,
			ProductId,
			Quantity
		)
		VALUES (
			@CartActionId,
			@ProductId,
			@Quantity
		)
	END
END
GO
