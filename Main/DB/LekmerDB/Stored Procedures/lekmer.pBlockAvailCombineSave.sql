
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockAvailCombineSave]
	@BlockId INT,
	@UseCartPredictions BIT,
	@UseClickStreamPredictions BIT,
	@UseLogPurchase BIT,
	@UsePersonalPredictions BIT,
	@UseProductSearchPredictions BIT,
	@UseProductsPredictions BIT,
	@UseProductsPredictionsFromClicksCategory BIT,
	@UseProductsPredictionsFromClicksProduct BIT,
	@UseLogClickedOn BIT,
	@GetClickHistoryFromCookie BIT
AS
BEGIN
	UPDATE
		[lekmer].[tBlockAvailCombine]
	SET
		UseCartPredictions = @UseCartPredictions,
		UseClickStreamPredictions = @UseClickStreamPredictions,
		UseLogPurchase = @UseLogPurchase,
		UsePersonalPredictions = @UsePersonalPredictions,
		UseProductSearchPredictions = @UseProductSearchPredictions,
		UseProductsPredictions = @UseProductsPredictions,
		UseProductsPredictionsFromClicksCategory = @UseProductsPredictionsFromClicksCategory,
		UseProductsPredictionsFromClicksProduct = @UseProductsPredictionsFromClicksProduct,
		UseLogClickedOn=@UseLogClickedOn,
		GetClickHistoryFromCookie=@GetClickHistoryFromCookie
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockAvailCombine] (
		BlockId,
		UseCartPredictions,
		UseClickStreamPredictions,
		UseLogPurchase,
		UsePersonalPredictions,
		UseProductSearchPredictions,
		UseProductsPredictions,
		UseProductsPredictionsFromClicksCategory,
		UseProductsPredictionsFromClicksProduct,
		UseLogClickedOn,
		GetClickHistoryFromCookie
	)
	VALUES (
		@BlockId,
		@UseCartPredictions,
		@UseClickStreamPredictions,
		@UseLogPurchase,
		@UsePersonalPredictions,
		@UseProductSearchPredictions,
		@UseProductsPredictions,
		@UseProductsPredictionsFromClicksCategory,
		@UseProductsPredictionsFromClicksProduct,
		@UseLogClickedOn,
		@GetClickHistoryFromCookie
	)
END
GO
