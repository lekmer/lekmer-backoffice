SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionCurrencyInsert]
	@ProductActionId	INT,
	@CurrencyId			INT,
	@Value				DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailProductActionCurrency] (
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ProductActionId,
		@CurrencyId,
		@Value
	)
END
GO
