SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDropShipGetAllByOrder]
	@Orderid INT
AS
BEGIN
	SELECT
		[ds].*
	FROM
		[orderlek].[vDropShip] ds
	WHERE
		[ds].[DropShip.OrderId] = @OrderId
END
GO
