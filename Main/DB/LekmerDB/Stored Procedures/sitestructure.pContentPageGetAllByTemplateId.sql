SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetAllByTemplateId]
	@TemplateId	int
AS
BEGIN
	SELECT 
		*
	FROM 
		[sitestructure].[vCustomContentPageSecure]
	WHERE
		[ContentPage.TemplateId] = @TemplateId
END

GO
