SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		cmf.*
	FROM
		[lekmer].[vContentMessageFolder] cmf
END
GO
