
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListDelete]
	@BlockId INT
AS 
BEGIN
	DELETE [lekmer].[tBlockBrandListBrand]
	WHERE [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandList]
	WHERE [BlockId] = @BlockId
END 
GO
