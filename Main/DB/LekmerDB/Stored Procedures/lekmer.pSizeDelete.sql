SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeDelete]
	@SizeId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	-- Should not exist product with such size (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tProductSize]
		WHERE [SizeId] = @SizeId
	)
	RETURN -1
	
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTableRow]
		WHERE [SizeId] = @SizeId
	)
	RETURN -1

	DELETE FROM [lekmer].[tSize]
	WHERE [SizeId] = @SizeId

	RETURN 0
END
GO
