SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBlockImageRotatorDelete]
	@BlockId	int
as
begin

	delete
		[lekmer].[tImageRotatorGroup]
	where
		[BlockId] = @BlockId
		
	delete
		[lekmer].[tBlockImageRotator]
	where
		[BlockId] = @BlockId
end
GO
