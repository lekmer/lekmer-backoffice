SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pEsalesModelParameterAllByModel]
	@SiteStructureRegistryId INT,
	@ModelComponentId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[emp].[Name] AS 'EsalesModelParameter.Name',
		[emp].[Value] AS 'EsalesModelParameter.Value'
	FROM
		[esales].[tEsalesModel] em
		INNER JOIN [esales].[tEsalesModelComponent] emc ON emc.[ModelId] = em.[ModelId]
		INNER JOIN [esales].[tEsalesModelParameter] emp ON emp.[ModelId] = em.[ModelId]
	WHERE
		emc.[ModelComponentId] = @ModelComponentId
		AND
		emp.[SiteStructureRegistryId] = @SiteStructureRegistryId
END
GO
