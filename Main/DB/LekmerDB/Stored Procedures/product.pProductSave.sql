SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [product].[pProductSave]
	@ProductId			int,
	@ItemsInPackage		int,
	@ErpId				varchar(50),
	@EANCode			varchar(20),
	@Title				nvarchar(256),
	@WebShopTitle		nvarchar(256),
	@ProductStatusId	int,
	@Description		nvarchar(max),
	@ShortDescription	nvarchar(max),
	@CategoryId			int,
	@MediaId			int	
as
begin
	set nocount on

	update
		[product].[tProduct]
	set
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	where
		[ProductId] = @ProductId
end
GO
