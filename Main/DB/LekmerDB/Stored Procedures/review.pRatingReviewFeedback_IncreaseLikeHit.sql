SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_IncreaseLikeHit]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE [review].[tRatingReviewFeedback]
	SET [LikeHit] = [LikeHit] + 1
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
