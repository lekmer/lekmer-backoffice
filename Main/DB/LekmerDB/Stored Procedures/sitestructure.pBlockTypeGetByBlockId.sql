SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User:Roman D.	Date: 21.05.2009	Time: 15:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pBlockTypeGetByBlockId]
	@BlockId	int
as
begin
	select 
		bt.*
	from 
		[sitestructure].[vCustomBlockType] AS bt 
		INNER JOIN sitestructure.vCustomBlockSecure AS b ON b.[BlockType.BlockTypeId] = bt.[BlockType.BlockTypeId]
	where
		b.[Block.BlockId] = @BlockId
end

GO
