SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderAuditDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].[tOrderAudit]
	WHERE [OrderId] = @OrderId
END
GO
