SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pImageRotatorGroupSave]
	@ImageGroupId int,
	@BlockId int,
	@MainImageId int = null,
	@ThumbnailImageId int = null,
	@InternalLinkContentNodeId int = null,
	@ExternalLink varchar(1000) = null,
	@IsMovie bit = 0,
	@Title varchar(100) = null,
	@HtmalMarkup varchar(max) = null,
	@InternalLinkContentNodeId2 int = null,
	@ExternalLink2 varchar(1000) = null,
	@IsMovie2 bit = 0,
	@InternalLinkContentNodeId3 int = null,
	@ExternalLink3 varchar(1000) = null,
	@IsMovie3 bit = 0,
	@Ordinal int=0,	
	@StartDateTime		datetime,
	@EndDateTime		datetime
as	
begin
	 
	update
		lekmer.[tImageRotatorGroup]
	set
		BlockId=@BlockId,
		MainImageId=@MainImageId,
		ThumbnailImageId=@ThumbnailImageId,
		InternalLinkContentNodeId=@InternalLinkContentNodeId,
		ExternalLink=@ExternalLink,
		IsMovie=@IsMovie,
		Title=@Title,
		HtmalMarkup=@HtmalMarkup,
		InternalLinkContentNodeId2=@InternalLinkContentNodeId2,
		ExternalLink2=@ExternalLink2,
		IsMovie2=@IsMovie2,
		InternalLinkContentNodeId3=@InternalLinkContentNodeId3,
		ExternalLink3=@ExternalLink3,
		IsMovie3=@IsMovie3,
		StartDateTime = @StartDateTime,
		EndDateTime = @EndDateTime,
		Ordinal = @Ordinal		
	where
		[ImageGroupId] = @ImageGroupId
	
	if @@ROWCOUNT = 0
	begin
		insert
			lekmer.[tImageRotatorGroup]
		(
			BlockId,
			MainImageId,
			ThumbnailImageId,
			InternalLinkContentNodeId,
			ExternalLink,
			IsMovie,
			Title,
			HtmalMarkup,
			InternalLinkContentNodeId2,
			ExternalLink2,
			IsMovie2,
			InternalLinkContentNodeId3,
			ExternalLink3,
			IsMovie3,
			StartDateTime,
		    EndDateTime,
		    Ordinal
		)
		values
		(
			@BlockId,
			@MainImageId,
			@ThumbnailImageId,
			@InternalLinkContentNodeId,
			@ExternalLink,
			@IsMovie,
			@Title,
			@HtmalMarkup,
			@InternalLinkContentNodeId2,
			@ExternalLink2,
			@IsMovie2,
			@InternalLinkContentNodeId3,
			@ExternalLink3,
			@IsMovie3,
			@StartDateTime,
			@EndDateTime,
			@Ordinal
			)
	end
end

GO
