SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2ProductSave]
	@BlockId	INT,
	@ProductId	INT,
	@Position	INT
AS
BEGIN
	SET NOCOUNT ON

	INSERT [esales].[tBlockEsalesTopSellersV2Product] (
		[BlockId],
		[ProductId],
		[Position]
	)
	VALUES (
		@BlockId,
		@ProductId,
		@Position
	)
END
GO
