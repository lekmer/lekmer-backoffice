SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pAliasSearch]
	@Name	varchar(MAX)
AS
BEGIN
	SET @Name = [generic].[fPrepareSearchParameter](@Name)
	select
		a.*
	from
		[template].[vCustomAliasSecure] a
	where
		(a.[Alias.CommonName] like @Name ESCAPE '\' 
		OR a.[Alias.Description] like @Name ESCAPE '\'
		OR a.[Alias.Value] like @Name ESCAPE '\')
	order by
		a.[Alias.AliasFolderId] asc
end

GO
