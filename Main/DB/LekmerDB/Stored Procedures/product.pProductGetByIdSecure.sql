SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetByIdSecure]
	@ProductId	int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;	
	
	DECLARE @CurrencyId int, @PriceListRegistryId int, @PriceListId int
	SELECT @CurrencyId = CurrencyId, @PriceListRegistryId = PriceListRegistryId
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT *		
	FROM 
		[product].[vCustomProductSecure] as p
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = @PriceListId
	WHERE
		p.[Product.Id] = @ProductId
END
GO
