
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pProductStoreStockByProduct]
	@ProductId	int
as
BEGIN
	SELECT
		c.[StoreProductSize.ProductId] as ProductId,
		c.[StoreProduct.Quantity] as Quantity,
		ps. *
	FROM
		[product].[vCustomStoreProductSize] c
	LEFT JOIN 
		[lekmer].[vSize] ps
	ON ps.[Size.Id]= c.[StoreProductSize.SizeId]
	WHERE c.[StoreProductSize.ProductId] = @ProductId
		

END
GO
