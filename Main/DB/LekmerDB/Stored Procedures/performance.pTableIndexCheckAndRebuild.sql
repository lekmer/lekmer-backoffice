SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-----------------------------------------------------------------------------
-- performance.pTableIndexCheckAndRebuild
--
-- Summary.....: A simpler variant of the scheduled rebuild indexes that takes 
--               all sorts of considerations.  
--      [performance].[pTableIndexCheckAndRebuild]
--         'heppo',
--         'SAMPLED',
--         '<tables>
--			  <table tablename="[order].tCart" fragmentationlevel="10"/>
--			  <table tablename="[order].tCartItem" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrderAudit" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrderPayment" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrderCartCampaign" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrderAddress" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrder" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrderItem" fragmentationlevel="10"/>
--			  <table tablename="[order].tOrderItemProduct" fragmentationlevel="10"/>
--			  <table tablename="product.tProduct" fragmentationlevel="10"/>
--			  <table tablename="product.tPriceLisItem" fragmentationlevel="10"/>
--			  <table tablename="product.tProductImage" fragmentationlevel="10"/>
--			  <table tablename="product.tProductModuleChannel" fragmentationlevel="10"/>
--			  <table tablename="product.tProductRegistryProduct" fragmentationlevel="10"/>
--			  <table tablename="product.tProductTranslation" fragmentationlevel="10"/>
--			  <table tablename="product.tCategoryTranslation" fragmentationlevel="10"/>
--			  <table tablename="product.tProductSeoSettingTranslation" fragmentationlevel="10"/>
--			  <table tablename="product.tPriceListItem" fragmentationlevel="10"/>
--			  <table tablename="product.tProductSiteStructureRegistry" fragmentationlevel="10"/>
--			  <table tablename="product.tRelationListProduct" fragmentationlevel="10"/>
--			  <table tablename="product.tBlockProductListProduct" fragmentationlevel="10"/>
--			  <table tablename="product.tCategorySiteStructureRegistry" fragmentationlevel="10"/>
--			  <table tablename="product.tProductRelationList" fragmentationlevel="10"/>
--			  <table tablename="product.tProductSeoSetting" fragmentationlevel="10"/>
--			  <table tablename="product.tCategory" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tOrderItemSize" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tProductUrl" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tProductTag" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tLekmerProduct" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBrandTranslation" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockBrandListBrand" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockProductFilterBrand" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tCustomerForgotPassword" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tTag" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockProductFilterTag" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tProductSize" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tTagTranslation" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tNewsletterSubscriber" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockBrandList" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBrandSiteStructureRegistry" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockBrandProductList" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tLekmerProductTranslation" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockBrandProductListBrand" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tProductDiscountActionItem" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBlockProductFilter" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tProductPopularity" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tBrand" fragmentationlevel="10"/>
--			  <table tablename="lekmer.tMonitorProduct" fragmentationlevel="10"/>
--			  <table tablename="media.tMedia" fragmentationlevel="10"/>
--			  <table tablename="media.tMediaFolder" fragmentationlevel="10"/>
--			  <table tablename="messaging.tMessageCustomField" fragmentationlevel="10"/>
--			  <table tablename="messaging.tMessage" fragmentationlevel="10"/>
--			  <table tablename="messaging.tBatch" fragmentationlevel="10"/>
--			  <table tablename="messaging.tRecipient" fragmentationlevel="10"/>
--			  <table tablename="messaging.tRecipientCustomField" fragmentationlevel="10"/>
--			  <table tablename="customer.tCustomerUser" fragmentationlevel="10"/>
--			  <table tablename="customer.tAddress" fragmentationlevel="10"/>
--			  <table tablename="customer.tCustomerInformation" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tBlock" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tContentNode" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tContentPage" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tBlockTranslation" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tContentPageSeoSetting" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tBlockRichText" fragmentationlevel="10"/>
--			  <table tablename="sitestructure.tContentNodeShortcutLink" fragmentationlevel="10"/>
--			  <table tablename="Template.tAlias" fragmentationlevel="10"/>
--			  <table tablename="Template.tInclude" fragmentationlevel="10"/>
--			  <table tablename="Template.tTemplate" fragmentationlevel="10"/>
--			  <table tablename="Template.tTemplateFragment" fragmentationlevel="10"/>
--			  <table tablename="template.tEntityFunction" fragmentationlevel="10"/>
--			  <table tablename="template.tModelFragment" fragmentationlevel="10"/>
--			  <table tablename="template.tAliasTranslation" fragmentationlevel="10"/>
--			  <table tablename="template.tModelFragmentEntity" fragmentationlevel="10"/>
--			  <table tablename="template.tModelFragmentRegion" fragmentationlevel="10"/>
--			  <table tablename="template.tModelFragmentFunction" fragmentationlevel="10"/>
--			  <table tablename="addon.tReview" fragmentationlevel="10"/>
--			  <table tablename="[statistics].tDailySearch" fragmentationlevel="10"/>
--			  <table tablename="[statistics].tHourlyVisitor" fragmentationlevel="10"/>
--			  <table tablename="campaign.tPercentagePriceDiscountActionIncludeProduct" fragmentationlevel="10"/>
--			  <table tablename="campaign.tCampaign" fragmentationlevel="10"/>
--			  <table tablename="import.tProduct" fragmentationlevel="10"/>
--			  <table tablename="import.tProductPrice" fragmentationlevel="10"/>
--			  <table tablename="import.tProductStockPosition" fragmentationlevel="10"/>
--			  <table tablename="integration.tProductStockPosition" fragmentationlevel="10"/>
--			  <table tablename="integration.tProductNews" fragmentationlevel="10"/>
--			  <table tablename="integration.tProductNumberInStockDayBeforeNews" fragmentationlevel="10"/>
--			  <table tablename="integration.tOrderDataStatistics" fragmentationlevel="10"/>
--         </tables>',
--         1
-- Returns.....: -
-- Parameters..:
-- 
-- $Revision: 8 $
-- 
-- $History: pTableIndexCheckAndRebuild.sql $
-- 
-----------------------------------------------------------------------------
CREATE procedure [performance].[pTableIndexCheckAndRebuild]
(
   @DB nvarchar(50),
   @Mode nvarchar(50) = 'SAMPLED', --LIMITED, SAMPLED, or DETAILED,
   @Tables xml, 
   @Execute bit = 1,
   @OnlyOnlineRebuild bit = null 
)
as
begin
	set nocount on
-- exec [integration].[pCheckDefragmentation]
   declare @dbid smallint
           ,@objectid int
           ,@TableName nvarchar(255)
           ,@FragmentationLevel int 
           ,@ExcludeIndex nvarchar(max)
           ,@IndexName nvarchar(255)
           ,@HasVarcharMax bit
           ,@ActualFragmentationLevel decimal(10,2)
           ,@TmpStr nvarchar(max)
           ,@ObjName varchar(250)
           ,@ErrMsg			nvarchar(2048) 
           ,@SP				nvarchar(256)
           ,@Severity		int 
           ,@State			int 
           ,@ErrorNumber	int
           ,@LockTimeOut   int
   declare @tmp table (TableName nvarchar(255) not null, FragmentationLevel int not null, ExcludeIndex nvarchar(max) null, LockTimeOut int null)
   set @ObjName = object_name(@@procid)
   
   insert into @tmp (TableName, FragmentationLevel, ExcludeIndex, LockTimeOut)
   select T.c.value('(@tablename)[1]', 'nvarchar(255)') as 'TableName'
         ,T.c.value('(@fragmentationlevel)[1]', 'int') as 'FragmentationLevel'
         ,T.c.value('(@excludeindex)[1]', 'nvarchar(max)') as 'ExcludeIndex'
         ,case when len(rtrim(T.c.value('(@locktimeout)[1]', 'nvarchar(255)'))) = 0 then null 
               else T.c.value('(@locktimeout)[1]', 'int')
          end as 'LockTimeout'
     from @Tables.nodes('tables/table') as T(c)

   set @dbid = db_id(@db)
   if @dbid is null
   begin
       print N'invalid database'
       return 1
   end

   declare lcur cursor local fast_forward for 
      select TableName, FragmentationLevel, ExcludeIndex, LockTimeOut
      from @Tmp
   open lcur
   while 1=1
   begin 

      fetch next from lcur into @TableName, @FragmentationLevel, @ExcludeIndex, @LockTimeOut

      if @@fetch_status = -1 break     -- eof
      if @@fetch_status = -2 continue  -- record were deleted

      set @objectid = object_id(@TableName)

      if @objectid is null
      begin
          set @TmpStr = 'Invalid object ' + @TableName
          print @TmpStr
          continue
      end
      else
      begin

         set @TmpStr = 'Checking fragmentation for ' + @TableName
         print @TmpStr

         declare lcurIndexCheckFrag cursor local fast_forward for 
                  select IndexName, HasVarcharMax, cast(avg_fragmentation_in_percent as decimal(10,2))
                    from (select o.name as tablename, ix.name as indexname, 
                                 case when (select count(*) from sys.columns where object_id = @objectid and max_length = -1) > 0 then 1 else 0 end as hasvarcharmax, 
              	                  i.*
                            from sys.dm_db_index_physical_stats(@dbid, @objectid, null, null , @mode) i
                                   inner join sys.objects o on i.object_id = o.object_id
                                   inner join sys.indexes ix on o.object_id = ix.object_id and i.index_id = ix.index_id
                           where avg_fragmentation_in_percent > @FragmentationLevel
                             and (@ExcludeIndex is null or ix.name not in (select sepstring from [generic].[fStringToStringTablePerformance](@ExcludeIndex,';')))
                         ) t
         open lcurIndexCheckFrag
         while 1=1
         begin 
            declare @sql nvarchar(max)
            fetch next from lcurIndexCheckFrag into @IndexName, @HasVarcharMax, @ActualFragmentationLevel

            if @@fetch_status = -1 break     -- eof
            if @@fetch_status = -2 continue  -- record were deleted
            
            
            
            --if @HasVarcharMax = 0 /* = "Enterprise"
            --   set @sql = 'ALTER INDEX ' + @IndexName + ' ON ' + @TableName + ' REBUILD WITH (ONLINE = ON)'
            --else 
            set @sql = 'ALTER INDEX ' + @IndexName + ' ON ' + @TableName + ' REBUILD'   

            if @Execute = 1
            begin
               if @HasVarcharMax = 1 
                  and @OnlyOnlineRebuild = 1 
               begin
                  set @TmpStr = @TableName + ' had fragmentation ' + cast(@ActualFragmentationLevel as varchar(30)) + '. Ignoring rebuild since it has varcharmax, sql : ' + @sql
                  print @TmpStr
               end   
               else
               begin
                  set @TmpStr = @TableName + ' had fragmentation ' + cast(@ActualFragmentationLevel as varchar(30)) + '. Running : ' + @sql
                  print @TmpStr
                  begin try
                     if @LockTimeOut is null
                     begin
                        exec(@sql)
                     end
                     else
                     begin 
                        -- Add lock timeout to the rebuild command before we run it
                        set @sql = 'set lock_timeout ' + cast(@LockTimeOut as varchar(30)) + char(13) + @sql + char(13)
                        exec(@sql)
                     end
                  end try
                  begin catch
                     select 
                        @ErrMsg = error_message(), 
                        @SP = error_procedure(), 
                        @Severity = error_severity(), 
                        @State = error_state(), 
                        @ErrorNumber = error_number()
                        begin try
                           if error_number() = 1222  
                              set @ErrMsg = 'Lock Timeout occurred on table ' + @TableName + ' index ' + @IndexName + ' when trying to rebuild index. Index rebuild is ignored. (Error: ' + error_message() + ')'
                           print	@ErrMsg 	
                        end try
                        begin catch
                        end catch
                  end catch
               end
            end
            else 
            begin
               set @TmpStr = @TableName + ' had fragmentation ' + cast(@ActualFragmentationLevel as varchar(30)) + '. Rebuild sql : ' + @sql
               print @TmpStr
            end

         end
         if (cursor_status ('local', 'lcurIndexCheckFrag') in (0, 1))
         begin
            close lcurIndexCheckFrag
            deallocate lcurIndexCheckFrag
         end
      end
            
   end
   if (cursor_status ('local', 'lcur') in (0, 1))
   begin
      close lcur
      deallocate lcur
   end

   set @TmpStr = 'Done'
   print @TmpStr

end

GO
