
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCustomerForgotPasswordSave]
	@CustomerId		INT,
	@Guid			UNIQUEIDENTIFIER,
	@CreatedDate	DATETIME,
	@ValidToDate	DATETIME = NULL
AS	
BEGIN
	SET NOCOUNT ON
	INSERT [lekmer].[tCustomerForgotPassword]
	VALUES (@CustomerId, @Guid, @CreatedDate, @ValidToDate)
END
GO
