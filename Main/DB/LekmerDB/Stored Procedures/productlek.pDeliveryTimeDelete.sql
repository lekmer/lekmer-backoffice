SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pDeliveryTimeDelete]
	@Id INT
AS 
BEGIN
	DELETE FROM [productlek].[tDeliveryTime]
	WHERE [DeliveryTimeId] = @Id
END
GO
