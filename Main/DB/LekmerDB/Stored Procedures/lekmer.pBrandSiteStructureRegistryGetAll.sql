SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureRegistryGetAll]
as
begin
	select
		*
	FROM
		lekmer.vBrandSiteStructureRegistry
end

GO
