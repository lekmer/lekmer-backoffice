SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pVariationTypeSave]
	@VariationTypeId	int,
	@VariationGroupId	int,
	@Title				nvarchar(250),
	@Ordinal			int
as
begin

	insert into [product].[tVariationType]
	(
		[VariationGroupId],
		[Title],
		[Ordinal]
	) 
	values
	(
		@VariationGroupId,
		@Title,
		@Ordinal
	)
	return scope_Identity()
end






GO
