SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetAllByFolder]
	@ContentMessageFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[cm].*
	FROM
		[lekmer].[vContentMessageSecure] cm
	WHERE
		[cm].[ContentMessage.ContentMessageFolderId] = @ContentMessageFolderId
END
GO
