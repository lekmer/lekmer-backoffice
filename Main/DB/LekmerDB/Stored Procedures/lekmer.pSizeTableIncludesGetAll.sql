
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludesGetAll]
	@SizeTableId	INT
AS
BEGIN
	SELECT [ProductId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [SizeTableId] = @SizeTableId
	SELECT [Id], [CategoryId], [BrandId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] WHERE [SizeTableId] = @SizeTableId
END
GO
