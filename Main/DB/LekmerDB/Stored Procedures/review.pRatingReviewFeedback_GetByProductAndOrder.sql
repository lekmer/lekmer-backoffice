SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_GetByProductAndOrder]
	@ChannelId	INT,
	@ProductId	INT,
	@OrderId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.ProductId] = @ProductId
		AND rrf.[RatingReviewFeedback.OrderId] = @OrderId
END
GO
