SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartActionTargetProductTypeDelete]
	@CartActionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE [campaignlek].[tCartActionTargetProductType]
	WHERE [CartActionId] = @CartActionId
END
GO
