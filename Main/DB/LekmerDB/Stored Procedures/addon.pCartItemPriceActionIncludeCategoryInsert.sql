SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartItemPriceActionIncludeCategoryInsert]
	@CartActionId			int,
	@CategoryId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionIncludeCategory
	(
		CartActionId,
		CategoryId
	)
	VALUES
	(
		@CartActionId,
		@CategoryId
	)
END 



GO
