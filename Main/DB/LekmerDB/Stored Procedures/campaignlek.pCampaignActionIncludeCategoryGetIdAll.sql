
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[aic].[CategoryId],
		[aic].[IncludeSubCategories]
	FROM 
		[campaignlek].[tCampaignActionIncludeCategory] aic
	WHERE 
		[aic].[ConfigId] = @ConfigId
END
GO
