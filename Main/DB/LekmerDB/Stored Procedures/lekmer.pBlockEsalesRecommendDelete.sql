
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tBlockEsalesRecommend]
	WHERE
		[BlockId] = @BlockId
END
GO
