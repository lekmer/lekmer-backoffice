
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pGetCustomersSegmentList]
	@DateFrom		DATETIME,
	@DateTo			DATETIME,
	@ChannelId		INT,
	@BrandIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX)
AS
BEGIN
	DECLARE @tTempOrders TABLE (OrderId INT)
	
	IF ((@BrandIds IS NULL OR @BrandIds = '-1') AND (@CategoryIds IS NULL OR @CategoryIds = '-1'))
	BEGIN
		INSERT @tTempOrders
		SELECT
			MAX([o].[OrderId])
		FROM
			[order].[tOrder] o
		WHERE
			[o].[OrderStatusId] = 4
			AND [o].[ChannelId] = @ChannelId
			AND [o].[CreatedDate] > @DateFrom
			AND [o].[CreatedDate] < @DateTo
			AND NOT EXISTS (
				SELECT 1
				FROM [lekmer].[tNewsletterUnsubscriber] nu
				WHERE [nu].[Email] = [o].[Email]
			)
		GROUP BY
			[o].[Email]
	END
	ELSE
	BEGIN
		DECLARE @tCategories TABLE (Id INT)
		INSERT @tCategories SELECT [ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, ',')
		
		DECLARE @tBrands TABLE (Id INT)
		INSERT @tBrands SELECT [ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, ',')

		INSERT @tTempOrders
		SELECT
			MAX([o].[OrderId])
		FROM
			[order].[tOrder] o
		WHERE
			[o].[OrderStatusId] = 4
			AND [o].[ChannelId] = @ChannelId
			AND [o].[CreatedDate] > @DateFrom
			AND [o].[CreatedDate] < @DateTo
			AND EXISTS (
				SELECT 1
				FROM 
					[order].[tOrderItem] oi
					INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[OrderItemId] = [oi].[OrderItemId]
					INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [oip].[ProductId]
					INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
					INNER JOIN [product].[tCategory] c3 ON [p].[CategoryId] = [c3].[CategoryId]
					INNER JOIN [product].[tCategory] c2 ON [c2].[CategoryId] = [c3].[ParentCategoryId]
					INNER JOIN [product].[tCategory] c1 ON [c1].[CategoryId] = [c2].[ParentCategoryId]
				WHERE
					(
						[c1].[CategoryId] IN (SELECT [Id] FROM @tCategories)
						OR [c2].[CategoryId] IN (SELECT [Id] FROM @tCategories)
						OR [c3].[CategoryId] IN (SELECT [Id] FROM @tCategories)
						OR [lp].[BrandId] IN (SELECT [Id] FROM @tBrands)
					)
					AND [oi].[OrderId] = [o].[OrderId]
			)
			AND NOT EXISTS (
				SELECT 1
				FROM [lekmer].[tNewsletterUnsubscriber] nu
				WHERE [nu].[Email] = [o].[Email]
			)
		GROUP BY
			[o].[Email]
	END

	SELECT 
		[o].[Email]							'Email',
		[ci].[FirstName]					'First Name', 
		[ci].[LastName]						'Last Name', 
		COALESCE([gt].[Title], 'Unknown')	'Gender',
		[oa].[StreetAddress]				'Address',
		[oa].[PostalCode]					'Postal',
		[oa].[City]							'City',
		[c].[Title]							'Country',
		[ci].[CellPhoneNumber]				'Mobile'
	FROM @tTempOrders tto
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [tto].[OrderId]
		INNER JOIN [customer].[tCustomerInformation] ci ON [ci].[CustomerId] = [o].[CustomerId]
		INNER JOIN [order].[tOrderAddress] oa ON [oa].[OrderAddressId] = [o].[BillingAddressId]
		INNER JOIN [core].[tCountry] c ON [c].[CountryId] = [oa].[CountryId]
		LEFT JOIN [customerlek].[tCustomerInformation] lci ON [lci].[CustomerId] = [o].[CustomerId]
		LEFT JOIN [customerlek].[tGenderType] gt ON [gt].[GenderTypeId] = [lci].[GenderTypeId]
	ORDER BY [o].[Email]
END
GO
