
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pPaymentTypeGetAll]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	WHERE
		[PaymentType.ChannelId] = @ChannelId
	ORDER BY
		[PaymentType.Ordinal],
		[PaymentType.PaymentTypeId]
END
GO
