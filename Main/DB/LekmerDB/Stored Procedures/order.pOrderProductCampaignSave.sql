
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderProductCampaignSave]
	@Id				INT,
	@CampaignId		INT,
	@OrderItemId	INT,
	@Title			NVARCHAR(500)
AS
BEGIN
	UPDATE
		[order].tOrderProductCampaign
	SET
		CampaignId = @CampaignId,
		OrderItemId = @OrderItemId,
		Title = @Title
	WHERE
		OrderProductCampaignId = @Id
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [order].tOrderProductCampaign (
			CampaignId,
			OrderItemId,
			Title
		)
		VALUES (
			@CampaignId,
			@OrderItemId,
			@Title
		)
		
		SET @Id = SCOPE_IDENTITY();
	END
	
	RETURN @Id
END
GO
