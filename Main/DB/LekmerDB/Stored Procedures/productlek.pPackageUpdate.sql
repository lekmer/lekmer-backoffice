
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageUpdate]
AS
BEGIN
	/*
	1. Update package stock numbers
		package.stock = MIN (products.stock)
		product.stock = SUM (product-sizes.stock) || product.stock
		
	2.	OBSOLETE
		Put packages offline when they are out of stock
		When
			package is online
			package is passive
			stock is 0
		Then
			set package offline
	*/

	SET NOCOUNT ON

	DECLARE @PackageId INT
	DECLARE @MasterProductId INT
	DECLARE @NumberInStock INT

	DECLARE cur_package CURSOR FAST_FORWARD
	FOR
		SELECT
			[PackageId],
			[MasterProductId]
		FROM
			[productlek].[tPackage] p

	OPEN cur_package

	FETCH NEXT FROM cur_package
	INTO @PackageId, @MasterProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @NumberInStock = (
			SELECT
				MIN(COALESCE(a1.[NumberInStock], p.NumberInStock))
			FROM
				[productlek].[tPackageProduct] pp
				INNER JOIN [product].[tProduct] p ON p.[ProductId] = pp.[ProductId]
				CROSS APPLY (
					SELECT
						SUM(ps.[NumberInStock]) AS 'NumberInStock'
					FROM
						[lekmer].[tProductSize] ps
					WHERE
						[ps].[ProductId] = [p].[ProductId]
				) a1
			WHERE
				[pp].[PackageId] = @PackageId
		)

		IF @NumberInStock IS NOT NULL
		BEGIN
			UPDATE
				[product].[tProduct]
			SET
				[NumberInStock] = @NumberInStock
			WHERE
				[ProductId] = @MasterProductId
				AND
				[NumberInStock] != @NumberInStock
		END

		FETCH NEXT FROM cur_package
		INTO @PackageId, @MasterProductId
	END

	CLOSE cur_package
	DEALLOCATE cur_package
END
GO
