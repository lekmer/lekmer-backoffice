
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCartItemDiscountCartActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
