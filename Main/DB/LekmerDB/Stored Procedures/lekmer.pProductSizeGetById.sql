
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeGetById]
	@ProductId		INT,
	@SizeId			INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		lekmer.vProductSize
	WHERE
		[ProductSize.ProductId] = @ProductId AND
		[ProductSize.SizeId] = @SizeId
END
GO
