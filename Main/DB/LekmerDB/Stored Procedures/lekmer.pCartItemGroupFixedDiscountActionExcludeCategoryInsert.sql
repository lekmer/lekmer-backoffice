SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
