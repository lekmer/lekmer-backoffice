SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pSavedCartUpdate]
	@CartId			INT,
	@IsReminderSent	BIT,
	@IsNeedReminder	BIT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tSavedCart]
	SET
		[IsReminderSent] = @IsReminderSent,
		[IsNeedReminder] = @IsNeedReminder
	WHERE
		[CartId] = @CartId
END
GO
