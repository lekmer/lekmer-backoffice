
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMonitorProductGetNextPortion]
	@PreviousMaxId	INT,
	@Quantity		INT
AS 
BEGIN 
	SELECT TOP (@Quantity)
		*
	FROM 
		[lekmer].[tMonitorProduct] mp
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = mp.[ProductId]
	WHERE
		mp.[MonitorProductId] > @PreviousMaxId
		AND
		p.[ProductStatusId] = 0 -- Online only
	ORDER BY 
		mp.[MonitorProductId] ASC
END
GO
