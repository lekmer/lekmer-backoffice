
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
