SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dovhun Roman
-- Create date: 2009-03-25
-- Description:	created
-- =============================================
CREATE PROCEDURE [media].[pMediaFormatGroupGetById]
	@MediaFormatGroupId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		 *
  FROM [media].[vCustomMediaFormatGroup] v
	WHERE  v.[MediaFormatGroup.Id] = @MediaFormatGroupId
END

GO
