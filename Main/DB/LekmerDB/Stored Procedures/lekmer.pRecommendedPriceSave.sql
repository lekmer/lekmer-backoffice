SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pRecommendedPriceSave]
	@ProductId	INT,
	@ChannelId	INT,
	@Price		DECIMAL(16,2)
AS
BEGIN
	INSERT INTO [lekmer].[tRecommendedPrice] (
		ProductId,
		ChannelId,
		Price
	)
	VALUES(
		@ProductId,
		@ChannelId,
		@Price
	)
END

GO
