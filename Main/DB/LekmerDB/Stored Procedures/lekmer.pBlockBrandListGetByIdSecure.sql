SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListGetByIdSecure]
@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		bc.*,
		b.*
	FROM
		[lekmer].[vBlockBrandList] AS bc
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bc.[BlockBrandList.BlockId] = b.[Block.BlockId]
	WHERE
		bc.[BlockBrandList.BlockId] = @BlockId
END

GO
