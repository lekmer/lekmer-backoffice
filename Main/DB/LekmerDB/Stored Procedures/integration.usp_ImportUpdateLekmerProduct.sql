
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

CREATE PROCEDURE [integration].[usp_ImportUpdateLekmerProduct]
AS
BEGIN
	IF OBJECT_ID('tempdb..#tProductUpdate') IS NOT NULL
		DROP TABLE #tProductUpdate

	CREATE TABLE #tProductUpdate (
		[FullHYId]				VARCHAR(50) NOT NULL,
		[ChannelId]				VARCHAR(50) NOT NULL,
		[HYErpId]				VARCHAR(50) NOT NULL,
		[LekmerErpId]			VARCHAR(50) NULL,
		[NoInStock]				NVARCHAR(25) NULL,
		[Weight]				DECIMAL(18, 3) NULL,
		[IsAbove60L]			BIT NULL,
		[StockStatusId]			INT NULL,
		[SupplierId]			NVARCHAR(500) NULL,
		[PurchasePrice]			DECIMAL (16, 2) NULL,		
		[PurchaseCurrencyId]	INT NULL,
		[SupplierArticleNumber]	VARCHAR(50) NULL,
		[AveragePrice]			DECIMAL (16, 2) NULL
		
		CONSTRAINT PK_#tProductUpdate PRIMARY KEY([FullHYId])
	)

	INSERT INTO [#tProductUpdate] (
		[FullHYId],
		[ChannelId],
		[HYErpId],
		[LekmerErpId],
		[NoInStock],
		[Weight],
		[IsAbove60L],
		[StockStatusId],
		[SupplierId],
		[PurchasePrice],
		[PurchaseCurrencyId],
		[SupplierArticleNumber],
		[AveragePrice]
	)
	SELECT
		[tp].[HYarticleId],
		SUBSTRING([tp].[HYarticleId], 1, 3), -- Channel
		SUBSTRING([tp].[HYarticleId], 5, 12),
		[tp].[LekmerArtNo],
		[tp].[NoInStock],
		CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000),
		CASE WHEN [tp].[PossibleToDisplay] IS NULL OR [tp].[PossibleToDisplay] <> 'G' THEN 0 ELSE 1 END,
		[tp].[Ref1],
		[tp].[SupplierId],
		CONVERT(DECIMAl(16,2), CONVERT(DECIMAl(16,2), [tp].[PurchasePrice]) / 100.0),
		(SELECT TOP(1) [CurrencyId] FROM [core].[tCurrency] WHERE [ISO] = [tp].[PurchaseCurrency]),
		[tp].[SupplierArticleNumber],
		CONVERT(DECIMAl(16,2), CONVERT(DECIMAl(16,2), [tp].[AveragePrice]) / 100.0)
		
	FROM
		[integration].[tempProduct] tp


	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[ExpectedBackInStock] = NULL
	FROM 
		[#tProductUpdate] pu
		INNER JOIN [lekmer].[tLekmerProduct] lp				ON [lp].[HYErpId] = [pu].[HYErpId]
		INNER JOIN [product].[tProduct] p					ON [p].[ProductId] = [lp].[ProductId]
		INNER JOIN [lekmer].[tLekmerChannel] lc				ON [lc].[ErpId] = pu.[ChannelId]
		INNER JOIN [product].[tProductRegistryProduct] prp	ON [prp].[ProductId] = [lp].[ProductId]
		INNER JOIN [product].[tProductModuleChannel] pmc	ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = [lc].[ChannelId]
	WHERE 
		[pu].[NoInStock] > 0


	UPDATE 
		[lp]
	SET 
		[lp].[LekmerErpId]			= [pu].[LekmerErpId],
		[lp].[Weight]				= [pu].[Weight],
		[lp].[SupplierId]			= [pu].[SupplierId],
		[lp].[PurchasePrice]		= [pu].[PurchasePrice],
		[lp].[PurchaseCurrencyId]	= [pu].[PurchaseCurrencyId],
		[lp].[IsAbove60L]			= [pu].[IsAbove60L],
		[lp].[SupplierArticleNumber]= [pu].[SupplierArticleNumber],
		[lp].[AveragePrice]         = [pu].[AveragePrice]
	FROM 
		[#tProductUpdate] pu
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = [pu].[HYErpId]
	WHERE
		[pu].[ChannelId] = '001'
		AND
			(
				[lp].[LekmerErpId] IS NULL OR [lp].[LekmerErpId] <> [pu].[LekmerErpId]
				OR [lp].[Weight] IS NULL OR [lp].[Weight] <> [pu].[Weight]
				OR [lp].[SupplierId] IS NULL OR [lp].[SupplierId] <> [pu].[SupplierId]
				OR [lp].[PurchasePrice] IS NULL OR [lp].[PurchasePrice] <> [pu].[PurchasePrice]
				OR [lp].[PurchaseCurrencyId] IS NULL OR [lp].[PurchaseCurrencyId] <> [pu].[PurchaseCurrencyId]
				OR [lp].[IsAbove60L] <> [pu].[IsAbove60L]
				OR [lp].[SupplierArticleNumber] IS NULL OR [lp].[SupplierArticleNumber] <> [pu].[SupplierArticleNumber]
				OR [lp].[AveragePrice] IS NULL OR [lp].[AveragePrice] <> [pu].[AveragePrice]
			)


	UPDATE 
		[lp]
	SET
		[lp].[StockStatusId] = [ss].[StockStatusId]
	FROM 
		[#tProductUpdate] pu
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = [pu].[HYErpId]
		INNER JOIN [productlek].[tStockStatus] ss ON [ss].[ErpId] = [pu].[StockStatusId]
		LEFT OUTER JOIN [lekmer].[tProductSize] ps ON [ps].[ProductId] = [lp].[ProductId]
	WHERE
		[pu].[ChannelId] = '001'
		AND [ps].[ProductId] IS NULL -- only products without sizes
		AND [lp].[StockStatusId] != [ss].[StockStatusId]

END
GO
