SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pAdFolderGetById]
	@AdFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[esales].[vAdFolder] rf
	WHERE
		rf.[AdFolder.AdFolderId] = @AdFolderId
END
GO
