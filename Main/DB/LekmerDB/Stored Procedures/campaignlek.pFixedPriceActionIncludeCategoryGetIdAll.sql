
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedPriceActionIncludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
