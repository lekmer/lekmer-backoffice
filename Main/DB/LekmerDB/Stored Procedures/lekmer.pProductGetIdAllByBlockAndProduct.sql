
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByBlockAndProduct]
	@ChannelId		INT,
	@CustomerId		INT,
	@BlockId		INT,
	@ProductId		INT,
	@ShowVariants	BIT,
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(50) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @RelationListVariantTypeId INT
	SET @RelationListVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant')
	DECLARE @RelationListColorVariantTypeId INT
	SET @RelationListColorVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'ColorVariant')
		
	DECLARE @maxCount INT
	SET @maxCount = 2000

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	DECLARE @tResult TABLE (ProductId INT, SortBy NVARCHAR(MAX))	

	INSERT @tResult	(ProductId, SortBy)
	SELECT DISTINCT TOP (@maxCount) p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND rlp.ProductId <> @ProductId
		' + CASE WHEN (@ShowVariants = 0) 
				THEN ' AND rl.RelationListTypeId <> @RelationListVariantTypeId AND rl.RelationListTypeId <> @RelationListColorVariantTypeId'
				ELSE ' '
			END + '
		AND p.[Product.ChannelId] = @ChannelId
		AND bprli.BlockId = @BlockId

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP (@Page * @PageSize) *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP @maxCount ProductId FROM @tResult AS SearchResult'
	END

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@CustomerId	INT,
			@BlockId	INT,
			@ProductId	INT,
			@ShowVariants	BIT,
			@Page		INT,
			@PageSize	INT,
			@RelationListVariantTypeId	INT,
			@RelationListColorVariantTypeId INT,
			@maxCount	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@ProductId,
			@ShowVariants,
			@Page,
			@PageSize,
			@RelationListVariantTypeId,
			@RelationListColorVariantTypeId,
			@maxCount
END
GO
