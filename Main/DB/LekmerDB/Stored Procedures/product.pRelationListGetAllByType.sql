SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pRelationListGetAllByType]
	@CommonName VARCHAR(50)
AS
BEGIN
	SELECT
		*			
	FROM
		vRelationList v
	WHERE 
		v.[RelationList.CommonName] = @CommonName
END
GO
