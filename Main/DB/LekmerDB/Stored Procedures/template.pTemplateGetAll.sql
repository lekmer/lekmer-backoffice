SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 2  *****************
User: Roman D		Date:26.11.2008		Time: 17:40
Description:
			Edited 
*/

/*
*****************  Version 1  *****************
User: Victor E.		Date: 18.10.2008		Time: 16:40
Description:
			Created
*****************  Version 2  *****************
User: Yuriy P.		Date: 22.01.2009
Description:
			added Image
*/
CREATE PROCEDURE [template].[pTemplateGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomTemplate]
END

GO
