
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					INT,
	@Title						NVARCHAR(50),
	@AllowMultipleSizesPurchase	BIT,
	@PackageInfo				NVARCHAR(MAX),
	@MonitorThreshold			INT,
	@MaxQuantityPerOrder		INT,
	@DeliveryTimeId				INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold,
		[MaxQuantityPerOrder] = @MaxQuantityPerOrder,
		[DeliveryTimeId] = @DeliveryTimeId
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase],
			[PackageInfo],
			[MonitorThreshold],
			[MaxQuantityPerOrder],
			[DeliveryTimeId]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase,
			@PackageInfo,
			@MonitorThreshold,
			@MaxQuantityPerOrder,
			@DeliveryTimeId
		)
	END
END
GO
