
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pWishListItemSave]
	@Id				INT,
	@WishListId		INT,
	@ProductId		INT,
	@SizeId			INT,
	@Ordinal		INT
AS
BEGIN
	UPDATE [lekmer].[tWishListItem] 
	SET
		[Ordinal] = @Ordinal,
		[SizeId] = @SizeId
	WHERE
		[Id] = @Id
		
	IF @@ROWCOUNT = 0
	BEGIN	
		INSERT INTO [lekmer].[tWishListItem] (
			WishListId,
			ProductId,
			SizeId,
			Ordinal
		)
		VALUES (
			@WishListId,
			@ProductId,
			@SizeId,
			@Ordinal
		)
		SET @Id = SCOPE_IDENTITY()
	END

	RETURN @Id
END
GO
