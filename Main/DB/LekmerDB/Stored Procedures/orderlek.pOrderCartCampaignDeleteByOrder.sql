SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderCartCampaignDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE [order].[tOrderCartCampaign]
	WHERE [OrderId] = @OrderId
END
GO
