SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewUserSave]
	@RatingReviewUserId INT,
	@CustomerId			INT,
	@Token				VARCHAR(50),
	@CreatedDate		DATETIME
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[review].[tRatingReviewUser]
	SET 
		[CustomerId] = @CustomerId
	WHERE
		[RatingReviewUserId] = @RatingReviewUserId
		AND [CustomerId] IS NULL
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingReviewUser] (
			[CustomerId],
			[Token],
			[CreatedDate]
		)
		VALUES (
			@CustomerId,
			@Token,
			@CreatedDate
		)

		SET @RatingReviewUserId = SCOPE_IDENTITY()
	END

	RETURN @RatingReviewUserId
END
GO
