SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [core].[pChannelSave]
	@ChannelId				int,
	@Title					nvarchar(100),
	@CountryId				int,
	@CurrencyId				int,
	@LanguageId				int,
	@ApplicationName		varchar(100),
	@AlternateLayoutRatio	decimal(9,4),
	@CultureId				int
	
AS
BEGIN
	SET NOCOUNT ON
	
	IF exists (
				SELECT
					1
				FROM
					[core].[tChannel]
				WHERE
					[Title] = @Title
					AND ChannelId <> @ChannelId
				)
		RETURN -1
		
	UPDATE
		[core].[tChannel]
	SET
		[Title]					= @Title,
		[CountryId]				= @CountryId,
		[CurrencyId]			= @CurrencyId,
		[LanguageId]			= @LanguageId,
		[ApplicationName]		= @ApplicationName,
		[AlternateLayoutRatio]	= @AlternateLayoutRatio,
		[CultureId]				= @CultureId
	WHERE
		[ChannelId] = @ChannelId

	RETURN @ChannelId
END

GO
