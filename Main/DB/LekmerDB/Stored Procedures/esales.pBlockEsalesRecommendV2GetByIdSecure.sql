SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pBlockEsalesRecommendV2GetByIdSecure]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[esales].[vBlockEsalesRecommendV2Secure] AS ber
	WHERE
		[ber].[Block.BlockId] = @BlockId
END
GO
