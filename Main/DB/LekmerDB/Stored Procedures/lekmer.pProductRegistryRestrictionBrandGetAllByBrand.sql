
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]
	@BrandId		INT
AS
BEGIN
	SELECT
		[rb].[ProductRegistryId] 'ProductRegistryId',
		[rb].[BrandId] 'ItemId',
		[rb].[RestrictionReason] 'RestrictionReason',
		[rb].[UserId] 'UserId',
		[rb].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM 
		[lekmer].[tProductRegistryRestrictionBrand] rb
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
	WHERE
		[rb].[BrandId] = @BrandId
END
GO
