SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pAliasFolderGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[AliasFolder.Id],
			v.[AliasFolder.ParentAliasFolderId],
			v.[AliasFolder.Title]
		FROM template.vCustomAliasFolder AS v
		WHERE v.[AliasFolder.ParentAliasFolderId] IS NULL
	
	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[AliasFolder.Id],
					v.[AliasFolder.ParentAliasFolderId],
					v.[AliasFolder.Title]
				FROM template.vCustomAliasFolder AS v
				WHERE v.[AliasFolder.ParentAliasFolderId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[AliasFolder.ParentAliasFolderId]
									 FROM template.vCustomAliasFolder AS v
									 WHERE v.[AliasFolder.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT
							v.[AliasFolder.Id],
							v.[AliasFolder.ParentAliasFolderId],
							v.[AliasFolder.Title]
						FROM template.vCustomAliasFolder AS v
						WHERE v.[AliasFolder.ParentAliasFolderId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM template.vCustomAliasFolder AS v
									WHERE v.[AliasFolder.ParentAliasFolderId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY ParentId ASC
END

GO
