SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [core].[pChannelGetByCommonName]
	@CommonName varchar(50)
as
begin
	set nocount on

	select
		*
	from
		[core].[vCustomChannel]
	where
		[Channel.CommonName] = @CommonName
end


GO
