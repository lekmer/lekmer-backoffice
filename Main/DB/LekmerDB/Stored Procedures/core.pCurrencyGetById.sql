SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Roman A.         Date: 09.09.2008        Time: 14:00

*/
CREATE PROCEDURE [core].[pCurrencyGetById]
	@CurrencyId INT
as
begin
	set nocount on
	
	select
		*
	from
		[core].[vCustomCurrency]
	where
		[Currency.Id] = @CurrencyId
end

GO
