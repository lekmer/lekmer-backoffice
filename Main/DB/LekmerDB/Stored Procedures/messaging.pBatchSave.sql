SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [messaging].[pBatchSave]
    @BatchId UNIQUEIDENTIFIER,
    @StartDate DATETIME,
    @EndDate DATETIME,
    @MachineName VARCHAR(100),
    @ProcessId INT
AS 
    UPDATE  [messaging].[tBatch]
    SET     StartDate = @StartDate,
            EndDate = @EndDate,
            MachineName = @MachineName,
            ProcessId = @ProcessId
    WHERE   BatchId = @BatchId
                
    IF @@ROWCOUNT = 0 
    INSERT  INTO [messaging].[tBatch]
            (
              BatchId,
              StartDate,
              EndDate,
              MachineName,
              ProcessId
			
            )
    VALUES  (
              @BatchId,
              @StartDate,
              @EndDate,
              @MachineName,
              @ProcessId
			
            )
GO
