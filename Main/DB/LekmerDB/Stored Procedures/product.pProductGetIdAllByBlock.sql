
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetIdAllByBlock]
	@ChannelId INT,
	@CustomerId INT,
	@BlockId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	
	SET @sqlFilter = '
		(
			SELECT ROW_NUMBER() OVER (ORDER BY a1.[ProductInStock] DESC, bp.[BlockProductListProduct.Ordinal]) AS Number,
			[p].[Product.Id]
			FROM [product].[vCustomProduct] p
				INNER JOIN [product].[vCustomBlockProductListProduct] AS bp ON bp.[BlockProductListProduct.ProductId] = p.[Product.Id]
				INNER JOIN product.vCustomPriceListItem AS pli
					ON pli.[Price.ProductId] = P.[Product.Id]
					AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)
				CROSS APPLY (
					SELECT
						CASE WHEN ISNULL(SUM(ps.[NumberInStock]), p.[Product.NumberInStock]) > 0 THEN 1 ELSE 0 END AS ''ProductInStock''
					FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[Product.Id]
				) a1
			WHERE
				bp.[BlockProductListProduct.BlockId] = @BlockId
				AND p.[Product.ChannelId] = @ChannelId
		)'

	SET @sql = '
		SELECT * FROM
		' + @sqlFilter + '
		AS SearchResult'
	SET @sqlCount = '
		SELECT COUNT(1) FROM
		' + @sqlFilter + '
		AS SearchResultsCount'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END	

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT,
			@CustomerId	INT,
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@Page,
			@PageSize
			
	EXEC sp_executesql @sql,
		N'	@ChannelId	INT,
			@CustomerId	INT,
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@Page,
			@PageSize
END
GO
