SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pReviewSearch]
         @ProductTitle       nvarchar(256),
         @ProductId          int,
         @Message             nvarchar(500),
         @Author                        nvarchar(50),
         @StatusId       int,
         @CreatedFrom    datetime,
         @CreatedTo      datetime,
         @Page           int = NULL,
         @PageSize       int,
         @SortBy         varchar(20) = NULL,
         @SortDescending bit = NULL
as   
begin
    set nocount on

         IF (generic.fnValidateColumnName(@SortBy) = 0) 
         BEGIN
                   RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
                   RETURN
         END

         DECLARE @sql nvarchar(max)
         DECLARE @sqlCount nvarchar(max)
         DECLARE @sqlFragment nvarchar(max)
         SET @sqlFragment = '
                   SELECT ROW_NUMBER() OVER (ORDER BY [' 
                            + COALESCE(@SortBy, 'Review.CreatedDate')
                            + ']'
                            + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
                            r.*
                   from
                            [addon].[vCustomReviewRecord] r
                   where         
                            (@ProductId IS NULL OR r.[Review.ProductId] = @ProductId)'
                            + CASE WHEN (@ProductTitle IS NOT NULL) THEN + 'AND r.[Product.Title] LIKE ''%' + Replace(@ProductTitle, '''', '''''') + '%''' ELSE '' END
                            + CASE WHEN (@StatusId IS NOT NULL) THEN + 'AND r.[Review.ReviewStatusId] = @StatusId ' ELSE '' END
                            + CASE WHEN (@Message IS NOT NULL) THEN + 'AND r.[Review.Message] LIKE ''%' + Replace(@Message, '''', '''''') + '%''' ELSE '' END
                            + CASE WHEN (@Author IS NOT NULL) THEN + 'AND r.[Review.Author] = @Author ' ELSE '' END
                            + CASE WHEN (@CreatedFrom IS NOT NULL) THEN + 'AND r.[Review.CreatedDate] >= @CreatedFrom ' ELSE '' END
                            + CASE WHEN (@CreatedTo IS NOT NULL) THEN + 'AND r.[Review.CreatedDate] <= @CreatedTo ' ELSE '' END

         SET @sql = 
                   'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

         IF @Page != 0 AND @Page IS NOT NULL 
         BEGIN
                   SET @sql = @sql + '
                   WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
         END
         
         SET @sqlCount = 'SELECT COUNT(1) FROM
                   (
                   ' + @sqlFragment + '
                   )
                   AS CountResults'

         EXEC sp_executesql @sqlCount,
                   N'      @ProductTitle       nvarchar(256),
                            @ProductId          int,
                            @Message             nvarchar(500),
                            @Author                        nvarchar(50),
                            @StatusId       int,
                            @CreatedFrom    datetime,
                            @CreatedTo      datetime',
                            @ProductTitle,
                            @ProductId,          
                            @Message, 
                            @Author,     
                            @StatusId,
                            @CreatedFrom,  
                            @CreatedTo
                                 
         EXEC sp_executesql @sql, 
                   N'      @ProductTitle       nvarchar(256),
                            @ProductId          int,
                            @Message             nvarchar(500),
                            @Author                        nvarchar(50),
                            @StatusId       int,
                            @CreatedFrom    datetime,
                            @CreatedTo      datetime',
                            @ProductTitle,
                            @ProductId,          
                            @Message, 
                            @Author,     
                            @StatusId,
                            @CreatedFrom,  
                            @CreatedTo
end

GO
