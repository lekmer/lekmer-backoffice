
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionSave]
	@CartActionId		INT,
	@SendingInterval	INT,
	@TemplateId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailCartAction]
	SET
		[SendingInterval] = @SendingInterval,
		[TemplateId] = @TemplateId
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tGiftCardViaMailCartAction] (
			[CartActionId],
			[SendingInterval],
			[TemplateId]
		)
		VALUES (
			@CartActionId,
			@SendingInterval,
			@TemplateId
		)
	END
END

GO
