SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		stf.*
	FROM
		[lekmer].[vSizeTableFolder] stf
END
GO
