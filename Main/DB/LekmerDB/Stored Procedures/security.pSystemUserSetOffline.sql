SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserSetOffline]
	@SystemUserId	int
as
begin
	set nocount on

	update
		security.tSystemUser
	set
		StatusId = (
			select
				SystemUserStatusId
			from
				security.tSystemUserStatus
			where
				CommonName = 'Offline'
		)
	where
		SystemUserId = @SystemUserId
end




GO
