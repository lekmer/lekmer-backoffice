SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSimilarSave]
	@HYProductErpId			VARCHAR(50),
	@ProductSimilarIdList	VARCHAR(MAX),
	@ScoreList				VARCHAR(MAX),
	@Separator				CHAR(1)
AS
BEGIN
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] 
					  WHERE HYErpId = @HYProductErpId)

	EXEC [lekmer].[pProductSimilarDelete] @ProductId

	INSERT INTO [lekmer].[tProductSimilar] (
		ProductId,
		SimilarProductId,
		Score
	)
	SELECT
		@ProductId,
		lp.ProductId,
		idList.Score
	FROM [lekmer].[fnConvertIDListToTableWithScore] (@ProductSimilarIdList, @ScoreList, @Separator) idList
	INNER JOIN [lekmer].[tLekmerProduct] lp ON idList.ErpId = lp.HYErpId
	
	RETURN @ProductId
END
GO
