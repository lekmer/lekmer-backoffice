SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageMasterProductIdsGetByPackageProduct]
	@ProductId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	     [Package.MasterProductId]
	FROM
	    [productlek].[vPackage]
	WHERE
		[ChannelId] = @ChannelId
		AND [Package.PackageId] IN (
			SELECT [PackageId] FROM [productlek].[tPackageProduct] WHERE [ProductId] = @ProductId
		)
END
GO
