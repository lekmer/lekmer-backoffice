SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignGetAllCartCampaigns]
AS
BEGIN
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C 
		INNER JOIN campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	ORDER BY 
		C.[Campaign.Priority] ASC
END

GO
