SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderDelete]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [lekmer].[tLekmerOrder]
	WHERE [OrderId] = @OrderId
	
	DELETE [order].[tOrder]
	WHERE [OrderId] = @OrderId
END
GO
