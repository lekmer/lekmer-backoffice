
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pBackofficeProductInfoImport]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@TagIds			VARCHAR(MAX),
	@ChannelId		INT,
	@LanguageId		INT,
	@UserName		NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON

	-- Get product id by HYErpId
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	-- insert into tProductTransaltion empty rows
	IF NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] pt WHERE [pt].[ProductId] = @ProductId AND [pt].[LanguageId] = @LanguageId)
	BEGIN
		INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
		VALUES (@ProductId, @LanguageId)
	END


	IF @Title = ''
		SET @Title = NULL
	DECLARE @TitleFixed NVARCHAR(256)
	SET @TitleFixed = [generic].[fStripIllegalSymbols](@Title)


	BEGIN TRY
		BEGIN TRANSACTION				

		IF @LanguageId != 1
		BEGIN
			UPDATE [product].[tProductTranslation]
			SET Title = @TitleFixed
			WHERE	ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (Title != @TitleFixed OR Title IS NULL)

			-- Insert product to tProductRegistryProduct
			IF @TitleFixed IS NOT NULL
			BEGIN
				DECLARE @BrandId INT
				DECLARE @CategoryId INT
				DECLARE @ProductRegistryId INT
				SET @BrandId = (SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
				SET @CategoryId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
				SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)

				-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
				DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
				WHERE	ProductRegistryId = @ProductRegistryId
						AND ProductId = @ProductId
						AND RestrictionReason = 'No Translation'
						AND UserId IS NULL

				DECLARE @CategoryRestriction TABLE(CategoryId INT, ProductRegistryId INT)
				INSERT INTO @CategoryRestriction (CategoryId, ProductRegistryId)
				EXEC [lekmer].[pProductRegistryRestrictionCategoryGetAll]

				-- Add product to tProductRegistryProduct table.
				IF (NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] WHERE ProductId = @ProductId AND ProductRegistryId = @ProductRegistryId)
					AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] WHERE ProductId = @ProductId AND ProductRegistryId = @ProductRegistryId)
					AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionBrand] WHERE BrandId = @BrandId AND ProductRegistryId = @ProductRegistryId)
					AND NOT EXISTS (SELECT 1 FROM @CategoryRestriction WHERE [CategoryId] = @CategoryId AND [ProductRegistryId] = @ProductRegistryId)
					)
				BEGIN
					INSERT INTO [product].[tProductRegistryProduct] (ProductId, ProductRegistryId)
					VALUES (@ProductId, @ProductRegistryId)
				END
			END

			UPDATE [product].[tProductTranslation]
			SET [Description] = @Description
			WHERE
				ProductId = @ProductId
				AND LanguageId = @LanguageId
				AND (@Description IS NOT NULL AND @Description != '')
				AND ([Description] != @Description OR [Description] IS NULL)
		END
		ELSE
		BEGIN
			IF @TitleFixed IS NOT NULL
			BEGIN
				UPDATE [product].[tProduct]
				SET Title = @TitleFixed
				WHERE ProductId = @ProductId
			END
				
			UPDATE [product].[tProduct]
			SET [Description] = @Description
			WHERE
				ProductId = @ProductId
				AND (@Description IS NOT NULL AND @Description != '')
				AND ([Description] != @Description OR [Description] IS NULL)
		END

		-- Update/Insert product tags.
		DECLARE @tTagIds TABLE(TagId INT NOT NULL);
		INSERT INTO @tTagIds
		SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
		
		INSERT INTO [lekmer].[tProductTag] (ProductId, TagId)
		SELECT @ProductId, [t].[TagId]
		FROM @tTagIds t
		WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] pt
				  		  WHERE [pt].[ProductId] = @ProductId
								AND [pt].[TagId] = [t].[TagId])
		
		DECLARE @TagId INT = 0
		WHILE (1 = 1) 
		BEGIN  
		  SELECT TOP 1 @TagId = [TagId] 
		  FROM @tTagIds
		  WHERE [TagId] > @TagId 
		  ORDER BY [TagId]
		  
		  IF @@ROWCOUNT = 0 BREAK;
		  
		  EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, -1
		END

		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		INSERT INTO [integration].[tBackofficeProductInfoImport] (
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelId]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate])
		VALUES (
			@HYErpId
			,@Title
			,@Description
			,@ChannelId
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS SMALLDATETIME))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] (
			[Data],
			[Message],
			[Date],
			[OcuredInProcedure])
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE())
	END CATCH		 
END
GO
