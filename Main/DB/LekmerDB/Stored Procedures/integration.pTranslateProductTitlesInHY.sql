SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pTranslateProductTitlesInHY]
	@ISO	nvarchar(10)
AS
begin
	set nocount on				
		
		declare @channel int
		set @channel = (
							select ChannelId from core.tChannel c
							inner join core.tLanguage l
								on c.LanguageId = l.LanguageId
							where l.ISO = @ISO)
		select 
			l.HYErpId, 
			t.ProductId, 
			t.LanguageId, 
			t.Title,
			@channel 
		from lekmer.tLekmerProduct l
				inner join product.tProductTranslation t
					on l.ProductId = t.ProductId 
		where 
			t.LanguageId = (select LanguageId from core.tLanguage where ISO = @ISO) 
			and t.Title Is not NULL
			and t.Title != ''
			and not exists 
							(
								select 1 
								from integration.[tProductsTranslatedInHY] i
								where i.ProductId = t.ProductId
								and i.languageId = t.LanguageId					
							)		 
end
GO
