SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pAgeIntervalGetAllSecure]
AS
begin
	set nocount on

	select
		*
	from
		lekmer.[vAgeIntervalSecure]
end

GO
