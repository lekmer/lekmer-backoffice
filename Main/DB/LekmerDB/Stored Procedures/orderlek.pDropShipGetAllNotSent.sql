SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDropShipGetAllNotSent]
AS 
BEGIN 
	SELECT
		[ds].*
	FROM
		[orderlek].[vDropShip] ds
	WHERE
		[ds].[DropShip.SendDate] IS NULL
END
GO
