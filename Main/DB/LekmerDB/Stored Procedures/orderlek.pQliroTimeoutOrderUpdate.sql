SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pQliroTimeoutOrderUpdate]
	@TransactionId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tQliroTransaction]
	SET	
		[TimeoutProcessed] = 1
	WHERE
		[TransactionId] = @TransactionId
END
GO
