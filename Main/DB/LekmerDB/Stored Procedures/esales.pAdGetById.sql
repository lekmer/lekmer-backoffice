SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pAdGetById]
	@AdId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[a].*
	FROM
		[esales].[vAd] a
	WHERE
		[a].[Ad.AdId] = @AdId
END
GO
