
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateSizeLekmer]
AS
BEGIN

	UPDATE
		[ps]
	SET
		[ps].[NumberInStock] = [tp].[NoInStock],
		[ps].[Weight] = CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
	FROM
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tProductSize] ps ON [ps].[ErpId] = SUBSTRING([tp].[HYarticleId], 5, 16)
	WHERE 
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
		(
			[ps].[NumberInStock] <> [tp].[NoInStock]
			OR [ps].[Weight] IS NULL
			OR [ps].[Weight] <> CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
		)
			
			
	UPDATE
		[ps]
	SET
		[ps].[StockStatusId] = [ss].[StockStatusId]
	FROM
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tProductSize] ps ON [ps].[ErpId] = SUBSTRING([tp].[HYarticleId], 5, 16)
		INNER JOIN [productlek].[tStockStatus] ss ON [ss].[ErpId] = [tp].[Ref1]
	WHERE 
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
		[ps].[StockStatusId] != [ss].[StockStatusId]
		
END
GO
