SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionGetProductIds]
	@ConfigId INT
AS
BEGIN
	DECLARE @IsIncludeAll BIT
	SET @IsIncludeAll = (SELECT [IncludeAllProducts] FROM [campaignlek].[tCampaignActionConfigurator] WHERE [CampaignActionConfiguratorId] = @ConfigId)

	DECLARE @IncludedProducts TABLE (ProductId INT)
	DECLARE @ExcludedProducts TABLE (ProductId INT)

	INSERT INTO @ExcludedProducts ([ProductId])
	(
		SELECT DISTINCT [p].[ProductId]
		FROM [lekmer].[tLekmerProduct] p
		WHERE [p].[BrandId] IN (SELECT [eb].[BrandId]
								FROM [campaignlek].[tCampaignActionExcludeBrand] eb
								WHERE [eb].[ConfigId] = @ConfigId)

		UNION

		SELECT DISTINCT [ep].[ProductId]
		FROM [campaignlek].[tCampaignActionExcludeProduct] ep
		WHERE [ep].[ConfigId] = @ConfigId

		UNION

		SELECT DISTINCT [p].[ProductId]
		FROM [product].[tProduct] p 
		WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tCampaignActionExcludeCategory] ec
								   CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) src
								   WHERE [ec].[ConfigId] = @ConfigId)
	)

	IF (@IsIncludeAll = 1)
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		SELECT [ProductId] FROM [product].[tProduct]
	END
	ELSE
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[BrandId] IN (SELECT [ib].[BrandId]
									FROM [campaignlek].[tCampaignActionIncludeBrand] ib
									WHERE [ib].[ConfigId] = @ConfigId)

			UNION

			SELECT DISTINCT [ip].[ProductId]
			FROM [campaignlek].[tCampaignActionIncludeProduct] ip
			WHERE [ip].[ConfigId] = @ConfigId

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tCampaignActionIncludeCategory] ic
									   CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) src
									   WHERE [ic].[ConfigId] = @ConfigId)
		)
	END

	SELECT [ProductId] FROM @IncludedProducts
	EXCEPT
	SELECT [ProductId] FROM @ExcludedProducts
END
GO
