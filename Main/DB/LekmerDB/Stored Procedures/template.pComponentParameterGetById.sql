SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Artymyshyn M.		Date: 17.02.2009		Time: 10:20
Description:
			create					
*/


CREATE PROCEDURE [template].[pComponentParameterGetById]
	@ComponentParameterId	INT
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomComponentParameter]
	WHERE
		[ComponentParameter.Id] = @ComponentParameterId
END

GO
