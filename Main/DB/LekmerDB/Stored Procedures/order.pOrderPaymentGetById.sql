SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderPaymentGetById]
	@OrderPaymentId	INT
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM
		[order].[vCustomOrderPayment]
	WHERE
		[OrderPayment.OrderPaymentId] = @OrderPaymentId
END

GO
