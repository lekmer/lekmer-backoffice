SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountAction]
	WHERE
		[FixedDiscountAction.ProductActionId] = @ProductActionId
END
GO
