SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_ImportUpdatePrice]
AS
BEGIN
	-- Update product price
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- Update package price
	EXEC [productlek].[pPackagePriceUpdate]
END
GO
