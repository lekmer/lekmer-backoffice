SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandProductListBrandGetAllByBlockInvertedSecure]
	@BlockId int
as
begin
	
	SELECT 
		b.*
	from 
		[lekmer].[tBrand] b
	where
		b.[BrandId] NOT in (Select BrandId From [lekmer].[tBlockBrandProductListBrand] where BlockId = @BlockId )
end


GO
