SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pLanguageGetById]
	@LanguageId	int
as
begin
	set nocount on

	select
		*
	from
		core.vCustomLanguage
	where
		[Language.Id] = @LanguageId
end

GO
