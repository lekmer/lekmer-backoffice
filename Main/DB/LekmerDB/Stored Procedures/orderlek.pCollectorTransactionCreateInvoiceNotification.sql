SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionCreateInvoiceNotification]
	@StoreId INT,
	@TransactionTypeId INT,
	@ResponseContent NVARCHAR(MAX),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tCollectorTransaction]
			( [StoreId],
			  [TransactionTypeId],
			  [ResponseContent],
			  [Created]
			)
	VALUES
			( @StoreId,
			  @TransactionTypeId,			  
			  @ResponseContent,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
