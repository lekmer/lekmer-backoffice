
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandTopListSave]
	@BlockId INT,
	@IncludeAllCategories BIT,
	@OrderStatisticsDayCount INT,
	@LinkContentNodeId INT
AS
BEGIN
	UPDATE lekmer.tBlockBrandTopList
	SET
		[IncludeAllCategories] = @IncludeAllCategories,
		[OrderStatisticsDayCount] = @OrderStatisticsDayCount,
		[LinkContentNodeId] = @LinkContentNodeId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0 RETURN
		
	INSERT lekmer.tBlockBrandTopList (
		[BlockId],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId]
	)
	VALUES (
		@BlockId,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId
	)
END
GO
