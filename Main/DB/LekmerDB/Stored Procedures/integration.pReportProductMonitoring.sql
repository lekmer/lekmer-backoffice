SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductMonitoring]

AS
begin
	set nocount on
	
	select 
		HYErpId,
		p.Title,
		count(*) as 'Count'
	from 
		lekmer.tMonitorProduct m 
			inner join lekmer.tLekmerProduct l on m.ProductId = l.ProductId
			inner join product.tProduct p on l.ProductId = p.ProductId
	group by 
		HYErpId, p.Title having count(*) > 1
	order by 
		count(*) desc

end
GO
