SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [product].[pPriceListSetStatus]
	@PriceListId int,
	@PriceListStatusId int
as
begin
	set nocount off	
	
	update
		[product].[tPriceList]
	set
		[PriceListStatusId] = @PriceListStatusId
	where
		[PriceListId] = @PriceListId
end







GO
