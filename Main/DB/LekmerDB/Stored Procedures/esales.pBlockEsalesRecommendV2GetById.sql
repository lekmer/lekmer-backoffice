SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pBlockEsalesRecommendV2GetById]
	@LanguageId INT,
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[esales].[vBlockEsalesRecommendV2] AS ber
	WHERE
		[ber].[Block.BlockId] = @BlockId
		AND [ber].[Block.LanguageId] = @LanguageId
END
GO
