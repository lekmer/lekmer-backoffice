SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationGetById]
	@ChannelId	int,
	@VariationId	int
as
begin
	set nocount off
	SELECT 
		V.*
		,ch.ChannelId
	FROM 
		[product].[vCustomVariation] V
		 INNER JOIN core.tChannel AS  ch ON ch.LanguageId = V.[Variation.LanguageId] 
	where
		V.[Variation.Id] = @VariationId AND ch.ChannelId = @ChannelId 
	order by V.[Variation.Ordinal]
end

GO
