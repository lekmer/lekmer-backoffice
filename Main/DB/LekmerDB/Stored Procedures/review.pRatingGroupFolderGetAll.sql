SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgf.*
	FROM
		[review].[vRatingGroupFolder] rgf
END
GO
