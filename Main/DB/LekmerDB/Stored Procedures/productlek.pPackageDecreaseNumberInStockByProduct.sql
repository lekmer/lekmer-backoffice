
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageDecreaseNumberInStockByProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @tmpPackage TABLE (PackageId INT, MasterProductId INT, NumberInStock INT)
	INSERT INTO @tmpPackage (PackageId, MasterProductId)
	SELECT DISTINCT [pak].[PackageId], [pak].[MasterProductId] FROM [productlek].[tPackage] pak
		INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
		WHERE [pp].[ProductId] = @ProductId
		
	DECLARE @tmpPackageProduct TABLE (PackageId INT, ProductId INT, ItemsCount INT)	
	INSERT INTO @tmpPackageProduct (PackageId, ProductId, ItemsCount)
	SELECT DISTINCT [pak].[PackageId], [pp].[ProductId], [a0].[ItemsCount] FROM @tmpPackage pak
		INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
		OUTER APPLY
		(
			SELECT
				COUNT([pp0].[ProductId]) 'ItemsCount'
			FROM
				[productlek].[tPackageProduct] pp0
			WHERE
				[pp0].[PackageId] = [pak].[PackageId]
				AND [pp0].[ProductId] = [pp].[ProductId]
		) a0
	
	;WITH temp AS (
		SELECT  [pak].[PackageId], MIN(p1.[NumberInStock] / [pp].[ItemsCount]) as newNumberInStock
		FROM @tmpPackage pak
		INNER JOIN @tmpPackageProduct pp ON [pp].[PackageId] = [pak].[PackageId]
		INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [pp].[ProductId]
		GROUP BY [pak].[PackageId])
	UPDATE pak
	SET [pak].[NumberInStock] = newNumberInStock
	FROM @tmpPackage pak 
	INNER JOIN [temp] t ON [t].[PackageId] = [pak].[PackageId]
END
GO
