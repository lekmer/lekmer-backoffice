SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2ProductGetAllByBlock]
	@BlockId	INT,
	@LanguageId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[bp].*,
		[pli].*
	FROM
		[esales].[vBlockEsalesTopSellersV2Product] bp
		INNER JOIN [product].[vCustomPriceListItem] pli
			ON [pli].[Price.ProductId] = [bp].[Product.Id]
			AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				[bp].[Product.CurrencyId],
				[bp].[Product.Id],
				[bp].[Product.PriceListRegistryId],
				NULL
			)
		CROSS APPLY (
			SELECT SUM([ps].[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE [ps].[ProductId] = [bp].[Product.Id]
		) a1
	WHERE
		[bp].[BlockEsalesTopSellersV2Product.BlockId] = @BlockId
		AND [bp].[Product.LanguageId] = @LanguageId
		AND ISNULL([a1].[NumberInStock], [bp].[Product.NumberInStock]) > 0
	ORDER BY
		[bp].[BlockEsalesTopSellersV2Product.Position]
END
GO
