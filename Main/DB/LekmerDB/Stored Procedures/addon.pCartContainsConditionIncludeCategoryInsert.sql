SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryInsert]
	@ConditionId			int,
	@CategoryId				int,
	@IncludeSubcategories	bit
AS 
BEGIN 
	INSERT
		[addon].tCartContainsConditionIncludeCategory
	(
		ConditionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES
	(
		@ConditionId,
		@CategoryId,
		@IncludeSubcategories
	)
END 



GO
