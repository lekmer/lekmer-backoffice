SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorInsert]
	@ChannelCommonName	VARCHAR(50),
	@FetchDate			DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [orderlek].[tMaksuturvaCompensationError] (
		[ChannelCommonName],
		[FetchDate]
	)
	VALUES (
		@ChannelCommonName,
		@FetchDate
	)
END
GO
