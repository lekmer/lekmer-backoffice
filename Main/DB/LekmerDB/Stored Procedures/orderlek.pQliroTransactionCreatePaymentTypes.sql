SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [orderlek].[pQliroTransactionCreatePaymentTypes]
	@ClientRef VARCHAR(50),
	@TransactionTypeId INT,
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [TransactionTypeId],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @TransactionTypeId,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END

GO
