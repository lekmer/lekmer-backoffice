SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pTagFlagDeleteByTag]
	@TagId INT
AS 
BEGIN 
	DELETE 
		[productlek].[tTagFlag]
	WHERE
		[TagId] = @TagId
END
GO
