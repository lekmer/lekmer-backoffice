
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionSaveResult]
	@TransactionId INT,
	@StatusCode INT,
	@ReturnCodeId INT,
	@Message NVARCHAR(MAX),
	@CustomerMessage NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tQliroTransaction]
	SET
		[StatusCode] = @StatusCode,
		[ReturnCodeId] = @ReturnCodeId,
		[Message] = @Message,
		[CustomerMessage] = @CustomerMessage,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[TransactionId] = @TransactionId
END
GO
