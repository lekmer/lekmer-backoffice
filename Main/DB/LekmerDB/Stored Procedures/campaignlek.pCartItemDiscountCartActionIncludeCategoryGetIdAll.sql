
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionIncludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
