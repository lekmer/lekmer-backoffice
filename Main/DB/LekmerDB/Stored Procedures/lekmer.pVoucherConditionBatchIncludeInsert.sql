SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionBatchIncludeInsert]
	@ConditionId INT,
	@BatchId INT
AS 
BEGIN 
	SET NOCOUNT ON

	INSERT lekmer.tVoucherBatchesInclude
	( 
		ConditionId,
		BatchId
	)
	VALUES 
	(
		@ConditionId,
		@BatchId
	)
END
GO
