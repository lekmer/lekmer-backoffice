
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetAllByProduct]
	@ProductId		INT,
	@LanguageId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	
		@Category3Id INT,
		@Category2Id INT,
		@Category1Id INT

	SELECT @Category3Id = [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId
	SELECT @Category2Id = [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @Category3Id
	SELECT @Category1Id = [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @Category2Id
	
	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ProductId] = @ProductId)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeBrand] WHERE [BrandId] = 
			(SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
		)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(
		 SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeSupplier] WHERE [SupplierId] = 
			(SELECT [s].[SupplierId] FROM [lekmer].[tLekmerProduct] lp 
			 INNER JOIN [productlek].[tSupplier] s ON [s].[SupplierNo] = [lp].[SupplierId]
			 WHERE [lp].[ProductId] = @ProductId)
		)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @Category3Id)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @Category2Id)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @Category1Id)

	ORDER BY [ContentMessage.ContentMessageId] DESC
END
GO
