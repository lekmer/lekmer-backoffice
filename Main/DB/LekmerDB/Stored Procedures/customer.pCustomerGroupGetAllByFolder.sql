SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupGetAllByFolder]
@FolderId	int
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerGroup]
	WHERE
		[CustomerGroup.FolderId] = @FolderId
		OR (@FolderId IS NULL AND [CustomerGroup.FolderId] IS NULL)
END

GO
