
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 19.09.2008		Time: 11:20
Description:
			Created 
*****************  Version 2  *****************
User: Victor E.		Date: 23.01.2009
Description: Add Path
*****************  Version 3  *****************
User: Yuriy P.		Date: 26.01.2009
Description: Remove Path
*****************  Version 4  *****************
User: Roman GRom.	Date: 19.07.2013
Description: Add LastUsedDate
*/
CREATE PROCEDURE [template].[pAliasGetByCommonName]
	@LanguageId	INT,
	@CommonName	VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		a.*
	FROM
		[template].[vCustomAlias] a
	WHERE
		a.[Alias.CommonName] = @CommonName
		AND a.[Alias.LanguageId] = @LanguageId
		
	UPDATE [template].[tAlias]
	SET [LastUsedDate] = GETDATE()
	WHERE [CommonName] = @CommonName
END
GO
