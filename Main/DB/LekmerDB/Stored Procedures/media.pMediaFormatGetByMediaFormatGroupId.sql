SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-26
-- Description:	Gets all media format by MediaFormatGroupId 
-- =============================================
CREATE PROCEDURE [media].[pMediaFormatGetByMediaFormatGroupId]
	@MediaFormatGroupId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomMediaFormat] v
	WHERE  v.[MediaFormat.GroupId] = @MediaFormatGroupId
END

GO
