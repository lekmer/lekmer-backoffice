SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetByTypeAndRegistry]
@CommonName varchar(50),
@SiteStructureRegistryId int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageSecure]
	WHERE 
	[ContentPageType.CommonName] = @CommonName
	AND [ContentNode.SiteStructureRegistryId] = @SiteStructureRegistryId
end

GO
