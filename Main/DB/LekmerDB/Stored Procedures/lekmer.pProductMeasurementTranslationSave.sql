SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductMeasurementTranslationSave]
@ProductId	int,
@LanguageId	int,
@Value		nvarchar(max)
AS
BEGIN
	SET nocount ON
	
	UPDATE
		lekmer.tLekmerProductTranslation
	SET
		Measurement = @Value 
	WHERE
		ProductId = @ProductId AND
		LanguageId = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tLekmerProductTranslation
		(
			ProductId,
			LanguageId,
			Measurement				
		)
		VALUES
		(
			@ProductId,
			@LanguageId,
			@Value
		)
	END
END




GO
