SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartActionTypeGetAll]
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCartActionType
end

GO
