
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductExportToCdonOLD2]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000533, 0, 1)	-- Root id for toys.
	
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0


   -- Import products using the same third level category	
   if object_id('tempdb..#OnlineCategory') is not null
       drop table #OnlineCategory

   create table #OnlineCategory
   (
      CategoryId int primary key
   )
   
   insert into #OnlineCategory
   select distinct CategoryId
   from product.tProduct p
   where p.ProductStatusId = 0

   if object_id('tempdb..#CategoryLevel') is not null
       drop table #CategoryLevel

   create table #CategoryLevel
   (
      CategoryId int primary key,
      ErpId varchar(13),
      Level1Id int,
      Level2Id int,
      Level3Id int
   )
   	
   insert into #CategoryLevel ( CategoryId, ErpId, Level1Id, Level2Id, Level3Id )
   select c.CategoryId, 
      c.ErpId, 
      convert(int, substring(erpId, 3, 2)) as Level1Id, 
      convert(int, substring(erpId, 6, 3)) as Level2Id, 
      convert(int, substring(erpId, 10, 4)) as Level3Id
   from #OnlineCategory oc
		inner join product.tCategory c on c.CategoryId = oc.CategoryId
   where len(c.ErpId) = 13
 

   if object_id('tempdb..#CategoryMapping') is not null
       drop table #CategoryMapping

   create table #CategoryMapping
   (
      SourceCategoryId int primary key with (ignore_dup_key = on),
      DestinationCategoryId int
   )	

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and cl2.Level2Id = cl.Level2Id
         and not cl2.Level1Id in (30)
   where cl.Level1Id = 30     

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and not cl2.Level1Id in (30)
      inner join product.tCategory c on c.CategoryId = cl.CategoryId
		and not c.Title in ('Övrigt', 'Tillbehör och accessoarer', 'Blandade titlar', 'Reservdelar och tillbehör',
			'Plocklådor')
   where cl.Level1Id = 30


--   if object_id('tempdb..#CategoryMappingPath') is not null
--       drop table #CategoryMappingPath
   
--select cm.DestinationCategoryId, ppc.Title + '/' + pc.Title + '/' + c.Title as Path1, 
--   cm.SourceCategoryId, ppc2.Title + '/' +  pc2.Title + '/' +  c2.Title as Path2
----   ,p.Title
--into #CategoryMappingPath
--from #CategoryMapping cm
--   inner join product.tCategory c on c.CategoryId = cm.DestinationCategoryId
--   inner join product.tCategory pc on pc.CategoryId = c.ParentCategoryId
--   inner join product.tCategory ppc on ppc.CategoryId = pc.ParentCategoryId      
--   inner join product.tCategory c2 on c2.CategoryId = cm.SourceCategoryId
--   inner join product.tCategory pc2 on pc2.CategoryId = c2.ParentCategoryId
--   inner join product.tCategory ppc2 on ppc2.CategoryId = pc2.ParentCategoryId      
   

--select cmp.*, p.ProductId, p.Title
--from #CategoryMappingPath cmp 
--   inner join product.tProduct p on p.CategoryId = cmp.DestinationCategoryId
--where p.ProductStatusId = 0
--order by cmp.Path1, cmp.Path2, p.Title
  
   drop table #CategoryLevel
	
	insert into @Product
	select p.ProductId
	from #CategoryMapping cm
	   inner join product.tProduct p on p.CategoryId = cm.SourceCategoryId
	where p.IsDeleted = 0

	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)


	declare @SpecialCategoryMapping table
	(
		SourceCategoryId int primary key,
		DestinationCategoryId int
	)
	 insert into @SpecialCategoryMapping ( SourceCategoryId, DestinationCategoryId )
	 select c.CategoryId, 1001016
	 from product.tCategory c
	 where ParentCategoryId = 1001015
		and not CategoryId in (1000938
,1001016
,1001017
,1001018
,1001047
,1001062
,1001065
,1001071
,1001072
,1001209
,1001284
,1001355
,1001368
,1002017)

	
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,case 
				when (scm.DestinationCategoryId is not null) then scm.DestinationCategoryId 
				when (cm.DestinationCategoryId is not null) then cm.DestinationCategoryId 			
			else 
				p.CategoryId 
			end as CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on t.ProductId = lp.ProductId
          left outer join @SpecialCategoryMapping scm on scm.SourceCategoryId = p.CategoryId
          left outer join #CategoryMapping cm on cm.SourceCategoryId = p.CategoryId
			
	  drop table #CategoryMapping
---- Discounts
--declare @PriceListDiscounts table
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2)
--)
--insert into @PriceListDiscounts ( PriceListId, ProductId, DiscountPrice )
--select 
--   case 
--      when (c.CampaignRegistryId = 1) then 1
--      when (c.CampaignRegistryId = 2) then 2
--      when (c.CampaignRegistryId = 3) then 3
--      when (c.CampaignRegistryId = 4) then 4
--      when (c.CampaignRegistryId = 5) then 5
--   else
--      c.CampaignRegistryId
--   end as PriceListId,
--   pdai.ProductId, pdai.DiscountPrice, pda.ProductActionId
--from campaign.tCampaign c
--   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId   
--   inner join lekmer.tProductDiscountAction pda on pda.ProductActionId = pa.ProductActionId
--   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pda.ProductActionId   
--where c.CampaignStatusId = 0
--   and (c.StartDate is null or c.StartDate <= getdate())
--   and (c.EndDate is null or getdate() <= c.EndDate)
--   and pdai.ProductId = 7364


--create table #PriceListDiscount
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2) primary key (PriceListId, ProductId)
--)

--insert into #PriceListDiscount
--select PriceListId, ProductId, min(DiscountPrice)
--from @PriceListDiscounts
--group by PriceListId, ProductId

if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   Price decimal(16, 2)
)
insert into #BestCampaignPrice (ProductId, PriceListId, Price)
select cp.ProductId, c.PriceListId, min(cp.Price) as Price
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
group by cp.ProductId, c.PriceListId

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	select pl.ChannelId, pli.ProductId, 
	   pli.PriceIncludingVat as Price, pli.PriceIncludingVat - pli.PriceExcludingVat as Vat, 
	   pld.Price as DiscountPrice, case when (pld.Price is null) then null else pld.Price * pli.VatPercentage / 100 end as DiscountPriceVat
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join @Product t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = t.ProductId
	
   drop table #BestCampaignPrice
		
	--drop table #PriceListDiscount
	
	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
   -- Icons				   
    SELECT [lpi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON [lpi].[ProductId] = [p].[ProductId]
	UNION
	SELECT [bi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[BrandId] = [lp].[BrandId]
	UNION
	SELECT [ci].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](p1.CategoryId))
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands  
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
