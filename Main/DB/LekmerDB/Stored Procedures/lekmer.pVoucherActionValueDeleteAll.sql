SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [lekmer].[pVoucherActionValueDeleteAll]
	@CartActionId int
as
begin
	DELETE
		lekmer.tVoucherActionCurrency
	WHERE
		CartActionId = @CartActionId
end




GO
