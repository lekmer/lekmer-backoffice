SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityGetByCommonName]
	@CommonName	varchar(50)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomEntity] 
	where
		[Entity.CommonName] = @CommonName
end

GO
