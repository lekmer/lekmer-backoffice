SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionSave]
	@CartActionId			INT,
	@DiscountAmount			DECIMAL(16,2),
	@IncludeAllProducts		BIT
AS
BEGIN
	UPDATE
		[lekmer].[tCartItemPercentageDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tCartItemPercentageDiscountAction] (
			CartActionId,
			DiscountAmount,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@DiscountAmount,
			@IncludeAllProducts
		)
	END
END

GO
