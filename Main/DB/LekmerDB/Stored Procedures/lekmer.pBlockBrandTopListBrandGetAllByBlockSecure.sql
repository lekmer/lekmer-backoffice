SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListBrandSecure] AS b
	WHERE
		b.[BlockBrandTopListBrand.BlockId] = @BlockId
	ORDER BY
		[BlockBrandTopListBrand.Position]
END
GO
