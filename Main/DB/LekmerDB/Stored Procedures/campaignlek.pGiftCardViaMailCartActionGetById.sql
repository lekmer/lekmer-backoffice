SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailCartAction]
	WHERE
		[GiftCardViaMailCartAction.CartActionId] = @CartActionId
END

GO
