SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tGiftCardViaMailProductActionCurrency]
	WHERE
		ProductActionId = @ProductActionId
END
GO
