SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductIsDeleted]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS (SELECT 1 FROM [product].[tProduct] WHERE [ProductId] = @ProductId AND [IsDeleted] = 1)
	BEGIN
		RETURN 1
	END

	RETURN 0
END
GO
