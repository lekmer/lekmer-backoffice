SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreProductSave]
	@StoreId		int,
	@ProductId		int,
	@Quantity		int,
	@Threshold		int
AS
BEGIN
	UPDATE 
		product.tStoreProduct
	SET 
		Quantity = @Quantity, 
		Threshold = @Threshold
	WHERE 
		StoreId = @StoreId
		AND ProductId = @ProductId
		
	IF @@ROWCOUNT = 0 
	BEGIN 
		INSERT 
			product.tStoreProduct
		( 
			StoreId, 
			ProductId, 
			Quantity, 
			Threshold
		)
		VALUES 
		(
			@StoreId,
			@ProductId,
			@Quantity,
			@Threshold
		)			
	END 
END 
GO
