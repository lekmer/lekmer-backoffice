SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCustomerForgotPasswordGetByGuid]
	@Guid UNIQUEIDENTIFIER
AS 
BEGIN 
	SET NOCOUNT ON
	SELECT 
		* 
	FROM 
		[lekmer].[vCustomerForgotPassword]
	WHERE 
		[ForgotPassword.Guid] = @Guid
END
GO
