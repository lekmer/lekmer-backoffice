SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagUsageDelete]
	@ProductId	INT = NULL,
	@TagIds		VARCHAR(MAX),
	@ObjectId	INT = NULL,
	@Delimiter	CHAR(1) = ','
AS
BEGIN
	IF (@ProductId IS NULL AND LEN(@TagIds)=0 AND @ObjectId IS NOT NULL)
	BEGIN
		DELETE ptu
		FROM [lekmer].[tProductTagUsage] ptu
		WHERE ptu.[ObjectId] = @ObjectId
		RETURN
	END
	
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'DELETE ptu
				FROM [lekmer].[tProductTagUsage] ptu
				INNER JOIN [lekmer].[tProductTag] pt ON [pt].[ProductTagId] = [ptu].[ProductTagId]
				INNER JOIN [generic].[fnConvertIDListToTable](@TagIds, @Delimiter) AS t ON [t].[ID] = [pt].[TagId]'

	DECLARE @IsProductId BIT = 0
	IF @ProductId IS NOT NULL
	BEGIN
		SET @sql = @sql + ' WHERE [pt].[ProductId] = @ProductId'
		SET @IsProductId = 1
	END
	
	IF @ObjectId IS NOT NULL
	BEGIN
		IF @IsProductId = 1
			SET @sql = @sql + ' AND [ptu].[ObjectId] = @ObjectId'
		ELSE
			SET @sql = @sql + ' WHERE [ptu].[ObjectId] = @ObjectId'
	END
	
	EXEC sp_executesql @sql, 
	N'@ProductId	INT = NULL,
	  @TagIds		VARCHAR(MAX),
	  @ObjectId		INT = NULL,
	  @Delimiter	CHAR(1)',
	  @ProductId,
	  @TagIds,
	  @ObjectId,
	  @Delimiter
END
GO
