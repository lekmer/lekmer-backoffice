SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 20.11.2008		Time: 18:40
Description: Created 
*/
CREATE PROCEDURE [customer].[pCustomerStatusGetById]
	@CustomerStatusId	int
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerStatus]
	WHERE
		[CustomerStatus.CustomerStatusId] = @CustomerStatusId
END

GO
