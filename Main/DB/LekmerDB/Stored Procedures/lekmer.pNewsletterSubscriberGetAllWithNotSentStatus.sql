SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberGetAllWithNotSentStatus]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vNewsletterSubscriber] ns
	WHERE
		ns.[NewsletterSubscriber.SentStatus] = 0
		OR 
		ns.[NewsletterSubscriber.SentStatus] IS NULL
END
GO
