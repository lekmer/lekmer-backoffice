SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandProductListBrandGetAllByBlock]
	@ChannelId int,
	@BlockId int
as
begin
	
	SELECT 
		bc.*, b.*
	from 
		[lekmer].[tBlockBrandProductListBrand] as bc
		inner join [lekmer].[vBrand] as b on bc.[BrandId] = b.[Brand.BrandId]
	where
		bc.[BlockId] = @BlockId and b.ChannelId = @ChannelId
end


GO
