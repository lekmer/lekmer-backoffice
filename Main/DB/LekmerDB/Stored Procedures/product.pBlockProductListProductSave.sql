SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
*****************  Version 1  *****************
User: Yura P.	Date: 22.12.2008	Time: 11:00
Description:	Created

*****************  Version 2  *****************
User: Yura P.	Date: 21.01.2009	
Description:	Edited
*/

CREATE procedure [product].[pBlockProductListProductSave]
@BlockId		int,
@ProductId		int,
@Ordinal		int
as
BEGIN
	update
		[product].[tBlockProductListProduct]
	set
		[Ordinal] = @Ordinal
	where
		[ProductId] = @ProductId
		and [BlockId] = @BlockId
		
	if  @@ROWCOUNT = 0
		begin
			insert
				[product].[tBlockProductListProduct]
				(
					[BlockId],
					[ProductId],
					[Ordinal]
				)
			values
				(
					@BlockId,
					@ProductId,
					@Ordinal
				)
		end
end



GO
