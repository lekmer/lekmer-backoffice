CREATE TABLE [campaignlek].[tFixedDiscountCartActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartActionCurrency] ADD CONSTRAINT [PK_tFixedDiscountCartActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartActionCurrency] ADD CONSTRAINT [FK_tFixedDiscountCartActionCurrency_tFixedDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tFixedDiscountCartAction] ([CartActionId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartActionCurrency] ADD CONSTRAINT [FK_tFixedDiscountCartActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
