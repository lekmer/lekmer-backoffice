CREATE TABLE [productlek].[tPackageProduct]
(
[PackageProductId] [int] NOT NULL IDENTITY(1, 1),
[PackageId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [productlek].[tPackageProduct] ADD 
CONSTRAINT [PK_tPackageProduct] PRIMARY KEY CLUSTERED  ([PackageProductId]) ON [PRIMARY]
ALTER TABLE [productlek].[tPackageProduct] ADD
CONSTRAINT [FK_tPackageProduct_tPackage] FOREIGN KEY ([PackageId]) REFERENCES [productlek].[tPackage] ([PackageId])
ALTER TABLE [productlek].[tPackageProduct] ADD
CONSTRAINT [FK_tPackageProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
