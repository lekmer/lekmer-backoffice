CREATE TABLE [addon].[tBlockTopListProduct]
(
[BlockId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Position] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopListProduct] ADD CONSTRAINT [PK_tBlockTopListProduct] PRIMARY KEY CLUSTERED  ([BlockId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopListProduct] ADD CONSTRAINT [IX_tBlockTopListProduct] UNIQUE NONCLUSTERED  ([BlockId], [Position]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopListProduct] ADD CONSTRAINT [FK_tBlockTopListProduct_tBlockTopList] FOREIGN KEY ([BlockId]) REFERENCES [addon].[tBlockTopList] ([BlockId])
GO
ALTER TABLE [addon].[tBlockTopListProduct] ADD CONSTRAINT [FK_tBlockTopListProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
