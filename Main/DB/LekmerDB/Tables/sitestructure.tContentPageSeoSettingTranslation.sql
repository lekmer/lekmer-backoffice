CREATE TABLE [sitestructure].[tContentPageSeoSettingTranslation]
(
[ContentNodeId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ADD CONSTRAINT [PK_tContentPageSeoSettingTranslation] PRIMARY KEY CLUSTERED  ([ContentNodeId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ADD CONSTRAINT [FK_tContentPageSeoSettingTranslation_tContentPageSeoSetting] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentPageSeoSetting] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ADD CONSTRAINT [FK_tContentPageSeoSettingTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
