CREATE TABLE [integration].[tempProductDescription]
(
[HYarticleId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Language] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[DescriptionType] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Text] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
