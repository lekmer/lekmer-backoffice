CREATE TABLE [review].[tRatingGroup]
(
[RatingGroupId] [int] NOT NULL IDENTITY(1, 1),
[RatingGroupFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRatingGroup_CommonName] ON [review].[tRatingGroup] ([CommonName]) ON [PRIMARY]

ALTER TABLE [review].[tRatingGroup] ADD
CONSTRAINT [FK_tRatingGroup_tRatingGroupFolder] FOREIGN KEY ([RatingGroupFolderId]) REFERENCES [review].[tRatingGroupFolder] ([RatingGroupFolderId])
GO
ALTER TABLE [review].[tRatingGroup] ADD CONSTRAINT [PK_tRatingGroup] PRIMARY KEY CLUSTERED  ([RatingGroupId]) ON [PRIMARY]
GO
