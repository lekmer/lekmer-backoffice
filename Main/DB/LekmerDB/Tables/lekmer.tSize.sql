CREATE TABLE [lekmer].[tSize]
(
[SizeId] [int] NOT NULL IDENTITY(1, 1),
[ErpId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ErpTitle] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EU] [decimal] (5, 2) NOT NULL,
[EUTitle] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[USMale] [decimal] (3, 1) NOT NULL,
[USFemale] [decimal] (3, 1) NOT NULL,
[UKMale] [decimal] (3, 1) NOT NULL,
[UKFemale] [decimal] (3, 1) NOT NULL,
[Millimeter] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSize] ADD CONSTRAINT [UC_ErpId] UNIQUE NONCLUSTERED  ([ErpId]) ON [PRIMARY]

ALTER TABLE [lekmer].[tSize] ADD 
CONSTRAINT [PK_tSize] PRIMARY KEY CLUSTERED  ([SizeId]) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tSize_ErpId] ON [lekmer].[tSize] ([ErpId]) ON [PRIMARY]



GO
