CREATE TABLE [integration].[SEOBrands]
(
[ContentNodeId] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ContentNodeTitle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
