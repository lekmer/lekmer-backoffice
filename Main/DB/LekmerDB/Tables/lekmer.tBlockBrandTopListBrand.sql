CREATE TABLE [lekmer].[tBlockBrandTopListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Position] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [PK_tBlockBrandTopListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [IX_tBlockBrandTopListBrand] UNIQUE NONCLUSTERED  ([BlockId], [Position]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [FK_tBlockBrandTopListBrand_tBlockBrandTopList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandTopList] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [FK_tBlockBrandTopListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
