CREATE TABLE [productlek].[tBlockProductType]
(
[BlockId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockProductType] ADD CONSTRAINT [PK_tBlockProductType] PRIMARY KEY CLUSTERED  ([BlockId], [ProductTypeId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockProductType] ADD CONSTRAINT [FK_tBlockProductType_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [productlek].[tBlockProductType] ADD CONSTRAINT [FK_tBlockProductType_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
