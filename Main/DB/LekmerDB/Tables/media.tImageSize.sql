CREATE TABLE [media].[tImageSize]
(
[ImageSizeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Width] [int] NOT NULL,
[Height] [int] NOT NULL,
[Quality] [int] NOT NULL,
[MobileWidth] [int] NULL,
[MobileHeight] [int] NULL,
[MobileQuality] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [media].[tImageSize] ADD CONSTRAINT [PK_tImageSize] PRIMARY KEY CLUSTERED  ([ImageSizeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tImageSize_CommonName] ON [media].[tImageSize] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tImageSize_Quality_Width_Height] ON [media].[tImageSize] ([Quality], [Width], [Height]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Could be smallint, but skipped for fear of reprisal.', 'SCHEMA', N'media', 'TABLE', N'tImageSize', 'COLUMN', N'Height'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Could be tinyint, but skipped for fear of reprisal.', 'SCHEMA', N'media', 'TABLE', N'tImageSize', 'COLUMN', N'Quality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Could be smallint, but skipped for fear of reprisal.', 'SCHEMA', N'media', 'TABLE', N'tImageSize', 'COLUMN', N'Width'
GO
