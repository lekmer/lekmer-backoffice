CREATE TABLE [template].[tTheme]
(
[ThemeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tTheme] ADD CONSTRAINT [PK_tTheme] PRIMARY KEY CLUSTERED  ([ThemeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tTheme_Title] ON [template].[tTheme] ([Title]) ON [PRIMARY]
GO
