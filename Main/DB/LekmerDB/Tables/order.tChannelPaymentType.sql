CREATE TABLE [order].[tChannelPaymentType]
(
[ChannelId] [int] NOT NULL,
[PaymentTypeId] [int] NOT NULL,
[IsPersonDefault] [bit] NOT NULL CONSTRAINT [DF_tChannelPaymentType_IsDefault] DEFAULT ((0)),
[IsCompanyDefault] [bit] NOT NULL CONSTRAINT [DF_tChannelPaymentType_IsCompanyDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [order].[tChannelPaymentType] ADD CONSTRAINT [PK_ChannelPaymentType] PRIMARY KEY CLUSTERED  ([ChannelId], [PaymentTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tChannelPaymentType] ON [order].[tChannelPaymentType] ([ChannelId], [PaymentTypeId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tChannelPaymentType] ADD CONSTRAINT [FK_tChannelPaymentType_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [order].[tChannelPaymentType] ADD CONSTRAINT [FK_tChannelPaymentType_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
