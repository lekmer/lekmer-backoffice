CREATE TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage]
(
[CampaignLandingPageId] [int] NOT NULL,
[CampaignRegistryId] [int] NOT NULL,
[IconMediaId] [int] NULL,
[ImageMediaId] [int] NULL,
[LinkContentNodeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage] ADD CONSTRAINT [PK_tCampaignRegistryCampaignLandingPage] PRIMARY KEY CLUSTERED  ([CampaignLandingPageId], [CampaignRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage] ADD CONSTRAINT [FK_tCampaignRegistryCampaignLandingPage_tCampaignLandingPage] FOREIGN KEY ([CampaignLandingPageId]) REFERENCES [campaignlek].[tCampaignLandingPage] ([CampaignLandingPageId])
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage] ADD CONSTRAINT [FK_tCampaignRegistryCampaignLandingPage_tCampaignRegistry] FOREIGN KEY ([CampaignRegistryId]) REFERENCES [campaign].[tCampaignRegistry] ([CampaignRegistryId])
GO
