CREATE TABLE [addon].[tCartContainsConditionExcludeCategory]
(
[ConditionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeCategory] ADD CONSTRAINT [PK_tCartContainsConditionExcludeCategory] PRIMARY KEY CLUSTERED  ([ConditionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeCategory] ADD CONSTRAINT [FK_tCartContainsConditionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeCategory] ADD CONSTRAINT [FK_tCartContainsConditionExcludeCategory_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId])
GO
