CREATE TABLE [campaignlek].[tFixedPriceActionIncludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeCategory] ADD CONSTRAINT [PK_tFixedPriceActionIncludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionIncludeCategory_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
