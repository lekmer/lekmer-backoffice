CREATE TABLE [lekmer].[tBlockContactUs]
(
[BlockId] [int] NOT NULL,
[Receiver] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockContactUs] ADD CONSTRAINT [PK_tBlockContactUs] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockContactUs] ADD CONSTRAINT [FK_tBlockContactUs_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
