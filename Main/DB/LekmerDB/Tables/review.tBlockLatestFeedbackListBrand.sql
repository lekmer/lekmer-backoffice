CREATE TABLE [review].[tBlockLatestFeedbackListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockLatestFeedbackListBrand] ADD CONSTRAINT [PK_tBlockLatestFeedbackListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockLatestFeedbackListBrand] ADD CONSTRAINT [FK_tBlockLatestFeedbackListBrand_tBlockLatestFeedbackList] FOREIGN KEY ([BlockId]) REFERENCES [review].[tBlockLatestFeedbackList] ([BlockId])
GO
ALTER TABLE [review].[tBlockLatestFeedbackListBrand] ADD CONSTRAINT [FK_tBlockLatestFeedbackListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
