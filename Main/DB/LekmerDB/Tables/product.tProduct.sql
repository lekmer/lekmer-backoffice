CREATE TABLE [product].[tProduct]
(
[ProductId] [int] NOT NULL IDENTITY(1, 1),
[ItemsInPackage] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_tProduct_IsDeleted] DEFAULT ((0)),
[NumberInStock] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[Title] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[WebShopTitle] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL CONSTRAINT [DF_tProduct_Description] DEFAULT (''),
[ShortDescription] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[MediaId] [int] NULL,
[ProductStatusId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tProduct_ErpId] ON [product].[tProduct] ([ErpId]) ON [PRIMARY]



CREATE NONCLUSTERED INDEX [IX_tProduct_IsDeleted_ProductStatusId] ON [product].[tProduct] ([IsDeleted], [ProductStatusId]) INCLUDE ([CategoryId], [NumberInStock], [ProductId], [Title]) ON [PRIMARY]

GO
EXEC sp_addextendedproperty N'Description', N'Index is used by [lekmer].[vFilterProduct]', 'SCHEMA', N'product', 'TABLE', N'tProduct', 'INDEX', N'IX_tProduct_IsDeleted_ProductStatusId'
GO

ALTER TABLE [product].[tProduct] ADD CONSTRAINT [PK_tProduct] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProduct_CategoryId] ON [product].[tProduct] ([CategoryId]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_tProduct_IsDeleted] ON [product].[tProduct] ([IsDeleted]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProduct] ON [product].[tProduct] ([IsDeleted], [ProductStatusId]) INCLUDE ([ProductId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProduct_Covered] ON [product].[tProduct] ([IsDeleted], [ProductStatusId]) INCLUDE ([CategoryId], [ProductId], [Title]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProduct_MediaId] ON [product].[tProduct] ([MediaId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProduct] ADD CONSTRAINT [FK_tProduct_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [product].[tProduct] ADD CONSTRAINT [FK_tProduct_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId]) ON DELETE SET NULL
GO
ALTER TABLE [product].[tProduct] ADD CONSTRAINT [FK_tProduct_tProductStatus] FOREIGN KEY ([ProductStatusId]) REFERENCES [product].[tProductStatus] ([ProductStatusId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of products in one package.', 'SCHEMA', N'product', 'TABLE', N'tProduct', 'COLUMN', N'ItemsInPackage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique, automatic, identity for the product.', 'SCHEMA', N'product', 'TABLE', N'tProduct', 'COLUMN', N'ProductId'
GO
CREATE FULLTEXT INDEX ON [product].[tProduct] KEY INDEX [PK_tProduct] ON [fulltextcatalog_tproduct_default]
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ADD ([Title] LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [product].[tProduct] ADD ([WebShopTitle] LANGUAGE 0)
GO
