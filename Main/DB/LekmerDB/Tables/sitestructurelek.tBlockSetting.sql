CREATE TABLE [sitestructurelek].[tBlockSetting]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[ColumnCountMobile] [int] NULL,
[RowCount] [int] NOT NULL,
[RowCountMobile] [int] NULL,
[TotalItemCount] [int] NOT NULL,
[TotalItemCountMobile] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructurelek].[tBlockSetting] ADD CONSTRAINT [PK_tBlockSetting] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructurelek].[tBlockSetting] ADD CONSTRAINT [FK_tBlockSetting_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
