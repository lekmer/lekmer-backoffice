CREATE TABLE [lekmer].[tProductUrlHistory]
(
[ProductUrlHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[UrlTitleOld] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tProductUrlHistory_CreationDate] DEFAULT (getdate()),
[LastUsageDate] [datetime] NOT NULL CONSTRAINT [DF_tProductUrlHistory_LastUsageDate] DEFAULT (getdate()),
[UsageAmount] [int] NOT NULL CONSTRAINT [DF_tProductUrlHistory_UsageAmount] DEFAULT ((0)),
[HistoryLifeIntervalTypeId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tProductUrlHistory] ADD
CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])

GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [PK_tProductUrlHistory] PRIMARY KEY CLUSTERED  ([ProductUrlHistoryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tProductUrlHistory_ProductId_LanguageId_UrlTitleOld] ON [lekmer].[tProductUrlHistory] ([ProductId], [LanguageId], [UrlTitleOld]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductUrlHistory_UrlTitleOld] ON [lekmer].[tProductUrlHistory] ([UrlTitleOld]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [FK_tProductUrlHistory_tHistoryLifeIntervalType] FOREIGN KEY ([HistoryLifeIntervalTypeId]) REFERENCES [lekmer].[tHistoryLifeIntervalType] ([HistoryLifeIntervalTypeId])
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [FK_tProductUrlHistory_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
