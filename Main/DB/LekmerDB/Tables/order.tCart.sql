CREATE TABLE [order].[tCart]
(
[CartId] [int] NOT NULL IDENTITY(1, 1),
[CartGuid] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_tCart_CreatedDateTime] DEFAULT (getdate()),
[UpdatedDate] [datetime] NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[RemovedItemsCount] [int] NOT NULL CONSTRAINT [DF_tCart_RemovedItemsCount] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [order].[tCart] ADD CONSTRAINT [PK_tCart] PRIMARY KEY CLUSTERED  ([CartId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCart_CartGuid] ON [order].[tCart] ([CartGuid]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'The cart used in session by a visitor or customer.', 'SCHEMA', N'order', 'TABLE', N'tCart', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique identity of the cart.', 'SCHEMA', N'order', 'TABLE', N'tCart', 'COLUMN', N'CartId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date and time when the cart was created.', 'SCHEMA', N'order', 'TABLE', N'tCart', 'COLUMN', N'CreatedDate'
GO
