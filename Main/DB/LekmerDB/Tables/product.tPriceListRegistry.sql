CREATE TABLE [product].[tPriceListRegistry]
(
[PriceListRegistryId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceListRegistry] ADD CONSTRAINT [PK_tPriceListRegistry] PRIMARY KEY CLUSTERED  ([PriceListRegistryId]) ON [PRIMARY]
GO
