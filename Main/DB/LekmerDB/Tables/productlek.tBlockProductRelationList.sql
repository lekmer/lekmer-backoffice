CREATE TABLE [productlek].[tBlockProductRelationList]
(
[BlockId] [int] NOT NULL,
[ProductSortOrderId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockProductRelationList] ADD CONSTRAINT [PK_tBlockProductRelationList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductRelationList_ProductSortOrderId] ON [productlek].[tBlockProductRelationList] ([ProductSortOrderId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockProductRelationList] ADD CONSTRAINT [FK_tBlockProductRelationList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [productlek].[tBlockProductRelationList] ADD CONSTRAINT [FK_tBlockProductRelationList_tProductSortOrder] FOREIGN KEY ([ProductSortOrderId]) REFERENCES [product].[tProductSortOrder] ([ProductSortOrderId])
GO
