CREATE TABLE [integration].[tImageImportLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Data] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tImageImportLog] ADD CONSTRAINT [PK__tImageIm__3214EC0758F81366] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
