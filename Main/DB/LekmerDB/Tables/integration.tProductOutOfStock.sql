CREATE TABLE [integration].[tProductOutOfStock]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[OutOfStockDate] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tProductOutOfStock] ADD 
CONSTRAINT [PK_tProductOutOfStock] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId]) ON [PRIMARY]
GO
