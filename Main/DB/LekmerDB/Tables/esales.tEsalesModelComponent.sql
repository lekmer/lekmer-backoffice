CREATE TABLE [esales].[tEsalesModelComponent]
(
[ModelComponentId] [int] NOT NULL IDENTITY(1, 1),
[ModelId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelComponent] ADD CONSTRAINT [PK_tEsalesModelComponent] PRIMARY KEY CLUSTERED  ([ModelComponentId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelComponent] ADD CONSTRAINT [FK_tEsalesModelComponent_tEsalesModel] FOREIGN KEY ([ModelId]) REFERENCES [esales].[tEsalesModel] ([ModelId])
GO
