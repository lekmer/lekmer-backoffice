CREATE TABLE [media].[tMedia]
(
[MediaId] [int] NOT NULL IDENTITY(1, 1),
[MediaFormatId] [int] NOT NULL,
[MediaFolderId] [int] NOT NULL,
[Title] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[FileName] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [media].[tMedia] ADD CONSTRAINT [PK_tMedia] PRIMARY KEY CLUSTERED  ([MediaId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tMedia_MediaFolderId] ON [media].[tMedia] ([MediaFolderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tMedia_MediaFormatId] ON [media].[tMedia] ([MediaFormatId]) ON [PRIMARY]
GO
ALTER TABLE [media].[tMedia] ADD CONSTRAINT [FK_tMedia_tMediaFolder] FOREIGN KEY ([MediaFolderId]) REFERENCES [media].[tMediaFolder] ([MediaFolderId])
GO
ALTER TABLE [media].[tMedia] ADD CONSTRAINT [FK_tMedia_tMediaFormat] FOREIGN KEY ([MediaFormatId]) REFERENCES [media].[tMediaFormat] ([MediaFormatId])
GO
