CREATE TABLE [esales].[tBlockEsalesRecommendV2]
(
[BlockId] [int] NOT NULL,
[PanelName] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[WindowLastEsalesValue] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [esales].[tBlockEsalesRecommendV2] ADD 
CONSTRAINT [PK_BlockEsalesRecommendV2] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
ALTER TABLE [esales].[tBlockEsalesRecommendV2] ADD
CONSTRAINT [FK_BlockEsalesRecommendV2_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
