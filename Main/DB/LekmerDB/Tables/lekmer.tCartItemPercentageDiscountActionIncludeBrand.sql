CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeBrand_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
