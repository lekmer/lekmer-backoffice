CREATE TABLE [lekmer].[tAllProductTempTable]
(
[ProductId] [int] NOT NULL,
[PriceListId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[PriceListRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
