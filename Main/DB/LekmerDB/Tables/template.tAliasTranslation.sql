CREATE TABLE [template].[tAliasTranslation]
(
[AliasId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Value] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [template].[tAliasTranslation] ADD CONSTRAINT [PK_tAliasTranslation] PRIMARY KEY CLUSTERED  ([AliasId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tAliasTranslation] ADD CONSTRAINT [FK_tAliasTranslation_tAlias] FOREIGN KEY ([AliasId]) REFERENCES [template].[tAlias] ([AliasId])
GO
ALTER TABLE [template].[tAliasTranslation] ADD CONSTRAINT [FK_tAliasTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
