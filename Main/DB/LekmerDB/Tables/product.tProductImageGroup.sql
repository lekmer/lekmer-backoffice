CREATE TABLE [product].[tProductImageGroup]
(
[ProductImageGroupId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[MaxCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductImageGroup] ADD CONSTRAINT [PK_tProductImageGroup] PRIMARY KEY CLUSTERED  ([ProductImageGroupId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductImageGroup] ADD CONSTRAINT [UQ_tProductImageGroup_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
