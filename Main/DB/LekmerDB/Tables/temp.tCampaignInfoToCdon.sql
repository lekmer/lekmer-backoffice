CREATE TABLE [temp].[tCampaignInfoToCdon]
(
[Status] [varchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[Product] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CategoryErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Category] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[NumberInStock] [int] NULL,
[PriceList] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceListPrice] [decimal] (16, 2) NULL,
[Price] [decimal] (16, 2) NULL,
[DiscountPrice] [decimal] (16, 2) NULL,
[Discount] [decimal] (16, 2) NULL,
[DiscountPercentage] [int] NULL,
[Campaign] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[CampaignStart] [datetime] NULL,
[CampaignEnd] [datetime] NULL
) ON [PRIMARY]
GO
