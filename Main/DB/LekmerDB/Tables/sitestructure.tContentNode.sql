CREATE TABLE [sitestructure].[tContentNode]
(
[ContentNodeId] [int] NOT NULL IDENTITY(1, 1),
[ParentContentNodeId] [int] NULL,
[ContentNodeTypeId] [int] NOT NULL,
[ContentNodeStatusId] [int] NOT NULL,
[Title] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[AccessId] [int] NOT NULL CONSTRAINT [DF_tContentNode_AccessId] DEFAULT ((1)),
[SiteStructureRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNode] ADD CONSTRAINT [PK_tContentNode] PRIMARY KEY CLUSTERED  ([ContentNodeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentNode_AccessId] ON [sitestructure].[tContentNode] ([AccessId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tContentNode_ContentNodeId_SSRegistryId] ON [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentNode_ContentNodeStatusId] ON [sitestructure].[tContentNode] ([ContentNodeStatusId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentNode_ContentNodeTypeId] ON [sitestructure].[tContentNode] ([ContentNodeTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentNode_ParentContentNodeId] ON [sitestructure].[tContentNode] ([ParentContentNodeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentNode_SiteStructureRegistryId] ON [sitestructure].[tContentNode] ([SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNode] ADD CONSTRAINT [FK_tContentNode_tAccess] FOREIGN KEY ([AccessId]) REFERENCES [sitestructure].[tAccess] ([AccessId])
GO
ALTER TABLE [sitestructure].[tContentNode] ADD CONSTRAINT [FK_tContentNode_tContentNodeStatus] FOREIGN KEY ([ContentNodeStatusId]) REFERENCES [sitestructure].[tContentNodeStatus] ([ContentNodeStatusId])
GO
ALTER TABLE [sitestructure].[tContentNode] ADD CONSTRAINT [FK_tContentNode_tContentNodeType] FOREIGN KEY ([ContentNodeTypeId]) REFERENCES [sitestructure].[tContentNodeType] ([ContentNodeTypeId])
GO
ALTER TABLE [sitestructure].[tContentNode] ADD CONSTRAINT [FK_tContentNode_tContentNode] FOREIGN KEY ([ParentContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tContentNode] ADD CONSTRAINT [FK_tContentNode_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
