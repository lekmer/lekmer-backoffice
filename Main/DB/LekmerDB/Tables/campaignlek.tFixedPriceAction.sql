CREATE TABLE [campaignlek].[tFixedPriceAction]
(
[ProductActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tFixedPriceAction_IncludeAllProducts] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceAction] ADD CONSTRAINT [PK_tFixedPriceAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceAction] ADD CONSTRAINT [FK_tFixedPriceAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
