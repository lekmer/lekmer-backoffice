CREATE TABLE [review].[tRating]
(
[RatingId] [int] NOT NULL IDENTITY(1, 1),
[RatingFolderId] [int] NOT NULL,
[RatingRegistryId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[CommonForVariants] [bit] NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRating_RatingRegistryId_CommonName] ON [review].[tRating] ([RatingRegistryId], [CommonName]) ON [PRIMARY]

ALTER TABLE [review].[tRating] ADD
CONSTRAINT [FK_tRating_tRatingFolder] FOREIGN KEY ([RatingFolderId]) REFERENCES [review].[tRatingFolder] ([RatingFolderId])
GO
ALTER TABLE [review].[tRating] ADD CONSTRAINT [PK_tRatingInfo] PRIMARY KEY CLUSTERED  ([RatingId]) ON [PRIMARY]
GO

ALTER TABLE [review].[tRating] ADD CONSTRAINT [FK_tRating_tRatingRegistry] FOREIGN KEY ([RatingRegistryId]) REFERENCES [review].[tRatingRegistry] ([RatingRegistryId])
GO
