CREATE TABLE [lekmer].[tBrandIcon]
(
[BrandId] [int] NOT NULL,
[IconId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrandIcon] ADD CONSTRAINT [PK_tBrandIcon] PRIMARY KEY CLUSTERED  ([BrandId], [IconId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrandIcon] ADD CONSTRAINT [FK_tBrandIcon_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tBrandIcon] ADD CONSTRAINT [FK_tBrandIcon_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
GO
