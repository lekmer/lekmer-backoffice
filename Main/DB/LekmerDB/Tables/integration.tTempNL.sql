CREATE TABLE [integration].[tTempNL]
(
[HyId] [nvarchar] (25) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tTempNL] ADD CONSTRAINT [PK__tTempNL__67B3117156A3C881] PRIMARY KEY CLUSTERED  ([HyId]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
