CREATE TABLE [integration].[tempSupplier]
(
[SupplierNo] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ActivePassiveCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[Address] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[ZipArea] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[City] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CountryISO] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[LanguageISO] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[VATRate] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone1] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone2] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tempSupplier] ADD CONSTRAINT [PK_tempSupplier] PRIMARY KEY CLUSTERED  ([SupplierNo]) ON [PRIMARY]
GO
