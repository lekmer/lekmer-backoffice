CREATE TABLE [integration].[tProductStockStore]
(
[ErpId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[LagerProductTitle] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[LagerAmmount] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ImportDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductStockStore] ADD CONSTRAINT [PK_tProductStockStore] PRIMARY KEY CLUSTERED  ([ErpId]) ON [PRIMARY]
GO
