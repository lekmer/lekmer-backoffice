CREATE TABLE [sitestructure].[tContentPage]
(
[ContentNodeId] [int] NOT NULL CONSTRAINT [DF_tContentPage_ContentNodeId] DEFAULT ((1)),
[SiteStructureRegistryId] [int] NOT NULL,
[TemplateId] [int] NOT NULL,
[MasterTemplateId] [int] NULL,
[Title] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[UrlTitle] [varchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ContentPageTypeId] [int] NOT NULL CONSTRAINT [DF_tContentPage_ContentPageTypeId] DEFAULT ((1)),
[IsMaster] [bit] NOT NULL CONSTRAINT [DF_tContentPage_IsMaster] DEFAULT ((0)),
[MasterPageId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPage] ADD CONSTRAINT [PK_tContentPage] PRIMARY KEY CLUSTERED  ([ContentNodeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tContentPage_ContentNodeId_SSRegistryId] ON [sitestructure].[tContentPage] ([ContentNodeId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentPage_tContentPageTypeId] ON [sitestructure].[tContentPage] ([ContentPageTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentPage_MasterPageId] ON [sitestructure].[tContentPage] ([MasterPageId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentPage_TemplateId] ON [sitestructure].[tContentPage] ([TemplateId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPage] WITH NOCHECK ADD CONSTRAINT [FK_tContentPage_tContentNode] FOREIGN KEY ([ContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId])
GO
ALTER TABLE [sitestructure].[tContentPage] ADD CONSTRAINT [FK_tContentPage_tContentPageType] FOREIGN KEY ([ContentPageTypeId]) REFERENCES [sitestructure].[tContentPageType] ([ContentPageTypeId])
GO
ALTER TABLE [sitestructure].[tContentPage] ADD CONSTRAINT [FK_tContentPage_tContentPage] FOREIGN KEY ([MasterPageId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tContentPage] ADD CONSTRAINT [FK_tContentPage_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
