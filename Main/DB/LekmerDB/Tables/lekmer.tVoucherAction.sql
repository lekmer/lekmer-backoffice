CREATE TABLE [lekmer].[tVoucherAction]
(
[CartActionId] [int] NOT NULL,
[Fixed] [bit] NOT NULL,
[Percentage] [bit] NOT NULL,
[DiscountValue] [decimal] (16, 2) NULL,
[AllowSpecialOffer] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherAction] ADD CONSTRAINT [PK_tVoucherAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherAction] ADD CONSTRAINT [FK_tVoucherAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
