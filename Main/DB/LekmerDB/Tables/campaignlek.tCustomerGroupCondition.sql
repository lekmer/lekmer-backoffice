CREATE TABLE [campaignlek].[tCustomerGroupCondition]
(
[ConditionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCustomerGroupCondition] ADD CONSTRAINT [PK_tCustomerGroupCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCustomerGroupCondition] ADD CONSTRAINT [FK_tCustomerGroupCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
