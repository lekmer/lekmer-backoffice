CREATE TABLE [review].[tRatingReviewStatus]
(
[RatingReviewStatusId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewStatus] ADD CONSTRAINT [PK_tRatingReviewStatus] PRIMARY KEY CLUSTERED  ([RatingReviewStatusId]) ON [PRIMARY]
GO
