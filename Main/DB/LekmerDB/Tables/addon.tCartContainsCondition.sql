CREATE TABLE [addon].[tCartContainsCondition]
(
[ConditionId] [int] NOT NULL,
[MinQuantity] [int] NOT NULL,
[AllowDuplicates] [bit] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsCondition] ADD CONSTRAINT [PK_tCartContainsCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsCondition] ADD CONSTRAINT [FK_tCartContainsCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
