CREATE TABLE [productlek].[tProductEventStatus]
(
[ProductEventStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tProductEventStatus] ADD CONSTRAINT [PK_tProductEventStatus] PRIMARY KEY CLUSTERED  ([ProductEventStatusId]) ON [PRIMARY]
GO
