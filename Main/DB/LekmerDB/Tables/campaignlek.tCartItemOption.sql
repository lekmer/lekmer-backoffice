CREATE TABLE [campaignlek].[tCartItemOption]
(
[CartItemOptionId] [int] NOT NULL IDENTITY(1, 1),
[CartId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_tCartItemOption_Quantity] DEFAULT ((1)),
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [campaignlek].[tCartItemOption] ADD
CONSTRAINT [FK_tCartItemOption_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
ALTER TABLE [campaignlek].[tCartItemOption] ADD CONSTRAINT [PK_tCartItemOption] PRIMARY KEY CLUSTERED  ([CartItemOptionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemOption] ADD CONSTRAINT [FK_tCartItemOption_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId])
GO
ALTER TABLE [campaignlek].[tCartItemOption] ADD CONSTRAINT [FK_tCartItemOption_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
