CREATE TABLE [order].[tOrderItemStatus]
(
[OrderItemStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderItemStatus] ADD CONSTRAINT [PK_tOrderItemStatus] PRIMARY KEY CLUSTERED  ([OrderItemStatusId]) ON [PRIMARY]
GO
