CREATE TABLE [productlek].[tColorTranslation]
(
[ColorId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tColorTranslation] ADD CONSTRAINT [PK_tColorTranslation] PRIMARY KEY CLUSTERED  ([ColorId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tColorTranslation] ADD CONSTRAINT [FK_tColorTranslation_tColor] FOREIGN KEY ([ColorId]) REFERENCES [productlek].[tColor] ([ColorId])
GO
ALTER TABLE [productlek].[tColorTranslation] ADD CONSTRAINT [FK_tColorTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
