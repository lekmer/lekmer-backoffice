CREATE TABLE [sitestructure].[tContentNodeUrlLink]
(
[ContentNodeId] [int] NOT NULL,
[LinkUrl] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeUrlLink] ADD CONSTRAINT [PK_tContentNodeUrlLink] PRIMARY KEY CLUSTERED  ([ContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeUrlLink] ADD CONSTRAINT [FK_tContentNodeUrlLink_tContentNode] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
