CREATE TABLE [product].[tStoreProductSize]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[StoreId] [int] NULL,
[Quantity] [int] NOT NULL,
[Threshold] [int] NULL CONSTRAINT [DF_tStoreProductSize_Threshold] DEFAULT ((0))
) ON [PRIMARY]
ALTER TABLE [product].[tStoreProductSize] ADD 
CONSTRAINT [PK_tStoreProductSize] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId]) ON [PRIMARY]
ALTER TABLE [product].[tStoreProductSize] ADD
CONSTRAINT [FK_tStoreProductSize_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [product].[tStoreProductSize] ADD
CONSTRAINT [FK_tStoreProductSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
