CREATE TABLE [security].[tRole]
(
[RoleId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL CONSTRAINT [DF_tRole_Title] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [security].[tRole] ADD CONSTRAINT [PK_tRole] PRIMARY KEY CLUSTERED  ([RoleId]) ON [PRIMARY]
GO
ALTER TABLE [security].[tRole] ADD CONSTRAINT [UQ_tRole_Title] UNIQUE NONCLUSTERED  ([Title]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the role. Used i GUI of Back office.', 'SCHEMA', N'security', 'TABLE', N'tRole', 'COLUMN', N'Title'
GO
