CREATE TABLE [campaign].[tCartCampaign]
(
[CampaignId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartCampaign] ADD CONSTRAINT [PK_tCartCampaign] PRIMARY KEY CLUSTERED  ([CampaignId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartCampaign] ADD CONSTRAINT [FK_tCartCampaign_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
