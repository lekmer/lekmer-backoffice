CREATE TABLE [integration].[tLekmerArtNo]
(
[HYarticleId] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[LakmerArtNo] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tLekmerArtNo] ADD 
CONSTRAINT [PK_tLekmerArtNo] PRIMARY KEY CLUSTERED  ([HYarticleId]) ON [PRIMARY]
GO
