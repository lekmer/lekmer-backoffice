CREATE TABLE [lekmer].[tLekmerChannel]
(
[ChannelId] [int] NOT NULL,
[TimeFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[WeekDayFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DayFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DateTimeFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TimeZoneDiff] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[VatPercentage] [decimal] (16, 2) NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tLekmerChannel_ErpId] ON [lekmer].[tLekmerChannel] ([ErpId]) ON [PRIMARY]



ALTER TABLE [lekmer].[tLekmerChannel] ADD 
CONSTRAINT [PK_tLekmerChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
ALTER TABLE [lekmer].[tLekmerChannel] ADD
CONSTRAINT [FK_tLekmerChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
