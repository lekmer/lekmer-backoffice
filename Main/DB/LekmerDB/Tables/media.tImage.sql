CREATE TABLE [media].[tImage]
(
[MediaId] [int] NOT NULL,
[Width] [int] NOT NULL,
[Height] [int] NOT NULL,
[AlternativeText] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [media].[tImage] ADD CONSTRAINT [PK_tImage] PRIMARY KEY CLUSTERED  ([MediaId]) ON [PRIMARY]
GO
ALTER TABLE [media].[tImage] ADD CONSTRAINT [FK_tImage_tMedia] FOREIGN KEY ([MediaId]) REFERENCES [media].[tMedia] ([MediaId])
GO
