CREATE TABLE [campaignlek].[tFixedPriceActionExcludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeCategory] ADD CONSTRAINT [PK_tFixedPriceActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionExcludeCategory_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
