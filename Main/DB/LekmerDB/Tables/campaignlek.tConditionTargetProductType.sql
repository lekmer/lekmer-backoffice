CREATE TABLE [campaignlek].[tConditionTargetProductType]
(
[ConditionId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tConditionTargetProductType] ADD CONSTRAINT [PK_tConditionTargetProductType] PRIMARY KEY CLUSTERED  ([ConditionId], [ProductTypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tConditionTargetProductType] ADD CONSTRAINT [FK_tConditionTargetProductType_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tConditionTargetProductType] ADD CONSTRAINT [FK_tConditionTargetProductType_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
