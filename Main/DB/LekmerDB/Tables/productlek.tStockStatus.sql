CREATE TABLE [productlek].[tStockStatus]
(
[StockStatusId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tStockStatus] ADD CONSTRAINT [PK_tStockStatus] PRIMARY KEY CLUSTERED  ([StockStatusId]) ON [PRIMARY]
GO
