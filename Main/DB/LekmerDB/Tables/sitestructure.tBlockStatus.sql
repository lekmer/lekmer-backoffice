CREATE TABLE [sitestructure].[tBlockStatus]
(
[BlockStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockStatus] ADD CONSTRAINT [PK_tBlockStatus] PRIMARY KEY CLUSTERED  ([BlockStatusId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockStatus] ADD CONSTRAINT [UQ_tBlockStatus_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
