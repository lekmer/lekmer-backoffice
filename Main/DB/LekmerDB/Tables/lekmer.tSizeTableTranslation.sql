CREATE TABLE [lekmer].[tSizeTableTranslation]
(
[SizeTableId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Column1Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Column2Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableTranslation] ADD 
CONSTRAINT [PK_tSizeTableTranslation] PRIMARY KEY CLUSTERED  ([SizeTableId], [LanguageId]) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableTranslation] ADD
CONSTRAINT [FK_tSizeTableTranslation_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableTranslation] ADD
CONSTRAINT [FK_tSizeTableTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
