CREATE TABLE [integration].[TempLekmerSizes]
(
[HyId] [nvarchar] (5) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SizeDisplyed] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[TempLekmerSizes] ADD CONSTRAINT [PK__TempLekm__67B3117164DFE9BC] PRIMARY KEY CLUSTERED  ([HyId]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
