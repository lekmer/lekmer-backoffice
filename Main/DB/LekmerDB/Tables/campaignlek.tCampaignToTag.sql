CREATE TABLE [campaignlek].[tCampaignToTag]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[ProcessingDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignToTag] ADD CONSTRAINT [PK_tCampaignToTag] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
