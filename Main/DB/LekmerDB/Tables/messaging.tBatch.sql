CREATE TABLE [messaging].[tBatch]
(
[BatchId] [uniqueidentifier] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NULL,
[MachineName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ProcessId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tBatch] ADD CONSTRAINT [PK_tBatch] PRIMARY KEY NONCLUSTERED  ([BatchId]) ON [PRIMARY]
GO
