CREATE TABLE [export].[tCdonExportIncludeBrand]
(
[ProductRegistryId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportIncludeBrand] ADD CONSTRAINT [PK_tCdonExportIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportIncludeBrand] ADD CONSTRAINT [FK_tCdonExportIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [export].[tCdonExportIncludeBrand] ADD CONSTRAINT [FK_tCdonExportIncludeBrand_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
