CREATE TABLE [template].[tAlias]
(
[AliasId] [int] NOT NULL IDENTITY(1, 1),
[AliasFolderId] [int] NOT NULL,
[CommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AliasTypeId] [int] NOT NULL,
[Value] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[LastUsedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tAlias_CommonName] ON [template].[tAlias] ([CommonName]) ON [PRIMARY]

GO
ALTER TABLE [template].[tAlias] ADD CONSTRAINT [PK_tAlias] PRIMARY KEY CLUSTERED  ([AliasId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tAlias_AliasTypeId] ON [template].[tAlias] ([AliasTypeId]) ON [PRIMARY]
GO

ALTER TABLE [template].[tAlias] ADD CONSTRAINT [FK_tAlias_tAliasFolder] FOREIGN KEY ([AliasFolderId]) REFERENCES [template].[tAliasFolder] ([AliasFolderId])
GO
ALTER TABLE [template].[tAlias] ADD CONSTRAINT [FK_tAlias_tAliasType] FOREIGN KEY ([AliasTypeId]) REFERENCES [template].[tAliasType] ([AliasTypeId])
GO
