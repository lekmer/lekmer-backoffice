CREATE TABLE [product].[tProductSeoSetting]
(
[ProductId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSeoSetting] ADD CONSTRAINT [PK_tProductSEOSettings_1] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSeoSetting] ADD CONSTRAINT [FK_tProductSEOSettings_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
