CREATE TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeBrand_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
