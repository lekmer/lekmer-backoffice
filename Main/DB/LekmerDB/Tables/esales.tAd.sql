CREATE TABLE [esales].[tAd]
(
[AdId] [int] NOT NULL IDENTITY(1, 1),
[FolderId] [int] NOT NULL,
[Key] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DisplayFrom] [date] NULL,
[DisplayTo] [date] NULL,
[ProductCategory] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Gender] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Format] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[SearchTags] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tAd] ADD CONSTRAINT [PK_tAd] PRIMARY KEY CLUSTERED  ([AdId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tAd] ADD CONSTRAINT [FK_tAd_tAdFolder] FOREIGN KEY ([FolderId]) REFERENCES [esales].[tAdFolder] ([AdFolderId])
GO
