CREATE TABLE [export].[tCdonExportIncludeCategory]
(
[ProductRegistryId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportIncludeCategory] ADD CONSTRAINT [PK_tCdonExportIncludeCategory] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportIncludeCategory] ADD CONSTRAINT [FK_tCdonExportIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [export].[tCdonExportIncludeCategory] ADD CONSTRAINT [FK_tCdonExportIncludeCategory_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
