CREATE TABLE [review].[tReviewHistory]
(
[ReviewHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ReviewId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [nvarchar] (3000) COLLATE Finnish_Swedish_CI_AS NULL,
[CreationDate] [datetime] NOT NULL,
[AuthorId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tReviewHistory] ADD CONSTRAINT [PK_tReviewHistory] PRIMARY KEY CLUSTERED  ([ReviewHistoryId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tReviewHistory] ADD CONSTRAINT [FK_tReviewHistory_tSystemUser] FOREIGN KEY ([AuthorId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
ALTER TABLE [review].[tReviewHistory] ADD CONSTRAINT [FK_tReviewHistory_tReview] FOREIGN KEY ([ReviewId]) REFERENCES [review].[tReview] ([ReviewId])
GO
