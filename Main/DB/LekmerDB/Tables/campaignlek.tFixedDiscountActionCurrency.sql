CREATE TABLE [campaignlek].[tFixedDiscountActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionCurrency] ADD CONSTRAINT [PK_tFixedDiscountActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionCurrency] ADD CONSTRAINT [FK_tFixedDiscountActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionCurrency] ADD CONSTRAINT [FK_tFixedDiscountActionCurrency_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
