CREATE TABLE [sitestructure].[tContentArea]
(
[ContentAreaId] [int] NOT NULL IDENTITY(1, 1),
[TemplateId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentArea] ADD CONSTRAINT [PK_tContentArea] PRIMARY KEY CLUSTERED  ([ContentAreaId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentArea] ADD CONSTRAINT [UQ_tContentArea_TemplateId_CommonName] UNIQUE NONCLUSTERED  ([TemplateId], [CommonName]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentArea] ADD CONSTRAINT [FK_tContentArea_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
