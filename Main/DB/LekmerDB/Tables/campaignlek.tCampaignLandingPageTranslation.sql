CREATE TABLE [campaignlek].[tCampaignLandingPageTranslation]
(
[CampaignLandingPageId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[WebTitle] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignLandingPageTranslation] ADD CONSTRAINT [PK_tCampaignLandingPageTranslation] PRIMARY KEY CLUSTERED  ([CampaignLandingPageId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignLandingPageTranslation] ADD CONSTRAINT [FK_tCampaignLandingPageTranslation_tCampaignLandingPage] FOREIGN KEY ([CampaignLandingPageId]) REFERENCES [campaignlek].[tCampaignLandingPage] ([CampaignLandingPageId]) ON DELETE CASCADE
GO
ALTER TABLE [campaignlek].[tCampaignLandingPageTranslation] ADD CONSTRAINT [FK_tCampaignLandingPageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
