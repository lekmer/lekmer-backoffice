CREATE TABLE [customerlek].[tFacebookCustomerUser]
(
[CustomerId] [int] NOT NULL,
[CustomerRegistryId] [int] NOT NULL,
[FacebookId] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Name] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Email] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [customerlek].[tFacebookCustomerUser] ADD CONSTRAINT [PK_tFacebookCustomerUser] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
ALTER TABLE [customerlek].[tFacebookCustomerUser] ADD CONSTRAINT [FK_tCustomer_tFacebookCustomerUser] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
