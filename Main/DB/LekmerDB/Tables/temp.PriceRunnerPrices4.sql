CREATE TABLE [temp].[PriceRunnerPrices4]
(
[country] [varchar] (5) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[productId] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[price] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
