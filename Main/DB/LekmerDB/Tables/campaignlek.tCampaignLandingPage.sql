CREATE TABLE [campaignlek].[tCampaignLandingPage]
(
[CampaignLandingPageId] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[WebTitle] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignLandingPage] ADD CONSTRAINT [PK_tCampaignLandingPage] PRIMARY KEY CLUSTERED  ([CampaignLandingPageId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignLandingPage_CampaignId] ON [campaignlek].[tCampaignLandingPage] ([CampaignId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignLandingPage] ADD CONSTRAINT [FK_tCampaignLandingPage_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
