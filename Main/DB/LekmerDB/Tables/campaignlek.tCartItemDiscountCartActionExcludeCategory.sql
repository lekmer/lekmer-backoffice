CREATE TABLE [campaignlek].[tCartItemDiscountCartActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemDiscountCartActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemDiscountCartActionExcludeCategory_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemDiscountCartActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
