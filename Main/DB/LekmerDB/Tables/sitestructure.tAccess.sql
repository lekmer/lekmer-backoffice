CREATE TABLE [sitestructure].[tAccess]
(
[AccessId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tAccess] ADD CONSTRAINT [PK_tAccess] PRIMARY KEY CLUSTERED  ([AccessId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tAccess_CommonName] ON [sitestructure].[tAccess] ([CommonName]) ON [PRIMARY]
GO
