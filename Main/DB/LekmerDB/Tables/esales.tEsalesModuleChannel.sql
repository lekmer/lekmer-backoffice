CREATE TABLE [esales].[tEsalesModuleChannel]
(
[ChannelId] [int] NOT NULL,
[EsalesRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModuleChannel] ADD CONSTRAINT [PK_tEsalesModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tEsalesModuleChannel_EsalesRegistryId] ON [esales].[tEsalesModuleChannel] ([EsalesRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModuleChannel] ADD CONSTRAINT [FK_tEsalesModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [esales].[tEsalesModuleChannel] ADD CONSTRAINT [FK_tEsalesModuleChannel_tEsalesRegistry] FOREIGN KEY ([EsalesRegistryId]) REFERENCES [esales].[tEsalesRegistry] ([EsalesRegistryId])
GO
