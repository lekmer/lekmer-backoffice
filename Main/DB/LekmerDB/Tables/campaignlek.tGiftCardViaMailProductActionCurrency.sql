CREATE TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency] ADD CONSTRAINT [PK_tGiftCardViaMailProductActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency] ADD CONSTRAINT [FK_tGiftCardViaMailProductActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency] ADD CONSTRAINT [FK_tGiftCardViaMailProductActionCurrency_tGiftCardViaMailProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tGiftCardViaMailProductAction] ([ProductActionId])
GO
