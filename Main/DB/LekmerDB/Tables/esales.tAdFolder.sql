CREATE TABLE [esales].[tAdFolder]
(
[AdFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tAdFolder] ADD CONSTRAINT [PK_tAdFolder] PRIMARY KEY CLUSTERED  ([AdFolderId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tAdFolder] ADD CONSTRAINT [FK_tAdFolder_tAdFolder] FOREIGN KEY ([ParentFolderId]) REFERENCES [esales].[tAdFolder] ([AdFolderId])
GO
