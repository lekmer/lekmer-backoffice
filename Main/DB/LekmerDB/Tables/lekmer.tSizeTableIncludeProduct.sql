CREATE TABLE [lekmer].[tSizeTableIncludeProduct]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD 
CONSTRAINT [PK_tSizeTableIncludeProduct] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD
CONSTRAINT [FK_tSizeTableIncludeProduct_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD
CONSTRAINT [FK_tSizeTableIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])

GO
