CREATE TABLE [productlek].[tPackage]
(
[PackageId] [int] NOT NULL IDENTITY(1, 1),
[MasterProductId] [int] NOT NULL,
[GeneralInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackage] ADD CONSTRAINT [PK_tPackage] PRIMARY KEY CLUSTERED  ([PackageId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackage] WITH NOCHECK ADD CONSTRAINT [FK_tPackage_tProduct] FOREIGN KEY ([MasterProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
ALTER TABLE [productlek].[tPackage] NOCHECK CONSTRAINT [FK_tPackage_tProduct]
GO
