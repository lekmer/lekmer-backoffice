CREATE TABLE [campaignlek].[tFreightValueActionDeliveryMethod]
(
[CartActionId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFreightValueActionDeliveryMethod] ADD CONSTRAINT [PK_tFreightValueActionDeliveryMethod] PRIMARY KEY CLUSTERED  ([CartActionId], [DeliveryMethodId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFreightValueActionDeliveryMethod] ADD CONSTRAINT [FK_tFreightValueActionDeliveryMethod_tFreightValueAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tFreightValueAction] ([CartActionId])
GO
ALTER TABLE [campaignlek].[tFreightValueActionDeliveryMethod] ADD CONSTRAINT [FK_tFreightValueActionDeliveryMethod_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
