CREATE TABLE [lekmer].[tBlockBrandTopListCategory]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD CONSTRAINT [PK_tBlockBrandTopListCategory] PRIMARY KEY CLUSTERED  ([BlockId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD CONSTRAINT [FK_tBlockBrandTopListCategory_tBlockBrandTopList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandTopList] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD CONSTRAINT [FK_tBlockBrandTopListCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
