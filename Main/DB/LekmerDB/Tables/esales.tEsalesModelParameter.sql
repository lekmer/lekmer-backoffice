CREATE TABLE [esales].[tEsalesModelParameter]
(
[ModelParameterId] [int] NOT NULL IDENTITY(1, 1),
[ModelId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NOT NULL,
[Name] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [varchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelParameter] ADD CONSTRAINT [PK_tEsalesModelParameter] PRIMARY KEY CLUSTERED  ([ModelParameterId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tEsalesModelParameter] ON [esales].[tEsalesModelParameter] ([ModelId], [SiteStructureRegistryId], [Name]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelParameter] ADD CONSTRAINT [FK_tEsalesModelParameter_tEsalesModel] FOREIGN KEY ([ModelId]) REFERENCES [esales].[tEsalesModel] ([ModelId])
GO
ALTER TABLE [esales].[tEsalesModelParameter] ADD CONSTRAINT [FK_tEsalesModelParameter_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
