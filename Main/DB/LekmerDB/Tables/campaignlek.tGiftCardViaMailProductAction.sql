CREATE TABLE [campaignlek].[tGiftCardViaMailProductAction]
(
[ProductActionId] [int] NOT NULL,
[SendingInterval] [int] NOT NULL,
[ConfigId] [int] NOT NULL,
[TemplateId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD
CONSTRAINT [FK_tGiftCardViaMailProductAction_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD CONSTRAINT [PK_tGiftCardViaMailProductAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD CONSTRAINT [FK_tGiftCardViaMailProductAction_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD CONSTRAINT [FK_tGiftCardViaMailProductAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
