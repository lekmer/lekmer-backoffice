CREATE TABLE [integration].[tProductsTranslatedInHY]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tProductsTranslatedInHY] ADD 
CONSTRAINT [PK_tProductsTranslatedInHY] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
