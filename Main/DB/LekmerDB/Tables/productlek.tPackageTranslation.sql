CREATE TABLE [productlek].[tPackageTranslation]
(
[PackageId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[GeneralInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackageTranslation] ADD CONSTRAINT [PK_tPackageTranslation] PRIMARY KEY CLUSTERED  ([PackageId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackageTranslation] ADD CONSTRAINT [FK_tPackageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
ALTER TABLE [productlek].[tPackageTranslation] ADD CONSTRAINT [FK_tPackageTranslation_tPackage] FOREIGN KEY ([PackageId]) REFERENCES [productlek].[tPackage] ([PackageId]) ON DELETE CASCADE
GO
