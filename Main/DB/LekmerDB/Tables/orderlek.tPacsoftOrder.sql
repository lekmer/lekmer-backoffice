CREATE TABLE [orderlek].[tPacsoftOrder]
(
[OrderId] [int] NOT NULL,
[NeedSendPacsoftInfo] [bit] NOT NULL CONSTRAINT [DF_tPacsoftOrder_NeedSendPacsoftInfo] DEFAULT ((1))
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPacsoftOrder_OrderId] ON [orderlek].[tPacsoftOrder] ([OrderId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tPacsoftOrder] ADD CONSTRAINT [FK_tPacsoftOrder_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
