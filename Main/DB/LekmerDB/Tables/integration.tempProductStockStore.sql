CREATE TABLE [integration].[tempProductStockStore]
(
[ErpId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[LagerProductTitle] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[LagerAmmount] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
