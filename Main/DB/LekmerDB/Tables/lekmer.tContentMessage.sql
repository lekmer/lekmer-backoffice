CREATE TABLE [lekmer].[tContentMessage]
(
[ContentMessageId] [int] NOT NULL IDENTITY(1, 1),
[ContentMessageFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[IsDropShip] [bit] NOT NULL CONSTRAINT [DF_tContentMessage_IsDropShip] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[StartDailyIntervalMinutes] [int] NULL,
[EndDailyIntervalMinutes] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessage] ADD CONSTRAINT [PK_tContentMessage] PRIMARY KEY CLUSTERED  ([ContentMessageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessage] ADD CONSTRAINT [FK_tContentMessage_tContentMessageFolder] FOREIGN KEY ([ContentMessageFolderId]) REFERENCES [lekmer].[tContentMessageFolder] ([ContentMessageFolderId])
GO
