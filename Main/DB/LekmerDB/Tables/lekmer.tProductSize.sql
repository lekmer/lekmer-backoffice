CREATE TABLE [lekmer].[tProductSize]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NumberInStock] [int] NOT NULL,
[MillimeterDeviation] [int] NULL,
[OverrideEU] [decimal] (3, 1) NULL,
[OverrideMillimeter] [int] NULL,
[Weight] [decimal] (18, 3) NULL,
[StockStatusId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tProductSize] ADD 
CONSTRAINT [PK_tProductSize] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId]) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tProductSize_NumberInStock(ProductId)] ON [lekmer].[tProductSize] ([NumberInStock]) INCLUDE ([ProductId]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId] ON [lekmer].[tProductSize] ([ProductId]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId_NumberInStock] ON [lekmer].[tProductSize] ([ProductId], [NumberInStock]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tProductSize_ErpId] ON [lekmer].[tProductSize] ([ErpId]) ON [PRIMARY]

ALTER TABLE [lekmer].[tProductSize] ADD
CONSTRAINT [FK_tProductSize_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductSize] ADD
CONSTRAINT [FK_tProductSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
ALTER TABLE [lekmer].[tProductSize] ADD
CONSTRAINT [FK_tProductSize_tStockStatus] FOREIGN KEY ([StockStatusId]) REFERENCES [productlek].[tStockStatus] ([StockStatusId])









GO
