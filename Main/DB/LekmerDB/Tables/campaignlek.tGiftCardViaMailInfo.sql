CREATE TABLE [campaignlek].[tGiftCardViaMailInfo]
(
[GiftCardViaMailInfoId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[CampaignId] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[DiscountValue] [decimal] (16, 2) NOT NULL,
[DateToSend] [datetime] NOT NULL,
[StatusId] [int] NOT NULL,
[VoucherInfoId] [int] NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD 
CONSTRAINT [PK_tGiftCardViaMailInfo] PRIMARY KEY CLUSTERED  ([GiftCardViaMailInfoId]) ON [PRIMARY]
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD
CONSTRAINT [FK_tGiftCardViaMailInfo_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD
CONSTRAINT [FK_tGiftCardViaMailInfo_tGiftCardViaMailInfoStatus] FOREIGN KEY ([StatusId]) REFERENCES [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId])

GO
