CREATE TABLE [lekmer].[tContentMessageFolder]
(
[ContentMessageFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentContentMessageFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageFolder] ADD CONSTRAINT [PK_tContentMessageFolder] PRIMARY KEY CLUSTERED  ([ContentMessageFolderId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageFolder] ADD CONSTRAINT [FK_tContentMessageFolder_tContentMessageFolder] FOREIGN KEY ([ParentContentMessageFolderId]) REFERENCES [lekmer].[tContentMessageFolder] ([ContentMessageFolderId])
GO
