CREATE TABLE [campaign].[tCampaignModuleChannel]
(
[ChannelId] [int] NOT NULL,
[CampaignRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignModuleChannel] ADD CONSTRAINT [PK_tCampaignModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCampaignModuleChannel_CampagnRegistryId] ON [campaign].[tCampaignModuleChannel] ([CampaignRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignModuleChannel] ADD CONSTRAINT [FK_tCampaignModuleChannel_tCampaignRegistry] FOREIGN KEY ([CampaignRegistryId]) REFERENCES [campaign].[tCampaignRegistry] ([CampaignRegistryId])
GO
ALTER TABLE [campaign].[tCampaignModuleChannel] ADD CONSTRAINT [FK_tCampaignModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
