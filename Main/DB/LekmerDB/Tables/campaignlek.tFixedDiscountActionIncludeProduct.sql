CREATE TABLE [campaignlek].[tFixedDiscountActionIncludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tFixedDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tFixedDiscountActionIncludeProduct_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tFixedDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
