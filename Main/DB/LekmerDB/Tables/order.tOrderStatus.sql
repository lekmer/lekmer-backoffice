CREATE TABLE [order].[tOrderStatus]
(
[OrderStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderStatus] ADD CONSTRAINT [PK_tOrderStatus] PRIMARY KEY CLUSTERED  ([OrderStatusId]) ON [PRIMARY]
GO
