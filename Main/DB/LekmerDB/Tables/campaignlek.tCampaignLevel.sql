CREATE TABLE [campaignlek].[tCampaignLevel]
(
[LevelId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Priority] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignLevel] ADD CONSTRAINT [PK_tCampaignLevel] PRIMARY KEY CLUSTERED  ([LevelId]) ON [PRIMARY]
GO
