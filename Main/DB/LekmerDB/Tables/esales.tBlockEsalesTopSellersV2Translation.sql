CREATE TABLE [esales].[tBlockEsalesTopSellersV2Translation]
(
[BlockId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[UrlTitle] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Translation] ADD CONSTRAINT [PK_tBlockEsalesTopSellersV2Translation] PRIMARY KEY CLUSTERED  ([BlockId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Translation] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2Translation_tBlockEsalesTopSellersV2] FOREIGN KEY ([BlockId]) REFERENCES [esales].[tBlockEsalesTopSellersV2] ([BlockId])
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Translation] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2Translation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
