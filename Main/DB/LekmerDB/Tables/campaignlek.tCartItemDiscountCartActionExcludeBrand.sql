CREATE TABLE [campaignlek].[tCartItemDiscountCartActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemDiscountCartActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemDiscountCartActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemDiscountCartActionExcludeBrand_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId]) ON DELETE CASCADE
GO
