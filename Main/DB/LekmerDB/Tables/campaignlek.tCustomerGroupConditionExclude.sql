CREATE TABLE [campaignlek].[tCustomerGroupConditionExclude]
(
[ConditionId] [int] NOT NULL,
[CustomerGroupId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionExclude] ADD CONSTRAINT [PK_tCustomerGroupConditionExclude] PRIMARY KEY CLUSTERED  ([ConditionId], [CustomerGroupId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionExclude] ADD CONSTRAINT [FK_tCustomerGroupConditionExclude_tCustomerGroupCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCustomerGroupCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionExclude] ADD CONSTRAINT [FK_tCustomerGroupConditionExclude_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId]) ON DELETE CASCADE
GO
