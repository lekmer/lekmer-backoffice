SET IDENTITY_INSERT [template].[tModelFolder] ON

SET IDENTITY_INSERT [template].[tModelFolder] OFF
SET IDENTITY_INSERT [template].[tModelFolder] ON
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (19, N'Master')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (20, N'Site structure')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (21, N'Product')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (23, N'Order')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (31, N'Customer')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (32, N'General')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (182, N'E-mail')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (183, N'Search')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (184, N'Campaign')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (1000001, N'Avail')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (1000002, N'ServiceCenter')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (1000003, N'Product review')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (1000004, N'eSales')
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (1000005, N'Payment')
SET IDENTITY_INSERT [template].[tModelFolder] OFF
