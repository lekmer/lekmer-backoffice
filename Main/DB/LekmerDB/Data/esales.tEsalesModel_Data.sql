SET IDENTITY_INSERT [esales].[tEsalesModel] ON
INSERT INTO [esales].[tEsalesModel] ([ModelId], [Title], [CommonName], [Ordinal]) VALUES (1, N'Category Page', N'CategoryPage', 10)
INSERT INTO [esales].[tEsalesModel] ([ModelId], [Title], [CommonName], [Ordinal]) VALUES (2, N'Brand Page', N'BrandPage', 20)
INSERT INTO [esales].[tEsalesModel] ([ModelId], [Title], [CommonName], [Ordinal]) VALUES (3, N'Top Category Page', N'TopCategoryPage', 30)
SET IDENTITY_INSERT [esales].[tEsalesModel] OFF
