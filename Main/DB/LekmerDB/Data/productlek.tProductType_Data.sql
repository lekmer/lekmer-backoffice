SET IDENTITY_INSERT [productlek].[tProductType] ON
INSERT INTO [productlek].[tProductType] ([ProductTypeId], [Title], [CommonName]) VALUES (1, N'Product', N'product')
INSERT INTO [productlek].[tProductType] ([ProductTypeId], [Title], [CommonName]) VALUES (2, N'Package', N'package')
SET IDENTITY_INSERT [productlek].[tProductType] OFF
