SET IDENTITY_INSERT [review].[tRatingReviewStatus] ON
INSERT INTO [review].[tRatingReviewStatus] ([RatingReviewStatusId], [CommonName], [Title]) VALUES (4, N'Pending', N'Pending')
INSERT INTO [review].[tRatingReviewStatus] ([RatingReviewStatusId], [CommonName], [Title]) VALUES (5, N'Approved', N'Approved')
SET IDENTITY_INSERT [review].[tRatingReviewStatus] OFF
