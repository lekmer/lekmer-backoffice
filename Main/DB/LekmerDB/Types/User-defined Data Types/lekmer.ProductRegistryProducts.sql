CREATE TYPE [lekmer].[ProductRegistryProducts] AS TABLE
(
[ProductId] [int] NOT NULL,
[ProductRegistryId] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId])
)
GO
