
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fGetImageUrl]
(
	@ProductId INT,
	@ApplicationName VARCHAR(100)
)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @MediaId INT

	SELECT @MediaId = MediaId FROM (
		SELECT TOP 1 MediaId
		FROM product.tProductImage
		WHERE ProductImageGroupId = 1 AND ProductId = @ProductId
		ORDER BY ordinal) t

	RETURN 'http://' + @ApplicationName + '/mediaarchive/' + CAST(@MediaId AS VARCHAR) + '/' + CAST(@MediaId AS VARCHAR) + '.jpg'
END
GO
