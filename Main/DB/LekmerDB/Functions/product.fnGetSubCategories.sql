SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [product].[fnGetSubCategories](
	@CategoryId int
)
RETURNS @tSubCategories TABLE (CategoryId int)
AS 
BEGIN
	WITH SubCategories (CategoryId, ParentCategoryId) AS 
	(
		SELECT CategoryId, ParentCategoryId
		FROM product.tCategory
		WHERE ParentCategoryId = @CategoryId
		UNION ALL
		SELECT C.CategoryId, C.ParentCategoryId
		FROM product.tCategory C 
		JOIN SubCategories SC ON SC.CategoryId = C.ParentCategoryId
	)
	INSERT INTO @tSubCategories (CategoryId)
	SELECT CategoryId FROM SubCategories
	UNION ALL
	SELECT @CategoryId
	
	RETURN 
END 
GO
