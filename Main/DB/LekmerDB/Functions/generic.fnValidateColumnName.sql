SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [generic].[fnValidateColumnName]
(
	@Parameter VARCHAR(max)
)
RETURNS BIT
AS
BEGIN
	DECLARE @Position INT, @Result INT
	SET @Position = 1
	SET @Result = 1
	WHILE @Position < LEN(@Parameter) + 1
	BEGIN
		IF (SUBSTRING(@Parameter, @Position, 1) NOT LIKE '[0-9a-z.\[\]]' escape '\')
			BEGIN
				SET @Result = 0
				BREAK
			END
		
		SET @Position = @Position + 1
	END
	RETURN @Result
END

GO
