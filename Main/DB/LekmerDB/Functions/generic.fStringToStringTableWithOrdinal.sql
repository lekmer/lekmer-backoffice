SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fStringToStringTableWithOrdinal](
	@String NVARCHAR(MAX),
	@Delimiter CHAR(1) = ','
)
RETURNS @tblSepString TABLE(SepString NVARCHAR(50), Ordinal INT)
AS
BEGIN
	IF @String IS NULL OR @String = ''
		RETURN

	IF SUBSTRING(@String, LEN(@String), 1) <> @Delimiter
		SELECT @String = @String + @Delimiter

	DECLARE @ind1 INT, @ind2 INT, @counter INT
	SELECT @ind1 = 1
	
	SET @counter = 1;

	WHILE 1 = 1
	BEGIN
		SELECT @ind2 = CHARINDEX(@Delimiter, @String, @ind1)

		IF @ind2 = 0
			BREAK

		INSERT
			@tblSepString
		VALUES
		(
			SUBSTRING(@String, @ind1, @ind2 - @ind1),
			@counter
		)
		SET @counter = @counter + 1
		SET @ind1 = @ind2 + 1
	END

	RETURN
END
GO
