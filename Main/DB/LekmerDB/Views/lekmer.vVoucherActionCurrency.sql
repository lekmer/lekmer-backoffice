SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vVoucherActionCurrency]
as
	select
		v.CartActionId AS 'VoucherAction.ActionId',
		v.CurrencyId AS 'VoucherAction.CurrencyId',
		v.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		lekmer.tVoucherActionCurrency v
		inner join core.vCustomCurrency c on v.CurrencyId = c.[Currency.Id]


GO
