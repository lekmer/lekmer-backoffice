
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vPercentagePriceDiscountAction]
AS
	SELECT
		[ppda].[ProductActionId] AS 'PercentagePriceDiscountAction.ProductActionId',
		[ppda].[DiscountAmount] AS 'PercentagePriceDiscountAction.DiscountAmount',
		[ppda].[ConfigId] AS 'PercentagePriceDiscountAction.ConfigId',
		[pa].*
	FROM
		[campaign].[tPercentagePriceDiscountAction] ppda
		INNER JOIN [campaign].[vCustomProductAction] pa on [pa].[ProductAction.Id] = [ppda].[ProductActionId]
GO
