SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vSizeTableRowTranslation]
AS
SELECT 
	[SizeTableRowId]	AS 'SizeTableRowTranslation.SizeTableRowId',
	[LanguageId]		AS 'SizeTableRowTranslation.LanguageId',
	[Column1Value]		AS 'SizeTableRowTranslation.Column1Value',
	[Column2Value]		AS 'SizeTableRowTranslation.Column2Value'
FROM 
	[lekmer].[tSizeTableRowTranslation]
GO
