SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [sitestructure].[vBlockListSecure]
AS
	SELECT
		[b].*,
		[bl].*,
		[bs].*
	FROM
		[sitestructure].[tBlock] bl
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [b].[Block.BlockId] = [bl].[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]



GO
