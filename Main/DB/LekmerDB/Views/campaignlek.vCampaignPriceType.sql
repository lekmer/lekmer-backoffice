SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vCampaignPriceType]
AS
	SELECT 
		[CampaignPriceTypeId]	'CampaignPriceType.Id',
		[CommonName]			'CampaignPriceType.CommonName',
		[Title]					'CampaignPriceType.Title'
	FROM 
		[campaignlek].[tCampaignPriceType]
GO
