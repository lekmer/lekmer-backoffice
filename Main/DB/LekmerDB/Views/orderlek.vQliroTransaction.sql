
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [orderlek].[vQliroTransaction]
AS
SELECT
	[qt].[TransactionId],
	[qt].[ClientRef],
	[qt].[Mode] AS 'ProductionStatusType',
	
	CASE [qt].[Mode]
		WHEN 0 THEN 'Production'
		WHEN 1 THEN 'Test'
		ELSE CONVERT(VARCHAR(50), [qt].[Mode])
	END AS 'ProductionStatusType.Name',
	
	[qt].[TransactionTypeId],
	
	CASE [qt].[TransactionTypeId]
		WHEN 0 THEN 'None'
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'ReserveAmount'
		WHEN 3 THEN 'ReserveAmountCompany'
		WHEN 4 THEN 'CancelReservation'
		WHEN 5 THEN 'CheckOrderStatus'
		WHEN 6 THEN 'GetPaymentTypes'
		WHEN 7 THEN 'ReserveAmountTimeout'
		ELSE CONVERT(VARCHAR(50), [qt].[TransactionTypeId])
	END AS 'TransactionType.Name',
	
	[qt].[TimeoutProcessed],
	[qt].[StatusCode],
	
	CASE [qt].[StatusCode]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Ok'
		WHEN 2 THEN 'TimeoutResponse'
		WHEN 3 THEN 'UnspecifiedError'
		ELSE CONVERT(VARCHAR(50), [qt].[StatusCode])
	END AS 'StatusCode.Name',
	
	[qt].[Created],
	[qt].[CivicNumber],
	[qt].[Duration],
	[qt].[OrderId],
	[qt].[Amount],
	[qt].[CurrencyCode],
	[qt].[PaymentType],
	[qt].[ReservationNumber],
	[qt].[InvoiceStatus],
	
	CASE [qt].[InvoiceStatus]
		WHEN 0 THEN 'Invalid'
		WHEN 1 THEN 'Ok'
		WHEN 2 THEN 'Denied'
		WHEN 3 THEN 'Holding'
		WHEN 4 THEN 'Timeout'
		WHEN 5 THEN 'NoRisk'
		ELSE CONVERT(VARCHAR(50), [qt].[InvoiceStatus])
	END AS 'InvoiceStatus.Name',
	
	[qt].[ReturnCodeId],
	[qt].[Message],
	[qt].[CustomerMessage],
	[qt].[ResponseContent]
FROM
	[orderlek].[tQliroTransaction] qt



GO
