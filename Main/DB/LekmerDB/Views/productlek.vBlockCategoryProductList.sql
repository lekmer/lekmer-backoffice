SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vBlockCategoryProductList]
AS
SELECT 
	[bcpl].[BlockId] AS 'BlockCategoryProductList.BlockId',
	[bcpl].[ProductSortOrderId] AS 'BlockCategoryProductList.ProductSortOrderId',
    [pso].*,
    [bs].*
  FROM [productlek].[tBlockCategoryProductList] bcpl
  INNER JOIN [product].[vCustomProductSortOrder] pso ON [pso].[ProductSortOrder.Id] = [bcpl].[ProductSortOrderId]
  INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bcpl].[BlockId]
GO
