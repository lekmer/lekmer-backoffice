SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingFolder]
AS
	SELECT
		rf.[RatingFolderId]			'RatingFolder.RatingFolderId',
		rf.[ParentRatingFolderId]	'RatingFolder.ParentRatingFolderId',
		rf.[Title]					'RatingFolder.Title'
	FROM 
		[review].[tRatingFolder] rf
GO
