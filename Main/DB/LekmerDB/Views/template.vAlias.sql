SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [template].[vAlias]
AS
SELECT     
      a.AliasId AS 'Alias.AliasId', 
      a.AliasFolderId AS 'Alias.AliasFolderId', 
      a.Description AS 'Alias.Description', 
      a.CommonName AS 'Alias.CommonName', 
      a.AliasTypeId AS 'Alias.AliasTypeId',
      COALESCE (at.Value, a.Value) AS 'Alias.Value', 
      t.LanguageId AS 'Alias.LanguageId'
FROM
      template.tAlias AS a
      CROSS JOIN core.tLanguage AS t
      LEFT JOIN template.tAliasTranslation AS at ON at.AliasId = a.AliasId AND at.LanguageId = t.LanguageId

GO
