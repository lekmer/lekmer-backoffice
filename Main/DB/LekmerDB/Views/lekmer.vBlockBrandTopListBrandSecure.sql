SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBlockBrandTopListBrandSecure]
AS
	SELECT
		btlb.[BlockId] AS 'BlockBrandTopListBrand.BlockId',
		btlb.[BrandId] AS 'BlockBrandTopListBrand.BrandId',
		btlb.[Position] AS 'BlockBrandTopListBrand.Position',
		b.*
	FROM
		[lekmer].[tBlockBrandTopListBrand] btlb
		INNER JOIN [lekmer].[vBrandSecure] b on b.[Brand.BrandId] = btlb.[BrandId]
GO
