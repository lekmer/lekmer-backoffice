SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomVariationGroup]
as
	select
		*
	from
		[product].[vVariationGroup]
GO
