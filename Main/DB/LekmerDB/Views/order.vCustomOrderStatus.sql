SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCustomOrderStatus]
AS 
SELECT 
	*
FROM 	  
	[order].[vOrderStatus]
GO
