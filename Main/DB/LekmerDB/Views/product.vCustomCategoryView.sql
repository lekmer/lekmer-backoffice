SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [product].[vCustomCategoryView]
as
	select
		*
	from
		[product].[vCategoryView]

GO
