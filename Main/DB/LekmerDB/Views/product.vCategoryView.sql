
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vCategoryView]
AS
	SELECT     
		c.*,
		r.[CategoryRegistry.ProductParentContentNodeId],
		r.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		[product].[vCustomCategory] AS c
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] mch ON mch.[ChannelId] = c.[Category.ChannelId]
		LEFT JOIN [product].[vCustomCategorySiteStructureRegistry] r
			ON mch.[SiteStructureRegistryId] = r.[CategoryRegistry.SiteStructureRegistryId]
			AND c.[Category.Id] = r.[CategoryRegistry.CategoryId]

GO
