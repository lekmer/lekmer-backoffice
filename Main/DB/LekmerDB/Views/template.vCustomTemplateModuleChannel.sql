SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomTemplateModuleChannel]
as
	select
		*
	from
		[template].[vTemplateModuleChannel]
GO
