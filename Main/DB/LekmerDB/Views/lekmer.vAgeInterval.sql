SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vAgeInterval]
AS
SELECT     
      a.AgeIntervalId AS 'AgeInterval.AgeIntervalId', 
      a.FromMonth AS 'AgeInterval.FromMonth', 
      a.ToMonth AS 'AgeInterval.ToMonth', 
      a.Ordinal AS 'AgeInterval.Ordinal', 
      COALESCE (at.Title, a.Title) AS 'AgeInterval.Title',
      t.LanguageId AS 'AgeInterval.LanguageId',
      Assr.ContentNodeId AS 'AgeInterval.ContentNodeId'
FROM
      [lekmer].[tAgeInterval] AS a
      CROSS JOIN core.tChannel AS t
      LEFT JOIN [lekmer].tAgeIntervalTranslation AS at ON at.AgeIntervalId = a.AgeIntervalId AND at.LanguageId = t.LanguageId
      LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = t.ChannelId
	  LEFT JOIN lekmer.tAgeIntervalSiteStructureRegistry Assr ON Assr.AgeIntervalId = a.AgeIntervalId AND Assr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId
GO
