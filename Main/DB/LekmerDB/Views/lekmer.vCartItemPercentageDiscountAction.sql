SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vCartItemPercentageDiscountAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemPercentageDiscountAction.CartActionId',
		cid.[DiscountAmount]	AS 'CartItemPercentageDiscountAction.DiscountAmount',
		cid.[IncludeAllProducts]		AS 'CartItemPercentageDiscountAction.IncludeAllProducts',
		ca.*
	FROM
		[lekmer].[tCartItemPercentageDiscountAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]



GO
