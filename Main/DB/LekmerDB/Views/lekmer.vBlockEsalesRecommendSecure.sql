
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vBlockEsalesRecommendSecure]
AS
	SELECT
		[b].*,
		[ber].[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		[ber].[PanelPath] AS 'BlockEsalesRecommend.PanelPath',
		[ber].[FallbackPanelPath] AS 'BlockEsalesRecommend.FallbackPanelPath',
		[bs].*
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = ber.[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
GO
