SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vVariationTypeSecure]
AS
SELECT 
		 t.[VariationGroupId] 'VariationType.VariationGroupId',
		 t.[VariationTypeId] 'VariationType.Id',
		 t.[Ordinal] 'VariationType.Ordinal',
		 t.[Title] 'VariationType.Title'
FROM 
	  [product].[tVariationType] AS t
GO
