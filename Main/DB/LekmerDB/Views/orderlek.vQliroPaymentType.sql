SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vQliroPaymentType]
AS
SELECT
	[qpt].[QliroPaymentTypeId] AS 'QliroPaymentType.Id',
	[qpt].[ChannelId] AS 'QliroPaymentType.ChannelId',
	[qpt].[Code] AS 'QliroPaymentType.Code',
	[qpt].[Description] AS 'QliroPaymentType.Description',
	[qpt].[RegistrationFee] AS 'QliroPaymentType.RegistrationFee',
	[qpt].[SettlementFee] AS 'QliroPaymentType.SettlementFee',
	[qpt].[InterestRate] AS 'QliroPaymentType.InterestRate',
	[qpt].[InterestType] AS 'QliroPaymentType.InterestType',
	[qpt].[InterestCalculation] AS 'QliroPaymentType.InterestCalculation',
	[qpt].[NoOfMonths] AS 'QliroPaymentType.NoOfMonths',
	[qpt].[MinPurchaseAmount] AS 'QliroPaymentType.MinPurchaseAmount'
FROM
	[orderlek].[tQliroPaymentType] qpt
GO
