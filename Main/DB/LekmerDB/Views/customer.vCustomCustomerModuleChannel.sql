SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomerModuleChannel]
as
	select
		*
	from
		[customer].[vCustomerModuleChannel]
GO
