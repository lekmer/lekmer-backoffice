SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vProductSiteStructureRegistry]
AS
SELECT     
	PR.ProductId 'SiteStructureRegistry.ProductId',
	PR.SiteStructureRegistryId 'SiteStructureRegistry.SiteStructureRegistryId',
	PR.ParentContentNodeId 'SiteStructureRegistry.ParentContentNodeId',
	PR.TemplateContentNodeId 'SiteStructureRegistry.TemplateContentNodeId'
FROM
	product.tProductSiteStructureRegistry PR
GO
