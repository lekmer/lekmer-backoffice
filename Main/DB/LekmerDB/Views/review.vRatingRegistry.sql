SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vRatingRegistry]
AS
	SELECT
		rr.[RatingRegistryId]	'RatingRegistry.RatingRegistryId',
		rr.[CommonName]			'RatingRegistry.CommonName',
		rr.[Title]				'RatingRegistry.Title'
	FROM 
		[review].[tRatingRegistry] rr
GO
