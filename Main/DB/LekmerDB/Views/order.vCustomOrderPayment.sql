
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCustomOrderPayment]
AS
	SELECT
		*
	FROM
		[order].[vOrderPayment]

GO
