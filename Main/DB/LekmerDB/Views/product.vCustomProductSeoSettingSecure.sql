SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductSeoSettingSecure]
as
	select
		*
	from
		[product].[vProductSeoSettingSecure]
GO
