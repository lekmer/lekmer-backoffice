SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [security].[vCustomSystemUser]
as
	select
		*
	from
		[security].[vSystemUser]
GO
