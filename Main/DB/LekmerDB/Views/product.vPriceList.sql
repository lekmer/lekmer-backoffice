SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vPriceList]
AS
SELECT
		PriceListId 'PriceList.Id',
		CurrencyId 'PriceList.CurrencyId',
		ErpId 'PriceList.ErpId',
		Title 'PriceList.Title',
		CommonName 'PriceList.CommonName',
		StartDateTime 'PriceList.StartDateTime',
		EndDateTime 'PriceList.EndDateTime',
		PriceListFolderId 'PriceList.FolderId',
		PriceListStatusId 'PriceList.StatusId',
		PriceListRegistryId 'PriceList.RegistryId'
	FROM
		[product].[tPriceList]
GO
