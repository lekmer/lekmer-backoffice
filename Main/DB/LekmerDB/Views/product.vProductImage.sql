SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vProductImage]
AS
SELECT
	p.ProductId 'ProductImage.ProductId',
	p.MediaId 'ProductImage.MediaId',
	p.ProductImageGroupId 'ProductImage.GroupId',
	p.Ordinal 'ProductImage.Ordinal',
	I.*
FROM
    product.tProductImage AS p
    INNER JOIN media.vCustomImageSecure I ON I.[Media.Id] = p.MediaId

GO
