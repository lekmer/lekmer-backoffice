SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBlockContactUs]
AS
SELECT   
	[BlockId] AS 'BlockContactUs.BlockId',
	[Receiver] AS 'BlockContactUs.Receiver'
FROM [lekmer].[tBlockContactUs]
GO
