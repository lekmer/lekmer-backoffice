SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vBlockLatestFeedbackList]
AS
	SELECT
		[BlockId] AS 'BlockLatestFeedbackList.BlockId',
		[CategoryId] AS 'BlockLatestFeedbackList.CategoryId',
		[NumberOfItems] AS 'BlockLatestFeedbackList.NumberOfItems'
	FROM
		[review].[tBlockLatestFeedbackList]
GO
