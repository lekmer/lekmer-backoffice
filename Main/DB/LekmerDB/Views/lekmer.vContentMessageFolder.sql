SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vContentMessageFolder]
AS
	SELECT
		[cmf].[ContentMessageFolderId]			'ContentMessageFolder.ContentMessageFolderId',
		[cmf].[ParentContentMessageFolderId]	'ContentMessageFolder.ParentContentMessageFolderId',
		[cmf].[Title]							'ContentMessageFolder.Title'
	FROM 
		[lekmer].[tContentMessageFolder] cmf
GO
