SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vOrderAddress]
AS
SELECT [OrderAddressId] AS 'OrderAddress.OrderAddressId'
      ,[Addressee] AS 'OrderAddress.Addressee'
      ,[StreetAddress] AS 'OrderAddress.StreetAddress'
      ,[StreetAddress2] AS 'OrderAddress.StreetAddress2' 
      ,[PostalCode] AS 'OrderAddress.PostalCode'
      ,[City] AS 'OrderAddress.City'
      ,[CountryId] AS 'OrderAddress.CountryId'
      ,[PhoneNumber] AS 'OrderAddress.PhoneNumber'
  FROM [order].[tOrderAddress]
GO
