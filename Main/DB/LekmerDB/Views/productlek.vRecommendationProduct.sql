SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [productlek].[vRecommendationProduct]
AS
	SELECT
		[RecommendationProductId] AS 'RecommendationProduct.RecommendationProductId',
		[RecommenderItemId] AS 'RecommendationProduct.RecommenderItemId',
		[ProductId] AS 'RecommendationProduct.ProductId',
		[Ordinal] AS 'RecommendationProduct.Ordinal'
	FROM
		[productlek].[tRecommendationProduct]

GO
