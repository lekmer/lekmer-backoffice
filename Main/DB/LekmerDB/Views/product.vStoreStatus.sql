SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vStoreStatus]
AS
SELECT 
	t.StoreStatusId 'StoreStatus.Id',
	t.Title 'StoreStatus.Title',
	t.CommonName 'StoreStatus.CommonName'
FROM
	product.tStoreStatus t
GO
