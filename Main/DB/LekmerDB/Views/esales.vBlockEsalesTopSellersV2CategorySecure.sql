SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [esales].[vBlockEsalesTopSellersV2CategorySecure]
AS
	SELECT
		[bc].[BlockId]				AS 'BlockEsalesTopSellersV2Category.BlockId',
		[bc].[IncludeSubcategories] AS 'BlockEsalesTopSellersV2Category.IncludeSubcategories',
		[c].*
	FROM
		[esales].[tBlockEsalesTopSellersV2Category] bc
		INNER JOIN [product].[vCustomCategorySecure] c ON [c].[Category.Id] = [bc].[CategoryId]
GO
