SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomerGroupFolder]
as
	select
		*
	from
		[customer].[vCustomerGroupFolder]
GO
