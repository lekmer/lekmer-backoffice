SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vProductWithoutStatusFilter]
AS
SELECT     
	p.[ProductId] 'Product.Id',
	p.[ItemsInPackage] 'Product.ItemsInPackage',
	p.[ErpId] 'Product.ErpId',
	p.[EanCode] 'Product.EanCode',
	p.[NumberInStock] 'Product.NumberInStock',
    p.[CategoryId] 'Product.CategoryId',
    COALESCE(pt.[WebShopTitle], p.[WebShopTitle]) 'Product.WebShopTitle',
	COALESCE(pt.[Title], p.[Title]) 'Product.Title',
    p.[ProductStatusId] 'Product.ProductStatusId',
    COALESCE(pt.[ShortDescription], p.[ShortDescription]) 'Product.ShortDescription',
    ch.[ChannelId] 'Product.ChannelId',
    ch.[CurrencyId] 'Product.CurrencyId',
    pmc.[PriceListRegistryId] 'Product.PriceListRegistryId',
    p.[MediaId] 'Product.MediaId'
FROM
	[product].[tProduct] AS p
	/* filetrring */
	INNER JOIN [product].[tProductRegistryProduct] AS prp ON p.[ProductId] = prp.[ProductId]
	INNER JOIN [product].[tProductModuleChannel] AS pmc	ON prp.[ProductRegistryId] = pmc.[ProductRegistryId]
	INNER JOIN [core].[tChannel] AS ch ON pmc.[ChannelId] = ch.[ChannelId]
	LEFT JOIN [product].[tProductTranslation] AS pt ON p.[ProductId] = pt.[ProductId] AND ch.[LanguageId] = pt.[LanguageId]
WHERE
	p.[IsDeleted] = 0
GO
