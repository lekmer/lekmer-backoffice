SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [lekmer].[vFreeFreightFlagAction]
as
	select
		a.*
	from
		lekmer.tFreeFreightFlagAction f
		inner join campaign.vCustomProductAction a on f.ProductActionId = a.[ProductAction.Id]

GO
