SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vIconTranslation]
AS
SELECT 
	[IconId] AS 'IconTranslation.IconId',
	[LanguageId] AS 'IconTranslation.LanguageId',
	[Title] AS 'IconTranslation.Title',
	[Description] AS 'IconTranslation.Description'
FROM 
	[lekmer].[tIconTranslation]
GO
