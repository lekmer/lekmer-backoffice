SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductSeoSetting]
as
	select
		*
	from
		[product].[vProductSeoSetting]
GO
