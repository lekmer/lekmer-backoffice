SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [campaignlek].[vCampaignLandingPage]
AS
SELECT 
	[clp].[CampaignLandingPageId]							AS 'CampaignLandingPage.CampaignLandingPageId',
	[clp].[CampaignId]										AS 'CampaignLandingPage.CampaignId',
	COALESCE ([clpt].[WebTitle], [clp].[WebTitle])			AS 'CampaignLandingPage.WebTitle',
	COALESCE ([clpt].[Description], [clp].[Description])	AS 'CampaignLandingPage.Description',
	[l].[LanguageId]										AS 'CampaignLandingPage.LanguageId'
FROM 
	[campaignlek].[tCampaignLandingPage] clp
	CROSS JOIN [core].[tLanguage] AS l
	LEFT JOIN  [campaignlek].[tCampaignLandingPageTranslation] clpt ON [clpt].[CampaignLandingPageId] = [clp].[CampaignLandingPageId]
																		AND [clpt].[LanguageId] = [l].[LanguageId]
GO
