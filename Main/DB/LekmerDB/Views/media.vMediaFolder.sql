SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [media].[vMediaFolder]
AS
SELECT MediaFolderId 'MediaFolder.Id',
	   MediaFolderParentId 'MediaFolder.ParentId',
	   Title 'MediaFolder.Title'
FROM   [media].[tMediaFolder]
GO
