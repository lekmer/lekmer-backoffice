SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomRelationListProduct]
as
	select
		*
	from
		[product].[vRelationListProduct]
GO
