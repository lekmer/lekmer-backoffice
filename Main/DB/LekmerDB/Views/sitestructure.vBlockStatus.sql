SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vBlockStatus]
AS
SELECT 
	 [BlockStatusId] AS 'BlockStatus.BlockStatusId'
	,[Title] AS 'BlockStatus.Title'
	,[CommonName] AS 'BlockStatus.CommonName'
	,[Ordinal] AS 'BlockStatus.Ordinal'
FROM 
	[sitestructure].[tBlockStatus]
GO
