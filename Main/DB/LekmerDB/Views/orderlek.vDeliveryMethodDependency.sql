SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [orderlek].[vDeliveryMethodDependency]
AS
SELECT
	[DeliveryMethodDependencyId] AS 'DeliveryMethodDependency.Id',
	[BaseDeliveryMethodId] AS 'DeliveryMethodDependency.BaseDeliveryMethodId',
	[OptionalDeliveryMethodId] AS 'DeliveryMethodDependency.OptionalDeliveryMethodId'
FROM
	[orderlek].[tDeliveryMethodDependency]

GO
