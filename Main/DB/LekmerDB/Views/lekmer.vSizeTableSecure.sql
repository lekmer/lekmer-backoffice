
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vSizeTableSecure]
AS
	SELECT
		[st].[SizeTableId]			'SizeTable.SizeTableId',
		[st].[SizeTableFolderId]	'SizeTable.SizeTableFolderId',
		[st].[CommonName]			'SizeTable.CommonName',
		[st].[Title]				'SizeTable.Title',
		[st].[Description]			'SizeTable.Description',
		[st].[Column1Title]			'SizeTable.Column1Title',
		[st].[Column2Title]			'SizeTable.Column2Title'
	FROM 
		[lekmer].[tSizeTable] st
GO
