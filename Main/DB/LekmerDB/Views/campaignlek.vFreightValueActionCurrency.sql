SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vFreightValueActionCurrency]
AS
SELECT
	[fvdm].[CartActionId] AS 'FreightValueAction.ActionId',
	[fvdm].[DeliveryMethodId] AS 'FreightValueAction.DeliveryMethodId'
FROM
	[campaignlek].[tFreightValueActionDeliveryMethod] fvdm
GO
