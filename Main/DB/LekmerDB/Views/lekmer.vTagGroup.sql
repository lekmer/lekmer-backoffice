SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create VIEW [lekmer].[vTagGroup]
AS
SELECT     
	TagGroupId AS 'TagGroup.TagGroupId',
	Title AS 'TagGroup.Title',
	CommonName AS 'TagGroup.CommonName'
FROM
	[lekmer].[tTagGroup]
GO
