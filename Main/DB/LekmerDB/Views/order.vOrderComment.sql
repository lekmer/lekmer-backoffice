SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create VIEW [order].[vOrderComment]
AS
SELECT     OC.OrderCommentId AS [OrderComment.Id], OC.OrderId AS [OrderComment.OrderId], 
                      OC.CreationDate AS [OrderComment.CreationDate], OC.Comment AS [OrderComment.Comment],  
                      CSU.[SystemUser.Id], CSU.[SystemUser.Username], CSU.[SystemUser.Password], 
                      CSU.[SystemUser.Name], CSU.[SystemUser.FailedSigninCounter], 
                      CSU.[SystemUser.ExpirationDate], CSU.[SystemUser.ActivationDate], 
                      CSU.[SystemUserStatus.Id], CSU.[SystemUserStatus.Title], 
                      CSU.[SystemUserStatus.CommonName], CSU.[Culture.Id], CSU.[Culture.CommonName], 
                      CSU.[Culture.Title]
FROM         [order].tOrderComment as OC inner JOIN
                      security.vCustomSystemUser as CSU ON OC.AuthorId = CSU.[SystemUser.Id]



GO
