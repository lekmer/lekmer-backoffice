SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomProductCampaign]
as
	select
		*
	from
		[campaign].[vProductCampaign]
GO
