SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeStatus]
as
	select
		*
	from
		[sitestructure].[vContentNodeStatus]
GO
