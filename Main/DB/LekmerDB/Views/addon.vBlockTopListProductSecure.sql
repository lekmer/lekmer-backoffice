SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view [addon].[vBlockTopListProductSecure]
as
	select
		b.BlockId as 'BlockTopListProduct.BlockId',
		b.ProductId as 'BlockTopListProduct.ProductId',
		b.Position as 'BlockTopListProduct.Position',
		p.*
	from
		addon.tBlockTopListProduct b
		inner join product.vCustomProductSecure p on b.ProductId = p.[Product.Id]



GO
