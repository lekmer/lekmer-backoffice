
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vPackageOrderItem]
AS
SELECT
	[poi].[PackageOrderItemId] 'PackageOrderItem.PackageOrderItemId',
	[poi].[OrderItemId] 'PackageOrderItem.OrderItemId',
	[poi].[OrderId] 'PackageOrderItem.OrderId',
	[poi].[Quantity] 'PackageOrderItem.Quantity',
	[poi].[PackagePriceIncludingVat] 'PackageOrderItem.PackagePriceIncludingVat',
	[poi].[ActualPriceIncludingVat] 'PackageOrderItem.ActualPriceIncludingVat',
	[poi].[OriginalPriceIncludingVat] 'PackageOrderItem.OriginalPriceIncludingVat',
	[poi].[VAT] 'PackageOrderItem.VAT',
	[poip].[ProductId] 'PackageOrderItem.ProductId',
	[poip].[ErpId] 'PackageOrderItem.ErpId',
	[poip].[EanCode] 'PackageOrderItem.EanCode',
	[poip].[Title] 'PackageOrderItem.Title',
	[poip].[SizeId] 'PackageOrderItem.SizeId',
	[poip].[SizeErpId] 'PackageOrderItem.SizeErpId',
	s.*
FROM 
	[orderlek].[tPackageOrderItem] poi
	INNER JOIN [orderlek].[tPackageOrderItemProduct] poip ON [poip].[PackageOrderItemId] = [poi].[PackageOrderItemId]
	INNER JOIN [order].[vCustomOrderItemStatus] s ON [poi].[OrderItemStatusId] = [s].[OrderItemStatus.Id]
GO
