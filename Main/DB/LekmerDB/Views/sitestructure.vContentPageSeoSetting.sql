SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentPageSeoSetting]
AS
SELECT     
	cpss.ContentNodeId AS 'ContentPageSeoSetting.ContentNodeId', 
	COALESCE (cpsst.Title, cpss.Title) AS 'ContentPageSeoSetting.Title', 
	COALESCE (cpsst.Description, cpss.Description) AS 'ContentPageSeoSetting.Description', 
	COALESCE (cpsst.Keywords, cpss.Keywords) AS 'ContentPageSeoSetting.Keywords', 
	l.LanguageId AS 'ContentPageSeoSetting.LanguageId'

FROM
	sitestructure.tContentPageSeoSetting AS cpss
	CROSS JOIN core.tLanguage l
	LEFT OUTER JOIN sitestructure.tContentPageSeoSettingTranslation AS cpsst ON cpsst.ContentNodeId = cpss.ContentNodeId AND cpsst.LanguageId = l.LanguageId
GO
