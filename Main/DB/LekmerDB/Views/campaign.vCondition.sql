SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCondition]
as
	select
		ConditionId as 'Condition.Id',
		CampaignId as 'Condition.CampaignId',
		ConditionTypeId as 'Condition.ConditionTypeId',
		t.*
	from
		campaign.tCondition c
		inner join campaign.vCustomConditionType t on c.ConditionTypeId = t.[ConditionType.Id]

GO
