SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomEntity]
as
	select
		*
	from
		[template].[vEntity]
GO
