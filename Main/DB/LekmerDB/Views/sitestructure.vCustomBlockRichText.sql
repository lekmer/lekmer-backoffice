SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlockRichText]
as
	select
		*
	from
		[sitestructure].[vBlockRichText]
GO
