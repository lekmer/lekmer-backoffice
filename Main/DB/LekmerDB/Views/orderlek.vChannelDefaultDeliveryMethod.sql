SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [orderlek].[vChannelDefaultDeliveryMethod]
AS
	SELECT
		[ChannelId] 'ChannelDefaultDeliveryMethod.ChannelId',
		[DeliveryMethodId] 'ChannelDefaultDeliveryMethod.DeliveryMethodId'
	FROM
		[orderlek].[tChannelDefaultDeliveryMethod]

GO
