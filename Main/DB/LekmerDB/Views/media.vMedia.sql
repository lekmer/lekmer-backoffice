SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [media].[vMedia]
AS
	SELECT [MediaId] 'Media.Id',
		   [MediaFormatId] 'Media.FormatId',
		   [MediaFolderId] 'Media.FolderId',
		   [Title] 'Media.Title',
		   [FileName] 'Media.FileName'
	FROM   [media].[tMedia]
GO
