SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vCampaignRegistryCampaignLandingPage]
AS
SELECT 
	[CampaignLandingPageId] AS 'CampaignRegistryCampaignLandingPage.CampaignLandingPageId',
	[CampaignRegistryId]	AS 'CampaignRegistryCampaignLandingPage.CampaignRegistryId',
	[IconMediaId]			AS 'CampaignRegistryCampaignLandingPage.IconMediaId',
	[ImageMediaId]			AS 'CampaignRegistryCampaignLandingPage.ImageMediaId',
	[LinkContentNodeId]		AS 'CampaignRegistryCampaignLandingPage.ContentNodeId'
FROM 
	[campaignlek].[tCampaignRegistryCampaignLandingPage]

GO
