SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomSiteStructureModuleChannel]
as
	select
		*
	from
		[sitestructure].[vSiteStructureModuleChannel]
GO
