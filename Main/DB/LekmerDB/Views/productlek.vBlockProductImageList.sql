SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vBlockProductImageList]
AS
	SELECT 
		[bpil].[BlockId] 'BlockProductImageList.BlockId',
		[bpil].[ProductImageGroupId] 'BlockProductImageList.ProductImageGroupId',
		[bs].*
	FROM 
		[productlek].[tBlockProductImageList] bpil
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bpil].[BlockId]
GO
