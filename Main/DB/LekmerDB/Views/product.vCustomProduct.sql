
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vCustomProduct]
AS 
	SELECT
		[p].*
	FROM
		[product].[vCustomProductInclPackageProducts] p
	WHERE
		[p].[Lekmer.SellOnlyInPackage] = 0
GO
