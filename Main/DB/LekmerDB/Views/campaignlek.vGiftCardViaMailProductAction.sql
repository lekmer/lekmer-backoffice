
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailProductAction]
AS
	SELECT
		gcpa.[ProductActionId] AS 'GiftCardViaMailProductAction.ProductActionId',
		gcpa.[SendingInterval] AS 'GiftCardViaMailProductAction.SendingInterval',
		gcpa.[TemplateId] AS 'GiftCardViaMailProductAction.TemplateId',
		gcpa.[ConfigId] AS 'GiftCardViaMailProductAction.ConfigId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailProductAction] gcpa
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = gcpa.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = gcpa.[ConfigId]
GO
