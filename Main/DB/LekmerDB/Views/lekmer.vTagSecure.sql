
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vTagSecure]
AS
SELECT     
      [t].[TagId]		AS 'Tag.TagId', 
      [t].[TagGroupId]	AS 'Tag.TagGroupId', 
      [t].[Value]		AS 'Tag.Value',
      [t].[CommonName]	AS 'Tag.CommonName',
      [tf].[FlagId]		AS 'Tag.FlagId'
FROM
      [lekmer].[tTag] AS t
      LEFT JOIN [productlek].[tTagFlag] tf ON [tf].[TagId] = [t].[TagId]
GO
