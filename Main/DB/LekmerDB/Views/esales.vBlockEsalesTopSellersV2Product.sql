SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [esales].[vBlockEsalesTopSellersV2Product]
AS
	SELECT
		[bp].[BlockId]	AS 'BlockEsalesTopSellersV2Product.BlockId',
		[bp].[Position] AS 'BlockEsalesTopSellersV2Product.Position',
		[p].*
	FROM
		[esales].[tBlockEsalesTopSellersV2Product] bp
		INNER JOIN [product].[vCustomProduct] p ON [p].[Product.Id] = [bp].[ProductId]
GO
