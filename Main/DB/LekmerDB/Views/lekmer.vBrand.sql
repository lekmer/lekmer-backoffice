
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vBrand]
AS
	SELECT     
		[B].[BrandId] 'Brand.BrandId',
		COALESCE ([bt].[BrandTranslation.Title], [B].[Title]) 'Brand.Title',
		COALESCE ([bt].[BrandTranslation.Description], [B].[Description]) 'Brand.Description',		
		COALESCE ([bt].[BrandTranslation.PackageInfo], [B].[PackageInfo]) 'Brand.PackageInfo',		
		[B].[ExternalUrl] 'Brand.ExternalUrl',
		[B].[MediaId] 'Brand.MediaId',
		[B].[ErpId] 'Brand.ErpId',
		[B].[MonitorThreshold] 'Brand.MonitorThreshold',
		[B].[MaxQuantityPerOrder] 'Brand.MaxQuantityPerOrder',
		[B].[DeliveryTimeId] 'Brand.DeliveryTimeId',
		[B].[IsOffline] 'Brand.IsOffline',
		[Ch].[ChannelId],
		[I].*,
		[dt].*,
		[Bssr].[ContentNodeId] 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [B].[DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [Ch].[ChannelId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId


GO
