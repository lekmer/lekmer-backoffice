SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [esales].[vBlockEsalesTopSellersV2ProductSecure]
AS
	SELECT
		[bp].[BlockId]	AS 'BlockEsalesTopSellersV2Product.BlockId',
		[bp].[Position] AS 'BlockEsalesTopSellersV2Product.Position',
		[p].*
	FROM
		[esales].[tBlockEsalesTopSellersV2Product] bp
		INNER JOIN [product].[vCustomProductSecure] p ON [p].[Product.Id] = [bp].[ProductId]
GO
