SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [esales].[vEsalesRegistry]
AS
	SELECT
		[er].[EsalesRegistryId]	'EsalesRegistry.EsalesRegistryId',
		[er].[CommonName]		'EsalesRegistry.CommonName',
		[er].[Title]			'EsalesRegistry.Title',
		[emc].[ChannelId] AS	'EsalesRegistry.ChannelId',
		[ssmc].[SiteStructureRegistryId] AS 'EsalesRegistry.SiteStructureRegistryId'
	FROM
		[esales].[tEsalesRegistry] er
		LEFT OUTER JOIN [esales].[tEsalesModuleChannel] emc ON [emc].[EsalesRegistryId] = [er].[EsalesRegistryId]
		LEFT OUTER JOIN [sitestructure].[tSiteStructureModuleChannel] ssmc ON [ssmc].[ChannelId] = [emc].[ChannelId]
GO
