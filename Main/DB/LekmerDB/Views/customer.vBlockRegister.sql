SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [customer].[vBlockRegister]
AS
SELECT   
       [BlockId] AS 'BlockRegister.BlockId'
      ,[RedirectContentNodeId] AS 'BlockRegister.RedirectContentNodeId'
  FROM [customer].[tBlockRegister]
GO
