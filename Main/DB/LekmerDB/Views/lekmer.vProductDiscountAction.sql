
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [lekmer].[vProductDiscountAction]
as
	select
		a.*
	from
		lekmer.tProductDiscountAction p
		inner join campaign.vCustomProductAction a on p.ProductActionId = a.[ProductAction.Id]


GO
