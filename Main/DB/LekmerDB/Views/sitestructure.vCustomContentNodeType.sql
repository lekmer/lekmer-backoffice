SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeType]
as
	select
		*
	from
		[sitestructure].[vContentNodeType]
GO
