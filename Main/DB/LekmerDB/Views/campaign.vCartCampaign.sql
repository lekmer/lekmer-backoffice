SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCartCampaign]
as
	select
		c.*
	from
		campaign.vCustomCampaign c
		inner join campaign.tCartCampaign cc on c.[Campaign.Id] = cc.CampaignId

GO
