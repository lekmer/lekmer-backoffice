
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [review].[vReviewRecord]
AS
	SELECT
		r.*,
		rrf.*,
		(CASE WHEN p.[WebShopTitle] IS NULL THEN p.[Title] ELSE p.[WebShopTitle] END) 'ReviewRecord.Product.Title',
		[p].[ErpId] 'ReviewRecord.Product.ErpId',
		ISNULL(c.[UserName], [fc].[Email]) 'ReviewRecord.Customer.UserName'
	FROM 
		[review].[vReview] r
		INNER JOIN [review].[vRatingReviewFeedback] rrf ON rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = r.[Review.RatingReviewFeedbackId]
		INNER JOIN [review].[tRatingReviewUser] rru ON rru.[RatingReviewUserId] = rrf.[RatingReviewFeedback.RatingReviewUserId]
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = rrf.[RatingReviewFeedback.ProductId]
		LEFT JOIN [customer].[tCustomerUser] c ON c.[CustomerId] = rru.[CustomerId]
		LEFT JOIN [customerlek].[tFacebookCustomerUser] fc ON fc.[CustomerId] = rru.[CustomerId]
GO
