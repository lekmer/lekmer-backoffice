SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vBlockSecure]
AS
SELECT   
	   t.[BlockId] AS 'Block.BlockId'
      ,t.[ContentNodeId] AS 'Block.ContentNodeId'
      ,t.[BlockStatusId] AS 'Block.BlockStatusId'
      ,t.[ContentAreaId] AS 'Block.ContentAreaId'
      ,t.[Ordinal] AS 'Block.Ordinal'
      ,t.[AccessId] AS 'Block.AccessId'
      ,t.[Title] AS 'Block.Title'
      ,t.[TemplateId] AS 'Block.TemplateId'
      ,bt.*
FROM         	
    sitestructure.tBlock AS t 
    inner join sitestructure.[vCustomBlockType] as bt on bt.[BlockType.BlockTypeId] = t.[BlockTypeId]

GO
