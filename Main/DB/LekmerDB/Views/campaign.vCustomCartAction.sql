SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCartAction]
as
	select
		*
	from
		[campaign].[vCartAction]
GO
