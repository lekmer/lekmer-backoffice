
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [core].[vCustomChannel]
AS
	SELECT
		vC.*,
		vLC.[Channel.TimeFormat],
		vLC.[Channel.WeekDayFormat],
		vLC.[Channel.DayFormat],
		vLC.[Channel.DateTimeFormat],
		vLC.[Channel.TimeZoneDiff],
		vLC.[Channel.ErpId],
		vLC.[Channel.VatPercentage]
	FROM
		[core].[vChannel] AS vC
		LEFT OUTER JOIN [lekmer].[vLekmerChannel] vLC ON vLC.[Channel.Id] = vC.[Channel.Id]

GO
