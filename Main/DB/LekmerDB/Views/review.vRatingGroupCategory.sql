SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingGroupCategory]
AS
	SELECT
		rgc.[RatingGroupId]			'RatingGroupCategory.RatingGroupId',
		rgc.[CategoryId]			'RatingGroupCategory.CategoryId',
		rgc.[IncludeSubCategories]	'RatingGroupCategory.IncludeSubCategories'
	FROM 
		[review].[tRatingGroupCategory] rgc
GO
