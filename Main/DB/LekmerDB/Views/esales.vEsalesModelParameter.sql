SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [esales].[vEsalesModelParameter]
AS
	SELECT
		[emp].[ModelParameterId] AS 'EsalesModelParameter.ModelParameterId',
		[emp].[ModelId] AS 'EsalesModelParameter.ModelId',
		[emp].[SiteStructureRegistryId] AS 'EsalesModelParameter.SiteStructureRegistryId',
		[emp].[Name] AS 'EsalesModelParameter.Name',
		[emp].[Value] AS 'EsalesModelParameter.Value',
		[emp].[Ordinal] AS 'EsalesModelParameter.Ordinal'
	FROM
		[esales].[tEsalesModelParameter] emp

GO
