
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [lekmer].[vBrandSecure]
AS
SELECT     
	[B].[BrandId] 'Brand.BrandId',
    [B].[Title] 'Brand.Title',
    [B].[Description] 'Brand.Description',
    [B].[PackageInfo] 'Brand.PackageInfo',
    [B].[ExternalUrl] 'Brand.ExternalUrl',
    [B].[MediaId] 'Brand.MediaId',
    [B].[ErpId] 'Brand.ErpId',
	[B].[MonitorThreshold] 'Brand.MonitorThreshold',
	[B].[MaxQuantityPerOrder] 'Brand.MaxQuantityPerOrder',
	[B].[DeliveryTimeId] 'Brand.DeliveryTimeId',
	[B].[IsOffline] 'Brand.IsOffline',
	[I].*,
	[dt].*,
    NULL 'Brand.ContentNodeId'
FROM
	lekmer.tBrand B
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = B.MediaId
	LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [B].[DeliveryTimeId]

GO
