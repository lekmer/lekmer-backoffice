
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vPaymentType]
AS
SELECT
	[pt].[PaymentTypeId]		'PaymentType.PaymentTypeId',
	[pt].[CommonName]			'PaymentType.CommonName',
	[pt].[ErpId]				'PaymentType.ErpId',
	[pt].[Title]				'PaymentType.Title',
	[pt].[Ordinal]				'PaymentType.Ordinal',
	[cpt].[ChannelId]			'PaymentType.ChannelId',
	[cpt].[IsPersonDefault]		'PaymentType.IsPersonDefault',
	[cpt].[IsCompanyDefault]	'PaymentType.IsCompanyDefault'
FROM
	[order].[tPaymentType] pt
	INNER JOIN [order].[tChannelPaymentType] cpt ON [cpt].[PaymentTypeId] = [pt].[PaymentTypeId]

GO
