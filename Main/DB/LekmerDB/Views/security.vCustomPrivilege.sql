SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [security].[vCustomPrivilege]
as
	select
		*
	from
		[security].[vPrivilege]
GO
