SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [esales].[vEsalesModelComponentParameter]
AS
	SELECT
		[emp].[ModelComponentParameterId] AS 'EsalesModelComponentParameter.ModelComponentParameterId',
		[emp].[ModelComponentId] AS 'EsalesModelComponentParameter.ModelComponentId',
		[emp].[SiteStructureRegistryId] AS 'EsalesModelComponentParameter.SiteStructureRegistryId',
		[emp].[Name] AS 'EsalesModelComponentParameter.Name',
		[emp].[Value] AS 'EsalesModelComponentParameter.Value',
		[emp].[Ordinal] AS 'EsalesModelComponentParameter.Ordinal'
	FROM
		[esales].[tEsalesModelComponentParameter] emp
GO
