SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [core].[vLanguage]
AS
select
	[LanguageId] 'Language.Id',
	[Title] 'Language.Title',
	[ISO] 'Language.ISO'
from
	[core].[tLanguage]
GO
