SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vPackageTranslation]
AS
SELECT 
	[PackageId]		'PackageTranslation.PackageId',
	[LanguageId]	'PackageTranslation.LanguageId',
	[GeneralInfo]	'PackageTranslation.GeneralInfo'
FROM 
	[productlek].[tPackageTranslation]
GO
