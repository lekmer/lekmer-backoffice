SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vSiteStructureRegistry]
AS
	select 
		SiteStructureRegistryId 'SiteStructureRegistry.Id',
		Title 'SiteStructureRegistry.Title',
		CommonName 'SiteStructureRegistry.CommonName',
		StartContentNodeId 'SiteStructureRegistry.StartContentNodeId'
	from 
		[sitestructure].[tSiteStructureRegistry]

GO
