SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [core].[vCustomCountry]
as
	select
		*
	from
		[core].[vCountry]
GO
