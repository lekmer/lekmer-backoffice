/*
Run this script on the client database.
You are recommended to back up your database before running this script.
*/

/* ---------------------------------------- SETUP SCHEMA ---------------------------------------- */

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'addon')
EXEC ('CREATE SCHEMA [addon] AUTHORIZATION [dbo]')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsCondition]'
GO
CREATE TABLE [addon].[tCartContainsCondition]
(
[ConditionId] [int] NOT NULL,
[MinQuantity] [int] NOT NULL,
[AllowDuplicates] [bit] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsCondition] on [addon].[tCartContainsCondition]'
GO
ALTER TABLE [addon].[tCartContainsCondition] ADD CONSTRAINT [PK_tCartContainsCondition] PRIMARY KEY CLUSTERED  ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsConditionExcludeCategory]'
GO
CREATE TABLE [addon].[tCartContainsConditionExcludeCategory]
(
[ConditionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsConditionExcludeCategory] on [addon].[tCartContainsConditionExcludeCategory]'
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeCategory] ADD CONSTRAINT [PK_tCartContainsConditionExcludeCategory] PRIMARY KEY CLUSTERED  ([ConditionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsConditionIncludeCategory]'
GO
CREATE TABLE [addon].[tCartContainsConditionIncludeCategory]
(
[ConditionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsConditionIncludeCategory] on [addon].[tCartContainsConditionIncludeCategory]'
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeCategory] ADD CONSTRAINT [PK_tCartContainsConditionIncludeCategory] PRIMARY KEY CLUSTERED  ([ConditionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsConditionExcludeProduct]'
GO
CREATE TABLE [addon].[tCartContainsConditionExcludeProduct]
(
[ConditionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsConditionExcludeProduct] on [addon].[tCartContainsConditionExcludeProduct]'
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeProduct] ADD CONSTRAINT [PK_tCartContainsConditionExcludeProduct] PRIMARY KEY CLUSTERED  ([ConditionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsConditionIncludeProduct]'
GO
CREATE TABLE [addon].[tCartContainsConditionIncludeProduct]
(
[ConditionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsConditionIncludeProduct] on [addon].[tCartContainsConditionIncludeProduct]'
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeProduct] ADD CONSTRAINT [PK_tCartContainsConditionIncludeProduct] PRIMARY KEY CLUSTERED  ([ConditionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionSave]'
GO
CREATE procedure [addon].[pCartContainsConditionSave]
	@ConditionId		int,
	@MinQuantity		int,
	@AllowDuplicates	bit,
	@IncludeAllProducts	bit
AS 
BEGIN 
	UPDATE
		addon.tCartContainsCondition
	SET
		MinQuantity = @MinQuantity,
		AllowDuplicates = @AllowDuplicates,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ConditionId = @ConditionId
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			addon.tCartContainsCondition
		(
			ConditionId,
			MinQuantity,
			AllowDuplicates,
			IncludeAllProducts
		)
		VALUES
		(
			@ConditionId,
			@MinQuantity,
			@AllowDuplicates,
			@IncludeAllProducts
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeCategoryDeleteAll]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryDeleteAll]
	@ConditionId int
as
begin
	DELETE
		[addon].tCartContainsConditionExcludeCategory
	WHERE
		ConditionId = @ConditionId
end



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeCategoryGetIdAll]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryGetIdAll]
	@ConditionId		int
AS
BEGIN
	SELECT 
		c.CategoryId,
		A.IncludeSubcategories
	FROM 
		[addon].tCartContainsConditionExcludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeCategoryInsert]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryInsert]
	@ConditionId			int,
	@CategoryId				int,
	@IncludeSubcategories	bit
AS 
BEGIN 
	INSERT
		addon.tCartContainsConditionExcludeCategory
	(
		ConditionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES
	(
		@ConditionId,
		@CategoryId,
		@IncludeSubcategories
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeProductDeleteAll]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeProductDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].tCartContainsConditionExcludeProduct
	WHERE
		ConditionId = @ConditionId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeProductInsert]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeProductInsert]
	@ConditionId	int,
	@ProductId		int
AS 
BEGIN 
	INSERT
		[addon].tCartContainsConditionExcludeProduct
	(
		ConditionId,
		ProductId
	)
	VALUES
	(
		@ConditionId,
		@ProductId
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeCategoryDeleteAll]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].tCartContainsConditionIncludeCategory
	WHERE
		ConditionId = @ConditionId
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeCategoryGetIdAll]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryGetIdAll]
	@ConditionId		int
AS
BEGIN
	SELECT 
		c.CategoryId,
		A.IncludeSubcategories
	FROM 
		[addon].tCartContainsConditionIncludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeCategoryInsert]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryInsert]
	@ConditionId			int,
	@CategoryId				int,
	@IncludeSubcategories	bit
AS 
BEGIN 
	INSERT
		[addon].tCartContainsConditionIncludeCategory
	(
		ConditionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES
	(
		@ConditionId,
		@CategoryId,
		@IncludeSubcategories
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeProductDeleteAll]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeProductDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].tCartContainsConditionIncludeProduct
	WHERE
		ConditionId = @ConditionId
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeProductInsert]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeProductInsert]
	@ConditionId	int,
	@ProductId		int
AS 
BEGIN 
	INSERT
		[addon].tCartContainsConditionIncludeProduct
	(
		ConditionId,
		ProductId
	)
	VALUES
	(
		@ConditionId,
		@ProductId
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeCategoryGetIdAllRecursive]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryGetIdAllRecursive]
	@ConditionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM addon.tCartContainsConditionIncludeCategory
	WHERE ConditionId = @ConditionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeCategoryGetIdAllRecursive]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryGetIdAllRecursive]
	@ConditionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM addon.tCartContainsConditionExcludeCategory
	WHERE ConditionId = @ConditionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionDelete]'
GO
CREATE procedure [addon].[pCartContainsConditionDelete]
	@ConditionId int
as
begin
	delete addon.tCartContainsConditionIncludeProduct
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsConditionExcludeProduct
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsConditionIncludeCategory
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsConditionExcludeCategory
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsCondition
	where ConditionId = @ConditionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeProductGetIdAll]'
GO
CREATE procedure [addon].[pCartContainsConditionExcludeProductGetIdAll]
	@ConditionId	int
AS
BEGIN
	SELECT 
		P.ProductId
	FROM 
		[addon].tCartContainsConditionExcludeProduct A INNER JOIN 
		product.tProduct P ON A.ProductId = P.ProductId
	WHERE 
		A.ConditionId = @ConditionId AND 
		P.IsDeleted = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeProductGetIdAll]'
GO
CREATE procedure [addon].[pCartContainsConditionIncludeProductGetIdAll]
	@ConditionId	int
AS
BEGIN
	SELECT 
		P.ProductId
	FROM 
		[addon].tCartContainsConditionIncludeProduct A INNER JOIN 
		product.tProduct P ON A.ProductId = P.ProductId
	WHERE 
		A.ConditionId = @ConditionId AND 
		P.IsDeleted = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vConditionType]'
GO
EXEC sp_refreshview N'[campaign].[vConditionType]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCustomConditionType]'
GO
EXEC sp_refreshview N'[campaign].[vCustomConditionType]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCondition]'
GO
EXEC sp_refreshview N'[campaign].[vCondition]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCustomCondition]'
GO
EXEC sp_refreshview N'[campaign].[vCustomCondition]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[vCartContainsCondition]'
GO
CREATE VIEW [addon].[vCartContainsCondition]
AS 
	SELECT 
		ccc.MinQuantity AS 'CartContainsCondition.MinQuantity',
		ccc.AllowDuplicates AS 'CartContainsCondition.AllowDuplicates',
		ccc.IncludeAllProducts AS 'CartContainsCondition.IncludeAllProducts',
		cc.*
	FROM 
		addon.tCartContainsCondition ccc
		INNER JOIN campaign.vCustomCondition cc ON ccc.[ConditionId] = cc.[Condition.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[vCustomCartContainsCondition]'
GO

CREATE VIEW [addon].[vCustomCartContainsCondition]
AS 
	SELECT 
		*
	FROM 
		[addon].[vCartContainsCondition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionGetById]'
GO
CREATE procedure [addon].[pCartContainsConditionGetById]
	@ConditionId int
as
begin
	select
		*
	from
		addon.vCustomCartContainsCondition
	where
		[Condition.Id] = @ConditionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsCondition]'
GO
ALTER TABLE [addon].[tCartContainsCondition] ADD
CONSTRAINT [FK_tCartContainsCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsConditionExcludeCategory]'
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeCategory] ADD
CONSTRAINT [FK_tCartContainsConditionExcludeCategory_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId]),
CONSTRAINT [FK_tCartContainsConditionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsConditionExcludeProduct]'
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeProduct] ADD
CONSTRAINT [FK_tCartContainsConditionExcludeProduct_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId]),
CONSTRAINT [FK_tCartContainsConditionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsConditionIncludeCategory]'
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeCategory] ADD
CONSTRAINT [FK_tCartContainsConditionIncludeCategory_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId]),
CONSTRAINT [FK_tCartContainsConditionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsConditionIncludeProduct]'
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeProduct] ADD
CONSTRAINT [FK_tCartContainsConditionIncludeProduct_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId]),
CONSTRAINT [FK_tCartContainsConditionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


















/* ---------------------------------------- SETUP DATA ---------------------------------------- */

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

-- Drop constraint FK_tCondition_tConditionType from [campaign].[tCondition]
ALTER TABLE [campaign].[tCondition] DROP CONSTRAINT [FK_tCondition_tConditionType]

-- Add rows to [campaign].[tConditionType]
SET IDENTITY_INSERT [campaign].[tConditionType] ON
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (100003, N'Cart contains', N'CartContains')
SET IDENTITY_INSERT [campaign].[tConditionType] OFF
-- Operation applied to 1 rows out of 2

-- Add constraint FK_tCondition_tConditionType to [campaign].[tCondition]
ALTER TABLE [campaign].[tCondition] WITH NOCHECK ADD CONSTRAINT [FK_tCondition_tConditionType] FOREIGN KEY ([ConditionTypeId]) REFERENCES [campaign].[tConditionType] ([ConditionTypeId])
COMMIT TRANSACTION
GO