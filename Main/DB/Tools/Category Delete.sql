--DELETE product.tCategory

SET XACT_ABORT ON
GO

BEGIN TRANSACTION 

	DECLARE @CategoryID INT


	-- Set category ID
	--SET @CategoryID = Put Id Here

	-- Get category ID
	SELECT @CategoryID = c.CategoryId
	FROM product.tCategory c
	WHERE c.Title = '--Put Name Here--'


		
	IF @CategoryID IS NULL
	RAISERROR (N'Category does not exist!', 18, 222);
		
	IF EXISTS (SELECT 1 FROM product.tCategory c WHERE c.ParentCategoryId = @CategoryID )
	RAISERROR (N'Category has childs. Child categories should be deleted first!', 18, 223);

	IF EXISTS (SELECT 1 FROM product.tProduct p WHERE p.CategoryId = @CategoryID )
	RAISERROR (N'Category has products. Products should be deleted first!', 18, 224);



	-- Remove rows from product.tBlockCategoryProductListCategory
	DELETE product.tBlockCategoryProductListCategory
	WHERE CategoryId = @CategoryID


	-- Set NUUL in lekmer.tBlockProductFilter
	UPDATE lekmer.tBlockProductFilter
	SET DefaultCategoryId = NULL
	WHERE DefaultCategoryId = @CategoryID


	-- Delete rows from addon.tBlockTopListCategory
	DELETE addon.tBlockTopListCategory
	WHERE CategoryId = @CategoryID


	-- Delete rows from addon.tCartContainsConditionExcludeCategory
	DELETE addon.tCartContainsConditionExcludeCategory
	WHERE CategoryId = @CategoryID


	-- Delete rows from addon.tCartContainsConditionIncludeCategory
	DELETE addon.tCartContainsConditionIncludeCategory
	WHERE CategoryId = @CategoryID


	-- Delete rows from addon.tCartItemPriceActionExcludeCategory
	DELETE addon.tCartItemPriceActionExcludeCategory
	WHERE CategoryId = @CategoryID


	-- Delete rows from addon.tCartItemPriceActionIncludeCategory
	DELETE addon.tCartItemPriceActionIncludeCategory
	WHERE CategoryId = @CategoryID


	-- Delete rows from product.tCategorySiteStructureRegistry
	DELETE product.tCategorySiteStructureRegistry
	WHERE CategoryId = @CategoryID


	-- Delete rows from product.tCategoryTranslation
	DELETE product.tCategoryTranslation
	WHERE CategoryId = @CategoryID


	-- Delete rows from campaign.tPercentagePriceDiscountActionExcludeCategory
	DELETE campaign.tPercentagePriceDiscountActionExcludeCategory
	WHERE CategoryId = @CategoryID

	-- Delete rows from campaign.tPercentagePriceDiscountActionIncludeCategory
	DELETE campaign.tPercentagePriceDiscountActionIncludeCategory
	WHERE CategoryId = @CategoryID

	-- Set NULL in product.tProduct
	UPDATE product.tProduct
	SET CategoryId = NULL
	WHERE CategoryId = @CategoryID


	-- Delete row from product.tCategory
	DELETE product.tCategory
	WHERE CategoryId = @CategoryID

COMMIT