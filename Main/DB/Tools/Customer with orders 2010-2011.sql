/*Select from CustomerList*/

select
	CH.ChannelId,
	CH.CommonName,
	CivicNumber_NewCustomers_2010 = NewCustomers2010ByCivicNumber.CustomerCivicNumberCount,
	CivicNumber_NewCustomers_2011 = NewCustomers2011ByCivicNumber.CustomerCivicNumberCount,
	CivicNumber_ReturnedCustomers_2010_2011 = ReturnedCustomers2011ByCivicNumber.CustomerCivicNumberCount,
	Email_NewCustomers_2010 = NewCustomers2010ByEmail.CustomerEmailCount,
	Email_NewCustomers_2011 = NewCustomers2011ByEmail.CustomerEmailCount,
	Email_ReturnedCustomers_2010_2011 = ReturnedCustomers2011ByEmail.CustomerEmailCount
from
	core.tChannel CH

--BY Civic 2010
outer apply
	(
		select
			CustomerCivicNumberCount = COUNT(*)
		from 
		(
			select
				CustomerCivicNumber = ci.CivicNumber
			from
				customer.tCustomerInformation CI
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
					where
						O.CustomerId = ci.CustomerId
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel = 1
				and
				OrderChannel = CH.ChannelId
				and
				ci.CivicNumber is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2011-01-01'
			group by
				ci.CivicNumber
		) CustomerCivicNumberCount2010
	) NewCustomers2010ByCivicNumber

--BY Civic 2011
outer apply
	(
		select
			CustomerCivicNumberCount = COUNT(*)
		from 
		(
			select
				CustomerCivicNumber = ci.CivicNumber
			from
				customer.tCustomerInformation CI
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
					where
						O.CustomerId = ci.CustomerId
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel = 1
				and
				OrderChannel = CH.ChannelId
				and
				ci.CivicNumber is not null
				and
				CustomerFirstOrderDate between '2011-01-01' and '2012-01-01'
			group by
				ci.CivicNumber
		) CustomerCivicNumberCount2011
	) NewCustomers2011ByCivicNumber	
	
-- BY Email	2010
outer apply
	(
		select
			CustomerEmailCount = COUNT(*)
		from 
		(
			select
				CustomerEmail = ci.Email
			from
				customer.tCustomerInformation CI
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
					where
						O.CustomerId = ci.CustomerId
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel != 1
				and
				OrderChannel = CH.ChannelId
				and
				ci.Email is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2011-01-01'
			group by
				ci.Email
		) CustomerEmailCount2010
	) NewCustomers2010ByEmail

--BY Email 2011
outer apply
	(
		select
			CustomerEmailCount = COUNT(*)
		from 
		(
			select
				CustomerEmail = ci.Email
			from
				customer.tCustomerInformation CI
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
					where
						O.CustomerId = ci.CustomerId
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel != 1
				and
				OrderChannel = CH.ChannelId
				and
				ci.Email is not null
				and
				CustomerFirstOrderDate between '2011-01-01' and '2012-01-01'
			group by
				ci.Email
		) CustomerEmailCount2011
	) NewCustomers2011ByEmail

--BY Civic Returned 2010-2011
outer apply
	(
		select
			CustomerCivicNumberCount = COUNT(*)
		from 
		(
			select
				CustomerCivicNumber = ci.CivicNumber
			from
				customer.tCustomerInformation CI
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
					where
						O.CustomerId = ci.CustomerId
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel = 1
				and
				OrderChannel = CH.ChannelId
				and
				ci.CivicNumber is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2012-01-01'
				and
				CustomerLaterOrderDate between '2010-01-01' and '2012-01-01'
			group by
				ci.CivicNumber
			having
				MIN(CustomerFirstOrderDate) between '2010-01-01' and '2011-01-01'
				and
				MAX(CustomerLaterOrderDate) between '2011-01-01' and '2012-01-01'
		) CustomerReturnedCivicNumberCount2011
	) ReturnedCustomers2011ByCivicNumber

--BY Email Returned 2010-2011
outer apply
	(
		select
			CustomerEmailCount = COUNT(*)
		from 
		(
			select
				CustomerEmail = ci.Email
			from
				customer.tCustomerInformation CI
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
					where
						O.CustomerId = ci.CustomerId
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel != 1
				and
				OrderChannel = CH.ChannelId
				and
				ci.Email is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2012-01-01'
				and
				CustomerLaterOrderDate between '2010-01-01' and '2012-01-01'
			group by
				ci.Email
			having
				MIN(CustomerFirstOrderDate) between '2010-01-01' and '2011-01-01'
				and
				MAX(CustomerLaterOrderDate) between '2011-01-01' and '2012-01-01'
		) CustomerReturnedEmailCount2011
	) ReturnedCustomers2011ByEmail

order by
	CH.ChannelId