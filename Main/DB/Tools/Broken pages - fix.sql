/*
Fix broken pages with block product filter
-- set pages offline
-- remove blocks
*/

SET XACT_ABORT ON
GO

BEGIN TRANSACTION 

	DECLARE @BlockTypeID INT

	SELECT
		@BlockTypeID = bt.BlockTypeId
	FROM
		sitestructure.tBlockType bt
	WHERE
		bt.CommonName = 'ProductFilter'

	UPDATE
		sitestructure.tContentNode
	SET
		ContentNodeStatusId = 1
	FROM
		sitestructure.tBlock b
		LEFT OUTER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = b.ContentNodeId
		LEFT OUTER JOIN lekmer.tBlockProductFilter bpf ON bpf.BlockId = b.BlockId
	WHERE
		b.BlockTypeId = @BlockTypeID
		AND
		bpf.BlockId IS NULL

	DELETE
		sitestructure.tBlock
	FROM
		sitestructure.tBlock b
		LEFT OUTER JOIN lekmer.tBlockProductFilter bpf ON bpf.BlockId = b.BlockId
	WHERE
		b.BlockTypeId = @BlockTypeID
		AND
		bpf.BlockId IS NULL
		
COMMIT