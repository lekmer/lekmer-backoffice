/*
Script for populating tSize.EUTitle
*/

SET XACT_ABORT ON

BEGIN TRANSACTION

-- Step 1
UPDATE lekmer.tSize
SET EUTitle = CONVERT(NVARCHAR, EU)

UPDATE lekmer.tSize
SET EUTitle = REPLACE(EUTitle, '.00', '')

UPDATE lekmer.tSize
SET EUTitle = REPLACE(EUTitle, ',00', '')

UPDATE lekmer.tSize
SET EUTitle = REPLACE(EUTitle, '.50', '.5')

UPDATE lekmer.tSize
SET EUTitle = REPLACE(EUTitle, ',50', ',5')

-- Step 2
UPDATE s
SET
	s.EUTitle = tAS.AccessoriesSize
FROM
	lekmer.tSize s
	INNER JOIN integration.tAccessoriesSize tAS ON tAS.AccessoriesSizeId = s.SizeId
	
-- Step 3

UPDATE s
SET
	s.EUTitle = REPLACE(tas.AccessoriesSize, '_', '-')
FROM
	integration.tAccessoriesSize tas
	INNER JOIN integration.tSocks ts ON ts.SocksHYId = tas.AccessoriesSizeId
	INNER JOIN lekmer.tSize s ON s.EU = tas.AccessoriesSizeId
	
--
	
COMMIT TRANSACTION