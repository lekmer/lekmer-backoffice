/*
SELECT *
FROM
	[core].tChannel ch
*/

SELECT
	DISTINCT
	RTRIM(LTRIM(o.Email))
FROM
	[order].[tOrder] o
	LEFT OUTER JOIN [core].tChannel ch ON ch.ChannelId = o.ChannelId
WHERE
	ch.ChannelId = 1
	AND
	o.Email IS NOT NULL
	AND
	CHARINDEX(',', o.Email) <= 0
ORDER BY
	RTRIM(LTRIM(o.Email))