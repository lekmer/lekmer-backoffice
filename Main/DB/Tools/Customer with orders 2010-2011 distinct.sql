/*Select from CivicNumberList or EmailList*/

select
	CH.ChannelId,
	CH.CommonName,
	CivicNumber_NewCustomers_2010 = NewCustomers2010ByCivicNumber.CustomerCivicNumberCount,
	CivicNumber_NewCustomers_2011 = NewCustomers2011ByCivicNumber.CustomerCivicNumberCount,
	CivicNumber_ReturnedCustomers_2010_2011 = ReturnedCustomers2011ByCivicNumber.CustomerCivicNumberCount,
	Email_NewCustomers_2010 = NewCustomers2010ByEmail.CustomerEmailCount,
	Email_NewCustomers_2011 = NewCustomers2011ByEmail.CustomerEmailCount,
	Email_ReturnedCustomers_2010_2011 = ReturnedCustomers2011ByEmail.CustomerEmailCount
from
	core.tChannel CH

--BY Civic 2010
outer apply
	(
		select
			CustomerCivicNumberCount = COUNT(*)
		from 
		(
			select
				CustomerCivicNumber = a0_CivicNumber.CivicNumber
			from
				(
					select
						distinct CI.CivicNumber as 'CivicNumber'
					from
						customer.tCustomerInformation CI
				) a0_CivicNumber
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
						inner join customer.tCustomerInformation CI on CI.CustomerId = O.CustomerId
					where
						CI.CivicNumber = a0_CivicNumber.CivicNumber
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel = 1
				and
				OrderChannel = CH.ChannelId
				and
				a0_CivicNumber.CivicNumber is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2011-01-01'
			group by
				a0_CivicNumber.CivicNumber
		) CustomerCivicNumberCount2010
	) NewCustomers2010ByCivicNumber

--BY Civic 2011
outer apply
	(
		select
			CustomerCivicNumberCount = COUNT(*)
		from 
		(
			select
				CustomerCivicNumber = a0_CivicNumber.CivicNumber
			from
				(
					select
						distinct CI.CivicNumber as 'CivicNumber'
					from
						customer.tCustomerInformation CI
				) a0_CivicNumber
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
						inner join customer.tCustomerInformation CI on CI.CustomerId = O.CustomerId
					where
						CI.CivicNumber = a0_CivicNumber.CivicNumber
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel = 1
				and
				OrderChannel = CH.ChannelId
				and
				a0_CivicNumber.CivicNumber is not null
				and
				CustomerFirstOrderDate between '2011-01-01' and '2012-01-01'
			group by
				a0_CivicNumber.CivicNumber
		) CustomerCivicNumberCount2011
	) NewCustomers2011ByCivicNumber	
	
-- BY Email	2010
outer apply
	(
		select
			CustomerEmailCount = COUNT(*)
		from 
		(
			select
				CustomerEmail = a0_Email.Email
			from
				(
					select
						distinct CI.Email as 'Email'
					from
						customer.tCustomerInformation CI
				) a0_Email
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
						inner join customer.tCustomerInformation CI on CI.CustomerId = O.CustomerId
					where
						CI.Email = a0_Email.Email
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel != 1
				and
				OrderChannel = CH.ChannelId
				and
				a0_Email.Email is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2011-01-01'
			group by
				a0_Email.Email
		) CustomerEmailCount2010
	) NewCustomers2010ByEmail

--BY Email 2011
outer apply
	(
		select
			CustomerEmailCount = COUNT(*)
		from 
		(
			select
				CustomerEmail = a0_Email.Email
			from
				(
					select
						distinct CI.Email as 'Email'
					from
						customer.tCustomerInformation CI
				) a0_Email
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
						inner join customer.tCustomerInformation CI on CI.CustomerId = O.CustomerId
					where
						CI.Email = a0_Email.Email
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel != 1
				and
				OrderChannel = CH.ChannelId
				and
				a0_Email.Email is not null
				and
				CustomerFirstOrderDate between '2011-01-01' and '2012-01-01'
			group by
				a0_Email.Email
		) CustomerEmailCount2011
	) NewCustomers2011ByEmail

--BY Civic Returned 2010-2011
outer apply
	(
		select
			CustomerCivicNumberCount = COUNT(*)
		from 
		(
			select
				CustomerCivicNumber = a0_CivicNumber.CivicNumber
			from
				(
					select
						distinct CI.CivicNumber as 'CivicNumber'
					from
						customer.tCustomerInformation CI
				) a0_CivicNumber
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
						inner join customer.tCustomerInformation CI on CI.CustomerId = O.CustomerId
					where
						CI.CivicNumber = a0_CivicNumber.CivicNumber
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel = 1
				and
				OrderChannel = CH.ChannelId
				and
				a0_CivicNumber.CivicNumber is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2012-01-01'
				and
				CustomerLaterOrderDate between '2010-01-01' and '2012-01-01'
			group by
				a0_CivicNumber.CivicNumber
			having
				MIN(CustomerFirstOrderDate) between '2010-01-01' and '2011-01-01'
				and
				MAX(CustomerLaterOrderDate) between '2011-01-01' and '2012-01-01'
		) CustomerReturnedCivicNumberCount2011
	) ReturnedCustomers2011ByCivicNumber

--BY Email Returned 2010-2011
outer apply
	(
		select
			CustomerEmailCount = COUNT(*)
		from 
		(
			select
				CustomerEmail = a0_Email.Email
			from
				(
					select
						distinct CI.Email as 'Email'
					from
						customer.tCustomerInformation CI
				) a0_Email
			cross apply
				(
					select
						OrderChannel = O.ChannelId,
						CustomerFirstOrderDate = MIN(O.CreatedDate),
						CustomerLaterOrderDate = MAX(O.CreatedDate),
						CustomerOrdersAmount = COUNT(O.OrderId)
					from
						[order].tOrder O
						inner join customer.tCustomerInformation CI on CI.CustomerId = O.CustomerId
					where
						CI.Email = a0_Email.Email
					group by
						O.ChannelId
				) a1_O
			where
				OrderChannel != 1
				and
				OrderChannel = CH.ChannelId
				and
				a0_Email.Email is not null
				and
				CustomerFirstOrderDate between '2010-01-01' and '2012-01-01'
				and
				CustomerLaterOrderDate between '2010-01-01' and '2012-01-01'
			group by
				a0_Email.Email
			having
				MIN(CustomerFirstOrderDate) between '2010-01-01' and '2011-01-01'
				and
				MAX(CustomerLaterOrderDate) between '2011-01-01' and '2012-01-01'
		) CustomerReturnedEmailCount2011
	) ReturnedCustomers2011ByEmail

order by
	CH.ChannelId