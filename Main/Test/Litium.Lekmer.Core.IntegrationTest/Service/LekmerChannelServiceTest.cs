﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerChannelServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerChannelService_ResolveBaseService_LekmerServiceResolved()
		{
			var service = IoC.Resolve<IChannelService>();

			Assert.IsInstanceOf<ILekmerChannelService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelService_ResolveLekmerService_LekmerServiceResolved()
		{
			var service = IoC.Resolve<ILekmerChannelService>();

			Assert.IsInstanceOf<ILekmerChannelService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelService_ResolveBaseServiceTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IChannelService>();
			var secureService2 = IoC.Resolve<IChannelService>();

			Assert.AreEqual(secureService1, secureService2);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelService_ResolveLekmerServiceTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<ILekmerChannelService>();
			var secureService2 = IoC.Resolve<ILekmerChannelService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}