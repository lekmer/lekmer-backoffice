﻿using Litium.Lekmer.Core.Repository;
using Litium.Scensum.Core.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerChannelRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerChannelRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ChannelRepository>();

			Assert.IsInstanceOf<LekmerChannelRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ChannelRepository>();
			var repository2 = IoC.Resolve<ChannelRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}