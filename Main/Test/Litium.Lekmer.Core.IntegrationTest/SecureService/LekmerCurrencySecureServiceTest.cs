﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCurrencySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCurrencySecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<ICurrencySecureService>();

			Assert.IsInstanceOf<LekmerCurrencySecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCurrencySecureService_ResolveLekmer_Resolved()
		{
			var secureService = IoC.Resolve<ILekmerCurrencySecureService>();

			Assert.IsInstanceOf<LekmerCurrencySecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCurrencySecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<ICurrencySecureService>();
			var secureService2 = IoC.Resolve<ICurrencySecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}