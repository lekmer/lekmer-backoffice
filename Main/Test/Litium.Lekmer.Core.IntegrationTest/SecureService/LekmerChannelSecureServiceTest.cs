﻿using System.Linq;
using System.Transactions;
using Litium.Lekmer.Core.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerChannelSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerChannelSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IChannelSecureService>();

			Assert.IsInstanceOf<LekmerChannelSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IChannelSecureService>();
			var secureService2 = IoC.Resolve<IChannelSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}

		[Test]
		public void Create_NewChannel_FormatInfoNotNull()
		{
			var secureService = new LekmerChannelSecureService(null, null);

			var channel = secureService.Create() as ILekmerChannel;

			Assert.IsNotNull(channel.FormatInfo);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_Execute_AllChannelsWithFormatInfoReturned()
		{
			var secureService = new LekmerChannelSecureService(null, new LekmerChannelRepository());
			var allChannels = secureService.GetAll();
			Assert.IsNotNull(allChannels, "AllChannels");

			allChannels.All(channel =>
			{
				Assert.IsNotNull((channel as ILekmerChannel).FormatInfo, "Channel.FormatInfo");
				return true;
			});
		}

		[Test]
		[Category("Database.Update")]
		public void Save_ChangeChannelFormatInfoAndSave_FormatInfoUpdated_()
		{
			using (TransactionScope scope = new TransactionScope())
			{
				var mocker = new MockRepository();
				var systemUser = mocker.Stub<SystemUserFull>();
				var accessValidator = mocker.Stub<AccessValidator>();

				var secureService = new LekmerChannelSecureService(accessValidator, new LekmerChannelRepository());

				var lekmerChannel = secureService.GetAll()[0] as ILekmerChannel;

				IChannelFormatInfo formatInfo = lekmerChannel.FormatInfo;
				formatInfo.ChannelId = -1;
				formatInfo.TimeFormat = "TimeFormat";
				formatInfo.WeekDayFormat = "WeekDayFormat";
				formatInfo.DayFormat = "DayFormat";
				formatInfo.DateTimeFormat = "DateTimeFormat";
				formatInfo.TimeZoneDiff = 100;

				secureService.Save(systemUser, lekmerChannel);

				var updatedLekmerChannel = secureService.GetById(lekmerChannel.Id) as ILekmerChannel;
				IChannelFormatInfo updatedFormatInfo = updatedLekmerChannel.FormatInfo;

				Assert.AreEqual(updatedFormatInfo.ChannelId, lekmerChannel.Id);
				Assert.AreEqual(updatedFormatInfo.TimeFormat, "TimeFormat");
				Assert.AreEqual(updatedFormatInfo.WeekDayFormat, "WeekDayFormat");
				Assert.AreEqual(updatedFormatInfo.DayFormat, "DayFormat");
				Assert.AreEqual(updatedFormatInfo.DateTimeFormat, "DateTimeFormat");
				Assert.AreEqual(updatedFormatInfo.TimeZoneDiff, 100);

				scope.Dispose();
			}
		}
	}
}