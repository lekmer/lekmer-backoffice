﻿using Litium.Lekmer.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductActionAppliedItemTest
	{
		[Test]
		[Category("IoC")]
		public void ProductActionAppliedItem_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductActionAppliedItem>();

			Assert.IsInstanceOf<IProductActionAppliedItem>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductActionAppliedItem_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductActionAppliedItem>();
			var instance2 = IoC.Resolve<IProductActionAppliedItem>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}