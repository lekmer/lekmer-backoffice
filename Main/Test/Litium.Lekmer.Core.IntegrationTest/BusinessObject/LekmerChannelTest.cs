﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerChannelTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerChannel_Resolve_Resolved()
		{
			var channel = IoC.Resolve<IChannel>();

			Assert.IsInstanceOf<ILekmerChannel>(channel);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannel_ResolveTwice_DifferentObjects()
		{
			var channel1 = IoC.Resolve<IChannel>();
			var channel2 = IoC.Resolve<IChannel>();

			Assert.AreNotEqual(channel1, channel2);
		}
	}
}