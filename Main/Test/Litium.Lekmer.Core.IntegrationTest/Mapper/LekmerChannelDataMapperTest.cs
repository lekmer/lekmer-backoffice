﻿using System.Data;
using Litium.Lekmer.Core.Mapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerChannelDataMapperTest
	{
		private static MockRepository _mocker;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelDataMapper_Resolve_Resolved()
		{
			var dataReader = _mocker.Stub<IDataReader>();

			var mapper = DataMapperResolver.Resolve<IChannel>(dataReader);

			Assert.IsInstanceOf<LekmerChannelDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerChannelDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataReader = _mocker.Stub<IDataReader>();

			var mapper1 = DataMapperResolver.Resolve<IChannel>(dataReader);
			var mapper2 = DataMapperResolver.Resolve<IChannel>(dataReader);

			Assert.AreNotEqual(mapper1, mapper2);
		}
	}
}