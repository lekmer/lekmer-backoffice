﻿using System.Data;
using Litium.Lekmer.Core.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SubDomainDataMapperTest
	{
		private static MockRepository _mocker;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		[Category("IoC")]
		public void SubDomainDataMapper_Resolve_Resolved()
		{
			var dataReader = _mocker.Stub<IDataReader>();

			var mapper = DataMapperResolver.Resolve<ISubDomain>(dataReader);

			Assert.IsInstanceOf<SubDomainDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void SubDomainDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataReader = _mocker.Stub<IDataReader>();

			var mapper1 = DataMapperResolver.Resolve<ISubDomain>(dataReader);
			var mapper2 = DataMapperResolver.Resolve<ISubDomain>(dataReader);

			Assert.AreNotEqual(mapper1, mapper2);
		}
	}
}