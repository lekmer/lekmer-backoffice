﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Extensions
{
	[TestFixture]
	public class GlobalExtensionsTest
	{
		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "System.Collections.ICollection must be set before calling current method.")]
		public void EnsureNotNull_UseWithNull_ThrowException()
		{
			ICollection collection = null;

			collection.EnsureNotNull();
		}

		[Test]
		public void EnsureNotNull_UseWithNotNull_NoException()
		{
			ICollection collection = new Collection<int>();

			collection.EnsureNotNull();
		}
	}
}
