﻿using Litium.Lekmer.Common.Extensions;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Extensions
{
	[TestFixture]
	public class StringExtensionsTest
	{
		[Test]
		public void IsEmpty_CheckNullString_ReturnTrue()
		{
			string testingString = null;

			bool isEmpty = testingString.IsEmpty();

			Assert.IsTrue(isEmpty);
		}

		[Test]
		public void IsEmpty_CheckEmptyString_ReturnTrue()
		{
			string testingString = "";

			bool isEmpty = testingString.IsEmpty();

			Assert.IsTrue(isEmpty);
		}

		[Test]
		public void IsEmpty_CheckRealString_ReturnFalse()
		{
			string testingString = "Test me :)";

			bool isEmpty = testingString.IsEmpty();

			Assert.IsFalse(isEmpty);
		}


		[Test]
		public void HasValue_CheckNullString_ReturnFalse()
		{
			string testingString = null;

			bool hasValue = testingString.HasValue();

			Assert.IsFalse(hasValue);
		}

		[Test]
		public void HasValue_CheckEmptyString_ReturnFalse()
		{
			string testingString = "";

			bool hasValue = testingString.HasValue();

			Assert.IsFalse(hasValue);
		}

		[Test]
		public void HasValue_CheckRealString_ReturnTrue()
		{
			string testingString = "Test me :)";

			bool hasValue = testingString.HasValue();

			Assert.IsTrue(hasValue);
		}
	}
}
