﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Service
{
	[TestFixture]
	public class SessionStateServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SessionStateService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISessionStateService>();

			Assert.IsInstanceOf<ISessionStateService>(service);
			Assert.IsInstanceOf<SessionStateService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SessionStateService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISessionStateService>();
			var service2 = IoC.Resolve<ISessionStateService>();

			Assert.AreEqual(service1, service2);
		}
	}
}