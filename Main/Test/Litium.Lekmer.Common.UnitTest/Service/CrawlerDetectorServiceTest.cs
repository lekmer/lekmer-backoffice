﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Service
{
	[TestFixture]
	public class CrawlerDetectorServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CrawlerDetectorService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			Assert.IsInstanceOf<ICrawlerDetectorService>(service);
			Assert.IsInstanceOf<CrawlerDetectorService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CrawlerDetectorService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICrawlerDetectorService>();
			var service2 = IoC.Resolve<ICrawlerDetectorService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void Match_ChromeUserAgent_NotMatch()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36";

			Assert.AreEqual(false, service.IsMatch(userAgent));
		}

		[Test]
		public void Match_CDONLogUserAgent_Match()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"Mozilla/5.0+(compatible;+CDONLog/2.1;)";

			Assert.AreEqual(true, service.IsMatch(userAgent));
		}

		[Test]
		public void Match_BingBotUserAgent_Match()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"Mozilla/5.0+(compatible;+bingbot/2.0;++http://www.bing.com/bingbot.htm)";

			Assert.AreEqual(true, service.IsMatch(userAgent));
		}

		[Test]
		public void Match_GoogleBotUserAgent_Match()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"Mozilla/5.0+(compatible;+Googlebot/2.1;++http://www.google.com/bot.html)";

			Assert.AreEqual(true, service.IsMatch(userAgent));
		}

		[Test]
		public void Match_PimonsterUserAgent_Match()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"Mozilla/5.0+(compatible;+Pimonster;+www.pricepi.com)";

			Assert.AreEqual(true, service.IsMatch(userAgent));
		}

		[Test]
		public void Match_WgetUserAgent_Match()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"Wget/1.12+(linux-gnu)";

			Assert.AreEqual(true, service.IsMatch(userAgent));
		}

		[Test]
		public void Match_NagiosUserAgent_Match()
		{
			var service = IoC.Resolve<ICrawlerDetectorService>();

			string userAgent = @"check_http/v1.5.git+(nagios-plugins+1.5)";

			Assert.AreEqual(true, service.IsMatch(userAgent));
		}
	}
}