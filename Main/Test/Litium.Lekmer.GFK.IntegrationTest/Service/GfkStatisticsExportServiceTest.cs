﻿using Litium.Lekmer.GFK.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GFK.IntegrationTest.Service
{
	[TestFixture]
	public class GfkStatisticsExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GfkStatisticsExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IGfkStatisticsExportService>();

			Assert.IsInstanceOf<IGfkStatisticsExportService>(service);
			Assert.IsInstanceOf<GfkStatisticsExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GfkStatisticsExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IGfkStatisticsExportService>();
			var service2 = IoC.Resolve<IGfkStatisticsExportService>();
			Assert.AreEqual(service1, service2);
		}
	}
}