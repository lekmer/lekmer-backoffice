using Litium.Lekmer.GFK.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GFK.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class GfkInfoTest
	{
		[Test]
		[Category("IoC")]
		public void GfkInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IGfkInfo>();
			Assert.IsInstanceOf<IGfkInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void GfkInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IGfkInfo>();
			var info2 = IoC.Resolve<IGfkInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}