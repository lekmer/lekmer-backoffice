﻿using Litium.Lekmer.GFK.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GFK.IntegrationTest.Exporter
{
	[TestFixture]
	public class GfkStatisticsExporterTest
	{
		[Test]
		[Category("IoC")]
		public void GfkStatisticsExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("GfkStatisticsExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<GfkStatisticsExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void GfkStatisticsExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("GfkStatisticsExporter");
			var exporter2 = IoC.Resolve<IExporter>("GfkStatisticsExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}