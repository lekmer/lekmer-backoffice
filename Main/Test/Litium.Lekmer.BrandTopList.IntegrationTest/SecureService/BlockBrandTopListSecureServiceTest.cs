﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockBrandTopListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBrandTopListSecureService>();

			Assert.IsInstanceOf<IBlockBrandTopListSecureService>(service);
			Assert.IsInstanceOf<BlockBrandTopListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListSecureService>();
			var instance2 = IoC.Resolve<IBlockBrandTopListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveProductSimilar_ProductSimilarSaved()
		//{
		//    using (TransactionScope transactionScope = new TransactionScope())
		//    {
		//        ProductHelper productHelper = new ProductHelper();

		//        // Get product collection.
		//        ProductCollection productCollection = productHelper.GetProducts();

		//        int expectedProductId = productCollection[0].Id;

		//        // Save similar products.
		//        IProductSimilar savedProductSimilar = productHelper.SaveSimilarProduct(productCollection);

		//        Assert.AreEqual(expectedProductId, savedProductSimilar.ProductId);

		//        for (int i = 1; i < productCollection.Count; i++)
		//        {
		//            Assert.AreEqual(savedProductSimilar.SimilarProductCollection[i - 1].ProductErpId, productCollection[i].ErpId);
		//            Assert.AreEqual(savedProductSimilar.SimilarProductCollection[i - 1].Score, i);
		//        }

		//        transactionScope.Dispose();
		//    }
		//}
	}
}