﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockBrandTopListCategorySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategorySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBrandTopListCategorySecureService>();

			Assert.IsInstanceOf<IBlockBrandTopListCategorySecureService>(service);
			Assert.IsInstanceOf<BlockBrandTopListCategorySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategorySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListCategorySecureService>();
			var instance2 = IoC.Resolve<IBlockBrandTopListCategorySecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}