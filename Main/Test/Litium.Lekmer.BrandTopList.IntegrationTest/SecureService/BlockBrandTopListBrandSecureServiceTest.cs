﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockBrandTopListBrandSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrandSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBrandTopListBrandSecureService>();

			Assert.IsInstanceOf<IBlockBrandTopListBrandSecureService>(service);
			Assert.IsInstanceOf<BlockBrandTopListBrandSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrandSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListBrandSecureService>();
			var instance2 = IoC.Resolve<IBlockBrandTopListBrandSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}