﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Repository
{
	[TestFixture]
	public class TopListBrandRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void TopListBrandRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<TopListBrandRepository>();

			Assert.IsInstanceOf<TopListBrandRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void TopListBrandRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<TopListBrandRepository>();
			var instance2 = IoC.Resolve<TopListBrandRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}