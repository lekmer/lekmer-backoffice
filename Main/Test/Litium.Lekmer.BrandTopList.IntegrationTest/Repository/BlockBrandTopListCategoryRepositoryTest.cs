﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockBrandTopListCategoryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategoryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockBrandTopListCategoryRepository>();

			Assert.IsInstanceOf<BlockBrandTopListCategoryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategoryRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockBrandTopListCategoryRepository>();
			var instance2 = IoC.Resolve<BlockBrandTopListCategoryRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}