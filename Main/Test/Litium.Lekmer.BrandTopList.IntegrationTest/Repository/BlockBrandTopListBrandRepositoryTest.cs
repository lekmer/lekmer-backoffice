﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockBrandTopListBrandRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrandRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockBrandTopListBrandRepository>();

			Assert.IsInstanceOf<BlockBrandTopListBrandRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrandRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockBrandTopListBrandRepository>();
			var instance2 = IoC.Resolve<BlockBrandTopListBrandRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}