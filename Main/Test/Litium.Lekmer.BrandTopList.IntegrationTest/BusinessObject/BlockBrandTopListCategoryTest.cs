﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockBrandTopListCategoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategory_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockBrandTopListCategory>();

			Assert.IsInstanceOf<IBlockBrandTopListCategory>(instance);
			Assert.IsInstanceOf<BlockBrandTopListCategory>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategory_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListCategory>();
			var instance2 = IoC.Resolve<IBlockBrandTopListCategory>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}