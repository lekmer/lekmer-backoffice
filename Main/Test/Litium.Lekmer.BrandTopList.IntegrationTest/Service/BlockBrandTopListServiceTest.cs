﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Service
{
	[TestFixture]
	public class BlockBrandTopListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBrandTopListService>();

			Assert.IsInstanceOf<IBlockBrandTopListService>(service);
			Assert.IsInstanceOf<BlockBrandTopListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListService>();
			var instance2 = IoC.Resolve<IBlockBrandTopListService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}