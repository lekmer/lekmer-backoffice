﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Service
{
	[TestFixture]
	public class BlockBrandTopListCategoryServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategoryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBrandTopListCategoryService>();

			Assert.IsInstanceOf<IBlockBrandTopListCategoryService>(service);
			Assert.IsInstanceOf<BlockBrandTopListCategoryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategoryService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListCategoryService>();
			var instance2 = IoC.Resolve<IBlockBrandTopListCategoryService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}