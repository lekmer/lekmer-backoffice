using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonExportRestrictionItemTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionItem_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonExportRestrictionItem>();
			Assert.IsInstanceOf<ICdonExportRestrictionItem>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionItem_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonExportRestrictionItem>();
			var info2 = IoC.Resolve<ICdonExportRestrictionItem>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}