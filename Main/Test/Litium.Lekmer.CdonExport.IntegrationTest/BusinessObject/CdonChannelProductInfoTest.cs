using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonChannelProductInfoTest
	{
		[Test]
		[Category("IoC")]
		public void CdonChannelProductInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonChannelProductInfo>();
			Assert.IsInstanceOf<ICdonChannelProductInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonChannelProductInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonChannelProductInfo>();
			var info2 = IoC.Resolve<ICdonChannelProductInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}