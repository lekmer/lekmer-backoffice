using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonProductInfoTest
	{
		[Test]
		[Category("IoC")]
		public void CdonProductInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonProductInfo>();
			Assert.IsInstanceOf<ICdonProductInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonProductInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonProductInfo>();
			var info2 = IoC.Resolve<ICdonProductInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}