using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonIconTest
	{
		[Test]
		[Category("IoC")]
		public void CdonIcon_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonIcon>();
			Assert.IsInstanceOf<ICdonIcon>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonIcon_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonIcon>();
			var info2 = IoC.Resolve<ICdonIcon>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}