using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonTagTest
	{
		[Test]
		[Category("IoC")]
		public void CdonTag_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonTag>();
			Assert.IsInstanceOf<ICdonTag>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonTag_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonTag>();
			var info2 = IoC.Resolve<ICdonTag>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}