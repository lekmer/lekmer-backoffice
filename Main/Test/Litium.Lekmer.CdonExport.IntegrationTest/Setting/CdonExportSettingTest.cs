﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Setting
{
	[TestFixture]
	public class CdonExportSettingTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<ICdonExportSetting>();
			Assert.IsInstanceOf<ICdonExportSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<ICdonExportSetting>();
			var setting2 = IoC.Resolve<ICdonExportSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}