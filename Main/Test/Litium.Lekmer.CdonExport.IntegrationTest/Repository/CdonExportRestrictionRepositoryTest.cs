﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Repository
{
	[TestFixture]
	public class CdonExportRestrictionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CdonExportRestrictionRepository>();
			Assert.IsInstanceOf<CdonExportRestrictionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CdonExportRestrictionRepository>();
			var repository2 = IoC.Resolve<CdonExportRestrictionRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}