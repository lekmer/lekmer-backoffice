﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Repository
{
	[TestFixture]
	public class CdonExportRestrictionBrandRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CdonExportRestrictionBrandRepository>();
			Assert.IsInstanceOf<CdonExportRestrictionBrandRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CdonExportRestrictionBrandRepository>();
			var repository2 = IoC.Resolve<CdonExportRestrictionBrandRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}