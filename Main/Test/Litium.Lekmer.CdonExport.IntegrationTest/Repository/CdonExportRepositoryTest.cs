﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Exporter
{
	[TestFixture]
	public class CdonExportRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportService_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CdonExportRepository>();
			Assert.IsInstanceOf<CdonExportRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportService_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CdonExportRepository>();
			var repository2 = IoC.Resolve<CdonExportRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}