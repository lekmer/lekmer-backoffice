﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Service
{
	[TestFixture]
	public class CdonExportRestrictionProductServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProductService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionProductService>();

			Assert.IsInstanceOf<ICdonExportRestrictionProductService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionProductService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProductService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionProductService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionProductService>();
			Assert.AreEqual(service1, service2);
		}
	}
}