﻿using System.Collections.Generic;
using Litium.Lekmer.Core.Web;
using NUnit.Framework;

namespace Litium.Lekmer.Core.UnitTest.Paging
{
	[TestFixture]
	public class PagingCalculatorTest
	{
		[Test]
		public void CalculateVisiblePageLinks_FirstSelectedOneDot_CorrectResult()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			// [1] 2 3 4 ... 30 31 32
			SortedDictionary<int, int> visiblePageLinks = pagingCalculator.CalculateVisiblePageLinks(32, 1, 3, 2);

			for (int i = 1; i < 4 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}

			for (int i = 5; i < 30; i++)
			{
				Assert.IsFalse(visiblePageLinks.ContainsKey(i));
			}

			for (int i = 30; i < 32 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}
		}

		[Test]
		public void CalculateVisiblePageLinks_LeftSelectedOneDot_CorrectResult()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			// 1 2 3 [4] 5 6 7 ... 31 32
			SortedDictionary<int, int> visiblePageLinks = pagingCalculator.CalculateVisiblePageLinks(32, 4, 3, 2);

			for (int i = 1; i < 7 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}

			for (int i = 8; i < 31; i++)
			{
				Assert.IsFalse(visiblePageLinks.ContainsKey(i));
			}

			for (int i = 31; i < 32 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}
		}

		[Test]
		public void CalculateVisiblePageLinks_MiddleSelectedTwoDot_CorrectResult()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			// 1 2 ... 4 5 6 [7] 8 9 10 ... 31 32
			SortedDictionary<int, int> visiblePageLinks = pagingCalculator.CalculateVisiblePageLinks(32, 7, 3, 2);

			for (int i = 1; i < 2 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}

			for (int i = 3; i < 3 + 1; i++)
			{
				Assert.IsFalse(visiblePageLinks.ContainsKey(i));
			}

			for (int i = 4; i < 10 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}

			for (int i = 11; i < 30 + 1; i++)
			{
				Assert.IsFalse(visiblePageLinks.ContainsKey(i));
			}

			for (int i = 31; i < 32 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}
		}

		[Test]
		public void CalculateVisiblePageLinks_LastSelectedOneDot_CorrectResult()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			// 1 2 3 ... 29 30 31 [32]
			SortedDictionary<int, int> visiblePageLinks = pagingCalculator.CalculateVisiblePageLinks(32, 32, 3, 2);

			for (int i = 1; i < 3 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}

			for (int i = 4; i < 29; i++)
			{
				Assert.IsFalse(visiblePageLinks.ContainsKey(i));
			}

			for (int i = 29; i < 32 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}
		}

		[Test]
		public void CalculateVisiblePageLinks_FirstSelectedOneDotPageCountTwo_CorrectResult()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			// [1] 2
			SortedDictionary<int, int> visiblePageLinks = pagingCalculator.CalculateVisiblePageLinks(2, 1, 3, 2);

			foreach (KeyValuePair<int, int> visiblePage in visiblePageLinks)
			{
				Assert.IsTrue(visiblePage.Key >= 1);
				Assert.IsTrue(visiblePage.Key <= 2);
			}

			for (int i = 1; i < 2 + 1; i++)
			{
				Assert.AreEqual(i, visiblePageLinks[i]);
			}
		}

		[Test]
		public void CalculatePageCount_ExactMatch_CorrectPageCount()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			int actualPageCount = pagingCalculator.CalculatePageCount(13 * 18, 13);

			Assert.AreEqual(18, actualPageCount);
		}

		[Test]
		public void CalculatePageCount_LowBound_CorrectPageCount()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			int actualPageCount = pagingCalculator.CalculatePageCount(13 * 18 + 1, 13);

			Assert.AreEqual(19, actualPageCount);
		}

		[Test]
		public void CalculatePageCount_HighBound_CorrectPageCount()
		{
			IPagingCalculator pagingCalculator = new PagingCalculator();

			int actualPageCount = pagingCalculator.CalculatePageCount(13 * 18 + 12, 13);

			Assert.AreEqual(19, actualPageCount);
		}
	}
}