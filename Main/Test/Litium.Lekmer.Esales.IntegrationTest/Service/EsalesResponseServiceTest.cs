﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesResponseServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesResponseService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesResponseService>();

			Assert.IsInstanceOf<IEsalesResponseService>(service);
			Assert.IsInstanceOf<EsalesResponseService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesResponseService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesResponseService>();
			var service2 = IoC.Resolve<IEsalesResponseService>();

			Assert.AreEqual(service1, service2);
		}
	}
}