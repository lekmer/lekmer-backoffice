﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesProductKeyServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductKeyService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesProductKeyService>();

			Assert.IsInstanceOf<IEsalesProductKeyService>(service);
			Assert.IsInstanceOf<EsalesProductKeyService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductKeyService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesProductKeyService>();
			var service2 = IoC.Resolve<IEsalesProductKeyService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void ComposeProductKey_ProductIdAsInput_CorrecrKeyAsResult()
		{
			var service = IoC.Resolve<IEsalesProductKeyService>();

			string key = service.ComposeProductKey(123456);

			Assert.AreEqual("123456", key);
		}

		[Test]
		public void ComposeTagKey_TagtIdAsInput_CorrecrKeyAsResult()
		{
			var service = IoC.Resolve<IEsalesProductKeyService>();

			string key = service.ComposeTagKey(123456);

			Assert.AreEqual("t-123456", key);
		}

		[Test]
		public void ParseProductKey_KeyAsInput_CorrecrIdAsResult()
		{
			var service = IoC.Resolve<IEsalesProductKeyService>();

			int id = service.ParseProductKey("123456");

			Assert.AreEqual(123456, id);
		}

		[Test]
		public void ParseTagKey_KeyAsInput_CorrecrIdAsResult()
		{
			var service = IoC.Resolve<IEsalesProductKeyService>();

			int id = service.ParseTagKey("t-123456");

			Assert.AreEqual(123456, id);
		}
	}
}