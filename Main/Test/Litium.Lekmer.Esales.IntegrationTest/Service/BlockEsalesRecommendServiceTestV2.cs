﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class BlockEsalesRecommendServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesRecommendServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesRecommendServiceV2>(service);
			Assert.IsInstanceOf<BlockEsalesRecommendServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendServiceV2_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockEsalesRecommendServiceV2>();
			var instance2 = IoC.Resolve<IBlockEsalesRecommendServiceV2>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}