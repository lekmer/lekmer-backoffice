﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesService>();

			Assert.IsInstanceOf<IEsalesService>(service);
			Assert.IsInstanceOf<EsalesService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesService>();
			var service2 = IoC.Resolve<IEsalesService>();

			Assert.AreEqual(service1, service2);
		}
	}
}