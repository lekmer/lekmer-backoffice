﻿using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.Esales.IntegrationTest.Helper;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class AdFolderSecureServiceTest
	{
		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private IAdFolderSecureService _adFolderSecureService;
		private IAdSecureService _adSecureService;
		private AdFolderTestHelper _adFolderTestHelper;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();

			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();

			_adSecureService = _mocker.Stub<IAdSecureService>();
			_adFolderSecureService = new AdFolderSecureService(new AdFolderRepository(), _accessValidator, _adSecureService);
			_adFolderTestHelper = new AdFolderTestHelper(_adFolderSecureService, _systemUserFull);
		}

		[Test]
		[Category("IoC")]
		public void AdFolderSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IAdFolderSecureService>();

			Assert.IsInstanceOf<IAdFolderSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void AdFolderSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IAdFolderSecureService>();
			var instance2 = IoC.Resolve<IAdFolderSecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveNewAdFolder_FolderSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				var adFolder = _adFolderTestHelper.Create("Test Ad folder");

				// Save
				var actualFolder = _adFolderSecureService.Save(_systemUserFull, adFolder);

				Assert.IsTrue(actualFolder.Id > 0);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveAdFolderWithSameTitleOnRootLevel_FolderNotSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder1 = _adFolderTestHelper.Create("Test Ad folder same title");
				var adFolder2 = _adFolderTestHelper.Create("Test Ad folder same title");

				// Save
				var actualFolder1 = _adFolderSecureService.Save(_systemUserFull, adFolder1);
				var actualFolder2 = _adFolderSecureService.Save(_systemUserFull, adFolder2);

				// Check if it works
				Assert.Greater(actualFolder1.Id, 0, "AdFolder.Id");
				Assert.AreEqual(-1, actualFolder2.Id, "AdFolder.Id with same title");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveAdFolderWithSameTitleOnSubLevel_FolderNotSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var rootFolder = _adFolderTestHelper.CreateAndSave("Test root Ad folder");

				var adFolder1 = _adFolderTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);
				var adFolder2 = _adFolderTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);

				// Check if it works
				Assert.Greater(rootFolder.Id, 0, "AdFolder.Id");
				Assert.Greater(adFolder1.Id, 0, "AdFolder.Id");
				Assert.AreEqual(-1, adFolder2.Id, "AdFolder.Id with same title");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_UpdateAdFolder_FolderUpdated()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var rootFolder1 = _adFolderTestHelper.CreateAndSave("Test root Ad folder 1");
				var rootFolder2 = _adFolderTestHelper.CreateAndSave("Test root Ad folder 2");

				// Create folder
				var adFolder = _adFolderTestHelper.CreateAndSave("Test Ad folder", rootFolder1);

				// Update
				adFolder.ParentFolderId = rootFolder2.Id;
				adFolder.Title = "Test Ad folder updated";
				
				// Update in db
				var actualFolder = _adFolderSecureService.Save(_systemUserFull, adFolder);

				// Check if it works
				Assert.AreEqual(adFolder.Id, actualFolder.Id, "AdFolder.Id");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetById_GetFolderById_CorrectFolderReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder = _adFolderTestHelper.CreateAndSave("Test Ad folder 1");

				// Get all
				var actualAdFolder = _adFolderSecureService.GetById(adFolder.Id);

				_adFolderTestHelper.AssertAreEquel(adFolder, actualAdFolder);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllFolders_AllFoldersReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder1 = _adFolderTestHelper.CreateAndSave("Test Ad folder 1");
				var adFolder2 = _adFolderTestHelper.CreateAndSave("Test Ad folder 2");
				var adFolder3 = _adFolderTestHelper.CreateAndSave("Test Ad folder 3");

				// Get all
				var adFolders = _adFolderSecureService.GetAll();
				
				_adFolderTestHelper.AssertContains(adFolder1, adFolders);
				_adFolderTestHelper.AssertContains(adFolder2, adFolders);
				_adFolderTestHelper.AssertContains(adFolder3, adFolders);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllByParent_GetAllFoldersBySomeParent_AllSubFoldersReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder1 = _adFolderTestHelper.CreateAndSave("Test Ad folder 1");
				var adFolder2 = _adFolderTestHelper.CreateAndSave("Test Ad folder 2", adFolder1);
				var adFolder3 = _adFolderTestHelper.CreateAndSave("Test Ad folder 3", adFolder1);

				// Get by parent
				var adFolders = _adFolderSecureService.GetAllByParent(adFolder1.Id);

				_adFolderTestHelper.AssertDoesNotContain(adFolder1, adFolders);
				_adFolderTestHelper.AssertContains(adFolder2, adFolders);
				_adFolderTestHelper.AssertContains(adFolder3, adFolders);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetTree_SelectedIdNull_CorrectNodesReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder1 = _adFolderTestHelper.CreateAndSave("Test Ad folder 1");
				var adFolder2 = _adFolderTestHelper.CreateAndSave("Test Ad folder 2", adFolder1);
				var adFolder3 = _adFolderTestHelper.CreateAndSave("Test Ad folder 3", adFolder2);
				var adFolder4 = _adFolderTestHelper.CreateAndSave("Test Ad folder 4");

				// Get by parent
				Collection<INode> adFolderNodes = _adFolderSecureService.GetTree(null);

				_adFolderTestHelper.AssertContains(adFolder1, adFolderNodes);
				_adFolderTestHelper.AssertDoesNotContain(adFolder2, adFolderNodes);
				_adFolderTestHelper.AssertDoesNotContain(adFolder3, adFolderNodes);
				_adFolderTestHelper.AssertContains(adFolder4, adFolderNodes);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetTree_SecondLevelSelected_CorrectNodesReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder1 = _adFolderTestHelper.CreateAndSave("Test Ad folder 1");
				var adFolder2 = _adFolderTestHelper.CreateAndSave("Test Ad folder 2", adFolder1);
				var adFolder3 = _adFolderTestHelper.CreateAndSave("Test Ad folder 3", adFolder2);
				var adFolder4 = _adFolderTestHelper.CreateAndSave("Test Ad folder 4", adFolder3);
				var adFolder5 = _adFolderTestHelper.CreateAndSave("Test Ad folder 5");

				// Get by parent
				Collection<INode> adFolderNodes = _adFolderSecureService.GetTree(adFolder2.Id);

				_adFolderTestHelper.AssertContains(adFolder1, adFolderNodes);
				_adFolderTestHelper.AssertContains(adFolder2, adFolderNodes);
				_adFolderTestHelper.AssertContains(adFolder3, adFolderNodes);
				// Node 4 is not included in output
				_adFolderTestHelper.AssertDoesNotContain(adFolder4, adFolderNodes);
				_adFolderTestHelper.AssertContains(adFolder5, adFolderNodes);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void TryDelete_DeleteSingleFolder_FolderRemoved()
		{
			 Expect.Call(_adSecureService.GetAllByFolder(0)).Return(new Collection<IAd>() ).IgnoreArguments();

			_mocker.ReplayAll();

			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder = _adFolderTestHelper.CreateAndSave("Test Ad folder");

				// Get by parent
				bool isDeleted = _adFolderSecureService.TryDelete(_systemUserFull, adFolder.Id);
				var actualAdFolder = _adFolderSecureService.GetById(adFolder.Id);

				Assert.IsTrue(isDeleted, "AdFolder deleted");
				Assert.IsNull(actualAdFolder, "AdFolder deleted");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void TryDelete_DeleteFolderWithSubFoldersAndAds_FolderNotRemoved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var adFolder1 = _adFolderTestHelper.CreateAndSave("Test Ad folder");
				var adFolder2 = _adFolderTestHelper.CreateAndSave("Test Ad folder", adFolder1);

				Expect
					.Call(_adSecureService.GetAllByFolder(adFolder1.Id))
					.Return(new Collection<IAd>());

				Expect
					.Call(_adSecureService.GetAllByFolder(adFolder2.Id))
					.Return(new Collection<IAd> { new Ad() });

				_mocker.ReplayAll();
				
				// Get by parent
				bool isDeleted = _adFolderSecureService.TryDelete(_systemUserFull, adFolder1.Id);

				Assert.IsFalse(isDeleted, "AdFolder deleted");

				transactionScope.Dispose();
			}
		}
	}
}