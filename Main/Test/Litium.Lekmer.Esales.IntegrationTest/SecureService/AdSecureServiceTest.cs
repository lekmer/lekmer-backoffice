﻿using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.Esales.IntegrationTest.Helper;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class AdSecureServiceTest
	{
		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private IAdSecureService _AdSecureService;
		private IAdChannelSecureService _adChannelSecureService;
		//private AdTestHelper _AdTestHelper;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();

			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();

			_adChannelSecureService = _mocker.Stub<IAdChannelSecureService>();
			_AdSecureService = new AdSecureService(new AdRepository(), _accessValidator, _adChannelSecureService);
			//_AdTestHelper = new AdTestHelper(_AdSecureService, _systemUserFull);
		}

		[Test]
		[Category("IoC")]
		public void AdSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IAdSecureService>();

			Assert.IsInstanceOf<IAdSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void AdSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IAdSecureService>();
			var instance2 = IoC.Resolve<IAdSecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveNewAd_FolderSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		var Ad = _AdTestHelper.Create("Test Ad folder");

		//		// Save
		//		var actualFolder = _AdSecureService.Save(_systemUserFull, Ad);

		//		Assert.IsTrue(actualFolder.Id > 0);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveAdWithSameTitleOnRootLevel_FolderNotSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad1 = _AdTestHelper.Create("Test Ad folder same title");
		//		var Ad2 = _AdTestHelper.Create("Test Ad folder same title");

		//		// Save
		//		var actualFolder1 = _AdSecureService.Save(_systemUserFull, Ad1);
		//		var actualFolder2 = _AdSecureService.Save(_systemUserFull, Ad2);

		//		// Check if it works
		//		Assert.Greater(actualFolder1.Id, 0, "Ad.Id");
		//		Assert.AreEqual(-1, actualFolder2.Id, "Ad.Id with same title");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveAdWithSameTitleOnSubLevel_FolderNotSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var rootFolder = _AdTestHelper.CreateAndSave("Test root Ad folder");

		//		var Ad1 = _AdTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);
		//		var Ad2 = _AdTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);

		//		// Check if it works
		//		Assert.Greater(rootFolder.Id, 0, "Ad.Id");
		//		Assert.Greater(Ad1.Id, 0, "Ad.Id");
		//		Assert.AreEqual(-1, Ad2.Id, "Ad.Id with same title");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_UpdateAd_FolderUpdated()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var rootFolder1 = _AdTestHelper.CreateAndSave("Test root Ad folder 1");
		//		var rootFolder2 = _AdTestHelper.CreateAndSave("Test root Ad folder 2");

		//		// Create folder
		//		var Ad = _AdTestHelper.CreateAndSave("Test Ad folder", rootFolder1);

		//		// Update
		//		Ad.ParentFolderId = rootFolder2.Id;
		//		Ad.Title = "Test Ad folder updated";
				
		//		// Update in db
		//		var actualFolder = _AdSecureService.Save(_systemUserFull, Ad);

		//		// Check if it works
		//		Assert.AreEqual(Ad.Id, actualFolder.Id, "Ad.Id");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetById_GetFolderById_CorrectFolderReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad = _AdTestHelper.CreateAndSave("Test Ad folder 1");

		//		// Get all
		//		var actualAd = _AdSecureService.GetById(Ad.Id);

		//		_AdTestHelper.AssertAreEquel(Ad, actualAd);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetAll_GetAllFolders_AllFoldersReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad1 = _AdTestHelper.CreateAndSave("Test Ad folder 1");
		//		var Ad2 = _AdTestHelper.CreateAndSave("Test Ad folder 2");
		//		var Ad3 = _AdTestHelper.CreateAndSave("Test Ad folder 3");

		//		// Get all
		//		var Ads = _AdSecureService.GetAll();
				
		//		_AdTestHelper.AssertContains(Ad1, Ads);
		//		_AdTestHelper.AssertContains(Ad2, Ads);
		//		_AdTestHelper.AssertContains(Ad3, Ads);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetAllByParent_GetAllFoldersBySomeParent_AllSubFoldersReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad1 = _AdTestHelper.CreateAndSave("Test Ad folder 1");
		//		var Ad2 = _AdTestHelper.CreateAndSave("Test Ad folder 2", Ad1);
		//		var Ad3 = _AdTestHelper.CreateAndSave("Test Ad folder 3", Ad1);

		//		// Get by parent
		//		var Ads = _AdSecureService.GetAllByParent(Ad1.Id);

		//		_AdTestHelper.AssertDoesNotContain(Ad1, Ads);
		//		_AdTestHelper.AssertContains(Ad2, Ads);
		//		_AdTestHelper.AssertContains(Ad3, Ads);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetTree_SelectedIdNull_CorrectNodesReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad1 = _AdTestHelper.CreateAndSave("Test Ad folder 1");
		//		var Ad2 = _AdTestHelper.CreateAndSave("Test Ad folder 2", Ad1);
		//		var Ad3 = _AdTestHelper.CreateAndSave("Test Ad folder 3", Ad2);
		//		var Ad4 = _AdTestHelper.CreateAndSave("Test Ad folder 4");

		//		// Get by parent
		//		Collection<INode> AdNodes = _AdSecureService.GetTree(null);

		//		_AdTestHelper.AssertContains(Ad1, AdNodes);
		//		_AdTestHelper.AssertDoesNotContain(Ad2, AdNodes);
		//		_AdTestHelper.AssertDoesNotContain(Ad3, AdNodes);
		//		_AdTestHelper.AssertContains(Ad4, AdNodes);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetTree_SecondLevelSelected_CorrectNodesReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad1 = _AdTestHelper.CreateAndSave("Test Ad folder 1");
		//		var Ad2 = _AdTestHelper.CreateAndSave("Test Ad folder 2", Ad1);
		//		var Ad3 = _AdTestHelper.CreateAndSave("Test Ad folder 3", Ad2);
		//		var Ad4 = _AdTestHelper.CreateAndSave("Test Ad folder 4", Ad3);
		//		var Ad5 = _AdTestHelper.CreateAndSave("Test Ad folder 5");

		//		// Get by parent
		//		Collection<INode> AdNodes = _AdSecureService.GetTree(Ad2.Id);

		//		_AdTestHelper.AssertContains(Ad1, AdNodes);
		//		_AdTestHelper.AssertContains(Ad2, AdNodes);
		//		_AdTestHelper.AssertContains(Ad3, AdNodes);
		//		// Node 4 is not included in output
		//		_AdTestHelper.AssertDoesNotContain(Ad4, AdNodes);
		//		_AdTestHelper.AssertContains(Ad5, AdNodes);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Delete")]
		//public void TryDelete_DeleteSingleFolder_FolderRemoved()
		//{
		//	 //Expect.Call(_adSecureService.GetAllByFolder(0)).Return(new Collection<IAd>() ).IgnoreArguments();

		//	_mocker.ReplayAll();

		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad = _AdTestHelper.CreateAndSave("Test Ad folder");

		//		// Get by parent
		//		bool isDeleted = _AdSecureService.TryDelete(_systemUserFull, Ad.Id);
		//		var actualAd = _AdSecureService.GetById(Ad.Id);

		//		Assert.IsTrue(isDeleted, "Ad deleted");
		//		Assert.IsNull(actualAd, "Ad deleted");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Delete")]
		//public void TryDelete_DeleteFolderWithSubFoldersAndAds_FolderNotRemoved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var Ad1 = _AdTestHelper.CreateAndSave("Test Ad folder");
		//		var Ad2 = _AdTestHelper.CreateAndSave("Test Ad folder", Ad1);

		//		//Expect
		//		//	.Call(_adSecureService.GetAllByFolder(Ad1.Id))
		//		//	.Return(new Collection<IAd>());

		//		//Expect
		//		//	.Call(_adSecureService.GetAllByFolder(Ad2.Id))
		//		//	.Return(new Collection<IAd> { new Ad() });

		//		_mocker.ReplayAll();
				
		//		// Get by parent
		//		bool isDeleted = _AdSecureService.TryDelete(_systemUserFull, Ad1.Id);

		//		Assert.IsFalse(isDeleted, "Ad deleted");

		//		transactionScope.Dispose();
		//	}
		//}
	}
}