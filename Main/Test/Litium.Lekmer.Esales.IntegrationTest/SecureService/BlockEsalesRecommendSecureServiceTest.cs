﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockEsalesRecommendSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesRecommendSecureService>();

			Assert.IsInstanceOf<IBlockEsalesRecommendSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockEsalesRecommendSecureService>();
			var service2 = IoC.Resolve<IBlockEsalesRecommendSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}