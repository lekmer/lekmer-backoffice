using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class SearchRequestTest
	{
		[Test]
		[Category("IoC")]
		public void SearchRequest_Resolve_Resolved()
		{
			var info = IoC.Resolve<ISearchRequest>();

			Assert.IsInstanceOf<ISearchRequest>(info);
			Assert.IsInstanceOf<SearchRequest>(info);
		}

		[Test]
		[Category("IoC")]
		public void SearchRequest_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ISearchRequest>();
			var esalesInfo2 = IoC.Resolve<ISearchRequest>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}