using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class AdHitsTest
	{
		[Test]
		[Category("IoC")]
		public void AdHits_Resolve_Resolved()
		{
			var info = IoC.Resolve<IAdHits>();

			Assert.IsInstanceOf<IAdHits>(info);
			Assert.IsInstanceOf<AdHits>(info);
		}

		[Test]
		[Category("IoC")]
		public void AdHits_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IAdHits>();
			var esalesInfo2 = IoC.Resolve<IAdHits>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}