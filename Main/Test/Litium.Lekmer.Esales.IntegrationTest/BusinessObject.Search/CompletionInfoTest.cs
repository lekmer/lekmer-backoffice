using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class CompletionInfoTest
	{
		[Test]
		[Category("IoC")]
		public void CompletionInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICompletionInfo>();

			Assert.IsInstanceOf<ICompletionInfo>(info);
			Assert.IsInstanceOf<CompletionInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void CompletionInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ICompletionInfo>();
			var esalesInfo2 = IoC.Resolve<ICompletionInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}