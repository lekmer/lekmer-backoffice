using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class CountHitsTest
	{
		[Test]
		[Category("IoC")]
		public void CountHits_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICountHits>();

			Assert.IsInstanceOf<ICountHits>(info);
			Assert.IsInstanceOf<CountHits>(info);
		}

		[Test]
		[Category("IoC")]
		public void CountHits_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ICountHits>();
			var esalesInfo2 = IoC.Resolve<ICountHits>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}