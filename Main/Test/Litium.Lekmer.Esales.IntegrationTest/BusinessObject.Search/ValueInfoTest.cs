using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class ValueInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ValueInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IValueInfo>();

			Assert.IsInstanceOf<IValueInfo>(info);
			Assert.IsInstanceOf<ValueInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void ValueInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IValueInfo>();
			var esalesInfo2 = IoC.Resolve<IValueInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}