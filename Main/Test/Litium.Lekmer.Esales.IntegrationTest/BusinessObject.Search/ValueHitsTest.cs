using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class ValueHitsTest
	{
		[Test]
		[Category("IoC")]
		public void ValueHits_Resolve_Resolved()
		{
			var info = IoC.Resolve<IValueHits>();

			Assert.IsInstanceOf<IValueHits>(info);
			Assert.IsInstanceOf<ValueHits>(info);
		}

		[Test]
		[Category("IoC")]
		public void ValueHits_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IValueHits>();
			var esalesInfo2 = IoC.Resolve<IValueHits>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}