﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class CartEsalesRequestTest
	{
		[Test]
		[Category("IoC")]
		public void CartEsalesRequest_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICartEsalesRequest>();

			Assert.IsInstanceOf<ICartEsalesRequest>(info);
			Assert.IsInstanceOf<CartEsalesRequest>(info);
		}

		[Test]
		[Category("IoC")]
		public void CartEsalesRequest_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ICartEsalesRequest>();
			var esalesInfo2 = IoC.Resolve<ICartEsalesRequest>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}

	}
}
