using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class ProductTicketEsalesRequestTest
	{
		[Test]
		[Category("IoC")]
		public void ProductTicketEsalesRequest_Resolve_Resolved()
		{
			var info = IoC.Resolve<IProductTicketEsalesRequest>();

			Assert.IsInstanceOf<IProductTicketEsalesRequest>(info);
			Assert.IsInstanceOf<ProductTicketEsalesRequest>(info);
		}

		[Test]
		[Category("IoC")]
		public void ProductTicketEsalesRequest_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IProductTicketEsalesRequest>();
			var esalesInfo2 = IoC.Resolve<IProductTicketEsalesRequest>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}