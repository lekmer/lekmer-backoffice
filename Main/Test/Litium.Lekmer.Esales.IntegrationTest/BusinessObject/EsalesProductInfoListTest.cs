using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class EsalesProductInfoListTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductInfoList_Resolve_Resolved()
		{
			var info = IoC.Resolve<IEsalesProductInfoList>();

			Assert.IsInstanceOf<IEsalesProductInfoList>(info);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductInfoList_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IEsalesProductInfoList>();
			var esalesInfo2 = IoC.Resolve<IEsalesProductInfoList>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}