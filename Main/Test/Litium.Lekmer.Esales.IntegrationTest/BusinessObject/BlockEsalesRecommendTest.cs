﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockEsalesRecommendTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommend_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockEsalesRecommend>();

			Assert.IsInstanceOf<IBlockEsalesRecommend>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommend_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockEsalesRecommend>();
			var block2 = IoC.Resolve<IBlockEsalesRecommend>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}