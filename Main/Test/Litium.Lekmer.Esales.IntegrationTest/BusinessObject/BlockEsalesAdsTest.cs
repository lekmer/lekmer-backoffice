﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockEsalesAdsTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesAds_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockEsalesAds>();

			Assert.IsInstanceOf<IBlockEsalesAds>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesAds_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockEsalesAds>();
			var block2 = IoC.Resolve<IBlockEsalesAds>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}