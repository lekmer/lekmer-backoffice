﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class AdFolderTest
	{
		[Test]
		[Category("IoC")]
		public void AdFolder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IAdFolder>();

			Assert.IsInstanceOf<IAdFolder>(instance);
			Assert.IsInstanceOf<IAdFolder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void AdFolder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IAdFolder>();
			var instance2 = IoC.Resolve<IAdFolder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}