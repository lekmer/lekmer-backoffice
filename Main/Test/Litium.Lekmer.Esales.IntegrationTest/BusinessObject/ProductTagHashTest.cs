using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductTagHashTest
	{
		[Test]
		[Category("IoC")]
		public void ProductTagHash_Resolve_Resolved()
		{
			var hash = IoC.Resolve<IProductTagHash>();

			Assert.IsInstanceOf<IProductTagHash>(hash);
		}

		[Test]
		[Category("IoC")]
		public void ProductTagHash_ResolveTwice_DifferentObjects()
		{
			var hash1 = IoC.Resolve<IProductTagHash>();
			var hash2 = IoC.Resolve<IProductTagHash>();

			Assert.AreNotEqual(hash1, hash2);
		}
	}
}