﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class AdTest
	{
		[Test]
		[Category("IoC")]
		public void Ad_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IAd>();

			Assert.IsInstanceOf<IAd>(instance);
			Assert.IsInstanceOf<IAd>(instance);
		}

		[Test]
		[Category("IoC")]
		public void Ad_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IAd>();
			var instance2 = IoC.Resolve<IAd>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}