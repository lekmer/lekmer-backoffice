﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockEsalesRecommendRepositoryTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendRepositoryV2_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockEsalesRecommendRepositoryV2>();

			Assert.IsInstanceOf<BlockEsalesRecommendRepositoryV2>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendRepositoryV2_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockEsalesRecommendRepositoryV2>();
			var repository2 = IoC.Resolve<BlockEsalesRecommendRepositoryV2>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}