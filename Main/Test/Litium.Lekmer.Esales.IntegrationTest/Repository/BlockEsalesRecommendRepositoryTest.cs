﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockEsalesRecommendRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockEsalesRecommendRepository>();

			Assert.IsInstanceOf<BlockEsalesRecommendRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockEsalesRecommendRepository>();
			var repository2 = IoC.Resolve<BlockEsalesRecommendRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}