﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class EsalesRegistryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesRegistryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<EsalesRegistryRepository>();

			Assert.IsInstanceOf<EsalesRegistryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRegistryRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<EsalesRegistryRepository>();
			var instance2 = IoC.Resolve<EsalesRegistryRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}