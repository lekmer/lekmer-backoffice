﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockEsalesTopSellersCategoryRepositoryTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryRepositoryV2_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockEsalesTopSellersCategoryRepositoryV2>();

			Assert.IsInstanceOf<BlockEsalesTopSellersCategoryRepositoryV2>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryRepositoryV2_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockEsalesTopSellersCategoryRepositoryV2>();
			var repository2 = IoC.Resolve<BlockEsalesTopSellersCategoryRepositoryV2>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}