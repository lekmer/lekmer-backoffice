﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockEsalesRecommendDataMapperTestV2
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendDataMapperV2_Resolve_Resolved()
		{
			DataMapperBase<IBlockEsalesRecommendV2> dataMapper = DataMapperResolver.Resolve<IBlockEsalesRecommendV2>(_dataReader);

			Assert.IsInstanceOf<BlockEsalesRecommendDataMapperV2>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendDataMapperV2_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockEsalesRecommendV2> dataMapper1 = DataMapperResolver.Resolve<IBlockEsalesRecommendV2>(_dataReader);
			DataMapperBase<IBlockEsalesRecommendV2> dataMapper2 = DataMapperResolver.Resolve<IBlockEsalesRecommendV2>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}