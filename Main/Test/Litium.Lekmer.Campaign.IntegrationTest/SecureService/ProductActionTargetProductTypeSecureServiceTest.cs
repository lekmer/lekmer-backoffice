﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductActionTargetProductTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionTargetProductTypeSecureService>();

			Assert.IsInstanceOf<IProductActionTargetProductTypeSecureService>(service);
			Assert.IsInstanceOf<ProductActionTargetProductTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductTypeSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionTargetProductTypeSecureService>();
			var instance2 = IoC.Resolve<IProductActionTargetProductTypeSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}