﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class FixedDiscountCartActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountCartActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginSecureService>("secure_FixedCartDiscount");

			Assert.IsInstanceOf<ICartActionPluginSecureService>(service);
			Assert.IsInstanceOf<FixedDiscountCartActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountCartActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginSecureService>("secure_FixedCartDiscount");
			var instance2 = IoC.Resolve<ICartActionPluginSecureService>("secure_FixedCartDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}