﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CampaignPriceTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignPriceTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignPriceTypeSecureService>();

			Assert.IsInstanceOf<ICampaignPriceTypeSecureService>(service);
			Assert.IsInstanceOf<CampaignPriceTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignPriceTypeSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignPriceTypeSecureService>();
			var instance2 = IoC.Resolve<ICampaignPriceTypeSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}