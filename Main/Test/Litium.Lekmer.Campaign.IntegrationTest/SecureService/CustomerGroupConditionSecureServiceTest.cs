﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CustomerGroupConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginSecureService>("secure_CustomerGroupUser");

			Assert.IsInstanceOf<IConditionPluginSecureService>(service);
			Assert.IsInstanceOf<CustomerGroupConditionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginSecureService>("secure_CustomerGroupUser");
			var instance2 = IoC.Resolve<IConditionPluginSecureService>("secure_CustomerGroupUser");

			Assert.AreEqual(instance1, instance2);
		}
	}
}