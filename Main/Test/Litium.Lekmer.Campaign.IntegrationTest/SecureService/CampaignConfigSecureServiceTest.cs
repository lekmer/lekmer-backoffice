﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CampaignConfigSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignConfigSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignConfigSecureService>();

			Assert.IsInstanceOf<ICampaignConfigSecureService>(service);
			Assert.IsInstanceOf<CampaignConfigSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignConfigSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignConfigSecureService>();
			var instance2 = IoC.Resolve<ICampaignConfigSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}