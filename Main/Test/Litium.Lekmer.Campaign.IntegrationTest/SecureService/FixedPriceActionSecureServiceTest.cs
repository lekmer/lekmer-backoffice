﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class FixedPriceActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
        public void FixedPriceActionSecureService_Resolve_Resolved()
		{
            var service = IoC.Resolve<IProductActionPluginSecureService>("secure_FixedPrice");

			Assert.IsInstanceOf<IProductActionPluginSecureService>(service);
            Assert.IsInstanceOf<FixedPriceActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
        public void FixedPriceActionSecureService_ResolveTwice_SameObjects()
		{
            var instance1 = IoC.Resolve<IProductActionPluginSecureService>("secure_FixedPrice");
            var instance2 = IoC.Resolve<IProductActionPluginSecureService>("secure_FixedPrice");

			Assert.AreEqual(instance1, instance2);
		}
	}
}