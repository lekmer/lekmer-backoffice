﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class GiftCardViaMailInfoSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IGiftCardViaMailInfoSecureService>();

			Assert.IsInstanceOf<IGiftCardViaMailInfoSecureService>(service);
			Assert.IsInstanceOf<GiftCardViaMailInfoSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IGiftCardViaMailInfoSecureService>();
			var instance2 = IoC.Resolve<IGiftCardViaMailInfoSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}