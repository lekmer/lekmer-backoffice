﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class ConditionTargetProductTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionTargetProductTypeSecureService>();

			Assert.IsInstanceOf<IConditionTargetProductTypeSecureService>(service);
			Assert.IsInstanceOf<ConditionTargetProductTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionTargetProductTypeSecureService>();
			var instance2 = IoC.Resolve<IConditionTargetProductTypeSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}