﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CartActionTargetProductTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionTargetProductTypeSecureService>();

			Assert.IsInstanceOf<ICartActionTargetProductTypeSecureService>(service);
			Assert.IsInstanceOf<CartActionTargetProductTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionTargetProductTypeSecureService>();
			var instance2 = IoC.Resolve<ICartActionTargetProductTypeSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}