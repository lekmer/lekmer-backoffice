﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignPriceTypeTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignPriceType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignPriceType>();
			Assert.IsInstanceOf<ICampaignPriceType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignPriceType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignPriceType>();
			var instance2 = IoC.Resolve<ICampaignPriceType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}