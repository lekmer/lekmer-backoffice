﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductDiscountActionTest
	{
		[Test]
		[Category("IoC")]
		public void ProductDiscountAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductDiscountAction>();

			Assert.IsInstanceOf<IProductDiscountAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductDiscountAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductDiscountAction>();
			var instance2 = IoC.Resolve<IProductDiscountAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}