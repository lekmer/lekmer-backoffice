﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignRegistryCampaignRegistryCampaignLandingPageTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignRegistryCampaignLandingPage_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignRegistryCampaignLandingPage>();
			Assert.IsInstanceOf<ICampaignRegistryCampaignLandingPage>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignLandingPage_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignRegistryCampaignLandingPage>();
			var instance2 = IoC.Resolve<ICampaignRegistryCampaignLandingPage>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}