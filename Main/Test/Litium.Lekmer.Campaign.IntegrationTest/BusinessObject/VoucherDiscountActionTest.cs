﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class VoucherDiscountActionTest
	{
		[Test]
		[Category("IoC")]
		public void VoucherDiscountAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IVoucherDiscountAction>();

			Assert.IsInstanceOf<IVoucherDiscountAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void VoucherDiscountAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IVoucherDiscountAction>();
			var instance2 = IoC.Resolve<IVoucherDiscountAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}