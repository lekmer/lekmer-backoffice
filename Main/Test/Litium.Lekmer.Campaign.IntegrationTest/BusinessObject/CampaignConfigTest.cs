﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignConfigTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignConfig_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignConfig>();

			Assert.IsInstanceOf<ICampaignConfig>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignConfig_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignConfig>();
			var instance2 = IoC.Resolve<ICampaignConfig>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}