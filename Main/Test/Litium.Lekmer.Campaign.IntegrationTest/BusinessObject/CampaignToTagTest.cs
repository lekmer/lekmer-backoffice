﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignToTagTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignToTag_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignToTag>();

			Assert.IsInstanceOf<ICampaignToTag>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignToTag_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignToTag>();
			var instance2 = IoC.Resolve<ICampaignToTag>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}