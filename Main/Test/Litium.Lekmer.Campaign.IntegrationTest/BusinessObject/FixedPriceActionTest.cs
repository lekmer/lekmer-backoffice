﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class FixedPriceActionTest
	{
		[Test]
		[Category("IoC")]
		public void FixedPriceAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IFixedPriceAction>();

			Assert.IsInstanceOf<IFixedPriceAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void FixedPriceAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IFixedPriceAction>();
			var instance2 = IoC.Resolve<IFixedPriceAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}