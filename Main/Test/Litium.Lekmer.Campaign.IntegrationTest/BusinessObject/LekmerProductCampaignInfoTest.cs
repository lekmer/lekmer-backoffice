﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerProductCampaignInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ProductCampaignInfo_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductCampaignInfo>();
			Assert.IsInstanceOf<ILekmerProductCampaignInfo>(instance);
			Assert.IsInstanceOf<LekmerProductCampaignInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductCampaignInfo_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductCampaignInfo>();
			var instance2 = IoC.Resolve<IProductCampaignInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}