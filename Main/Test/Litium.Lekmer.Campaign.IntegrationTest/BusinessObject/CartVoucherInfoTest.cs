﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartVoucherInfoTest
	{
		[Test]
		[Category("IoC")]
		public void CartVoucherInfo_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartVoucherInfo>();

			Assert.IsInstanceOf<ICartVoucherInfo>(instance);
			Assert.IsInstanceOf<CartVoucherInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartVoucherInfo_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartVoucherInfo>();
			var instance2 = IoC.Resolve<ICartVoucherInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}