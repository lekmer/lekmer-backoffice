﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCartCampaignTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartCampaign_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartCampaign>();

			Assert.IsInstanceOf<ICartCampaign>(instance);
			Assert.IsInstanceOf<ILekmerCartCampaign>(instance);
			Assert.IsInstanceOf<LekmerCartCampaign>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductCampaign_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartCampaign>();
			var instance2 = IoC.Resolve<ICartCampaign>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}