﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignLandingPageTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLandingPage_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignLandingPage>();
			Assert.IsInstanceOf<ICampaignLandingPage>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLandingPage_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignLandingPage>();
			var instance2 = IoC.Resolve<ICampaignLandingPage>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}