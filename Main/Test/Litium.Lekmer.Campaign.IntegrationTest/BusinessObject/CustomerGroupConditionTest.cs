﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CustomerGroupConditionTest
	{
		[Test]
		[Category("IoC")]
		public void CustomerGroupCondition_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICustomerGroupCondition>();

			Assert.IsInstanceOf<ICustomerGroupCondition>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CustomerGroupCondition_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICustomerGroupCondition>();
			var instance2 = IoC.Resolve<ICustomerGroupCondition>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}