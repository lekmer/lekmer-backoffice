﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CurrencyValueRangeTest
	{
		[Test]
		[Category("IoC")]
		public void CurrencyValueRange_Resolve_Resolved()
		{
			var instance = IoC.Resolve<CurrencyValueRange>();

			Assert.IsInstanceOf<CurrencyValueRange>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CurrencyValueRange_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<CurrencyValueRange>();
			var instance2 = IoC.Resolve<CurrencyValueRange>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}