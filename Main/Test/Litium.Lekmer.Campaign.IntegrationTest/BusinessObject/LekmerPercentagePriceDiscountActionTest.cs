﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerPercentagePriceDiscountActionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IPercentagePriceDiscountAction>();

			Assert.IsInstanceOf<IPercentagePriceDiscountAction>(instance);
			Assert.IsInstanceOf<ILekmerPercentagePriceDiscountAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IPercentagePriceDiscountAction>();
			var instance2 = IoC.Resolve<IPercentagePriceDiscountAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}