﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartValueRangeConditionTest
	{
		[Test]
		[Category("IoC")]
		public void CartValueRangeCondition_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartValueRangeCondition>();

			Assert.IsInstanceOf<ICartValueRangeCondition>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartValueRangeCondition_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartValueRangeCondition>();
			var instance2 = IoC.Resolve<ICartValueRangeCondition>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}