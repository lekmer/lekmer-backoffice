﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCartActionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartAction>();

			Assert.IsInstanceOf<ICartAction>(instance);
			Assert.IsInstanceOf<ILekmerCartAction>(instance);
			Assert.IsInstanceOf<LekmerCartAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartAction>();
			var instance2 = IoC.Resolve<ICartAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}