﻿using System.Data;
using Litium.Lekmer.Campaign.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.IntegrationTest.Mapper
{
	[TestFixture]
	public class CampaignRegistryCampaignLandingPageDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignLandingPageDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ICampaignRegistryCampaignLandingPage>(_dataReader);
			Assert.IsInstanceOf<CampaignRegistryCampaignLandingPageDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignLandingPageDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ICampaignRegistryCampaignLandingPage>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ICampaignRegistryCampaignLandingPage>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}