﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartItemsGroupValueConditionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueConditionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginService>("CartItemsGroupValue");

			Assert.IsInstanceOf<IConditionPluginService>(service);
			Assert.IsInstanceOf<CartItemsGroupValueConditionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueConditionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginService>("CartItemsGroupValue");
			var instance2 = IoC.Resolve<IConditionPluginService>("CartItemsGroupValue");

			Assert.AreEqual(instance1, instance2);
		}
	}
}