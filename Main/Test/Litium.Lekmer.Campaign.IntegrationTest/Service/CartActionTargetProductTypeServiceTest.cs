﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartActionTargetProductTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionTargetProductTypeService>();

			Assert.IsInstanceOf<ICartActionTargetProductTypeService>(service);
			Assert.IsInstanceOf<CartActionTargetProductTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionTargetProductTypeService>();
			var instance2 = IoC.Resolve<ICartActionTargetProductTypeService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}