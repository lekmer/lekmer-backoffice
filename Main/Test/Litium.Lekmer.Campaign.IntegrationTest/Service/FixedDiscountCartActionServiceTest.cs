﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class FixedDiscountCdartActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountCartActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginService>("FixedCartDiscount");

			Assert.IsInstanceOf<ICartActionPluginService>(service);
			Assert.IsInstanceOf<FixedDiscountCartActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountCartActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginService>("FixedCartDiscount");
			var instance2 = IoC.Resolve<ICartActionPluginService>("FixedCartDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}