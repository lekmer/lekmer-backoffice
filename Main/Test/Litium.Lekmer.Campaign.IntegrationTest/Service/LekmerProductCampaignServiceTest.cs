﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerProductCampaignServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerProductCampaignService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductCampaignService>();

			Assert.IsInstanceOf<IProductCampaignService>(service);
			Assert.IsInstanceOf<LekmerProductCampaignService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductCampaignService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductCampaignService>();
			var instance2 = IoC.Resolve<IProductCampaignService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}