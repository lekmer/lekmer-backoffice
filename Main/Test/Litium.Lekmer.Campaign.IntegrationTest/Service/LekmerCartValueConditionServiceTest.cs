﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCartValueConditionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartValueConditionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginService>("CartValue");

			Assert.IsInstanceOf<IConditionPluginService>(service);
			Assert.IsInstanceOf<LekmerCartValueConditionService>(service);
			Assert.IsInstanceOf<CartValueConditionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartValueConditionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginService>("CartValue");
			var instance2 = IoC.Resolve<IConditionPluginService>("CartValue");

			Assert.AreEqual(instance1, instance2);
		}
	}
}