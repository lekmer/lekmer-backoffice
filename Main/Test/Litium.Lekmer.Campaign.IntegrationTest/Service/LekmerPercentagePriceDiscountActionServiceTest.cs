﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerPercentagePriceDiscountActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginService>("PercentagePriceDiscount");

			Assert.IsInstanceOf<IProductActionPluginService>(service);
			Assert.IsInstanceOf<LekmerPercentagePriceDiscountActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginService>("PercentagePriceDiscount");
			var instance2 = IoC.Resolve<IProductActionPluginService>("PercentagePriceDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}