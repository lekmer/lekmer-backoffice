﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CampaignLandingPageServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLandingPageService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignLandingPageService>();

			Assert.IsInstanceOf<ICampaignLandingPageService>(service);
			Assert.IsInstanceOf<CampaignLandingPageService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLandingPageService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignLandingPageService>();
			var instance2 = IoC.Resolve<ICampaignLandingPageService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}