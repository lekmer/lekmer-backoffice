﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CampaignActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignActionRepository>();

			Assert.IsInstanceOf<CampaignActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CampaignActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CampaignActionRepository>();
			var instance2 = IoC.Resolve<CampaignActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}