﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CartActionTargetProductTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartActionTargetProductTypeRepository>();

			Assert.IsInstanceOf<CartActionTargetProductTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartActionTargetProductTypeRepository>();
			var instance2 = IoC.Resolve<CartActionTargetProductTypeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}