﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerCartValueConditionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartValueConditionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartValueConditionRepository>();

			Assert.IsInstanceOf<CartValueConditionRepository>(repository);
			Assert.IsInstanceOf<LekmerCartValueConditionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartValueConditionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartValueConditionRepository>();
			var instance2 = IoC.Resolve<CartValueConditionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}