﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CartItemsGroupValueConditionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueConditionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartItemsGroupValueConditionRepository>();

			Assert.IsInstanceOf<CartItemsGroupValueConditionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueConditionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartItemsGroupValueConditionRepository>();
			var instance2 = IoC.Resolve<CartItemsGroupValueConditionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}