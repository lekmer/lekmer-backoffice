﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class PercentageDiscountCartActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<PercentageDiscountCartActionRepository>();

			Assert.IsInstanceOf<PercentageDiscountCartActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<PercentageDiscountCartActionRepository>();
			var instance2 = IoC.Resolve<PercentageDiscountCartActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}