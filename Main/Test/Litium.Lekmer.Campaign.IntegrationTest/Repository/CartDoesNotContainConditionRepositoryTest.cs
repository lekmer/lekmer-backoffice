﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CartDoesNotContainConditionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartDoesNotContainConditionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartDoesNotContainConditionRepository>();

			Assert.IsInstanceOf<CartDoesNotContainConditionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartDoesNotContainConditionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartDoesNotContainConditionRepository>();
			var instance2 = IoC.Resolve<CartDoesNotContainConditionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}