﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CartItemDiscountCartActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartItemDiscountCartActionRepository>();

			Assert.IsInstanceOf<CartItemDiscountCartActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartItemDiscountCartActionRepository>();
			var instance2 = IoC.Resolve<CartItemDiscountCartActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}