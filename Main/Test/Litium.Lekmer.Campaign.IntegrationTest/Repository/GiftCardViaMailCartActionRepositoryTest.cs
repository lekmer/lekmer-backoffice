﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class GiftCardViaMailCartActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailCartActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<GiftCardViaMailCartActionRepository>();

			Assert.IsInstanceOf<GiftCardViaMailCartActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailCartActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<GiftCardViaMailCartActionRepository>();
			var instance2 = IoC.Resolve<GiftCardViaMailCartActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}