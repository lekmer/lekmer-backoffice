﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Service
{
	[TestFixture]
	public class KlarnaTransactionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaTransactionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaTransactionService>();

			Assert.IsInstanceOf<IKlarnaTransactionService>(service);
			Assert.IsInstanceOf<KlarnaTransactionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaTransactionService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IKlarnaTransactionService>();
			var service2 = IoC.Resolve<IKlarnaTransactionService>();

			Assert.AreEqual(service1, service2);
		}
	}
}