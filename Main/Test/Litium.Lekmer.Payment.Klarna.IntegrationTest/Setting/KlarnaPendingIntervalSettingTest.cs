﻿using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Setting
{
	[TestFixture]
	public class KlarnaPendingIntervalSettingTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaPendingIntervalSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaPendingIntervalSetting>();

			Assert.IsInstanceOf<IKlarnaPendingIntervalSetting>(service);
			Assert.IsInstanceOf<IKlarnaPendingIntervalSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaPendingIntervalSetting_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IKlarnaPendingIntervalSetting>();
			var service2 = IoC.Resolve<IKlarnaPendingIntervalSetting>();

			Assert.AreEqual(service1, service2);
		}
	}
}