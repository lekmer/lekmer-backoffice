﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.API
{
	[TestFixture]
	public class KlarnaCheckoutClientTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaCheckoutClient_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaCheckoutClient>();

			Assert.IsInstanceOf<IKlarnaCheckoutClient>(service);
			Assert.IsInstanceOf<KlarnaCheckoutClient>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaCheckoutClient_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IKlarnaCheckoutClient>();
			var service2 = IoC.Resolve<IKlarnaCheckoutClient>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}