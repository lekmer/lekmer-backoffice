﻿using Litium.Lekmer.Oculus.Repository;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Oculus.IntegrationTest.Cache
{
	[TestFixture]
	public class ProductSimilarServiceTest
	{
		private static readonly int PRODUCT_ID_1 = 109547;
		private static readonly int PRODUCT_ID_2 = 109548;
		private static readonly int PRODUCT_ID_3 = 109549;
		private static readonly int PRODUCT_ID_4 = 109550;

		private static MockRepository _mocker;
		private static ProductSimilarRepository _productSimilarRepository;
		private static IProductSimilarService _productSimilarService;
		private static IProductSimilarSecureService _productSimilarSecureService;
		private ProductHelper _productHelper;
		private ProductCollection _productCollection;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_productHelper = new ProductHelper();

			// Get product collection.
			_productCollection = _productHelper.GetProducts();
		}

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_productSimilarRepository = _mocker.DynamicMock<ProductSimilarRepository>();
			_productSimilarService = new ProductSimilarService(_productSimilarRepository);
			_productSimilarSecureService = new ProductSimilarSecureService(_productSimilarRepository, null);

			ProductSimilarCache.Instance.Flush();
		}

		[Test]
		public void GetAllByProductId_SameId_CacheUsed()
		{
			Expect.Call(_productSimilarRepository.GetAllByProductId(PRODUCT_ID_1)).Return(new ProductSimilar { ProductId = PRODUCT_ID_1 });

			_mocker.ReplayAll();

			Assert.AreEqual(PRODUCT_ID_1, _productSimilarService.GetAllByProductId(PRODUCT_ID_1).ProductId);
			Assert.AreEqual(PRODUCT_ID_1, _productSimilarService.GetAllByProductId(PRODUCT_ID_1).ProductId);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetAllByProductId_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_productSimilarRepository.GetAllByProductId(PRODUCT_ID_2)).Return(new ProductSimilar { ProductId = PRODUCT_ID_2 });
			Expect.Call(_productSimilarRepository.GetAllByProductId(PRODUCT_ID_3)).Return(new ProductSimilar { ProductId = PRODUCT_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(PRODUCT_ID_2, _productSimilarService.GetAllByProductId(PRODUCT_ID_2).ProductId);
			Assert.AreEqual(PRODUCT_ID_3, _productSimilarService.GetAllByProductId(PRODUCT_ID_3).ProductId);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetAllByProductId_ProductSeoSettingsUpdated_CacheRemoved()
		{
			int productId = _productCollection[0].Id;

			IProductSimilar productSimilar = new ProductSimilar { ProductId = productId };

			Expect.Call(_productSimilarRepository.GetAllByProductId(productId)).Return(productSimilar).Repeat.Twice();
			Expect.Call(_productSimilarRepository.Save(null)).IgnoreArguments().Return(productSimilar);
			
			_mocker.ReplayAll();

			Assert.AreEqual(productId, _productSimilarService.GetAllByProductId(productId).ProductId);
			_productHelper.SaveSimilarProduct(_productCollection, _productSimilarSecureService);
			Assert.AreEqual(productId, _productSimilarService.GetAllByProductId(productId).ProductId);

			_mocker.VerifyAll();
		}
	}
}