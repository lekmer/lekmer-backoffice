﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.UnitTest.BusinessObject
{
	[TestFixture]
	public class LekmerOrderFullTest
	{
		[Test]
		public void GetFreightCostVat_is_zero_on_zero_freightcost()
		{
			var order = OrderCreator.New()
				.FreightCost(0)
				.PaymentCost(0)
				.OrderItem(new Price(100, 80), 1)
				.Create();

			Assert.AreEqual(0m, order.GetFreightCostVat());
		}

		[Test]
		public void GetFreightCostVat_is_zero_when_order_has_zero_items()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(0)
				.Create();

			Assert.AreEqual(0m, order.GetFreightCostVat());
		}

		[Test]
		public void GetFreightCostVat_is_zero_when_orderitem_cost_zero()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(0)
				.OrderItem(new Price(0, 0), 1)
				.Create();

			Assert.AreEqual(0, order.GetFreightCostVat());
		}

		[Test]
		public void GetFreightCostVat_is_calculated_with_order_items_vat()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(0)
				.OrderItem(new Price(200, 160), 1)
				.Create();

			Assert.AreEqual(20m, order.GetFreightCostVat());
		}

		[Test]
		public void GetFreightCostVat_is_calculated_with_order_items_having_different_vat()
		{
			var book = new OrderItem { ActualPrice = new Price(6600, 6000), Quantity = 1 };
			var cd = new OrderItem { ActualPrice = new Price(2600, 2000), Quantity = 1 };

			var order = OrderCreator.New()
				.FreightCost(230)
				.CampaignFreightCost(50)
				.PaymentCost(0)
				.OrderItem(book)
				.OrderItem(cd)
				.Create();

			Assert.AreEqual(30m, Math.Round(order.GetFreightCostVat(), 4, MidpointRounding.AwayFromZero));
			//Assert.AreEqual(87, order.GetFreightCostVat());

			// Calculation:
			//			ExVat			Vat+1		IncVat
			// Book:	6000	*		1,10		6600
			// CD:		2000	*		1,30		2600
			// Summary:	8000
			// 
			// Ratio		Vat
			// 6000/10000	*	0,10	=	0,075
			// 2000/10000	*	0,30	=	0,075
			//				Freight vat:	0,15
			//
			// Freight ex vat:
			// Freight IncVat			
			// 230		/		(1 +	0,15)		=		200
			//
			// Freight vat:
			// 230 - 200 = 30
		}

		[Test]
		public void GetActualFreightCostVat_is_calculated_with_order_items_having_different_vat()
		{
			var book = new OrderItem { ActualPrice = new Price(6600, 6000), Quantity = 1 };
			var cd = new OrderItem { ActualPrice = new Price(2600, 2000), Quantity = 1 };

			var order = OrderCreator.New()
				.FreightCost(230)
				.CampaignFreightCost(230)
				.PaymentCost(0)
				.OrderItem(book)
				.OrderItem(cd)
				.Create();

			Assert.AreEqual(30m, Math.Round(order.GetActualFreightCostVat(), 4, MidpointRounding.AwayFromZero));

			// Calculation:
			//			ExVat			Vat+1		IncVat
			// Book:	6000	*		1,10		6600
			// CD:		2000	*		1,30		2600
			// Summary:	8000
			// 
			// Ratio		Vat
			// 6000/8000	*	0,10	=	0,075
			// 2000/8000	*	0,30	=	0,075
			//				Freight vat:	0,15
			//
			// Freight ex vat:
			// Freight IncVat			
			// 230		/		(1 +	0,15)		=		200
			//
			// Freight vat:
			// 230 - 200 = 30
		}
		[Test]
		public void GetPaymentCostVat_is_zero_on_zero_paymentcost()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(0)
				.OrderItem(new Price(100, 80), 1)
				.Create();

			Assert.AreEqual(0, order.GetPaymentCostVat());
		}

		[Test]
		public void GetPaymentCostVat_is_zero_when_order_has_zero_items()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(100)
				.Create();

			Assert.AreEqual(0, order.GetPaymentCostVat());
		}

		[Test]
		public void GetPaymentCostVat_is_zero_when_orderitem_cost_zero()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(100)
				.OrderItem(new Price(0, 0), 1)
				.Create();

			Assert.AreEqual(0, order.GetPaymentCostVat());
		}

		[Test]
		public void GetPaymentCostVat_is_calculated_with_order_items_vat()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.PaymentCost(100)
				.OrderItem(new Price(200, 160), 1)
				.Create();

			Assert.AreEqual(20, order.GetPaymentCostVat());
		}

		[Test]
		public void GetPaymentCostVat_is_calculated_with_order_items_having_different_vat()
		{
			var book = new OrderItem { ActualPrice = new Price(6600, 6000), Quantity = 1 };
			var cd = new OrderItem { ActualPrice = new Price(2600, 2000), Quantity = 1 };

			var order = OrderCreator.New()
				.FreightCost(200)
				.CampaignFreightCost(100)
				.PaymentCost(230)
				.OrderItem(book)
				.OrderItem(cd)
				.Create();

			Assert.AreEqual(30m, Math.Round(order.GetPaymentCostVat(), 4, MidpointRounding.AwayFromZero));
		}

		[Test]
		public void GetPriceTotal_is_zero_when_order_is_empty()
		{
			var order = OrderCreator.New()
				.FreightCost(0)
				.CampaignFreightCost(0)
				.PaymentCost(0)
				.Create();

			Assert.AreEqual(new Price(0, 0), order.GetPriceTotal());
		}

		[Test]
		public void GetPriceTotal_only_summarizes_freight_cost_and_payment_cost_when_order_has_no_items()
		{
			var order = OrderCreator.New()
				.FreightCost(150)
				.CampaignFreightCost(100)
				.PaymentCost(200)
				.Create();

			var price = order.GetPriceTotal();
			Assert.AreEqual(350, price.IncludingVat);
			Assert.AreEqual(350, price.ExcludingVat);
		}

		[Test]
		public void GetPriceTotal_summarizes_order_item_cost()
		{
			var order = OrderCreator.New()
				.FreightCost(0)
				.CampaignFreightCost(0)
				.PaymentCost(0)
				.OrderItem(new Price(200, 160), 1)
				.OrderItem(new Price(100, 80), 10)
				.Create();

			Assert.AreEqual(1200, order.GetPriceTotal().IncludingVat);
			Assert.AreEqual(960, order.GetPriceTotal().ExcludingVat);
		}

		[Test]
		public void GetActualPriceTotal_summarizes_order_item_cost_freight_cost_and_payment_cost()
		{
			var order = OrderCreator.New()
				.FreightCost(100)
				.CampaignFreightCost(100)
				.PaymentCost(100)
				.OrderItem(new Price(200, 160), 1)
				.OrderItem(new Price(100, 80), 10)
				.Create();

			Assert.AreEqual(1400, order.GetActualPriceTotal().IncludingVat);
			Assert.AreEqual(1120, order.GetActualPriceTotal().ExcludingVat);
		}

		[Test]
		public void GetPriceTotal_summarizes_order_item_cost_freight_cost_and_payment_cost_where_order_items_has_different_vat()
		{
			var book = new OrderItem { ActualPrice = new Price(6600, 6000), Quantity = 1 };
			var cd = new OrderItem { ActualPrice = new Price(2600, 2000), Quantity = 1 };

			var order = OrderCreator.New()
				.FreightCost(230)
				.CampaignFreightCost(230)
				.PaymentCost(0)
				.OrderItem(book)
				.OrderItem(cd)
				.Create();

			Assert.AreEqual(9430m, order.GetPriceTotal().IncludingVat);
			Assert.AreEqual(8200, order.GetPriceTotal().ExcludingVat);
		}

		[Test]
		public void GetVatTotal_is_zero_when_order_is_empty()
		{
			var order = OrderCreator.New()
				.FreightCost(0)
				.CampaignFreightCost(0)
				.PaymentCost(0)
				.Create();

			Assert.AreEqual(0, order.GetVatTotal());
		}

		[Test]
		public void GetActualVatTotal_summarizes_vat_with_order_items_having_different_vat()
		{
			var book = new OrderItem { ActualPrice = new Price(6600, 6000), Quantity = 1 };
			var cd = new OrderItem { ActualPrice = new Price(2600, 2000), Quantity = 1 };

			var order = OrderCreator.New()
				.FreightCost(230)
				.CampaignFreightCost(230)
				.PaymentCost(0)
				.OrderItem(book)
				.OrderItem(cd)
				.Create();

			Assert.AreEqual(1230m, Math.Round(order.GetActualVatTotal(), 4, MidpointRounding.AwayFromZero));
		}
	}
}
