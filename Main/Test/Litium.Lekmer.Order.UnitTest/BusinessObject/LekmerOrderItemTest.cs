﻿using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.UnitTest.BusinessObject
{
	[TestFixture]
	public class LekmerOrderItemTest
	{
		[Test]
		public void CalculatePackageItemsPrice_PackagePriceSameAsItemsSum_ItemPriceCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(2500, 2000);
			lekmerOrderItem.ActualPrice = new Price(1250, 1000);

			var packageOrderItem1 = new PackageOrderItem();
			packageOrderItem1.OriginalPrice = new Price(1375, 1100);
			packageOrderItem1.ActualPrice = new Price(750, 600);

			var packageOrderItem2 = new PackageOrderItem();
			packageOrderItem2.OriginalPrice = new Price(1125, 900);
			packageOrderItem2.ActualPrice = new Price(500, 400);

			lekmerOrderItem.PackageOrderItems = new Collection<IPackageOrderItem> { packageOrderItem1, packageOrderItem2 };

			lekmerOrderItem.CalculatePackageItemsPrice();

			Assert.AreEqual(lekmerOrderItem.ActualPrice.IncludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.ActualPrice.IncludingVat));
			Assert.AreEqual(lekmerOrderItem.ActualPrice.ExcludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.ActualPrice.ExcludingVat));

			Assert.AreEqual(750, packageOrderItem1.PackagePrice.IncludingVat);
			Assert.AreEqual(600, packageOrderItem1.PackagePrice.ExcludingVat);

			Assert.AreEqual(500, packageOrderItem2.PackagePrice.IncludingVat);
			Assert.AreEqual(400, packageOrderItem2.PackagePrice.ExcludingVat);
		}

		[Test]
		public void CalculatePackageItemsPrice_PackagePriceLessThenItemsSum_ItemPriceCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(2500, 2000);
			lekmerOrderItem.ActualPrice = new Price(1000, 800);

			var packageOrderItem1 = new PackageOrderItem();
			packageOrderItem1.OriginalPrice = new Price(1375, 1100);
			packageOrderItem1.ActualPrice = new Price(750, 600);

			var packageOrderItem2 = new PackageOrderItem();
			packageOrderItem2.OriginalPrice = new Price(1125, 900);
			packageOrderItem2.ActualPrice = new Price(500, 400);

			lekmerOrderItem.PackageOrderItems = new Collection<IPackageOrderItem> { packageOrderItem1, packageOrderItem2 };

			lekmerOrderItem.CalculatePackageItemsPrice();

			Assert.AreEqual(lekmerOrderItem.ActualPrice.IncludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.PackagePrice.IncludingVat));
			Assert.AreEqual(lekmerOrderItem.ActualPrice.ExcludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.PackagePrice.ExcludingVat));

			Assert.AreEqual(600, packageOrderItem1.PackagePrice.IncludingVat);
			Assert.AreEqual(480, packageOrderItem1.PackagePrice.ExcludingVat);

			Assert.AreEqual(400, packageOrderItem2.PackagePrice.IncludingVat);
			Assert.AreEqual(320, packageOrderItem2.PackagePrice.ExcludingVat);
		}

		[Test]
		public void CalculatePackageItemsPrice_PackagePriceGreaterThenItemsSum_ItemPriceCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(2500, 2000);
			lekmerOrderItem.ActualPrice = new Price(2250, 1800);

			var packageOrderItem1 = new PackageOrderItem();
			packageOrderItem1.OriginalPrice = new Price(1375, 1100);
			packageOrderItem1.ActualPrice = new Price(750, 600);

			var packageOrderItem2 = new PackageOrderItem();
			packageOrderItem2.OriginalPrice = new Price(1125, 900);
			packageOrderItem2.ActualPrice = new Price(500, 400);

			lekmerOrderItem.PackageOrderItems = new Collection<IPackageOrderItem> { packageOrderItem1, packageOrderItem2 };

			lekmerOrderItem.CalculatePackageItemsPrice();

			Assert.AreEqual(lekmerOrderItem.ActualPrice.IncludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.PackagePrice.IncludingVat));
			Assert.AreEqual(lekmerOrderItem.ActualPrice.ExcludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.PackagePrice.ExcludingVat));

			Assert.AreEqual(1350, packageOrderItem1.PackagePrice.IncludingVat);
			Assert.AreEqual(1080, packageOrderItem1.PackagePrice.ExcludingVat);

			Assert.AreEqual(900, packageOrderItem2.PackagePrice.IncludingVat);
			Assert.AreEqual(720, packageOrderItem2.PackagePrice.ExcludingVat);
		}

		[Test]
		public void CalculatePackageItemsPrice_PackagePriceLessThenItemsSumAndReal_ItemPriceCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(797, 637.6m);
			lekmerOrderItem.ActualPrice = new Price(599, 479.2m);

			var packageOrderItem1 = new PackageOrderItem();
			packageOrderItem1.OriginalPrice = new Price(329, 263.2m);
			packageOrderItem1.ActualPrice = new Price(249, 199.2m);

			var packageOrderItem2 = new PackageOrderItem();
			packageOrderItem2.OriginalPrice = new Price(279, 223.2m);
			packageOrderItem2.ActualPrice = new Price(199, 159.2m);

			var packageOrderItem3 = new PackageOrderItem();
			packageOrderItem3.OriginalPrice = new Price(279, 223.2m);
			packageOrderItem3.ActualPrice = new Price(199, 159.2m);

			lekmerOrderItem.PackageOrderItems = new Collection<IPackageOrderItem> { packageOrderItem1, packageOrderItem2, packageOrderItem3 };

			lekmerOrderItem.CalculatePackageItemsPrice();

			Assert.AreEqual(lekmerOrderItem.ActualPrice.IncludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.PackagePrice.IncludingVat));
			Assert.AreEqual(lekmerOrderItem.ActualPrice.ExcludingVat, lekmerOrderItem.PackageOrderItems.Sum(packageOrderItem => packageOrderItem.PackagePrice.ExcludingVat));

			Assert.AreEqual(230.53m, packageOrderItem1.PackagePrice.IncludingVat);
			Assert.AreEqual(184.42m, packageOrderItem1.PackagePrice.ExcludingVat);

			Assert.AreEqual(184.24m, packageOrderItem2.PackagePrice.IncludingVat);
			Assert.AreEqual(147.39m, packageOrderItem2.PackagePrice.ExcludingVat);

			Assert.AreEqual(184.23m, packageOrderItem3.PackagePrice.IncludingVat);
			Assert.AreEqual(147.39m, packageOrderItem3.PackagePrice.ExcludingVat); //147.39m instead of 147.38m because we need to have the same sum over items in comparioson with package
		}


		[Test]
		public void GetOriginalVatAmount_OrderItemOriginalPriceIsSet_VatAmountCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(125, 100);

			decimal actualVatAmount = lekmerOrderItem.GetOriginalVatAmount();

			Assert.AreEqual(25, actualVatAmount);
		}
		
		[Test]
		public void GetOriginalVatPercent_OrderItemOriginalPriceIsSet_VatPercentCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(125, 100);

			decimal actualVatPercent = lekmerOrderItem.GetOriginalVatPercent();

			Assert.AreEqual(25, actualVatPercent);
		}


		[Test]
		public void RowDiscount_OrderItemOriginalAndActualPriceIsSet_DiscountCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(1250, 1000);
			lekmerOrderItem.ActualPrice = new Price(1150, 920);

			decimal discountExclVat = lekmerOrderItem.RowDiscount(false);
			decimal discountIncVat = lekmerOrderItem.RowDiscount(true);

			Assert.AreEqual(80, discountExclVat);
			Assert.AreEqual(100, discountIncVat);
		}
		
		[Test]
		public void RowDiscountPercent_OrderItemOriginalAndActualPriceIsSet_DiscountPercentCalculatedCorrectly()
		{
			var lekmerOrderItem = new LekmerOrderItem();
			lekmerOrderItem.OriginalPrice = new Price(1250, 1000);
			lekmerOrderItem.ActualPrice = new Price(1150, 920);

			double discountPercentExclVat = lekmerOrderItem.RowDiscountPercent(false);
			double discountPercentIncVat = lekmerOrderItem.RowDiscountPercent(true);

			Assert.AreEqual(8, discountPercentExclVat);
			Assert.AreEqual(8, discountPercentIncVat);
		}
	}
}
