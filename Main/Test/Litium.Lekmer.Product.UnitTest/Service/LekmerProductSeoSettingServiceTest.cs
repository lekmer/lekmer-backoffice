﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.UnitTest.Service
{
	[TestFixture]
	public class LekmerProductSeoSettingServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int PRODUCT_ID_1 = 109547;
		private static readonly int PRODUCT_ID_2 = 109548;
		private static readonly int PRODUCT_ID_3 = 109549;
		private static readonly int PRODUCT_ID_4 = 109550;

		private static MockRepository _mocker;
		private static IUserContext _userContext;
		private static ISystemUserFull _systemUserFull;
		private static ProductSeoSettingRepository _productSeoSettingRepository;
		private static LekmerProductSeoSettingService _lekmerProductSeoSettingService;
		private static LekmerProductSeoSettingSecureService _lekmerProductSeoSettingSecureService;
		private static IChannel _channel;
		private static ILanguage _language;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_userContext = _mocker.Stub<IUserContext>();
			_productSeoSettingRepository = _mocker.DynamicMock<ProductSeoSettingRepository>();
			_lekmerProductSeoSettingService = new LekmerProductSeoSettingService(_productSeoSettingRepository);

			_language = _mocker.Stub<ILanguage>();
			_language.Id = CHANNEL_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			ProductSeoSettingCache.Instance.Flush();
		}

		[Test]
		public void GetByProductId_SameId_CacheUsed()
		{
			Expect.Call(_productSeoSettingRepository.GetById(_channel, PRODUCT_ID_1)).Return(new ProductSeoSetting { Id = PRODUCT_ID_1 });

			_mocker.ReplayAll();

			Assert.AreEqual(PRODUCT_ID_1, _lekmerProductSeoSettingService.GetByProductId(_userContext, PRODUCT_ID_1).Id);
			Assert.AreEqual(PRODUCT_ID_1, _lekmerProductSeoSettingService.GetByProductId(_userContext, PRODUCT_ID_1).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetByProductId_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_productSeoSettingRepository.GetById(_channel, PRODUCT_ID_2)).Return(new ProductSeoSetting { Id = PRODUCT_ID_2 });
			Expect.Call(_productSeoSettingRepository.GetById(_channel, PRODUCT_ID_3)).Return(new ProductSeoSetting { Id = PRODUCT_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(PRODUCT_ID_2, _lekmerProductSeoSettingService.GetByProductId(_userContext, PRODUCT_ID_2).Id);
			Assert.AreEqual(PRODUCT_ID_3, _lekmerProductSeoSettingService.GetByProductId(_userContext, PRODUCT_ID_3).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetByProductId_ProductSeoSettingsUpdated_CacheRemoved()
		{
			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_lekmerProductSeoSettingSecureService = new LekmerProductSeoSettingSecureService(_productSeoSettingRepository);

			IProductSeoSetting productSeoSeoSetting = new ProductSeoSetting { Id = PRODUCT_ID_4 };

			Expect.Call(_productSeoSettingRepository.GetById(_channel, PRODUCT_ID_4)).Return(productSeoSeoSetting).Repeat.Twice();
			Expect.Call(() => _productSeoSettingRepository.Save(productSeoSeoSetting)).IgnoreArguments().Repeat.Once();

			_mocker.ReplayAll();

			Assert.AreEqual(PRODUCT_ID_4, _lekmerProductSeoSettingService.GetByProductId(_userContext, PRODUCT_ID_4).Id);
			_lekmerProductSeoSettingSecureService.Save(_systemUserFull, productSeoSeoSetting);
			Assert.AreEqual(PRODUCT_ID_4, _lekmerProductSeoSettingService.GetByProductId(_userContext, PRODUCT_ID_4).Id);

			_mocker.VerifyAll();
		}
	}
}