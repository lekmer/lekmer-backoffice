﻿using System;
using System.Web;
using Litium.Lekmer.Common;
using NUnit.Framework;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;
using Rhino.Mocks.Interfaces;
using Is = Rhino.Mocks.Constraints.Is;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class WishListCookieServiceTest
	{
		private MockRepository _mocker;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void CreateNewWishListKey_CreateNewKey_NotEmptyKeyCreated()
		{
			IWishListCookieService service = new WishListCookieService(null);

			Guid newWishListKey = service.CreateNewWishListKey();

			Assert.AreNotEqual(Guid.Empty, newWishListKey);
		}


		[Test]
		[ExpectedException(typeof(InvalidOperationException))]
		public void GetWishListKey_NullCookieService_ExceptionRaised()
		{
			IWishListCookieService service = new WishListCookieService(null);

			service.GetWishListKey();
		}

		[Test]
		public void GetWishListKey_CookieDoesNotExist_EmptyKeyReturned()
		{
			var cookieService = _mocker.DynamicMock<ICookieService>();
			Expect
				.Call(cookieService.GetRequestCookie("key"))
				.IgnoreArguments()
				.Return(null);

			_mocker.ReplayAll();

			IWishListCookieService service = new WishListCookieService(cookieService);

			Guid newWishListKey = service.GetWishListKey();

			Assert.AreEqual(Guid.Empty, newWishListKey, "Wishlist key");

			_mocker.VerifyAll();
		}

		[Test]
		public void GetWishListKey_CookieHasValidValue_ValidKeyReturned()
		{
			Guid key = Guid.NewGuid();

			var cookieService = _mocker.DynamicMock<ICookieService>();
			Expect
				.Call(cookieService.GetRequestCookie("key"))
				.IgnoreArguments()
				.Return(new HttpCookie("key", key.ToString()));

			_mocker.ReplayAll();

			IWishListCookieService service = new WishListCookieService(cookieService);

			Guid newWishListKey = service.GetWishListKey();

			Assert.AreEqual(key, newWishListKey, "Wishlist key");

			_mocker.VerifyAll();
		}

		[Test]
		public void GetWishListKey_CookieHasInvalidValue_EmptyKeyReturned()
		{
			var cookieService = _mocker.DynamicMock<ICookieService>();
			Expect
				.Call(cookieService.GetRequestCookie("key"))
				.IgnoreArguments()
				.Return(new HttpCookie("key", "invalid-value"));

			_mocker.ReplayAll();

			IWishListCookieService service = new WishListCookieService(cookieService);

			Guid newWishListKey = service.GetWishListKey();

			Assert.AreEqual(Guid.Empty, newWishListKey, "Wishlist key");

			_mocker.VerifyAll();
		}

		[Test]
		public void GetWishListKey_CookieHasInvalidValue_CookieRemoved()
		{
			var cookieService = _mocker.DynamicMock<ICookieService>();
			Expect
				.Call(cookieService.GetRequestCookie("key"))
				.IgnoreArguments()
				.Return(new HttpCookie("key", "invalid-value")).Repeat.Any();

			cookieService.SetResponseCookie(null);
			LastCall.IgnoreArguments();

			IWishListCookieService service = _mocker.PartialMock<WishListCookieService>(cookieService);
			Expect.Call(service.RemoveWishListKey).CallOriginalMethod(OriginalCallOptions.CreateExpectation);

			_mocker.ReplayAll();

			service.GetWishListKey();

			_mocker.VerifyAll();
		}


		[Test]
		[ExpectedException(typeof(InvalidOperationException))]
		public void SetWishListKey_NullCookieService_ExceptionRaised()
		{
			Guid key = Guid.NewGuid();

			IWishListCookieService service = new WishListCookieService(null);

			service.SetWishListKey(key);
		}

		[Test]
		public void SetWishListKey_ValidKey_CookieAdded()
		{
			Guid key = Guid.NewGuid();

			var cookieService = _mocker.DynamicMock<ICookieService>();
			cookieService.SetResponseCookie(null);
			LastCall
				.IgnoreArguments()
				.Constraints(Property.Value("Value", key.ToString()));

			_mocker.ReplayAll();

			IWishListCookieService service = new WishListCookieService(cookieService);

			string wishListKey = service.SetWishListKey(key);

			Assert.AreEqual(key.ToString(), wishListKey);

			_mocker.VerifyAll();
		}


		[Test]
		[ExpectedException(typeof(InvalidOperationException))]
		public void RemoveWishListKey_NullCookieService_ExceptionRaised()
		{
			IWishListCookieService service = new WishListCookieService(null);

			service.RemoveWishListKey();
		}

		[Test]
		public void RemoveWishListKey_MakeCall_CookieRemoved()
		{
			var cookieService = _mocker.DynamicMock<ICookieService>();
			Expect
				.Call(cookieService.GetRequestCookie("key"))
				.IgnoreArguments()
				.Return(new HttpCookie("key", "invalid-value")).Repeat.Any();

			cookieService.SetResponseCookie(null);
			LastCall
				.IgnoreArguments()
				.Constraints(Is.Matching<HttpCookie>(c => c.Value == Guid.Empty.ToString() && c.Expires < DateTime.Now));

			_mocker.ReplayAll();

			IWishListCookieService service = new WishListCookieService(cookieService);
			service.RemoveWishListKey();

			_mocker.VerifyAll();
		}
	}
}