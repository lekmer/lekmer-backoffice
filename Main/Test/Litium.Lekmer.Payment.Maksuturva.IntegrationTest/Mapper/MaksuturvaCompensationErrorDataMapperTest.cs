﻿using System.Data;
using Litium.Lekmer.Payment.Maksuturva.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Mapper
{
	[TestFixture]
	public class MaksuturvaCompensationErrorDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationErrorDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IMaksuturvaCompensationError>(_dataReader);

			Assert.IsInstanceOf<MaksuturvaCompensationErrorDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationErrorDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IMaksuturvaCompensationError>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IMaksuturvaCompensationError>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}