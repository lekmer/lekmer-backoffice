﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Service
{
	[TestFixture]
	public class MaksuturvaCompensationErrorServiceTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationErrorService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMaksuturvaCompensationErrorService>();

			Assert.IsInstanceOf<IMaksuturvaCompensationErrorService>(service);
			Assert.IsInstanceOf<MaksuturvaCompensationErrorService>(service);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationErrorService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMaksuturvaCompensationErrorService>();
			var service2 = IoC.Resolve<IMaksuturvaCompensationErrorService>();

			Assert.AreEqual(service1, service2);
		}
	}
}