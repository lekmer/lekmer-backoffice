﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Service
{
	[TestFixture]
	public class MaksuturvaPendingOrderServiceTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrderService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMaksuturvaPendingOrderService>();

			Assert.IsInstanceOf<IMaksuturvaPendingOrderService>(service);
			Assert.IsInstanceOf<MaksuturvaPendingOrderService>(service);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrderService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMaksuturvaPendingOrderService>();
			var service2 = IoC.Resolve<IMaksuturvaPendingOrderService>();

			Assert.AreEqual(service1, service2);
		}
	}
}