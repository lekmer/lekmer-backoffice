﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service.Search
{
	[TestFixture]
	public class BlockProductSearchResultServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchResultService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductSearchResultService>();

			Assert.IsInstanceOf<IBlockProductSearchResultService>(service);
			Assert.IsInstanceOf<BlockProductSearchResultService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchResultService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductSearchResultService>();
			var instance2 = IoC.Resolve<IBlockProductSearchResultService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}