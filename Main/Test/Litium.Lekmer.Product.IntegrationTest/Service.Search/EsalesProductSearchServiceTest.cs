﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service.Search
{
	[TestFixture]
	public class EsalesProductSearchServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductSearchService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesProductSearchService>();

			Assert.IsInstanceOf<IEsalesProductSearchService>(service);
			Assert.IsInstanceOf<EsalesProductSearchService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductSearchService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesProductSearchService>();
			var service2 = IoC.Resolve<IEsalesProductSearchService>();

			Assert.AreEqual(service1, service2);
		}
	}
}