﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service.Search
{
	[TestFixture]
	public class EsalesAutoCompleteServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesAutoCompleteService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesAutoCompleteService>();

			Assert.IsInstanceOf<IEsalesAutoCompleteService>(service);
			Assert.IsInstanceOf<EsalesAutoCompleteService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesAutoCompleteService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesAutoCompleteService>();
			var service2 = IoC.Resolve<IEsalesAutoCompleteService>();

			Assert.AreEqual(service1, service2);
		}
	}
}