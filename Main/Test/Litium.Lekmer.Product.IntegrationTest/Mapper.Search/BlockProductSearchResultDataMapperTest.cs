﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper.Search
{
	[TestFixture]
	public class BlockProductSearchResultDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchResult_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IBlockProductSearchResult>(_dataReader);

			Assert.IsInstanceOf<BlockProductSearchResultDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchResult_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IBlockProductSearchResult>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IBlockProductSearchResult>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}