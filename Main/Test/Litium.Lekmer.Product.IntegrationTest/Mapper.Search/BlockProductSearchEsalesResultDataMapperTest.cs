﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockProductSearchEsalesResultDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResult_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IBlockProductSearchEsalesResult>(_dataReader);

			Assert.IsInstanceOf<BlockProductSearchEsalesResultDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResult_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IBlockProductSearchEsalesResult>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IBlockProductSearchEsalesResult>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}