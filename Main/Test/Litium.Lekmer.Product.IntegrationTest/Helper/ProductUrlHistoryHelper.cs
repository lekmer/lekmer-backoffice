﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Helper
{
	public class ProductUrlHistoryHelper
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		public static readonly int PRODUCT_COUNT = 20;
		private static readonly string CHANNEL_CommonName = "Sweden";

		private MockRepository _mocker;

		public ProductUrlHistoryHelper()
		{
			_mocker = new MockRepository();
		}

		public ProductCollection GetProducts()
		{
			ILanguage language = _mocker.Stub<ILanguage>();
			language.Id = CHANNEL_ID;

			ICurrency currency = _mocker.Stub<ICurrency>();
			currency.Id = CURRENCY_ID;

			IChannel channel = _mocker.Stub<IChannel>();
			channel.Id = CHANNEL_ID;
			channel.CommonName = CHANNEL_CommonName;
			channel.Language = language;
			channel.Currency = currency;

			IUserContext userContext = _mocker.Stub<IUserContext>();
			userContext.Channel = channel;

			ILekmerProductService lekmerProductService = IoC.Resolve<ILekmerProductService>();
			ProductCollection productCollection = lekmerProductService.GetAll(userContext, 1, PRODUCT_COUNT + 1);

			return productCollection;
		}
	}
}