﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductRelationListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRelationListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductRelationListSecureService>();

			Assert.IsInstanceOf<IBlockProductRelationListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRelationListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductRelationListSecureService>();
			var instance2 = IoC.Resolve<IBlockProductRelationListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}