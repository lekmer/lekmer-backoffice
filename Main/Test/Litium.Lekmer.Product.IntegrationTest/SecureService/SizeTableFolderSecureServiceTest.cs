﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class SizeTableFolderSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableFolderSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISizeTableFolderSecureService>();

			Assert.IsInstanceOf<ISizeTableFolderSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableFolderSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableFolderSecureService>();
			var instance2 = IoC.Resolve<ISizeTableFolderSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}