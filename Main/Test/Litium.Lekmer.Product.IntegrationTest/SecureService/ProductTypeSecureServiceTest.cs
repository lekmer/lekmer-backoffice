﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductTypeSecureService>();

			Assert.IsInstanceOf<IProductTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductTypeSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductTypeSecureService>();
			var instance2 = IoC.Resolve<IProductTypeSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}