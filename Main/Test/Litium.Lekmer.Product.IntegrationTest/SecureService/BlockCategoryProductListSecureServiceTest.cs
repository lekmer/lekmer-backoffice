﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockCategoryProductListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockCategoryProductListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockCategoryProductListSecureService>();

			Assert.IsInstanceOf<IBlockCategoryProductListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockCategoryProductListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockCategoryProductListSecureService>();
			var instance2 = IoC.Resolve<IBlockCategoryProductListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}