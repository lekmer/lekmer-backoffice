﻿using System.Transactions;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductChangesSecureServiceTest
	{
		private MockRepository _mocker;
		private ISystemUserFull _systemUserFull;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_systemUserFull.UserName = "Unit tests";
		}

		[Test]
		[Category("IoC")]
		public void ProductChangesSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IProductChangesSecureService>();

			Assert.IsInstanceOf<IProductChangesSecureService>(secureService);
			Assert.IsInstanceOf<ProductChangesSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void ProductChangesSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IProductChangesSecureService>();
			var secureService2 = IoC.Resolve<IProductChangesSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}

		[Test]
		[Category("Database.Save")]
		public void Insert_CreateNewItemAndSave_ItemSaved()
		{
			var service = IoC.Resolve<IProductChangesSecureService>();
			var productChangeEvent = service.Create(_systemUserFull, 123456);

			using (var transactionScope = new TransactionScope())
			{
				productChangeEvent.Id = service.Insert(productChangeEvent);
				transactionScope.Dispose();
			}

			Assert.IsTrue(productChangeEvent.Id > 0);
		}
	}
}