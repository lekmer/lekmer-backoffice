﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductRegistryProductSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryProductSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductRegistryProductSecureService>();

			Assert.IsInstanceOf<IProductRegistryProductSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryProductSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistryProductSecureService>();
			var instance2 = IoC.Resolve<IProductRegistryProductSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}