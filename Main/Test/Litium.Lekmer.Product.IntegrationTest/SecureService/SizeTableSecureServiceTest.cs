﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class SizeTableSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISizeTableSecureService>();

			Assert.IsInstanceOf<ISizeTableSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableSecureService>();
			var instance2 = IoC.Resolve<ISizeTableSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}