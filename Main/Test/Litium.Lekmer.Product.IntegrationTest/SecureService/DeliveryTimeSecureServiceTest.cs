﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class DeliveryTimeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void DeliveryTimeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IDeliveryTimeSecureService>();

			Assert.IsInstanceOf<IDeliveryTimeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void DeliveryTimeSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IDeliveryTimeSecureService>();
			var instance2 = IoC.Resolve<IDeliveryTimeSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}