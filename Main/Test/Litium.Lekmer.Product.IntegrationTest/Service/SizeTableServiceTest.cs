﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class SizeTableServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISizeTableService>();

			Assert.IsInstanceOf<ISizeTableService>(service);
			Assert.IsInstanceOf<SizeTableService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISizeTableService>();
			var service2 = IoC.Resolve<ISizeTableService>();

			Assert.AreEqual(service1, service2);
		}
	}
}