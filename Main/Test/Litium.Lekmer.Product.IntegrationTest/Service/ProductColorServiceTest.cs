﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductColorServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductColorService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductColorService>();

			Assert.IsInstanceOf<IProductColorService>(service);
			Assert.IsInstanceOf<ProductColorService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductColorService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductColorService>();
			var service2 = IoC.Resolve<IProductColorService>();

			Assert.AreEqual(service1, service2);
		}
	}
}