﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class BlockPackageProductListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockPackageProductListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockPackageProductListService>();

			Assert.IsInstanceOf<IBlockPackageProductListService>(service);
			Assert.IsInstanceOf<BlockPackageProductListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockPackageProductListService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockPackageProductListService>();
			var service2 = IoC.Resolve<IBlockPackageProductListService>();

			Assert.AreEqual(service1, service2);
		}
	}
}