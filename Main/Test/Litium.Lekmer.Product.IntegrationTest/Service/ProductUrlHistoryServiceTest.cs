﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product.IntegrationTest.Helper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductUrlHistoryServiceTest
	{
		private static readonly int MAX_COUNT = 5;
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";
		private static readonly string TEST_URL_TITLE_1 = "Test URL Title1_";
		private static readonly string TEST_URL_TITLE_2 = "Test URL Title2_";
		private static readonly string TEST_URL_TITLE_3 = "Test URL Title3_";
		private static readonly string TEST_URL_TITLE_4 = "Test URL Title4_";
		private static readonly string TEST_URL_TITLE_5 = "Test URL Title5_";

		private MockRepository _mocker;
		private IUserContext _userContext;
		private ISystemUserFull _systemUserFull;
		private IPrivilege _privilege;
		private IChannel _channel;
		private ILanguage _language;

		private IProductUrlHistorySecureService _productUrlHistorySecureService;
		private IProductUrlHistoryService _productUrlHistoryService;

		private ProductUrlHistoryHelper _productUrlHistoryHelper;
		private ProductCollection _productCollection;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_privilege = _mocker.Stub<IPrivilege>();
			_privilege.CommonName = PrivilegeConstant.AssortmentProduct;

			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_systemUserFull.Privileges = new Collection<IPrivilege>{_privilege};
			
			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			_productUrlHistorySecureService = IoC.Resolve<IProductUrlHistorySecureService>();
			_productUrlHistoryService = IoC.Resolve<IProductUrlHistoryService>();

			_productUrlHistoryHelper = new ProductUrlHistoryHelper();
			_productCollection = _productUrlHistoryHelper.GetProducts();
		}

		[Test]
		[Category("IoC")]
		public void ProductUrlHistoryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductUrlHistoryService>();

			Assert.IsInstanceOf<IProductUrlHistoryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductUrlHistoryService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductUrlHistoryService>();
			var service2 = IoC.Resolve<IProductUrlHistoryService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void ProductUrlHistoryService_GetAllByUrlTitle_ReturnedList()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						_productCollection[i].Id, 
						LANGUAGE_ID,
						TEST_URL_TITLE_1 + i,
						(int) HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Get ProductUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_1 + i);

					Assert.AreEqual(1, productUrlHistoryCollection.Count);
					Assert.AreEqual(_productCollection[i].Id, productUrlHistoryCollection[0].ProductId);
					Assert.AreEqual(LANGUAGE_ID, productUrlHistoryCollection[0].LanguageId);
					Assert.AreEqual(TEST_URL_TITLE_1 + i, productUrlHistoryCollection[0].UrlTitleOld);
					Assert.AreEqual((int)HistoryLifeIntervalType.Immortal, productUrlHistoryCollection[0].TypeId);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_GetAllByLanguage_ReturnedList()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						_productCollection[i].Id,
						LANGUAGE_ID,
						TEST_URL_TITLE_2 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Get ProductUrlHistory by Language.
				Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistoryService.GetAllByLanguage(_userContext);

				Assert.IsTrue(productUrlHistoryCollection.Count >= 11);

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_Update_UsageAmountUpdated()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						_productCollection[i].Id,
						LANGUAGE_ID,
						TEST_URL_TITLE_3 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				List<int> productUrlHistoryIds = _productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Update ProductUrlHistory collection.
				foreach (int productUrlHistoryId in productUrlHistoryIds)
				{
					_productUrlHistoryService.Update(productUrlHistoryId);
				}

				// Get ProductUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_3 + i);

					Assert.AreEqual(1, productUrlHistoryCollection.Count, "Count");
					Assert.AreEqual(1, productUrlHistoryCollection[0].UsageAmount, "UsageAmount_" + i);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_CleanUp_RemoveRedundant()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				int productId = _productCollection[0].Id;

				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						productId,
						LANGUAGE_ID,
						TEST_URL_TITLE_4 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Remove redundant
				_productUrlHistoryService.CleanUp(MAX_COUNT);

				// Get ProductUrlHistory by Product.
				IEnumerable<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistorySecureService.GetAllByProduct(productId)
					.Where(h => h.LanguageId == LANGUAGE_ID);

				Assert.AreEqual(MAX_COUNT, productUrlHistoryCollection.Count(), "Returned count");

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_DeleteExpiredItems_RemoveExpired()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						_productCollection[i].Id,
						LANGUAGE_ID,
						TEST_URL_TITLE_5 + i,
						(int)HistoryLifeIntervalType.Expired));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Remove redundant
				_productUrlHistoryService.DeleteExpiredItems();

				// Get ProductUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_5 + i);

					Assert.AreEqual(0, productUrlHistoryCollection.Count);
				}

				transactionScope.Dispose();
			}
		}
	}
}