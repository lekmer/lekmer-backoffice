﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerProductServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;
		private ICurrency _currency;

		private ILekmerProductService _lekmerProductService;
		private LekmerProductRepository _lekmerProductRepository;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_currency = _mocker.Stub<ICurrency>();
			_currency.Id = CURRENCY_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;
			_channel.Currency = _currency;

			_userContext.Channel = _channel;

			_lekmerProductService = IoC.Resolve<ILekmerProductService>();
			_lekmerProductRepository = (LekmerProductRepository) IoC.Resolve<ProductRepository>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ILekmerProductService>();

			Assert.IsInstanceOf<ILekmerProductService>(service);
			Assert.IsInstanceOf<LekmerProductService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ILekmerProductService>();
			var service2 = IoC.Resolve<ILekmerProductService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Get")]
		public void PopulateProducts_ExecuteWithIdList_ProductListReturned()
		{
			ProductIdCollection productIdCollection = _lekmerProductRepository.GetIdAll(_channel, null, 1, 100);

			ProductCollection productCollection = _lekmerProductService.PopulateProducts(_userContext, productIdCollection);

			Assert.AreEqual(productIdCollection.Count, productCollection.Count, "Count");
			Assert.AreEqual(productIdCollection.TotalCount, productCollection.TotalCount, "TotalCount");
		}

		[Test]
		[Category("Database.Get")]
		public void PopulateViewProducts_ExecuteWithIdList_ProductListReturned()
		{
			ProductIdCollection productIdCollection = _lekmerProductRepository.GetIdAll(_channel, null, 1, 100);

			ProductCollection productCollection = _lekmerProductService.PopulateViewProducts(_userContext, productIdCollection);

			Assert.AreEqual(productIdCollection.Count, productCollection.Count, "Count");
			Assert.AreEqual(productIdCollection.TotalCount, productCollection.TotalCount, "TotalCount");
		}
	}
}