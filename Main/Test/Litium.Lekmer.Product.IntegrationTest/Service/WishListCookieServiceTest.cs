﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class WishListCookieServiceTest
	{
		[Test]
		[Category("IoC")]
		public void WishListCookieService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IWishListCookieService>();

			Assert.IsInstanceOf<IWishListCookieService>(service);
			Assert.IsInstanceOf<WishListCookieService>(service);
		}

		[Test]
		[Category("IoC")]
		public void WishListCookieService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IWishListCookieService>();
			var service2 = IoC.Resolve<IWishListCookieService>();

			Assert.AreEqual(service1, service2);
		}
	}
}