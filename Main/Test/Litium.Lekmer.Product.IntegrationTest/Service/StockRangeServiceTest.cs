﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class StockRangeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void StockRangeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IStockRangeService>();

			Assert.IsInstanceOf<IStockRangeService>(service);
			Assert.IsInstanceOf<StockRangeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void StockRangeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IStockRangeService>();
			var service2 = IoC.Resolve<IStockRangeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}