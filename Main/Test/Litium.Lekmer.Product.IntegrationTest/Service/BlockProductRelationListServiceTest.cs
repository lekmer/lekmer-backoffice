﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductRelationListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRelationListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductRelationListService>();

			Assert.IsInstanceOf<IBlockProductRelationListService>(service);
			Assert.IsInstanceOf<BlockProductRelationListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRelationListService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockProductRelationListService>();
			var service2 = IoC.Resolve<IBlockProductRelationListService>();

			Assert.AreEqual(service1, service2);
		}
	}
}