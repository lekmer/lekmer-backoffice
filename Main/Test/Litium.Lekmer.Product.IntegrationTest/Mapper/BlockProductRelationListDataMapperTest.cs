﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockProductRelationListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRelationListDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IBlockProductRelationList>(_dataReader);

			Assert.IsInstanceOf<BlockProductRelationListDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRelationListDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IBlockProductRelationList>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IBlockProductRelationList>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}