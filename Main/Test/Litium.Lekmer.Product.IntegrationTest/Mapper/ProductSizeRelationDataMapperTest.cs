﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class ProductSizeRelationDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ProductSizeRelation_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IProductSizeRelation>(_dataReader);

			Assert.IsInstanceOf<ProductSizeRelationDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void ProductSizeRelation_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IProductSizeRelation>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IProductSizeRelation>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}