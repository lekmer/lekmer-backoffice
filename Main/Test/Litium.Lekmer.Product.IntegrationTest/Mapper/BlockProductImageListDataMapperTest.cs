﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockProductImageListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductImageListDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IBlockProductImageList>(_dataReader);

			Assert.IsInstanceOf<BlockProductImageListDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductImageListDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IBlockProductImageList>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IBlockProductImageList>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}