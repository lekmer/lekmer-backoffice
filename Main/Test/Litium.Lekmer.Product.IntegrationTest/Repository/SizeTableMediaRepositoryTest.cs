﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class SizeTableMediaRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SizeTableMediaRepository>();

			Assert.IsInstanceOf<SizeTableMediaRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SizeTableMediaRepository>();
			var instance2 = IoC.Resolve<SizeTableMediaRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}