﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductRegistryRestrictionBrandRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionBrandRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductRegistryRestrictionBrandRepository>();

			Assert.IsInstanceOf<ProductRegistryRestrictionBrandRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionBrandRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductRegistryRestrictionBrandRepository>();
			var repository2 = IoC.Resolve<ProductRegistryRestrictionBrandRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}