﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerRelationListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerRelationListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RelationListRepository>();

			Assert.IsInstanceOf<RelationListRepository>(repository);
			Assert.IsInstanceOf<LekmerRelationListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerRelationListRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RelationListRepository>();
			var instance2 = IoC.Resolve<RelationListRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}