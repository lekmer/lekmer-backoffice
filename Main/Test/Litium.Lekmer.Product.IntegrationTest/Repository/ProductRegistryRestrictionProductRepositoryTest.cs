﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductRegistryRestrictionProductRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionProductRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductRegistryRestrictionProductRepository>();

			Assert.IsInstanceOf<ProductRegistryRestrictionProductRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionProductRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductRegistryRestrictionProductRepository>();
			var repository2 = IoC.Resolve<ProductRegistryRestrictionProductRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}