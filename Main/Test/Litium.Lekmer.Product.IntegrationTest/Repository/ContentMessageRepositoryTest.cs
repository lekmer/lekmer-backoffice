﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ContentMessageRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ContentMessageRepository>();

			Assert.IsInstanceOf<ContentMessageRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ContentMessageRepository>();
			var instance2 = IoC.Resolve<ContentMessageRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}