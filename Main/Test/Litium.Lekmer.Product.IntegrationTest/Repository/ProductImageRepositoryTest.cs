﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductImageRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductImageRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductImageRepository>();

			Assert.IsInstanceOf<ProductImageRepository>(repository);
			Assert.IsInstanceOf<LekmerProductImageRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductImageRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductImageRepository>();
			var repository2 = IoC.Resolve<ProductImageRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}