﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductSizeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductSizeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductSizeRepository>();

			Assert.IsInstanceOf<ProductSizeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductSizeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductSizeRepository>();
			var repository2 = IoC.Resolve<ProductSizeRepository>();

			Assert.AreEqual(repository1, repository2);
		}

		[Test]
		public void ProductSizeRepository_GetAll_ResultCollection()
		{
			var repository = IoC.Resolve<ProductSizeRepository>();
			Collection<IProductSizeRelation> productSizeRelations = repository.GetAll();

			Assert.IsNotNull(productSizeRelations);
			Assert.IsTrue(productSizeRelations.Count > 0);
		}
	}
}