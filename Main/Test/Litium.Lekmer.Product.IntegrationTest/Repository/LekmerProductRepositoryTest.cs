﻿using System.Linq;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerProductRepositoryTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;

		private LekmerProductRepository _lekmerProductRepository;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			_lekmerProductRepository = (LekmerProductRepository)IoC.Resolve<ProductRepository>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<ProductRepository>();

			Assert.IsInstanceOf<ProductRepository>(service);
			Assert.IsInstanceOf<LekmerProductRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ProductRepository>();
			var service2 = IoC.Resolve<ProductRepository>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllByIdList_ExecuteWithIdList_ProductListReturned()
		{
			ProductIdCollection productIdCollection = _lekmerProductRepository.GetIdAll(_channel, null, 1, 100);

			ProductCollection productCollection = _lekmerProductRepository.GetAllByIdList(_channel.Id, null, Convert.ToStringIdentifierList(productIdCollection), Convert.DefaultListSeparator);

			Assert.AreEqual(productIdCollection.Count, productCollection.Count, "Count");
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllByIdList_ExecuteWithIdList_CompareResultsWithPaging()
		{
			ProductIdCollection productIdCollection = _lekmerProductRepository.GetIdAll(_channel, null, 1, 100);

			ProductCollection productCollection = _lekmerProductRepository.GetAllByIdList(_channel.Id, null, Convert.ToStringIdentifierList(productIdCollection), Convert.DefaultListSeparator);

			ProductCollection productCollectionPaging = _lekmerProductRepository.GetAllByIdList(_channel.Id, null, Convert.ToStringIdentifierList(productIdCollection), Convert.DefaultListSeparator, 1, 100);

			Assert.AreEqual(productIdCollection.Count, productCollection.Count, "Count");
			Assert.AreEqual(productIdCollection.Count, productCollectionPaging.Count, "Count");

			var productDictionary = productCollection.ToDictionary(p => p.Id);
			var productPagingDictionary = productCollectionPaging.ToDictionary(p => p.Id);

			for (int i = 0; i < 100; i++)
			{
				Assert.IsTrue(productDictionary.ContainsKey(productIdCollection[i]), "Product.Id");
				Assert.IsTrue(productPagingDictionary.ContainsKey(productIdCollection[i]), "Product.Id");
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllViewByIdList_ExecuteWithIdList_ProductListReturned()
		{
			ProductIdCollection productIdCollection = _lekmerProductRepository.GetIdAll(_channel, null, 1, 100);

			LekmerProductViewCollection productViewCollection = _lekmerProductRepository.GetAllViewByIdList(_channel.Id, null, Convert.ToStringIdentifierList(productIdCollection), Convert.DefaultListSeparator);

			Assert.AreEqual(productIdCollection.Count, productViewCollection.Count, "Count");
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllViewByIdList_ExecuteWithIdList_CompareResultsWithPaging()
		{
			ProductIdCollection productIdCollection = _lekmerProductRepository.GetIdAll(_channel, null, 1, 100);

			LekmerProductViewCollection productViewCollection = _lekmerProductRepository.GetAllViewByIdList(_channel.Id, null, Convert.ToStringIdentifierList(productIdCollection), Convert.DefaultListSeparator);

			LekmerProductViewCollection productViewCollectionPaging = _lekmerProductRepository.GetAllViewByIdList(_channel.Id, null, Convert.ToStringIdentifierList(productIdCollection), Convert.DefaultListSeparator, 1, 100);

			Assert.AreEqual(productIdCollection.Count, productViewCollection.Count, "Count");
			Assert.AreEqual(productIdCollection.Count, productViewCollectionPaging.Count, "Count");

			var productDictionary = productViewCollection.ToDictionary(p => p.Id);
			var productPagingDictionary = productViewCollectionPaging.ToDictionary(p => p.Id);

			for (int i = 0; i < 100; i++)
			{
				Assert.IsTrue(productDictionary.ContainsKey(productIdCollection[i]), "Product.Id");
				Assert.IsTrue(productPagingDictionary.ContainsKey(productIdCollection[i]), "Product.Id");
			}
		}
	}
}