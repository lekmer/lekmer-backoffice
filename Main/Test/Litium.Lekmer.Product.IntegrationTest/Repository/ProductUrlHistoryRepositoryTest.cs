﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductUrlHistoryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductUrlHistoryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductUrlHistoryRepository>();

			Assert.IsInstanceOf<ProductUrlHistoryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductUrlHistoryRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductUrlHistoryRepository>();
			var repository2 = IoC.Resolve<ProductUrlHistoryRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}