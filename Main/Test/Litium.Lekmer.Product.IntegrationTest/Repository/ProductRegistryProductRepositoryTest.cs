﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductRegistryProductRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryProductRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductRegistryProductRepository>();

			Assert.IsInstanceOf<ProductRegistryProductRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryProductRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductRegistryProductRepository>();
			var repository2 = IoC.Resolve<ProductRegistryProductRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}