﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository.Search
{
	[TestFixture]
	public class BlockProductSearchResultRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchResultRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockProductSearchResultRepository>();

			Assert.IsInstanceOf<BlockProductSearchResultRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchResultRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockProductSearchResultRepository>();
			var repository2 = IoC.Resolve<BlockProductSearchResultRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}