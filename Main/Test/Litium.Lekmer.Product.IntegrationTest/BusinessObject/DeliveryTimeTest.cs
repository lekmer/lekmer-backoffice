﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class DeliveryTimeTest
	{
		[Test]
		[Category("IoC")]
		public void DeliveryTime_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IDeliveryTime>();
			Assert.IsInstanceOf<IDeliveryTime>(instance);
		}

		[Test]
		[Category("IoC")]
		public void DeliveryTime_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IDeliveryTime>();
			var instance2 = IoC.Resolve<IDeliveryTime>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}