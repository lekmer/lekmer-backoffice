﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductRegistryRestrictionItemTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionItem_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductRegistryRestrictionItem>();

			Assert.IsInstanceOf<IProductRegistryRestrictionItem>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionItem_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistryRestrictionItem>();
			var instance2 = IoC.Resolve<IProductRegistryRestrictionItem>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}