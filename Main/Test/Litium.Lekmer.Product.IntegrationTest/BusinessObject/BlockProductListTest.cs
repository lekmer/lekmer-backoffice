﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockProductList>();
			Assert.IsInstanceOf<IBlockProductList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductList>();
			var instance2 = IoC.Resolve<IBlockProductList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}