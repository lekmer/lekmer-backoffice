﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ContentMessageFolderTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageFolder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IContentMessageFolder>();

			Assert.IsInstanceOf<IContentMessageFolder>(instance);
			Assert.IsInstanceOf<IContentMessageFolder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageFolder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IContentMessageFolder>();
			var instance2 = IoC.Resolve<IContentMessageFolder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}