﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CategoryTagGroupTest
	{
		[Test]
		[Category("IoC")]
		public void CategoryTagGroup_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICategoryTagGroup>();

			Assert.IsInstanceOf<ICategoryTagGroup>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CategoryTagGroup_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICategoryTagGroup>();
			var instance2 = IoC.Resolve<ICategoryTagGroup>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}