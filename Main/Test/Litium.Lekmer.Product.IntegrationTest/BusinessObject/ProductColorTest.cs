﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductColorTest
	{
		[Test]
		[Category("IoC")]
		public void ProductColor_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductColor>();

			Assert.IsInstanceOf<IProductColor>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductColor_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductColor>();
			var instance2 = IoC.Resolve<IProductColor>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}