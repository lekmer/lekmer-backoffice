﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockCategoryProductListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockCategoryProductList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockCategoryProductList>();
			Assert.IsInstanceOf<IBlockCategoryProductList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockCategoryProductList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockCategoryProductList>();
			var instance2 = IoC.Resolve<IBlockCategoryProductList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}