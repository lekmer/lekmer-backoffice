﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableRowTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableRow_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableRow>();

			Assert.IsInstanceOf<ISizeTableRow>(instance);
			Assert.IsInstanceOf<ISizeTableRow>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRow_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableRow>();
			var instance2 = IoC.Resolve<ISizeTableRow>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}