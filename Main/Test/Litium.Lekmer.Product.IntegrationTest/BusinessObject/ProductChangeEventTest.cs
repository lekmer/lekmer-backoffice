﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductChangeEventTest
	{
		[Test]
		[Category("IoC")]
		public void ProductChangeEvent_Resolve_Resolved()
		{
			var productChangeEvent = IoC.Resolve<IProductChangeEvent>();

			Assert.IsInstanceOf<IProductChangeEvent>(productChangeEvent);
			Assert.IsInstanceOf<ProductChangeEvent>(productChangeEvent);
		}

		[Test]
		[Category("IoC")]
		public void ProductChangeEvent_ResolveTwice_DifferentObjects()
		{
			var event1 = IoC.Resolve<IProductChangeEvent>();
			var event2 = IoC.Resolve<IProductChangeEvent>();

			Assert.AreNotEqual(event1, event2);
		}
	}
}