﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductRelationListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRelationList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockProductRelationList>();
			Assert.IsInstanceOf<IBlockProductRelationList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRelationList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductRelationList>();
			var instance2 = IoC.Resolve<IBlockProductRelationList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}