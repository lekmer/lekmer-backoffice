﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableMediaRegistryTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistry_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableMediaRegistry>();

			Assert.IsInstanceOf<ISizeTableMediaRegistry>(instance);
			Assert.IsInstanceOf<ISizeTableMediaRegistry>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistry_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableMediaRegistry>();
			var instance2 = IoC.Resolve<ISizeTableMediaRegistry>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}