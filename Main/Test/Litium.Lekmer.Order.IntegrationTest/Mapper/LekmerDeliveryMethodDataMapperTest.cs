﻿using System.Data;
using Litium.Lekmer.Order.Mapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Mapper
{
	[TestFixture]
	public class LekmerDeliveryMethodDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IDeliveryMethod>(_dataReader);

			Assert.IsInstanceOf<LekmerDeliveryMethodDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IDeliveryMethod>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IDeliveryMethod>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}