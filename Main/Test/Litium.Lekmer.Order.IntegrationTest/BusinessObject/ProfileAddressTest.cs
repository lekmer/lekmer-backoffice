﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProfileAddressTest
	{
		[Test]
		[Category("IoC")]
		public void ProfileAddress_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProfileAddress>();

			Assert.IsInstanceOf<IProfileAddress>(instance);
			Assert.IsInstanceOf<ProfileAddress>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProfileAddress_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProfileAddress>();
			var instance2 = IoC.Resolve<IProfileAddress>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}