﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class DeliveryMethodWeightPriceTest
	{
		[Test]
		[Category("IoC")]
		public void DeliveryMethodWeightPrice_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IDeliveryMethodWeightPrice>();

			Assert.IsInstanceOf<IDeliveryMethodWeightPrice>(instance);
		}

		[Test]
		[Category("IoC")]
		public void DeliveryMethodWeightPrice_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IDeliveryMethodWeightPrice>();
			var instance2 = IoC.Resolve<IDeliveryMethodWeightPrice>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}