﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	public class LekmerOrderSearchCriteriaTest
	{
		[TestFixture]
		public class AgeIntervalTest
		{
			[Test]
			[System.ComponentModel.Category("IoC")]
			public void LekmerOrderSearchCriteria_Resolve_Resolved()
			{
				var lekmerOrderSearchCriteria = IoC.Resolve<ILekmerOrderSearchCriteria>();

				Assert.IsInstanceOf<ILekmerOrderSearchCriteria>(lekmerOrderSearchCriteria);
			}

			[Test]
			[System.ComponentModel.Category("IoC")]
			public void LekmerOrderSearchCriteria_ResolveTwice_DifferentObjects()
			{
				var lekmerOrderSearchCriteria1 = IoC.Resolve<ILekmerOrderSearchCriteria>();
				var lekmerOrderSearchCriteria2 = IoC.Resolve<ILekmerOrderSearchCriteria>();

				Assert.AreNotEqual(lekmerOrderSearchCriteria1, lekmerOrderSearchCriteria2);
			}
		}
	}
}