﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SavedCartTest
	{
		[Test]
		[Category("IoC")]
		public void SavedCart_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISavedCartObject>();
			Assert.IsInstanceOf<ISavedCartObject>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SavedCart_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISavedCartObject>();
			var instance2 = IoC.Resolve<ISavedCartObject>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}