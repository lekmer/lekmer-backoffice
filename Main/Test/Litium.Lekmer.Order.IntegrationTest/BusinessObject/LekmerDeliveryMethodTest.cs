﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerDeliveryMethodTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethod_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IDeliveryMethod>();

			Assert.IsInstanceOf<IDeliveryMethod>(instance);
			Assert.IsInstanceOf<ILekmerDeliveryMethod>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethod_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IDeliveryMethod>();
			var instance2 = IoC.Resolve<IDeliveryMethod>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}
