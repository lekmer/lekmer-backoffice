﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProfileCompanyInformationTest
	{
		[Test]
		[Category("IoC")]
		public void ProfileCompanyInformation_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProfileCompanyInformation>();

			Assert.IsInstanceOf<IProfileCompanyInformation>(instance);
			Assert.IsInstanceOf<ProfileCompanyInformation>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProfileCompanyInformation_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProfileCompanyInformation>();
			var instance2 = IoC.Resolve<IProfileCompanyInformation>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}