﻿using Litium.Framework.Messaging;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Messaging
{
	[TestFixture]
	public class OrderRejectMessengerTest
	{
		[Test]
		[Category("IoC")]
		public void OrderRejectMessenger_Resolve_Resolved()
		{
			var messenger = IoC.Resolve<IMessenger<IOrderFull>>("OrderRejectMessenger");

			Assert.IsInstanceOf<OrderRejectMessenger>(messenger);
		}

		[Test]
		[Category("IoC")]
		public void OrderRejectMessenger_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IMessenger<IOrderFull>>("OrderRejectMessenger");
			var instance2 = IoC.Resolve<IMessenger<IOrderFull>>("OrderRejectMessenger");

			Assert.AreEqual(instance1, instance2);
		}
	}
}