﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class OrderVoucherInfoSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderVoucherInfoSecureService>();

			Assert.IsInstanceOf<IOrderVoucherInfoSecureService>(service);
			Assert.IsInstanceOf<OrderVoucherInfoSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderVoucherInfoSecureService>();
			var service2 = IoC.Resolve<IOrderVoucherInfoSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}