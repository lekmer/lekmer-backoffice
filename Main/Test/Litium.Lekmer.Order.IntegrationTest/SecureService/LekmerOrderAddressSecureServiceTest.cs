﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerOrderAddressSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderAddressSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderAddressSecureService>();

			Assert.IsInstanceOf<IOrderAddressSecureService>(service);
			Assert.IsInstanceOf<OrderAddressSecureService>(service);
			Assert.IsInstanceOf<ILekmerOrderAddressSecureService>(service);
			Assert.IsInstanceOf<LekmerOrderAddressSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderAddressSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderAddressSecureService>();
			var service2 = IoC.Resolve<IOrderAddressSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}