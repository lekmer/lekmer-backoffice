﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderCommentRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderCommentRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderCommentRepository>();

			Assert.IsInstanceOf<OrderCommentRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderCommentRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderCommentRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<OrderCommentRepository>();
			var instance2 = IoC.Resolve<OrderCommentRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}