﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class SavedCartRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SavedCartRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SavedCartObjectRepository>();
			Assert.IsInstanceOf<SavedCartObjectRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SavedCartRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SavedCartObjectRepository>();
			var instance2 = IoC.Resolve<SavedCartObjectRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}