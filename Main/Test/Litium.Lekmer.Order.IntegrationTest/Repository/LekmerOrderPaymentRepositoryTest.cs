﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderPaymentRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderPaymentRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderPaymentRepository>();

			Assert.IsInstanceOf<OrderPaymentRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderPaymentRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderPaymentRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<OrderPaymentRepository>();
			var instance2 = IoC.Resolve<OrderPaymentRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}