﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class CartItemPackageElementRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemPackageElementRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartItemPackageElementRepository>();

			Assert.IsInstanceOf<CartItemPackageElementRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartItemPackageElementRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartItemPackageElementRepository>();
			var instance2 = IoC.Resolve<CartItemPackageElementRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}