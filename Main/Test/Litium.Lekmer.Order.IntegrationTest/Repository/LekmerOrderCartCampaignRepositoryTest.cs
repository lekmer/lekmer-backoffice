﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository.Campaign;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderCartCampaignRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderCartCampaignRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderCartCampaignRepository>();

			Assert.IsInstanceOf<OrderCartCampaignRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderCartCampaignRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderCartCampaignRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<OrderCartCampaignRepository>();
			var instance2 = IoC.Resolve<OrderCartCampaignRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}