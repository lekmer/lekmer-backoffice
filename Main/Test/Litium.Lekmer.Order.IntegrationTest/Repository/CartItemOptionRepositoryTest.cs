﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class CartItemOptionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemOptionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartItemOptionRepository>();

			Assert.IsInstanceOf<CartItemOptionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartItemOptionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartItemOptionRepository>();
			var instance2 = IoC.Resolve<CartItemOptionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}