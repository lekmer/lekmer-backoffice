﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerPaymentTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerPaymentTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<PaymentTypeRepository>();

			Assert.IsInstanceOf<PaymentTypeRepository>(repository);
			Assert.IsInstanceOf<LekmerPaymentTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerPaymentTypeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<PaymentTypeRepository>();
			var instance2 = IoC.Resolve<PaymentTypeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}