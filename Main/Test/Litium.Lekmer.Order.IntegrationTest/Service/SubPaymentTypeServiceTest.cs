﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class SubPaymentTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SubPaymentTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISubPaymentTypeService>();

			Assert.IsInstanceOf<ISubPaymentTypeService>(service);
			Assert.IsInstanceOf<SubPaymentTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SubPaymentTypeService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISubPaymentTypeService>();
			var instance2 = IoC.Resolve<ISubPaymentTypeService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}