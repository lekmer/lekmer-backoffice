﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class PackageOrderItemServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PackageOrderItemService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPackageOrderItemService>();

			Assert.IsInstanceOf<IPackageOrderItemService>(service);
			Assert.IsInstanceOf<PackageOrderItemService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItemService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IPackageOrderItemService>();
			var instance2 = IoC.Resolve<IPackageOrderItemService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}