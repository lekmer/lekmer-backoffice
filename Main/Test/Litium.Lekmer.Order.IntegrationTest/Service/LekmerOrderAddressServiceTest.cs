﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerOrderAddressServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderAddressService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderAddressService>();

			Assert.IsInstanceOf<IOrderAddressService>(service);
			Assert.IsInstanceOf<OrderAddressService>(service);
			Assert.IsInstanceOf<LekmerOrderAddressService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderAddressService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderAddressService>();
			var service2 = IoC.Resolve<IOrderAddressService>();

			Assert.AreEqual(service1, service2);
		}
	}
}