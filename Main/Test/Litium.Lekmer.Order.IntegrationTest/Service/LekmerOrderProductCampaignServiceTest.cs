﻿using Litium.Lekmer.Order.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Campaign;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerOrderProductCampaignServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderProductCampaignService_ResolveByBaseInterface_Resolved()
		{
			var service = IoC.Resolve<IOrderProductCampaignService>();

			Assert.IsInstanceOf<IOrderProductCampaignService>(service);
			Assert.IsInstanceOf<ILekmerOrderProductCampaignService>(service);
			Assert.IsInstanceOf<LekmerOrderProductCampaignService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderProductCampaignService_ResolveTwiceByBaseInterface_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderProductCampaignService>();
			var service2 = IoC.Resolve<IOrderProductCampaignService>();

			Assert.AreEqual(service1, service2);
		}
	}
}