using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GoogleFeed.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class GoogleFeedInfoTest
	{
		[Test]
		[Category("IoC")]
		public void GoogleFeedInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IGoogleFeedInfo>();
			Assert.IsInstanceOf<IGoogleFeedInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void GoogleFeedInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IGoogleFeedInfo>();
			var info2 = IoC.Resolve<IGoogleFeedInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}