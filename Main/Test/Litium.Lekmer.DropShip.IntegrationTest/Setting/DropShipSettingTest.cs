﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Setting
{
	[TestFixture]
	public class DropShipSettingTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<IDropShipSetting>();
			Assert.IsInstanceOf<IDropShipSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void DropShipSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<IDropShipSetting>();
			var setting2 = IoC.Resolve<IDropShipSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}