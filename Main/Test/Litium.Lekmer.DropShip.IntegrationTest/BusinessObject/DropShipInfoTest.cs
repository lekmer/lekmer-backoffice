using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class DropShipInfoTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IDropShipInfo>();
			Assert.IsInstanceOf<IDropShipInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void DropShipInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IDropShipInfo>();
			var info2 = IoC.Resolve<IDropShipInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}