﻿using Litium.Lekmer.Pacsoft.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Pacsoft.IntegrationTest.Setting
{
	[TestFixture]
	public class PacsoftExportSettingTest
	{
		[Test]
		[Category("IoC")]
		public void PacsoftExportSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<IPacsoftExportSetting>();
			Assert.IsInstanceOf<IPacsoftExportSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void PacsoftExportSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<IPacsoftExportSetting>();
			var setting2 = IoC.Resolve<IPacsoftExportSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}