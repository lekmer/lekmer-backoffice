﻿using Litium.Lekmer.Pacsoft;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class PacsoftExportRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void PacsoftExportRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<PacsoftExportRepository>();
			Assert.IsInstanceOf<PacsoftExportRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void PacsoftExportRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<PacsoftExportRepository>();
			var instance2 = IoC.Resolve<PacsoftExportRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}