﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerMediaFormatServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerMediaFormatService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMediaFormatService>();

			Assert.IsInstanceOf<IMediaFormatService>(service);
			Assert.IsInstanceOf<LekmerMediaFormatService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerMediaFormatService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMediaFormatService>();
			var service2 = IoC.Resolve<IMediaFormatService>();

			Assert.AreEqual(service1, service2);
		}
	}
}