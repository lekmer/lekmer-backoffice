﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerImageSizeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerImageSizeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IImageSizeService>();

			Assert.IsInstanceOf<IImageSizeService>(service);
			Assert.IsInstanceOf<ILekmerImageSizeService>(service);
			Assert.IsInstanceOf<LekmerImageSizeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerImageSizeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IImageSizeService>();
			var service2 = IoC.Resolve<IImageSizeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}