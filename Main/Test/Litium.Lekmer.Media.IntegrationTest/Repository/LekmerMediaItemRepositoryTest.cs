﻿using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerMediaItemRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerMediaItemRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<MediaItemRepository>();

			Assert.IsInstanceOf<MediaItemRepository>(repository);
			Assert.IsInstanceOf<LekmerMediaItemRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerMediaItemRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<MediaItemRepository>();
			var repository2 = IoC.Resolve<MediaItemRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}