﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerMediaItemSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerMediaItemSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMediaItemSecureService>();

			Assert.IsInstanceOf<IMediaItemSecureService>(service);
			Assert.IsInstanceOf<ILekmerMediaItemSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerMediaItemSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMediaItemSecureService>();
			var service2 = IoC.Resolve<IMediaItemSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}