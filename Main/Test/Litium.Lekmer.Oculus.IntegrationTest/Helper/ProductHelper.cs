﻿using Litium.Lekmer.Product;
using Litium.Scensum.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Rhino.Mocks;


namespace Litium.Lekmer.Oculus.IntegrationTest
{
	public class ProductHelper
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";
		public static readonly int SIMILAR_PRODUCT_COUNT = 20;

		private MockRepository _mocker;

		public ProductHelper()
		{
			_mocker = new MockRepository();
		}

		public ProductCollection GetProducts()
		{
			ILanguage language = _mocker.Stub<ILanguage>();
			language.Id = CHANNEL_ID;

			ICurrency currency = _mocker.Stub<ICurrency>();
			currency.Id = CURRENCY_ID;

			IChannel channel = _mocker.Stub<IChannel>();
			channel.Id = CHANNEL_ID;
			channel.CommonName = CHANNEL_CommonName;
			channel.Language = language;
			channel.Currency = currency;

			IUserContext userContext = _mocker.Stub<IUserContext>();
			userContext.Channel = channel;

			ILekmerProductService lekmerProductService = IoC.Resolve<ILekmerProductService>();
			ProductCollection productCollection = lekmerProductService.GetAll(userContext, 1, SIMILAR_PRODUCT_COUNT + 1);

			return productCollection;
		}

		public IProductSimilar SaveSimilarProduct(ProductCollection productCollection)
		{
			IProductSimilar productSimilar = IoC.Resolve<IProductSimilar>();
			productSimilar.ProductErpId = productCollection[0].ErpId;

			for (int i = 1; i < productCollection.Count; i++)
			{
				ISimilarProductItem similarProductItem = IoC.Resolve<ISimilarProductItem>();

				similarProductItem.ProductErpId = productCollection[i].ErpId;
				similarProductItem.Score = i;

				productSimilar.SimilarProductCollection.Add(similarProductItem);
			}

			IProductSimilarSecureService productSimilarSecureService = IoC.Resolve<IProductSimilarSecureService>();
			IProductSimilar savedProductSimilar = productSimilarSecureService.Save(productSimilar);

			return savedProductSimilar;
		}
	}
}