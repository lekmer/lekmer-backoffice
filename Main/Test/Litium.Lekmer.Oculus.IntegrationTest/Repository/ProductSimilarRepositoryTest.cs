﻿using Litium.Lekmer.Oculus.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductSimilarRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductSimilarRepository_Resolve_Resolved()
		{
			ProductSimilarRepository productSimilarRepository = IoC.Resolve<ProductSimilarRepository>();

			Assert.IsInstanceOf<ProductSimilarRepository>(productSimilarRepository);
		}

		[Test]
		[Category("IoC")]
		public void ProductSimilarRepository_ResolveTwice_SameObjects()
		{
			ProductSimilarRepository productSimilarRepository1 = IoC.Resolve<ProductSimilarRepository>();
			ProductSimilarRepository productSimilarRepository2 = IoC.Resolve<ProductSimilarRepository>();

			Assert.AreEqual(productSimilarRepository1, productSimilarRepository2);
		}
	}
}