﻿using System.Transactions;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductSimilarSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductSimilarSecureService_Resolve_Resolved()
		{
			IProductSimilarSecureService productSimilarSecureService = IoC.Resolve<IProductSimilarSecureService>();

			Assert.IsInstanceOf<IProductSimilarSecureService>(productSimilarSecureService);
		}

		[Test]
		[Category("IoC")]
		public void ProductSimilarSecureService_ResolveTwice_SameObjects()
		{
			IProductSimilarSecureService productSimilarSecureService1 = IoC.Resolve<IProductSimilarSecureService>();
			IProductSimilarSecureService productSimilarSecureService2 = IoC.Resolve<IProductSimilarSecureService>();

			Assert.AreEqual(productSimilarSecureService1, productSimilarSecureService2);
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveProductSimilar_ProductSimilarSaved()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				ProductHelper productHelper = new ProductHelper();

				// Get product collection.
				ProductCollection productCollection = productHelper.GetProducts();

				int expectedProductId = productCollection[0].Id;

				// Save similar products.
				IProductSimilar savedProductSimilar = productHelper.SaveSimilarProduct(productCollection);

				Assert.AreEqual(expectedProductId, savedProductSimilar.ProductId);

				for (int i = 1; i < productCollection.Count; i++)
				{
					Assert.AreEqual(savedProductSimilar.SimilarProductCollection[i - 1].ProductErpId, productCollection[i].ErpId);
					Assert.AreEqual(savedProductSimilar.SimilarProductCollection[i - 1].Score, i);
				}

				transactionScope.Dispose();
			}
		}
	}
}