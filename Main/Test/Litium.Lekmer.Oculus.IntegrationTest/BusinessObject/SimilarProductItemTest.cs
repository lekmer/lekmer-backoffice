﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SimilarProductItemTest
	{
		[Test]
		[Category("IoC")]
		public void SimilarProductItem_Resolve_Resolved()
		{
			ISimilarProductItem similarProductItem = IoC.Resolve<ISimilarProductItem>();

			Assert.IsInstanceOf<ISimilarProductItem>(similarProductItem);
		}

		[Test]
		[Category("IoC")]
		public void SimilarProductItem_ResolveTwice_DifferentObjects()
		{
			ISimilarProductItem similarProductItem1 = IoC.Resolve<ISimilarProductItem>();
			ISimilarProductItem similarProductItem2 = IoC.Resolve<ISimilarProductItem>();

			Assert.AreNotEqual(similarProductItem1, similarProductItem2);
		}
	}
}