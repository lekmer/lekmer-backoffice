﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductSimilarListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSimilarList_Resolve_Resolved()
		{
			IBlockProductSimilarList blockProductSimilarList = IoC.Resolve<IBlockProductSimilarList>();

			Assert.IsInstanceOf<IBlockProductSimilarList>(blockProductSimilarList);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSimilarList_ResolveTwice_DifferentObjects()
		{
			IBlockProductSimilarList blockProductSimilarList1 = IoC.Resolve<IBlockProductSimilarList>();
			IBlockProductSimilarList blockProductSimilarList2 = IoC.Resolve<IBlockProductSimilarList>();

			Assert.AreNotEqual(blockProductSimilarList1, blockProductSimilarList2);
		}
	}
}