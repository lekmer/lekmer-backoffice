﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCustomerGroupServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerGroupService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ILekmerCustomerGroupService>();

			Assert.IsInstanceOf<ILekmerCustomerGroupService>(service);
			Assert.IsInstanceOf<LekmerCustomerGroupService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerGroupService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ILekmerCustomerGroupService>();
			var service2 = IoC.Resolve<ILekmerCustomerGroupService>();

			Assert.AreEqual(service1, service2);
		}
	}
}