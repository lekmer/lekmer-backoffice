﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.SecureService
{
	[TestFixture]
	public class GenderTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GenderTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IGenderTypeSecureService>();

			Assert.IsInstanceOf<IGenderTypeSecureService>(service);
			Assert.IsInstanceOf<GenderTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GenderTypeSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IGenderTypeSecureService>();
			var service2 = IoC.Resolve<IGenderTypeSecureService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllGenderTypes_AllReturned()
		{
			var service = IoC.Resolve<IGenderTypeSecureService>();

			Collection<IGenderType> genderTypes = service.GetAll();

			Assert.IsNotNull(genderTypes, "GenderTypeSecureService.GetAll() returns NULL");
			Assert.IsTrue(genderTypes.Count > 0, "GenderTypeSecureService.GetAll() returns empty collection");
		}
	}
}