﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerAddressSecureServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IAddressSecureService>();

			Assert.IsInstanceOf<IAddressSecureService>(service);
			Assert.IsInstanceOf<AddressSecureService>(service);
			Assert.IsInstanceOf<LekmerAddressSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IAddressSecureService>();
			var service2 = IoC.Resolve<IAddressSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}