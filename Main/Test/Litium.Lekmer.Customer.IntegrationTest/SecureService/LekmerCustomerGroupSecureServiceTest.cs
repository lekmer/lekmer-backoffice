﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCustomerGroupSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerGroupSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICustomerGroupSecureService>();

			Assert.IsInstanceOf<ICustomerGroupSecureService>(service);
			Assert.IsInstanceOf<CustomerGroupSecureService>(service);
			Assert.IsInstanceOf<LekmerCustomerGroupSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerGroupSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICustomerGroupSecureService>();
			var service2 = IoC.Resolve<ICustomerGroupSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}