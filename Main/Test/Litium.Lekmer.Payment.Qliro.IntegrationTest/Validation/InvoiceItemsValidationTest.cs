﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class InvoiceItemsValidationTest
	{
		[Test]
		[Category("IoC")]
		public void InvoiceItemsValidation_Resolve_Resolved()
		{
			var service = IoC.Resolve<IInvoiceItemsValidation>();

			Assert.IsInstanceOf<IInvoiceItemsValidation>(service);
			Assert.IsInstanceOf<InvoiceItemsValidation>(service);
		}

		[Test]
		[Category("IoC")]
		public void InvoiceItemsValidation_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IInvoiceItemsValidation>();
			var service2 = IoC.Resolve<IInvoiceItemsValidation>();

			Assert.AreEqual(service1, service2);
		}
	}
}