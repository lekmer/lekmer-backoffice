﻿using Litium.Lekmer.Payment.Qliro.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Repository
{
	[TestFixture]
	public class QliroPendingOrderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPendingOrderRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<QliroPendingOrderRepository>();

			Assert.IsInstanceOf<QliroPendingOrderRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroPendingOrderRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<QliroPendingOrderRepository>();
			var service2 = IoC.Resolve<QliroPendingOrderRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}