﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class QliroPaymentTypeTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPaymentType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IQliroPaymentType>();

			Assert.IsInstanceOf<IQliroPaymentType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void QliroPaymentType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IQliroPaymentType>();
			var instance2 = IoC.Resolve<IQliroPaymentType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}