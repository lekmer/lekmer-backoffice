﻿using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.API
{
	[TestFixture]
	public class QliroClientTestInAction
	{
		private MockRepository _mocker;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void GetAddress_ValidCivicNumber_AddressReturned()
		{
			IChannel channel = CreateChannel();
			IQliroClient qliroClient = CreateQliroClient();

			_mocker.ReplayAll();

			qliroClient.Initialize(channel.CommonName);

			IQliroGetAddressResult qliroGetAddressResult = qliroClient.GetAddress(channel, "5702184218", "127.0.0.1", false); // OK

			_mocker.VerifyAll();

			Assert.IsNotNull(qliroGetAddressResult);

			Assert.AreEqual((int)QliroResponseCode.Ok, qliroGetAddressResult.ReturnCode, "Return code");

			Assert.IsNotNull(qliroGetAddressResult.QliroAddresses);
			Assert.IsTrue(qliroGetAddressResult.QliroAddresses.Count > 0);

			Assert.AreEqual("Mustafa", qliroGetAddressResult.QliroAddresses[0].FirstName);
			Assert.AreEqual("Merinen", qliroGetAddressResult.QliroAddresses[0].LastName);
			Assert.AreEqual("Kungshamra 21 Lgh 1006", qliroGetAddressResult.QliroAddresses[0].StreetAddress);
			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].StreetAddress2);
			Assert.AreEqual("Asmundtorp", qliroGetAddressResult.QliroAddresses[0].City);
			Assert.AreEqual("26175", qliroGetAddressResult.QliroAddresses[0].PostalCode);
		}

		[Test]
		public void GetAddress_CivicNumberHiddenAddress_EmptyAddressReturned()
		{
			IChannel channel = CreateChannel();
			IQliroClient qliroClient = CreateQliroClient();

			_mocker.ReplayAll();

			qliroClient.Initialize(channel.CommonName);

			IQliroGetAddressResult qliroGetAddressResult = qliroClient.GetAddress(channel, "5612267293", "127.0.0.1", false); // Hidden address info

			_mocker.VerifyAll();

			Assert.IsNotNull(qliroGetAddressResult);

			Assert.AreEqual((int)QliroResponseCode.Ok, qliroGetAddressResult.ReturnCode, "Return code");

			Assert.IsNotNull(qliroGetAddressResult.QliroAddresses);
			Assert.IsTrue(qliroGetAddressResult.QliroAddresses.Count > 0);

			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].FirstName);
			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].LastName);
			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].StreetAddress);
			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].StreetAddress2);
			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].City);
			Assert.AreEqual("", qliroGetAddressResult.QliroAddresses[0].PostalCode);
		}

		[Test]
		public void GetAddress_CompanyNumberCareOfAddress_AddressReturned()
		{
			IChannel channel = CreateChannel();
			IQliroClient qliroClient = CreateQliroClient();

			_mocker.ReplayAll();

			qliroClient.Initialize(channel.CommonName);

			IQliroGetAddressResult qliroGetAddressResult = qliroClient.GetAddress(channel, "5810116490", "127.0.0.1", false); // Care of address

			_mocker.VerifyAll();

			Assert.IsNotNull(qliroGetAddressResult);

			Assert.AreEqual((int)QliroResponseCode.Ok, qliroGetAddressResult.ReturnCode, "Return code");

			Assert.IsNotNull(qliroGetAddressResult.QliroAddresses);
			Assert.IsTrue(qliroGetAddressResult.QliroAddresses.Count > 0);

			Assert.AreEqual("Lars", qliroGetAddressResult.QliroAddresses[0].FirstName);
			Assert.AreEqual("Gierlach-Dudas", qliroGetAddressResult.QliroAddresses[0].LastName);
			Assert.AreEqual("Norra Niemisel 323", qliroGetAddressResult.QliroAddresses[0].StreetAddress);
			Assert.AreEqual("c/o Andersson  Strandlyckan", qliroGetAddressResult.QliroAddresses[0].StreetAddress2);
			Assert.AreEqual("Rävlanda", qliroGetAddressResult.QliroAddresses[0].City);
			Assert.AreEqual("43095", qliroGetAddressResult.QliroAddresses[0].PostalCode);
		}

		[Test]
		public void GetPaymentTypes_ValidChannel_PaymentTypesReturned()
		{
			IChannel channel = CreateChannel();
			IQliroClient qliroClient = CreateQliroClient();

			_mocker.ReplayAll();

			qliroClient.Initialize(channel.CommonName);

			IQliroGetPaymentTypesResult qliroGetPaymentTypesResult = qliroClient.GetPaymentTypes(channel);

			_mocker.VerifyAll();

			Assert.IsNotNull(qliroGetPaymentTypesResult);

			Assert.AreEqual((int)QliroResponseCode.Ok, qliroGetPaymentTypesResult.ReturnCode, "Return code");

			Assert.IsNotNull(qliroGetPaymentTypesResult.PaymentTypes);
			Assert.IsTrue(qliroGetPaymentTypesResult.PaymentTypes.Count > 0);
		}

		public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			if (sslPolicyErrors == SslPolicyErrors.None)
			{
				return true;
			}

			return true;
		}

		private IQliroClient CreateQliroClient()
		{
			var qliroClient = new QliroClient(CreateQliroApi());
			qliroClient.QliroSetting = CreateQliroSetting();
			return qliroClient;
		}

		private IQliroApi CreateQliroApi()
		{
			var qliroApi = new QliroApi(CreateQliroTransactionService());
			qliroApi.QliroSetting = CreateQliroSetting();
			return qliroApi;
		}

		private IQliroSetting CreateQliroSetting()
		{
			var qliroSetting = _mocker.Stub<IQliroSetting>();

			qliroSetting.Expect(ks => ks.Url).Return("https://api.int.qit.nu/checkout.asmx").Repeat.Any();
			qliroSetting.Expect(ks => ks.UserName).Return("lekmer_intsystem").Repeat.Any();
			qliroSetting.Expect(ks => ks.Password).Return("qK9Vpnmp").Repeat.Any();
			qliroSetting.Expect(ks => ks.ClientRef).Return("902").Repeat.Any();
			qliroSetting.Expect(ks => ks.IgnoreCertificateValidation).Return(true).Repeat.Any();
			qliroSetting.Expect(ks => ks.DefaultResponseTimeout).Return(10000).Repeat.Any();

			Expect.Call(() => qliroSetting.Initialize(null)).IgnoreArguments().Repeat.Any();

			return qliroSetting;
		}

		private IQliroTransactionService CreateQliroTransactionService()
		{
			return _mocker.DynamicMock<IQliroTransactionService>();
		}

		private IChannel CreateChannel()
		{
			var channel = _mocker.Stub<IChannel>();
			channel.CommonName = "Sweden";

			channel.Country = _mocker.Stub<ICountry>();
			channel.Country.Iso = "SE";

			channel.Language = _mocker.Stub<ILanguage>();
			channel.Language.Iso = "SV";

			return channel;
		}
	}
}