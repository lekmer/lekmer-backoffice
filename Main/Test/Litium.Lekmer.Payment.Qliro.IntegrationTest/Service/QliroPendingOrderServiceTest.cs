﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Service
{
	[TestFixture]
	public class QliroPendingOrderServiceTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPendingOrderService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroPendingOrderService>();

			Assert.IsInstanceOf<IQliroPendingOrderService>(service);
			Assert.IsInstanceOf<QliroPendingOrderService>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroPendingOrderService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IQliroPendingOrderService>();
			var service2 = IoC.Resolve<IQliroPendingOrderService>();

			Assert.AreEqual(service1, service2);
		}
	}
}