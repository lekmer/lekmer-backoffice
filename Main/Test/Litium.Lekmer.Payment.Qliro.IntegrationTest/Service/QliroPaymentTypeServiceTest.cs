﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Service
{
	[TestFixture]
	public class QliroPaymentTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPaymentTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroPaymentTypeService>();

			Assert.IsInstanceOf<IQliroPaymentTypeService>(service);
			Assert.IsInstanceOf<QliroPaymentTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroPaymentTypeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IQliroPaymentTypeService>();
			var service2 = IoC.Resolve<IQliroPaymentTypeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}