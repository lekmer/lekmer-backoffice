﻿using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Setting
{
	[TestFixture]
	public class QliroSettingTest
	{
		[Test]
		[Category("IoC")]
		public void QliroSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroSetting>();

			Assert.IsInstanceOf<IQliroSetting>(service);
			Assert.IsInstanceOf<IQliroSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroSetting_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IQliroSetting>();
			var service2 = IoC.Resolve<IQliroSetting>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}