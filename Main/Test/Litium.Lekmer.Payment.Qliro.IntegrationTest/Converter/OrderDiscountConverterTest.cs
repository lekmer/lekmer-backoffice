﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class OrderDiscountConverterTest
	{
		[Test]
		[Category("IoC")]
		public void OrderDiscountConverter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderDiscountConverter>();

			Assert.IsInstanceOf<IOrderDiscountConverter>(service);
			Assert.IsInstanceOf<OrderDiscountConverter>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderDiscountConverter_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderDiscountConverter>();
			var service2 = IoC.Resolve<IOrderDiscountConverter>();

			Assert.AreEqual(service1, service2);
		}
	}
}