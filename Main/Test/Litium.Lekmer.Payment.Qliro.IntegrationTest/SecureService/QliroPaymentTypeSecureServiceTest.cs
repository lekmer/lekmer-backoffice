﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.SecureService
{
	[TestFixture]
	public class QliroPaymentTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPaymentTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroPaymentTypeSecureService>();

			Assert.IsInstanceOf<IQliroPaymentTypeSecureService>(service);
			Assert.IsInstanceOf<QliroPaymentTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroPaymentTypeSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IQliroPaymentTypeSecureService>();
			var service2 = IoC.Resolve<IQliroPaymentTypeSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}