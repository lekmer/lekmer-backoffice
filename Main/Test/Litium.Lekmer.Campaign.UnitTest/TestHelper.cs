﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign.UnitTest
{
	public static class TestHelper
	{
		public static Currency GetSwedishKronorCurrency()
		{
			return new Currency
			{
				Id = 1,
				Iso = "SEK"
			};
		}

		public static Currency GetBolivianBolivianosCurrency()
		{
			return new Currency
			{
				Id = 999,
				Iso = "BOB"
			};
		}

		public static Channel GetSwedishChannel()
		{
			return new Channel
			{
				Id = 1,
				Currency = GetSwedishKronorCurrency()
			};
		}

		public static Scensum.Product.Product GetProductFifa()
		{
			return new Scensum.Product.Product
			{
				Id = 1,
				CategoryId = 12,
				Title = "FIFA 10",
				EanCode = "3005986716",
				Price = new PriceListItem
				{
					Id = 123,
					PriceExcludingVat = 479.2m,
					PriceIncludingVat = 599m,
					VatPercentage = 25m,
					PriceListId = 1234,
					ProductId = 1
				}
			};
		}

		public static Scensum.Product.Product GetProductNhl()
		{
			return new Scensum.Product.Product
			{
				Id = 2,
				CategoryId = 12,
				Title = "NHL 10",
				EanCode = "3005986714",
				Price = new PriceListItem
				{
					Id = 123,
					PriceExcludingVat = 479.2m,
					PriceIncludingVat = 599m,
					VatPercentage = 25m,
					PriceListId = 1234,
					ProductId = 1
				}
			};
		}

		public static Scensum.Product.Product GetProductPes()
		{
			return new Scensum.Product.Product
			{
				Id = 3,
				CategoryId = 12,
				Title = "PES 2010",
				EanCode = "3006970217",
				Price = new PriceListItem
				{
					Id = 123,
					PriceExcludingVat = 479.2m,
					PriceIncludingVat = 599m,
					VatPercentage = 25m,
					PriceListId = 1234,
					ProductId = 1
				}
			};
		}

		public static CartFull GetCartTemplate()
		{
			return new CartFull
			{
				Status = BusinessObjectStatus.New,
				Id = 321,
				CreatedDate = DateTime.Now
			};
		}

		public static UserContext GetSwedishUserContext()
		{
			return new UserContext
			{
				Cart = new CartFull(),
				Channel = GetSwedishChannel(),
				Customer = null
			};
		}
	}
}