﻿using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class LekmerPercentagePriceDiscountActionTest
	{
		private const int _swedishKronorCurrencyId = 1;

		[Test]
		public void Apply_ProductAndTargetProduct_ActionApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { PriceIncludingVat = 200, PriceExcludingVat = 160, VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = CreateAction();
			action.TargetProductTypes.Add((int)ProductTypeEnum.Product);

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(true, isApplied);
			Assert.AreEqual(180m, product.CampaignInfo.Price.IncludingVat);
		}

		[Test]
		public void Apply_PackageAndTargetProduct_ActionNotApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Package
			};

			var action = CreateAction();
			action.TargetProductTypes.Add((int)ProductTypeEnum.Product);

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(false, isApplied);
			Assert.AreEqual(200m, product.CampaignInfo.Price.IncludingVat);
		}

		[Test]
		public void Apply_ProductAndTargetPackage_ActionNotApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = CreateAction();
			action.TargetProductTypes.Add((int)ProductTypeEnum.Package);

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(false, isApplied);
			Assert.AreEqual(200m, product.CampaignInfo.Price.IncludingVat);
		}


		private LekmerPercentagePriceDiscountAction CreateAction()
		{
			var campaignConfig = new CampaignConfig
				{
					IncludeAllProducts = true,
					IncludeProducts = new ProductIdDictionary(),
					ExcludeProducts = new ProductIdDictionary(),
					IncludeCategories = new CampaignCategoryDictionary(),
					ExcludeCategories = new CampaignCategoryDictionary(),
					IncludeBrands = new BrandIdDictionary(),
					ExcludeBrands = new BrandIdDictionary()
				};

			var actionType = IoC.Resolve<IProductActionType>();
			actionType.CommonName = "PercentagePriceDiscount";

			var action = new LekmerPercentagePriceDiscountAction
			{
				DiscountAmount = 10m,
				CampaignConfig = campaignConfig,
				TargetProductTypes = new IdDictionary(),
				ActionType = actionType
			};

			return action;
		}
	}
}