﻿using System.Collections.Generic;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class ProductDiscountActionTest
	{
		private const int _swedishKronorCurrencyId = 1;
		private const int _americanDollarCurrencyId = 2;

		private ICurrency SwedichCurrency
		{
			get { return new Currency { Id = _swedishKronorCurrencyId }; }
		}

		private ICurrency AmericanCurrency
		{
			get { return new Currency { Id = _americanDollarCurrencyId }; }
		}

		[Test]
		public void Dollar_campaign_is_not_applied_in_sweden()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
							{
								Id = 1,
								CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
								Price = new PriceListItem { VatPercentage = 25 }
							};

			var action = new ProductDiscountAction
							{
								ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
                                                            {
                                                                {1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = AmericanCurrency, MonetaryValue = 9.99m}}},
																{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = AmericanCurrency, MonetaryValue = 11.99m}}}
                                                            }
							};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_when_product_doesnt_have_campaign_price()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
							{
								Id = 1,
								CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
								Price = new PriceListItem { VatPercentage = 25 }
							};

			var action = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
                                                            {
																{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
                                                            }
			};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_when_campaign_price_is_higher_then_product_price()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
				Price = new PriceListItem { VatPercentage = 25 }
			};

			var action = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
                                                            {
                                                                {1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 120.00m}}},
																{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
                                                            }
			};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_when_campaign_price_equal_to_product_price()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
				Price = new PriceListItem { VatPercentage = 25 }
			};

			var action = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
                                                            {
                                                                {1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 100.00m}}},
																{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
                                                            }
			};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_applied_when_currency_matches_and_campaign_price_is_lower()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
							{
								Id = 1,
								CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
								Price = new PriceListItem { PriceIncludingVat = 100, PriceExcludingVat = 80, VatPercentage = 25 },
							};

			var action = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
					{
						{1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 50.00m}}},
						{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
					},
				ActionType = CreateProductActionType()
			};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsTrue(applied, "Action applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_after_PercentageDiscountAction_when_campaign_price_is_higher()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
							{
								Id = 1,
								CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
								Price = new PriceListItem { VatPercentage = 25, PriceIncludingVat = 100, PriceExcludingVat = 80}
							};

			var percentagePriceDiscountAction = CreateAction();
			bool percentagePriceDiscountActionApplied = percentagePriceDiscountAction.Apply(swedishContext, product);

			Assert.IsTrue(percentagePriceDiscountActionApplied, "Percentage discount applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);

			var discountAction = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
                                                            {
                                                                {1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 60.00m}}},
																{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
                                                            }
			};
			bool discountActionApplied = discountAction.Apply(swedishContext, product);

			Assert.IsFalse(discountActionApplied, "Discount action applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_applied_after_PercentageDiscountAction_when_campaign_price_is_lower()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
							{
								Id = 1,
								CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
								Price = new PriceListItem { PriceIncludingVat = 100, PriceExcludingVat = 80, VatPercentage = 25 },
							};

			var percentagePriceDiscountAction = CreateAction();
			bool percentagePriceDiscountActionApplied = percentagePriceDiscountAction.Apply(swedishContext, product);

			Assert.IsTrue(percentagePriceDiscountActionApplied, "Percentage discount applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);

			var discountAction = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
					{
						{1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 10.00m}}},
						{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
					},
				ActionType = CreateProductActionType()
			};
			bool discountActionApplied = discountAction.Apply(swedishContext, product);

			Assert.IsTrue(discountActionApplied, "Discount action applied");
			Assert.AreEqual(new Price(10, 8), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_applied_before_PercentageDiscountAction()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
							{
								Id = 1,
								CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100, 80) },
								Price = new PriceListItem { PriceIncludingVat = 100, PriceExcludingVat = 80, VatPercentage = 25 },
							};

			var discountAction = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>
					{
						{1, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 75}}},
						{2, new LekmerCurrencyValueDictionary{new CurrencyValue{Currency = SwedichCurrency, MonetaryValue = 99.50m}}}
					},
				ActionType = CreateProductActionType()
			};
			bool discountActionApplied = discountAction.Apply(swedishContext, product);

			Assert.IsTrue(discountActionApplied, "Discount action applied");
			Assert.AreEqual(new Price(75, 60), product.CampaignInfo.Price);

			var percentagePriceDiscountAction = CreateAction();
			bool percentagePriceDiscountActionApplied = percentagePriceDiscountAction.Apply(swedishContext, product);

			Assert.IsTrue(percentagePriceDiscountActionApplied, "Percentage discount applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);
		}

		[Test]
		public void Apply_EmptyTargetProductTypes_ActionApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { PriceIncludingVat = 200, PriceExcludingVat = 160, VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary> { { 1, new LekmerCurrencyValueDictionary { new CurrencyValue { Currency = SwedichCurrency, MonetaryValue = 50.00m } } } },
				TargetProductTypes = new IdDictionary(),
				ActionType = CreateProductActionType()
			};

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(true, isApplied);
			Assert.AreEqual(50m, product.CampaignInfo.Price.IncludingVat);
		}

		[Test]
		public void Apply_DifferentTargetProductTypes_ActionApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { PriceIncludingVat = 200, PriceExcludingVat = 160, VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = new ProductDiscountAction
			{
				ProductDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary> { { 1, new LekmerCurrencyValueDictionary { new CurrencyValue { Currency = SwedichCurrency, MonetaryValue = 50.00m } } } },
				TargetProductTypes = new IdDictionary { (int)ProductTypeEnum.Package },
				ActionType = CreateProductActionType()
			};

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(true, isApplied);
			Assert.AreEqual(50m, product.CampaignInfo.Price.IncludingVat);
		}

		private LekmerPercentagePriceDiscountAction CreateAction()
		{
			var campaignConfig = new CampaignConfig
			{
				IncludeAllProducts = true,
				IncludeProducts = new ProductIdDictionary(),
				ExcludeProducts = new ProductIdDictionary(),
				IncludeCategories = new CampaignCategoryDictionary(),
				ExcludeCategories = new CampaignCategoryDictionary(),
				IncludeBrands = new BrandIdDictionary(),
				ExcludeBrands = new BrandIdDictionary()
			};

			var actionType = IoC.Resolve<IProductActionType>();
			actionType.CommonName = "PercentagePriceDiscount";

			var action = new LekmerPercentagePriceDiscountAction
			{
				DiscountAmount = 50,
				CampaignConfig = campaignConfig,
				TargetProductTypes = new IdDictionary(),
				ActionType = actionType
			};

			return action;
		}

		private IProductActionType CreateProductActionType()
		{
			var actionType = IoC.Resolve<IProductActionType>();
			actionType.CommonName = "ProductDiscount";
			return actionType;
		}
	}
}