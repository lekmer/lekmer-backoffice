﻿using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerContentNodeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerContentNodeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ContentNodeRepository>();

			Assert.IsInstanceOf<ContentNodeRepository>(repository);
			Assert.IsInstanceOf<LekmerContentNodeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentNodeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ContentNodeRepository>();
			var repository2 = IoC.Resolve<ContentNodeRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}