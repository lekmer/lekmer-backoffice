﻿using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockListRepository>();
			Assert.IsInstanceOf<BlockListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockListRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockListRepository>();
			var repository2 = IoC.Resolve<BlockListRepository>();
			Assert.AreEqual(repository1, repository2);
		}
	}
}