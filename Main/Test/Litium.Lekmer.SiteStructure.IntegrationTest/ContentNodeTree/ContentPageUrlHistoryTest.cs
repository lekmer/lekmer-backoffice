﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class TreeItemsHashTest
	{
		[Test]
		[Category("IoC")]
		public void TreeItemsHash_Resolve_Resolved()
		{
			var hash = IoC.Resolve<ITreeItemsHash>();

			Assert.IsInstanceOf<ITreeItemsHash>(hash);
			Assert.IsInstanceOf<TreeItemsHash>(hash);
		}

		[Test]
		[Category("IoC")]
		public void TreeItemsHash_ResolveTwice_DifferentObjects()
		{
			var hash1 = IoC.Resolve<ITreeItemsHash>();
			var hash2 = IoC.Resolve<ITreeItemsHash>();

			Assert.AreNotEqual(hash1, hash2);
		}
	}
}