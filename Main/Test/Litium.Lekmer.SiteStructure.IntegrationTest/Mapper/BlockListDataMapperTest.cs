﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockListDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockList> dataMapper = DataMapperResolver.Resolve<IBlockList>(_dataReader);

			Assert.IsInstanceOf<BlockListDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockListDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockList> dataMapper1 = DataMapperResolver.Resolve<IBlockList>(_dataReader);
			DataMapperBase<IBlockList> dataMapper2 = DataMapperResolver.Resolve<IBlockList>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}
