﻿using System.Data;
using Litium.Lekmer.SiteStructure.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Mapper
{
	[TestFixture]
	public class ContentPageUrlHistoryDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistory_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IContentPageUrlHistory>(_dataReader);

			Assert.IsInstanceOf<ContentPageUrlHistoryDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistory_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IContentPageUrlHistory>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IContentPageUrlHistory>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}