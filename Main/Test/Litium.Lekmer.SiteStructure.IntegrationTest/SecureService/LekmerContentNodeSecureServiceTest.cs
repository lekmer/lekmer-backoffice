﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerContentNodeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerContentNodeSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IContentNodeSecureService>();

			Assert.IsInstanceOf<ContentNodeSecureService>(secureService);
			Assert.IsInstanceOf<LekmerContentNodeSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentNodeSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IContentNodeSecureService>();
			var secureService2 = IoC.Resolve<IContentNodeSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}