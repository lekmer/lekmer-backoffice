﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Litium.Lekmer.Common;
using Litium.Lekmer.SiteStructure.IntegrationTest.Helper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class ContentPageUrlHistorySecureServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly string TEST_URL_TITLE_1 = "Test URL Title1_";
		private static readonly string TEST_URL_TITLE_2 = "Test URL Title2_";
		private static readonly string TEST_URL_TITLE_3 = "Test URL Title3_";
		private static readonly string TEST_URL_TITLE_4 = "Test URL Title4_";

		private MockRepository _mocker;
		private IUserContext _userContext;
		private ISystemUserFull _systemUserFull;
		private IPrivilege _privilege;
		private IChannel _channel;

		private IContentPageUrlHistorySecureService _contentPageUrlHistorySecureService;
		private IContentPageUrlHistoryService _contentPageUrlHistoryService;

		private ContentPageUrlHistoryHelper _contentPageUrlHistoryHelper;
		private List<IContentPage> _contentNodeCollection;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_privilege = _mocker.Stub<IPrivilege>();
			_privilege.CommonName = PrivilegeConstant.SiteStructurePages;

			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_systemUserFull.Privileges = new Collection<IPrivilege> { _privilege };

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;

			_userContext.Channel = _channel;

			_contentPageUrlHistorySecureService = IoC.Resolve<IContentPageUrlHistorySecureService>();
			_contentPageUrlHistoryService = IoC.Resolve<IContentPageUrlHistoryService>();

			_contentPageUrlHistoryHelper = new ContentPageUrlHistoryHelper();
			_contentNodeCollection = _contentPageUrlHistoryHelper.GetContentNodes(_userContext).ToList();
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistorySecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IContentPageUrlHistorySecureService>();

			Assert.IsInstanceOf<IContentPageUrlHistorySecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistorySecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IContentPageUrlHistorySecureService>();
			var secureService2 = IoC.Resolve<IContentPageUrlHistorySecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}

		[Test]
		public void ContentPageUrlHistoryService_GetAllByContentPage_ReturnedList()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				int contentPageId = _contentNodeCollection[0].Id;

				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						contentPageId,
						TEST_URL_TITLE_1 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Get ContentPageUrlHistory by ContentPage.
				IEnumerable<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistorySecureService.GetAllByContentPage(contentPageId);

				for (int i = 0; i <= 10; i++)
				{
					IContentPageUrlHistory contentPageUrlHistory = contentPageUrlHistoryCollection.FirstOrDefault(h => h.UrlTitleOld == TEST_URL_TITLE_1 + i);

					Assert.AreEqual(contentPageId, contentPageUrlHistory.ContentPageId);
					Assert.AreEqual((int)HistoryLifeIntervalType.Immortal, contentPageUrlHistory.TypeId);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_DeleteById_Deleted()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						_contentNodeCollection[i].Id,
						TEST_URL_TITLE_2 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				List<int> contentPageUrlHistoryIds = _contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				foreach (int contentPageUrlHistoryId in contentPageUrlHistoryIds)
				{
					_contentPageUrlHistorySecureService.DeleteById(_systemUserFull, contentPageUrlHistoryId);
				}

				// Get ContentPageUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_2 + i);

					Assert.AreEqual(0, contentPageUrlHistoryCollection.Count);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_DeleteAllByContentPage_Deleted()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				int contentPageId = _contentNodeCollection[0].Id;

				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						contentPageId,
						TEST_URL_TITLE_3 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Delete ContentPageUrlHistory collection by ContentPage.
				_contentPageUrlHistorySecureService.DeleteAllByContentPage(_systemUserFull, contentPageId);

				// Get ContentPageUrlHistory by ContentPage.
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistorySecureService.GetAllByContentPage(contentPageId);
				Assert.AreEqual(0, contentPageUrlHistoryCollection.Count);

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_DeleteAllByContentPageAndUrlTitle_Deleted()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						_contentNodeCollection[i].Id,
						TEST_URL_TITLE_4 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Delete ContentPageUrlHistory collection.
				for (int i = 0; i <= 10; i++)
				{
					_contentPageUrlHistorySecureService.DeleteAllByContentPageAndUrlTitle(_systemUserFull, _contentNodeCollection[i].Id, TEST_URL_TITLE_4 + i);
				}

				// Get ContentPageUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_4 + i);

					Assert.AreEqual(0, contentPageUrlHistoryCollection.Count);
				}

				transactionScope.Dispose();
			}
		}
	}
}