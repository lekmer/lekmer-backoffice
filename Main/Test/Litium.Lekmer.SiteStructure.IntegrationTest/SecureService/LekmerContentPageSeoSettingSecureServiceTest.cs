﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerContentPageSeoSettingSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerContentPageSeoSettingSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IContentPageSeoSettingSecureService>();

			Assert.IsInstanceOf<IContentPageSeoSettingSecureService>(secureService);
			Assert.IsInstanceOf<LekmerContentPageSeoSettingSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentPageSeoSettingSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IContentPageSeoSettingSecureService>();
			var secureService2 = IoC.Resolve<IContentPageSeoSettingSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}