﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockSettingSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSettingSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IBlockSettingSecureService>();
			Assert.IsInstanceOf<IBlockSettingSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void BlockSettingSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IBlockSettingSecureService>();
			var secureService2 = IoC.Resolve<IBlockSettingSecureService>();
			Assert.AreEqual(secureService1, secureService2);
		}
	}
}