﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerBlockSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerBlockSecureService_ResolveByKey_Resolved()
		{
			var secureService1 = IoC.Resolve<IBlockCreateSecureService>("NewsletterRegistration");
			var secureService2 = IoC.Resolve<IBlockCreateSecureService>("NewsletterUnregistration");

			Assert.IsInstanceOf<IBlockCreateSecureService>(secureService1);
			Assert.IsInstanceOf<IBlockCreateSecureService>(secureService2);
		}
	}
}