﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockListSecureService>();

			Assert.IsInstanceOf<IBlockListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockListSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockListSecureService>();
			var service2 = IoC.Resolve<IBlockListSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}
