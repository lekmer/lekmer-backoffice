﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Litium.Lekmer.Common;
using Litium.Lekmer.SiteStructure.IntegrationTest.Helper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class ContentPageUrlHistoryServiceTest
	{
		private static readonly int MAX_COUNT = 5;
		private static readonly int CHANNEL_ID = 1;
		private static readonly string TEST_URL_TITLE_1 = "Test URL Title1_";
		private static readonly string TEST_URL_TITLE_2 = "Test URL Title2_";
		private static readonly string TEST_URL_TITLE_3 = "Test URL Title3_";
		private static readonly string TEST_URL_TITLE_4 = "Test URL Title4_";
		private static readonly string TEST_URL_TITLE_5 = "Test URL Title5_";

		private MockRepository _mocker;
		private IUserContext _userContext;
		private ISystemUserFull _systemUserFull;
		private IPrivilege _privilege;
		private IChannel _channel;

		private IContentPageUrlHistorySecureService _contentPageUrlHistorySecureService;
		private IContentPageUrlHistoryService _contentPageUrlHistoryService;

		private ContentPageUrlHistoryHelper _contentPageUrlHistoryHelper;
		private List<IContentPage> _contentNodeCollection;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_privilege = _mocker.Stub<IPrivilege>();
			_privilege.CommonName = PrivilegeConstant.SiteStructurePages;

			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_systemUserFull.Privileges = new Collection<IPrivilege> { _privilege };

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;

			_userContext.Channel = _channel;

			_contentPageUrlHistorySecureService = IoC.Resolve<IContentPageUrlHistorySecureService>();
			_contentPageUrlHistoryService = IoC.Resolve<IContentPageUrlHistoryService>();

			_contentPageUrlHistoryHelper = new ContentPageUrlHistoryHelper();
			_contentNodeCollection = _contentPageUrlHistoryHelper.GetContentNodes(_userContext).ToList();
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistoryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IContentPageUrlHistoryService>();

			Assert.IsInstanceOf<IContentPageUrlHistoryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistoryService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IContentPageUrlHistoryService>();
			var service2 = IoC.Resolve<IContentPageUrlHistoryService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void ContentPageUrlHistoryService_GetAllByUrlTitle_ReturnedList()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						_contentNodeCollection[i].Id,
						TEST_URL_TITLE_1 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Get ContentPageUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_1 + i);

					Assert.AreEqual(1, contentPageUrlHistoryCollection.Count, "Count");
					Assert.AreEqual(_contentNodeCollection[i].Id, contentPageUrlHistoryCollection[0].ContentPageId, "ContentPageId");
					Assert.AreEqual(TEST_URL_TITLE_1 + i, contentPageUrlHistoryCollection[0].UrlTitleOld, "UrlTitleOld");
					Assert.AreEqual((int)HistoryLifeIntervalType.Immortal, contentPageUrlHistoryCollection[0].TypeId, "TypeId");
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_GetAllByLanguage_ReturnedList()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						_contentNodeCollection[i].Id,
						TEST_URL_TITLE_2 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Get ContentPageUrlHistory by Language.
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistoryService.GetAll(_userContext);

				Assert.IsTrue(contentPageUrlHistoryCollection.Count >= 11, "Count >= 11");

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_Update_UsageAmountUpdated()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						_contentNodeCollection[i].Id,
						TEST_URL_TITLE_3 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				List<int> contentPageUrlHistoryIds = _contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Update ContentPageUrlHistory collection.
				foreach (int contentPageUrlHistoryId in contentPageUrlHistoryIds)
				{
					_contentPageUrlHistoryService.Update(contentPageUrlHistoryId);
				}

				// Get ContentPageUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_3 + i);

					Assert.AreEqual(1, contentPageUrlHistoryCollection.Count, "Count");
					Assert.AreEqual(1, contentPageUrlHistoryCollection[0].UsageAmount, "UsageAmount_" + i);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_CleanUp_RemoveRedundant()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				int contentPageId = _contentNodeCollection[0].Id;

				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						contentPageId,
						TEST_URL_TITLE_4 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Remove redundant
				_contentPageUrlHistoryService.CleanUp(MAX_COUNT);

				// Get ContentPageUrlHistory by ContentPage.
				IEnumerable<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistorySecureService.GetAllByContentPage(contentPageId);

				Assert.AreEqual(MAX_COUNT, contentPageUrlHistoryCollection.Count(), "Returned count");

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ContentPageUrlHistoryService_DeleteExpiredItems_RemoveExpired()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IContentPageUrlHistory> contentPageUrlHistoryCollectionToSave = new Collection<IContentPageUrlHistory>();

				// Populate collection of ContentPageUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					contentPageUrlHistoryCollectionToSave.Add(_contentPageUrlHistorySecureService.Create(
						_contentNodeCollection[i].Id,
						TEST_URL_TITLE_5 + i,
						(int)HistoryLifeIntervalType.Expired));
				}

				// Save ContentPageUrlHistory collection.
				_contentPageUrlHistorySecureService.Save(_systemUserFull, contentPageUrlHistoryCollectionToSave);

				// Remove redundant
				_contentPageUrlHistoryService.DeleteExpiredItems();

				// Get ContentPageUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection = _contentPageUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_5 + i);

					Assert.AreEqual(0, contentPageUrlHistoryCollection.Count);
				}

				transactionScope.Dispose();
			}
		}
	}
}