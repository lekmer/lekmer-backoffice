using Litium.Lekmer.ProductFilter;
using NUnit.Framework;

namespace Lekmer.ProductFilter.UnitTest
{
	[TestFixture]
	public class FilteredProductRangeStatisticsTest
	{
		[Test]
		public void Calculates_statistics_for_five_products()
		{
			var products = FilterProductHelper.FiveProducts;

			var stats = new FilteredProductRangeStatistics();
			stats.LoadAgeIntervals(FilterProductHelper.AgeIntervals);
			stats.LoadPriceIntervals(FilterProductHelper.PriceIntervals);
			stats.LoadProducts(products);

			Assert.AreEqual(2, stats.BrandCount.Count, "BrandCount.Count");
			Assert.AreEqual(2, stats.BrandCount[FilterProductHelper.LegoBrandId], "Brand Lego");
			Assert.AreEqual(3, stats.BrandCount[FilterProductHelper.BrioBrandId], "Brand Brio");

			Assert.AreEqual(5, stats.CategoryCount.Count, "CategoryCount.Count");
			Assert.AreEqual(3, stats.CategoryCount[FilterProductHelper.SportCategoryId], "Category Sport");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.FootballCategoryId], "Category Football");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.FloorballCategoryId], "Category Floorball");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.BookCategoryId], "Category Book");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.DollCategoryId], "Category Doll");

			Assert.AreEqual(2, stats.TagCount.Count, "TagCount.Count");
			Assert.AreEqual(3, stats.TagCount[FilterProductHelper.SummerTagId], "Tag Summer");
			Assert.AreEqual(2, stats.TagCount[FilterProductHelper.WinterTagId], "Tag Winter");

			Assert.AreEqual(0, stats.AgeInterval.FromMonth, "AgeInterval.FromMonth");
			Assert.AreEqual(24, stats.AgeInterval.ToMonth, "AgeInterval.ToMonth");

			Assert.AreEqual(0.0m, stats.PriceInterval.From, "PriceInterval.From");
			Assert.AreEqual(300.0m, stats.PriceInterval.To, "PriceInterval.To");

			Assert.AreEqual(2, stats.SizeCount.Count, "SizeCount.Count");
			Assert.AreEqual(3, stats.SizeCount[FilterProductHelper.Size1Id], "Size 1");
			Assert.AreEqual(2, stats.SizeCount[FilterProductHelper.Size2Id], "Size 2");
		}

		//Erik: I give up!
		//[Test]
		//public void Calculate_statistics_on_10000_products_takes_less_then_100_ms()
		//{
		//    IEnumerable<FilterProduct> products = FilterProductHelper.GenerateRandomProducts(10000);

		//    TimeSpan? lowestTime = null;

		//    for (int i = 0; i < 3; i++)
		//    {
		//        var stopwatch = Stopwatch.StartNew();

		//        var stats = new FilteredProductRangeStatistics();
		//        stats.LoadAgeIntervals(FilterProductHelper.AgeIntervals);
		//        stats.LoadPriceIntervals(FilterProductHelper.PriceIntervals);
		//        stats.LoadProducts(products);

		//        stopwatch.Stop();
		//        TimeSpan processTime = stopwatch.Elapsed;

		//        if (!lowestTime.HasValue || processTime < lowestTime.Value)
		//        {
		//            lowestTime = processTime;
		//        }

		//        if (lowestTime.Value.TotalMilliseconds <= 100) return;
		//    }

		//    if (lowestTime.HasValue && lowestTime.Value.TotalMilliseconds > 100)
		//    {
		//        Assert.Fail(string.Format("Calculating statistics took {0} ms.", lowestTime.Value.TotalMilliseconds));
		//    }
		//}
	}
}