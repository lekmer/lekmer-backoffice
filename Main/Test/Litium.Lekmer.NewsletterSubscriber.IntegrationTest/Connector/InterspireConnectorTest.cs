﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Connector
{
	[TestFixture]
	public class InterspireConnectorTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireConnector_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireConnector>();

			Assert.IsInstanceOf<IInterspireConnector>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireConnector_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IInterspireConnector>();
			var instance2 = IoC.Resolve<IInterspireConnector>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		public void AddSubscriberToList_Execute_ResultReturned()
		{
			IInterspireConnector interspireConnector = IoC.Resolve<IInterspireConnector>();

			IInterspireResponseAddSubscriber responseAddSubscriber = interspireConnector.AddSubscriberToList("test123321@lekmer.se", "94");
		}

		[Test]
		public void DeleteSubscriber_Execute_ResultReturned()
		{
			IInterspireConnector interspireConnector = IoC.Resolve<IInterspireConnector>();

			IInterspireResponseDeleteSubscriber responseDeleteSubscriber = interspireConnector.DeleteSubscriber("test123321@lekmer.se", "94");
		}

		[Test]
		public void UnsubscribeSubscriber_Execute_ResultReturned()
		{
			IInterspireConnector interspireConnector = IoC.Resolve<IInterspireConnector>();

			IInterspireResponseUnsubscribeSubscriber responseUnsubscribeSubscriber = interspireConnector.UnsubscribeSubscriber("test123321@lekmer.se", "94");
		}

		[Test]
		public void GetLists_Execute_ResultReturned()
		{
			IInterspireConnector interspireConnector = IoC.Resolve<IInterspireConnector>();

			IInterspireResponseGetLists responseGetLists = interspireConnector.GetLists();
		}

		[Test]
		public void IsContactOnList_Execute_ResultReturned()
		{
			IInterspireConnector interspireConnector = IoC.Resolve<IInterspireConnector>();

			IInterspireResponseIsContactOnList responseIsContactOnList = interspireConnector.IsContactOnList("test123321@lekmer.se", "94");
		}

		[Test]
		public void AddBannedSubscriber_Execute_ResultReturned()
		{
			var interspireConnector = IoC.Resolve<IInterspireConnector>();
			interspireConnector.AddBannedSubscriber("roma.grom8@gmail.com");
		}
		[Test]
		public void AddBannedSubscriber1_Execute_ResultReturned()
		{
			var interspireConnector = IoC.Resolve<IInterspireConnector>();
			interspireConnector.AddBannedSubscriber("roma.grom9@gmail.com");
		}
		[Test]
		public void FetchBannedSubscriber_Execute_ResultReturned()
		{
			var interspireConnector = IoC.Resolve<IInterspireConnector>();
			interspireConnector.FetchBannedSubscriber("roma.grom1@gmail.com");
		}
		[Test]
		public void RemoveBannedSubscriber1_Execute_ResultReturned()
		{
			var email = "roma.grom@gmail.com";
			var interspireConnector = IoC.Resolve<IInterspireConnector>();
			var interspireResponseFetchBannedSubscriber = interspireConnector.FetchBannedSubscriber(email);
			if (interspireResponseFetchBannedSubscriber.BanList != null && interspireResponseFetchBannedSubscriber.BanList.Email == email)
			{
				interspireConnector.RemoveBannedSubscriber(interspireResponseFetchBannedSubscriber.BanList.BanId);
			}
		}
		[Test]
		public void RemoveBannedSubscriber2_Execute_ResultReturned()
		{
			var email = "roma.grom1@gmail.com";
			var interspireConnector = IoC.Resolve<IInterspireConnector>();
			var interspireResponseFetchBannedSubscriber = interspireConnector.FetchBannedSubscriber(email);
			if (interspireResponseFetchBannedSubscriber.BanList != null && interspireResponseFetchBannedSubscriber.BanList.Email == email)
			{
				interspireConnector.RemoveBannedSubscriber(interspireResponseFetchBannedSubscriber.BanList.BanId);
			}
		}
	}
}