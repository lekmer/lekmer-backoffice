﻿using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Repository
{
	[TestFixture]
	public class NewsletterUnsubscriberRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<NewsletterUnsubscriberRepository>();

			Assert.IsInstanceOf<NewsletterUnsubscriberRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<NewsletterUnsubscriberRepository>();
			var repository2 = IoC.Resolve<NewsletterUnsubscriberRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}