﻿using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Repository
{
	[TestFixture]
	public class NewsletterUnsubscriberOptionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOptionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<NewsletterUnsubscriberOptionRepository>();

			Assert.IsInstanceOf<NewsletterUnsubscriberOptionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOptionRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<NewsletterUnsubscriberOptionRepository>();
			var repository2 = IoC.Resolve<NewsletterUnsubscriberOptionRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}