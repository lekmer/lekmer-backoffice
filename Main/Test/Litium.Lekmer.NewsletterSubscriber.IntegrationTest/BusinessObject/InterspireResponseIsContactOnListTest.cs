﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class InterspireResponseIsContactOnListTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireResponseIsContactOnList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireResponseIsContactOnList>();

			Assert.IsInstanceOf<IInterspireResponseIsContactOnList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireResponseIsContactOnList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IInterspireResponseIsContactOnList>();
			var instance2 = IoC.Resolve<IInterspireResponseIsContactOnList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}