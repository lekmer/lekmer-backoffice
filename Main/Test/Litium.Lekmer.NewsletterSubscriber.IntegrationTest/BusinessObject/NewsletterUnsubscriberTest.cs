﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class NewsletterUnsubscriberTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriber_Resolve_Resolved()
		{
			var instance = IoC.Resolve<INewsletterUnsubscriber>();

			Assert.IsInstanceOf<INewsletterUnsubscriber>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriber_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<INewsletterUnsubscriber>();
			var instance2 = IoC.Resolve<INewsletterUnsubscriber>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}