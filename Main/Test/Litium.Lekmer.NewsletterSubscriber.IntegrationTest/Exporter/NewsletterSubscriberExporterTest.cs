﻿using Litium.Lekmer.NewsletterSubscriber;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Service
{
	[TestFixture]
	public class NewsletterSubscriberExporterTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberExporter_Resolve_Resolved()
		{
			var instance = IoC.Resolve<INewsletterSubscriberExporter>();

			Assert.IsInstanceOf<INewsletterSubscriberExporter>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberExporter_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<INewsletterSubscriberExporter>();
			var instance2 = IoC.Resolve<INewsletterSubscriberExporter>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}