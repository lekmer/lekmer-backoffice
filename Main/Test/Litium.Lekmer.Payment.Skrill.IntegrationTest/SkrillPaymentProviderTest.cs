﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Skrill.IntegrationTest
{
	[TestFixture]
	public class SkrillPaymentProviderTest
	{
		[Test]
		[Category("IoC")]
		public void SkrillPaymentProvider_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISkrillPaymentProvider>();

			Assert.IsInstanceOf<ISkrillPaymentProvider>(service);
			Assert.IsInstanceOf<SkrillPaymentProvider>(service);
		}
	}
}