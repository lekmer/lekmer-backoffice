﻿using System.Collections.Specialized;
using System.Web;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.UnitTest
{
	[TestFixture]
	public class DibsResponseTest
	{
		[Test]
		public void DibsResponse_CreateNewWithNullResponse_CreatedAndNotValid()
		{
			var dibsResponse = new DibsResponse(null);

			Assert.That(dibsResponse.Validate(), Is.EqualTo(false));
		}

		[Test]
		public void DibsResponse_CreateNewWithSuccessResponse_CreatedAndValid()
		{
			string queryString = @"?action=success&reply=A&verifyid=201073783&orderno=1765543&sum=247,00&currency=SEK&ccType=Visa&ccPrefix=426387&expM=02&expY=16&MAC=7B507A36FD89C3E7E637C88B6BADAC2D97BC45A7";
			NameValueCollection response = HttpUtility.ParseQueryString(queryString);
			var dibsResponse = new DibsResponse(response);

			Assert.That(dibsResponse.Validate(), Is.EqualTo(true));

			Assert.That(dibsResponse.Action, Is.EqualTo("success"));
			Assert.That(dibsResponse.Reply, Is.EqualTo("A"));
			Assert.That(dibsResponse.VerifyId, Is.EqualTo("201073783"));
			Assert.That(dibsResponse.OrderId, Is.EqualTo(1765543));
			Assert.That(dibsResponse.Sum, Is.EqualTo("247,00"));
			Assert.That(dibsResponse.Currency, Is.EqualTo("SEK"));
			Assert.That(dibsResponse.CreditCardType, Is.EqualTo("Visa"));
			Assert.That(dibsResponse.CreditCardPrefix, Is.EqualTo("426387"));
			Assert.That(dibsResponse.CreditCardExpMonth, Is.EqualTo("02"));
			Assert.That(dibsResponse.CreditCardExpYear, Is.EqualTo("16"));
			Assert.That(dibsResponse.Mac, Is.EqualTo("7B507A36FD89C3E7E637C88B6BADAC2D97BC45A7"));
		}

		[Test]
		public void DibsResponse_CreateNewWithFailureResponse_CreatedAndValid()
		{
			string queryString = @"?action=failure&reply=D&replyText=This%20card%20is%20ineligible%20for%203-D%20Secure%20payments.&verifyid=201073513&orderno=1765543&sum=247,00&currency=SEK&authCode=FRAUD_PROTECTION_HIT&acqrCode=DT&ccType=&ccPart=****%20****%20****%209587&ccPrefix=426381&expM=02&expY=16&MAC=3EF219890B1FAF80F2251D2ABF9EAB6070D965C7";
			NameValueCollection response = HttpUtility.ParseQueryString(queryString);
			var dibsResponse = new DibsResponse(response);

			Assert.That(dibsResponse.Validate(), Is.EqualTo(true));

			Assert.That(dibsResponse.Action, Is.EqualTo("failure"));
			Assert.That(dibsResponse.Reply, Is.EqualTo("D"));
			Assert.That(dibsResponse.ReplyText, Is.EqualTo("This card is ineligible for 3-D Secure payments."));
			Assert.That(dibsResponse.VerifyId, Is.EqualTo("201073513"));
			Assert.That(dibsResponse.OrderId, Is.EqualTo(1765543));
			Assert.That(dibsResponse.Sum, Is.EqualTo("247,00"));
			Assert.That(dibsResponse.Currency, Is.EqualTo("SEK"));
			Assert.That(dibsResponse.AuthenticationCode, Is.EqualTo("FRAUD_PROTECTION_HIT"));
			Assert.That(dibsResponse.AcquirerResponseCode, Is.EqualTo("DT"));
			Assert.That(dibsResponse.CreditCardType, Is.EqualTo(""));
			Assert.That(dibsResponse.CreditCardPart, Is.EqualTo("**** **** **** 9587"));
			Assert.That(dibsResponse.CreditCardPrefix, Is.EqualTo("426381"));
			Assert.That(dibsResponse.CreditCardExpMonth, Is.EqualTo("02"));
			Assert.That(dibsResponse.CreditCardExpYear, Is.EqualTo("16"));
			Assert.That(dibsResponse.Mac, Is.EqualTo("3EF219890B1FAF80F2251D2ABF9EAB6070D965C7"));
		}
	}
}
