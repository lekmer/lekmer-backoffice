﻿using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.UnitTest.Converter
{
	[TestFixture]
	public class OrderItemsConverterTest
	{
		[Test]
		public void Convert_OneItemNoDiscount_CorrectAmounts()
		{
			ILekmerChannel lekmerChannel = CreateChannel();

			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(100, 80).ActualPrice(100, 80).Quantity(1).Create())
				.Create();

			IOrderItemsConverter orderConverter = CreateOrderItemsConverter();
			IInvoiceItemsValidation invoiceItemsValidation = new InvoiceItemsValidation();

			Collection<invoiceItemType> invoiceTypes = orderConverter.Convert(order, lekmerChannel);

			bool isValid = invoiceItemsValidation.Validate(1, invoiceTypes.ToArray(), 149m);

			Assert.AreEqual(true, isValid);
		}

		[Test]
		public void Convert_TwoItemsSameVatNoDiscount_CorrectAmounts()
		{
			ILekmerChannel lekmerChannel = CreateChannel();

			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(100m, 80m).ActualPrice(100m, 80m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(129m, 103.2m).Quantity(1).Create())
				.Create();

			IOrderItemsConverter orderConverter = CreateOrderItemsConverter();
			IInvoiceItemsValidation invoiceItemsValidation = new InvoiceItemsValidation();

			Collection<invoiceItemType> invoiceTypes = orderConverter.Convert(order, lekmerChannel);

			bool isValid = invoiceItemsValidation.Validate(1, invoiceTypes.ToArray(), 278m);

			Assert.AreEqual(true, isValid);
		}

		[Test]
		public void Convert_MixedVatAndDiscount_CorrectShipmentAmounts()
		{
			ILekmerChannel lekmerChannel = CreateChannel();

			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(129.0m, 103.20m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(079.0m, 063.20m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(090.3m, 072.24m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(099m, 093.4m).ActualPrice(079.0m, 074.53m).Quantity(1).Create())
				.Create();

			IOrderItemsConverter orderConverter = CreateOrderItemsConverter();
			IInvoiceItemsValidation invoiceItemsValidation = new InvoiceItemsValidation();

			Collection<invoiceItemType> invoiceTypes = orderConverter.Convert(order, lekmerChannel);

			bool isValid = invoiceItemsValidation.Validate(1, invoiceTypes.ToArray(), 426.30m);

			Assert.AreEqual(true, isValid);
		}

		[Test]
		public void Convert_MixedVatAndRoundingIssue_CorrectShipmentAmounts()
		{
			ILekmerChannel lekmerChannel = CreateChannel();

			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(099m, 093.4m).ActualPrice(099m, 093.4m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(399m, 319.2m).ActualPrice(399m, 319.2m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(299m, 239.2m).ActualPrice(000m, 000.0m).Quantity(1).Create())
				.Create();

			IOrderItemsConverter orderConverter = CreateOrderItemsConverter();
			IInvoiceItemsValidation invoiceItemsValidation = new InvoiceItemsValidation();

			Collection<invoiceItemType> invoiceTypes = orderConverter.Convert(order, lekmerChannel);

			bool isValid = invoiceItemsValidation.Validate(1, invoiceTypes.ToArray(), 547m);

			Assert.AreEqual(true, isValid);
		}

		[Test]
		public void Convert_ItemHasQuantity_CorrectShipmentAmounts()
		{
			ILekmerChannel lekmerChannel = CreateChannel();

			var order = OrderCreator.New()
				.FreightCost(299m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(189m, 151.2m).ActualPrice(149m, 119.2m).Quantity(5).Create())
				.Create();

			IOrderItemsConverter orderConverter = CreateOrderItemsConverter();
			IInvoiceItemsValidation invoiceItemsValidation = new InvoiceItemsValidation();

			Collection<invoiceItemType> invoiceTypes = orderConverter.Convert(order, lekmerChannel);

			bool isValid = invoiceItemsValidation.Validate(1, invoiceTypes.ToArray(), 1044m);

			Assert.AreEqual(true, isValid);
		}

		[Test]
		public void Convert_OneItemSmallPrice_CorrectVat()
		{
			ILekmerChannel lekmerChannel = CreateChannel();

			var order = OrderCreator.New()
				.FreightCost(4.9m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(14.90m, 12.02m).ActualPrice(11.18m, 9.02m).Quantity(1).Create())
				.Create();

			IOrderItemsConverter orderConverter = CreateOrderItemsConverter();
			IInvoiceItemsValidation invoiceItemsValidation = new InvoiceItemsValidation();

			Collection<invoiceItemType> invoiceTypes = orderConverter.Convert(order, lekmerChannel);

			bool isValid = invoiceItemsValidation.Validate(1, invoiceTypes.ToArray(), 16.08m);

			Assert.AreEqual(true, isValid);
			Assert.AreEqual(24m, invoiceTypes[0].vatPercent);
		}

		private IOrderItemsConverter CreateOrderItemsConverter()
		{
			IOrderItemsConverter orderItemsConverter = new OrderItemsConverter(
				new OrderProductItemConverter(),
				new OrderPackageConverter(new PackageOrderItemConverter()),
				new OrderShipmentConverter(),
				new OrderDiscountConverter());

			return orderItemsConverter;
		}

		private ILekmerChannel CreateChannel()
		{
			ILekmerChannel lekmerChannel = new LekmerChannel();
			lekmerChannel.Id = 1;
			lekmerChannel.VatPercentage = 25m;

			return lekmerChannel;
		}
	}
}
