﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.UnitTest.Converter
{
	[TestFixture]
	public class OrderShipmentConverterTest
	{
		[Test]
		public void Convert_OneItemNoDiscount_CorrectShipmentAmounts()
		{
			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(100, 80).ActualPrice(100, 80).Quantity(1).Create())
				.Create();

			IOrderShipmentConverter orderShipmentConverter = new OrderShipmentConverter();

			invoiceItemType invoiceShipmentItem = orderShipmentConverter.Convert(order, false, 25m);

			Assert.AreEqual(39.2m, invoiceShipmentItem.itemPrice);
			Assert.AreEqual(49m, invoiceShipmentItem.grossAmount);
			Assert.AreEqual(39.2m, invoiceShipmentItem.netAmount);
			Assert.AreEqual(25m, invoiceShipmentItem.vatPercent);
			Assert.AreEqual(1m, invoiceShipmentItem.quantity);
		}

		[Test]
		public void Convert_TwoItemsSameVatNoDiscount_CorrectShipmentAmounts()
		{
			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(100m, 80m).ActualPrice(100m, 80m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(129m, 103.2m).Quantity(1).Create())
				.Create();

			IOrderShipmentConverter orderShipmentConverter = new OrderShipmentConverter();

			invoiceItemType invoiceShipmentItem = orderShipmentConverter.Convert(order, false, 25m);

			Assert.AreEqual(39.2m, invoiceShipmentItem.itemPrice);
			Assert.AreEqual(49m, invoiceShipmentItem.grossAmount);
			Assert.AreEqual(39.2m, invoiceShipmentItem.netAmount);
			Assert.AreEqual(25m, invoiceShipmentItem.vatPercent);
			Assert.AreEqual(1m, invoiceShipmentItem.quantity);
		}

		[Test]
		public void Convert_VatNoDiscount_CorrectShipmentAmounts()
		{
			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(100m, 80m).ActualPrice(100m, 80m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(129m, 103.2m).Quantity(1).Create())
				.Create();

			IOrderShipmentConverter orderShipmentConverter = new OrderShipmentConverter();

			invoiceItemType invoiceShipmentItem = orderShipmentConverter.Convert(order, false, 25m);

			Assert.AreEqual(39.2m, invoiceShipmentItem.itemPrice);
			Assert.AreEqual(49m, invoiceShipmentItem.grossAmount);
			Assert.AreEqual(39.2m, invoiceShipmentItem.netAmount);
			Assert.AreEqual(25m, invoiceShipmentItem.vatPercent);
			Assert.AreEqual(1m, invoiceShipmentItem.quantity);
		}

		[Test]
		public void Convert_MixedVatAndDiscount_CorrectShipmentAmounts()
		{
			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(129.0m, 103.20m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(079.0m, 063.20m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(090.3m, 072.24m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(099m, 093.4m).ActualPrice(079.0m, 074.53m).Quantity(1).Create())
				.Create();

			IOrderShipmentConverter orderShipmentConverter = new OrderShipmentConverter();

			invoiceItemType invoiceShipmentItem = orderShipmentConverter.Convert(order, false, 25m);

			Assert.AreEqual(39.2m, invoiceShipmentItem.itemPrice);
			Assert.AreEqual(49m, invoiceShipmentItem.grossAmount);
			Assert.AreEqual(39.2m, invoiceShipmentItem.netAmount);
			Assert.AreEqual(25m, invoiceShipmentItem.vatPercent);
			Assert.AreEqual(1m, invoiceShipmentItem.quantity);
		}

		[Test]
		public void Convert_TaxFreeZone_CorrectShipmentAmounts()
		{
			var order = OrderCreator.New()
				.FreightCost(49m)
				.OrderItem(OrderItemCreator.New().OriginalPrice(100m, 80m).ActualPrice(100m, 80m).Quantity(1).Create())
				.OrderItem(OrderItemCreator.New().OriginalPrice(129m, 103.2m).ActualPrice(129m, 103.2m).Quantity(1).Create())
				.Create();

			IOrderShipmentConverter orderShipmentConverter = new OrderShipmentConverter();

			invoiceItemType invoiceShipmentItem = orderShipmentConverter.Convert(order, true, 25m);

			Assert.AreEqual(39.2m, invoiceShipmentItem.itemPrice);
			Assert.AreEqual(39.2m, invoiceShipmentItem.grossAmount);
			Assert.AreEqual(39.2m, invoiceShipmentItem.netAmount);
			Assert.AreEqual(0m, invoiceShipmentItem.vatPercent);
			Assert.AreEqual(1m, invoiceShipmentItem.quantity);
		}
	}
}
