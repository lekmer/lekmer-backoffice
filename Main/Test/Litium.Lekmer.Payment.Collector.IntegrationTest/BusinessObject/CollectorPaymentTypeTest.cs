﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CollectorPaymentTypeTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorPaymentType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICollectorPaymentType>();

			Assert.IsInstanceOf<ICollectorPaymentType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CollectorPaymentType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICollectorPaymentType>();
			var instance2 = IoC.Resolve<ICollectorPaymentType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}