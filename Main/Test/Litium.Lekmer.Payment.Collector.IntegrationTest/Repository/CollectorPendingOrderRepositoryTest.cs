﻿using Litium.Lekmer.Payment.Collector.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Repository
{
	[TestFixture]
	public class CollectorPendingOrderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorPendingOrderRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<CollectorPendingOrderRepository>();

			Assert.IsInstanceOf<CollectorPendingOrderRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorPendingOrderRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<CollectorPendingOrderRepository>();
			var service2 = IoC.Resolve<CollectorPendingOrderRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}