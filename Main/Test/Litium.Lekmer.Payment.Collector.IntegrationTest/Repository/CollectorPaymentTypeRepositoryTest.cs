﻿using Litium.Lekmer.Payment.Collector.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Repository
{
	[TestFixture]
	public class CollectorPaymentTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorPaymentTypeRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<CollectorPaymentTypeRepository>();

			Assert.IsInstanceOf<CollectorPaymentTypeRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorPaymentTypeRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<CollectorPaymentTypeRepository>();
			var service2 = IoC.Resolve<CollectorPaymentTypeRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}