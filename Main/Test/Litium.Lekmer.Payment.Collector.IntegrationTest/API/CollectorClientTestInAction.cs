﻿using Litium.Lekmer.Payment.Collector.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.API
{
	[TestFixture]
	public class CollectorClientTestInAction
	{
		private MockRepository _mocker;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void GetAddress_ValidCivicNumber_AddressReturned()
		{
			IChannel channel = CreateChannel();
			ICollectorClient collectorClient = CreateCollectorClient();

			_mocker.ReplayAll();

			collectorClient.Initialize(channel.CommonName);

			ICollectorGetAddressResult collectorGetAddressResult = collectorClient.GetAddress(channel, "01121579533", "127.0.0.1"); // Testperson-no Approved

			_mocker.VerifyAll();

			Assert.IsNotNull(collectorGetAddressResult);

			Assert.AreEqual((int)CollectorTransactionStatus.UnspecifiedError, collectorGetAddressResult.StatusCode, "Status code");

			Assert.IsNotNull(collectorGetAddressResult.CollectorAddresses);
			Assert.IsTrue(collectorGetAddressResult.CollectorAddresses.Count == 0);
		}

		private ICollectorClient CreateCollectorClient()
		{
			var collectorClient = new CollectorClient(CreateCollectorApi(), IoC.Resolve<IChannelService>(), IoC.Resolve<IAliasSharedService>(), IoC.Resolve<ICollectorPaymentTypeService>());
			collectorClient.CollectorSetting = CreateCollectorSetting_NO();
			return collectorClient;
		}

		private ICollectorApi CreateCollectorApi()
		{
			var collectorApi = new CollectorApi(CreateCollectorTransactionService());
			collectorApi.CollectorSetting = CreateCollectorSetting_NO();
			return collectorApi;
		}

		private ICollectorSetting CreateCollectorSetting_NO()
		{
			var collectorSetting = _mocker.Stub<ICollectorSetting>();

			collectorSetting.Expect(ks => ks.Url).Return("https://ecommercetest.collector.se/v3.0/InvoiceServiceV31.svc").Repeat.Any();
			collectorSetting.Expect(ks => ks.UserName).Return("lekmer_test").Repeat.Any();
			collectorSetting.Expect(ks => ks.Password).Return("lekmer_test").Repeat.Any();
			collectorSetting.Expect(ks => ks.StoreId).Return(532).Repeat.Any(); // Norway

			Expect.Call(() => collectorSetting.Initialize(null)).IgnoreArguments().Repeat.Any();

			return collectorSetting;
		}

		private ICollectorTransactionService CreateCollectorTransactionService()
		{
			return _mocker.DynamicMock<ICollectorTransactionService>();
		}

		private IChannel CreateChannel()
		{
			var channel = _mocker.Stub<IChannel>();
			channel.CommonName = "Norway";

			channel.Country = _mocker.Stub<ICountry>();
			channel.Country.Iso = "NO";

			channel.Language = _mocker.Stub<ILanguage>();
			channel.Language.Iso = "NO";

			return channel;
		}
	}
}