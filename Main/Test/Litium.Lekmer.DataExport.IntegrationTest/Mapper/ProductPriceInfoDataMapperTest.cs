﻿using System.Data;
using Litium.Lekmer.DataExport.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.DataExport.IntegrationTest.Mapper
{
	[TestFixture]
	public class ProductPriceInfoDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfo_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IProductPriceInfo>(_dataReader);

			Assert.IsInstanceOf<ProductPriceInfoDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfo_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IProductPriceInfo>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IProductPriceInfo>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}