﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DataExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductPriceInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ProductPriceInfo_Resolve_Resolved()
		{
			var productPriceInfo = IoC.Resolve<IProductPriceInfo>();

			Assert.IsInstanceOf<IProductPriceInfo>(productPriceInfo);
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfo_ResolveTwice_DifferentObjects()
		{
			var productPriceInfo1 = IoC.Resolve<IProductPriceInfo>();
			var productPriceInfo2 = IoC.Resolve<IProductPriceInfo>();

			Assert.AreNotEqual(productPriceInfo1, productPriceInfo2);
		}
	}
}