﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure.Web;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.Web.IntegrationTest
{
	[TestFixture]
	public class BlockBrandTopListControlTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListControl_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockControl>("BrandTopList");

			Assert.IsInstanceOf<BlockBrandTopListControl>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListControl_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockControl>("BrandTopList");
			var instance2 = IoC.Resolve<IBlockControl>("BrandTopList");

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}