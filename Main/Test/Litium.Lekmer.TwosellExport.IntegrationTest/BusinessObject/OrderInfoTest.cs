using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.TwosellExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class OrderInfoTest
	{
		[Test]
		[Category("IoC")]
		public void OrderInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IOrderInfo>();
			Assert.IsInstanceOf<IOrderInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void OrderInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IOrderInfo>();
			var info2 = IoC.Resolve<IOrderInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}