﻿using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.TwosellExport.IntegrationTest.Service
{
	[TestFixture]
	public class TwosellExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void TwosellExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITwosellExportService>();

			Assert.IsInstanceOf<ITwosellExportService>(service);
			Assert.IsInstanceOf<TwosellExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void TwosellExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ITwosellExportService>();
			var service2 = IoC.Resolve<ITwosellExportService>();
			Assert.AreEqual(service1, service2);
		}
	}
}