﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.TwosellExport.IntegrationTest.Repository
{
	[TestFixture]
	public class TwosellExportRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void TwosellExportRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<TwosellExportRepository>();
			Assert.IsInstanceOf<TwosellExportRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void TwosellExportRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<TwosellExportRepository>();
			var service2 = IoC.Resolve<TwosellExportRepository>();
			Assert.AreEqual(service1, service2);
		}
	}
}