﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockBestRatedProductListBrandServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBestRatedProductListBrandService>();

			Assert.IsInstanceOf<IBlockBestRatedProductListBrandService>(service);
			Assert.IsInstanceOf<BlockBestRatedProductListBrandService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBestRatedProductListBrandService>();
			var instance2 = IoC.Resolve<IBlockBestRatedProductListBrandService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}