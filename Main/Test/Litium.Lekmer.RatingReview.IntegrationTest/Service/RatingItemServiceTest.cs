﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingItemServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingItemService>();

			Assert.IsInstanceOf<IRatingItemService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemService>();
			var instance2 = IoC.Resolve<IRatingItemService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}