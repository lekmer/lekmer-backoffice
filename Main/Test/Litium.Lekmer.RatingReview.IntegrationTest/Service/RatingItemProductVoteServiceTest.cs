﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingItemProductVoteServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductVoteService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingItemProductVoteService>();

			Assert.IsInstanceOf<IRatingItemProductVoteService>(service);
			Assert.IsInstanceOf<RatingItemProductVoteService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductVoteService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductVoteService>();
			var instance2 = IoC.Resolve<IRatingItemProductVoteService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}