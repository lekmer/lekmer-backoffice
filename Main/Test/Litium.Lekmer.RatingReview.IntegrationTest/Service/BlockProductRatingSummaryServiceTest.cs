﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductRatingSummaryServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRatingSummaryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductRatingSummaryService>();

			Assert.IsInstanceOf<IBlockProductRatingSummaryService>(service);
			Assert.IsInstanceOf<BlockProductRatingSummaryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRatingSummaryService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductRatingSummaryService>();
			var instance2 = IoC.Resolve<IBlockProductRatingSummaryService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}