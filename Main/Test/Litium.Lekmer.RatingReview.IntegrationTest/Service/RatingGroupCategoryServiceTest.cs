﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingGroupCategoryServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupCategoryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupCategoryService>();

			Assert.IsInstanceOf<IRatingGroupCategoryService>(service);
			Assert.IsInstanceOf<RatingGroupCategoryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupCategoryService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupCategoryService>();
			var instance2 = IoC.Resolve<IRatingGroupCategoryService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}