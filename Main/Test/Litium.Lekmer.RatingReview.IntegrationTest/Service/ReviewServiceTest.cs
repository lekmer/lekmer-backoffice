﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class ReviewServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ReviewService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IReviewService>();

			Assert.IsInstanceOf<IReviewService>(service);
			Assert.IsInstanceOf<ReviewService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ReviewService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IReviewService>();
			var instance2 = IoC.Resolve<IReviewService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}