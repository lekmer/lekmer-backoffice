﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingService>();

			Assert.IsInstanceOf<IRatingService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingService>();
			var instance2 = IoC.Resolve<IRatingService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}