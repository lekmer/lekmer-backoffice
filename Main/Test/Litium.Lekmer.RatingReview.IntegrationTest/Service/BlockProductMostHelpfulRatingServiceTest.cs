﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductMostHelpfulRatingServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductMostHelpfulRatingService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductMostHelpfulRatingService>();

			Assert.IsInstanceOf<IBlockProductMostHelpfulRatingService>(service);
			Assert.IsInstanceOf<BlockProductMostHelpfulRatingService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductMostHelpfulRatingService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductMostHelpfulRatingService>();
			var instance2 = IoC.Resolve<IBlockProductMostHelpfulRatingService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}