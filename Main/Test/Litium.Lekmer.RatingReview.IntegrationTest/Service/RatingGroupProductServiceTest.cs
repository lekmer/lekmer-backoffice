﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingGroupProductServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupProductService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupProductService>();

			Assert.IsInstanceOf<IRatingGroupProductService>(service);
			Assert.IsInstanceOf<RatingGroupProductService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupProductService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupProductService>();
			var instance2 = IoC.Resolve<IRatingGroupProductService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}