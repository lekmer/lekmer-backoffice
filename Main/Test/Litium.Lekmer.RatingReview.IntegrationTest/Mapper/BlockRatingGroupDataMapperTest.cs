﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockRatingGroupDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingGroupDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockRatingGroup> blockRatingGroupDataMapper = DataMapperResolver.Resolve<IBlockRatingGroup>(_dataReader);

			Assert.IsInstanceOf<BlockRatingGroupDataMapper>(blockRatingGroupDataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingGroupDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockRatingGroup> blockRatingGroupDataMapper1 = DataMapperResolver.Resolve<IBlockRatingGroup>(_dataReader);
			DataMapperBase<IBlockRatingGroup> blockRatingGroupDataMapper2 = DataMapperResolver.Resolve<IBlockRatingGroup>(_dataReader);

			Assert.AreNotEqual(blockRatingGroupDataMapper1, blockRatingGroupDataMapper2);
		}
	}
}