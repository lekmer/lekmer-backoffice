﻿using System.Data;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class RatingReviewUserDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewUserDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IRatingReviewUser>(_dataReader);

			Assert.IsInstanceOf<RatingReviewUserDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewUserDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IRatingReviewUser>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IRatingReviewUser>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}