﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockProductLatestFeedbackListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockProductLatestFeedbackList> dataMapper = DataMapperResolver.Resolve<IBlockProductLatestFeedbackList>(_dataReader);

			Assert.IsInstanceOf<BlockProductLatestFeedbackListDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockProductLatestFeedbackList> dataMapper1 = DataMapperResolver.Resolve<IBlockProductLatestFeedbackList>(_dataReader);
			DataMapperBase<IBlockProductLatestFeedbackList> dataMapper2 = DataMapperResolver.Resolve<IBlockProductLatestFeedbackList>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}