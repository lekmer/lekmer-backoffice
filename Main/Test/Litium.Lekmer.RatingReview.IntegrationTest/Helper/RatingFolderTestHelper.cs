﻿using System.Collections.Generic;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Helper
{
	public class RatingFolderTestHelper
	{
		public ISystemUserFull SystemUserFull { get; private set; }
		public IRatingFolderSecureService RatingFolderSecureService { get; private set; }

		public RatingFolderTestHelper(IRatingFolderSecureService ratingFolderSecureService, ISystemUserFull systemUserFull)
		{
			RatingFolderSecureService = ratingFolderSecureService;
			SystemUserFull = systemUserFull;
		}


		public IRatingFolder Create(string title)
		{
			var ratingFolder = RatingFolderSecureService.Create();
			ratingFolder.Title = title;
			return ratingFolder;
		}

		public IRatingFolder Create(string title, int parentId)
		{
			var ratingFolder = RatingFolderSecureService.Create();
			ratingFolder.ParentRatingFolderId = parentId;
			ratingFolder.Title = title;
			return ratingFolder;
		}

		public IRatingFolder Create(string title, IRatingFolder parent)
		{
			return Create(title, parent.Id);
		}

		public IRatingFolder CreateAndSave(string title)
		{
			var ratingFolder = Create(title);
			return RatingFolderSecureService.Save(SystemUserFull, ratingFolder);
		}

		public IRatingFolder CreateAndSave(string title, int parentId)
		{
			var ratingFolder = Create(title, parentId);
			return RatingFolderSecureService.Save(SystemUserFull, ratingFolder);
		}

		public IRatingFolder CreateAndSave(string title, IRatingFolder parent)
		{
			return CreateAndSave(title, parent.Id);
		}


		// Asserts
		public void AssertContains(IRatingFolder expected, IEnumerable<IRatingFolder> actual)
		{
			Assert.IsNotNull(expected, "RatingFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<RatingFolder> actual");

			var actualRatingFolder = actual.FirstOrDefault(rf => rf.Id == expected.Id);

			Assert.IsNotNull(actualRatingFolder, "RatingFolder actual");

			AssertAreEquel(expected, actualRatingFolder);
		}

		public void AssertContains(IRatingFolder expected, IEnumerable<INode> actual)
		{
			Assert.IsNotNull(expected, "RatingFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<RatingFolder> actual");

			var actualNode = actual.FirstOrDefault(n => n.Id == expected.Id);

			Assert.IsNotNull(actualNode, "RatingFolder actual");

			AssertAreEquel(expected, actualNode);
		}

		public void AssertDoesNotContain(IRatingFolder expected, IEnumerable<IRatingFolder> actual)
		{
			Assert.IsNotNull(expected, "RatingFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<RatingFolder> actual");

			var actualRatingFolder = actual.FirstOrDefault(rf => rf.Id == expected.Id);

			Assert.IsNull(actualRatingFolder, "RatingFolder actual");
		}

		public void AssertDoesNotContain(IRatingFolder expected, IEnumerable<INode> actual)
		{
			Assert.IsNotNull(expected, "RatingFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<INode> actual");

			var actualNode = actual.FirstOrDefault(n => n.Id == expected.Id);

			Assert.IsNull(actualNode, "RatingFolder actual");
		}

		public void AssertAreEquel(IRatingFolder expected, IRatingFolder actual)
		{
			Assert.IsNotNull(expected, "RatingFolder expected");
			Assert.IsNotNull(actual, "RatingFolder actual");

			Assert.AreEqual(expected.Id, actual.Id, "RatingFolder.Id");
			Assert.AreEqual(expected.ParentRatingFolderId, actual.ParentRatingFolderId, "RatingFolder.ParentRatingFolderId");
			Assert.AreEqual(expected.Title, actual.Title, "RatingFolder.Title");
		}

		public void AssertAreEquel(IRatingFolder expected, INode actual)
		{
			Assert.IsNotNull(expected, "RatingFolder expected");
			Assert.IsNotNull(actual, "RatingFolder actual");

			Assert.AreEqual(expected.Id, actual.Id, "RatingFolder.Id");
			Assert.AreEqual(expected.ParentRatingFolderId, actual.ParentId, "RatingFolder.ParentRatingFolderId");
			Assert.AreEqual(expected.Title, actual.Title, "RatingFolder.Title");
		}
	}
}