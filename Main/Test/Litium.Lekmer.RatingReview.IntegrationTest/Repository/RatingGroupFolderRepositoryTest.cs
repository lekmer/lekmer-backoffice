﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingGroupFolderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupFolderRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingGroupFolderRepository>();

			Assert.IsInstanceOf<RatingGroupFolderRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupFolderRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingGroupFolderRepository>();
			var instance2 = IoC.Resolve<RatingGroupFolderRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}