﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingRepository>();

			Assert.IsInstanceOf<RatingRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingRepository>();
			var instance2 = IoC.Resolve<RatingRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}