﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockBestRatedProductListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListRepository_Resolve_Resolved()
		{
			BlockBestRatedProductListRepository repository = IoC.Resolve<BlockBestRatedProductListRepository>();

			Assert.IsInstanceOf<BlockBestRatedProductListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListRepository_ResolveTwice_SameObjects()
		{
			BlockBestRatedProductListRepository repository1 = IoC.Resolve<BlockBestRatedProductListRepository>();
			BlockBestRatedProductListRepository repository2 = IoC.Resolve<BlockBestRatedProductListRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}