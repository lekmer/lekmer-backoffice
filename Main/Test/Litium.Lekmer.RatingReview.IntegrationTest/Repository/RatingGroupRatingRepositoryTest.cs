﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingGroupRatingRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupRatingRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingGroupRatingRepository>();

			Assert.IsInstanceOf<RatingGroupRatingRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupRatingRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingGroupRatingRepository>();
			var instance2 = IoC.Resolve<RatingGroupRatingRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}