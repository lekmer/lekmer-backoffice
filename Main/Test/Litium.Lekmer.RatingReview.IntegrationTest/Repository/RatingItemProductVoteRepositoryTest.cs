﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingItemProductVoteRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductVoteRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingItemProductVoteRepository>();

			Assert.IsInstanceOf<RatingItemProductVoteRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductVoteRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingItemProductVoteRepository>();
			var instance2 = IoC.Resolve<RatingItemProductVoteRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}