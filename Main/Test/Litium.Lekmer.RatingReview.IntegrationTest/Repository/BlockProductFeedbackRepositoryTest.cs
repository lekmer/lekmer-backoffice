﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductFeedbackRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackRepository_Resolve_Resolved()
		{
			BlockProductFeedbackRepository blockProductFeedbackRepository = IoC.Resolve<BlockProductFeedbackRepository>();

			Assert.IsInstanceOf<BlockProductFeedbackRepository>(blockProductFeedbackRepository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackRepository_ResolveTwice_SameObjects()
		{
			BlockProductFeedbackRepository blockProductFeedbackRepository1 = IoC.Resolve<BlockProductFeedbackRepository>();
			BlockProductFeedbackRepository blockProductFeedbackRepository2 = IoC.Resolve<BlockProductFeedbackRepository>();

			Assert.AreEqual(blockProductFeedbackRepository1, blockProductFeedbackRepository2);
		}
	}
}