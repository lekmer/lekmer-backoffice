﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingReviewUserRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewUserRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingReviewUserRepository>();

			Assert.IsInstanceOf<RatingReviewUserRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewUserRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingReviewUserRepository>();
			var instance2 = IoC.Resolve<RatingReviewUserRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}