﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingItemProductScoreRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingItemProductScoreRepository>();

			Assert.IsInstanceOf<RatingItemProductScoreRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingItemProductScoreRepository>();
			var instance2 = IoC.Resolve<RatingItemProductScoreRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}