﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockLatestFeedbackListBrandTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrand_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockLatestFeedbackListBrand>();

			Assert.IsInstanceOf<IBlockLatestFeedbackListBrand>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrand_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockLatestFeedbackListBrand>();
			var block2 = IoC.Resolve<IBlockLatestFeedbackListBrand>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}