﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockRatingItemTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingItem_Resolve_Resolved()
		{
			IBlockRatingItem block = IoC.Resolve<IBlockRatingItem>();

			Assert.IsInstanceOf<IBlockRatingItem>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingItem_ResolveTwice_DifferentObjects()
		{
			IBlockRatingItem block1 = IoC.Resolve<IBlockRatingItem>();
			IBlockRatingItem block2 = IoC.Resolve<IBlockRatingItem>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}