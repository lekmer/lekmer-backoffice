﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockBestRatedProductListBrandTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrand_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockBestRatedProductListBrand>();

			Assert.IsInstanceOf<IBlockBestRatedProductListBrand>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrand_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockBestRatedProductListBrand>();
			var block2 = IoC.Resolve<IBlockBestRatedProductListBrand>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}