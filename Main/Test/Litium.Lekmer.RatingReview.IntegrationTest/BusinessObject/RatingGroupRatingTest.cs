﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingGroupRatingTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupRating_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingGroupRating>();

			Assert.IsInstanceOf<IRatingGroupRating>(instance);
			Assert.IsInstanceOf<IRatingGroupRating>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupRating_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupRating>();
			var instance2 = IoC.Resolve<IRatingGroupRating>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}