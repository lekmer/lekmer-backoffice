﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockRatingGroupTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingGroup_Resolve_Resolved()
		{
			IBlockRatingGroup blockRatingGroup = IoC.Resolve<IBlockRatingGroup>();

			Assert.IsInstanceOf<IBlockRatingGroup>(blockRatingGroup);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingGroup_ResolveTwice_DifferentObjects()
		{
			IBlockRatingGroup blockRatingGroup1 = IoC.Resolve<IBlockRatingGroup>();
			IBlockRatingGroup blockRatingGroup2 = IoC.Resolve<IBlockRatingGroup>();

			Assert.AreNotEqual(blockRatingGroup1, blockRatingGroup2);
		}
	}
}