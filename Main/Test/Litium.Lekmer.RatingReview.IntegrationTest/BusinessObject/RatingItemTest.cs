﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingItemTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItem_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingItem>();

			Assert.IsInstanceOf<IRatingItem>(instance);
			Assert.IsInstanceOf<IRatingItem>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingItem_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingItem>();
			var instance2 = IoC.Resolve<IRatingItem>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}