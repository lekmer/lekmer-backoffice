﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingFolderTest
	{
		[Test]
		[Category("IoC")]
		public void RatingFolder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingFolder>();

			Assert.IsInstanceOf<IRatingFolder>(instance);
			Assert.IsInstanceOf<IRatingFolder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingFolder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingFolder>();
			var instance2 = IoC.Resolve<IRatingFolder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}