﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingReviewFeedbackTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedback_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingReviewFeedback>();

			Assert.IsInstanceOf<IRatingReviewFeedback>(instance);
			Assert.IsInstanceOf<IRatingReviewFeedback>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedback_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewFeedback>();
			var instance2 = IoC.Resolve<IRatingReviewFeedback>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}