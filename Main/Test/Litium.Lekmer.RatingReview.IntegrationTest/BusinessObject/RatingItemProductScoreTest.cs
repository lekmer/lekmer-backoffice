﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingItemProductScoreTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductScore_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingItemProductScore>();

			Assert.IsInstanceOf<IRatingItemProductScore>(instance);
			Assert.IsInstanceOf<IRatingItemProductScore>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductScore_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductScore>();
			var instance2 = IoC.Resolve<IRatingItemProductScore>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}