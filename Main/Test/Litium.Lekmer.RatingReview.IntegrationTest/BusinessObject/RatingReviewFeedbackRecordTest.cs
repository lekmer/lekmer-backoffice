﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingReviewFeedbackRecordTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackRecord_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingReviewFeedbackRecord>();

			Assert.IsInstanceOf<IRatingReviewFeedbackRecord>(instance);
			Assert.IsInstanceOf<IRatingReviewFeedbackRecord>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackRecord_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewFeedbackRecord>();
			var instance2 = IoC.Resolve<IRatingReviewFeedbackRecord>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}