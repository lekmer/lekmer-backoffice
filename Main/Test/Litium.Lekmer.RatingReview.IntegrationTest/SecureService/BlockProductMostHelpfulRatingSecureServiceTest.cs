﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductMostHelpfulRatingSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductMostHelpfulRatingSecureService_Resolve_Resolved()
		{
			IBlockProductMostHelpfulRatingSecureService service = IoC.Resolve<IBlockProductMostHelpfulRatingSecureService>();

			Assert.IsInstanceOf<IBlockProductMostHelpfulRatingSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductMostHelpfulRatingSecureService_ResolveTwice_SameObjects()
		{
			IBlockProductMostHelpfulRatingSecureService service1 = IoC.Resolve<IBlockProductMostHelpfulRatingSecureService>();
			IBlockProductMostHelpfulRatingSecureService service2 = IoC.Resolve<IBlockProductMostHelpfulRatingSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}