﻿using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.RatingReview.IntegrationTest.Helper;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingGroupFolderSecureServiceTest
	{
		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private IRatingGroupFolderSecureService _ratingGroupFolderSecureService;
		private IRatingGroupSecureService _ratingGroupSecureService;
		private RatingGroupFolderTestHelper _ratingGroupFolderTestHelper;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();

			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();

			_ratingGroupSecureService = _mocker.Stub<IRatingGroupSecureService>();
			_ratingGroupFolderSecureService = new RatingGroupFolderSecureService(new RatingGroupFolderRepository(), _accessValidator, _ratingGroupSecureService);
			_ratingGroupFolderTestHelper = new RatingGroupFolderTestHelper(_ratingGroupFolderSecureService, _systemUserFull);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupFolderSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupFolderSecureService>();

			Assert.IsInstanceOf<IRatingGroupFolderSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupFolderSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupFolderSecureService>();
			var instance2 = IoC.Resolve<IRatingGroupFolderSecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveNewRatingGroupFolder_FolderSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				var ratingGroupFolder = _ratingGroupFolderTestHelper.Create("Test rating group folder");

				// Save
				var actualFolder = _ratingGroupFolderSecureService.Save(_systemUserFull, ratingGroupFolder);

				Assert.IsTrue(actualFolder.Id > 0);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveRatingGroupFolderWithSameTitleOnRootLevel_FolderNotSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.Create("Test rating group folder same title");
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.Create("Test rating group folder same title");

				// Save
				var actualFolder1 = _ratingGroupFolderSecureService.Save(_systemUserFull, ratingGroupFolder1);
				var actualFolder2 = _ratingGroupFolderSecureService.Save(_systemUserFull, ratingGroupFolder2);

				// Check if it works
				Assert.Greater(actualFolder1.Id, 0, "RatingGroupFolder.Id");
				Assert.AreEqual(-1, actualFolder2.Id, "RatingGroupFolder.Id with same title");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveRatingGroupFolderWithSameTitleOnSubLevel_FolderNotSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var rootFolder = _ratingGroupFolderTestHelper.CreateAndSave("Test root rating group folder");

				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder same title", rootFolder);
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder same title", rootFolder);

				// Check if it works
				Assert.Greater(rootFolder.Id, 0, "RatingGroupFolder.Id");
				Assert.Greater(ratingGroupFolder1.Id, 0, "RatingGroupFolder.Id");
				Assert.AreEqual(-1, ratingGroupFolder2.Id, "RatingGroupFolder.Id with same title");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_UpdateRatingGroupFolder_FolderUpdated()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var rootFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test root rating group folder 1");
				var rootFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test root rating group folder 2");

				// Create folder
				var ratingGroupFolder = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder", rootFolder1);

				// Update
				ratingGroupFolder.ParentRatingGroupFolderId = rootFolder2.Id;
				ratingGroupFolder.Title = "Test rating group folder updated";

				// Update in db
				var actualFolder = _ratingGroupFolderSecureService.Save(_systemUserFull, ratingGroupFolder);

				// Check if it works
				Assert.AreEqual(ratingGroupFolder.Id, actualFolder.Id, "RatingGroupFolder.Id");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetById_GetFolderById_CorrectFolderReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 1");

				// Get all
				var actualRatingGroupFolder = _ratingGroupFolderSecureService.GetById(ratingGroupFolder.Id);

				_ratingGroupFolderTestHelper.AssertAreEquel(ratingGroupFolder, actualRatingGroupFolder);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllFolders_AllFoldersReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 1");
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 2");
				var ratingGroupFolder3 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 3");

				// Get all
				var ratingGroupFolders = _ratingGroupFolderSecureService.GetAll();

				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder1, ratingGroupFolders);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder2, ratingGroupFolders);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder3, ratingGroupFolders);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllByParent_GetAllFoldersBySomeParent_AllSubFoldersReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 1");
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 2", ratingGroupFolder1);
				var ratingGroupFolder3 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 3", ratingGroupFolder1);

				// Get by parent
				var ratingGroupFolders = _ratingGroupFolderSecureService.GetAllByParent(ratingGroupFolder1.Id);

				_ratingGroupFolderTestHelper.AssertDoesNotContain(ratingGroupFolder1, ratingGroupFolders);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder2, ratingGroupFolders);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder3, ratingGroupFolders);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetTree_SelectedIdNull_CorrectNodesReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 1");
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 2", ratingGroupFolder1);
				var ratingGroupFolder3 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 3", ratingGroupFolder2);
				var ratingGroupFolder4 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 4");

				// Get by parent
				Collection<INode> ratingGroupFolderNodes = _ratingGroupFolderSecureService.GetTree(null);

				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder1, ratingGroupFolderNodes);
				_ratingGroupFolderTestHelper.AssertDoesNotContain(ratingGroupFolder2, ratingGroupFolderNodes);
				_ratingGroupFolderTestHelper.AssertDoesNotContain(ratingGroupFolder3, ratingGroupFolderNodes);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder4, ratingGroupFolderNodes);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetTree_SecondLevelSelected_CorrectNodesReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 1");
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 2", ratingGroupFolder1);
				var ratingGroupFolder3 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 3", ratingGroupFolder2);
				var ratingGroupFolder4 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 4", ratingGroupFolder3);
				var ratingGroupFolder5 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder 5");

				// Get by parent
				Collection<INode> ratingGroupFolderNodes = _ratingGroupFolderSecureService.GetTree(ratingGroupFolder2.Id);

				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder1, ratingGroupFolderNodes);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder2, ratingGroupFolderNodes);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder3, ratingGroupFolderNodes);
				// Node 4 is not included in output
				_ratingGroupFolderTestHelper.AssertDoesNotContain(ratingGroupFolder4, ratingGroupFolderNodes);
				_ratingGroupFolderTestHelper.AssertContains(ratingGroupFolder5, ratingGroupFolderNodes);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void TryDelete_DeleteSingleFolder_FolderRemoved()
		{
			Expect.Call(_ratingGroupSecureService.GetAllByFolder(0)).Return(new Collection<IRatingGroup>()).IgnoreArguments();

			_mocker.ReplayAll();

			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder");

				// Get by parent
				bool isDeleted = _ratingGroupFolderSecureService.TryDelete(_systemUserFull, ratingGroupFolder.Id);
				var actualRatingGroupFolder = _ratingGroupFolderSecureService.GetById(ratingGroupFolder.Id);

				Assert.IsTrue(isDeleted, "RatingGroupFolder deleted");
				Assert.IsNull(actualRatingGroupFolder, "RatingGroupFolder deleted");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void TryDelete_DeleteFolderWithSubFoldersAndRatings_FolderNotRemoved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingGroupFolder1 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder");
				var ratingGroupFolder2 = _ratingGroupFolderTestHelper.CreateAndSave("Test rating group folder", ratingGroupFolder1);

				Expect
					.Call(_ratingGroupSecureService.GetAllByFolder(ratingGroupFolder1.Id))
					.Return(new Collection<IRatingGroup>());

				Expect
					.Call(_ratingGroupSecureService.GetAllByFolder(ratingGroupFolder2.Id))
					.Return(new Collection<IRatingGroup> { new RatingGroup() });

				_mocker.ReplayAll();

				// Get by parent
				bool isDeleted = _ratingGroupFolderSecureService.TryDelete(_systemUserFull, ratingGroupFolder1.Id);

				Assert.IsFalse(isDeleted, "RatingGroupFolder deleted");

				transactionScope.Dispose();
			}
		}
	}
}