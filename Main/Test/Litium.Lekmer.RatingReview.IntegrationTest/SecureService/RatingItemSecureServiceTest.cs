﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingItemProductVoteSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductVoteSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingItemProductVoteSecureService>();

			Assert.IsInstanceOf<IRatingItemProductVoteSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductVoteSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductVoteSecureService>();
			var instance2 = IoC.Resolve<IRatingItemProductVoteSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}