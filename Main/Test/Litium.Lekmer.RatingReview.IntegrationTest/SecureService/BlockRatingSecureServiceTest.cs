﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockRatingSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingSecureService_Resolve_Resolved()
		{
			IBlockRatingSecureService service = IoC.Resolve<IBlockRatingSecureService>();

			Assert.IsInstanceOf<IBlockRatingSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingSecureService_ResolveTwice_SameObjects()
		{
			IBlockRatingSecureService service1 = IoC.Resolve<IBlockRatingSecureService>();
			IBlockRatingSecureService service2 = IoC.Resolve<IBlockRatingSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}