﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingGroupCategorySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupCategorySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupCategorySecureService>();

			Assert.IsInstanceOf<IRatingGroupCategorySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupCategorySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupCategorySecureService>();
			var instance2 = IoC.Resolve<IRatingGroupCategorySecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}