﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class ReviewSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ReviewSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IReviewSecureService>();

			Assert.IsInstanceOf<IReviewSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ReviewSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IReviewSecureService>();
			var instance2 = IoC.Resolve<IReviewSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}