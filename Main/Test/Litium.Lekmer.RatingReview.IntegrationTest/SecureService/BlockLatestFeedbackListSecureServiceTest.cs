﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockLatestFeedbackListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListSecureService_Resolve_Resolved()
		{
			IBlockLatestFeedbackListSecureService service = IoC.Resolve<IBlockLatestFeedbackListSecureService>();

			Assert.IsInstanceOf<IBlockLatestFeedbackListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListSecureService_ResolveTwice_SameObjects()
		{
			IBlockLatestFeedbackListSecureService service1 = IoC.Resolve<IBlockLatestFeedbackListSecureService>();
			IBlockLatestFeedbackListSecureService service2 = IoC.Resolve<IBlockLatestFeedbackListSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}