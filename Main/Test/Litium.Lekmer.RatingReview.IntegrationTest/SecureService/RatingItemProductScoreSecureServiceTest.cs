﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingItemProductScoreSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingItemProductScoreSecureService>();

			Assert.IsInstanceOf<IRatingItemProductScoreSecureService>(service);
			Assert.IsInstanceOf<RatingItemProductScoreSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductScoreSecureService>();
			var instance2 = IoC.Resolve<IRatingItemProductScoreSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}