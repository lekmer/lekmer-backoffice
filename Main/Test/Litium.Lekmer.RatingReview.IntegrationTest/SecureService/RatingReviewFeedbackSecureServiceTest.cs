﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingReviewFeedbackSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewFeedbackSecureService>();

			Assert.IsInstanceOf<IRatingReviewFeedbackSecureService>(service);
			Assert.IsInstanceOf<RatingReviewFeedbackSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewFeedbackSecureService>();
			var instance2 = IoC.Resolve<IRatingReviewFeedbackSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}